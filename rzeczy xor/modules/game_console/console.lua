m_ConsoleFunctions = {}
m_ConsoleList = {}

m_ConsoleFunctions.BLINK_BLACK_SCREEN = 1

m_ConsoleFunctions.CHAT_ENABLED = 1
m_ConsoleFunctions.CHAT_DISABLED = 2

m_ConsoleFunctions.NPCMessageNormal = 1
m_ConsoleFunctions.NPCMessageChoose = 2
m_ConsoleFunctions.NPCMessageOther = 3
m_ConsoleFunctions.NPCMessageLine = 4
m_ConsoleFunctions.NPCMessageBlack = 5

m_ConsoleFunctions.helpDescription = {
	{'Ctrl + G', tr('hide/show chat')},
	{'Shift + Up/Down', tr('navigate message history')},
	{'Tab', tr('select next top tab')},
	{'Shift + Tab', tr('select previous top tab')},
	{'Ctrl + Tab', tr('select next left side tab')},
	{'Ctrl + Shift + Tab', tr('select previous left side tab')},
	{'Enter', tr('send current message')},
	{'Ctrl + E', tr('remove current top tab')},
	{'Ctrl + O', tr('start new conversation')}
}

m_ConsoleFunctions.communicationSettings = {
	useIgnoreList = true,
	useWhiteList = true,
	privateMessages = false,
	yelling = false,
	allowVIPs = false,
	ignoredPlayers = {},
	whitelistedPlayers = {}
}

m_ConsoleFunctions.nameColor = {
	vip = '#0066FF',
	self = '#F9A704',
	admin = '#DC143C',
	player = '#32CD32',
	npc = '#C0C0C0',
}

m_ConsoleFunctions.SpeakTypesSettings = {
	none = {},
	say = { speakType = MessageModes.Say, color = '#FFFFFF' },
	whisper = { speakType = MessageModes.Whisper, color = '#FFFFFF' },
	yell = { speakType = MessageModes.Yell, color = '#FFFFFF' },
	broadcast = { speakType = MessageModes.GamemasterPrivateFrom, color = '#F55E5E' },
	private = { speakType = MessageModes.PrivateTo, color = '#5FF7F7', private = true },
	privateRed = { speakType = MessageModes.GamemasterTo, color = '#F55E5E', private = true },
	privatePlayerToPlayer = { speakType = MessageModes.PrivateTo, color = '#9F9DFD', private = true },
	privatePlayerToNpc = { speakType = MessageModes.NpcTo, color = '#9F9DFD', private = true, npcChat = true },
	privateNpcToPlayer = { speakType = MessageModes.NpcFrom, color = '#5FF7F7', private = true, npcChat = true },
	channelYellow = { speakType = MessageModes.Channel, color = '#FFFFFF' },
	channelWhite = { speakType = MessageModes.ChannelManagement, color = '#FFFFFF' },
	channelRed = { speakType = MessageModes.GamemasterChannel, color = '#F55E5E' },
	channelOrange = { speakType = MessageModes.ChannelHighlight, color = '#FE6500' },
	monsterSay = { speakType = MessageModes.MonsterSay, color = '#FE6500', hideInConsole = true},
	monsterYell = { speakType = MessageModes.MonsterYell, color = '#FE6500', hideInConsole = true},
	rvrAnswerFrom = { speakType = MessageModes.RVRAnswer, color = '#FE6500' },
	rvrAnswerTo = { speakType = MessageModes.RVRAnswer, color = '#FE6500' },
	rvrContinue = { speakType = MessageModes.RVRContinue, color = '#FFFFFF' },
}

m_ConsoleFunctions.SpeakTypes = {
	[MessageModes.Say] = m_ConsoleFunctions.SpeakTypesSettings.say,
	[MessageModes.Whisper] = m_ConsoleFunctions.SpeakTypesSettings.whisper,
	[MessageModes.Yell] = m_ConsoleFunctions.SpeakTypesSettings.yell,
	[MessageModes.GamemasterBroadcast] = m_ConsoleFunctions.SpeakTypesSettings.broadcast,
	[MessageModes.PrivateFrom] = m_ConsoleFunctions.SpeakTypesSettings.private,
	[MessageModes.GamemasterPrivateFrom] = m_ConsoleFunctions.SpeakTypesSettings.privateRed,
	[MessageModes.NpcTo] = m_ConsoleFunctions.SpeakTypesSettings.privatePlayerToNpc,
	[MessageModes.NpcFrom] = m_ConsoleFunctions.SpeakTypesSettings.privateNpcToPlayer,
	[MessageModes.Channel] = m_ConsoleFunctions.SpeakTypesSettings.channelYellow,
	[MessageModes.ChannelManagement] = m_ConsoleFunctions.SpeakTypesSettings.channelWhite,
	[MessageModes.GamemasterChannel] = m_ConsoleFunctions.SpeakTypesSettings.channelRed,
	[MessageModes.ChannelHighlight] = m_ConsoleFunctions.SpeakTypesSettings.channelOrange,
	[MessageModes.MonsterSay] = m_ConsoleFunctions.SpeakTypesSettings.monsterSay,
	[MessageModes.MonsterYell] = m_ConsoleFunctions.SpeakTypesSettings.monsterYell,

	-- ignored types
	[MessageModes.Spell] = m_ConsoleFunctions.SpeakTypesSettings.none,
	[MessageModes.BarkLow] = m_ConsoleFunctions.SpeakTypesSettings.none,
	[MessageModes.BarkLoud] = m_ConsoleFunctions.SpeakTypesSettings.none,
}

m_ConsoleFunctions.channels = {
	[8] = 'Trade',
	[9] = 'Help',
	[10] = 'Loot Log',
	[11] = 'Damage Log'
}

m_ConsoleFunctions.channelsName = {
	['Trade'] = '8',
	['Help'] = '9',
	['Loot Log'] = '10',
	['Damage Log'] = '11'
}

m_ConsoleFunctions.linkId = 0
m_ConsoleFunctions.messageHistory = {}
m_ConsoleFunctions.copyText = ''
m_ConsoleFunctions.copyId = 0

m_ConsoleFunctions.MAX_HISTORY = 250
m_ConsoleFunctions.MAX_LINES = 100

m_ConsoleFunctions.unselectConsoleChannel = function(self, ignore)
	if ignore then
		m_ConsoleFunctions.unselectCategoryChannel(self, false)
		
		local id = tonumber(self:getId())
		if id == 12 or id == 10 or id == 11 then
			m_ConsoleFunctions.disableChat()
		else
			m_ConsoleFunctions.enableChat()
		end
	end
	
	local tab = m_ConsoleList.consoleTabBar:getCurrentTab()
	if tab then
		m_ConsoleList.consoleTabBar:unselectTab(tab)
	end
	
	if self then
		self.tabBar:selectTab(self)
	end
end

m_ConsoleFunctions.unselectCategoryChannel = function(self, ignore)
	if ignore then
		m_ConsoleFunctions.unselectConsoleChannel(self, false)
		m_ConsoleFunctions.enableChat()
	end
	
	local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
	if tab then
		m_ConsoleList.categoryTabBar:unselectTab(tab)
	end
	
	self.tabBar:selectTab(self)
end

m_ConsoleFunctions.addTabText = function(text, groupId, speaktype, tab, creatureName, sendLinkId, message, isNpc, isPlayer)
	if not tab or tab.locked or not text or #text == 0 then
		return true
	end
	
	local panel = m_ConsoleList.consoleTabBar:getTabPanel(tab)
	if not panel then
		return true
	end
	
	local consoleBuffer = panel:getChildById('consoleBuffer')
	local label
	if not sendLinkId and m_ConsoleFunctions.linkId ~= 0 then
		label = g_ui.createWidget('LinkLabel', consoleBuffer)
		label.id = m_ConsoleFunctions.linkId
		label.onMouseRelease = function (self, mousePos, mouseButton)
									m_ConsoleFunctions.processLinkMenu(mousePos, mouseButton, creatureName, text, label.id)
								end
	elseif text and text:find('http') then
		local fromIndex = text:find('http')
		local toIndex = text:len()
		for i = fromIndex, toIndex do
			if text:sub(i, i) == ' ' then
				toIndex = i
				break
			end
		end
		
		label = g_ui.createWidget('HiperLabel', consoleBuffer)
		label.message = text:sub(fromIndex, toIndex)
		label.onMouseRelease = function (self, mousePos, mouseButton)
			m_ConsoleFunctions.processHiperMenu(mousePos, mouseButton, creatureName, self)
		end
	else
		label = g_ui.createWidget('GeneralChatLabel', consoleBuffer)
		label:setColor(speaktype.color)
		if tab:getId() == '11' and isInArray(CriticalMessageTypes, speaktype.messageType) and speaktype.combatType ~= 0 then
			if speaktype.combatType ~= 0 then
				label:setOn(true)
				
				local icon = label:getChildById('icon')
				icon:show()
				modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, modules.game_lookat.m_LookAtFunctions.getIconByCombatType(speaktype.combatType))
			end
		end
		
		label.onMouseRelease = function (self, mousePos, mouseButton)
			m_ConsoleFunctions.processMessageMenu(mousePos, mouseButton, creatureName, text, self, tab)
		end
	end

	local description = creatureName
	if modules.client_options.getOption('showTimestampsInConsole') then
		if creatureName then
			description = os.date('%H:%M') .. ' ' .. creatureName .. ' '
		else
			description = os.date('%H:%M') .. ' '
		end

		text = os.date('%H:%M') .. ' ' .. text
	end
	
	local labelName = label:getChildById('name')
	if labelName and description then
		local player = g_game.getLocalPlayer()
		local color
		if player:hasVip(creatureName) then
			color = m_ConsoleFunctions.nameColor.vip
		elseif g_game.getCharacterName() == creatureName then
			color = m_ConsoleFunctions.nameColor.self
		elseif groupId > 1 then
			color = m_ConsoleFunctions.nameColor.admin
		elseif isNpc then
			color = m_ConsoleFunctions.nameColor.npc
		else
			color = m_ConsoleFunctions.nameColor.player
		end
		
		if color then
			labelName:setText(description)
			labelName:setColor(color)
		end
	end
	
	label:setId('consoleLabel' .. consoleBuffer:getChildCount())
	m_ConsoleList.consoleTabBar:blinkTab(tab)
	
	text = g_game.setHighlightedText(label, text, '#1E90FF')
	label.name = creatureName

	if consoleBuffer:getChildCount() > m_ConsoleFunctions.MAX_LINES then
		consoleBuffer:getFirstChild():destroy()
	end
end

m_ConsoleFunctions.getCurrentTab = function()
	return m_ConsoleList.consoleTabBar:getCurrentTab() or m_ConsoleList.categoryTabBar:getCurrentTab()
end

m_ConsoleFunctions.clearChannel = function(tab)
	local tab = tab or m_ConsoleList.consoleTabBar:getCurrentTab() or m_ConsoleList.categoryTabBar:getCurrentTab()
	if tab:getId() == '12' then
		local panel = tab.tabPanel:getChildById('consoleBuffer')
		for i = panel:getChildCount(), 1, -1 do
			local widget = panel:getChildByIndex(i)
			if not widget.id then
				widget:destroy()
			end
		end
	else
		tab.tabPanel:getChildById('consoleBuffer'):destroyChildren()
	end
end

m_ConsoleFunctions.getOwnPrivateTab = function()
	if not m_ConsoleList.ownPrivateName then
		return false
	end
	
	return m_ConsoleFunctions.getTab(m_ConsoleList.ownPrivateName, m_ConsoleList.consoleTabBar)
end

m_ConsoleFunctions.setTextEditText = function(text)
	m_ConsoleList.consoleTextEdit:setText(text)
	m_ConsoleList.consoleTextEdit:setCursorPos(-1)
end

m_ConsoleFunctions.enableChat = function()
	if m_ConsoleFunctions.lockId ~= 0 then
		return true
	end
	
	if m_ConsoleList.expandButton:isOn() then
		m_ConsoleList.enabled = true
		return true
	end
	
	m_ConsoleList.consoleTextEdit:show()
	m_ConsoleList.consoleTextEdit:setEnabled(true)
	m_ConsoleList.consoleTextEdit:focus()
end

m_ConsoleFunctions.disableChat = function()
	m_ConsoleList.consoleTextEdit:hide()
	m_ConsoleList.consoleTextEdit:setEnabled(false)
end

m_ConsoleFunctions.expandChat = function(instant)
	if instant or m_ConsoleList.expandButton:isOn() then
		m_ConsoleList.bottomResizeBorder:show()
		m_ConsoleList.rightResizeBorder:show()
		m_ConsoleList.consoleTabBar:show()
		m_ConsoleList.consoleContentPanel:show()
		m_ConsoleList.title:show()
		m_ConsoleList.background:show()
		m_ConsoleList.consoleTextEdit:show()
		m_ConsoleList.enterButton:show()
		m_ConsoleList.newWindowButton:show()
		m_ConsoleList.dragWindowButton:show()
		m_ConsoleList.clearWindowButton:show()
		m_ConsoleList.nextChannelButton:show()
		m_ConsoleList.prevChannelButton:show()
		m_ConsoleList.helpButton:show()
		m_ConsoleList.leafs:show()
		m_ConsoleList.backgroundPanel:setWidth(m_ConsoleList.oldSize or 507)
		m_ConsoleList.chatStatus = m_ConsoleFunctions.CHAT_ENABLED
		
		local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
		if not tab then
			m_ConsoleList.defaultTab.tabBar:selectTab(m_ConsoleList.defaultTab)
			m_ConsoleList.enabled = true
		end
		
		m_ConsoleList.expandButton:setOn(false)
		m_ConsoleList.expandButton:rotate(0)
		m_ConsoleList.expandButton:setTooltip(tr("Disable chat mode, allow to walk using WASD (Ctrl+G)"))
		
		if m_ConsoleList.enabled then
			m_ConsoleFunctions.enableChat()
		else
			m_ConsoleFunctions.disableChat()
		end
	else
		m_ConsoleList.bottomResizeBorder:hide()
		m_ConsoleList.rightResizeBorder:hide()
		m_ConsoleList.consoleTabBar:hide()
		m_ConsoleList.consoleContentPanel:hide()
		m_ConsoleList.title:hide()
		m_ConsoleList.background:hide()
		m_ConsoleList.consoleTextEdit:hide()
		m_ConsoleList.enterButton:hide()
		m_ConsoleList.newWindowButton:hide()
		m_ConsoleList.dragWindowButton:hide()
		m_ConsoleList.clearWindowButton:hide()
		m_ConsoleList.nextChannelButton:hide()
		m_ConsoleList.prevChannelButton:hide()
		m_ConsoleList.helpButton:hide()
		m_ConsoleList.leafs:hide()
		m_ConsoleList.oldSize = m_ConsoleList.backgroundPanel:getWidth()
		m_ConsoleList.backgroundPanel:setWidth(42)
		m_ConsoleList.chatStatus = m_ConsoleFunctions.CHAT_DISABLED
		m_ConsoleList.enabled = m_ConsoleList.consoleTextEdit:isEnabled()
		
		m_ConsoleFunctions.hoverDestroy()
		m_ConsoleFunctions.disableChat()
		m_ConsoleList.expandButton:setTooltip(tr("Enable chat mode, disable walk using WASD (Ctrl+G)"))
		m_ConsoleList.expandButton:setOn(true)
		m_ConsoleList.expandButton:rotate(180)
	end
end

m_ConsoleFunctions.navigateMessageHistory = function(step)
	if not m_ConsoleList.consoleTextEdit:isEnabled() then
		return true
	end
	
	m_ConsoleFunctions.clearLinkItem()
	
	local numCommands = #m_ConsoleFunctions.messageHistory
	if numCommands > 0 then
		m_ConsoleList.currentMessageIndex = math.min(math.max(m_ConsoleList.currentMessageIndex + step, 0), numCommands)
		if m_ConsoleList.currentMessageIndex > 0 then
			local command = m_ConsoleFunctions.messageHistory[numCommands - m_ConsoleList.currentMessageIndex + 1]
			m_ConsoleFunctions.setTextEditText(command)
		else
			m_ConsoleList.consoleTextEdit:clearText()
		end
	end
end

m_ConsoleFunctions.processChannelTabMenu = function(tab, mousePos, mouseButton)
	local menu = g_ui.createWidget('PopupMenu')
	menu:setGameMenu(true)

	local channelName = tab:getText()
	if tab ~= m_ConsoleList.defaultTab and tab ~= m_ConsoleList.serverTab then
		menu:addOption(tr('Close'), function() m_ConsoleFunctions.removeTab(channelName) end)
		menu:addSeparator()
	end
	
	menu:addOption(tr('Clear Messages'), function() m_ConsoleFunctions.clearChannel(tab) end)
	menu:display(mousePos)
end

m_ConsoleFunctions.processTextEditMenu = function(mousePos, mouseButton)
	if mouseButton == MouseRightButton and ((m_ConsoleFunctions.copyText ~= '' and m_ConsoleFunctions.copyId ~= 0) or g_window.getClipboardText() ~= '') then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		if m_ConsoleFunctions.copyText ~= '' and m_ConsoleFunctions.copyId ~= 0 then
			menu:addOption(tr('Paste link'), function() onLootLink(m_ConsoleFunctions.copyId, m_ConsoleFunctions.copyText) end)
		end
		
		if g_window.getClipboardText() ~= '' then
			menu:addOption(tr('Paste'), function() 
												m_ConsoleList.consoleTextEdit:setText(g_window.getClipboardText())
												m_ConsoleList.consoleTextEdit:setColor('#AAAAAA')
												
												m_ConsoleFunctions.linkItem = nil
												m_ConsoleFunctions.linkId = 0
											end)
		end
		
		menu:display(mousePos)
	end
end

m_ConsoleFunctions.processHiperMenu = function(mousePos, mouseButton, creatureName, self)
	if mouseButton == MouseLeftButton then
		g_game.goToWebSite(self.message)
	elseif mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		
		if creatureName and #creatureName > 0 then
			if creatureName ~= g_game.getCharacterName() then
				menu:addOption(tr('Message to ') .. creatureName, function () g_game.openPrivateChannel(creatureName) end)
				if not g_game.getLocalPlayer():hasVip(creatureName) then
					menu:addOption(tr('Add to VIP list'), function () g_game.addVip(creatureName) end)
				end
				
				if m_ConsoleFunctions.getOwnPrivateTab() then
					menu:addSeparator()
					menu:addOption(tr('Invite to private chat'), function() g_game.inviteToOwnChannel(creatureName) end)
					menu:addOption(tr('Exclude from private chat'), function() g_game.excludeFromOwnChannel(creatureName) end)
				end
				
				if isIgnored(creatureName) then
					menu:addOption(tr('Unignore') .. ' ' .. creatureName, function() removeIgnoredPlayer(creatureName) end)
				else
					menu:addOption(tr('Ignore') .. ' ' .. creatureName, function() addIgnoredPlayer(creatureName) end)
				end
				menu:addSeparator()
			end

			menu:addOption(tr('Copy name'), function () g_window.setClipboardText(creatureName) end)
		end
		
		menu:addOption(tr('Copy'), function() g_window.setClipboardText(self.message) end)
		menu:display(mousePos)
	end
end

m_ConsoleFunctions.processLinkMenu = function(mousePos, mouseButton, creatureName, text, id)
	if mouseButton == MouseLeftButton then
		g_game.useLookLink(id)
	elseif mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		
		if creatureName and #creatureName > 0 then
			if creatureName ~= g_game.getCharacterName() then
				menu:addOption(tr('Message to ') .. creatureName, function () g_game.openPrivateChannel(creatureName) end)
				if not g_game.getLocalPlayer():hasVip(creatureName) then
					menu:addOption(tr('Add to VIP list'), function () g_game.addVip(creatureName) end)
				end
				
				if m_ConsoleFunctions.getOwnPrivateTab() then
					menu:addSeparator()
					menu:addOption(tr('Invite to private chat'), function() g_game.inviteToOwnChannel(creatureName) end)
					menu:addOption(tr('Exclude from private chat'), function() g_game.excludeFromOwnChannel(creatureName) end)
				end
				
				if isIgnored(creatureName) then
					menu:addOption(tr('Unignore') .. ' ' .. creatureName, function() removeIgnoredPlayer(creatureName) end)
				else
					menu:addOption(tr('Ignore') .. ' ' .. creatureName, function() addIgnoredPlayer(creatureName) end)
				end
				menu:addSeparator()
			end

			menu:addOption(tr('Copy name'), function () g_window.setClipboardText(creatureName) end)
		end
		
		menu:addOption(tr('Copy'), function()
											local found = text:find(creatureName)
											if found then
												m_ConsoleFunctions.copyText = text:sub(creatureName:len() + 8, text:len())
												m_ConsoleFunctions.copyId = id
											end
										end)
		
		menu:display(mousePos)
	end
end

m_ConsoleFunctions.processMessageMenu = function(mousePos, mouseButton, creatureName, text, label, tab)
	if mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		
		if creatureName and #creatureName > 0 then
			if creatureName ~= g_game.getCharacterName() then
				menu:addOption(tr('Message to ') .. creatureName, function () g_game.openPrivateChannel(creatureName) end)
				
				if not g_game.getLocalPlayer():hasVip(creatureName) then
					menu:addOption(tr('Add to VIP list'), function () g_game.addVip(creatureName) end)
				end
				
				if m_ConsoleFunctions.getOwnPrivateTab() then
					menu:addSeparator()
					menu:addOption(tr('Invite to private chat'), function() g_game.inviteToOwnChannel(creatureName) end)
					menu:addOption(tr('Exclude from private chat'), function() g_game.excludeFromOwnChannel(creatureName) end)
				end
				
				if isIgnored(creatureName) then
					menu:addOption(tr('Unignore') .. ' ' .. creatureName, function() removeIgnoredPlayer(creatureName) end)
				else
					menu:addOption(tr('Ignore') .. ' ' .. creatureName, function() addIgnoredPlayer(creatureName) end)
				end
				menu:addSeparator()
			end

			menu:addOption(tr('Copy name'), function () g_window.setClipboardText(creatureName) end)
		end
		
		if label:hasSelection() then
			menu:addOption(tr('Copy'), function() g_window.setClipboardText(label:getSelection()) end, '(Ctrl+C)')
		end
		
		menu:addOption(tr('Copy message'), function() g_window.setClipboardText(text) end)
		menu:addOption(tr('Select all'), function() label:selectAll() end)
		menu:display(mousePos)
	end
end

m_ConsoleFunctions.clear = function()
	for k, channelName in pairs(m_ConsoleFunctions.channels) do
		local tab = m_ConsoleList.consoleTabBar:getTab(channelName)
		if tab then
			m_ConsoleList.consoleTabBar:removeTab(tab)
			m_ConsoleFunctions.channels[k] = nil
		end
	end
	
	m_ConsoleList.consoleTabBar:removeTab(m_ConsoleList.defaultTab)
	m_ConsoleList.defaultTab = nil
	m_ConsoleList.consoleTabBar:removeTab(m_ConsoleList.serverTab)
	m_ConsoleList.serverTab = nil
	
	m_ConsoleList.categoryTabBar:removeTab(m_ConsoleList.tradeTab)
	m_ConsoleList.tradeTab = nil
	m_ConsoleList.categoryTabBar:removeTab(m_ConsoleList.helpTab)
	m_ConsoleList.helpTab = nil
	m_ConsoleList.categoryTabBar:removeTab(m_ConsoleList.npcTab)
	m_ConsoleList.npcTab = nil
	m_ConsoleList.categoryTabBar:removeTab(m_ConsoleList.lootTab)
	m_ConsoleList.lootTab = nil
	m_ConsoleList.categoryTabBar:removeTab(m_ConsoleList.damageTab)
	m_ConsoleList.damageTab = nil
	
	if m_ConsoleList.channelsWindow then
		m_ConsoleList.channelsWindow:destroy()
		m_ConsoleList.channelsWindow = nil
	end
	
	if m_ConsoleList.communicationWindow then
		m_ConsoleList.communicationWindow:destroy()
		m_ConsoleList.communicationWindow = nil
	end

	m_ConsoleList.consoleTextEdit:clearText()
	m_ConsoleFunctions.clearLinkItem()
end

m_ConsoleFunctions.displaySettings = function(self)
	if m_ConsoleList.communicationWindow then
		return true
	end
	
	self:getChildById('mask'):show()
	m_ConsoleList.communicationWindow = g_ui.displayUI('communicationwindow')
	local ignoreListPanel = m_ConsoleList.communicationWindow:getChildById('ignoreList')
	local whiteListPanel = m_ConsoleList.communicationWindow:getChildById('whiteList')
	m_ConsoleList.communicationWindow.onDestroy = function()
															self:getChildById('mask'):hide()
															m_ConsoleList.communicationWindow = nil
														end
	
	local useIgnoreListBox = m_ConsoleList.communicationWindow:getChildById('checkboxUseIgnoreList')
	useIgnoreListBox:setChecked(m_ConsoleFunctions.communicationSettings.useIgnoreList)
	local useWhiteListBox = m_ConsoleList.communicationWindow:getChildById('checkboxUseWhiteList')
	useWhiteListBox:setChecked(m_ConsoleFunctions.communicationSettings.useWhiteList)

	local removeIgnoreButton = m_ConsoleList.communicationWindow:getChildById('buttonIgnoreRemove')
	removeIgnoreButton:disable()
	ignoreListPanel.onChildFocusChange = function()
										removeIgnoreButton:enable()
									end
	
	removeIgnoreButton.onClick = function()
										local selection = ignoreListPanel:getFocusedChild()
										if selection then
											ignoreListPanel:removeChild(selection)
											selection:destroy()
										end
										
										removeIgnoreButton:disable()
									end

	local removeWhitelistButton = m_ConsoleList.communicationWindow:getChildById('buttonWhitelistRemove')
	removeWhitelistButton:disable()
	whiteListPanel.onChildFocusChange = function()
											removeWhitelistButton:enable()
										end
	
	removeWhitelistButton.onClick = function()
										local selection = whiteListPanel:getFocusedChild()
										if selection then
											whiteListPanel:removeChild(selection)
											selection:destroy()
										end
										
										removeWhitelistButton:disable()
									end

	local newlyIgnoredPlayers = {}
	local addIgnoreName = m_ConsoleList.communicationWindow:getChildById('ignoreNameEdit')
	local addIgnoreButton = m_ConsoleList.communicationWindow:getChildById('buttonIgnoreAdd')
	local addIgnoreFunction = function()
									local newEntry = addIgnoreName:getText()
									if newEntry == '' then
										return true
									end
									
									if table.find(getIgnoredPlayers(), newEntry) then
										return true
									end
									
									if table.find(newlyIgnoredPlayers, newEntry) then
										return true
									end
									
									local label = g_ui.createWidget('IgnoreListLabel', ignoreListPanel)
									label:setText(newEntry)
									table.insert(newlyIgnoredPlayers, newEntry)
									addIgnoreName:setText('')
								end
	
	addIgnoreButton.onClick = addIgnoreFunction

	local newlyWhitelistedPlayers = {}
	local addWhitelistName = m_ConsoleList.communicationWindow:getChildById('whitelistNameEdit')
	local addWhitelistButton = m_ConsoleList.communicationWindow:getChildById('buttonWhitelistAdd')
	local addWhitelistFunction = function()
									local newEntry = addWhitelistName:getText()
									if newEntry == '' then
										return true
									end
									
									if table.find(getWhitelistedPlayers(), newEntry) then
										return true
									end
									
									if table.find(newlyWhitelistedPlayers, newEntry) then
										return true
									end
									
									local label = g_ui.createWidget('WhiteListLabel', whiteListPanel)
									label:setText(newEntry)
									table.insert(newlyWhitelistedPlayers, newEntry)
									addWhitelistName:setText('')
								end
	
	addWhitelistButton.onClick = addWhitelistFunction

	m_ConsoleList.communicationWindow.onEnter = function()
									if addWhitelistName:isFocused() then
										addWhitelistFunction()
									elseif addIgnoreName:isFocused() then
										addIgnoreFunction()
									end
								end

	local ignorePrivateMessageBox = m_ConsoleList.communicationWindow:getChildById('checkboxIgnorePrivateMessages')
	ignorePrivateMessageBox:setChecked(m_ConsoleFunctions.communicationSettings.privateMessages)
	local ignoreYellingBox = m_ConsoleList.communicationWindow:getChildById('checkboxIgnoreYelling')
	ignoreYellingBox:setChecked(m_ConsoleFunctions.communicationSettings.yelling)
	local allowVIPsBox = m_ConsoleList.communicationWindow:getChildById('checkboxAllowVIPs')
	allowVIPsBox:setChecked(m_ConsoleFunctions.communicationSettings.allowVIPs)

	local saveButton = m_ConsoleList.communicationWindow:recursiveGetChildById('buttonSave')
	saveButton.onClick = function()
									m_ConsoleFunctions.communicationSettings.ignoredPlayers = {}
									for i = 1, ignoreListPanel:getChildCount() do
										addIgnoredPlayer(ignoreListPanel:getChildByIndex(i):getText())
									end

									m_ConsoleFunctions.communicationSettings.whitelistedPlayers = {}
									for i = 1, whiteListPanel:getChildCount() do
										addWhitelistedPlayer(whiteListPanel:getChildByIndex(i):getText())
									end

									m_ConsoleFunctions.communicationSettings.useIgnoreList = useIgnoreListBox:isChecked()
									m_ConsoleFunctions.communicationSettings.useWhiteList = useWhiteListBox:isChecked()
									m_ConsoleFunctions.communicationSettings.yelling = ignoreYellingBox:isChecked()
									m_ConsoleFunctions.communicationSettings.privateMessages = ignorePrivateMessageBox:isChecked()
									m_ConsoleFunctions.communicationSettings.allowVIPs = allowVIPsBox:isChecked()
									m_ConsoleList.communicationWindow:destroy()
									m_ConsoleList.communicationWindow = nil
								end

	local cancelButton = m_ConsoleList.communicationWindow:recursiveGetChildById('buttonCancel')
	cancelButton.onClick = function()
									m_ConsoleList.communicationWindow:destroy()
									m_ConsoleList.communicationWindow = nil
								end

	local ignoredPlayers = getIgnoredPlayers()
	for i = 1, #ignoredPlayers do
		local label = g_ui.createWidget('IgnoreListLabel', ignoreListPanel)
		label:setText(ignoredPlayers[i])
	end
	
	local whitelistedPlayers = getWhitelistedPlayers()
	for i = 1, #whitelistedPlayers do
		local label = g_ui.createWidget('WhiteListLabel', whiteListPanel)
		label:setText(whitelistedPlayers[i])
	end
end

m_ConsoleFunctions.load = function()
	local settings = g_settings.getNode('game_console')
	if settings then
		m_ConsoleFunctions.messageHistory = settings.messageHistory or {}
	end
  
	loadCommunicationSettings()
end

m_ConsoleFunctions.save = function()
	local settings = {}
	settings.messageHistory = m_ConsoleFunctions.messageHistory
	g_settings.setNode('game_console', settings)
end

m_ConsoleFunctions.addPrivateChannel = function(receiver)
	m_ConsoleFunctions.channels[receiver] = receiver
	return m_ConsoleFunctions.addTab(receiver, true, m_ConsoleList.consoleTabBar, m_ConsoleFunctions.processChannelTabMenu)
end

m_ConsoleFunctions.onTextChange = function(self)
	local text = self:getText()
	if text ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
	
	if m_ConsoleFunctions.linkItem and self:getText() == '' then
		m_ConsoleFunctions.clearLinkItem()
	end
end

m_ConsoleFunctions.addText = function(text, groupId, speaktype, tabName, creatureName, subTabName, sendLinkId, message, isNpc, isPlayer)
	local tab = nil
	if subTabName then
		tab = m_ConsoleFunctions.getTab(subTabName, m_ConsoleList.consoleTabBar)
	end
	
	if not tab then
		if subTabName then
			tab = m_ConsoleFunctions.getTab(m_ConsoleFunctions.channelsName[subTabName], m_ConsoleList.categoryTabBar)
		end
		
		if not tab then
			tab = m_ConsoleFunctions.getTab(tabName, m_ConsoleList.consoleTabBar)
	
			if not tab then
				tab = m_ConsoleFunctions.getTab(m_ConsoleFunctions.channelsName[tabName], m_ConsoleList.categoryTabBar)
			end
		end
	end
	
	if tab then
		m_ConsoleFunctions.addTabText(text, groupId, speaktype, tab, creatureName, sendLinkId, message, isNpc, isPlayer)
	end
end

m_ConsoleFunctions.getTab = function(name, parent)
	return parent:getTab(name)
end

m_ConsoleFunctions.addTab = function(name, focus, parent, func, clip, tooltip)
	local tab = m_ConsoleFunctions.getTab(name, parent)
	if not tab then
		tab = parent:addTab(name, nil, func, clip, tooltip)
		tab.onClick = m_ConsoleFunctions.unselectCategoryChannel
	end
	
	if focus then
		m_ConsoleFunctions.enableChat()
		parent:selectTab(tab)
	end
	
	return tab
end

m_ConsoleFunctions.removeTab = function(tab)
	if type(tab) == 'string' then
		tab = m_ConsoleList.consoleTabBar:getTab(tab)
	end
	
	if tab == m_ConsoleList.defaultTab or tab == m_ConsoleList.serverTab or tab == m_ConsoleList.helpTab or tab == m_ConsoleList.npcTab or
		tab == m_ConsoleList.lootTab or tab == m_ConsoleList.damageTab or tab == m_ConsoleList.tradeTab then
		return false
	end
	
	local id = tonumber(tab:getId())
	if id and id > 0 then
		-- notificate the server that we are leaving the channel
		for k, v in pairs(m_ConsoleFunctions.channels) do
			if (k == id) then
				m_ConsoleFunctions.channels[k] = nil
			end
		end
		
		g_game.leaveChannel(id)
	end
	
	m_ConsoleList.consoleTabBar:removeTab(tab)
end

m_ConsoleFunctions.applyMessagePrefixies = function(name, level, message)
	if name then
		if modules.client_options.getOption('showLevelsInConsole') and level > 0 then
			message = name .. ' [' .. level .. '] ' .. message
		else
			message = name .. ' ' .. message
		end
	end
	
	return message
end

m_ConsoleFunctions.clearLinkItem = function()
	m_ConsoleList.consoleTextEdit:setColor('#AAAAAA')
	
	m_ConsoleFunctions.linkItem = nil
	m_ConsoleFunctions.linkId = 0
end

m_ConsoleFunctions.enabledChat = function()
	return not canUseWASD()
	-- return not m_ConsoleList.expandButton:isOn()
end

m_ConsoleFunctions.sendMessage = function(message, tab)
	local tab = tab or m_ConsoleFunctions.getCurrentTab()
	if not tab then
		g_game.talkChannel(m_ConsoleFunctions.SpeakTypesSettings['say'].speakType, 0, message)
		return
	end
	
	-- when talking on server log, the message goes to default channel
	local name = tab:getText()
	if tab == m_ConsoleList.serverTab then
		tab = m_ConsoleList.defaultTab
		name = m_ConsoleList.defaultTab:getText()
	end
	
	-- handling chat commands
	local channel = tab.channelId
	if not channel then
		channel = tonumber(tab:getId())
	end
	
	local originalMessage = message
	local chatCommandSayMode
	local chatCommandPrivate
	local chatCommandPrivateReady
	local chatCommandMessage

	-- player used yell command
	chatCommandMessage = message:match("^%#[y|Y] (.*)")
	if chatCommandMessage ~= nil then
		chatCommandSayMode = 'yell'
		channel = 0
		message = chatCommandMessage
	end

	-- player used whisper
	chatCommandMessage = message:match("^%#[w|W] (.*)")
	if chatCommandMessage ~= nil then
		chatCommandSayMode = 'whisper'
		message = chatCommandMessage
		channel = 0
	end

	-- player say
	chatCommandMessage = message:match("^%#[s|S] (.*)")
	if chatCommandMessage ~= nil then
		chatCommandSayMode = 'say'
		message = chatCommandMessage
		channel = 0
	end
  
	-- player red talk on channel
	chatCommandMessage = message:match("^%#[c|C] (.*)")
	if chatCommandMessage ~= nil then
		chatCommandSayMode = 'channelRed'
		message = chatCommandMessage
	end
  
	-- player broadcast
	chatCommandMessage = message:match("^%#[b|B] (.*)")
	if chatCommandMessage ~= nil then
		chatCommandSayMode = 'broadcast'
		message = chatCommandMessage
		channel = 0
	end

	local findIni, findEnd, chatCommandInitial, chatCommandPrivate, chatCommandEnd, chatCommandMessage = message:find("([%*%@])(.+)([%*%@])(.*)")
	if findIni ~= nil and findIni == 1 then -- player used private chat command
		if chatCommandInitial == chatCommandEnd then
			chatCommandPrivateRepeat = false
			if chatCommandInitial == "*" then
				m_ConsoleFunctions.setTextEditText('*'.. chatCommandPrivate .. '* ')
			end
			message = chatCommandMessage:trim()
			chatCommandPrivateReady = true
		end
	end

	message = message:gsub("^(%s*)(.*)","%2") -- remove space characters from message init
	if #message == 0 then
		return
	end

	-- add new command to history
	if m_ConsoleFunctions.linkId == 0 then
		m_ConsoleList.currentMessageIndex = 0
		if #m_ConsoleFunctions.messageHistory == 0 or m_ConsoleFunctions.messageHistory[#m_ConsoleFunctions.messageHistory] ~= originalMessage then
			table.insert(m_ConsoleFunctions.messageHistory, originalMessage)
			if #m_ConsoleFunctions.messageHistory > m_ConsoleFunctions.MAX_HISTORY then
				table.remove(m_ConsoleFunctions.messageHistory, 1)
			end
		end
	end
	
	local speaktypedesc
	if (channel or tab == m_ConsoleList.defaultTab) and not chatCommandPrivateReady then
		if tab == m_ConsoleList.defaultTab then
			speaktypedesc = chatCommandSayMode or 'say'
		else
			speaktypedesc = chatCommandSayMode or 'channelYellow'
		end
		
		g_game.talkChannel(m_ConsoleFunctions.SpeakTypesSettings[speaktypedesc].speakType, channel, message, m_ConsoleFunctions.linkId)
		return true
	else
		local isPrivateCommand = false
		local priv = true
		local tabname = name
		if chatCommandPrivateReady then
			speaktypedesc = 'privatePlayerToPlayer'
			name = chatCommandPrivate
			isPrivateCommand = true
		else
			speaktypedesc = 'privatePlayerToPlayer'
		end
		
		local speaktype = m_ConsoleFunctions.SpeakTypesSettings[speaktypedesc]
		local player = g_game.getLocalPlayer()
		g_game.talkPrivate(speaktype.speakType, name, message, m_ConsoleFunctions.linkId)
		
		message = m_ConsoleFunctions.applyMessagePrefixies(g_game.getCharacterName(), player:getLevel(), message)
		m_ConsoleFunctions.addPrivateText(message, speaktype, tabname, isPrivateCommand, g_game.getCharacterName())
	end
end

m_ConsoleFunctions.addPrivateText = function(text, speaktype, name, isPrivateCommand, creatureName)
	local focus = false
	local privateTab = m_ConsoleFunctions.getTab(name, m_ConsoleList.consoleTabBar)
	if not privateTab then
		m_ConsoleFunctions.addTabText(text, 0, speaktype, m_ConsoleList.defaultTab, creatureName)
		
		privateTab = m_ConsoleFunctions.addTab(name, false, m_ConsoleList.consoleTabBar, m_ConsoleFunctions.processChannelTabMenu)
		m_ConsoleFunctions.channels[name] = name
	elseif focus then
		m_ConsoleFunctions.enableChat()
		m_ConsoleList.categoryTabBar:selectTab(privateTab)
	end
	
	m_ConsoleFunctions.addTabText(text, 0, speaktype, privateTab, creatureName)
end

m_ConsoleFunctions.clearNPCTab = function(tab)
	local var = m_ConsoleFunctions.getTab('12', m_ConsoleList.categoryTabBar)
	if not tab then
		if var then
			local panel = m_ConsoleList.consoleTabBar:getTabPanel(var)
			if not panel then
				return true
			end
			
			tab = panel:getChildById('consoleBuffer')
		end
	end
	
	if tab then
		var:getChildById('blink'):hide()
		for i = tab:getChildCount(), 1, -1 do
			local widget = tab:getChildByIndex(i)
			if widget.id then
				if widget.id < 10 then
					g_keyboard.unbindKeyDown(tostring(widget.id), nil, nil, true)
				end
				widget:destroy()
			end
		end
	end
end

m_ConsoleFunctions.selectMessage = function(mouseButton, name, message, id, tab)
	local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
	if not tab or tab:getId() ~= '12' then
		return true
	end
	
	if mouseButton ~= MouseLeftButton or m_ConsoleList.exhausted then
		return false
	end
	
	m_ConsoleFunctions.clearNPCTab(tab)
	
	m_ConsoleList.exhausted = true
	scheduleEvent(function() m_ConsoleList.exhausted = false end, 100)
	
	local panel = m_ConsoleList.consoleTabBar:getTabPanel(tab)
	if not panel then
		return true
	end
	
	local consoleBuffer = panel:getChildById('consoleBuffer')
	local data = {}
	data.playerName = name
	data.message = message
	data.type = m_ConsoleFunctions.NPCMessageOther
	m_ConsoleFunctions.createNewMessage(consoleBuffer, data)
	
	g_game.sendChooseMessage(id)
end

m_ConsoleFunctions.selectNPC = function(name)
	modules.game_encyclopedia.m_NpcGlosaryFunctions.selectNpc(name)
end

m_ConsoleFunctions.createNewMessage = function(tab, data)
	if m_ConsoleList.expandButton:isOn() then
		m_ConsoleFunctions.expandChat(true)
		m_ConsoleFunctions.disableChat()
	end
	
	if data.type == m_ConsoleFunctions.NPCMessageNormal or data.type == m_ConsoleFunctions.NPCMessageOther or data.type == m_ConsoleFunctions.NPCMessageLine then
		local widget = g_ui.createWidget('ChooseMessage', tab)
		widget:setParent(tab)
		widget:setText(data.message)
		widget.onMouseRelease = function (self, mousePos, mouseButton)
									m_ConsoleFunctions.processMessageMenu(mousePos, mouseButton, nil, data.message, self, nil)
								end
		
		if (data.type == m_ConsoleFunctions.NPCMessageNormal and data.time == m_ConsoleFunctions.NPCMessageNormal) or data.type == m_ConsoleFunctions.NPCMessageOther then
			widget:setOn(true)
		elseif data.type == m_ConsoleFunctions.NPCMessageLine then
			widget:setTextAlign(AlignCenter)
		else
			local tmpWidget = widget:getChildById('name')
			local name = data.message:sub(0, string.find(data.message, ' -') - 1)
			tmpWidget:show()
			tmpWidget:setText(name .. ' -')
			tmpWidget.name = name
		end
	elseif data.type == m_ConsoleFunctions.NPCMessageChoose then
		local var = m_ConsoleFunctions.getTab('12', m_ConsoleList.categoryTabBar)
		var:getChildById('blink'):show()
		
		local messages = data.message:explode(';')
		for i = 1, #messages / 3 do
			local widget = g_ui.createWidget('MessageLabel', tab)
			widget:setParent(tab)
			widget:setText(messages[i * 3 - 2])
			widget.id = i
			widget.onMouseRelease = function(widget, mousePosition, mouseButton)
										m_ConsoleFunctions.selectMessage(mouseButton, data.name, messages[i * 3 - 2], tostring(messages[i * 3 - 1]), tab)
									end
			
			if tonumber(messages[i * 3]) == 2 then
				widget:setOn(true)
			end
			
			local id = widget:getChildById('number')
			id:setText(i .. ':- ')
			
			if i < 10 then
				g_keyboard.bindKeyDown(tostring(i), function()
					m_ConsoleFunctions.selectMessage(MouseLeftButton, data.name, messages[i * 3 - 2], tostring(messages[i * 3 - 1]), tab)
				end, nil, true)
			end
		end
	end
end

function init()
	connect(g_game, {
		onTalk = onTalk,
		onOpenChannel = onOpenChannel,
		onChannelList = onChannelList,
		onOpenPrivateChannel = onOpenPrivateChannel,
		onOpenOwnPrivateChannel = onOpenOwnPrivateChannel,
		onCloseChannel = onCloseChannel,
		onGameStart = online,
		onGameEnd = offline,
		onLootLink = onLootLink,
		onAddMessageTypes = onAddMessageTypes
	})
	
	local rootPanel = modules.game_interface.getRootPanel()
	m_ConsoleList.backgroundPanel = g_ui.loadUI('console', rootPanel)
	m_ConsoleList.backgroundPanel:setPosition({x = 12, y = rootPanel:getHeight() + 200})
	m_ConsoleList.backgroundPanel:setup()
	
	m_ConsoleList.consoleTextEdit = m_ConsoleList.backgroundPanel:getChildById('consoleTextEdit')
	m_ConsoleList.consoleTextEdit.onMouseRelease = function (self, mousePos, mouseButton)
		m_ConsoleFunctions.processTextEditMenu(mousePos, mouseButton)
	end
	
	m_ConsoleList.background = m_ConsoleList.backgroundPanel:getChildById('background')
	m_ConsoleList.title = m_ConsoleList.backgroundPanel:getChildById('title')
	m_ConsoleList.enterButton = m_ConsoleList.backgroundPanel:getChildById('enterButton')
	m_ConsoleList.newWindowButton = m_ConsoleList.backgroundPanel:getChildById('newWindowButton')
	m_ConsoleList.dragWindowButton = m_ConsoleList.backgroundPanel:getChildById('dragWindowButton')
	m_ConsoleList.clearWindowButton = m_ConsoleList.backgroundPanel:getChildById('clearWindowButton')
	m_ConsoleList.nextChannelButton = m_ConsoleList.backgroundPanel:getChildById('nextChannelButton')
	m_ConsoleList.prevChannelButton = m_ConsoleList.backgroundPanel:getChildById('prevChannelButton')
	m_ConsoleList.leafs = m_ConsoleList.backgroundPanel:getChildById('leafs')
	m_ConsoleList.categoryTabBackground = m_ConsoleList.backgroundPanel:getChildById('categoryTabBackground')
	m_ConsoleList.expandButton = m_ConsoleList.backgroundPanel:getChildById('expandButton')
	m_ConsoleList.rightResizeBorder = m_ConsoleList.backgroundPanel:getChildById('rightResizeBorder')
	m_ConsoleList.bottomResizeBorder = m_ConsoleList.backgroundPanel:getChildById('bottomResizeBorder')
	m_ConsoleList.helpButton = m_ConsoleList.backgroundPanel:getChildById('helpButton')
	m_ConsoleList.helpButton.onHoverChange = m_ConsoleFunctions.hover
	
	m_ConsoleList.consoleContentPanel = m_ConsoleList.backgroundPanel:getChildById('consoleContentPanel')
	m_ConsoleList.consoleTabBar = m_ConsoleList.backgroundPanel:getChildById('consoleTabBar')
	m_ConsoleList.consoleTabBar:setContentWidget(m_ConsoleList.consoleContentPanel)
	
	m_ConsoleList.categoryTabBar = m_ConsoleList.backgroundPanel:getChildById('categoryTabBar')
	m_ConsoleList.categoryTabBar:setContentWidget(m_ConsoleList.consoleContentPanel)
	m_ConsoleList.lockBoxEnabled = toboolean(g_settings.get('consoleLock', true))
	
	m_ConsoleList.consoleTabBar:setNavigation(m_ConsoleList.backgroundPanel:getChildById('prevChannelButton'), m_ConsoleList.backgroundPanel:getChildById('nextChannelButton'))
	
	m_ConsoleList.currentMessageIndex = 0
	m_ConsoleList.backgroundPanel.onKeyPress = function(self, keyCode, keyboardModifiers)
		if not (keyboardModifiers == KeyboardCtrlModifier and keyCode == KeyC) then
			return false
		end
		
		local tab = m_ConsoleFunctions.getCurrentTab()
		if not tab then
			return false
		end
		
		local consoleBuffer = tab.tabPanel:getChildById('consoleBuffer')
		if not consoleBuffer then
			return false
		end
		
		local consoleLabel = consoleBuffer:getFocusedChild()
		if not consoleLabel or not consoleLabel:hasSelection() then
			return false
		end
		
		g_window.setClipboardText(consoleLabel:getSelection())
		return true
	end

	g_keyboard.bindKeyPress('Ctrl+G', function() m_ConsoleFunctions.expandChat(false) end)
	g_keyboard.bindKeyPress('Ctrl+O', function() m_ConsoleFunctions.openNewWindowTab() end)
	g_keyboard.bindKeyPress('Ctrl+A', function() m_ConsoleList.consoleTextEdit:clearText() end)
	g_keyboard.bindKeyPress('Shift+Up', function() m_ConsoleFunctions.navigateMessageHistory(1) end)
	g_keyboard.bindKeyPress('Shift+Down', function() m_ConsoleFunctions.navigateMessageHistory(-1) end)
	g_keyboard.bindKeyPress('Tab', function() m_ConsoleFunctions.selectNextTab() end)
	g_keyboard.bindKeyPress('Shift+Tab', function() m_ConsoleFunctions.selectPrevTab() end)
	g_keyboard.bindKeyPress('Ctrl+Tab', function() m_ConsoleFunctions.selectNextPanel() end)
	g_keyboard.bindKeyPress('Ctrl+Shift+Tab', function() m_ConsoleFunctions.selectPrevPanel() end)
	g_keyboard.bindKeyDown('Enter', sendCurrentMessage)
	g_keyboard.bindKeyDown('Ctrl+E', removeCurrentTab)
	
	m_ConsoleFunctions.load()

	if g_game.isOnline() then
		online()
	end
end

m_ConsoleFunctions.addHelpLabel = function(label, parent, color)
	local widget = g_ui.createWidget('ConsoleHoverLabel', parent)
	widget:setText(label)
	widget:setParent(parent)
	if color then
		widget:setColor(color)
	end
	return widget:getHeight()
end

m_ConsoleFunctions.hoverCreate = function(self)
	if m_ConsoleList.hover then
		return false
	end
	
	m_ConsoleList.hover = g_ui.createWidget('ConsoleHoverWindow', modules.game_interface.getRootPanel())
	
	local height = 12
	local list = m_ConsoleList.hover:getChildById('list')
	local list2 = m_ConsoleList.hover:getChildById('list2')
	for i = 1, #m_ConsoleFunctions.helpDescription do
		local title, text = m_ConsoleFunctions.helpDescription[i][1], m_ConsoleFunctions.helpDescription[i][2]
		height = height + m_ConsoleFunctions.addHelpLabel(title .. ': ' .. text, list) + 2
		m_ConsoleFunctions.addHelpLabel(title, list2, '#00AAFF')
	end
	
	m_ConsoleList.hover:setHeight(height)
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 4
	pos.y = pos.y + self:getHeight() - 4
	
	m_ConsoleList.hover:setPosition(pos)
end

m_ConsoleFunctions.hoverDestroy = function()
	if m_ConsoleList.hover then
		m_ConsoleList.hover:getChildById('list'):destroyChildren()
		
		m_ConsoleList.hover:destroy()
		m_ConsoleList.hover = nil
	end
end

m_ConsoleFunctions.hover = function(self, hovered)
	if hovered then
		m_ConsoleFunctions.hoverCreate(self)
	else
		m_ConsoleFunctions.hoverDestroy()
	end
end

m_ConsoleFunctions.forceLock = function(self, id)
	self:setOn(id)
	
	if not id then
		m_ConsoleFunctions.lockId = 1
		m_ConsoleFunctions.disableChat(0)
	else
		m_ConsoleFunctions.lockId = 0
		m_ConsoleFunctions.enableChat()
	end
end

m_ConsoleFunctions.lock = function(self)
	m_ConsoleList.lockBoxEnabled = not self:isOn()
	self:setOn(m_ConsoleList.lockBoxEnabled)
	
	if not m_ConsoleList.lockBoxEnabled then
		m_ConsoleFunctions.lockId = 1
		m_ConsoleFunctions.disableChat()
	else
		m_ConsoleFunctions.lockId = 0
		m_ConsoleFunctions.enableChat()
	end
end

m_ConsoleFunctions.onGeometryChange = function(self)
	local height = self:getHeight()
	if height <= 204 then
		self:setHeight(204)
	end
	
	local width = self:getWidth()
	if width ~= 42 and width <= 507 then
		self:setWidth(507)
	end
end

m_ConsoleFunctions.selectPrevPanel = function()
	if m_ConsoleList.expandButton:isOn() then
		return true
	end
	
	local tab = m_ConsoleList.consoleTabBar:getCurrentTab()
	if tab then
		tab.tabBar:unselectTab(tab)
		m_ConsoleList.categoryTabBar:updateSelect()
	else
		m_ConsoleList.categoryTabBar:selectPrevTab()
	end
	
	local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
	local id = nil
	if tab then
		id = tonumber(tab:getId())
	end
	
	if not id or id == 8 or id == 9 then
		m_ConsoleFunctions.enableChat()
	else
		m_ConsoleFunctions.disableChat()
	end
end

m_ConsoleFunctions.selectNextPanel = function()
	if m_ConsoleList.expandButton:isOn() then
		return true
	end
	
	local tab = m_ConsoleList.consoleTabBar:getCurrentTab()
	if tab then
		tab.tabBar:unselectTab(tab)
		m_ConsoleList.categoryTabBar:updateSelect()
	else
		m_ConsoleList.categoryTabBar:selectNextTab()
	end
	
	local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
	local id = nil
	if tab then
		id = tonumber(tab:getId())
	end
	
	if not id or id == 8 or id == 9 then
		m_ConsoleFunctions.enableChat()
	else
		m_ConsoleFunctions.disableChat()
	end
end

m_ConsoleFunctions.selectPrevTab = function()
	if m_ConsoleList.expandButton:isOn() then
		return true
	end
	
	local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
	if tab then
		tab.tabBar:unselectTab(tab)
		m_ConsoleList.consoleTabBar:updateSelect()
	else
		m_ConsoleList.consoleTabBar:selectPrevTab()
	end
	
	m_ConsoleFunctions.enableChat()
end

m_ConsoleFunctions.selectNextTab = function()
	if m_ConsoleList.expandButton:isOn() then
		return true
	end
	
	local tab = m_ConsoleList.categoryTabBar:getCurrentTab()
	if tab then
		tab.tabBar:unselectTab(tab)
		m_ConsoleList.consoleTabBar:updateSelect()
	else
		m_ConsoleList.consoleTabBar:selectNextTab()
	end
	
	m_ConsoleFunctions.enableChat()
end

m_ConsoleFunctions.doChannelListSubmit = function()
	local channelListPanel = m_ConsoleList.channelsWindow:getChildById('channelList')
	local openPrivateChannelWith = m_ConsoleList.channelsWindow:getChildById('openPrivateChannelWith'):getText()
	if openPrivateChannelWith ~= '' then
		if openPrivateChannelWith:lower() ~= g_game.getCharacterName():lower() then
			g_game.openPrivateChannel(openPrivateChannelWith)
		else
			modules.game_textmessage.displayFailureMessage('You cannot create a private chat channel with yourself.')
		end
	else
		local selectedChannelLabel = channelListPanel:getFocusedChild()
		if not selectedChannelLabel then
			return true
		end
		
		if selectedChannelLabel.channelId == 0xFFFF then
			g_game.openOwnChannel()
		else
			g_game.joinChannel(selectedChannelLabel.channelId)
		end
	end
	
	m_ConsoleList.channelsWindow:destroy()
	m_ConsoleList.channelsWindow = nil
end

m_ConsoleFunctions.openNewWindowTab = function()
	g_game.requestChannels()
end

function terminate()
	m_ConsoleFunctions.save()
	disconnect(g_game, {
		onTalk = onTalk,
		onOpenChannel = onOpenChannel,
		onChannelList = onChannelList,
		onOpenPrivateChannel = onOpenPrivateChannel,
		onOpenOwnPrivateChannel = onOpenOwnPrivateChannel,
		onCloseChannel = onCloseChannel,
		onGameStart = online,
		onGameEnd = offline,
		onLootLink = onLootLink,
		onAddMessageTypes = onAddMessageTypes
	})

	if g_game.isOnline() then
		m_ConsoleFunctions.clear()
	end

	g_keyboard.unbindKeyPress('Ctrl+G')
	g_keyboard.unbindKeyPress('Ctrl+A')
	g_keyboard.unbindKeyPress('Shift+Up')
	g_keyboard.unbindKeyPress('Shift+Down')
	g_keyboard.unbindKeyPress('Tab')
	g_keyboard.unbindKeyPress('Shift+Tab')
	g_keyboard.unbindKeyDown('Enter')
	g_keyboard.unbindKeyDown('Ctrl+E')
	
	saveCommunicationSettings()
	
	if m_ConsoleList.channelsWindow then
		m_ConsoleList.channelsWindow:destroy()
	end
	
	if m_ConsoleList.communicationWindow then
		m_ConsoleList.communicationWindow:destroy()
	end
	
	if m_ConsoleList.channelsWindow then
		m_ConsoleList.channelsWindow:destroy()
	end
	
	m_ConsoleList.backgroundPanel:destroy()
	m_ConsoleList = {}
end

function onChannelList(channelList)
	if m_ConsoleList.channelsWindow then
		m_ConsoleList.channelsWindow:destroy()
		m_ConsoleList.channelsWindow = nil
	end
	
	m_ConsoleList.channelsWindow = g_ui.displayUI('channelswindow')
	m_ConsoleList.channelsWindow.onEnter = m_ConsoleFunctions.doChannelListSubmit
	m_ConsoleList.channelsWindow.onDestroy = function() m_ConsoleList.channelsWindow = nil end
	
	local list = m_ConsoleList.channelsWindow:getChildById('channelList')
	for k, v in pairs(channelList) do
		local channelId = v[1]
		local channelName = v[2]
		
		if #channelName > 0 and channelId < 8 then
			local label = g_ui.createWidget('ChannelListLabel', list)
			label.channelId = channelId
			label:setText(channelName)
			
			label:setPhantom(false)
			label.onDoubleClick = m_ConsoleFunctions.doChannelListSubmit
		end
	end
	
	local openPrivateChannelWith = m_ConsoleList.channelsWindow:getChildById('openPrivateChannelWith')
	m_ConsoleFunctions.enableChat()
	openPrivateChannelWith:focus()
end

function onOpenChannel(channelId, channelName)
	m_ConsoleFunctions.addChannel(channelName, channelId)
end

m_ConsoleFunctions.addChannel = function(name, id)
	if id >= 8 then
		return true
	end
	
	m_ConsoleFunctions.channels[id] = name
	local tab = m_ConsoleFunctions.addTab(name, true, m_ConsoleList.consoleTabBar, m_ConsoleFunctions.processChannelTabMenu)
	tab.channelId = id
	return tab
end

function online()
	m_ConsoleFunctions.forceLock(m_ConsoleList.backgroundPanel:getChildById('lock'), m_ConsoleList.lockBoxEnabled)
	
	m_ConsoleList.defaultTab = m_ConsoleFunctions.addTab(tr('Default'), true, m_ConsoleList.consoleTabBar, m_ConsoleFunctions.processChannelTabMenu)
	m_ConsoleList.serverTab = m_ConsoleFunctions.addTab(tr('Server Log'), false, m_ConsoleList.consoleTabBar, m_ConsoleFunctions.processChannelTabMenu)
	
	m_ConsoleList.defaultTab.onClick = m_ConsoleFunctions.unselectCategoryChannel
	m_ConsoleList.serverTab.onClick = m_ConsoleFunctions.unselectCategoryChannel
	
	m_ConsoleList.npcTab = m_ConsoleFunctions.addTab('12', false, m_ConsoleList.categoryTabBar, nil, '1761 512 27 27', 'NPC conversations')
	m_ConsoleList.tradeTab = m_ConsoleFunctions.addTab('8', false, m_ConsoleList.categoryTabBar, nil, '1761 454 27 27', 'Trade')
	m_ConsoleList.helpTab = m_ConsoleFunctions.addTab('9', false, m_ConsoleList.categoryTabBar, nil, '1761 482 27 27', 'Help')
	m_ConsoleList.lootTab = m_ConsoleFunctions.addTab('10', false, m_ConsoleList.categoryTabBar, nil, '1761 539 27 27', 'Basic Information Log')
	m_ConsoleList.damageTab = m_ConsoleFunctions.addTab('11', false, m_ConsoleList.categoryTabBar, nil, '1761 563 27 27', 'Damage Log')
	
	m_ConsoleList.tradeTab.onClick = m_ConsoleFunctions.unselectConsoleChannel
	m_ConsoleList.helpTab.onClick = m_ConsoleFunctions.unselectConsoleChannel
	m_ConsoleList.npcTab.onClick = m_ConsoleFunctions.unselectConsoleChannel
	m_ConsoleList.lootTab.onClick = m_ConsoleFunctions.unselectConsoleChannel
	m_ConsoleList.damageTab.onClick = m_ConsoleFunctions.unselectConsoleChannel
	
	for k, v in pairs(m_ConsoleFunctions.channels) do
		if tonumber(k) then
			g_game.joinChannel(k)
		end
	end
end

function onLootLink(id, name)
	m_ConsoleList.backgroundPanel:focus()
	m_ConsoleList.consoleTextEdit:setText(name)
	m_ConsoleList.consoleTextEdit:setColor('#1E90FF')
	
	m_ConsoleFunctions.linkItem = name
	m_ConsoleFunctions.linkId = id
end

function sendCurrentMessage()
	if not m_ConsoleList.lockBoxEnabled and m_ConsoleFunctions.lockId == 1 then
		m_ConsoleFunctions.lockId = 0
		m_ConsoleFunctions.enableChat()
		m_ConsoleFunctions.lockId = 2
	end
	
	local message = m_ConsoleList.consoleTextEdit:getText()
	if #message == 0 then
		return true
	end
	
	if not m_ConsoleList.consoleTextEdit:isEnabled() then
		return true
	end
	
	m_ConsoleFunctions.sendMessage(message)
	m_ConsoleFunctions.clearLinkItem()
	m_ConsoleList.consoleTextEdit:clearText()
	
	if not m_ConsoleList.lockBoxEnabled and m_ConsoleFunctions.lockId == 2 then
		m_ConsoleFunctions.disableChat()
		m_ConsoleFunctions.lockId = 1
	end
end

function onTalk(name, groupId, level, mode, message, channelId, creaturePos, isNpc, newLinkId)
	if mode == MessageModes.GamemasterBroadcast then
		modules.game_textmessage.displayBroadcastMessage(name .. ': ' .. message)
		return true
	end

	speaktype = m_ConsoleFunctions.SpeakTypes[mode]
	if not speaktype then
		perror('unhandled onTalk message mode ' .. mode .. ': ' .. message)
		return true
	end
	
	local localPlayer = g_game.getLocalPlayer()
	if name ~= g_game.getCharacterName() and isUsingIgnoreList() and not isUsingWhiteList() or (isUsingWhiteList() and not(isWhitelisted(name)) and not(isAllowingVIPs() and localPlayer:hasVip(name))) then

		if mode == MessageModes.Yell and isIgnoringYelling() then
			return true
		elseif speaktype.private and isIgnoringPrivate() and mode ~= MessageModes.NpcFrom then
			return true
		elseif isIgnored(name) then
			return true
		end
	end

	if (mode == MessageModes.Say or mode == MessageModes.Whisper or mode == MessageModes.Yell or
		mode == MessageModes.Spell or mode == MessageModes.MonsterSay or mode == MessageModes.MonsterYell or
		mode == MessageModes.NpcFrom or mode == MessageModes.BarkLow or mode == MessageModes.BarkLoud) and
		creaturePos and newLinkId == 0 then

		local staticText = StaticText.create()
		staticText:addMessage(name, mode, message)
		g_map.addThing(staticText, creaturePos, -1)
	end
	
	if isNpc and not modules.client_options.getOption('showNPCMessagesInLog') then
		return true
	end
	
	local defaultMessage = mode <= 3 and true or false
	if speaktype == m_ConsoleFunctions.SpeakTypesSettings.none then
		return true
	end
	
	if speaktype.hideInConsole then
		return true
	end
	
	m_ConsoleFunctions.linkId = newLinkId
	local composedMessage = m_ConsoleFunctions.applyMessagePrefixies(name, level, message)
	if mode == MessageModes.RVRContinue then
		m_ConsoleFunctions.addText(composedMessage, groupId, speaktype, name .. '\'...', name, nil, message, isNpc)
	elseif speaktype.private then
		m_ConsoleFunctions.addPrivateText(composedMessage, speaktype, name, false, name)
		if modules.client_options.getOption('showPrivateMessagesOnScreen') and speaktype ~= m_ConsoleFunctions.SpeakTypesSettings.privateNpcToPlayer then
			modules.game_textmessage.displayPrivateMessage(name .. ':\n' .. message)
		end	
	else
		local channel = tr('Default')
		if not defaultMessage then
			channel = m_ConsoleFunctions.channels[channelId]
		end
		
		if channel then
			m_ConsoleFunctions.addText(composedMessage, groupId, speaktype, channel, name, nil, false, message, isNpc, mode == MessageModes.Say)
		end
	end
	
	m_ConsoleFunctions.linkId = 0
end

function loadCommunicationSettings()
	m_ConsoleFunctions.communicationSettings.whitelistedPlayers = {}
	m_ConsoleFunctions.communicationSettings.ignoredPlayers = {}

	local ignoreNode = g_settings.getNode('IgnorePlayers')
	if ignoreNode then
		for i = 1, #ignoreNode do
			table.insert(m_ConsoleFunctions.communicationSettings.ignoredPlayers, ignoreNode[i])
		end
	end

	local whitelistNode = g_settings.getNode('WhitelistedPlayers')
	if whitelistNode then
		for i = 1, #whitelistNode do
			table.insert(m_ConsoleFunctions.communicationSettings.whitelistedPlayers, whitelistNode[i])
		end
	end

	m_ConsoleFunctions.communicationSettings.useIgnoreList = g_settings.getBoolean('UseIgnoreList')
	m_ConsoleFunctions.communicationSettings.useWhiteList = g_settings.getBoolean('UseWhiteList')
	m_ConsoleFunctions.communicationSettings.privateMessages = g_settings.getBoolean('IgnorePrivateMessages')
	m_ConsoleFunctions.communicationSettings.yelling = g_settings.getBoolean('IgnoreYelling')
	m_ConsoleFunctions.communicationSettings.allowVIPs = g_settings.getBoolean('AllowVIPs')
end

function canUseWASD()
	return not m_ConsoleList.consoleTextEdit:isEnabled()
end

function saveCommunicationSettings()
	local tmpIgnoreList = {}
	local ignoredPlayers = getIgnoredPlayers()
	for i = 1, #ignoredPlayers do
		table.insert(tmpIgnoreList, ignoredPlayers[i])
	end

	local tmpWhiteList = {}
	local whitelistedPlayers = getWhitelistedPlayers()
	for i = 1, #whitelistedPlayers do
		table.insert(tmpWhiteList, whitelistedPlayers[i])
	end

	g_settings.set('UseIgnoreList', m_ConsoleFunctions.communicationSettings.useIgnoreList)
	g_settings.set('UseWhiteList', m_ConsoleFunctions.communicationSettings.useWhiteList)
	g_settings.set('IgnorePrivateMessages', m_ConsoleFunctions.communicationSettings.privateMessages)
	g_settings.set('IgnoreYelling', m_ConsoleFunctions.communicationSettings.yelling)
	g_settings.setNode('IgnorePlayers', tmpIgnoreList)
	g_settings.setNode('WhitelistedPlayers', tmpWhiteList)
end

function getIgnoredPlayers()
	return m_ConsoleFunctions.communicationSettings.ignoredPlayers
end

function getWhitelistedPlayers()
	return m_ConsoleFunctions.communicationSettings.whitelistedPlayers
end

function isUsingIgnoreList()
	return m_ConsoleFunctions.communicationSettings.useIgnoreList
end

function isUsingWhiteList()
	return m_ConsoleFunctions.communicationSettings.useWhiteList
end

function isIgnored(name)
	return table.find(m_ConsoleFunctions.communicationSettings.ignoredPlayers, name, true)
end

function addIgnoredPlayer(name)
	if isIgnored(name) then
		return true
	end
	
	table.insert(m_ConsoleFunctions.communicationSettings.ignoredPlayers, name)
end

function removeIgnoredPlayer(name)
	table.removevalue(m_ConsoleFunctions.communicationSettings.ignoredPlayers, name)
end

function isWhitelisted(name)
	return table.find(m_ConsoleFunctions.communicationSettings.whitelistedPlayers, name, true)
end

function addWhitelistedPlayer(name)
	if isWhitelisted(name) then
		return true
	end
	
	table.insert(m_ConsoleFunctions.communicationSettings.whitelistedPlayers, name)
end

function removeWhitelistedPlayer(name)
	table.removevalue(m_ConsoleFunctions.communicationSettings.whitelistedPlayers, name)
end

function isIgnoringPrivate()
	return m_ConsoleFunctions.communicationSettings.privateMessages
end

function isIgnoringYelling()
	return m_ConsoleFunctions.communicationSettings.yelling
end

function isAllowingVIPs()
	return m_ConsoleFunctions.communicationSettings.allowVIPs
end

function offline()
	g_settings.set('consoleLock', m_ConsoleList.lockBoxEnabled)
	m_ConsoleFunctions.clearNPCTab()
	m_ConsoleFunctions.clear()
end

function onCloseChannel(channelId)
	local channel = m_ConsoleFunctions.channels[channelId]
	if channel then
		local tab = getTab(channel)
		if tab then
			m_ConsoleList.consoleTabBar:removeTab(tab)
		end
		
		for k, v in pairs(m_ConsoleFunctions.channels) do
			if k == tab.channelId then
				m_ConsoleFunctions.channels[k] = nil
			end
		end
	end
end

function onOpenPrivateChannel(receiver)
	m_ConsoleFunctions.addPrivateChannel(receiver)
end

function onOpenOwnPrivateChannel(channelId, channelName)
	local privateTab = m_ConsoleFunctions.getTab(channelName, m_ConsoleList.consoleTabBar)
	if privateTab == nil then
		m_ConsoleFunctions.addChannel(channelName, channelId)
	end
	
	m_ConsoleList.ownPrivateName = channelName
end

function removeCurrentTab()
	m_ConsoleFunctions.removeTab(m_ConsoleFunctions.getCurrentTab())
end

function onAddMessageTypes(name, message, var, duration)
	if var == m_ConsoleFunctions.NPCMessageBlack then
		modules.client_destroy.m_DestroyFunctions.onBlink(m_ConsoleFunctions.BLINK_BLACK_SCREEN, duration)
		return true
	end
	
	local tab = m_ConsoleList.consoleTabBar:getCurrentTab()
	if tab then
		m_ConsoleList.consoleTabBar:unselectTab(tab)
	end
	
	local tab = m_ConsoleFunctions.getTab('12', m_ConsoleList.categoryTabBar)
	if tab then
		local panel = m_ConsoleList.consoleTabBar:getTabPanel(tab)
		if not panel then
			return true
		end
		
		local consoleBuffer = panel:getChildById('consoleBuffer')
		local data = {}
		data.name = name
		data.message = message
		data.type = var
		data.time = duration
		m_ConsoleFunctions.clearNPCTab(consoleBuffer)
		
		m_ConsoleFunctions.unselectConsoleChannel(tab, true)
		m_ConsoleFunctions.createNewMessage(consoleBuffer, data)
		m_ConsoleFunctions.disableChat()
	end
end