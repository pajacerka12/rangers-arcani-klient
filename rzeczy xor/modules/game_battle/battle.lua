m_BattleFunctions = {}
m_BattleList = {}

m_BattleFunctions.lifeBarColors = {}
table.insert(m_BattleFunctions.lifeBarColors, {percentAbove = 92, 	color = '#00BC00' } )
table.insert(m_BattleFunctions.lifeBarColors, {percentAbove = 60, 	color = '#50A150' } )
table.insert(m_BattleFunctions.lifeBarColors, {percentAbove = 30, 	color = '#A1A100' } )
table.insert(m_BattleFunctions.lifeBarColors, {percentAbove = 8, 	color = '#BF0A0A' } )
table.insert(m_BattleFunctions.lifeBarColors, {percentAbove = 3, 	color = '#910F0F' } )
table.insert(m_BattleFunctions.lifeBarColors, {percentAbove = -1, 	color = '#850C0C' } )

m_BattleFunctions.creatureButtonColors = {
	onIdle = 	 {notHovered = '#846F60', hovered = '#FFFFFF', npchovered = '#277EFF', npcnotHovered = '#0066FF'},
	onTargeted = {notHovered = '#FF0000', hovered = '#FF9D9D'},
	onFollowed = {notHovered = '#00FF00', hovered = '#9DFF9D', npchovered = '#00FF00', npcnotHovered = '#9DFF9D'}
}

m_BattleFunctions.updateSkull = function(widget, skullId)
	local list = widget:getChildById('skull')
	if skullId ~= SkullNone then
		list:setImageSource(getSkullImagePath(skullId))
	end
end

m_BattleFunctions.getIconId = function(id)
	if id == CONDITION_POISON then
		return 57
	elseif id == CONDITION_FIRE then
		return 32
	elseif id == CONDITION_ENERGY then
		return 31
	elseif id == CONDITION_PHYSICAL then
		return 19
	elseif id == CONDITION_HASTE then
		return 51
	elseif id == CONDITION_PARALYZE then
		return 49
	elseif id == CONDITION_DRUNK then
		return 50
	elseif id == CONDITION_SOUL then
		return 56
	elseif id == CONDITION_DROWN then
		return 23
	elseif id == CONDITION_FREEZING then
		return 33
	elseif id == CONDITION_DAZZLED then
		return 36
	elseif id == CONDITION_CURSED then
		return 35
	elseif id == CONDITION_BLEED then
		return 37
	elseif id == CONDITION_WIND then
		return 52
	elseif id == CONDITION_TOXIC then
		return 48
	elseif id == CONDITION_STUN then
		return 16
	elseif id == CONDITION_FEAR then
		return 53
	end
	
	return 0
end

m_BattleFunctions.updateIcon = function(widget, id, destroy)
	local list = widget:getChildById('iconPanel')
	local tmpWidget = list:getChildById(id)
	if destroy then
		if tmpWidget then
			tmpWidget:destroy()
		end
		list:setWidth(list:getChildCount() * 19)
	elseif not tmpWidget then
		tmpWidget = g_ui.createWidget('IconWidget', list)
		tmpWidget:setId(id)
		modules.game_lookat.m_LookAtFunctions.setIconImageType(tmpWidget, m_BattleFunctions.getIconId(id))
		list:setWidth(list:getChildCount() * 19)
	end
end

m_BattleFunctions.checkCreatures = function()
	m_BattleFunctions.removeAllCreatures()
	
	local player = g_game.getLocalPlayer()
	local spectators = g_map.getSpectators(player:getPosition(), false)
	for _, creature in ipairs(spectators) do
		m_BattleFunctions.addCreature(creature)
	end
end

m_BattleFunctions.updateBattleButton = function(widget)
	m_BattleFunctions.update(widget)
	
	if widget.isTarget or widget.isFollowed then
		if m_BattleList.lastBattleButtonSwitched and m_BattleList.lastBattleButtonSwitched ~= widget then
			m_BattleList.lastBattleButtonSwitched.isTarget = false
			m_BattleList.lastBattleButtonSwitched.isFollowed = false
			m_BattleFunctions.updateBattleButton(m_BattleList.lastBattleButtonSwitched)
		end
		
		m_BattleList.lastBattleButtonSwitched = widget
	end
end

m_BattleFunctions.setLifeBarPercent = function(percent, widget)
	widget:setPercent(percent)
	for i, v in pairs(m_BattleFunctions.lifeBarColors) do
		if percent > v.percentAbove then
			widget:setBackgroundColor(v.color)
			break
		end
	end
end

m_BattleFunctions.updateColor = function(color)
	m_BattleFunctions.updateStaticSquare()
end

m_BattleFunctions.update = function(self)
	if not self.creature or (self.creature:isNpc() and self.onTargeted) then
		return false
	end
	
	local color = m_BattleFunctions.creatureButtonColors.onIdle
	if self.isTarget then
		color = m_BattleFunctions.creatureButtonColors.onTargeted
		color.notHovered = modules.client_options.m_OptionsFunctions.getCircleColor()
		self:setOn(true)
	elseif self.isFollowed then
		color = m_BattleFunctions.creatureButtonColors.onFollowed
		self:setOn(true)
	else
		self:setOn(false)
	end
	
	if self.creature:isNpc() then
		color = self.isHovered and color.npchovered or color.npcnotHovered
	else
		color = self.isHovered and color.hovered or color.notHovered
	end
	
	local creature = self:getChildById('creature')
	if self.isHovered or self.isTarget or self.isFollowed then
		self.creature:showStaticSquare(color)
		
		creature:setOn(true)
		creature:setImageColor(color)
		self:getChildById('label'):setColor(color)
	else
		self.creature:hideStaticSquare()
		
		creature:setOn(false)
		creature:setImageColor(color.notHovered)
		self:getChildById('label'):setColor(color)
	end
end

m_BattleFunctions.updateAttackSquare = function(creature, update)
	if update then
		creature:showStaticSquare(modules.client_options.m_OptionsFunctions.getCircleColor())
	else
		creature:hideStaticSquare()
	end
end

m_BattleFunctions.updateFollowSquare = function(creature, update)
	if update then
		creature:showStaticSquare(m_BattleFunctions.creatureButtonColors.onFollowed.notHovered)
	else
		creature:hideStaticSquare()
	end
end

m_BattleFunctions.updateStaticSquare = function()
	for _, pid in pairs(m_BattleList.battlePanel:getChildren()) do
		if pid.isTarget then
			m_BattleFunctions.update(pid)
			break
		end
	end
end

m_BattleFunctions.removeAllCreatures = function()
	if m_BattleList.lastBattleButtonSwitched then
		m_BattleList.lastBattleButtonSwitched = nil
	end
	
	m_BattleList.battlePanel:destroyChildren()
end

m_BattleFunctions.removeCreature = function(creature)
	local widget = m_BattleList.battlePanel:getChildById(creature:getId())
	if widget then
		if m_BattleList.lastBattleButtonSwitched == widget then
			m_BattleList.lastBattleButtonSwitched.creature:hideStaticSquare()
			m_BattleList.lastBattleButtonSwitched = nil
		end
	
		widget:destroy()
	end
end

m_BattleFunctions.addCreature = function(creature)
	if not creature or not creature:canBeSeen() or creature:isKnown() or not m_BattleFunctions.doCreatureFitFilters(creature) then
		return false
	end
	
	local creatureId = creature:getId()
	local widget = m_BattleList.battlePanel:getChildById(creatureId)
	if not widget then
		widget = g_ui.createWidget('BattleButton', m_BattleList.battlePanel)
		widget:setParent(m_BattleList.battlePanel)
		widget:setId(creatureId)
		widget.creature = creature
		
		local creatureWidget = widget:getChildById('creature')
		local labelWidget = widget:getChildById('label')
		local healthBar = widget:getChildById('healthBar')
		local healthBackground = widget:getChildById('background')
		
		labelWidget:setText(creature:getName())
		labelWidget:setOn(creature:isNpc())
		labelWidget:setChecked(creature:isHideHealth())
		creatureWidget:setCreature(creature)
		
		m_BattleFunctions.updateSkull(widget, creature:getSkull())
		
		if creature:isNpc() or creature:isHideHealth() then
			healthBar:hide()
			healthBackground:hide()
		else
			healthBar:show()
			healthBackground:show()
		end
		
		widget.isHovered = false
		widget.isTarget = false
		widget.isFollowed = false
		
		if creature == g_game.getAttackingCreature() then
			widget.isTarget = true
			onAttack(creature)
		end

		if creature == g_game.getFollowingCreature() then
			widget.isFollowed = true
			onFollow(creature)
		end
		
		if not creature:isHideHealth() and not creature:isNpc() then
			m_BattleFunctions.setLifeBarPercent(creature:getHealthPercent(), healthBar)
		end
		
		widget.onHoverChange = onBattleButtonHoverChange
		widget.onMouseRelease = onBattleButtonMouseRelease
		m_BattleFunctions.update(widget)
	end
end

m_BattleFunctions.doCreatureFitFilters = function(creature)
	if creature:isLocalPlayer() or not creature:canBeSeen() then
		return false
	end

	local pos = creature:getPosition()
	if not pos then
		return false
	end

	local localPlayer = g_game.getLocalPlayer()
	if pos.z ~= localPlayer:getPosition().z then
		return false
	end
	
	if m_BattleList.hidePlayersButton:isChecked() and creature:isPlayer() then
		return false
	elseif m_BattleList.hideNPCsButton:isChecked() and creature:isNpc() then
		return false
	elseif m_BattleList.hideMonstersButton:isChecked() and creature:isMonster() then
		return false
	elseif m_BattleList.hideSkullsButton:isChecked() and creature:isPlayer() and creature:getSkull() == SkullNone then
		return false
	elseif m_BattleList.hidePartyButton:isChecked() and creature:getShield() > ShieldWhiteBlue then
		return false
	end

	return true
end

m_BattleFunctions.isHidingFilters = function()
	local settings = g_settings.getNode('BattleList')
	if not settings then
		return false
	end
	
	return settings['hidingFilters']
end

m_BattleFunctions.setHidingFilters = function(state)
	settings = {}
	settings['hidingFilters'] = state
	g_settings.mergeNode('BattleList', settings)
end

m_BattleFunctions.showFilterPanel = function()
	m_BattleList.filterPanel:setHeight(m_BattleList.filterPanel.originalHeight)
	
	m_BattleList.toggleFilterButton:getParent():setOn(false)
	m_BattleList.toggleFilterButton:setOn(false)
	
	m_BattleFunctions.setHidingFilters(false)
	m_BattleList.filterPanel:show()
end

m_BattleFunctions.hideFilterPanel = function()
	m_BattleList.filterPanel.originalHeight = m_BattleList.filterPanel:getHeight()
	m_BattleList.filterPanel:setHeight(0)
	
	m_BattleList.toggleFilterButton:getParent():setOn(true)
	m_BattleList.toggleFilterButton:setOn(true)
	
	m_BattleFunctions.setHidingFilters(true)
	m_BattleList.filterPanel:hide()
end

m_BattleFunctions.toggleFilterPanel = function()
	if m_BattleList.filterPanel:isVisible() then
		m_BattleFunctions.hideFilterPanel()
	else
		m_BattleFunctions.showFilterPanel()
	end
end

m_BattleFunctions.destroy = function()
	m_BattleFunctions.attackCreature = nil
	
	if m_BattleList.window then
		m_BattleList.window:destroy()
		m_BattleList = {}
	end
end

function onLoad()
	connect(Creature, {
		onSkullChange = updateCreatureSkull,
		onHealthPercentChange = onCreatureHealthPercentChange,
		onAppear = onCreatureAppear,
		onDisappear = onCreatureDisappear,
		onIconChange = onIconChange
	})
	
	connect(LocalPlayer, {
		onPositionChange = onCreaturePositionChange
	})
	
	connect(g_game, {
		onAttackingCreatureChange = onAttack,
		onFollowingCreatureChange = onFollow,
		onUpdateTarget = onUpdateTarget,
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
	})
	
	g_ui.importStyle('battlebutton')
	
	m_BattleList.window = g_ui.loadUI('battle', modules.game_interface.getLeftPanel())
	m_BattleList.window:setContentMinimumHeight(70)
	m_BattleList.window:setup()
	
	m_BattleList.battlePanel = m_BattleList.window:recursiveGetChildById('battlePanel')
	m_BattleList.filterPanel = m_BattleList.window:recursiveGetChildById('filterPanel')
	m_BattleList.toggleFilterButton = m_BattleList.window:recursiveGetChildById('toggleFilterButton')
	
	m_BattleList.hidePlayersButton = m_BattleList.window:recursiveGetChildById('hidePlayers')
	m_BattleList.hideNPCsButton = m_BattleList.window:recursiveGetChildById('hideNPCs')
	m_BattleList.hideMonstersButton = m_BattleList.window:recursiveGetChildById('hideMonsters')
	m_BattleList.hideSkullsButton = m_BattleList.window:recursiveGetChildById('hideSkulls')
	m_BattleList.hidePartyButton = m_BattleList.window:recursiveGetChildById('hideParty')
	
	if m_BattleFunctions.isHidingFilters() then
		m_BattleFunctions.hideFilterPanel()
	end
	
	if g_game.isOnline() then
		m_BattleFunctions.checkCreatures()
		onGameStart()
	end	
	
	g_keyboard.bindKeyDown('Ctrl+B', toggle)
	
	local scrollbar = m_BattleList.window:getChildById('miniwindowScrollBar')
	scrollbar:mergeStyle({ ['$!on'] = { }})
end

function onUnload()
	disconnect(Creature, {
		onSkullChange = updateCreatureSkull,
		onHealthPercentChange = onCreatureHealthPercentChange,
		onAppear = onCreatureAppear,
		onDisappear = onCreatureDisappear,
		onIconChange = onIconChange
	})
	
	disconnect(LocalPlayer, {
		onPositionChange = onCreaturePositionChange
	})

	disconnect(g_game, {
		onAttackingCreatureChange = onAttack,
		onFollowingCreatureChange = onFollow,
		onUpdateTarget = onUpdateTarget,
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
	})
	
	g_keyboard.unbindKeyDown('Ctrl+B')
	
	onGameEnd()
	m_BattleFunctions.destroy()
end

function onGameStart()
	m_BattleList.battleTopButton = modules.client_topmenu.addTopButton('BattleTopButton', 'battleButton', tr('Battle') .. ' (Ctrl+B)', toggle)
	m_BattleList.battleTopButton:setOn(m_BattleList.window:isVisible())
end

function onGameEnd()
	m_BattleFunctions.removeAllCreatures()
	
	if m_BattleList.battleTopButton then
		m_BattleList.battleTopButton:destroy()
		m_BattleList.battleTopButton = nil
	end
end

function toggle()
	if m_BattleList.battleTopButton:isOn() then
		m_BattleList.window:close()
		m_BattleList.battleTopButton:setOn(false)
	else
		m_BattleList.window:open()
		m_BattleList.battleTopButton:setOn(true)
	end
end

function onCreatureAppear(creature)
	if m_BattleFunctions.doCreatureFitFilters(creature) then
		m_BattleFunctions.addCreature(creature)
	end
end

function onCreatureDisappear(creature)
	m_BattleFunctions.removeCreature(creature)
end

function onCreatureHealthPercentChange(creature, health)
	local widget = m_BattleList.battlePanel:getChildById(creature:getId())
	if widget then
		m_BattleFunctions.setLifeBarPercent(creature:getHealthPercent(), widget:getChildById('healthBar'))
	end
end

function onUpdateTarget(creatureId)
	local widget = m_BattleList.battlePanel:getChildById(creatureId)
	if widget then
		g_game.attack(widget.creature)
	end
end

function onAttack(creature)
	local widget = creature and m_BattleList.battlePanel:getChildById(creature:getId()) or m_BattleList.lastBattleButtonSwitched
	if widget then
		widget.isTarget = creature and true or false
		m_BattleFunctions.attackCreature = creature
		m_BattleFunctions.updateBattleButton(widget)
	else
		if m_BattleFunctions.attackCreature then
			m_BattleFunctions.updateAttackSquare(m_BattleFunctions.attackCreature, false)
			m_BattleFunctions.attackCreature = nil
		elseif creature then
			m_BattleFunctions.updateAttackSquare(creature, true)
			m_BattleFunctions.attackCreature = creature
		end
	end
end

function onFollow(creature)
	local widget = creature and m_BattleList.battlePanel:getChildById(creature:getId()) or m_BattleList.lastBattleButtonSwitched
	if widget then
		widget.isFollowed = creature and true or false
		m_BattleFunctions.followCreature = creature
		m_BattleFunctions.updateBattleButton(widget)
	else
		if m_BattleFunctions.followCreature then
			m_BattleFunctions.updateFollowSquare(m_BattleFunctions.followCreature, false)
			m_BattleFunctions.followCreature = nil
		elseif creature then
			m_BattleFunctions.updateFollowSquare(creature, true)
			m_BattleFunctions.followCreature = creature
		end
	end
end

function onBattleButtonHoverChange(widget, hovered)
	if widget.isBattleButton then
		widget.isHovered = hovered
		m_BattleFunctions.updateBattleButton(widget)
	end
end

function onBattleButtonMouseRelease(self, mousePosition, mouseButton)
	if m_BattleList.cancelNextRelease then
		m_BattleList.cancelNextRelease = false
		return false
	end
	
	if ((g_mouse.isPressed(MouseLeftButton) and mouseButton == MouseRightButton) or
		(g_mouse.isPressed(MouseRightButton) and mouseButton == MouseLeftButton)) then
		m_BattleList.cancelNextRelease = true
		g_game.look(self.creature, true)
		return true
	elseif mouseButton == MouseLeftButton and g_keyboard.isShiftPressed() then
		g_game.look(self.creature, true)
		return true
	elseif mouseButton == MouseRightButton and not g_mouse.isPressed(MouseLeftButton) then
		modules.game_interface.createThingMenu(mousePosition, nil, nil, self.creature)
		return true
	elseif mouseButton == MouseLeftButton and not g_mouse.isPressed(MouseRightButton) then
		if self.isTarget then
			g_game.cancelAttack()
		else
			g_game.attack(self.creature)
		end
		
		return true
	end
	
	return false
end

function onCreaturePositionChange(creature, newPos, oldPos)
	if creature:isLocalPlayer() then
		if oldPos and newPos and newPos.z ~= oldPos.z then
			m_BattleFunctions.checkCreatures()
		end
	end
end

function onMiniWindowClose()
	if m_BattleList.battleTopButton then
		m_BattleList.battleTopButton:setOn(false)
	end
end

function updateCreatureSkull(creature, skullId)
	local widget = creature and m_BattleList.battlePanel:getChildById(creature:getId())
	if widget then
		m_BattleFunctions.updateSkull(widget, skullId)
	end
end

function onIconChange(creature, id, destroy)
	local widget = creature and m_BattleList.battlePanel:getChildById(creature:getId())
	if widget then
		m_BattleFunctions.updateIcon(widget, id, destroy)
	end
end
