-- private variables
local topMenu
local centerButtonsPanel
local centerBackground

function addTopButton(class, id, description, callback, front)
	local button = centerButtonsPanel:getChildById(id)
	if not button then
		button = g_ui.createWidget(class)
		if not front then
			centerButtonsPanel:insertChild(1, button)
		else
			centerButtonsPanel:addChild(button)
		end
	end
	
	button:setId(id)
	button:setTooltip(description)
	button.onMouseRelease = function(widget, mousePos, mouseButton)
		if widget:containsPoint(mousePos) and mouseButton ~= MouseMiddleButton then
			callback()
			return true
		end
	end
	
	return button
end

function slide(self)
	if self:isOn() then
		self:setOn(false)
		centerBackground:show()
		centerButtonsPanel:show()
		modules.game_tutorial.m_TutorialFunctions.updatePosition(false)
	else
		self:setOn(true)
		centerBackground:hide()
		centerButtonsPanel:hide()
		modules.game_tutorial.m_TutorialFunctions.updatePosition(true)
	end
end

-- public functions
function init()
	connect(g_game, { 
		onGameStart = online,
		onGameEnd = onGameEnd,
		onPing = onPing,
		onPingBack = updatePing
	})
	connect(g_app, { onFps = updateFps })

	g_ui.importStyle('client_topmenu')
	
	topMenu = g_ui.createWidget('TopMenuWindow')
	centerButtonsPanel = topMenu:getChildById('centerButtonsPanel')
	centerBackground = topMenu:getChildById('background')
	pingLabel = topMenu:getChildById('pingLabel')
	fpsLabel = topMenu:getChildById('fpsLabel')

	if g_game.isOnline() then
		online()
	end
end

function terminate()
	disconnect(g_game, {
		onGameStart = online,
		onGameEnd = onGameEnd,
		onPing = onPing,
		onPingBack = updatePing 
	})
	disconnect(g_app, { onFps = updateFps })
	
	pingLabel = nil
	fpsLabel = nil
	topMenu:destroy()
end

function online()
	if not topMenu:getParent() then
		topMenu:setParent(modules.game_interface.getRootPanel())
		topMenu:setWidth(494)
		topMenu:setHeight(74)
		topMenu:addAnchor(AnchorTop, 'parent', AnchorTop)
		topMenu:addAnchor(AnchorLeft, 'parent', AnchorLeft)
		topMenu:addAnchor(AnchorRight, 'parent', AnchorRight)
	end
	
	topMenu:show()
end

function onGameEnd()
	topMenu:hide()
end

function updateFps(fps)
	text = 'FPS: ' .. fps
	fpsLabel:setText(text)
end

function onPing()
	
end

function updatePing(ping)
	-- 0-120ms - kolor zielony: PERFECT/PERFEKCYJNE
	-- 121-240ms - kolor jasnozielony: GOOD/DOBRE
	-- 241-360ms - kolor ��ty: AVERAGE'PRZECI�TNE
	-- 361ms=> - kolor czerwony: POOR/S�ABE
	
	local text = 'Connection: '
	local color
	if ping < 0 then
		text = text .. "??"
		color = '#FFFF00'
	else
		-- text = text .. ping .. ' ms'
		if ping > 360 then
			text = text .. 'Poor'
			color = '#E30E0E'
		elseif ping > 240 then
			text = text .. 'Average'
			color = '#FFFF00'
		elseif ping > 120 then
			text = text .. 'Good'
			color = '#64E764'
		else
			text = text .. 'Perfect'
			color = '#00EB00'
		end
	end
	
	pingLabel:setColor(color)
	pingLabel:setText(text)
end

function getPing()
	return tonumber(pingLabel:getText()) or 0
end

function setPingVisible(enable)
	pingLabel:setVisible(enable)
end

function setFpsVisible(enable)
	fpsLabel:setVisible(enable)
end

function getButton(id)
  return topMenu:recursiveGetChildById(id)
end

function getTopMenu()
  return topMenu
end
