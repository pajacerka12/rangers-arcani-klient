m_OutfitFunctions = {}
m_OutfitList = {}

m_OutfitFunctions.colorsAmount = 7
m_OutfitFunctions.colorsAtLine = 18

m_OutfitFunctions.updateAddons = function(addons)
	m_OutfitList.ignoreCheck = true
	if addons == 1 then
		m_OutfitList.addon1:setChecked(true)
	elseif addons == 2 then
		m_OutfitList.addon2:setChecked(true)
	elseif addons == 3 then
		m_OutfitList.addon1:setChecked(true)
		m_OutfitList.addon2:setChecked(true)
	end
	
	m_OutfitList.ignoreCheck = false
end

m_OutfitFunctions.updateOutfits = function()
	for _, widget in pairs(m_OutfitList.outfitList:getChildren()) do
		m_OutfitList.outfit.type = widget.outfit
		
		local availableAddons = m_OutfitList.outfitsList[widget.id][4]
		local outfit = {type = m_OutfitList.outfit.type, head = m_OutfitList.outfit.head, body = m_OutfitList.outfit.body, legs = m_OutfitList.outfit.legs, feet = m_OutfitList.outfit.feet, addons = m_OutfitList.outfit.addons}
		if availableAddons < 3 then
			if outfit.addons == 3 then
				outfit.addons = math.min(outfit.addons, availableAddons)
			elseif outfit.addons ~= availableAddons then
				outfit.addons = 0
			end
		elseif availableAddons == 255 then
			outfit.addons = 0
		end
		
		if outfit.type == m_OutfitList.currentOutfit.type then
			m_OutfitList.currentAddons = outfit.addons
		end
		
		widget:getChildById('outfit'):setOutfit(outfit)
	end
end

m_OutfitFunctions.changeColorCheck = function(self, id)
	if id == 1 then
		m_OutfitList.outfit.head = self.colorId
		m_OutfitList.currentOutfit.head = self.colorId
	elseif id == 2 then
		m_OutfitList.outfit.body = self.colorId
		m_OutfitList.currentOutfit.body = self.colorId
	elseif id == 3 then
		m_OutfitList.outfit.legs = self.colorId
		m_OutfitList.currentOutfit.legs = self.colorId
	elseif id == 4 then
		m_OutfitList.outfit.feet = self.colorId
		m_OutfitList.currentOutfit.feet = self.colorId
	end
	
	m_OutfitFunctions.updateOutfits()
end

m_OutfitFunctions.onColorCheckChange = function(self)
	local id = self.panelId
	if m_OutfitList.checking[id] then
		return true
	end
	
	if (id == 1 and self == m_OutfitList.currentHead) or
		(id == 2 and self == m_OutfitList.currentBody) or
		(id == 3 and self == m_OutfitList.currentLegs) or
		(id == 4 and self == m_OutfitList.currentFeet) then
		m_OutfitList.checking[id] = true
		self:setChecked(true)
		m_OutfitList.checking[id] = false
	else
		local widget = nil
		if id == 1 and m_OutfitList.currentHead then
			widget = m_OutfitList.currentHead
			m_OutfitList.currentHead = self
		elseif id == 2 and m_OutfitList.currentBody then
			widget = m_OutfitList.currentBody
			m_OutfitList.currentBody = self
		elseif id == 3 and m_OutfitList.currentLegs then
			widget = m_OutfitList.currentLegs
			m_OutfitList.currentLegs = self
		elseif id == 4 and m_OutfitList.currentFeet then
			widget = m_OutfitList.currentFeet
			m_OutfitList.currentFeet = self
		end
		
		if widget then
			m_OutfitList.checking[id] = true
			widget:setChecked(false)
			m_OutfitList.checking[id] = false
		end
	end
	
	m_OutfitFunctions.changeColorCheck(self, id)
end

m_OutfitFunctions.selectOutfit = function(self)
	if m_OutfitList.selectedOutfit then
		m_OutfitList.selectedOutfit:getChildById('blink'):hide()
	end
	
	m_OutfitList.selectedOutfit = self
	self:getChildById('blink'):show()
	m_OutfitList.currentOutfit.type = self.outfit
end

m_OutfitFunctions.selectMount = function(self)
	if m_OutfitList.selectedMount then
		m_OutfitList.selectedMount:getChildById('blink'):hide()
		
		if m_OutfitList.selectedMount == self then
			m_OutfitList.selectedMount = false
			m_OutfitList.currentOutfit.mount = 0
			return true
		end
	end
	
	m_OutfitList.selectedMount = self
	self:getChildById('blink'):show()
	m_OutfitList.currentOutfit.mount = self.outfit
end

function showUnavailableMounts(self)
	local visible = self:isChecked()
	for _, widget in pairs(m_OutfitList.mountList:getChildren()) do
		if widget:getChildById('mask'):isVisible() then
			if not visible then
				widget:hide()
			end
		else
			if visible then
				widget:show()
			end
		end
	end
end

function showUnavailableOutfits(self)
	local visible = self:isChecked()
	for _, widget in pairs(m_OutfitList.outfitList:getChildren()) do
		if widget:getChildById('mask'):isVisible() then
			if not visible then
				widget:hide()
			end
		else
			if visible then
				widget:show()
			end
		end
	end
end

function onAddonCheckChange(self, value)
	if m_OutfitList.ignoreCheck then
		return true
	end
	
	if self:isChecked() then
		m_OutfitList.outfit.addons = m_OutfitList.outfit.addons + value
	else
		m_OutfitList.outfit.addons = m_OutfitList.outfit.addons - value
	end
	
	m_OutfitFunctions.updateOutfits()
end

function randomize()
	local colorsPanels = {m_OutfitList.headColorBoxPanel, m_OutfitList.bodyColorBoxPanel, m_OutfitList.legsColorBoxPanel, m_OutfitList.feetColorBoxPanel}
	for i = 1, #colorsPanels do
		colorsPanels[i]:getChildById(math.random(1, 132)):setChecked(true)
	end
end

function accept()
	print('type="' .. m_OutfitList.currentOutfit.type .. '" head="' .. m_OutfitList.currentOutfit.head .. '" body="' .. m_OutfitList.currentOutfit.body .. '" legs="' .. m_OutfitList.currentOutfit.legs .. '" feet="' .. m_OutfitList.currentOutfit.feet .. '" addons ="' .. m_OutfitList.currentOutfit.addons .. '"')
	print('type = ' .. m_OutfitList.currentOutfit.type .. ', head = ' .. m_OutfitList.currentOutfit.head .. ', body = ' .. m_OutfitList.currentOutfit.body .. ', legs = ' .. m_OutfitList.currentOutfit.legs .. ', feet = ' .. m_OutfitList.currentOutfit.feet .. ', addons = ' .. m_OutfitList.currentOutfit.addons)
	
	m_OutfitList.currentOutfit.addons = m_OutfitList.currentAddons
	g_game.changeOutfit(m_OutfitList.currentOutfit)
	destroy()
end

function destroy()
	if m_OutfitList.window then
		m_OutfitList.headColorBoxPanel:destroyChildren()
		
		m_OutfitList.window:destroy()
		m_OutfitList = {}
	
		modules.game_inventory.setOutfit()
	end
end

function onOpenOutfitWindow(creatureOutfit, outfitList, mountList)
	if m_OutfitList.window then
		return true
	end
	
	m_OutfitList.window = g_ui.displayUI('outfitwindow')
	m_OutfitList.creatureOutfit = creatureOutfit
	m_OutfitList.outfitsList = outfitList
	m_OutfitList.mountsList = mountList
	m_OutfitList.outfitList = m_OutfitList.window:getChildById('outfitList')
	m_OutfitList.mountList = m_OutfitList.window:getChildById('mountList')
	m_OutfitList.headColorBoxPanel = m_OutfitList.window:getChildById('headColorBoxPanel'):getChildById('panel')
	m_OutfitList.bodyColorBoxPanel = m_OutfitList.window:getChildById('bodyColorBoxPanel'):getChildById('panel')
	m_OutfitList.legsColorBoxPanel = m_OutfitList.window:getChildById('legsColorBoxPanel'):getChildById('panel')
	m_OutfitList.feetColorBoxPanel = m_OutfitList.window:getChildById('feetColorBoxPanel'):getChildById('panel')
	m_OutfitList.addon1 = m_OutfitList.window:getChildById('addon1')
	m_OutfitList.addon2 = m_OutfitList.window:getChildById('addon2')
	
	m_OutfitList.checking = {}
	m_OutfitList.outfit = creatureOutfit:getOutfit()
	m_OutfitList.currentOutfit = creatureOutfit:getOutfit()
	
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		language = 3
	else
		language = 2
	end
	
	for i = 1, #outfitList do
		local widget = g_ui.createWidget('OutfitCreature', m_OutfitList.outfitList)
		m_OutfitList.outfit.type = outfitList[i][1]
		widget.outfit = outfitList[i][1]
		widget.id = i
		widget:getChildById('name'):setText(outfitList[i][language])
		
		widget.onMouseRelease = m_OutfitFunctions.selectOutfit
		
		if not outfitList[i][5] then
			widget:setEnabled(false)
			widget:hide()
			widget:getChildById('mask'):show()
		else
			widget:getChildById('mask'):hide()
		end
		
		local availableAddons = outfitList[i][4]
		if availableAddons == 1 or availableAddons == 3 then
			widget:getChildById('addon1'):setOn(true)
		end
		
		if availableAddons == 2 or availableAddons == 3 then
			widget:getChildById('addon2'):setOn(true)
		end
		
		if outfitList[i][1] == m_OutfitList.currentOutfit.type then
			widget:getChildById('blink'):show()
			m_OutfitList.selectedOutfit = widget
		end
	end
	
	for i = 1, #mountList do
		local widget = g_ui.createWidget('MountCreature', m_OutfitList.mountList)
		widget.outfit = mountList[i][1]
		widget.id = i
		widget:getChildById('name'):setText(mountList[i][language])
		widget:getChildById('outfit'):setOutfit({type = mountList[i][1]})
		
		widget.onMouseRelease = m_OutfitFunctions.selectMount
		
		if not mountList[i][4] then
			widget:setEnabled(false)
			widget:hide()
			widget:getChildById('mask'):show()
		else
			widget:getChildById('mask'):hide()
		end
		
		if mountList[i][1] == m_OutfitList.currentOutfit.mount then
			widget:getChildById('blink'):show()
			m_OutfitList.selectedMount = widget
		end
	end
	
	m_OutfitList.outfit.addons = math.min(m_OutfitList.outfit.addons, 3)
	m_OutfitList.currentOutfit.addons = math.min(m_OutfitList.outfit.addons, 3)
	m_OutfitFunctions.updateAddons(m_OutfitList.outfit.addons)
	m_OutfitFunctions.updateOutfits()
	
	m_OutfitList.outfitList:setWidth(math.min(8, #outfitList) * 108 - 8)
	local colorsPanels = {m_OutfitList.headColorBoxPanel, m_OutfitList.bodyColorBoxPanel, m_OutfitList.legsColorBoxPanel, m_OutfitList.feetColorBoxPanel}
	for j = 0, m_OutfitFunctions.colorsAtLine do
		for i = 0, m_OutfitFunctions.colorsAmount - 1 do
			for k = 1, #colorsPanels do
				local color = i * 19 + j
				local colorBox = g_ui.createWidget('ColorBox', colorsPanels[k])
				local outfitColor = getOufitColor(color)
				colorBox:setImageColor(outfitColor)
				colorBox:setId(color)
				colorBox.colorId = color
				colorBox.panelId = k
				
				if k == 1 then
					if color == m_OutfitList.outfit.head then
						m_OutfitList.currentHead = colorBox
						colorBox:setChecked(true)
					end
				elseif k == 2 then
					if color == m_OutfitList.outfit.body then
						m_OutfitList.currentBody = colorBox
						colorBox:setChecked(true)
					end
				elseif k == 3 then
					if color == m_OutfitList.outfit.legs then
						m_OutfitList.currentLegs = colorBox
						colorBox:setChecked(true)
					end
				elseif k == 4 then
					if color == m_OutfitList.outfit.feet then
						m_OutfitList.currentFeet = colorBox
						colorBox:setChecked(true)
					end
				end
				
				colorBox.onCheckChange = m_OutfitFunctions.onColorCheckChange
			end
		end
	end
end

function onLoad()
	connect(g_game, {
		onOpenOutfitWindow = onOpenOutfitWindow,
		onGameEnd = destroy
	})
end

function onUnload()
	disconnect(g_game, {
		onOpenOutfitWindow = onOpenOutfitWindow,
		onGameEnd = destroy
	})
	
	destroy()
end