m_OptionsFunctions = {}
m_OptionsList = {}

m_OptionsFunctions.text = {
	tr('Red saturation'),
	tr('Green saturation'),
	tr('Blue saturation')
}

m_OptionsFunctions.colors = {
	'#FF0000',
	'#FF7F00',
	'#FFFF00',
	'#00FF00',
	'#0000FF',
	'#4B0082',
	'#9400D3'
}

m_OptionsFunctions.colorsBars = {
	{'#46CC0C', '#52AE0C', '#63800D', '#75530D', '#80340E', '#8C160E'},
	{'#075774', '#29687B', '#5C8185', '#909B8F', '#B2AC96', '#D4BD9D'},
	{'#BC3BA8', '#9E37AD', '#8033B2', '#532CBA', '#2626C2', '#0822C7'},
	{'#2E882E', '#4C872A', '#798724', '#988620', '#C5851A', '#E38516'},
	{'#16B758', '#32AA6F', '#5C9791', '#8684B4', '#A278CB', '#BE6BE2'},
	{'#15EFA4', '#30C9A7', '#5890AB', '#8056AF', '#9B30B2', '#B60AB5'},
	{'#86F505', '#80CF18', '#7AAA2C', '#717149', '#683966', '#621379'},
}

m_OptionsFunctions.defaultOptions = {
	vsync = true,
	showFps = true,
	showPing = true,
	fullscreen = false,
	classicControl = true,
	smartWalk = true,
	dashWalk = false,
	showStatusMessagesInConsole = true,
	showEventMessagesInConsole = true,
	showInfoMessagesInConsole = true,
	showTimestampsInConsole = true,
	showLevelsInConsole = true,
	showPrivateMessagesOnScreen = true,
	showNPCMessagesInLog = true,
	itemDescriptionDuration = 10,
	foregroundFrameRate = 60,
	backgroundFrameRate = 60,
	painterEngine = 0,
	enableAudio = true,
	musicSoundVolume = 15,
	enableLights = true,
	ambientLight = 40,
	displayNames = true,
	displayHealth = true,
	displayText = true,
	displayInfoWindow = true,
	drawHealthManaArcs = false,
	displayItemCorners = true,
	displayItemFrames = false,
	displayFavoriteStar = true,
	displayHints = true,
	displayConditionIcons = true,
	displayAllConditionIcons = false,
	enableWASDDiagonals = false,
	cancelsSelectedTarget = true,
	visibleUnknownMonsters = false,
	visibleCompletedTasks = true,
	displayHealthAndManaBars = true,
	displayRightBottomPanel = true,
	moveStacksOfItemsWithoutCtrl = true,
	secureMode = false,
	
	circleColorId = 1,
	localPlayerColorId = 1,
	playerColorId = 1,
	monsterColorId = 1,
	npcColorId = 2,
	
	automaticallySendToMagazine = false,
	showAreaSpells = true,
	addSpellToBar = true,
	
	showLootMessage = true,
	showAdvanceMessage = true,
	showExperienceGain = true,
	showDamageDeal = true,
	showDamageRecived = true,
	showSummonDamageDeal = true,
	showSummonDamageRecived = true,
	showHealingDamage = true,
	showManaRecived = true,
	showStealHealth = true,
	showStealMana = true
}

m_OptionsFunctions.options = {}
m_OptionsFunctions.config = {
	{tr('Game'), '/images/optionstab/game', 'GamePanel'},
	{tr('Console'), '/images/optionstab/console', 'ConsolePanel'},
	{tr('Graphics'), '/images/optionstab/graphics', 'GraphicsPanel'},
	{tr('Colors'), '/images/optionstab/misc', 'GameWindowPanel'},
	{tr('Audio'), '/images/optionstab/audio', 'AudioPanel'},
}

m_OptionsFunctions.SETTINGS_GAME = 1
m_OptionsFunctions.SETTINGS_CONSOLE = 2
m_OptionsFunctions.SETTINGS_GRAPHICS = 3
m_OptionsFunctions.SETTINGS_GAMEWINDOW = 4
m_OptionsFunctions.SETTINGS_AUDIO = 5

m_OptionsFunctions.destroy = function()
	if m_OptionsList.currentBox then
		m_OptionsList.currentBox:destroy()
	end
	
	if m_OptionsList.window then
		m_OptionsList.window:destroy()
	end
end

function select(id)
	if m_OptionsList.currentBox then
		m_OptionsList.currentBox:hide()
	end
	
	m_OptionsList.currentBox = m_OptionsFunctions.list[tonumber(id)]
	m_OptionsList.currentBox:show()
	if m_OptionsFunctions.currentExpand then
		m_OptionsFunctions.currentExpand:setHeight(26)
		m_OptionsFunctions.currentArrow:setOn(false)
		
		m_OptionsFunctions.currentExpand = nil
		m_OptionsFunctions.currentArrow = nil
	end
end

function onLoad()
	connect(g_game, { onGameStart = onGameStart,
						onGameEnd = onGameEnd })
	
	for k, v in pairs(m_OptionsFunctions.defaultOptions) do
		g_settings.setDefault(k, v)
		m_OptionsFunctions.options[k] = v
	end
	
	g_ui.importStyle('game')
	g_ui.importStyle('console')
	g_ui.importStyle('graphics')
	g_ui.importStyle('gamewindow')
	g_ui.importStyle('audio')
	
	g_keyboard.bindKeyDown('Ctrl+F', function() toggleOption('fullscreen') end)
	g_keyboard.bindKeyDown('Ctrl+N', toggleDisplays)
	
	m_OptionsList.window = g_ui.displayUI('options')
	m_OptionsList.window:hide()
	
	m_OptionsList.panel = m_OptionsList.window:getChildById('optionsTabContent')
	m_OptionsList.optionsList = m_OptionsList.window:getChildById('optionsList')
	for i = 1, #m_OptionsFunctions.config do
		local widget = g_ui.createWidget('OptionTabButton', m_OptionsList.optionsList)
		widget:setParent(m_OptionsList.optionsList)
		widget:setText(m_OptionsFunctions.config[i][1])
		widget:getChildById('icon'):setImageSource(m_OptionsFunctions.config[i][2])
		widget:setId(i)
	end
	
	m_OptionsFunctions.list = {}
	addEvent(function() 
		for i = 1, #m_OptionsFunctions.config do
			local widget = g_ui.createWidget(m_OptionsFunctions.config[i][3], m_OptionsList.panel)
			widget:setParent(m_OptionsList.panel)
			widget:setWidth(m_OptionsList.panel:getWidth())
			
			if i == m_OptionsFunctions.SETTINGS_CONSOLE or i == m_OptionsFunctions.SETTINGS_GAME then
				widget:setHeight((#widget:getChildren() * 21))
			else
				widget:setHeight(m_OptionsList.panel:getHeight() - 16)
			end
			m_OptionsFunctions.list[i] = widget
		end
	
		select(m_OptionsFunctions.SETTINGS_GAME)
		m_OptionsFunctions.setup()
	end)
	
	m_OptionsList.optionsButton = modules.client_topmenu.addTopButton('SettingsButton', 'optionsButton', tr('Options'), toggle)
	m_OptionsList.audioButton = modules.client_topmenu.addTopButton('AudioButton', 'audioButton', tr('Audio'), function() toggleOption('enableAudio') end)
	
	if not g_game.isOnline() then
		m_OptionsList.optionsButton:hide()
		m_OptionsList.audioButton:hide()
	end
end

function onUnload()
	disconnect(g_game, { onGameStart = onGameStart,
						 onGameEnd = onGameEnd })
						 
	g_keyboard.unbindKeyDown('Ctrl+F')
	g_keyboard.unbindKeyDown('Ctrl+N')
	m_OptionsFunctions.destroy()
end

function onGameStart()
	m_OptionsList.optionsButton:show()
	m_OptionsList.audioButton:show()
	
	hide()
end

function onGameEnd()
	m_OptionsList.optionsButton:hide()
	m_OptionsList.audioButton:hide()
	
	hide()
end

function toggle()
	if m_OptionsList.window:isVisible() then
		hide()
	else
		show()
	end
end

function show()
	m_OptionsList.optionsButton:setOn(true)
	m_OptionsList.window:show()
	m_OptionsList.window:raise()
	m_OptionsList.window:focus()
end

function hide()
	m_OptionsList.optionsButton:setOn(false)
	m_OptionsList.window:hide()
	if m_OptionsFunctions.currentExpand then
		m_OptionsFunctions.currentExpand:setHeight(26)
		m_OptionsFunctions.currentArrow:setOn(false)
		
		m_OptionsFunctions.currentExpand = nil
		m_OptionsFunctions.currentArrow = nil
	end
end

function toggleDisplays()
	if m_OptionsFunctions.options['displayNames'] and m_OptionsFunctions.options['displayHealth'] then
		setOption('displayNames', false)
	elseif m_OptionsFunctions.options['displayHealth'] then
		setOption('displayHealth', false)
	else
		if not m_OptionsFunctions.options['displayNames'] and not m_OptionsFunctions.options['displayHealth'] then
			setOption('displayNames', true)
		else
			setOption('displayHealth', true)
		end
	end
end

function toggleOption(key)
	setOption(key, not getOption(key))
end

function getOption(key)
	return m_OptionsFunctions.options[key]
end

function setOption(key, value, force)
	if not force and m_OptionsFunctions.options[key] == value then
		return false
	end
	
	local gameMapPanel = modules.game_interface.getMapPanel()
	if key == 'vsync' then
		g_window.setVerticalSync(value)
	elseif key == 'showFps' then
		if modules.client_topmenu then
			modules.client_topmenu.setFpsVisible(value)
		end
	elseif key == 'showPing' then
		if modules.client_topmenu then
			modules.client_topmenu.setPingVisible(value)
		end
	elseif key == 'displayInfoWindow' then
		if modules.game_lookitemmove then
			modules.game_lookitemmove.m_LookItemMoveFunction.setWindowVisible(value)
		end
	elseif key == 'fullscreen' then
		modules.client_background.updateCloseButton(value)
		g_window.setFullscreen(value)
	elseif key == 'addSpellToBar' then
		if modules.game_character then
			modules.game_character.m_SpellsFunctions.setAddSpell(value)
		end
	elseif key == 'showAreaSpells' then
		if modules.game_character then
			modules.game_character.m_SpellsFunctions.setVisibleArea(value)
		end
	elseif key == 'automaticallySendToMagazine' then
		if modules.game_rewards then
			modules.game_rewards.m_CraftingFunctions.sendAutomatically(value)
		end
	elseif key == 'autoClearLoot' then
		if modules.game_tracker then
			modules.game_tracker.m_TrackerFunctions.setOption(value)
		end
	elseif key == 'visibleUnknownMonsters' then
		if modules.game_encyclopedia then
			modules.game_encyclopedia.m_BestiaryFunctions.setVisibleUnknownMonsters(value)
		end
	elseif key == 'visibleCompletedTasks' then
		if modules.game_encyclopedia then
			modules.game_encyclopedia.m_BestiaryFunctions.setVisibleCompletedTasks(value)
		end
	elseif key == 'displayHealthAndManaBars' then
		if modules.game_spellbar then
			modules.game_spellbar.m_SpellBarFunctions.setVisibleBars(value)
		end
	elseif key == 'displayRightBottomPanel' then
		if modules.game_bottompanel then
			modules.game_bottompanel.m_MainFunctions.setVisiblePanel(value)
		end
	elseif key == 'enableAudio' then
		if modules.game_sounds then
			modules.game_sounds.setEnableAudio(value)
		end
		
		modules.client_background.setEnableAudio(value)
		if value then
			m_OptionsList.audioButton:setOn(false)
		else
			m_OptionsList.audioButton:setOn(true)
		end
	elseif key == 'musicSoundVolume' then
		if modules.game_sounds then
			modules.game_sounds.setVolume(value)
		end
		
		m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_AUDIO]:getChildById('musicSoundVolumeLabel'):setText(tr('Music volume: %d', value))
	elseif key == 'backgroundFrameRate' then
		local text, v = value, value
		if value <= 0 or value >= 201 then
			text = 'max'
			v = 0
		end
		
		m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_GRAPHICS]:getChildById('backgroundFrameRateLabel'):setText(tr('Game framerate limit: %s', text))
		g_app.setBackgroundPaneMaxFps(v)
	elseif key == 'foregroundFrameRate' then
		local text, v = value, value
		if value <= 0 or value >= 61 then
			text = 'max'
			v = 0
		end
		
		m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_GRAPHICS]:getChildById('foregroundFrameRateLabel'):setText(tr('Interface framerate limit: %s', text))
		g_app.setForegroundPaneMaxFps(v)
	elseif key == 'itemDescriptionDuration' then
		m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_GRAPHICS]:getChildById('itemDescriptionDurationLabel'):setText(tr('Time after which the item description window will disappear: %d second%s', value, value > 1 and 's' or ''))
		modules.game_lookat.m_LookAtFunctions.setDuration(value)
	elseif key == 'enableLights' then
		gameMapPanel:setDrawLights(true)
	elseif key == 'ambientLight' then
		gameMapPanel:setMinimumAmbientLight(math.min(20, value / (100 * 5)))
		gameMapPanel:setDrawLights(true)
	elseif key == 'painterEngine' then
		g_graphics.selectPainterEngine(value)
	elseif key == 'displayNames' then
		gameMapPanel:setDrawNames(value)
	elseif key == 'displayHealth' then
		gameMapPanel:setDrawHealthBars(value)
	elseif key == 'displayConditionIcons' then
		g_game.displayConditionIcons(value, false)
	elseif key == 'displayAllConditionIcons' then
		g_game.displayConditionIcons(value, true)
	elseif key == 'displayText' then
		gameMapPanel:setDrawTexts(value)
	elseif key == 'drawHealthManaArcs' then
		gameMapPanel:setDrawHealthManaArcs(value)
		if modules.game_healthinfo then
			modules.game_healthinfo.m_HealthInfoFunctions.showArcs(value)
		end
	elseif key == 'displayItemCorners' then
		if modules.game_inventory then
			modules.game_inventory.updateCorners(value)
		end
		if modules.game_containers then
			modules.game_containers.updateCorners(value)
		end
		if modules.game_market then
			modules.game_market.m_MarketFunctions.updateCorners(value)
		end
	elseif key == 'displayItemFrames' then
		if modules.game_inventory then
			modules.game_inventory.updateFrames(value)
		end
		if modules.game_containers then
			modules.game_containers.updateFrames(value)
		end
		if modules.game_market then
			modules.game_market.m_MarketFunctions.updateFrames(value)
		end
	elseif key == 'displayFavoriteStar' then
		if modules.game_inventory then
			modules.game_inventory.updateFavoriteStars(value)
		end
		if modules.game_containers then
			modules.game_containers.updateFavoriteStars(value)
		end
	end
	
	for i = 1, #m_OptionsFunctions.list do
		local widget = m_OptionsFunctions.list[i]:recursiveGetChildById(key)
		if widget then
			if widget:getStyle().__class == 'UICheckBox' then
				widget:setChecked(value)
			elseif widget:getStyle().__class == 'UIScrollBar' then
				widget:setValue(value)
			end
			
			break
		end
	end
	
	g_settings.set(key, value)
	m_OptionsFunctions.options[key] = value
	return true
end

m_OptionsFunctions.setupGraphicsEngines = function()
	local widget = m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_GRAPHICS]
	local enginesRadioGroup = UIRadioGroup.create()
	local ogl1 = widget:getChildById('opengl1')
	local ogl2 = widget:getChildById('opengl2')
	local dx9 = widget:getChildById('directx9')
	
	enginesRadioGroup:addWidget(ogl1)
	enginesRadioGroup:addWidget(ogl2)
	enginesRadioGroup:addWidget(dx9)

	if g_window.getPlatformType() == 'WIN32-EGL' then
		enginesRadioGroup:selectWidget(dx9)
		ogl1:setEnabled(false)
		ogl2:setEnabled(false)
		dx9:setEnabled(true)
		dx9:show()
	else
		ogl1:setEnabled(g_graphics.isPainterEngineAvailable(1))
		ogl2:setEnabled(g_graphics.isPainterEngineAvailable(2))
		dx9:setEnabled(false)
		dx9:hide()
		if g_graphics.getPainterEngine() == 2 then
			enginesRadioGroup:selectWidget(ogl2)
		else
			enginesRadioGroup:selectWidget(ogl1)
		end

		if g_app.getOs() ~= 'windows' then
			dx9:hide()
		end
	end

	enginesRadioGroup.onSelectionChange = function(self, selected)
		if selected == ogl1 then
			setOption('painterEngine', 1)
		elseif selected == ogl2 then
			setOption('painterEngine', 2)
		end
	end

	if not g_graphics.canCacheBackbuffer() then
		widget:getChildById('foregroundFrameRate'):disable()
		widget:getChildById('foregroundFrameRateLabel'):disable()
	end
end

m_OptionsFunctions.expand = function(id, hide)
	local widget = m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_GAMEWINDOW]
	local list = widget:getChildById(id)
	local self = list:getChildById('arrow')
	if hide or self:isOn() then
		list:setHeight(26)
		self:setOn(false)
		
		if m_OptionsFunctions.currentExpand == list then
			m_OptionsFunctions.currentExpand = nil
			m_OptionsFunctions.currentArrow = nil
		end
	else
		list:setHeight((#list:getChildById('list'):getChildren() * 20) + 6)
		self:setOn(true)
		if m_OptionsFunctions.currentExpand then
			m_OptionsFunctions.currentExpand:setHeight(26)
			m_OptionsFunctions.currentArrow:setOn(false)
		end
		
		m_OptionsFunctions.currentExpand = list
		m_OptionsFunctions.currentArrow = self
	end
end

m_OptionsFunctions.selectColor = function(self)
	if m_OptionsFunctions.currentExpand then
		m_OptionsFunctions.currentExpand:setHeight(26)
		m_OptionsFunctions.currentArrow:setOn(false)
		
		m_OptionsFunctions.currentExpand = nil
		m_OptionsFunctions.currentArrow = nil
	end
	
	local id = tonumber(self:getId())
	if setOption('circleColorId', id) then
		m_OptionsFunctions.setupColors('circleColorChoose', 'circleColorId')
		modules.game_battle.m_BattleFunctions.updateColor(m_OptionsFunctions.colors[id])
	end
end

m_OptionsFunctions.selectColorBars = function(self)
	if m_OptionsFunctions.currentExpand then
		m_OptionsFunctions.currentExpand:setHeight(26)
		m_OptionsFunctions.currentArrow:setOn(false)
		
		m_OptionsFunctions.currentExpand = nil
		m_OptionsFunctions.currentArrow = nil
	end
	
	local id = tonumber(self:getId())
	if setOption(self.optionId, id) then
		m_OptionsFunctions.setupColors(self.panel, self.optionId)
	end
end

m_OptionsFunctions.addColor = function(id, parent)
	local color = g_ui.createWidget('GameWindowColorBox', parent)
	color:setParent(parent)
	color:setBackgroundColor(m_OptionsFunctions.colors[id])
	color:setId(id)
end

m_OptionsFunctions.addColorBar = function(id, parent, panel, optionId)
	local color = g_ui.createWidget('GameWindowColorBars', parent)
	color:setParent(parent)
	color.optionId = optionId
	color.panel = panel
	for i = 1, #m_OptionsFunctions.colorsBars[id] do
		color:getChildById('color' .. i):setBackgroundColor(m_OptionsFunctions.colorsBars[id][i])
	end
	
	color:setId(id)
end

m_OptionsFunctions.setupColors = function(panel, optionId)
	local widget = m_OptionsFunctions.list[m_OptionsFunctions.SETTINGS_GAMEWINDOW]
	local list = widget:getChildById(panel):getChildById('list')
	list:destroyChildren()
	
	local id = m_OptionsFunctions.options[optionId]
	if panel == 'circleColorChoose' then
		m_OptionsFunctions.addColor(id, list)
		for i = 1, #m_OptionsFunctions.colors do
			if i ~= id then
				m_OptionsFunctions.addColor(i, list)
			end
		end
	else
		m_OptionsFunctions.addColorBar(id, list, panel, optionId)
		for i = 1, #m_OptionsFunctions.colorsBars do
			if i ~= id then
				m_OptionsFunctions.addColorBar(i, list, panel, optionId)
			end
		end
		
		local gameMapPanel = modules.game_interface.getMapPanel()
		local var = m_OptionsFunctions.colorsBars[id]
		gameMapPanel:setBarColors(panel, var[1], var[2], var[3], var[4], var[5], var[6])
	end
end

m_OptionsFunctions.getCircleColor = function()
	return m_OptionsFunctions.colors[m_OptionsFunctions.options['circleColorId']]
end

m_OptionsFunctions.setup = function()
	m_OptionsFunctions.setupGraphicsEngines()

	for k,v in pairs(m_OptionsFunctions.defaultOptions) do
		if k ~= 'ambientLight' then
			if type(v) == 'boolean' then
				setOption(k, g_settings.getBoolean(k), true)
			elseif type(v) == 'number' then
				setOption(k, g_settings.getNumber(k), true)
			end
		else
			setOption(k, m_OptionsFunctions.defaultOptions.ambientLight, true)
		end
	end
	
	m_OptionsFunctions.setupColors('circleColorChoose', 'circleColorId')
	m_OptionsFunctions.setupColors('localPlayerColorChoose', 'localPlayerColorId')
	m_OptionsFunctions.setupColors('playerColorChoose', 'playerColorId')
	m_OptionsFunctions.setupColors('monsterColorChoose', 'monsterColorId')
	m_OptionsFunctions.setupColors('npcColorChoose', 'npcColorId')
end
