function UIItem:onDragEnter(mousePos)
	if self:isVirtual() then
		return false
	end

	local item = self:getItem()
	if not item then
		return false
	end
	
	modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem(item:getId())
	self:setBorderWidth(1)
	self.currentDragThing = item
	g_mouse.pushCursor('target')
	return true
end

function UIItem:onDragLeave(droppedWidget, mousePos)
	if self:isVirtual() then
		return false
	end
	
	self.currentDragThing = nil
	g_mouse.popCursor('target')
	self:setBorderWidth(0)
	self.hoveredWho = nil
	return true
end

function UIItem:onDrop(widget, mousePos)
	if not self:canAcceptDrop(widget, mousePos) then
		return false
	end
	
	local item = widget.currentDragThing
	if not item:isItem() then
		return false
	end
	
	local toPos = self.position
	if not toPos then
		return false
	end
	
	local itemPos = item:getPosition()
	if itemPos and itemPos.x == toPos.x and itemPos.y == toPos.y and itemPos.z == toPos.z then 
		return false 
	end
	
	modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem(item:getId())
	if item:getCount() > 1 then
		modules.game_interface.moveStackableItem(item, toPos)
	else
		g_game.move(item, toPos, 1)
	end

	self:setBorderWidth(0)
	return true
end

function UIItem:onDestroy()
	if modules.game_lookitemmove then
		modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem()
	end
	
	if self == g_ui.getDraggingWidget() and self.hoveredWho then
		self.hoveredWho:setBorderWidth(0)
	end
	
	if self.hoveredWho then
		self.hoveredWho = nil
	end
end

function UIItem:onHoverChange(hovered)
	if not hovered then
		modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindow()
	end
	
	if self:getId():find('protected') then
		return false
	end
	
	UIWidget.onHoverChange(self, hovered)
	local virtual = self:isVirtual() or self.classId or not self:isDraggable()
	local item = self:getItem()
	if item and hovered then
		modules.game_lookitemmove.m_LookItemMoveFunction.createWindow(self, self:getPosition(), item, getClassNameById(self.classId or item:getClassId()), item:getMarketId(), item:getMarketPage(), virtual)
	end
	
	if virtual then
		return false
	end
	
	local draggingWidget = g_ui.getDraggingWidget()
	if draggingWidget and self ~= draggingWidget then
		local gotMap = draggingWidget:getClassName() == 'UIGameMap'
		local gotItem = draggingWidget:getClassName() == 'UIItem' and not draggingWidget:isVirtual()
		if hovered and (gotItem or gotMap) then
			self:setBorderWidth(1)
			draggingWidget.hoveredWho = self
		else
			self:setBorderWidth(0)
			draggingWidget.hoveredWho = nil
		end
	end
end

function UIItem:onMouseMove(mousePos, mouseMoved)
	
end

function UIItem:onMouseRelease(mousePosition, mouseButton)
	if self:getId():find('protected') then
		return false
	end
	
	if self.cancelNextRelease then
		self.cancelNextRelease = false
		return true
	end
	
	local item = self:getItem()
	if not item then
		return false
	end
	
	modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem(item:getId())
	
	local virtual = self:isVirtual() or not self:isDraggable() or self:getItemCount() == 0 or self.ignoreSmelt
	local classId = getClassNameById(self.classId or self:getClassType())
	if not self:containsPoint(mousePosition) then
		modules.game_lookat.m_LookAtFunctions.inspectItem(item, classId, false, virtual, self)
		return false
	end
	
	if modules.client_options.getOption('classicControl') and
		((g_mouse.isPressed(MouseLeftButton) and mouseButton == MouseRightButton) or
		(g_mouse.isPressed(MouseRightButton) and mouseButton == MouseLeftButton)) then
		
		if modules.game_encyclopedia.m_ItemlistFunctions.isInspect() then
			local itemId = item:getId()
			local categoryId = item:getCategoryId()
			modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(itemId, categoryId)
		else
			modules.game_lookat.m_LookAtFunctions.inspectItem(item, classId, false, virtual, self)
		end
		
		self.cancelNextRelease = true
		return true
	elseif modules.game_interface.processMouseAction(mousePosition, mouseButton, nil, item, item, nil, nil, virtual, classId, self) then
		if self:getId() == 'slot10' and mouseButton == MouseRightButton then
			modules.game_inventory.stopBlink(7)
		end
		
		return true
	end
	
	return false
end

function UIItem:canAcceptDrop(widget, mousePos)
	if self:isVirtual() or not self:isDraggable() then
		return false
	end
	
	if not widget or not widget.currentDragThing then
		return false
	end
	
	local children = rootWidget:recursiveGetChildrenByPos(mousePos)
	for i = 1, #children do
		local child = children[i]
		if child == self then
			return true
		elseif not child:isPhantom() then
			return false
		end
	end

	error('Widget ' .. self:getId() .. ' not in drop list.')
	return false
end
