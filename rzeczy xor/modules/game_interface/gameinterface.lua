local area = {
				[-4] = {
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						},
				[-3] = {
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 3, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1}
						},
				[-2] = {
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 3, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1}
						},
				[-1] = {
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
						},
				[1] = {
						{3}
						},
				[2] = {
						{0, 1, 0},
						{1, 3, 1},
						{0, 1, 0}
						},
				[3] = {
						{1, 1, 1},
						{1, 3, 1},
						{1, 1, 1}
						},
				[4] = {
						{0, 0, 1, 0, 0},
						{0, 1, 1, 1, 0},
						{1, 1, 3, 1, 1},
						{0, 1, 1, 1, 0},
						{0, 0, 1, 0, 0}
						},
				[5] = {
						{0, 1, 1, 1, 0},
						{1, 1, 1, 1, 1},
						{1, 1, 3, 1, 1},
						{1, 1, 1, 1, 1},
						{0, 1, 1, 1, 0}
						},
				[6] = {
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 3, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 0, 0, 0}
						},
				[7] = {
						{0, 0, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 3, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 0, 0}
						},
				[8] = {
						{0, 0, 0, 0, 1, 0, 0, 0, 0},
						{0, 0, 0, 1, 1, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 3, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 1, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0, 0}
						},
				[9] = {
						{0, 0, 0, 1, 1, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 3, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 1, 1, 0, 0, 0}
						},
				[10] = {
						{0, 1, 0},
						{0, 1, 0},
						{0, 1, 0},
						{1, 1, 1},
						{1, 1, 1},
						{1, 1, 1},
						{0, 3, 0}
						},
				[11] = {
						{1, 1, 1},
						{0, 3, 0}
						},
				[12] = {
						{1},
						{1},
						{1},
						{1},
						{1},
						{3}
						},
				[13] = {
						{1, 1, 1},
						{1, 1, 1},
						{1, 1, 1},
						{1, 1, 1},
						{1, 1, 1},
						{1, 1, 1},
						{0, 3, 0}
						},
				[14] = {
						{1, 1, 1},
						{1, 3, 1},
						{1, 1, 1}
						},
				[15] = {
						{1},
						{3}
						},
				[16] = {
						{1},
						{1},
						{3}
						},
				[17] = {
						{1},
						{1},
						{1},
						{3}
						},
				[18] = {
						{0, 0, 1, 0, 0},
						{0, 1, 1, 1, 0},
						{1, 1, 3, 1, 1},
						{0, 1, 1, 1, 0},
						{0, 0, 1, 0, 0}
						},
				[19] = {
						{0, 1, 1, 1, 0},
						{1, 1, 1, 1, 1},
						{1, 1, 3, 1, 1},
						{1, 1, 1, 1, 1},
						{0, 1, 1, 1, 0}
						},
				[20] = {
						{0, 0, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 3, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 0, 0}
						},
				[21] = {
						{0, 0, 0, 1, 1, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 3, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 1, 1, 0, 0, 0}
						},
				[22] = {
						{1, 0, 1},
						{1, 3, 1},
						{1, 1, 1}
						},
				[23] = {
						{0, 0, 0, 0, 1, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0, 0},
						{0, 1, 0, 1, 1, 1, 0, 0, 0},
						{1, 0, 1, 1, 3, 1, 1, 0, 1},
						{0, 0, 0, 1, 1, 1, 0, 1, 0},
						{0, 0, 0, 0, 1, 0, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0, 0}
						},
				[24] = {
						{0, 1, 0},
						{1, 3, 1},
						{0, 1, 0}
						},
				[25] = {
						{1, 1, 1},
						{1, 1, 1},
						{1, 1, 1},
						{0, 1, 0},
						{0, 1, 0},
						{0, 3, 0}
						},
				[26] = {
						{1, 1, 1},
						{1, 3, 1},
						},
				[27] = {
						{3},
						},
				[28] = {
						{0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0},
						{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
						{0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0},
						},
				[29] = {
						{0, 1, 0},
						{1, 1, 1},
						{1, 1, 1},
						{0, 3, 0},
						},
				[30] = {
						{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
						{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}
						},
				[31] = {
						{1},
						{3},
						{1},
						},
				[32] = {
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{1},
						{3}
						},
				[33] = {
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
						},
				[34] = {
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 3, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 0, 0, 0},
						},
				[35] = {
						{1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1},
						{0, 1, 1, 1, 0},
						{0, 1, 1, 1, 0},
						{0, 1, 1, 1, 0},
						{0, 0, 1, 0, 0},
						{0, 0, 1, 0, 0},
						{0, 0, 1, 0, 0},
						{0, 0, 3, 0, 0},
						},
				[36] = {
						{0, 0, 1, 0, 0},
						{0, 0, 1, 0, 0},
						{1, 1, 3, 1, 1},
						{0, 0, 1, 0, 0},
						{0, 0, 1, 0, 0}
						},
				}

local ITEMACTION_LOOK = 0
local ITEMACTION_OPEN_CONTAINER = 1
local ITEMACTION_OPEN_CORPSE = 2
local ITEMACTION_OPEN_NEW_CONTAINER = 3
local ITEMACTION_MOVE_UP = 4
local ITEMACTION_MOVE_TO_MAGAZINE = 5
local ITEMACTION_MOVE_ITEMS_TO_MAGAZINE = 6
local ITEMACTION_GOTO_ITEM_LIST = 7
local ITEMACTION_ADD_LABEL = 8
local ITEMACTION_EDIT_LABEL = 9
local ITEMACTION_REMOVE_LABEL = 10
local ITEMACTION_ADD_TO_FAVORITES = 11
local ITEMACTION_REMOVE_FROM_FAVORITES = 12
local ITEMACTION_ADD_TO_LOOTLIST = 13
local ITEMACTION_REMOVE_FROM_LOOTLIST = 14
local ITEMACTION_REMOVE_TRACK_IGNORE = 15
local ITEMACTION_ADD_TRACK_IGNORE = 16
local ITEMACTION_CREATE_CHAT_WIDGET = 17
local ITEMACTION_USE = 18
local ITEMACTION_USE_WITH = 19
local ITEMACTION_TRADE_WITH = 20
local ITEMACTION_ROTATE = 21
local ITEMACTION_SMELT = 22
local ITEMACTION_REMOVE = 23

local currentArea = {0, 0, -1, false}

autoReverseMoving = false
blockingDiagonals = false
exitButton = nil
gameRootPanel = nil
gameMapPanel = nil

gameRightPanel = nil
gameNextRightPanel = nil
gameRightExpand = nil
gameRightHide = nil

gameLeftPanel = nil
gameNextLeftPanel = nil
gameLeftExpand = nil
gameLeftHide = nil

mouseGrabberWidget = nil
countWindow = nil
logoutWindow = nil
exitWindow = nil
limitedZoom = false
currentViewMode = 0
smartWalkDirs = {}
smartWalkDir = nil
walkFunction = nil
hookedMenuOptions = {}

local lastTile = nil
local lastTileList = {}

function addToPanels(self, previousContainer)
	self.onRemoveFromContainer = function(widget)
		if widget:getParent():getId() == 'gameRightPanel' then
			if gameNextRightPanel:isOn() and gameNextRightPanel:getEmptySpaceHeight() - widget:getHeight() >= 0 then
				widget:setParent(gameNextRightPanel)
			elseif gameNextLeftPanel:isOn() and gameNextLeftPanel:getEmptySpaceHeight() - widget:getHeight() >= 0 then
				widget:setParent(gameNextLeftPanel)
			elseif gameLeftPanel:getEmptySpaceHeight() - widget:getHeight() >= 0 then
				widget:setParent(gameLeftPanel)
			end
		elseif widget:getParent():getId() == 'gameNextRightPanel' then
			if gameNextLeftPanel:isOn() and gameNextLeftPanel:getEmptySpaceHeight() - widget:getHeight() >= 0 then
				widget:setParent(gameNextLeftPanel)
			elseif gameLeftPanel:getEmptySpaceHeight() - widget:getHeight() >= 0 then
				widget:setParent(gameLeftPanel)
			end
		elseif widget:getParent():getId() == 'gameNextLeftPanel' then
			if gameLeftPanel:getEmptySpaceHeight() - widget:getHeight() >= 0 then
				widget:setParent(gameLeftPanel)
			end
		end
	end

	if previousContainer then
		return false
	end
	
	if gameRightPanel:getEmptySpaceHeight() - self:getHeight() >= 0 then
		self:setParent(gameRightPanel)
	elseif gameNextRightPanel:isOn() and gameNextRightPanel:getEmptySpaceHeight() - self:getHeight() >= 0 then
		self:setParent(gameNextRightPanel)
	elseif gameNextLeftPanel:isOn() and gameNextLeftPanel:getEmptySpaceHeight() - self:getHeight() >= 0 then
		self:setParent(gameNextLeftPanel)
	else
		self:setParent(gameLeftPanel)
	end
end

local function unselectTiles()
	if lastTile then
		lastTile:unselect()
	end

	for _, pid in pairs(lastTileList) do
		pid:unselect()
	end
	
	lastTileList = {}
end

local function getDistanceBetween(fromPosition, toPosition)
	local x, y = math.abs(fromPosition.x - toPosition.x), math.abs(fromPosition.y - toPosition.y)
	local diff = math.max(x, y)
	if(fromPosition.z ~= toPosition.z) then
		diff = diff + 9 + 6
	end

	return diff
end

local function updateSelectionByDirection(tile)
	lastTile = tile
	
	local x, y, fromPos = 0, 0, tile:getPosition()
	local cache = area[currentArea[1]]
	for py = 1, #cache do
		for px = 1, #cache[py] do
			if cache[py][px] == 3 then
				x = px - 1
				y = py - 1
				break
			end
		end
	end
	
	local myPos = g_game.getLocalPlayer():getPosition()
	local dx = myPos.x - fromPos.x
	local dy = myPos.y - fromPos.y
	
	local tan = 10
	if dx ~= 0 then
		tan = dy / dx
	end
	
	local dir = South
	if math.abs(tan) < 1 then
		if dx > 0 then
			dir = West
		else
			dir = East
		end
	elseif dy > 0 then
		dir = North
	end
	
	local tiles = {myPos}
	if dir == North then
		for py = 1, #cache do
			for px = 1, #cache[py] do
				local var = cache[py][px]
				if var == 1 then
					local tilePos = {x = myPos.x + (px - x - 1), y = myPos.y + (py - y - 1), z = myPos.z}
					table.insert(tiles, tilePos)
				end
			end
		end
	elseif dir == South then
		for py = #cache, 1, -1 do
			for px = 1, #cache[py] do
				local var = cache[py][px]
				if var == 1 then
					local tilePos = {x = myPos.x + (px - x - 1), y = myPos.y - (py - y - 1), z = myPos.z}
					table.insert(tiles, tilePos)
				end
			end
		end
	elseif dir == West then
		for py = #cache, 1, -1 do
			for px = 1, #cache[py] do
				local var = cache[py][px]
				if var == 1 then
					local tilePos = {x = myPos.x + (py - y - 1), y = myPos.y - (px - x - 1), z = myPos.z}
					table.insert(tiles, tilePos)
				end
			end
		end
	elseif dir == East then
		for py = #cache, 1, -1 do
			for px = 1, #cache[py] do
				local var = cache[py][px]
				if var == 1 then
					local tilePos = {x = myPos.x - (py - y - 1), y = myPos.y - (px - x - 1), z = myPos.z}
					table.insert(tiles, tilePos)
				end
			end
		end
	end
	
	currentArea[3] = dir
	for _, pos in pairs(tiles) do
		local tile = g_map.getTile(pos)
		if tile then
			if tile:isWalkable() then
				if tile:hasCreature() then
					if g_game.isIdenticalPositions(myPos, pos) then
						tile:selectColor('#FF8C00')
					else
						tile:selectColor('#006AD1')
					end
				else
					tile:selectColor('#70DC70')
				end
			else
				if tile:hasCreature() then
					if g_game.isIdenticalPositions(myPos, pos) then
						tile:selectColor('#FF8C00')
					else
						tile:selectColor('#006AD1')
					end
				else
					tile:selectColor('#CD5C5C')
				end
			end

			tile:select()
			table.insert(lastTileList, tile)
		end
	end
end

local function updateSelection(tile)
	lastTile = tile
	
	local x, y, fromPos = 0, 0, tile:getPosition()
	local cache = area[currentArea[1]]
	for py = 1, #cache do
		for px = 1, #cache[py] do
			if cache[py][px] == 3 then
				x = px - 1
				y = py - 1
				break
			end
		end
	end
	
	local range = modules.game_character.m_SpellsFunctions.getSpellRange()
	if currentArea[4] or range == 1 or (currentArea[2] or 0) == 0 or currentArea[2] > range then
		range = currentArea[2]
	end
	
	local myPos = g_game.getLocalPlayer():getPosition()
	local canCast = getDistanceBetween(myPos, fromPos) <= range
	local canBuild = tile:isWalkable() and canCast
	if canBuild then
		if tile:hasCreature() and canCast then
			if g_game.isIdenticalPositions(myPos, fromPos) then
				tile:selectColor('#FF8C00')
			else
				tile:selectColor('#006AD1')
			end
		else
			tile:selectColor('#00FF00')
		end
	else
		if tile:hasCreature() and canCast then
			if g_game.isIdenticalPositions(myPos, fromPos) then
				tile:selectColor('#FF8C00')
			else
				tile:selectColor('#006AD1')
			end
		else
			tile:selectColor('#FF0000')
		end
	end
	
	for py = 1, #cache do
		for px = 1, #cache[py] do
			local var = cache[py][px]
			if var == 1 then
				local tilePos = {x = fromPos.x + (px - x - 1), y = fromPos.y + (py - y - 1), z = fromPos.z}
				local tile = g_map.getTile(tilePos)
				if tile then
					canBuild = tile:isWalkable() and getDistanceBetween(myPos, tilePos) <= range
					if canBuild then
						if tile:hasCreature() and canCast then
							tile:selectColor('#1E90FF')
						else
							tile:selectColor('#70DC70')
						end
					else
						if tile:hasCreature() and canCast then
							tile:selectColor('#1E90FF')
						else
							tile:selectColor('#CD5C5C')
						end
					end
					
					tile:select()
					table.insert(lastTileList, tile)
				end
			end
		end
	end
end

local function updatePosition(instant)
	if not selectedThing or currentArea[1] == 0 or not modules.game_character.m_SpellsFunctions.isShowingArea() then
		return false
	end

	local pos = g_window.getMousePosition()
	local tile = modules.game_interface.getMapPanel():getTile(pos)
	if not tile then
		return false
	end
	
	if not instant and lastTile == tile then
		return true
	end
	
	unselectTiles()
	
	if currentArea[2] and currentArea[2] > 0 then
		tile:select()
		updateSelection(tile)
	else
		updateSelectionByDirection(tile)
	end
end

function onPlayerPositionChange(localPlayer, newPos, oldPos)
	if oldPos and (newPos.z ~= oldPos.z or math.abs(newPos.x - oldPos.x) > 20 or math.abs(newPos.y - oldPos.y) > 20) then
		modules.game_bosshealth.onRemoveBossHealthBar()
	end
	
	updatePosition(true)
end

function onPositionChange(newPos, oldPos)
	updatePosition(true)
end

function init()
	-- g_ui.importStyle('styles/countwindow')

	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onLoginAdvice = onLoginAdvice
	}, true)
	
	connect(LocalPlayer, {
		onPositionChange = onPlayerPositionChange
	})
	
	connect(Creature, {
		onPositionChange = onPositionChange
	})

	gameRootPanel = g_ui.displayUI('gameinterface')
	gameRootPanel:hide()
	gameRootPanel:lower()
	gameRootPanel.onFocusChange = stopSmartWalk

	mouseGrabberWidget = gameRootPanel:getChildById('mouseGrabber')
	mouseGrabberWidget.onMouseRelease = onMouseGrabberRelease
	gameMapPanel = gameRootPanel:getChildById('gameMapPanel')

	gameRightPanel = gameRootPanel:getChildById('gameRightPanel')
	gameNextRightPanel = gameRootPanel:getChildById('gameNextRightPanel')
	gameRightExpand = gameRootPanel:getChildById('rightExpand')
	gameRightHide = gameRootPanel:getChildById('gameRightHide')
	
	gameLeftPanel = gameRootPanel:getChildById('gameLeftPanel')
	gameNextLeftPanel = gameRootPanel:getChildById('gameNextLeftPanel')
	gameLeftExpand = gameRootPanel:getChildById('leftExpand')
	gameLeftHide = gameRootPanel:getChildById('gameLeftHide')
	
	bindKeys()
	
	local id = tonumber(g_settings.get('rightPanel')) or 0
	if id > 0 then
		for i = 1, id do
			expandRightPanel(gameRightExpand)
		end
	end
	
	local id = tonumber(g_settings.get('leftPanel')) or 0
	if id > 0 then
		for i = 1, id do
			expandLeftPanel(gameLeftExpand)
		end
	end
	
	if g_game.isOnline() then
		if not exitButton then
			exitButton = modules.client_topmenu.addTopButton('ExitButton', 'exitButton', tr('Logout') .. ' (Ctrl+Q)', function() tryLogout(false) end, true)
		end
		show()
	end
end

function bindKeys()
	gameRootPanel:setAutoRepeatDelay(200)

	bindWalkKey('Up', North)
	bindWalkKey('Right', East)
	bindWalkKey('Down', South)
	bindWalkKey('Left', West)
	bindWalkKey('Numpad8', North)
	bindWalkKey('Numpad9', NorthEast)
	bindWalkKey('Numpad6', East)
	bindWalkKey('Numpad3', SouthEast)
	bindWalkKey('Numpad2', South)
	bindWalkKey('Numpad1', SouthWest)
	bindWalkKey('Numpad4', West)
	bindWalkKey('Numpad7', NorthWest)
	
	bindWalkKey("W", North)
	bindWalkKey("E", NorthEast)
	bindWalkKey("Q", NorthWest)
	bindWalkKey("S", South)
	bindWalkKey("C", SouthEast)
	bindWalkKey("Z", SouthWest)
	bindWalkKey("D", East)
	bindWalkKey("A", West)
	
	g_keyboard.bindKeyPress('Ctrl+Up', function() g_game.turn(North) changeWalkDir(North) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+Right', function() g_game.turn(East) changeWalkDir(East) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+Down', function() g_game.turn(South) changeWalkDir(South) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+Left', function() g_game.turn(West) changeWalkDir(West) end, gameRootPanel)
	
	g_keyboard.bindKeyPress('Ctrl+=', function() gameMapPanel:zoomIn() end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+-', function() gameMapPanel:zoomOut() end, gameRootPanel)
	
	g_keyboard.bindKeyPress('Ctrl+W', function() g_game.turn(North) changeWalkDir(North) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+D', function() g_game.turn(East) changeWalkDir(East) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+S', function() g_game.turn(South) changeWalkDir(South) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+A', function() g_game.turn(West) changeWalkDir(West) end, gameRootPanel)
	
	g_keyboard.bindKeyPress('Ctrl+Numpad8', function() g_game.turn(North) changeWalkDir(North) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+Numpad6', function() g_game.turn(East) changeWalkDir(East) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+Numpad2', function() g_game.turn(South) changeWalkDir(South) end, gameRootPanel)
	g_keyboard.bindKeyPress('Ctrl+Numpad4', function() g_game.turn(West) changeWalkDir(West) end, gameRootPanel)
	g_keyboard.bindKeyDown('Ctrl+Q', function() tryLogout(false) end, gameRootPanel)
	g_keyboard.bindKeyDown('Ctrl+W', function() g_map.cleanTexts() modules.game_textmessage.clearMessages() end, gameRootPanel)
end

function setReverseMoving(var)
	autoReverseMoving = var
end

function setBlockingDiagonal(var)
	blockingDiagonals = var
end

function getBlockingDiagonal()
	return blockingDiagonals
end

function bindWalkKey(key, dir)
	if key == 'W' or key == 'A' or key == 'S' or key == 'D' then
		g_keyboard.bindKeyDown(key, function() changeWalkDir(dir, false, 2) end, gameRootPanel, true)
		g_keyboard.bindKeyUp(key, function() changeWalkDir(dir, true, 2) end, gameRootPanel, true)
		g_keyboard.bindKeyPress(key, function() smartWalk(dir, 2) end, gameRootPanel)
	elseif key == 'Q' or key == 'E' or key == 'Z' or key == 'C' then
		g_keyboard.bindKeyDown(key, function() changeWalkDir(dir, false, 1) end, gameRootPanel, true)
		g_keyboard.bindKeyUp(key, function() changeWalkDir(dir, true, 1) end, gameRootPanel, true)
		g_keyboard.bindKeyPress(key, function() smartWalk(dir, 1) end, gameRootPanel)
	else
		g_keyboard.bindKeyDown(key, function() changeWalkDir(dir, false, 0) end, gameRootPanel, true)
		g_keyboard.bindKeyUp(key, function() changeWalkDir(dir, true, 0) end, gameRootPanel, true)
		g_keyboard.bindKeyPress(key, function() smartWalk(dir, 0) end, gameRootPanel)
	end
end

function unbindWalkKey(key)
	g_keyboard.unbindKeyDown(key, gameRootPanel)
	g_keyboard.unbindKeyUp(key, gameRootPanel)
	g_keyboard.unbindKeyPress(key, gameRootPanel)
end

function terminate()
	local id = 0
	if gameNextRightPanel:isOn() then
		id = 2
	elseif gameRightPanel:isOn() then
		id = 1
	end
	
	g_settings.set('rightPanel', id)

	local id = 0
	if gameNextLeftPanel:isOn() then
		id = 2
	elseif gameLeftPanel:isOn() then
		id = 1
	end
	
	g_settings.set('leftPanel', id)
	
	hide()

	hookedMenuOptions = {}

	stopSmartWalk()

	disconnect(LocalPlayer, {
		onPositionChange = onPlayerPositionChange
	})
	
	disconnect(Creature, {
		onPositionChange = onPositionChange
	})
	
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onLoginAdvice = onLoginAdvice
	})

	gameRootPanel:destroy()
end

function onGameStart()
	if not exitButton then
	exitButton = modules.client_topmenu.addTopButton('ExitButton', 'exitButton', tr('Logout') .. ' (Ctrl+Q)', function() tryLogout(false) end, true)
	end
	show()
end

function onGameEnd()
	if exitButton then
		exitButton:destroy()
		exitButton = nil
	end
	
	autoReverseMoving = false
	blockingDiagonals = false
	hide()
end

function show()
	connect(g_app, { onClose = tryExit })
	modules.client_background.hide()
	gameRootPanel:show()
	gameRootPanel:focus()
	gameMapPanel:followCreature(g_game.getLocalPlayer())

	addEvent(function()
		if not limitedZoom or g_game.isGM() then
			gameMapPanel:setMaxZoomOut(513)
			gameMapPanel:setLimitVisibleRange(false)
		else
			gameMapPanel:setMaxZoomOut(11)
			gameMapPanel:setLimitVisibleRange(true)
		end
	end)
end

function hide()
	disconnect(g_app, { onClose = tryExit })

	if logoutWindow then
		logoutWindow:destroy()
		logoutWindow = nil
	end
	if exitWindow then
		exitWindow:destroy()
		exitWindow = nil
	end
	if countWindow then
		countWindow:destroy()
		countWindow = nil
	end
	gameRootPanel:hide()
	modules.client_background.show()
end

function onLoginAdvice(message)
	displayInfoBox(tr("For Your Information"), message)
end

function forceExit()
	g_game.cancelLogin()
	scheduleEvent(exit, 10)
	return true
end

function tryExit()
	if exitWindow then
		return true
	end

	local exitFunc = function() g_game.safeLogout() forceExit() end
	local logoutFunc = function() g_game.safeLogout() exitWindow:destroy() exitWindow = nil end
	local cancelFunc = function() exitWindow:destroy() exitWindow = nil end

	exitWindow = displayGeneralBox(tr('Exit'), tr("If you shut down the program, your character might stay in the game.\nClick on 'Logout' to ensure that you character leaves the game properly.\nClick on 'Exit' if you want to exit the program without logging out your character."),
	{ { text=tr('Force Exit'), callback=exitFunc },
		{ text=tr('Logout'), callback=logoutFunc },
		{ text=tr('Cancel'), callback=cancelFunc },
		anchor=AnchorHorizontalCenter }, logoutFunc, cancelFunc)

	return true
end

function tryLogout(prompt)
	if type(prompt) ~= "boolean" then
		prompt = true
	end
	
	if not g_game.isOnline() then
		exit()
		return
	end

	if logoutWindow then
		return
	end

	local msg, yesCallback
	if not g_game.isConnectionOk() then
		msg = 'Your connection is failing, if you logout now your character will be still online, do you want to force logout?'

		yesCallback = function()
			g_game.forceLogout()
			if logoutWindow then
				logoutWindow:destroy()
				logoutWindow=nil
			end
		end
	else
		msg = 'Are you sure you want to logout?'

		yesCallback = function()
			g_game.safeLogout()
			if logoutWindow then
				logoutWindow:destroy()
				logoutWindow=nil
			end
		end
	end

	local noCallback = function()
		logoutWindow:destroy()
		logoutWindow=nil
	end

	if prompt then
		logoutWindow = displayGeneralBox(tr('Logout'), tr(msg), {
			{ text=tr('Yes'), callback=yesCallback },
			{ text=tr('No'), callback=noCallback },
			anchor=AnchorHorizontalCenter}, yesCallback, noCallback)
	else
		 yesCallback()
	end
end

function stopSmartWalk()
	smartWalkDirs = {}
	smartWalkDir = nil
end

function changeWalkDir(dir, pop, WASD)
	local player = g_game.getLocalPlayer()
	if player:getNoMove() then
		stopSmartWalk()
		return false
	end
	
	if WASD ~= 0 and not modules.game_console.canUseWASD() then
		return false
	elseif WASD == 1 and not modules.client_options.getOption('enableWASDDiagonals') then
		return false
	end
	
	while table.removevalue(smartWalkDirs, dir) do end
	if pop then
		if #smartWalkDirs == 0 then
			stopSmartWalk()
			return
		end
	else
		table.insert(smartWalkDirs, 1, dir)
	end
	
	smartWalkDir = smartWalkDirs[1]
	if not blockingDiagonals and #smartWalkDirs > 1 and modules.client_options.getOption('smartWalk') then
		for _,d in pairs(smartWalkDirs) do
			if (smartWalkDir == North and d == West) or (smartWalkDir == West and d == North) then
				smartWalkDir = NorthWest
				break
			elseif (smartWalkDir == North and d == East) or (smartWalkDir == East and d == North) then
				smartWalkDir = NorthEast
				break
			elseif (smartWalkDir == South and d == West) or (smartWalkDir == West and d == South) then
				smartWalkDir = SouthWest
				break
			elseif (smartWalkDir == South and d == East) or (smartWalkDir == East and d == South) then
				smartWalkDir = SouthEast
				break
			end
		end
	end
end

function getDirection(dire)
	if autoReverseMoving then
		if dire == North then
			dire = South
		elseif dire == East then
			dire = West
		elseif dire == West then
			dire = East
		elseif dire == South then
			dire = North
		elseif not blockingDiagonals then
			if dire == NorthEast then
				dire = SouthWest
			elseif dire == NorthWest then
				dire = SouthEast
			elseif dire == SouthEast then
				dire = NorthWest
			elseif dire == SouthWest then
				dire = NorthEast
			end
		end
	end
	
	return dire
end

function smartWalk(dir, WASD)
	local player = g_game.getLocalPlayer()
	if player:getNoMove() then
		stopSmartWalk()
		return false
	end
	
	if WASD ~= 0 and not modules.game_console.canUseWASD() then
		return false
	elseif WASD == 1 and not modules.client_options.getOption('enableWASDDiagonals') then
		return false
	end
	
	if g_keyboard.getModifiers() == KeyboardNoModifier then
		local func = walkFunction
		if not func then
			if modules.client_options.getOption('dashWalk') then
				func = g_game.dashWalk
			else
				func = g_game.walk
			end
		end
	
		local dire = smartWalkDir or dir
		func(getDirection(dire))
		return true
	end
	
	return false
end

function onMouseGrabberRelease(self, mousePosition, mouseButton)
	if selectedThing == nil then 
		return false
	end
	
	if mouseButton == MouseLeftButton then
		local clickedWidget = gameRootPanel:recursiveGetChildByPos(mousePosition, false)
		if clickedWidget then
			if selectedType == 'use' then
				onUseWith(clickedWidget, mousePosition)
			elseif selectedType == 'trade' then
				onTradeWith(clickedWidget, mousePosition)
			elseif selectedType == 'spell' then
				local range = modules.game_character.m_SpellsFunctions.getSpellRange()
				if range == 1 or (currentArea[2] or 0) == 0 or currentArea[2] > range then
					range = currentArea[2]
				end
				
				if lastTile and (range or 0) > 0 and getDistanceBetween(g_game.getLocalPlayer():getPosition(), lastTile:getPosition()) > range then
					return false
				end
				
				onUseSpell(clickedWidget, mousePosition)
			end
		end
	end

	unselectTiles()
	lastTile = nil
	currentArea = {0, 0, -1, false}
	
	selectedThing = nil
	g_mouse.popCursor('target')
	self:ungrabMouse()
	return true
end

function onUseSpell(clickedWidget, mousePosition)
	if clickedWidget:getClassName() == 'UIMap' then
		local tile = clickedWidget:getTile(mousePosition)
		if tile then
			selectedThing.target = nil
			selectedThing.pos = tile:getPosition()
			selectedThing.id = nil
			modules.game_spellbar.m_SpellBarFunctions.onClickSpell(selectedThing.name, selectedThing, currentArea[3])
		end
	elseif clickedWidget:getClassName() == 'UICreatureButton' then
		local creature = clickedWidget:getCreature()
		if creature then
			selectedThing.pos = nil
			selectedThing.target = creature
			selectedThing.id = nil
			modules.game_spellbar.m_SpellBarFunctions.onClickSpell(selectedThing.name, selectedThing, currentArea[3])
		end
	elseif clickedWidget:getClassName() == 'UIWidget' then
		local id = clickedWidget:getId()
		if id:find('partyMember') then
			id = id:explode('_')
			
			selectedThing.pos = nil
			selectedThing.target = nil
			selectedThing.id = id[2]
			
			modules.game_spellbar.m_SpellBarFunctions.onClickSpell(selectedThing.name, selectedThing, currentArea[3])
		end
	end
	
	unselectTiles()
	lastTile = nil
	currentArea = {0, 0, -1, false}
end

function onUseWith(clickedWidget, mousePosition)
	if clickedWidget:getClassName() == 'UIMap' then
		local tile = clickedWidget:getTile(mousePosition)
		if tile then
			if selectedThing:isMultiUse() then
				g_game.useWith(selectedThing, tile:getTopMultiUseThing())
			else
				g_game.useWith(selectedThing, tile:getTopUseThing())
			end
		end
	elseif clickedWidget:getClassName() == 'UIItem' and not clickedWidget:isVirtual() then
		g_game.useWith(selectedThing, clickedWidget:getItem())
	-- elseif clickedWidget.isBattleButton then
		-- local creature = clickedWidget:getChildById('creature'):getCreature()
		-- if creature then
			-- g_game.useWith(selectedThing, creature)
		-- end
	end
end

function onTradeWith(clickedWidget, mousePosition)
	if clickedWidget:getClassName() == 'UIMap' then
		local tile = clickedWidget:getTile(mousePosition)
		if tile then
			g_game.requestTrade(selectedThing, tile:getTopCreature())
		end
	end
end

function onMouseMove(self, mousePosition)
	if modules.game_character then
		modules.game_character.m_SpellsFunctions.updateSpellShadowPosition(mousePosition)
	end
end

function onMouseRelease(self, mousePosition, mouseButton)
	if modules.game_character then
		modules.game_character.m_SpellsFunctions.updateSpellShadowMouseRelease()
	end
end

function startUseWith(thing, isSpell, area, range, virtual, ignoreRange)
	if not isSpell and not thing then 
		return false
	end
	
	if not isSpell and thing and thing:isItem() and virtual then
		return false
	end
	
	if g_ui.isMouseGrabbed() then
		if selectedThing then
			selectedThing = thing
			if isSpell then
				selectedType = 'spell'
			else
				selectedType = 'use'
			end
		end
	
		if isSpell and area ~= 0 then
			currentArea = {area, range, -1, ignoreRange}
			updatePosition(true)
			mouseGrabberWidget.onMouseMove = updatePosition
		end
		
		return false
	end
	
	if isSpell then
		selectedType = 'spell'
	else
		selectedType = 'use'
	end
	
	selectedThing = thing
	mouseGrabberWidget:grabMouse()
	g_mouse.pushCursor('target')
	
	if isSpell and area ~= 0 then
		currentArea = {area, range, -1, ignoreRange}
		updatePosition()
		mouseGrabberWidget.onMouseMove = updatePosition
	end
end

function startTradeWith(thing)
	if not thing then
		return
	end
	
	if g_ui.isMouseGrabbed() then
		if selectedThing then
			selectedThing = thing
			selectedType = 'trade'
		end
		
		return
	end
	
	selectedType = 'trade'
	selectedThing = thing
	mouseGrabberWidget:grabMouse()
	g_mouse.pushCursor('target')
end

function isMenuHookCategoryEmpty(category)
	if category then
		for _,opt in pairs(category) do
			if opt then return false end
		end
	end
	
	return true
end

function addMenuHook(category, name, callback, condition, shortcut)
	if not hookedMenuOptions[category] then
		hookedMenuOptions[category] = {}
	end
	hookedMenuOptions[category][name] = {
										callback = callback,
										condition = condition,
										shortcut = shortcut
										}
end

function removeMenuHook(category, name)
	if not name then
		hookedMenuOptions[category] = {}
	else
		hookedMenuOptions[category][name] = nil
	end
end

function createThingMenu(menuPosition, lookThing, useThing, creatureThing, virtual, classId, self)
	if not g_game.isOnline() then 
		return true
	end
	
	local menu = g_ui.createWidget('PopupMenu')
	menu:setGameMenu(true)
	
	local classic = modules.client_options.getOption('classicControl')
	local shortcut = nil

	if not classic then
		shortcut = '(Shift)'
	else
		shortcut = nil
	end
	
	local list = {}
	if lookThing then
		if (lookThing:isItem() and lookThing:isPickupable()) or lookThing:isCreature() then
			table.insert(list, ITEMACTION_LOOK)
			
			if lookThing:isItem() then
				local categoryId = lookThing:getCategoryId()
				if categoryId ~= 0 then
					table.insert(list, ITEMACTION_GOTO_ITEM_LIST)
				end
				
				local itemId = lookThing:getId()
				if not virtual then
					if lookThing:getClassId() < ITEMCLASS_UNIQUE and categoryId == ITEMTYPE_CRAFTING_MATERIAL then
						table.insert(list, ITEMACTION_MOVE_TO_MAGAZINE)
					elseif categoryId == ITEMTYPE_BACKPACK then
						table.insert(list, ITEMACTION_MOVE_ITEMS_TO_MAGAZINE)
					end
					
					if lookThing:getClassId() ~= ITEMCLASS_QUEST then
						if lookThing:isInFavorites() then
							table.insert(list, ITEMACTION_REMOVE_FROM_FAVORITES)
						else
							table.insert(list, ITEMACTION_ADD_TO_FAVORITES)
						end
					end
					
					if modules.game_rewards.m_CraftingFunctions.isCraftingItem(itemId) then
						table.insert(list, ITEMACTION_SMELT)
					end
				end
				
				if modules.game_encyclopedia.m_BestiaryFunctions.isOnList(itemId) then
					table.insert(list, ITEMACTION_REMOVE_FROM_LOOTLIST)
				else
					table.insert(list, ITEMACTION_ADD_TO_LOOTLIST)
				end
				
				if modules.game_tracker.m_TrackerFunctions.isInIgnoreList(itemId) then
					table.insert(list, ITEMACTION_REMOVE_TRACK_IGNORE)
				else
					table.insert(list, ITEMACTION_ADD_TRACK_IGNORE)
				end
				
				if modules.game_console.m_ConsoleFunctions.enabledChat() then
					table.insert(list, ITEMACTION_CREATE_CHAT_WIDGET)
				end
			end
		end

		if not virtual and lookThing then
			if not lookThing:isCreature() and not lookThing:isNotMoveable() and lookThing:isPickupable() then
				table.insert(list, ITEMACTION_TRADE_WITH)
			end
			
			local parentContainer = lookThing:getParentContainer()
			if parentContainer and parentContainer:hasParent() then
				if lookThing:getId() == 12249 then
					table.insert(list, ITEMACTION_REMOVE)
				else
					table.insert(list, ITEMACTION_MOVE_UP)
				end
			end
		end
	end

	if not classic then
		shortcut = '(Ctrl)'
	else
		shortcut = nil
	end
	
	local position = nil
	if not virtual then
		if useThing then
			local tile = useThing:getTile()
			if tile then
				for _, v in pairs(tile:getItems()) do
					if v:isContainer() then
						useThing = v
						position = tile:getPosition()
						table.insert(list, ITEMACTION_OPEN_CORPSE)
						break
					end
				end
			end
			
			if useThing:isContainer() then
				if not isInArray(list, ITEMACTION_OPEN_CORPSE) then
					table.insert(list, ITEMACTION_OPEN_CONTAINER)
				end
				
				if useThing:getParentContainer() then
					table.insert(list, ITEMACTION_OPEN_NEW_CONTAINER)
				end
			else
				if useThing:isMultiUse() then
					table.insert(list, ITEMACTION_USE_WITH)
				else
					table.insert(list, ITEMACTION_USE)
				end
			end
			
			if useThing:isLabelable() then
				if useThing:getLabelColor() == 0 then
					table.insert(list, ITEMACTION_ADD_LABEL)
				else
					table.insert(list, ITEMACTION_REMOVE_LABEL)
					table.insert(list, ITEMACTION_EDIT_LABEL)
				end
			end
			
			if useThing:isRotateable() then
				table.insert(list, ITEMACTION_ROTATE)
			end
		end
	end
	
	table.sort(list)
	local lastId = -1
	for i = 1, #list do
		local id = list[i]
		local itemId = lookThing and lookThing:getId()
		if id == ITEMACTION_LOOK then
			menu:addOption(tr('Look'), function() 
				if lookThing:isItem() and modules.game_encyclopedia.m_ItemlistFunctions.isInspect() then
					local categoryId = lookThing:getCategoryId()
					modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(itemId, categoryId)
				else
					modules.game_lookat.m_LookAtFunctions.inspectItem(lookThing, classId, false, lookThing:isItem() and virtual, self)
				end
			end)
		end
		
		if id == ITEMACTION_OPEN_CORPSE and position then
			menu:addOption(tr('Open Container'), function() g_game.openCorpse(position) end)
		end
		
		if id == ITEMACTION_OPEN_CONTAINER then
			menu:addOption(tr('Open'), function() 
				local it = modules.game_lookat.m_LookAtFunctions.getItemByClientId(tonumber(useThing:getId()))
				if it and it.attributes.isCorpse then
					g_game.open(useThing)
				else
					g_game.open(useThing, useThing:getParentContainer())
				end
			end)
		end
		
		if id == ITEMACTION_OPEN_NEW_CONTAINER then
			menu:addOption(tr('Open in new window'), function() g_game.open(useThing) end)
		end
		
		if id == ITEMACTION_MOVE_UP then
			menu:addOption(tr('Move up'), function() g_game.moveToParentContainer(lookThing, lookThing:getCount()) end)
		end
		
		if id == ITEMACTION_REMOVE then
			menu:addOption(tr('Remove'), function() modules.game_rewards.m_CraftingFunctions.smeltItem(lookThing) end)
		end
		
		if id == ITEMACTION_MOVE_TO_MAGAZINE or id == ITEMACTION_MOVE_ITEMS_TO_MAGAZINE then
			if lastId > -1 and lastId < ITEMACTION_MOVE_TO_MAGAZINE then
				menu:addSeparator()
			end
			
			if id == ITEMACTION_MOVE_TO_MAGAZINE then
				menu:addOption(tr('Move to magazine'), function() modules.game_rewards.m_MagazineFunctions.moveItem(lookThing, 2) end)
			else
				menu:addOption(tr('Move materials from backpack to magazine'), function() modules.game_rewards.m_MagazineFunctions.moveItem(lookThing, 2) end)
			end
		end
		
		if id == ITEMACTION_GOTO_ITEM_LIST then
			if lastId > -1 and lastId < ITEMACTION_MOVE_TO_MAGAZINE then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Goto item list'), function() modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(itemId, lookThing:getCategoryId()) end)
		end
		
		if id == ITEMACTION_ADD_LABEL then
			if lastId > -1 and lastId < ITEMACTION_ADD_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Add label'), function() modules.game_containers.onCreateLabel(useThing) end)
		end
		
		if id == ITEMACTION_REMOVE_LABEL then
			if lastId > -1 and lastId < ITEMACTION_ADD_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Remove label'), function() modules.game_containers.onRemoveLabel(useThing) end)
		end
		
		if id == ITEMACTION_EDIT_LABEL then
			if lastId > -1 and lastId < ITEMACTION_ADD_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Edit label'), function() modules.game_containers.onCreateLabel(useThing) end)
		end
		
		if id == ITEMACTION_ADD_TO_FAVORITES then
			if lastId > -1 and lastId < ITEMACTION_EDIT_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Add to favorites'), function() modules.game_rewards.m_MagazineFunctions.moveItem(lookThing, 4, true) end)
		elseif id == ITEMACTION_REMOVE_FROM_FAVORITES then
			if lastId > -1 and lastId < ITEMACTION_EDIT_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Remove from favorites'), function() modules.game_rewards.m_MagazineFunctions.moveItem(lookThing, 4, false) end)
		end
		
		if id == ITEMACTION_ADD_TO_LOOTLIST then
			if lastId > -1 and lastId < ITEMACTION_EDIT_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Add to loot list'), function() modules.game_encyclopedia.m_BestiaryFunctions.sendItemToList(itemId, true) end)
		elseif id == ITEMACTION_REMOVE_FROM_LOOTLIST then
			if lastId > -1 and lastId < ITEMACTION_EDIT_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Remove from loot list'), function() modules.game_encyclopedia.m_BestiaryFunctions.sendItemToList(itemId, false) end)
		end
		
		if id == ITEMACTION_REMOVE_TRACK_IGNORE then
			if lastId > -1 and lastId < ITEMACTION_EDIT_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Remove from ignore list'), function() return modules.game_tracker.m_TrackerFunctions.removeFromIgnoreList(itemId) end)
		elseif id == ITEMACTION_ADD_TRACK_IGNORE then
			if lastId > -1 and lastId < ITEMACTION_EDIT_LABEL then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Add to ignore list'), function() return modules.game_tracker.m_TrackerFunctions.addToIgnoreList(itemId) end)
		end
		
		if id == ITEMACTION_CREATE_CHAT_WIDGET then
			if lastId > -1 and lastId <= ITEMACTION_ADD_TRACK_IGNORE then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Create chat widget'), function() g_game.createLookWindow(lookThing) end)
		end
		
		if id == ITEMACTION_USE then
			if lastId > -1 and lastId < ITEMACTION_CREATE_CHAT_WIDGET then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Use'), function() 
				modules.client_destroy.m_DestroyFunctions.use(useThing)
				g_game.use(useThing)
			end)
		end
		
		if id == ITEMACTION_USE_WITH then
			menu:addOption(tr('Use with ...'), function() startUseWith(useThing) end)
		end
		
		if id == ITEMACTION_TRADE_WITH then
			menu:addOption(tr('Trade with ...'), function() startTradeWith(lookThing) end)
		end
		
		if id == ITEMACTION_ROTATE then
			if lastId > -1 and lastId < ITEMACTION_TRADE_WITH then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Rotate'), function() g_game.rotate(useThing) end)
		end
		
		if id == ITEMACTION_SMELT then
			if lastId > -1 and lastId < ITEMACTION_ROTATE then
				menu:addSeparator()
			end
			
			menu:addOption(tr('Smelt'), function() modules.game_rewards.m_CraftingFunctions.smeltItem(lookThing) end)
		end
		
		lastId = id
	end
	
	if creatureThing then
		local localPlayer = g_game.getLocalPlayer()
		menu:addSeparator()
		if creatureThing:isLocalPlayer() then
			menu:addOption(tr('Set Outfit'), function() g_game.requestOutfit() end)

			if creatureThing:isPartyMember() then
				if creatureThing:isPartyLeader() then
					if creatureThing:isPartySharedExperienceActive() then
						menu:addOption(tr('Disable Shared Experience'), function() g_game.partyShareExperience(false) end)
					else
						menu:addOption(tr('Enable Shared Experience'), function() g_game.partyShareExperience(true) end)
					end
				end
				
				menu:addOption(tr('Leave Party'), function() g_game.partyLeave() end)
			end
		else
			local localPosition = localPlayer:getPosition()
			if not classic then 
				shortcut = '(Alt)' 
			else 
				shortcut = nil
			end
		
			if creatureThing:getPosition().z == localPosition.z then
				if g_game.getAttackingCreature() ~= creatureThing then
					menu:addOption(tr('Attack'), function() g_game.attack(creatureThing) end, shortcut)
				else
					menu:addOption(tr('Stop Attack'), function() g_game.cancelAttack() end, shortcut)
				end

				if g_game.getFollowingCreature() ~= creatureThing then
					menu:addOption(tr('Follow'), function() g_game.follow(creatureThing) end)
				else
					menu:addOption(tr('Stop Follow'), function() g_game.cancelFollow() end)
				end
			end

			if creatureThing:isPlayer() then
				menu:addSeparator()
				local creatureName = creatureThing:getName()
				menu:addOption(tr('Message to %s', creatureName), function() g_game.openPrivateChannel(creatureName) end)
				if modules.game_console.m_ConsoleFunctions.getOwnPrivateTab() then
					menu:addOption(tr('Invite to private chat'), function() g_game.inviteToOwnChannel(creatureName) end)
					menu:addOption(tr('Exclude from private chat'), function() g_game.excludeFromOwnChannel(creatureName) end) -- [TODO] must be removed after message's popup labels been implemented
				end
				
				if not localPlayer:hasVip(creatureName) then
					menu:addOption(tr('Add to VIP list'), function() g_game.addVip(creatureName) end)
				end

				if modules.game_console.isIgnored(creatureName) then
					menu:addOption(tr('Unignore') .. ' ' .. creatureName, function() modules.game_console.m_ConsoleFunctions.removeIgnoredPlayer(creatureName) end)
				else
					menu:addOption(tr('Ignore') .. ' ' .. creatureName, function() modules.game_console.m_ConsoleFunctions.addIgnoredPlayer(creatureName) end)
				end

				local localPlayerShield = localPlayer:getShield()
				local creatureShield = creatureThing:getShield()
				
				if localPlayerShield ~= ShieldNone and creatureShield ~= ShieldNone then
					menu:addOption(tr('Add to Party panel'), function() g_game.partyPanelAdd(creatureThing:getId()) end)
				end
				
				if localPlayerShield == ShieldNone or localPlayerShield == ShieldWhiteBlue then
					if creatureShield == ShieldWhiteYellow then
						menu:addOption(tr('Join %s\'s Party', creatureThing:getName()), function() g_game.partyJoin(creatureThing:getId()) end)
					else
						menu:addOption(tr('Invite to Party'), function() g_game.partyInvite(creatureThing:getId()) end)
					end
				elseif localPlayerShield == ShieldWhiteYellow then
					if creatureShield == ShieldWhiteBlue then
						menu:addOption(tr('Revoke %s\'s Invitation', creatureThing:getName()), function() g_game.partyRevokeInvitation(creatureThing:getId()) end)
					end
				elseif localPlayerShield == ShieldYellow or localPlayerShield == ShieldYellowSharedExp or localPlayerShield == ShieldYellowNoSharedExpBlink or localPlayerShield == ShieldYellowNoSharedExp then
					if creatureShield == ShieldWhiteBlue then
						menu:addOption(tr('Revoke %s\'s Invitation', creatureThing:getName()), function() g_game.partyRevokeInvitation(creatureThing:getId()) end)
					elseif creatureShield == ShieldBlue or creatureShield == ShieldBlueSharedExp or creatureShield == ShieldBlueNoSharedExpBlink or creatureShield == ShieldBlueNoSharedExp then
						if localPlayer:isPartyLeader() then
							menu:addOption(tr('Kick %s from party', creatureThing:getName()), function() g_game.partyRevokeInvitation(creatureThing:getId()) end)
						end
						
						menu:addOption(tr('Pass Leadership to %s', creatureThing:getName()), function() g_game.partyPassLeadership(creatureThing:getId()) end)
					else
						menu:addOption(tr('Invite to Party'), function() g_game.partyInvite(creatureThing:getId()) end)
					end
				end
			elseif creatureThing:isMonster() then
				local name = modules.game_lookat.m_LookAtFunctions.getMonsterByDescription(creatureThing:getName())
				if name then
					if not modules.game_encyclopedia.m_BestiaryFunctions.isInTrackList(name) then
						menu:addOption(tr('Add to bestiary tracker'), function() modules.game_encyclopedia.m_BestiaryFunctions.setTrack(true, name) end)
					else
						menu:addOption(tr('Remove from bestiary tracker'), function() modules.game_encyclopedia.m_BestiaryFunctions.setTrack(false, name) end)
					end
				end
			end
		end

		if modules.game_ruleviolation.hasWindowAccess() and creatureThing:isPlayer() then
			menu:addSeparator()
			menu:addOption(tr('Rule Violation'), function() modules.game_ruleviolation.show(creatureThing:getName()) end)
		end

		menu:addSeparator()
		menu:addOption(tr('Copy Name'), function() g_window.setClipboardText(creatureThing:getName()) end)
	end

	-- hooked menu options
	for _,category in pairs(hookedMenuOptions) do
		if not isMenuHookCategoryEmpty(category) then
			menu:addSeparator()
			for name,opt in pairs(category) do
				if opt and opt.condition(menuPosition, lookThing, useThing, creatureThing) then
					menu:addOption(name, function() opt.callback(menuPosition, lookThing, useThing, creatureThing) end, opt.shortcut)
				end
			end
		end
	end

	menu:display(menuPosition)
end

function processMouseAction(menuPosition, mouseButton, autoWalkPos, lookThing, useThing, creatureThing, attackCreature, virtual, classId, self)
	local player = g_game.getLocalPlayer()
	local keyboardModifiers = g_keyboard.getModifiers()

	if not modules.client_options.getOption('classicControl') then
		if keyboardModifiers == KeyboardNoModifier and mouseButton == MouseRightButton then
			createThingMenu(menuPosition, lookThing, useThing, creatureThing, virtual, classId, self)
			return true
		elseif lookThing and keyboardModifiers == KeyboardShiftModifier and mouseButton == MouseLeftButton then
			if lookThing:isItem() and modules.game_encyclopedia.m_ItemlistFunctions.isInspect() then
				local itemId = lookThing:getId()
				local categoryId = lookThing:getCategoryId()
				modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(itemId, categoryId)
			else
				modules.game_lookat.m_LookAtFunctions.inspectItem(lookThing, classId, false, virtual, self)
			end
			
			return true
		elseif not virtual and useThing and keyboardModifiers == KeyboardCtrlModifier and (mouseButton == MouseLeftButton or mouseButton == MouseRightButton) then
			if useThing:isContainer() then
				local it = modules.game_lookat.m_LookAtFunctions.getItemByClientId(tonumber(useThing:getId()))
				if it and it.attributes.isCorpse then
					g_game.open(useThing)
				elseif useThing:getParentContainer() then
					g_game.open(useThing, useThing:getParentContainer())
				else
					g_game.open(useThing)
				end
				return true
			elseif useThing:isMultiUse() then
				startUseWith(useThing, false, false, false, virtual)
				return true
			else
				modules.client_destroy.m_DestroyFunctions.use(useThing)
				g_game.use(useThing)
				return true
			end
			
			return true
		elseif attackCreature and g_keyboard.isShiftPressed() and mouseButton == MouseRightButton then
			g_game.follow(attackCreature)
			return true
		elseif attackCreature and g_keyboard.isAltPressed() and (mouseButton == MouseLeftButton or mouseButton == MouseRightButton) then
			g_game.attack(attackCreature)
			return true
		elseif creatureThing and creatureThing:getPosition().z == autoWalkPos.z and g_keyboard.isAltPressed() and (mouseButton == MouseLeftButton or mouseButton == MouseRightButton) then
			g_game.attack(creatureThing)
			return true
		end

	-- classic control
	else
		if useThing and keyboardModifiers == KeyboardNoModifier and mouseButton == MouseRightButton and not g_mouse.isPressed(MouseLeftButton) then
			if attackCreature and attackCreature ~= player then
				g_game.attack(attackCreature)
				return true
			elseif creatureThing and creatureThing ~= player and creatureThing:getPosition().z == autoWalkPos.z then
				g_game.attack(creatureThing)
				return true
			elseif useThing:isContainer() then
				local it = modules.game_lookat.m_LookAtFunctions.getItemByClientId(tonumber(useThing:getId()))
				if it and it.attributes.isCorpse then
					g_game.open(useThing)
				elseif useThing:getParentContainer() then
					g_game.open(useThing, useThing:getParentContainer())
				else
					g_game.open(useThing)
				end
				
				return true
			elseif useThing:isMultiUse() then
				startUseWith(useThing, false, false, false, virtual)
				return true
			else
				modules.client_destroy.m_DestroyFunctions.use(useThing)
				g_game.use(useThing)
				return true
			end
			
			return true
		elseif lookThing and ((keyboardModifiers == KeyboardShiftModifier and mouseButton == MouseLeftButton) or
							(g_mouse.isPressed(MouseLeftButton) and mouseButton == MouseRightButton) or
							(g_mouse.isPressed(MouseRightButton) and mouseButton == MouseLeftButton))then
			if lookThing:isItem() and modules.game_encyclopedia.m_ItemlistFunctions.isInspect() then
				local itemId = lookThing:getId()
				local categoryId = lookThing:getCategoryId()
				modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(itemId, categoryId)
			else
				modules.game_lookat.m_LookAtFunctions.inspectItem(lookThing, classId, false, virtual, self)
			end
			
			return true
		elseif useThing and keyboardModifiers == KeyboardCtrlModifier and (mouseButton == MouseLeftButton or mouseButton == MouseRightButton) then
			createThingMenu(menuPosition, lookThing, useThing, creatureThing, virtual, classId, self)
			return true
		elseif creatureThing and g_keyboard.isShiftPressed() and mouseButton == MouseRightButton then
			modules.game_inventory.onSetChaseMode(ChaseOpponent)
			if not g_game.getAttackingCreature() then
				g_game.follow(creatureThing)
			end
			return true
		elseif attackCreature and g_keyboard.isAltPressed() and (mouseButton == MouseLeftButton or mouseButton == MouseRightButton) then
			g_game.attack(attackCreature)
			return true
		elseif creatureThing and creatureThing:getPosition().z == autoWalkPos.z and g_keyboard.isAltPressed() and (mouseButton == MouseLeftButton or mouseButton == MouseRightButton) then
			g_game.attack(creatureThing)
			return true
		end
	end
	
	if player:getNoMove() then
		return false
	end
 
	if not blockingDiagonals and autoWalkPos and keyboardModifiers == KeyboardNoModifier and mouseButton == MouseLeftButton then
		player:stopAutoWalk()
		player:autoWalk(autoWalkPos)
		return true
	end

	return false
end

function moveStackableItem(item, toPos)
	if countWindow then
		return true
	end
	
	if g_keyboard.isShiftPressed() then
		g_game.move(item, toPos, 1)
		return true
	end
	
	local value = modules.client_options.getOption('moveStacksOfItemsWithoutCtrl')
	if (value and not g_keyboard.isCtrlPressed()) or (not value and g_keyboard.isCtrlPressed()) then
		g_game.move(item, toPos, item:getCount())
		return true
	end
	
	local count = item:getCount()

	countWindow = g_ui.displayUI('CountWindow')
	local itembox = countWindow:getChildById('item')
	local scrollbar = countWindow:getChildById('countScrollBar')
	itembox:setItemId(item:getId())
	itembox:setItemCount(count)
	scrollbar:setMaximum(count)
	scrollbar:setMinimum(1)
	scrollbar:setValue(count)

	local spinbox = countWindow:getChildById('spinBox')
	spinbox:setMaximum(count)
	spinbox:setMinimum(0)
	spinbox:setValue(0)
	spinbox:hideButtons()
	spinbox:focus()
	spinbox.firstEdit = true

	local spinBoxValueChange = function(self, value)
		spinbox.firstEdit = false
		scrollbar:setValue(value)
	end
	spinbox.onValueChange = spinBoxValueChange

	local check = function()
		if spinbox.firstEdit then
			spinbox:setValue(spinbox:getMaximum())
			spinbox.firstEdit = false
		end
	end
	g_keyboard.bindKeyPress("Up", function() check() spinbox:up() end, spinbox)
	g_keyboard.bindKeyPress("Down", function() check() spinbox:down() end, spinbox)
	g_keyboard.bindKeyPress("Right", function() check() spinbox:up() end, spinbox)
	g_keyboard.bindKeyPress("Left", function() check() spinbox:down() end, spinbox)
	g_keyboard.bindKeyPress("PageUp", function() check() spinbox:setValue(spinbox:getValue() + 10) end, spinbox)
	g_keyboard.bindKeyPress("PageDown", function() check() spinbox:setValue(spinbox:getValue() - 10) end, spinbox)

	scrollbar.onValueChange = function(self, value)
		itembox:setItemCount(value)
		spinbox.onValueChange = nil
		spinbox:setValue(value)
		spinbox.onValueChange = spinBoxValueChange
	end

	local okButton = countWindow:getChildById('buttonOk')
	local moveFunc = function()
		g_game.move(item, toPos, itembox:getItemCount())
		okButton:getParent():destroy()
		countWindow = nil
	end
	local cancelButton = countWindow:getChildById('buttonCancel')
	local cancelFunc = function()
		cancelButton:getParent():destroy()
		countWindow = nil
	end

	countWindow.onEnter = moveFunc
	countWindow.onEscape = cancelFunc

	okButton.onClick = moveFunc
	cancelButton.onClick = cancelFunc
end

function getRootPanel()
	return gameRootPanel
end

function getRightPanel()
	return gameRightPanel
end

function getNextRightPanel()
	return gameNextRightPanel
end

function getLeftPanel()
	return gameLeftPanel
end

function getNextLeftPanel()
	return gameNextLeftPanel
end

function getMapPanel()
	return gameMapPanel
end

function limitZoom()
	limitedZoom = true
end

function expandRightPanel(self)
	if not gameRightPanel:isOn() then
		gameMapPanel:addAnchor(AnchorRight, 'gameRightPanel', AnchorLeft)
		gameRightPanel:setOn(true)
	elseif not gameNextRightPanel:isOn() then
		gameMapPanel:addAnchor(AnchorRight, 'gameNextRightPanel', AnchorLeft)
		gameNextRightPanel:setOn(true)
		self:hide()
	end
	
	gameRightHide:show()
end

function hideRightPanel(self)
	if gameNextRightPanel:isOn() then
		gameMapPanel:addAnchor(AnchorRight, 'gameRightPanel', AnchorLeft)
		gameNextRightPanel:setOn(false)
	elseif gameRightPanel:isOn() then
		gameMapPanel:addAnchor(AnchorRight, 'parent', AnchorRight)
		gameRightPanel:setOn(false)
		self:hide()
	end
	
	gameRightExpand:show()
end

function expandLeftPanel(self)
	if not gameLeftPanel:isOn() then
		gameMapPanel:addAnchor(AnchorLeft, 'gameLeftPanel', AnchorRight)
		gameLeftPanel:setOn(true)
	elseif not gameNextLeftPanel:isOn() then
		gameMapPanel:addAnchor(AnchorLeft, 'gameNextLeftPanel', AnchorRight)
		gameNextLeftPanel:setOn(true)
		self:hide()
	end
	
	gameLeftHide:show()
end

function hideLeftPanel(self)
	if gameNextLeftPanel:isOn() then
		gameMapPanel:addAnchor(AnchorLeft, 'gameLeftPanel', AnchorRight)
		gameNextLeftPanel:setOn(false)
	elseif gameLeftPanel:isOn() then
		gameMapPanel:addAnchor(AnchorLeft, 'parent', AnchorLeft)
		gameLeftPanel:setOn(false)
		self:hide()
	end
	
	gameLeftExpand:show()
end
