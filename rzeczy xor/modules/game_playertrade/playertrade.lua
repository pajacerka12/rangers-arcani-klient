local tradeWindow = nil

function init()
	connect(g_game, {
		onOwnTrade = onGameOwnTrade,
		onCounterTrade = onGameCounterTrade,
		onCloseTrade = onGameCloseTrade,
		onGameEnd = onGameCloseTrade
	})
end

function terminate()
	disconnect(g_game, {
		onOwnTrade = onGameOwnTrade,
		onCounterTrade = onGameCounterTrade,
		onCloseTrade = onGameCloseTrade,
		onGameEnd = onGameCloseTrade
	})

	if tradeWindow then
		tradeWindow:destroy()
	end
end

function createTrade()
	tradeWindow = g_ui.displayUI('tradewindow')
	tradeWindow.onClose = function()
		g_game.rejectTrade()
		tradeWindow:hide()
	end
end

function fillTrade(name, items, counter)
	if not tradeWindow then
		createTrade()
	end
	
	local tradeContainer
	local label
	if counter then
		tradeContainer = tradeWindow:getChildById('counterTradeContainer')
		label = tradeWindow:getChildById('counterTradeLabel')

		tradeWindow:getChildById('acceptButton'):enable()
	else
		tradeContainer = tradeWindow:getChildById('ownTradeContainer')
		label = tradeWindow:getChildById('ownTradeLabel')
	end
	
	label:setText(name)
	
	for index, item in ipairs(items) do
		local itemWidget = g_ui.createWidget('ContainerItem', tradeContainer)
		itemWidget:setItem(item)
		itemWidget:setVirtual(true)
		itemWidget:setMargin(0)
		itemWidget.classId = item:getClassId()
		updateItemQuality(itemWidget, item:getClassId())
	end
end

function onGameOwnTrade(name, items)
	fillTrade(name, items, false)
end

function onGameCounterTrade(name, items)
	fillTrade(name, items, true)
end

function onGameCloseTrade()
	if tradeWindow then
		tradeWindow:destroy()
		tradeWindow = nil
	end
end
