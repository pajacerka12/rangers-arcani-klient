function getHexFromDec(num)
	if num == 10 then
		return 'A'
	elseif num == 11 then
		return 'B'
	elseif num == 12 then
		return 'C'
	elseif num == 13 then
		return 'D'
	elseif num == 14 then
		return 'E'
	elseif num == 15 then
		return 'F'
	end
	return num
end

function decToHex(num)
	local hex = ''
	while(num > 16) do
		local rest = num % 16;
		if rest >= 10 and rest < 16 then
			hex = hex .. getHexFromDec(rest)
		else
			hex = hex .. tostring(rest)
		end
		num = num / 16
	end
	hex = tostring(getHexFromDec(math.floor(num)) .. hex)
	return hex:len() == 1 and ('0' .. hex) or hex
end