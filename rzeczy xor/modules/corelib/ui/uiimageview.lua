-- @docclass
UIImageView = extends(UIWidget)

function UIImageView.create()
	local imageView = UIImageView.internalCreate()
	imageView.zoom = 1
	imageView.minZoom = 1
	imageView.maxZoom = 3
	imageView:setClipping(true)
	return imageView
end

function UIImageView:getDefaultZoom()
	local width = self:getWidth()
	local height = self:getHeight()
	local textureWidth = self:getImageTextureWidth()
	local textureHeight = self:getImageTextureHeight()
	local zoomX = width / textureWidth
	local zoomY = height / textureHeight
	return math.min(zoomX, zoomY)
end

function UIImageView:getImagePosition(x, y)
	x = x or self:getWidth() / 2
	y = y or self:getHeight() / 2
	local offsetX = self:getImageOffsetX()
	local offsetY = self:getImageOffsetY()
	local posX = (x - offsetX) / self.zoom
	local posY = (y - offsetY) / self.zoom
	return posX, posY
end

function UIImageView:setImage(image)
	self:setImageSource(image)
	local zoom = self:getDefaultZoom()
	self:setZoom(zoom)
	self:center()
end

function UIImageView:setZoom(zoom, x, y)
	zoom = math.max(math.min(zoom, self.maxZoom), self.minZoom)
	local posX, posY = self:getImagePosition(x, y)
	local textureWidth = self:getImageTextureWidth()
	local textureHeight = self:getImageTextureHeight()
	local imageWidth = textureWidth * zoom
	local imageHeight = textureHeight * zoom
	self:setImageWidth(imageWidth)
	self:setImageHeight(imageHeight)
	self.zoom = zoom
	self:move(posX, posY, x, y)
	
	for _, pid in pairs(self:getChildren()) do
		if pid.width and pid.height then
			pid:setSize({width = math.ceil(pid.width * zoom), height = math.ceil(pid.height * zoom)})
			
			for _, cid in pairs(pid:getChildren()) do
				cid:setSize({width = math.ceil(cid.width * zoom), height = math.ceil(cid.height * zoom)})
			end
		end
	end
end

function UIImageView:zoomIn(x, y)
	local zoom = self.zoom * 1.1
	self:setZoom(zoom, x, y)
end

function UIImageView:zoomOut(x, y)
	local zoom = self.zoom / 1.1
	self:setZoom(zoom, x, y)
end

function UIImageView:center()
	self:move(self:getImageTextureWidth() / 2, self:getImageTextureHeight() / 2)
end

function UIImageView:move(x, y, centerX, centerY)
	x = math.max(math.min(x, self:getImageTextureWidth()), 0)
	y = math.max(math.min(y, self:getImageTextureHeight()), 0)
	local centerX = centerX or self:getWidth() / 2
	local centerY = centerY or self:getHeight() / 2
	local offsetX = centerX - x * self.zoom
	local offsetY = centerY - y * self.zoom
	
	local maxOffsetX = (self:getImageTextureWidth() * self.zoom) - self:getWidth()
	if offsetX < 0 and math.abs(offsetX) >= maxOffsetX then
		offsetX = -maxOffsetX
	elseif offsetX > 0 then
		offsetX = 0
	end
	
	local maxOffsetY = (self:getImageTextureHeight() * self.zoom) - self:getHeight()
	if offsetY < 0 and math.abs(offsetY) >= maxOffsetY then
		offsetY = -maxOffsetY
	elseif offsetY > 0 then
		offsetY = 0
	end
	
	self:setImageOffset({x = offsetX, y = offsetY})
	for _, pid in pairs(self:getChildren()) do
		if pid.marginLeft and pid.marginLeft ~= 0 then
			pid:setMarginLeft(math.ceil((pid.marginLeft or 0) * self.zoom) + offsetX)
		end
		
		if pid.marginTop and pid.marginTop ~= 0 then
			pid:setMarginTop(math.ceil((pid.marginTop or 0) * self.zoom) + offsetY)
		end
		
		if pid.marginRight and pid.marginRight ~= 0 then
			pid:setMarginRight(math.ceil((pid.marginRight or 0) * self.zoom) + offsetX)
		end
		
		if pid.marginBottom and pid.marginBottom ~= 0 then
			pid:setMarginBottom(math.ceil((pid.marginBottom or 0) * self.zoom) + offsetY)
		end
	end
end

function UIImageView:moves(offset)
	self:setImageOffset(offset)
	
	for _, pid in pairs(self:getChildren()) do
		if pid.marginLeft and pid.marginLeft ~= 0 then
			pid:setMarginLeft(math.ceil((pid.marginLeft or 0) * self.zoom) + offset.x)
		end
		
		if pid.marginTop and pid.marginTop ~= 0 then
			pid:setMarginTop(math.ceil((pid.marginTop or 0) * self.zoom) + offset.y)
		end
		
		if pid.marginRight and pid.marginRight ~= 0 then
			pid:setMarginRight(math.ceil((pid.marginRight or 0) * self.zoom) + offset.x)
		end
		
		if pid.marginBottom and pid.marginBottom ~= 0 then
			pid:setMarginBottom(math.ceil((pid.marginBottom or 0) * self.zoom) + offset.y)
		end
	end
end

function UIImageView:onDragEnter(pos)
	return true
end

function UIImageView:onDragMove(pos, moved)
	local posX, posY = self:getImagePosition()
	self:move(posX - moved.x / self.zoom, posY - moved.y / self.zoom)
	return true
end

function UIImageView:onDragLeave(widget, pos)
	return true
end

function UIImageView:onMouseWheel(mousePos, direction)
	local x = mousePos.x - self:getX()
	local y = mousePos.y - self:getY()
	if direction == MouseWheelUp then
		self:zoomIn(x, y)
	elseif direction == MouseWheelDown then
		self:zoomOut(x, y)
	end
end
