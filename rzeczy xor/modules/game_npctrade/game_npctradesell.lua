m_NpcTradeSellFunctions.onLoad = function()
	m_NpcTradeSellFunctions.showAllItems = tonumber(g_settings.get('showAllNPCTradeItems', 1)) == 1
end

m_NpcTradeSellFunctions.onUnload = function()
	
end

m_NpcTradeSellFunctions.onOpenNpcTrade = function(items)
	for key, item in pairs(items) do
		if item[6] > 0 then
			local newItem = {}
			newItem.ptr = item[1]
			newItem.name = item[2]
			newItem.class = item[3]
			newItem.weight = item[4] / 1000
			newItem.price = item[6]
			
			local widget = g_ui.createWidget('NPCItemBox', m_NpcTradeSellList.list)
			local item = widget:getChildById('item')
			item:setItem(newItem.ptr)
			item:setItemCount(1)
			
			widget:getChildById('price'):setText(newItem.price)
			widget:getChildById('weight'):setText(newItem.weight)
			widget:setText(newItem.name)
			widget:setId(newItem.ptr:getId())
			widget.var = newItem
		end
	end
	
	m_NpcTradeSellFunctions.refreshItems()
end

m_NpcTradeSellFunctions.onGameStart = function()
	
end

m_NpcTradeSellFunctions.destroy = function()
	if m_NpcTradeSellList.window then
		m_NpcTradeSellList.window:destroy()
		m_NpcTradeSellList = {}
	end
end

m_NpcTradeSellFunctions.selectItem = function(self, id)
	if self then
		local itemId = self.var.ptr:getId()
		local amount = m_NpcTradeFunctions.playerItems[itemId]
		if amount then
			local value = m_NpcTradeSellList.quantityScroll:getValue()
			if value == 0 then
				m_NpcTradeSellList.quantityScroll:setMinimum(1)
			end
			
			m_NpcTradeSellList.quantityScroll:setMaximum(amount)
			m_NpcTradeSellList.quantityScroll:setValue(amount)
			m_NpcTradeSellFunctions.updateDescription(self, amount)
		else
			self = nil
		end
	else
		m_NpcTradeSellList.quantityScroll:setMaximum(1)
	end
	
	m_NpcTradeSellList.button:setEnabled(self)
	m_NpcTradeSellList.quantityScroll:setEnabled(self)
end

m_NpcTradeSellFunctions.onSearchTextChange = function(self)
	local text = self:getText():lower()
	if text == '' then
		m_NpcTradeSellList.text = nil
		self:getChildById('mask'):show()
	else
		m_NpcTradeSellList.text = text
		self:getChildById('mask'):hide()
	end
	
	m_NpcTradeSellFunctions.refreshItems()
end

m_NpcTradeSellFunctions.onCheckChange = function(id)
	if id == 1 then
		m_NpcTradeSellFunctions.showAllItems = m_NpcTradeSellList.showAllItems:isChecked()
		g_settings.set('showAllNPCTradeItems', m_NpcTradeSellFunctions.showAllItems and 1 or 0)
	end
	
	m_NpcTradeSellFunctions.refreshItems()
end

m_NpcTradeSellFunctions.refreshItems = function()
	for _, pid in pairs(m_NpcTradeSellList.list:getChildren()) do
		local itemId = pid.var.ptr:getId()
		local amount = m_NpcTradeFunctions.playerItems[itemId]
		local visible = true
		if m_NpcTradeSellFunctions.showAllItems or amount then
			if amount then
				visible = true
			end
		else
			visible = false
		end
		
		if (m_NpcTradeSellFunctions.showAllItems or visible) and (not m_NpcTradeSellList.text or string.find(pid:getText():lower(), m_NpcTradeSellList.text)) then
			if amount and pid == m_NpcTradeFunctions.currentItem then
				m_NpcTradeSellList.button:setEnabled(true)
				m_NpcTradeSellList.quantityScroll:setEnabled(true)
				m_NpcTradeSellList.quantityScroll:setMaximum(amount)
			end
			
			pid:show()
			if amount and amount > 0 then
				pid:setText(pid.var.name .. ' (' .. amount .. ')')
			else
				pid:setText(pid.var.name)
			end
		else
			if pid == m_NpcTradeFunctions.currentItem then
				m_NpcTradeSellList.button:setEnabled(false)
				m_NpcTradeSellList.quantityScroll:setEnabled(false)
			end
			
			pid:hide()
		end
	end
end

m_NpcTradeSellFunctions.sell = function()
	if m_NpcTradeFunctions.currentItem then
		g_game.sellItem(m_NpcTradeFunctions.currentItem.var.ptr, m_NpcTradeSellList.quantityScroll:getValue(), true)
	end
end

m_NpcTradeSellFunctions.getSellQuantity = function(itemId)
	return m_NpcTradeFunctions.playerItems[itemId] or 0
end

m_NpcTradeSellFunctions.sellAll = function()
	for itemId, item in pairs(m_NpcTradeFunctions.playerItems) do
		local item = Item.create(itemId)
		local amount = m_NpcTradeSellFunctions.getSellQuantity(itemId)
		if amount > 0 then
			g_game.sellItem(item, amount, true)
		end
	end
end

m_NpcTradeSellFunctions.onQuantityValueChange = function(value)
	if m_NpcTradeFunctions.currentItem then
		m_NpcTradeSellFunctions.updateDescription(m_NpcTradeFunctions.currentItem, value)
	end
end

m_NpcTradeSellFunctions.updateDescription = function(self, value)
	m_NpcTradeSellList.price:setText(self.var.price * value)
	m_NpcTradeSellList.weight:setText(self.var.weight * value)
end

m_NpcTradeSellFunctions.open = function(parent)
	if m_NpcTradeSellList.window then
		m_NpcTradeSellFunctions.destroy()
	else
		m_NpcTradeSellList.window = g_ui.createWidget('NpcTradeSell', parent)
		m_NpcTradeSellList.list = m_NpcTradeSellList.window:getChildById('list')
		m_NpcTradeSellList.quantityScroll = m_NpcTradeSellList.window:getChildById('quantityScroll')
		m_NpcTradeSellList.showAllItems = m_NpcTradeSellList.window:getChildById('showAllItems')
		m_NpcTradeSellList.price = m_NpcTradeSellList.window:getChildById('price')
		m_NpcTradeSellList.weight = m_NpcTradeSellList.window:getChildById('weight')
		m_NpcTradeSellList.button = m_NpcTradeSellList.window:getChildById('button')
		m_NpcTradeSellList.searchText = m_NpcTradeSellList.window:getChildById('searchText')
		
		if m_NpcTradeSellFunctions.showAllItems then
			m_NpcTradeSellList.showAllItems:setChecked(m_NpcTradeSellFunctions.showAllItems)
		end
	end
end
