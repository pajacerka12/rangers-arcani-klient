

m_NpcTradeBuyFunctions.onLoad = function()
	connect(g_game, {
					
					
					})
end

m_NpcTradeBuyFunctions.onUnload = function()
	disconnect(g_game, {
						
						
						})
end

m_NpcTradeBuyFunctions.onOpenNpcTrade = function(items)
	for key, item in pairs(items) do
		if item[5] > 0 then
			local newItem = {}
			newItem.ptr = item[1]
			newItem.name = item[2]
			newItem.class = item[3]
			newItem.weight = item[4] / 1000
			newItem.price = item[5]
			newItem.craftId = item[7]
			newItem.subType = item[8]
			newItem.fluidSource = item[9]
			
			local widget = g_ui.createWidget('NPCItemBox', m_NpcTradeBuyList.list)
			local item = widget:getChildById('item')
			item:setItem(newItem.ptr)
			
			if newItem.subType == 0xFF then
				item:setItemCount(1)
			else
				item.subType = newItem.subType
				item.fluidSource = newItem.fluidSource
				item:setItemCount(newItem.fluidSource)
			end
			
			if newItem.craftId ~= 0 then
				local craftId = widget:getChildById('craftId')
				craftId:show()
				craftId:setItemId(newItem.craftId)
			end
			
			widget:getChildById('price'):setText(newItem.price)
			widget:getChildById('weight'):setText(newItem.weight)
			widget:setText(newItem.name)
			widget:setId(newItem.ptr:getId())
			widget.var = newItem
		end
	end
end

m_NpcTradeBuyFunctions.onSearchTextChange = function(self)
	local text = self:getText():lower()
	if text == '' then
		self:getChildById('mask'):show()
	else
		self:getChildById('mask'):hide()
	end
	
	for _, pid in pairs(m_NpcTradeBuyList.list:getChildren()) do
		if not string.find(pid:getText():lower(), text) then
			pid:hide()
		else
			pid:show()
		end
	end
end

m_NpcTradeBuyFunctions.onCheckChange = function(id)
	if id == 0 then
		m_NpcTradeBuyFunctions.buyWithBackpack = m_NpcTradeBuyList.buyWithBackpack:isChecked()
	elseif id == 1 then
		m_NpcTradeBuyFunctions.ignoreCapacity = m_NpcTradeBuyList.ignoreCapacity:isChecked()
	end
	
	m_NpcTradeFunctions.refreshItem()
end

m_NpcTradeBuyFunctions.selectItem = function(self, id)
	if self then
		local balance = m_NpcTradeFunctions.balanceStatus
		if balance < 20 + self.var.price then
			m_NpcTradeBuyList.buyWithBackpack:setEnabled(false)
			m_NpcTradeBuyList.buyWithBackpack:setChecked(false)
		else
			m_NpcTradeBuyList.buyWithBackpack:setEnabled(true)
			if m_NpcTradeBuyList.buyWithBackpack:isChecked() then
				balance = balance - 20
			end
		end
		
		local maximum = math.floor(balance / self.var.price)
		if self.var.ptr:isStackable() then
			maximum = math.min(1000, maximum)
		else
			maximum = math.min(20, maximum)
		end
		
		local capacity = m_NpcTradeFunctions.freeCapacity
		if m_NpcTradeBuyList.ignoreCapacity:isChecked() then
			capacity = 65535
		elseif m_NpcTradeBuyList.buyWithBackpack:isChecked() then
			capacity = math.max(0, capacity - 1.2)
		end
		
		maximum = math.min(maximum, math.floor(capacity / self.var.weight))
		m_NpcTradeBuyList.quantityScroll:setMaximum(maximum)
		
		local value = m_NpcTradeBuyList.quantityScroll:getValue()
		if value == 0 then
			m_NpcTradeBuyList.quantityScroll:setMinimum(1)
			m_NpcTradeBuyList.quantityScroll:setValue(1)
		end
		
		m_NpcTradeBuyFunctions.updateDescription(self, value)
		
		if maximum == 0 then
			self = nil
		end
	else
		m_NpcTradeBuyList.quantityScroll:setMaximum(1)
	end
	
	m_NpcTradeBuyList.button:setEnabled(self)
	m_NpcTradeBuyList.quantityScroll:setEnabled(self)
end

m_NpcTradeBuyFunctions.buy = function()
	if m_NpcTradeFunctions.currentItem then
		g_game.buyItem(m_NpcTradeFunctions.currentItem.var.ptr, m_NpcTradeBuyList.quantityScroll:getValue(), m_NpcTradeFunctions.currentItem.var.craftId, m_NpcTradeBuyList.ignoreCapacity:isChecked(), m_NpcTradeBuyList.buyWithBackpack:isChecked())
	end
end

m_NpcTradeBuyFunctions.updateDescription = function(self, value)
	local amount = self.var.price * value
	if m_NpcTradeBuyList.buyWithBackpack:isChecked() then
		amount = amount + 20
	end
	
	m_NpcTradeBuyList.price:setText(amount .. ' (' .. m_NpcTradeFunctions.balanceStatus - amount .. ')')
	m_NpcTradeBuyList.weight:setText(self.var.weight * value)
end

m_NpcTradeBuyFunctions.onQuantityValueChange = function(value)
	if m_NpcTradeFunctions.currentItem then
		m_NpcTradeBuyFunctions.updateDescription(m_NpcTradeFunctions.currentItem, value)
	end
end

m_NpcTradeBuyFunctions.onGameStart = function()
	
end

m_NpcTradeBuyFunctions.destroy = function()
	if m_NpcTradeBuyList.window then
		m_NpcTradeBuyList.window:destroy()
		m_NpcTradeBuyList = {}
	end
end

m_NpcTradeBuyFunctions.open = function(parent)
	if m_NpcTradeBuyList.window then
		m_NpcTradeBuyFunctions.destroy()
	else
		m_NpcTradeBuyList.window = g_ui.createWidget('NpcTradeBuy', parent)
		m_NpcTradeBuyList.list = m_NpcTradeBuyList.window:getChildById('list')
		m_NpcTradeBuyList.quantityScroll = m_NpcTradeBuyList.window:getChildById('quantityScroll')
		m_NpcTradeBuyList.buyWithBackpack = m_NpcTradeBuyList.window:getChildById('buyWithBackpack')
		m_NpcTradeBuyList.ignoreCapacity = m_NpcTradeBuyList.window:getChildById('ignoreCapacity')
		m_NpcTradeBuyList.price = m_NpcTradeBuyList.window:getChildById('price')
		m_NpcTradeBuyList.weight = m_NpcTradeBuyList.window:getChildById('weight')
		m_NpcTradeBuyList.button = m_NpcTradeBuyList.window:getChildById('button')
		m_NpcTradeBuyList.searchText = m_NpcTradeBuyList.window:getChildById('searchText')
		
		m_NpcTradeBuyList.buyWithBackpack:setChecked(m_NpcTradeBuyFunctions.buyWithBackpack)
		m_NpcTradeBuyList.ignoreCapacity:setChecked(m_NpcTradeBuyFunctions.ignoreCapacity)
	end
end
