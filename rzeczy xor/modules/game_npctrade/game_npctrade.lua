m_NpcTradeFunctions = {}

m_NpcTradeSellList = {}
m_NpcTradeSellFunctions = {}

m_NpcTradeBuyList = {}
m_NpcTradeBuyFunctions = {}

ID_BUY = 1
ID_SELL = 2

m_NpcTradeFunctions.destroySelect = function()
	if m_NpcTradeFunctions.selectId == ID_BUY then
		m_NpcTradeBuyFunctions.destroy()
	elseif m_NpcTradeFunctions.selectId == ID_SELL then
		m_NpcTradeSellFunctions.destroy()
	end
end

m_NpcTradeFunctions.select = function(self, id, ignore)
	id = tonumber(id)
	if not ignore and m_NpcTradeFunctions.selectId == id then
		return true
	end
	
	m_NpcTradeFunctions.currentItem = nil
	m_NpcTradeFunctions.destroySelect()
	m_NpcTradeFunctions.selectId = id
	if id == ID_BUY then
		m_NpcTradeBuyFunctions.open(m_NpcTradeFunctions.description)
		m_NpcTradeBuyFunctions.onOpenNpcTrade(m_NpcTradeFunctions.items)
	elseif id == ID_SELL then
		m_NpcTradeSellFunctions.open(m_NpcTradeFunctions.description)
		m_NpcTradeSellFunctions.onOpenNpcTrade(m_NpcTradeFunctions.items)
	end
	
	if m_NpcTradeFunctions.current then
		m_NpcTradeFunctions.current:setOn(false)
	end
	
	m_NpcTradeFunctions.current = self
	self:setOn(true)
end

m_NpcTradeFunctions.instantSelect = function(id)
	if id == ID_BUY then
		m_NpcTradeFunctions.select(m_NpcTradeFunctions.window:getChildById('1'), ID_BUY, true)
	elseif id == ID_SELL then
		m_NpcTradeFunctions.select(m_NpcTradeFunctions.window:getChildById('2'), ID_SELL, true)
	end
end

m_NpcTradeFunctions.selectById = function(id)
	if not m_NpcTradeFunctions.window then
		m_NpcTradeFunctions.create(true)
	end
	
	m_NpcTradeFunctions.instantSelect(id)
end

m_NpcTradeFunctions.create = function(ignore)
	if m_NpcTradeFunctions.window then
		m_NpcTradeFunctions.destroy()
	else
		m_NpcTradeFunctions.window = g_ui.displayUI('game_npctrade')
		m_NpcTradeFunctions.description = m_NpcTradeFunctions.window:getChildById('descriptionWindow')
		m_NpcTradeFunctions.balance = m_NpcTradeFunctions.window:getChildById('balance')
		
		if m_NpcTradeFunctions.balanceStatus then
			m_NpcTradeFunctions.balance:setText(g_game.getMilharNumber(m_NpcTradeFunctions.balanceStatus))
		end
		
		if not ignore then
			if m_NpcTradeFunctions.selectId then
				m_NpcTradeFunctions.instantSelect(m_NpcTradeFunctions.selectId)
			else
				m_NpcTradeFunctions.select(m_NpcTradeFunctions.window:getChildById('1'), ID_BUY)
			end
		end
	end
end

m_NpcTradeFunctions.onSearchTextChange = function(self)
	if m_NpcTradeFunctions.currentItem then
		m_NpcTradeFunctions.currentItem:getChildById('blink'):hide()
		m_NpcTradeFunctions.currentItem = nil
		
		if m_NpcTradeFunctions.selectId == ID_SELL then
			m_NpcTradeSellFunctions.selectItem(nil)
		elseif m_NpcTradeFunctions.selectId == ID_BUY then
			m_NpcTradeBuyFunctions.selectItem(nil)
		end
	end
	
	if m_NpcTradeFunctions.selectId == ID_SELL then
		m_NpcTradeSellFunctions.onSearchTextChange(self)
	elseif m_NpcTradeFunctions.selectId == ID_BUY then
		m_NpcTradeBuyFunctions.onSearchTextChange(self)
	end
end

m_NpcTradeFunctions.refreshItem = function()
	if m_NpcTradeFunctions.currentItem then
		if m_NpcTradeFunctions.selectId == ID_SELL then
			m_NpcTradeSellFunctions.selectItem(m_NpcTradeFunctions.currentItem)
		elseif m_NpcTradeFunctions.selectId == ID_BUY then
			m_NpcTradeBuyFunctions.selectItem(m_NpcTradeFunctions.currentItem)
		end
	end
end

m_NpcTradeFunctions.selectItem = function(self)
	if m_NpcTradeFunctions.currentItem then
		m_NpcTradeFunctions.currentItem:getChildById('blink'):hide()
	end
	
	m_NpcTradeFunctions.currentItem = self
	self:getChildById('blink'):show()
	m_NpcTradeFunctions.refreshItem()
end

m_NpcTradeFunctions.destroy = function()
	if m_NpcTradeFunctions.window then
		m_NpcTradeFunctions.destroySelect()
		
		m_NpcTradeFunctions.window:destroy()
		m_NpcTradeFunctions.window = nil
		
		m_NpcTradeFunctions.description = nil
		m_NpcTradeFunctions.balance = nil
		m_NpcTradeFunctions.current = nil
		m_NpcTradeFunctions.currentItem = nil
		
		modules.game_bottompanel.m_MainFunctions.close(WINDOW_CRATES)
	end
end

m_NpcTradeFunctions.onGameEnd = function()
	m_NpcTradeFunctions.destroy()
end

m_NpcTradeFunctions.onGameStart = function()
	m_NpcTradeSellFunctions.onGameStart()
	m_NpcTradeBuyFunctions.onGameStart()
end

m_NpcTradeFunctions.onOpenNpcTrade = function(items)
	m_NpcTradeFunctions.items = items
	m_NpcTradeFunctions.create()
end

m_NpcTradeFunctions.onCloseNpcTrade = function()
	m_NpcTradeFunctions.destroy()
end

m_NpcTradeFunctions.onPlayerGoods = function(money, items)
	m_NpcTradeFunctions.balanceStatus = money
	
	m_NpcTradeFunctions.playerItems = {}
	for _, item in pairs(items) do
		local id = item[1]:getId()
		if not m_NpcTradeFunctions.playerItems[id] then
			m_NpcTradeFunctions.playerItems[id] = item[2]
		else
			m_NpcTradeFunctions.playerItems[id] = m_NpcTradeFunctions.playerItems[id] + item[2]
		end
	end
	
	if m_NpcTradeFunctions.window then
		m_NpcTradeFunctions.balance:setText(g_game.getMilharNumber(money))
		m_NpcTradeFunctions.refreshItem()
		
		if m_NpcTradeFunctions.selectId == ID_SELL then
			m_NpcTradeSellFunctions.refreshItems()
		end
	end
end

m_NpcTradeFunctions.onFreeCapacityChange = function(localPlayer, freeCapacity, oldFreeCapacity)
	m_NpcTradeFunctions.freeCapacity = freeCapacity / 1000
	if m_NpcTradeFunctions.window then
		m_NpcTradeFunctions.refreshItem()
	end
end

m_NpcTradeFunctions.onInventoryChange = function(inventory, item, oldItem)
	if m_NpcTradeFunctions.window and m_NpcTradeFunctions.selectId == ID_SELL then
		m_NpcTradeSellFunctions.refreshItems()
	end
end

function onLoad()
	g_ui.importStyle('game_npctradebuy')
	g_ui.importStyle('game_npctradesell')
	
	m_NpcTradeSellFunctions.onLoad()
	m_NpcTradeBuyFunctions.onLoad()
	
	connect(g_game, {
					onGameEnd = m_NpcTradeFunctions.onGameEnd,
					onGameStart = m_NpcTradeFunctions.onGameStart,
					onOpenNpcTrade = m_NpcTradeFunctions.onOpenNpcTrade,
					onCloseNpcTrade = m_NpcTradeFunctions.onCloseNpcTrade,
					onPlayerGoods = m_NpcTradeFunctions.onPlayerGoods
					})
	
	connect(LocalPlayer, {
					onFreeCapacityChange = m_NpcTradeFunctions.onFreeCapacityChange,
					onInventoryChange = m_NpcTradeFunctions.onInventoryChange
					})
	
	if g_game.isOnline() then
		m_NpcTradeFunctions.freeCapacity = g_game.getLocalPlayer():getFreeCapacity() / 1000
	end
end

function onUnload()
	m_NpcTradeFunctions.destroy()
	
	m_NpcTradeSellFunctions.onUnload()
	m_NpcTradeBuyFunctions.onUnload()
	
	disconnect(g_game, {
					onGameEnd = m_NpcTradeFunctions.onGameEnd,
					onGameStart = m_NpcTradeFunctions.onGameStart,
					onOpenNpcTrade = m_NpcTradeFunctions.onOpenNpcTrade,
					onCloseNpcTrade = m_NpcTradeFunctions.onCloseNpcTrade,
					onPlayerGoods = m_NpcTradeFunctions.onPlayerGoods
					})
	
	disconnect(LocalPlayer, {
					onFreeCapacityChange = m_NpcTradeFunctions.onFreeCapacityChange,
					onInventoryChange = m_NpcTradeFunctions.onInventoryChange
					})
end