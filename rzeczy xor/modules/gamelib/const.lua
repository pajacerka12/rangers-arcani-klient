onLoginTip = 'To register an account on our server, just click on the \"character with plus\"\nin the upper left corner of the screen.'
onRegisterTip = 'After setting up an account, you will be transferred to the account manager\nwhere you will be able to create your new character.'
onAccountExist = 'An account with that name already exists, please choose another name.'
onCreateCharacterTip = 'We invite you to create a character. You can choose from three vocations. Each of them has a different style of play,\nhas its own range of skills. Choose the vocation that suits you best. See you in the game!'
onCharacterListTip = {'No one is able to explore all the secrets of Ranger\'s Arcani.', 'All windows in the game are movable.', 'Each key on the keyboard can be a hotkey.', 'There are six subprofessions in the game, and each of them has a set of unique skills.', 'Completing a part or a full set of weapons provides additional bonuses.', 'The monsters at Ranger\'s Arcani show different levels of aggression - some will attack you only when you pass right next to them.', 'In Ranger\'s Arcani, darkness prevails. Always have a good source of light with you.', 'The old canals at Cirith have been taken over and turned into an underground city by coryms.', 'Some spells in the game consume special items, such as skulls and soul stones.', 'A team of several people is constantly working on the development of the game.', 'Sharing the experience in the team works from the ratio of 1:2 player levels, but it is developed with a system of penalties and bonuses, depending on the level of the player.', 'Get a pickaxe. It will definitely be useful.', 'The creators of Ranger\'s Arcani love intricate puzzles - this world is full of them.'}

TOWN_NONE = 0
TOWN_CIRITH = 1
TOWN_CHAECK = 7
TOWN_ICE_ISLANDS = 8
TOWN_ARENA = 9
TOWN_MARAROKO = 10

ITEMCLASS_NONE = 0
ITEMCLASS_DESOLATE = 1
ITEMCLASS_DAMAGED = 2
ITEMCLASS_NORMAL = 3
ITEMCLASS_PERFECT = 4
ITEMCLASS_LEGENDARY = 5
ITEMCLASS_UNIQUE = 6
ITEMCLASS_QUEST = 7
ITEMCLASS_IGNORE = 8
ITEMCLASS_SHOP = 9

FLUID_EMPTY = 0x00
FLUID_BLUE = 0x01
FLUID_RED = 0x02
FLUID_BROWN = 0x03
FLUID_GREEN = 0x04
FLUID_YELLOW = 0x05
FLUID_WHITE = 0x06
FLUID_PURPLE = 0x07

FLUID_NONE = FLUID_EMPTY
FLUID_WATER = FLUID_BLUE
FLUID_BLOOD = FLUID_RED
FLUID_BEER = FLUID_BROWN
FLUID_SLIME = FLUID_GREEN
FLUID_LEMONADE = FLUID_YELLOW
FLUID_MILK = FLUID_WHITE
FLUID_MANA = FLUID_PURPLE

FLUID_LIFE = FLUID_RED + 8
FLUID_OIL = FLUID_BROWN + 8
FLUID_URINE = FLUID_YELLOW + 8
FLUID_COCONUTMILK = FLUID_WHITE + 8
FLUID_WINE = FLUID_PURPLE + 8

FLUID_MUD = FLUID_BROWN + 16
FLUID_FRUITJUICE = FLUID_YELLOW + 16
FLUID_TOXIC = FLUID_PURPLE + 16

FLUID_LAVA = FLUID_RED + 24
FLUID_RUM = FLUID_BROWN + 24
FLUID_SWAMP = FLUID_GREEN + 24

fluidMap = {
	[0] = FLUID_EMPTY,
	[1] = FLUID_BLUE,
	[2] = FLUID_RED,
	[3] = FLUID_BROWN,
	[4] = FLUID_GREEN,
	[5] = FLUID_YELLOW,
	[6] = FLUID_WHITE,
	[7] = FLUID_PURPLE
}

ITEM_NAME_COLOR = {
	['desolate'] = '#848482',
	['damaged'] = '#E5E5E5',
	['normal'] = '#32CD32',
	['perfect'] = '#1E90FF',
	['magic'] = '#FFFFFF',
	['legendary'] = '#A020F0',
	['unique'] = '#FFD700',
	['quest'] = '#F9000E',
	[ITEMCLASS_DESOLATE] = '#848482',
	[ITEMCLASS_DAMAGED] = '#E5E5E5',
	[ITEMCLASS_NORMAL] = '#32CD32',
	[ITEMCLASS_PERFECT] = '#1E90FF',
	[ITEMCLASS_LEGENDARY] = '#A020F0',
	[ITEMCLASS_QUEST] = '#F9000E',
	[ITEMCLASS_UNIQUE] = '#FFD700',
	[ITEMCLASS_SHOP] = '#00FFEF',
}

ITEM_BUFF_COLOR = {
	['Weak'] = '#E5E5E5',
	['Normal'] = '#32CD32',
	['Improved'] = '#1E90FF',
	['Magnificent'] = '#A020F0',
	['Divine'] = '#FFD700'
}

FLUID_NAME = {
	[FLUID_WATER] = 'water',
	[FLUID_BLOOD] = 'blood',
	[FLUID_BEER] = 'beer',
	[FLUID_SLIME] = 'slime',
	[FLUID_LEMONADE] = 'lemonade',
	[FLUID_MILK] = 'milk',
	[FLUID_MANA] = 'mana',
	[FLUID_LIFE] = 'life',
	[FLUID_OIL] = 'oil',
	[FLUID_URINE] = 'urine',
	[FLUID_COCONUTMILK] = 'coconut milk',
	[FLUID_WINE] = 'wine',
	[FLUID_MUD] = 'mud',
	[FLUID_FRUITJUICE] = 'fruit juice',
	[FLUID_TOXIC] = 'toxic',
	[FLUID_LAVA] = 'lava',
	[FLUID_RUM] = 'rum',
	[FLUID_SWAMP] = 'swamp'
}

WINDOW_ENCYCLOPEDIA = 0
WINDOW_QUESTLOG = 1
WINDOW_CRATES = 2
WINDOW_MARKET = 3
WINDOW_STORE = 4
WINDOW_STATS = 5

WEAPON_NONE = 0
WEAPON_TWOHAND = 1
WEAPON_ONEHAND = 2
WEAPON_SHIELD = 3
WEAPON_WAND = 4
WEAPON_AMMO = 5

WEAPONGROUP_NONE = 0
WEAPONGROUP_MELEE = 1
WEAPONGROUP_DISTANCE = 2
WEAPONGROUP_WAND = 3

AMMO_NONE = 0
AMMO_BOLT = 1
AMMO_ARROW = 2
AMMO_SPEAR = 3
AMMO_THROWINGSTAR = 4
AMMO_THROWINGKNIFE = 5
AMMO_STONE = 6
AMMO_SNOWBALL = 7

ITEMTYPE_NONE = 0
ITEMTYPE_NECKLACE = 1
ITEMTYPE_FIRST = ITEMTYPE_NECKLACE
ITEMTYPE_RING = 2
ITEMTYPE_ARMOR = 3
ITEMTYPE_ONEHANDED_MELEE = 4
ITEMTYPE_TWOHANDED_MELEE = 5
ITEMTYPE_BOW = 6
ITEMTYPE_CROSSBOW = 7
ITEMTYPE_SPEAR = 8
ITEMTYPE_AMMUNITION = 9
ITEMTYPE_LEGS = 10
ITEMTYPE_FEET = 11
ITEMTYPE_SHIELD = 12
ITEMTYPE_HELMET = 13
ITEMTYPE_BACKPACK = 14
ITEMTYPE_FOOD = 15
ITEMTYPE_WAND = 16
ITEMTYPE_POTION = 17
ITEMTYPE_CREATURE_PRODUCT = 18
ITEMTYPE_RUNE = 19
ITEMTYPE_VALUABLE = 20
ITEMTYPE_CRAFTING_MATERIAL = 21
ITEMTYPE_DOLL = 22
ITEMTYPE_TROPHY = 23
ITEMTYPE_TOOL = 24
ITEMTYPE_CONTAINERS = 25
ITEMTYPE_QUIVER = 26
ITEMTYPE_PREMIUM = 27
ITEMTYPE_LAST = ITEMTYPE_PREMIUM
ITEMTYPE_ALL = ITEMTYPE_LAST + 1

BLINK_BLACK_SCREEN = 1
BLINK_QUEST_LOG = 2
BLINK_OBTAINED_PERFECT = 3
BLINK_OBTAINED_LEGENDARY = 4
BLINK_COLOR_SCREEN = 5
BLINK_SPELL_POINTS = 6
BLINK_QUEST_ITEMS = 7
BLINK_LEVEL_UP = 8
BLINK_LEVEL_DOWN = 9
BLINK_OBTAINED_UNIQUE = 10
BLINK_OPEN_CONVERTER = 11
BLINK_CLOSE_CONVERTER = 12
BLINK_CRATE = 13

ITEM_CATEGORY = {
	[ITEMTYPE_NECKLACE] = 'Necklaces',
	[ITEMTYPE_RING] = 'Rings',
	[ITEMTYPE_ARMOR] = 'Armors',
	[ITEMTYPE_ONEHANDED_MELEE] = 'One Handed Melee Weapons',
	[ITEMTYPE_TWOHANDED_MELEE] = 'Two Handed Melee Weapons',
	[ITEMTYPE_BOW] = 'Bows',
	[ITEMTYPE_CROSSBOW] = 'Crossbows',
	[ITEMTYPE_SPEAR] = 'Spears',
	[ITEMTYPE_AMMUNITION] = 'Ammunitions',
	[ITEMTYPE_QUIVER] = 'Quivers',
	[ITEMTYPE_LEGS] = 'Legs',
	[ITEMTYPE_FEET] = 'Boots',
	[ITEMTYPE_SHIELD] = 'Shields',
	[ITEMTYPE_HELMET] = 'Helmets',
	[ITEMTYPE_BACKPACK] = 'Backpacks',
	[ITEMTYPE_FOOD] = 'Food',
	[ITEMTYPE_WAND] = 'Wands',
	[ITEMTYPE_POTION] = 'Potions',
	[ITEMTYPE_CREATURE_PRODUCT] = 'Creature Products',
	[ITEMTYPE_RUNE] = 'Runes',
	[ITEMTYPE_VALUABLE] = 'Valuables',
	[ITEMTYPE_CRAFTING_MATERIAL] = 'Crafting Materials',
	[ITEMTYPE_DOLL] = 'Dolls',
	[ITEMTYPE_TROPHY] = 'Trophies',
	[ITEMTYPE_TOOL] = 'Tools',
	[ITEMTYPE_CONTAINERS] = 'Fluid containers',
	[ITEMTYPE_ALL] = 'All items',
	
	['Necklaces'] = ITEMTYPE_NECKLACE,
	['Rings'] = ITEMTYPE_RING,
	['Armors'] = ITEMTYPE_ARMOR,
	['One Handed Melee Weapons'] = ITEMTYPE_ONEHANDED_MELEE,
	['Two Handed Melee Weapons'] = ITEMTYPE_TWOHANDED_MELEE,
	['Bows'] = ITEMTYPE_BOW,
	['Crossbows'] = ITEMTYPE_CROSSBOW,
	['Spears'] = ITEMTYPE_SPEAR,
	['Ammunitions'] = ITEMTYPE_AMMUNITION,
	['Quivers'] = ITEMTYPE_QUIVER,
	['Legs'] = ITEMTYPE_LEGS,
	['Boots'] = ITEMTYPE_FEET,
	['Shields'] = ITEMTYPE_SHIELD,
	['Helmets'] = ITEMTYPE_HELMET,
	['Backpacks'] = ITEMTYPE_BACKPACK,
	['Food'] = ITEMTYPE_FOOD,
	['Wands'] = ITEMTYPE_WAND,
	['Potions'] = ITEMTYPE_POTION,
	['Creature Products'] = ITEMTYPE_CREATURE_PRODUCT,
	['Runes'] = ITEMTYPE_RUNE,
	['Valuables'] = ITEMTYPE_VALUABLE,
	['Crafting Materials'] = ITEMTYPE_CRAFTING_MATERIAL,
	['Dolls'] = ITEMTYPE_DOLL,
	['Trophies'] = ITEMTYPE_TROPHY,
	['Tools'] = ITEMTYPE_TOOL,
	['Fluid containers'] = ITEMTYPE_CONTAINERS,
	['All'] = ITEMTYPE_ALL
}

OPCODE_PUT_TOWER = 0
OPCODE_UPDATE_TOWER = 1
OPCODE_REMOVE_TOWER = 2

SOUNDS_NONE = 0
SOUNDS_MARKET = 1
SOUNDS_BATTLE = 2
SOUNDS_LOGIN = 3
SOUNDS_GAME = 4

ACCOUNT_NONE = 0
ACCOUNT_CREATE = 1
ACCOUNT_CHARACTER = 2
ACCOUNT_DELETE = 3
ACCOUNT_PASSWORD = 4
ACCOUNT_RESET = 5

CONDITION_NONE = 0
CONDITION_POISON = 1
CONDITION_FIRE = 2
CONDITION_ENERGY = 3
CONDITION_PHYSICAL = 4
CONDITION_HASTE = 5
CONDITION_PARALYZE = 6
CONDITION_OUTFIT = 7
CONDITION_INVISIBLE = 8
CONDITION_LIGHT = 9
CONDITION_MANASHIELD = 10
CONDITION_DIRECTIONS = 11
CONDITION_DRUNK = 12
CONDITION_EXHAUST = 13
CONDITION_REGENERATION = 14
CONDITION_SOUL_NOVA = 15
CONDITION_DROWN = 16
CONDITION_MUTED = 17
CONDITION_ATTRIBUTES = 18
CONDITION_FREEZING = 19
CONDITION_DAZZLED = 20
CONDITION_CURSED = 21
CONDITION_PACIFIED = 22
CONDITION_GAMEMASTER = 23
CONDITION_HUNTING = 24
CONDITION_BLEED = 25
CONDITION_WIND = 26
CONDITION_TOXIC = 27
CONDITION_ATTACK_SPEED = 28
CONDITION_SPELLCOOLDOWN = 30
CONDITION_SPELLGROUPCOOLDOWN = 31
CONDITION_PUSH = 32
CONDITION_STUN = 33
CONDITION_FEAR = 34
CONDITION_SILENCE = 35
CONDITION_BLIND = 36
CONDITION_LAST = CONDITION_BLIND

SKILL_NONE = -1
SKILL_FIRST = 0
SKILL_STRENGTH = SKILL_FIRST
SKILL_DEXTERITY = 1
SKILL_CONDITION = 2
SKILL_WISDOM = 3
SKILL_VIGOR = 4 
SKILL_LAST = SKILL_VIGOR
SKILL__LEVEL = 5
SKILL__LAST = SKILL__LEVEL

MONSTER_DAMAGE = 0
MONSTER_DEFENSE = 1

MONSTER_NONE = 2
MONSTER_FIRST = 3
MONSTER_UNDERWATER = MONSTER_FIRST
MONSTER_ANIMAL = 4
MONSTER_UNDEAD = 5
MONSTER_HUMAN = 6
MONSTER_SERPENT = 7
MONSTER_DRAKEN = 8
MONSTER_LIZARD = 9
MONSTER_DRAGON = 10
MONSTER_BOSS = 11
MONSTER_MUTATED = 12
MONSTER_APE = 13
MONSTER_DJINN = 14
MONSTER_BONELORD = 15
MONSTER_MINOTAUR = 16
MONSTER_TROLL = 17
MONSTER_ORC = 18
MONSTER_GOBLIN = 19
MONSTER_GIANT = 20
MONSTER_ELF = 21
MONSTER_DWARF = 22
MONSTER_GEO_ELEMENTAL = 23
MONSTER_PYRO_ELEMENTAL = 24
MONSTER_CRYO_ELEMENTAL = 25
MONSTER_ELECTRO_ELEMENTAL = 26
MONSTER_DEMON = 27
MONSTER_ARACHNID = 28
MONSTER_WIZARD = 29
MONSTER_YUAN_TI = 30
MONSTER_VAMPIRE = 31
MONSTER_KYNOHEAD = 32
MONSTER_PIRATE = 33
MONSTER_PERVERTED = 34
MONSTER_RATATOSK = 35
MONSTER_LAST = MONSTER_RATATOSK
MONSTER_ALL = MONSTER_LAST + 1

ITEM_NONE = 0

ITEM_WEIGHT = ITEM_NONE + 1
ITEM_FLOAT_LAST = ITEM_WEIGHT

ITEM_SPEED = ITEM_FLOAT_LAST + 1
ITEM_FIRST = ITEM_SPEED
ITEM_ATTACK_SPEED = ITEM_FIRST + 1
ITEM_DOUBLE_HIT = ITEM_ATTACK_SPEED + 1
ITEM_CRITICAL = ITEM_DOUBLE_HIT + 1
ITEM_BLOCK_CRITICAL = ITEM_CRITICAL + 1
ITEM_CRITICAL_HIT_MULTIPLIER = ITEM_BLOCK_CRITICAL + 1
ITEM_REDUCE_CRITICAL_HIT = ITEM_CRITICAL_HIT_MULTIPLIER + 1
ITEM_FULL_IGNORE_ARMOR = ITEM_REDUCE_CRITICAL_HIT + 1
ITEM_COUNT_STOLEN_LIFE = 10
ITEM_COUNT_STOLEN_MANA = ITEM_COUNT_STOLEN_LIFE + 1
ITEM_ARMOR = ITEM_COUNT_STOLEN_MANA + 1
ITEM_DEFENSE = ITEM_ARMOR + 1
ITEM_MAGIC_ATTACK = ITEM_DEFENSE + 1
ITEM_LUCK = ITEM_MAGIC_ATTACK + 1
ITEM_DEATH_HIT = ITEM_LUCK + 1
ITEM_MIN_DAMAGE = ITEM_DEATH_HIT + 1
ITEM_MAX_DAMAGE = ITEM_MIN_DAMAGE + 1
ITEM_DISTANCE_DAMAGE = ITEM_MAX_DAMAGE + 1
ITEM_SHOOT_RANGE = 20
ITEM_SLOTS = ITEM_SHOOT_RANGE + 1
ITEM_SLOTS_EFFICIENCY = ITEM_SLOTS + 1
ITEM_MAXHEALTH = ITEM_SLOTS_EFFICIENCY + 1
ITEM_MAXHEALTH_PERCENT = ITEM_MAXHEALTH + 1
ITEM_HEALTH_REGENERATION = ITEM_MAXHEALTH_PERCENT + 1
ITEM_HEALTH_TICKS = ITEM_HEALTH_REGENERATION + 1
ITEM_MAXMANA = ITEM_HEALTH_TICKS + 1
ITEM_MAXMANA_PERCENT = ITEM_MAXMANA + 1
ITEM_MANA_REGENERATION = ITEM_MAXMANA_PERCENT + 1
ITEM_MANA_TICKS = 30
ITEM_MONSTER_DEFENSE = ITEM_MANA_TICKS + 1
ITEM_MONSTER_DAMAGE = ITEM_MONSTER_DEFENSE + 1
ITEM_ABSORB = ITEM_MONSTER_DAMAGE + 1
ITEM_BONUS_DAMAGE = ITEM_ABSORB + 1
ITEM_INCREMENT = ITEM_BONUS_DAMAGE + 1
ITEM_REFLECT = ITEM_INCREMENT + 1
ITEM_REFLECT_CHANCE = ITEM_REFLECT + 1
ITEM_CONDITION_TICKS = ITEM_REFLECT_CHANCE + 1
ITEM_CONDITION = ITEM_CONDITION_TICKS + 1
ITEM_SKILL = 40
ITEM_STAT = ITEM_SKILL + 1
ITEM_REGENERATION = ITEM_STAT + 1
ITEM_FOOD = ITEM_REGENERATION + 1
ITEM_REGEN_HEALTH = ITEM_FOOD + 1
ITEM_REGEN_MANA = ITEM_REGEN_HEALTH + 1
ITEM_SPENT_MANA = ITEM_REGEN_MANA + 1
ITEM_BREAK_CHANCE = ITEM_SPENT_MANA + 1
ITEM_SPELL_CHANCE = ITEM_BREAK_CHANCE + 1
ITEM_CHARGES = ITEM_SPELL_CHANCE + 1
ITEM_CONTAINER = 50
ITEM_LEVEL = ITEM_CONTAINER + 1
ITEM_QUEST_ITEM = ITEM_LEVEL + 1
ITEM_POTIONS = ITEM_QUEST_ITEM + 1
ITEM_DURATION = ITEM_POTIONS + 1
ITEM_DECAYTO = ITEM_DURATION + 1
ITEM_ROTATETO = ITEM_DECAYTO + 1
ITEM_MALETRANSFORMTO = ITEM_ROTATETO + 1
ITEM_FEMALETRANSFORMTO = ITEM_MALETRANSFORMTO + 1
ITEM_MAXTEXTLEN = ITEM_FEMALETRANSFORMTO + 1
ITEM_WRITEONCEITEMID = 60
ITEM_WORTH = ITEM_WRITEONCEITEMID + 1
ITEM_LEVELDOOR = ITEM_WORTH + 1
ITEM_LEGENDARY = ITEM_LEVELDOOR + 1
ITEM_TRANSFORMDEEQUIPTO = ITEM_LEGENDARY + 1
ITEM_TRANSFORMEQUIPTO = ITEM_TRANSFORMDEEQUIPTO + 1
ITEM_FAMILIAR = ITEM_TRANSFORMEQUIPTO + 1
ITEM_TRANSFORMTO = ITEM_FAMILIAR + 1
ITEM_TYPE = ITEM_TRANSFORMTO + 1
ITEM_MANASTEAL_MULTIPLIER = ITEM_TYPE + 1
ITEM_BUFF_STRENGTH = 70
ITEM_EXPERIENCE = ITEM_BUFF_STRENGTH + 1
ITEM_DECAYEFFECT = ITEM_EXPERIENCE + 1
ITEM_FIELD = ITEM_DECAYEFFECT + 1
ITEM_CONDITION_CHANCE = ITEM_FIELD + 1
ITEM_CONDITION_TICK = ITEM_CONDITION_CHANCE + 1
ITEM_CONDITION_DAMAGE = ITEM_CONDITION_TICK + 1
ITEM_CONDITION_COUNT = ITEM_CONDITION_DAMAGE + 1
ITEM_BARRER = ITEM_CONDITION_COUNT + 1
ITEM_HOUSE_PRICE = ITEM_BARRER + 1
ITEM_SHOP_ITEM = 80
ITEM_CRAFTING = ITEM_SHOP_ITEM + 1
ITEM_CRAFT_ID = ITEM_CRAFTING + 1
ITEM_HEALTH_PERCENT_REGENERATION = ITEM_CRAFT_ID + 1
ITEM_MANA_PERCENT_REGENERATION = ITEM_HEALTH_PERCENT_REGENERATION + 1
ITEM_HIT_CHANCE = ITEM_MANA_PERCENT_REGENERATION + 1
ITEM_LIGHT_COLOR = ITEM_HIT_CHANCE + 1
ITEM_LIGHT_RADIUS = ITEM_LIGHT_COLOR + 1
ITEM_MAGIC_ATTACK_OVER_TIME = ITEM_LIGHT_RADIUS + 1
ITEM_BLOCK_STUN = ITEM_MAGIC_ATTACK_OVER_TIME + 1
ITEM_DODGE_MELEE = 90
ITEM_BOXID = ITEM_DODGE_MELEE + 1
ITEM_DODGE_RANGE = ITEM_BOXID + 1
ITEM_RECOVERY_AMMUNITION = ITEM_DODGE_RANGE + 1
ITEM_INT_LAST = 100

ITEM_FORCESERIALIZE = ITEM_INT_LAST + 1
ITEM_STOPDURATION = ITEM_FORCESERIALIZE + 1
ITEM_SHOWDURATION = ITEM_STOPDURATION + 1
ITEM_REPLACEABLE = ITEM_SHOWDURATION + 1
ITEM_BLOCKPROJECTTILE = ITEM_REPLACEABLE + 1
ITEM_ALLOWPICKUPABLE = ITEM_BLOCKPROJECTTILE + 1
ITEM_WRITEABLE = ITEM_ALLOWPICKUPABLE + 1
ITEM_READABLE = ITEM_WRITEABLE + 1
ITEM_STACKABLE = ITEM_READABLE + 1
ITEM_SHOWCHARGES = 110
ITEM_MOVEABLE = ITEM_SHOWCHARGES + 1
ITEM_THROWANYWHERE = ITEM_MOVEABLE + 1
ITEM_SHOWCOUNT = ITEM_THROWANYWHERE + 1
ITEM_INVISIBILITY = ITEM_SHOWCOUNT + 1
ITEM_VALUABLE = ITEM_INVISIBILITY + 1
ITEM_CREATUREPRODUCT = ITEM_VALUABLE + 1
ITEM_MANASHIELD = ITEM_CREATUREPRODUCT + 1
ITEM_TRAP = ITEM_MANASHIELD + 1
ITEM_CRAFTABLE = ITEM_TRAP + 1
ITEM_PREVENT_DROP = 120
ITEM_INSCRIPTION = ITEM_PREVENT_DROP + 1
ITEM_CRAFTINGMATERIAL = ITEM_INSCRIPTION + 1
ITEM_INSPECT = ITEM_CRAFTINGMATERIAL + 1
ITEM_BOOL_LAST = 200

ITEM_FLUIDSOURCE = ITEM_BOOL_LAST + 1
ITEM_FLOORCHANGE = ITEM_FLUIDSOURCE + 1
ITEM_FIELDTYPE = ITEM_FLOORCHANGE + 1
ITEM_WEAPON_TYPE = ITEM_FIELDTYPE + 1
ITEM_WEAPON_GROUP = ITEM_WEAPON_TYPE + 1
ITEM_EXCEPTIONAL_WORD = ITEM_WEAPON_GROUP + 1
ITEM_SLOTS_PRIORITY = ITEM_EXCEPTIONAL_WORD + 1
ITEM_SLOT_TYPE = ITEM_SLOTS_PRIORITY + 1
ITEM_POTIONS_TYPE = ITEM_SLOT_TYPE + 1
ITEM_NAME = 210
ITEM_DESCRIPTION = ITEM_NAME + 1
ITEM_SPELL_NAME = ITEM_DESCRIPTION + 1
ITEM_RUNE_WORD = ITEM_SPELL_NAME + 1
ITEM_BUFF_CLASS = ITEM_RUNE_WORD + 1
ITEM_BUFF_TYPE = ITEM_BUFF_CLASS + 1
ITEM_LOOTLIST = ITEM_BUFF_TYPE + 1
ITEM_PARTNERDIRECTION = ITEM_LOOTLIST + 1
ITEM_AMMOACTION = ITEM_PARTNERDIRECTION + 1
ITEM_CORPSETYPE = ITEM_AMMOACTION + 1
ITEM_SHOOTTYPE = 220
ITEM_AMMOTYPE = ITEM_SHOOTTYPE + 1
ITEM_BLOODTYPE = ITEM_AMMOTYPE + 1
ITEM_COMBATTYPE = ITEM_BLOODTYPE + 1
ITEM_EFFECT = ITEM_COMBATTYPE + 1
ITEM_HOUSE_OWNER = ITEM_EFFECT + 1
ITEM_HOUSE_TYPE = ITEM_HOUSE_OWNER + 1
ITEM_HOUSE_NAME = ITEM_HOUSE_TYPE + 1
ITEM_LIGHT_SOURCE = ITEM_HOUSE_NAME + 1
ITEM_SPELL_ATTRIBUTES = ITEM_LIGHT_SOURCE + 1
ITEM_STR_LAST = 300

ITEM_BUFF_NAME = ITEM_STR_LAST + 1
ITEM_VOCATION = ITEM_BUFF_NAME + 1
ITEM_SUBTYPE = ITEM_VOCATION + 1
ITEM_SPLASH = ITEM_SUBTYPE + 1
ITEM_TEXT = ITEM_SPLASH + 1
ITEM_SPELL = ITEM_TEXT + 1
ITEM_SPELL_COUNT = ITEM_SPELL + 1
ITEM_KEY = ITEM_SPELL_COUNT + 1
ITEM_SET_BONUSLIST = ITEM_KEY + 1
ITEM_SET_ITEMLIST = 310
ITEM_SET_NAME = ITEM_SET_ITEMLIST + 1
ITEM_CLASS_NAME = ITEM_SET_NAME + 1
ITEM_CLASS_ITEM = ITEM_CLASS_NAME + 1
ITEM_POS = ITEM_CLASS_ITEM + 1
ITEM_ID = ITEM_POS + 1
ITEM_BONUS_ARMOR = ITEM_ID + 1
ITEM_IGNORE_MAGICDAMAGE = ITEM_BONUS_ARMOR + 1
ITEM_DECREASE_ARMOR = ITEM_IGNORE_MAGICDAMAGE + 1
ITEM_NO_MANA = ITEM_DECREASE_ARMOR + 1
ITEM_SPAWN_COIN = 320
ITEM_CLIENT_ID = ITEM_SPAWN_COIN + 1
ITEM_SUPPRESS = ITEM_CLIENT_ID + 1
ITEM_BONUS_DEFENSE = ITEM_SUPPRESS + 1
ITEM_RUNE_REROLLS = ITEM_BONUS_DEFENSE + 1
ITEM_RUNE_QUALITY = ITEM_RUNE_REROLLS + 1
ITEM_COUNT = ITEM_RUNE_QUALITY + 1
ITEM_PREVENT_DEATH = ITEM_COUNT + 1
ITEM_LAST = ITEM_PREVENT_DEATH
ITEM_ALL = 500

COMBAT_NONE = 0
COMBAT_PHYSICALDAMAGE = 1
COMBAT_FIRST = COMBAT_PHYSICALDAMAGE
COMBAT_UNDEFINEDDAMAGE = 2
COMBAT_LIFEDRAIN = 3
COMBAT_MANADRAIN = 4
COMBAT_HEALING = 5
COMBAT_DROWNDAMAGE = 6
COMBAT_BLEEDDAMAGE = 7
COMBAT_HOLYDAMAGE = 8
COMBAT_DEATHDAMAGE = 9
COMBAT_TOXICDAMAGE = 10
COMBAT_ENERGYDAMAGE = 11
COMBAT_EARTHDAMAGE = 12
COMBAT_FIREDAMAGE = 13
COMBAT_ICEDAMAGE = 14
COMBAT_WINDDAMAGE = 15
COMBAT_LAST = COMBAT_WINDDAMAGE

-- @docconsts @{
CombatFirst	= 0
CombatPhysicalDamage = 1
CombatUndefineddamage = 2
CombatLifedrain = 3
CombatManadrain = 4
CombatHealing = 5
CombatDrownDamage = 6
CombatBleedDamage = 7
CombatHolyDamage = 8
CombatDeathDamage = 9
CombatToxicDamage = 10
CombatEnergyDamage = 11
CombatEarthDamage = 12
CombatFireDamage = 13
CombatIceDamage = 14
CombatWindDamage = 15

MarginTop = 0
MarginBottom = 1
MarginLeft = 2
MarginRight = 3

FloorHigher = 0
FloorLower = 15

SkullNone = 0
SkullYellow = 1
SkullGreen = 2
SkullWhite = 3
SkullRed = 4
SkullBlack = 5
SkullGolden = 6
SkullSilver = 7
SkullBronze = 8
SkullPink = 9
SkullPurple = 10
SkullOrange = 11
SkullBlue = 12
SkullPumpkin = 13
SkullChristmas = 14

ShieldNone = 0
ShieldWhiteYellow = 1
ShieldWhiteBlue = 2
ShieldBlue = 3
ShieldYellow = 4
ShieldBlueSharedExp = 5
ShieldYellowSharedExp = 6
ShieldBlueNoSharedExpBlink = 7
ShieldYellowNoSharedExpBlink = 8
ShieldBlueNoSharedExp = 9
ShieldYellowNoSharedExp = 10

EmblemNone = 0
EmblemGreen = 1
EmblemRed = 2
EmblemBlue = 3

North = 0
East = 1
South = 2
West = 3
NorthEast = 4
SouthEast = 5
SouthWest = 6
NorthWest = 7

FightOffensive = 1
FightBalanced = 2
FightDefensive = 3

DontChase = 0
ChaseOpponent = 1

GameProtocolChecksum = 1
GameAccountNames = 2
GameChallengeOnLogin = 3
GamePenalityOnDeath = 4
GameNameOnNpcTrade = 5
GameDoubleFreeCapacity = 6
GameDoubleExperience = 7
GameTotalCapacity = 8
GameSkillsBase = 9
GamePlayerRegenerationTime = 10
GameChannelPlayerList = 11
GamePlayerMounts = 12
GameEnvironmentEffect = 13
GameCreatureEmblems = 14
GameItemAnimationPhase = 15
GameMagicEffectU16 = 16
GamePlayerMarket = 17
GameSpritesU32 = 18
GameChargeableItems = 19
GameOfflineTrainingTime = 20
GamePurseSlot = 21
GameFormatCreatureName = 22
GameSpellList = 23
GameClientPing = 24
GameExtendedClientPing = 25
GameDoubleHealth = 28
GameDoubleSkills = 29
GameChangeMapAwareRange = 30
GameMapMovePosition = 31
GameAttackSeq = 32
GameBlueNpcNameColor = 33
GameDiagonalAnimatedText = 34
GameLoginPending = 35
GameNewSpeedLaw = 36
GameForceFirstAutoWalkStep = 37
GameMinimapRemove = 38
GameDoubleShopSellAmount = 39

TextColors = {
  red       = '#F55E5E',
  orange    = '#F36500',
  green     = '#00EB00',
  lightblue = '#5FF7F7',
  blue      = '#9F9DFD',
  --blue1     = '#6e50dc',
  --blue2     = '#3264c8',
  --blue3     = '#0096c8',
  purple    = '#FF00FF',
  yellow    = '#FFFF00',
  truered   = '#C71712',
  trueblue  = '#0066FF',
  white     = '#FFFFFF',
  npcto 	= '#0066FF',
  npcfrom 	= '#00B100',
  
  darkGreen = '#00A800',
  normGreen = '#00D800',
  normWhite = '#62FF2E',
  lightGreen = '#B8FF71',
  whiteGreen = '#62FF2E',
  normOrange = '#E69500',
}

MessageType = {
  None = 0,
  SummonDeal = 1,
  SummonRecived = 2,
  PlayerDeal = 3,
  PlayerRecivedHealth = 4,
  PlayerRecivedMana = 5,
  PlayerBlocked = 6,
  HealHealth = 7,
  HealMana = 8,
  StolenHealth = 9,
  StolenMana = 10,
  Loot = 11,
  Experience = 12,
  Advance = 13,
  Warning = 14,
  Orange = 15,
  Spell = 16,
  Blocked = 17,
  Dodged = 18,
  HealHealthCritical = 19,
  PlayerDealCritical = 20,
  SummonRecivedCritical = 21,
  SummonDealCritical = 22,
  HealHealthDeathHit = 23,
  PlayerDealDeathHit = 24,
  SummonRecivedDeathHit = 25,
  SummonDealDeathHit = 26,
  Private = 27
}

CriticalMessageTypes = {
	MessageType.SummonDealCritical,
	MessageType.SummonRecivedCritical,
	MessageType.PlayerDealCritical,
	MessageType.HealHealthCritical,
	MessageType.HealHealth,
	MessageType.PlayerDeal,
	MessageType.SummonDeal,
	MessageType.SummonRecived
}

DeathHitMessageTypes = {
	MessageType.SummonDealDeathHit,
	MessageType.SummonRecivedDeathHit,
	MessageType.PlayerDealDeathHit,
	MessageType.HealHealthDeathHit
}

MessagePosition = {
  None = 0,
  Top = 1,
  Middle = 2,
  Bottom = 3,
  Right = 4,
  Left = 5
}

MessageModes = {
  None                    = 0,
  Say                     = 1,
  Whisper                 = 2,
  Yell                    = 3,
  PrivateFrom             = 4,
  PrivateTo               = 5,
  ChannelManagement       = 6,
  Channel                 = 7,
  ChannelHighlight        = 8,
  Spell                   = 9,
  NpcFrom                 = 10,
  NpcTo                   = 11,
  GamemasterBroadcast     = 12,
  GamemasterChannel       = 13,
  GamemasterPrivateFrom   = 14,
  GamemasterPrivateTo     = 15,
  Login                   = 16,
  Warning                 = 17,
  Game                    = 18,
  Failure                 = 19,
  Look                    = 20,
  DamageDealed            = 21,
  DamageReceived          = 22,
  Heal                    = 23,
  Exp                     = 24,
  DamageOthers            = 25,
  HealOthers              = 26,
  ExpOthers               = 27,
  Status                  = 28,
  Loot                    = 29,
  TradeNpc                = 30,
  Guild                   = 31,
  PartyManagement         = 32,
  Party                   = 33,
  BarkLow                 = 34,
  BarkLoud                = 35,
  Report                  = 36,
  HotkeyUse               = 37,
  TutorialHint            = 38,
  Thankyou                = 39,
  Market                  = 40,
  BeyondLast              = 41,
  MonsterYell             = 42,
  MonsterSay              = 43,
  MessageDefault		  = 44,
  MessageServerLog		  = 45,
  MessageScreen			  = 46,
  MessagePrivate          = 47,
  Last		              = 48,
  Invalid                 = 255
}

OTSERV_RSA  = "1091201329673994292788609605089955415282375029027981291234687579" ..
              "3726629149257644633073969600111060390723088861007265581882535850" ..
              "3429057592827629436413108566029093628212635953836686562675849720" ..
              "6207862794310902180176810615217550567108238764764442605581471797" ..
              "07119674283982419152118103759076030616683978566631413"

CIPSOFT_RSA = "1321277432058722840622950990822933849527763264961655079678763618" ..
              "4334395343554449668205332383339435179772895415509701210392836078" ..
              "6959821132214473291575712138800495033169914814069637740318278150" ..
              "2907336840325241747827401343576296990629870233111328210165697754" ..
              "88792221429527047321331896351555606801473202394175817"

-- set to the latest Tibia.pic signature to make otclient compatible with official tibia
PIC_SIGNATURE = 0x50a6469d

OsTypes = {
  Linux = 1,
  Windows = 2,
  Flash = 3,
  OtclientLinux = 10,
  OtclientWindows = 11,
  OtclientMac = 12,
}

PathFindResults = {
  Ok = 0,
  Position = 1,
  Impossible = 2,
  TooFar = 3,
  NoWay = 4,
}

PathFindFlags = {
  AllowNullTiles = 1,
  AllowCreatures = 2,
  AllowNonPathable = 4,
  AllowNonWalkable = 8,
}

FriendState = {
  Offline = 0,
  Online = 1,
}

ExtendedIds = {
  Activate = 0,
  Locale = 1,
  Ping = 2,
  Sound = 3,
  Game = 4,
  Particles = 5,
  MapShader = 6,
  NeedsUpdate = 7
}

function updateItemQuality(widget, classId)
	local corner = widget:getChildById('corner')
	if classId == ITEMCLASS_NONE then
		classId = ITEMCLASS_IGNORE
	end
	
	if classId ~= ITEMCLASS_IGNORE then
		corner:setImageClip(getImageClipByClassId(classId))
		
		if not modules.client_options.getOption('displayItemCorners') then
			corner:hide()
		else
			corner:show()
		end
	else
		corner:hide()
	end
	
	if modules.client_options.getOption('displayItemFrames') then
		widget:setImageClip(getFrameClipByClassId(classId))
	else
		widget:setImageClip(getFrameClipByClassId(ITEMCLASS_IGNORE))
	end
end

function getFrameClipByClassId(id)
	if id == ITEMCLASS_DESOLATE then
		return '1505 2574 32 32'
	elseif id == ITEMCLASS_DAMAGED then
		return '1545 2574 32 32'
	elseif id == ITEMCLASS_NORMAL then
		return '1583 2574 32 32'
	elseif id == ITEMCLASS_PERFECT then
		return '1621 2574 32 32'
	elseif id == ITEMCLASS_LEGENDARY then
		return '1659 2574 32 32'
	elseif id == ITEMCLASS_UNIQUE then
		return '1736 2574 32 32'
	elseif id == ITEMCLASS_QUEST then
		return '1697 2574 32 32'
	elseif id == ITEMCLASS_SHOP then
		return '1775 2574 32 32'
	end
	
	return '8 445 34 34'
end

function getImageClipByClassId(id)
	if id == ITEMCLASS_DESOLATE then
		return '7 0 7 7'
	elseif id == ITEMCLASS_DAMAGED then
		return '0 0 7 7'
	elseif id == ITEMCLASS_NORMAL then
		return '14 0 7 7'
	elseif id == ITEMCLASS_PERFECT then
		return '21 0 7 7'
	elseif id == ITEMCLASS_LEGENDARY then
		return '28 0 7 7'
	elseif id == ITEMCLASS_UNIQUE then
		return '35 0 7 7'
	elseif id == ITEMCLASS_QUEST then
		return '42 0 7 7'
	elseif id == ITEMCLASS_SHOP then
		return '49 0 7 7'
	end
	
	return '0 0 7 7'
end

function getClassNameById(id)
	if id == ITEMCLASS_DESOLATE then
		return 'desolate'
	elseif id == ITEMCLASS_DAMAGED then
		return 'damaged'
	elseif id == ITEMCLASS_NORMAL then
		return 'normal'
	elseif id == ITEMCLASS_PERFECT then
		return 'perfect'
	elseif id == ITEMCLASS_LEGENDARY then
		return 'legendary'
	elseif id == ITEMCLASS_UNIQUE then
		return 'unique'
	end
	
	return ''
end

function getClassIdByName(id)
	if id == 'desolate' then
		return ITEMCLASS_DESOLATE
	elseif id == 'damaged' then
		return ITEMCLASS_DAMAGED
	elseif id == 'normal' then
		return ITEMCLASS_NORMAL
	elseif id == 'perfect' then
		return ITEMCLASS_PERFECT
	elseif id == 'legendary' then
		return ITEMCLASS_LEGENDARY
	elseif id == 'unique' then
		return ITEMCLASS_UNIQUE
	end
	
	return ITEMCLASS_IGNORE
end

function isInArray(value, key)
	for _, v in pairs(value) do
		if v == key then
			return true
		end
	end
	
	return false
end

function getRequiredVocationNameById(id, var)
	if isInArray(var, id) then
		return true, getVocationNameById(id)
	end
	
	local description = ''
	for _, v in pairs(var) do
		if description ~= '' then
			description = description .. ' ' .. tr('or') .. ' '
		end
		
		description = description .. getVocationNameById(v)
	end
	
	return false, description
end

function getVocationNameById(var)
	if var == 1 then
		return tr('Mage'), 'avatar_1'
	elseif var == 2 then
		return tr('Druid'), 'avatar_2'
	elseif var == 3 then
		return tr('Wizard'), 'avatar_3'
	elseif var == 4 then
		return tr('Hunter'), 'avatar_1'
	elseif var == 5 then
		return tr('Tracker'), 'avatar_2'
	elseif var == 6 then
		return tr('Grenadier'), 'avatar_3'
	elseif var == 7 then
		return tr('Mercenary'), 'avatar_1'
	elseif var == 8 then
		return tr('Paladin'), 'avatar_2'
	elseif var == 9 then
		return tr('Barbarian'), 'avatar_3'
	end
	
	return '[Error]', 'avatar_1'
end

-- @}

local start = os.time()
local linecount = 0
debug.sethook(function(event, line)
    linecount = linecount + 1
    if os.time() - start > 1 then
        if linecount >= 100000 then
            print(string.format("possible infinite loop in file %s near line %s, total line count %s", debug.getinfo(2).source, line, linecount))
            debug.sethook()
        end
		
        linecount = 0
        start = os.time()
    end
end, "l")
