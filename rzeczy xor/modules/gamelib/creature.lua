-- @docclass Creature

-- @docconsts @{

SkullNone = 0
SkullYellow = 1
SkullGreen = 2
SkullWhite = 3
SkullRed = 4
SkullBlack = 5
SkullGolden = 6
SkullSilver = 7
SkullBronze = 8
SkullPink = 9
SkullPurple = 10
SkullOrange = 11
SkullBlue = 12
SkullPumpkin = 13
SkullChristmas = 14

ShieldNone = 0
ShieldWhiteYellow = 1
ShieldWhiteBlue = 2
ShieldBlue = 3
ShieldYellow = 4
ShieldBlueSharedExp = 5
ShieldYellowSharedExp = 6
ShieldBlueNoSharedExpBlink = 7
ShieldYellowNoSharedExpBlink = 8
ShieldBlueNoSharedExp = 9
ShieldYellowNoSharedExp = 10

EmblemNone = 0
EmblemBleed = 1
EmblemEarth = 2
EmblemEnergy = 3
EmblemFire = 4
EmblemIce = 5
EmblemLight = 6
EmblemShadow = 7

-- @}

function getSkullImagePath(skullId)
  local path
  if skullId == SkullYellow then
    path = '/images/leaf/skulls/skull_yellow'
  elseif skullId == SkullGreen then
    path = '/images/leaf/skulls/skull_green'
  elseif skullId == SkullWhite then
    path = '/images/leaf/skulls/skull_white'
  elseif skullId == SkullRed then
    path = '/images/leaf/skulls/skull_red'
  elseif skullId == SkullBlack then
    path = '/images/leaf/skulls/skull_black'
  elseif skullId == SkullGolden then
    path = '/images/leaf/skulls/skull_golden'
  elseif skullId == SkullSilver then
    path = '/images/leaf/skulls/skull_silver'
  elseif skullId == SkullBronze then
    path = '/images/leaf/skulls/skull_bronze'
  elseif skullId == SkullPink then
    path = '/images/leaf/skulls/skull_pink'
  elseif skullId == SkullPurple then
    path = '/images/leaf/skulls/skull_purple'
  elseif skullId == SkullOrange then
    path = '/images/leaf/skulls/skull_orange'
  elseif skullId == SkullBlue then
    path = '/images/leaf/skulls/skull_blue'
  elseif skullId == SkullPumpkin then
    path = '/images/leaf/skulls/skull_pumpkin'
  elseif skullId == SkullChristmas then
    path = '/images/leaf/skulls/skull_christmas'
  end
  
  return path
end

function getShieldImagePathAndBlink(shieldId)
  local path, blink
  if shieldId == ShieldWhiteYellow then
    path, blink = '/images/leaf/shields/shield_yellow_white', false
  elseif shieldId == ShieldWhiteBlue then
    path, blink = '/images/leaf/shields/shield_blue_white', false
  elseif shieldId == ShieldBlue then
    path, blink = '/images/leaf/shields/shield_blue', false
  elseif shieldId == ShieldYellow then
    path, blink = '/images/leaf/shields/shield_yellow', false
  elseif shieldId == ShieldBlueSharedExp then
    path, blink = '/images/leaf/shields/shield_blue_shared', false
  elseif shieldId == ShieldYellowSharedExp then
    path, blink = '/images/leaf/shields/shield_yellow_shared', false
  elseif shieldId == ShieldBlueNoSharedExpBlink then
    path, blink = '/images/leaf/shields/shield_blue_not_shared', true
  elseif shieldId == ShieldYellowNoSharedExpBlink then
    path, blink = '/images/leaf/shields/shield_yellow_not_shared', true
  elseif shieldId == ShieldBlueNoSharedExp then
    path, blink = '/images/leaf/shields/shield_blue_not_shared', false
  elseif shieldId == ShieldYellowNoSharedExp then
    path, blink = '/images/leaf/shields/shield_yellow_not_shared', false
  elseif shieldId == ShieldGray then
    path, blink = '/images/leaf/shields/shield_gray', false
  end
  return path, blink
end

function getEmblemImagePath(emblemId)
  local path
  if emblemId == EmblemBleed then
    path = '/images/leaf/clans/bleed'
  elseif emblemId == EmblemEarth then
    path = '/images/leaf/clans/earth'
  elseif emblemId == EmblemEnergy then
    path = '/images/leaf/clans/energy'
  elseif emblemId == EmblemFire then
    path = '/images/leaf/clans/fire'
  elseif emblemId == EmblemIce then
    path = '/images/leaf/clans/ice'
  elseif emblemId == EmblemLight then
    path = '/images/leaf/clans/light'
  elseif emblemId == EmblemShadow then
    path = '/images/leaf/clans/shadow'
  end
  return path
end

function Creature:onSkullChange(skullId)
  local imagePath = getSkullImagePath(skullId)
  if imagePath then
    self:setSkullTexture(imagePath)
  end
end

function Creature:onShieldChange(shieldId)
  local imagePath, blink = getShieldImagePathAndBlink(shieldId)
  if imagePath then
    self:setShieldTexture(imagePath, blink)
  end
end

function Creature:onEmblemChange(emblemId)
  local imagePath = getEmblemImagePath(emblemId)
  if imagePath then
    self:setEmblemTexture(imagePath)
  end
end
