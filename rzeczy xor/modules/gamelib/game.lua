local currentRsa

function g_game.getHighlightedText(text, color, highlightColor)
	local tmpData = {}
	local tmpText = ''
	for i, part in ipairs(text:split("[c=")) do
		if i == 1 then
			tmpText = tmpText .. part
		else
			local fromId, toId = string.find(part, ']')
			if fromId and toId then
				local color = part:sub(1, toId - 1)
				local variable = part:sub(fromId + 1, part:len() - 1)
				
				local fromId, toId = string.find(variable, '/c')
				
				local fromText = variable:sub(1, fromId - 2)
				local toText = variable:sub(toId + 2, variable:len() - 1)
				variable = fromText .. toText
				
				if i > 2 then
					tmpText = tmpText .. ', '
				end
				
				if color ~= '#FFFFFF' then
					table.insert(tmpData, tmpText)
					table.insert(tmpData, variable)
					table.insert(tmpData, color)
				end
				
				tmpText = tmpText .. variable
				if color ~= '#FFFFFF' then
					table.insert(tmpData, tmpText:len() + 1)
				end
			end
		end
	end
	
	return tmpData, tmpText
end

function g_game.setHighlightedText(widget, text, highlightedColor)
	local highlightData, text = g_game.getHighlightedText(text, color, highlightedColor)
    if #highlightData > 0 then
		local tmpText = ''
		for i = 1, #highlightData / 4 do
			local prevText = highlightData[i * 4 - 3]
			local highlightText = highlightData[i * 4 - 2]
			local color = highlightData[i * 4 - 1]
			local toId = highlightData[i * 4]
			if color then
				local labelHighlight = g_ui.createWidget('ConsolePhantomLabel', widget)
				labelHighlight:setFont(widget:getFont())
				labelHighlight:setTextAlign(widget:getTextAlign())
				labelHighlight:setId('consoleLabelHighlight' .. i)
				labelHighlight:setColor(color)
				
				tmpText = tmpText .. prevText
				
				local tmpExplode = prevText:explode(' ')
				local v = ''
				for i = 1, #tmpExplode do
					widget:setText(tmpExplode[i])
					v = v .. string.rep(string.char(127), widget:getTextLength()) .. (i < #tmpExplode and ' ' or '')
				end
				
				v = v .. highlightText
				
				local tmpSecondText = text:sub(toId, text:len())
				local tmpExplode = tmpSecondText:explode(' ')
				for i = 1, #tmpExplode do
					widget:setText(tmpExplode[i])
					v = v .. string.rep(string.char(127), widget:getTextLength()) .. (i < #tmpExplode and ' ' or '')
				end
				
				labelHighlight:setText(v)
				tmpText = tmpText .. highlightText
			end
		end
	end
	
	widget:setText(text)
	return text
end

function g_game.getItemType(it)
	if not it.attributes then
		return ITEMTYPE_NONE
	end
	
	local var = ITEMTYPE_NONE
	if it.attributes.weapontype then
		if it.attributes.weapontype == 'ammunition' then
			var = ITEMTYPE_AMMUNITION
		elseif it.attributes.weapontype == 'wand' then
			var = ITEMTYPE_WAND
		elseif it.attributes.weapontype == 'quiver' then
			var = ITEMTYPE_QUIVER
		elseif it.attributes.weapontype == 'shield' then
			var = ITEMTYPE_SHIELD
		elseif it.attributes.weapontype == 'onehand' then
			if it.attributes.weapongroup == 'melee' then
				var = ITEMTYPE_ONEHANDED_MELEE
			elseif it.attributes.weapongroup == 'distance' then
				var = ITEMTYPE_SPEAR
			end
		elseif it.attributes.weapontype == 'twohand' then
			if it.attributes.weapongroup == 'melee' then
				var = ITEMTYPE_TWOHANDED_MELEE
			elseif it.attributes.weapongroup == 'distance' then
				if it.attributes.ammotype == 'bolt' then
					var = ITEMTYPE_CROSSBOW
				elseif it.attributes.ammotype == 'arrow' then
					var = ITEMTYPE_BOW
				end
			end
		end
	elseif it.attributes.slottype == 'head' then
		var = ITEMTYPE_HELMET
	elseif it.attributes.slottype == 'body' then
		var = ITEMTYPE_ARMOR
	elseif it.attributes.slottype == 'legs' then
		var = ITEMTYPE_LEGS
	elseif it.attributes.slottype == 'feet' then
		var = ITEMTYPE_FEET
	elseif it.attributes.slottype == 'backpack' or it.id == 16817 or (it.id and it.id >= 19527 and it.id <= 19530) then
		var = ITEMTYPE_BACKPACK
	elseif it.attributes.slottype == 'necklace' then
		var = ITEMTYPE_NECKLACE
	elseif it.attributes.slottype == 'ring' then
		var = ITEMTYPE_RING
	elseif it.attributes.food then
		var = ITEMTYPE_FOOD
	elseif it.attributes.valuable then
		var = ITEMTYPE_VALUABLE
	elseif it.attributes.craftingmaterial then
		var = ITEMTYPE_CRAFTING_MATERIAL
	elseif it.attributes.creatureproduct then
		var = ITEMTYPE_CREATURE_PRODUCT
	elseif it.attributes.runeword or it.id == 2260 then
		var = ITEMTYPE_RUNE
	elseif it.attributes.potion or it.id == 6558 or it.id == 7439 or it.id == 7440 or it.id == 7443 then
		var = ITEMTYPE_POTION
	elseif it.attributes.doll then
		var = ITEMTYPE_DOLL
	elseif it.attributes.trophy then
		var = ITEMTYPE_TROPHY
	elseif it.attributes.tool then
		var = ITEMTYPE_TOOL
	elseif it.attributes.container then
		var = ITEMTYPE_CONTAINERS
	elseif it.attributes.shopitem then
		var = ITEMTYPE_PREMIUM
	end
	
	return var
end

function g_game.getRsa()
  return G.currentRsa
end

function g_game.isIdenticalPositions(firstPos, secondPos)
	return firstPos.x == secondPos.x and firstPos.y == secondPos.y and firstPos.z == secondPos.z
end

function g_game.getMilharNumber(n)
	local value = tostring(math.floor(n)):reverse():gsub("(%d%d%d)", "%1."):gsub(",(%-?)$","%1"):reverse()
	if value:sub(1, 1) == '.' then
		return value:sub(2, value:len())
	end
	
	return value
end

function g_game.findPlayerItem(itemId, subType)
    local localPlayer = g_game.getLocalPlayer()
    if localPlayer then
        for slot = InventorySlotFirst, InventorySlotLast do
            local item = localPlayer:getInventoryItem(slot)
            if item and item:getId() == itemId and (subType == -1 or item:getSubType() == subType) then
                return item
            end
        end
    end

    return g_game.findItemInContainers(itemId, subType)
end

function g_game.chooseRsa(host)
  if G.currentRsa ~= CIPSOFT_RSA and G.currentRsa ~= OTSERV_RSA then return end
  if host:ends('.tibia.com') or host:ends('.cipsoft.com') then
    g_game.setRsa(CIPSOFT_RSA)

    if g_app.getOs() == 'windows' then
      g_game.setCustomOs(OsTypes.Windows)
    else
      g_game.setCustomOs(OsTypes.Linux)
    end
  else
    if G.currentRsa == CIPSOFT_RSA then
      g_game.setCustomOs(-1)
    end
    g_game.setRsa(OTSERV_RSA)
  end
end

function g_game.setRsa(rsa, e)
  e = e or '65537'
  g_crypt.rsaSetPublicKey(rsa, e)
  G.currentRsa = rsa
end

function g_game.isOfficialTibia()
  return G.currentRsa == CIPSOFT_RSA
end

function g_game.getSupportedClients()
  return {960}
end

function g_game.getProtocolVersionForClient(client)
  clients = {
    [980] = 971,
    [981] = 973,
    [982] = 974,
	  [983] = 975,
	  [984] = 976,
	  [985] = 977,
	  [986] = 978,
	  [1001] = 979,
	  [1002] = 980,
  }
  return clients[client] or client
end

if not G.currentRsa then
  g_game.setRsa(OTSERV_RSA)
end
