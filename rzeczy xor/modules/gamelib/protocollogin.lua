-- @docclass
ProtocolLogin = extends(Protocol)

LoginServerError = 10
LoginServerCreateError = 12
LoginServerSuccessAccount = 13
LoginServerAlreadyExistAccount = 14
LoginServerCharacterError = 15
LoginServerNewPassword = 16
LoginServerUpdate = 17
LoginServerMotd = 20
LoginServerUpdateNeeded = 30
LoginServerCharacterList = 100
LoginServerExtendedCharacterList = 101

function ProtocolLogin:checkHost(host, port, accountName, accountPassword)
	if string.len(host) == 0 or port == nil or port == 0 then
		signalcall(self.onLoginError, self, tr("You must enter a valid server address and port."))
		return
	end
	
	self.accountName = accountName
	self.accountPassword = accountPassword
end

function ProtocolLogin:delete(host, port, accountName, accountPassword, name)
	self:checkHost(host, port, accountName, accountPassword)
	self.characterName = name
	self.connectCallback = self.sendDeletePacket

	self:connect(host, port)
end

function ProtocolLogin:character(host, port, accountName, accountPassword, name, vocation)
	self:checkHost(host, port, accountName, accountPassword)
	self.characterName = name
	self.characterVocation = vocation
	self.connectCallback = self.sendCharacterPacket

	self:connect(host, port)
end

function ProtocolLogin:account(host, port, accountName, accountPassword, mail)
	self:checkHost(host, port, accountName, accountPassword)
	self.accountMail = mail
	self.connectCallback = self.sendAccountPacket

	self:connect(host, port)
end

function ProtocolLogin:login(host, port, accountName, accountPassword)
	self:checkHost(host, port, accountName, accountPassword)
	self.connectCallback = self.sendLoginPacket

	self:connect(host, port)
end

function ProtocolLogin:password(host, port, accountName, accountPassword, password)
	self:checkHost(host, port, accountName, accountPassword)
	self.newPassword = password
	self.connectCallback = self.sendPasswordPacket

	self:connect(host, port)
end

function ProtocolLogin:reset(host, port, mail)
	if string.len(host) == 0 or port == nil or port == 0 then
		signalcall(self.onLoginError, self, tr("You must enter a valid server address and port."))
		return
	end
	
	self.accountMail = mail
	self.connectCallback = self.sendResetPacket
	
	self:connect(host, port)
end

function ProtocolLogin:cancelLogin()
	self:disconnect()
end

function ProtocolLogin:sendResetPacket()
	self:sendPacket(ACCOUNT_RESET)
end

function ProtocolLogin:sendPasswordPacket()
	self:sendPacket(ACCOUNT_PASSWORD)
end

function ProtocolLogin:sendDeletePacket()
	self:sendPacket(ACCOUNT_DELETE)
end

function ProtocolLogin:sendCharacterPacket()
	self:sendPacket(ACCOUNT_CHARACTER)
end

function ProtocolLogin:sendAccountPacket()
	self:sendPacket(ACCOUNT_CREATE)
end

function ProtocolLogin:sendLoginPacket()
	self:sendPacket(ACCOUNT_NONE)
end

function ProtocolLogin:sendPacket(var)
	local msg = OutputMessage.create()
	msg:addU8(ClientOpcodes.ClientEnterAccount)
	msg:addU16(g_game.getOs())

	msg:addU16(960)

	msg:addU32(g_things.getDatSignature())
	msg:addU32(g_sprites.getSprSignature())
	msg:addU32(PIC_SIGNATURE)

	local offset = msg:getMessageSize()

	-- first RSA byte must be 0
	msg:addU8(0)
	-- xtea key
	self:generateXteaKey()
	local xteaKey = self:getXteaKey()
	msg:addU32(xteaKey[1])
	msg:addU32(xteaKey[2])
	msg:addU32(xteaKey[3])
	msg:addU32(xteaKey[4])

	msg:addU8(var)
	if var == ACCOUNT_RESET then
		msg:addString(self.accountMail)
	else
		msg:addString(self.accountName)
		msg:addString(self.accountPassword)
		if var == ACCOUNT_CREATE then
			msg:addString(self.accountMail)
		elseif var == ACCOUNT_CHARACTER then
			msg:addString(self.characterName)
			msg:addU16(self.characterVocation)
		elseif var == ACCOUNT_DELETE then
			msg:addString(self.characterName)
		elseif var == ACCOUNT_PASSWORD then
			msg:addString(self.newPassword)
		end
	end
	
	if self.getLoginExtendedData then
		local data = self:getLoginExtendedData()
		msg:addString(data)
	end

	local paddingBytes = g_crypt.rsaGetSize() - (msg:getMessageSize() - offset)
	assert(paddingBytes >= 0)
	msg:addPaddingBytes(paddingBytes, 0)
	msg:encryptRsa()

	self:enableChecksum()
	self:send(msg)
	self:enableXteaEncryption()
	self:recv()
end

function ProtocolLogin:onConnect()
	self.gotConnection = true
	self:connectCallback()
	self.connectCallback = nil
end

function ProtocolLogin:onRecv(msg)
  while not msg:eof() do
    local opcode = msg:getU8()
    if opcode == LoginServerError then
      self:parseError(msg)
	elseif opcode == LoginServerCreateError then
	  self:parseError(msg)
	elseif opcode == LoginServerSuccessAccount then
	  self:parseCreateAccount(msg)
	elseif opcode == LoginServerAlreadyExistAccount then
	  self:parseExistError(msg)
	elseif opcode == LoginServerCharacterError then
	  self:parseCharacterError(msg)
	elseif opcode == LoginServerNewPassword then
	  self:parseNewPassword(msg)
	elseif opcode == LoginServerMotd then
	  msg:getString()
    elseif opcode == LoginServerUpdateNeeded then
      signalcall(self.onLoginError, self, tr("Client needs update."))
    elseif opcode == LoginServerCharacterList then
      self:parseCharacterList(msg)
    elseif opcode == LoginServerExtendedCharacterList then
      self:parseExtendedCharacterList(msg)
    elseif opcode == LoginServerUpdate then
      local signature = msg:getString()
      signalcall(self.onUpdateNeeded, self, signature)
    else
      self:parseOpcode(opcode, msg)
    end
  end
  
  self:disconnect()
end

function ProtocolLogin:parseError(msg)
  local errorMessage = msg:getString()
  signalcall(self.onLoginError, self, errorMessage, nil)
end

function ProtocolLogin:parseCreateAccount(msg)
  signalcall(self.onCreateAccount, self)
end

function ProtocolLogin:parseNewPassword(msg)
  signalcall(self.onNewPassword, self)
end

function ProtocolLogin:parseCharacterError(msg)
  local description = msg:getString()
  signalcall(self.onCharacterError, self, description)
end

function ProtocolLogin:parseExistError(msg)
  signalcall(self.onErrorExistAccount, self)
end

function ProtocolLogin:parseCharacterList(msg)
	local characters = {}
	local serverPort = msg:getU16()
	
	local charactersCount = msg:getU8()
	for i = 1,charactersCount do
		local character = {}
		character.name = msg:getString()
		character.worldPort = serverPort
		
		if msg:getU8() == 1 then
			character.level = msg:getU32()
			character.vocationId = msg:getU16()
		end
		
		characters[i] = character
	end

	local account = {}
	account.premDays = msg:getU16()
	signalcall(self.onCharacterList, self, characters, account)
end

function ProtocolLogin:parseExtendedCharacterList(msg)
  local characters = msg:getTable()
  local account = msg:getTable()
  local otui = msg:getString()
  signalcall(self.onCharacterList, self, characters, account, otui)
end

function ProtocolLogin:parseOpcode(opcode, msg)
  signalcall(self.onOpcode, self, opcode, msg)
end

function ProtocolLogin:onError(msg, code)
  local text = translateNetworkError(code, self:isConnecting(), msg)
  signalcall(self.onLoginError, self, text)
end