local ip = {
	'51.68.124.93', -- France
	'142.44.210.245', -- Canada
	-- '127.0.0.1', -- Local
	'34.74.233.94' -- USA
}

local port = {
	7171,
	7171,
	7171
}

EnterGame = { }

-- private variables
local loadBox
local enterGame
local serverList
local enterGameButton
local clientBox
local protocolLogin
local errorBox
local currentSelect
local resetPasswordWindow

function EnterGame.destroyLoadBox()
	if loadBox then
		loadBox:destroy()
		loadBox = nil
	end
end

function selectOk()
	if loadBox then
		loadBox:destroy()
		loadBox = nil
		
		EnterGame.show()
	end
end

function onOk()
	EnterGame.destroyLoadBox()
	if errorBox then
		errorBox:destroy()
		errorBox = nil
		
		EnterGame.show()
	end
end

-- private functions
local function onError(protocol, message, errorCode)
	if enterGame and enterGame:isVisible() then
		return true
	end
	
	if loadBox then
		EnterGame.destroyLoadBox()
	end
	
	if not errorBox then
		errorBox = g_ui.displayUI('errorbox')
	end
	
	errorBox:getChildById('label'):setText(message)
	connect(errorBox:getChildById('button'), { onClick = onOk })
	connect(errorBox, { onEnter = onOk })
	connect(errorBox, { onEscape = onOk })
end

local function onCreateAccount(protocol)
  modules.game_registery.m_RegisteryFunctions.onCreateAccount()
end

local function onCharacterError(protocol, description)
  modules.game_registery.m_RegisteryFunctions.onCharacterError(description)
end

local function onErrorExistAccount(protocol)
  modules.game_registery.m_RegisteryFunctions.onErrorExistAccount()
end

local function onNewPassword(protocol)
  modules.game_registery.m_RegisteryFunctions.newPassword()
end

local function onCharacterList(protocol, characters, account, otui)
	-- Try add server to the server list
	modules.game_registery.m_RegisteryFunctions.destroy()
	modules.client_background.hideLogo()
	modules.client_background.setTip(tr(onCharacterListTip[math.random(#onCharacterListTip)]))
	
	ServerList.add(G.host, G.port)

	if enterGame:getChildById('rememberPasswordBox'):isChecked() then
		local account = g_crypt.encrypt(G.account)
		local password = g_crypt.encrypt(G.password)
	
		g_settings.set('account', account)
		g_settings.set('password', password)

		ServerList.setServerAccount(G.host, account)
		ServerList.setServerPassword(G.host, password)
	else
		-- reset server list account/password
		ServerList.setServerAccount(G.host, '')
		ServerList.setServerPassword(G.host, '')

		EnterGame.clearAccountFields()
	end

	EnterGame.destroyLoadBox()
  
	CharacterList.create(characters, account, otui)
	CharacterList.show()
end

local function onUpdateNeeded(protocol, signature)
	EnterGame.destroyLoadBox()

	if EnterGame.updateFunc then
		local continueFunc = EnterGame.show
		local cancelFunc = EnterGame.show
		EnterGame.updateFunc(signature, continueFunc, cancelFunc)
	else
		errorBox = g_ui.displayUI('errorbox')
		errorBox:getChildById('label'):setText(tr('Your client needs updating, try redownloading it.'))
		connect(errorBox:getChildById('button'), { onClick = onOk })
		connect(errorBox, { onEnter = onOk })
		connect(errorBox, { onEscape = onOk })
	end
end

-- public functions
function EnterGame.init()
	enterGame = g_ui.displayUI('entergame')
	serverList = enterGame:getChildById('serverList')
	
	local account = g_settings.get('account')
	local password = g_settings.get('password')
	local host = g_settings.get('host')
	local cn_server_check = tonumber(g_settings.get('cn_server_check', 0))
	EnterGame.setAccountName(account)
	EnterGame.setPassword(password)
	
	if cn_server_check == 1 then
		EnterGame.doCheck(serverList:getChildById('cn_server'))
	elseif cn_server_check == 2 then
		EnterGame.doCheck(serverList:getChildById('us_server'))
	else
		EnterGame.doCheck(serverList:getChildById('fr_server'))
	end
	
	clientBox = enterGame:getChildById('clientComboBox')
	
	local language = g_settings.get('locale', 'false')
	if language == 'false' then
		language = 'en'
	end
	
	if language == 'en' then
		enterGame:getChildById('login'):setOn(true)
		enterGame:getChildById('reset'):setOn(true)
		enterGame:getChildById('registery'):setOn(true)
	end
	
	enterGame:hide()
	
	if g_app.isRunning() and not g_game.isOnline() then
		enterGame:show()
	end
end

function EnterGame.firstShow()
	EnterGame.show()
end

function EnterGame.terminate()
	enterGame:destroy()
	enterGame = nil
	if enterGameButton then
		enterGameButton:destroy()
		enterGameButton = nil
	end
	clientBox = nil
	if loadBox then
		EnterGame.destroyLoadBox()
	end
	if protocolLogin then
		protocolLogin:cancelLogin()
		protocolLogin = nil
	end
	EnterGame = nil
end

function EnterGame.show()
	if loadBox or errorBox or g_game.isOnline() then
		return true
	end
	
	enterGame:show()
	enterGame:raise()
	enterGame:focus()
	modules.game_registery.m_RegisteryFunctions.back(true)
	if resetPasswordWindow then
		resetPasswordWindow:destroy()
		resetPasswordWindow = nil
		currentPasswordSelect = nil
	end
end

function EnterGame.hide()
	enterGame:hide()
end

function EnterGame.openWindow()
	if g_game.isOnline() then
		CharacterList.show()
	elseif not g_game.isLogging() and not CharacterList.isVisible() then
		EnterGame.show()
	end
end

function EnterGame.setAccountName(account)
	local account = g_crypt.decrypt(account)
	if account == '' then
		enterGame:getChildById('accountNameLabel'):show()
	else
		enterGame:getChildById('accountNameLabel'):hide()
	end
	
	enterGame:getChildById('accountNameTextEdit'):setText(account)
	enterGame:getChildById('accountNameTextEdit'):setCursorPos(-1)
	enterGame:getChildById('rememberPasswordBox'):setChecked(#account > 0)
end

function EnterGame.setPassword(password)
	local password = g_crypt.decrypt(password)
	if password == '' then
		enterGame:getChildById('accountPasswordLabel'):show()
	else
		enterGame:getChildById('accountPasswordLabel'):hide()
	end
	
	enterGame:getChildById('accountPasswordTextEdit'):setText(password)
end

function EnterGame.onAccountNameChange()
	if enterGame:getChildById('accountNameTextEdit'):getText() == '' then
		enterGame:getChildById('accountNameLabel'):show()
	else
		enterGame:getChildById('accountNameLabel'):hide()
	end
end

function EnterGame.onPasswordsChange()
	if enterGame:getChildById('accountPasswordTextEdit'):getText() == '' then
		enterGame:getChildById('accountPasswordLabel'):show()
	else
		enterGame:getChildById('accountPasswordLabel'):hide()
	end
end

function EnterGame.clearAccountFields()
	enterGame:getChildById('accountNameTextEdit'):clearText()
	enterGame:getChildById('accountPasswordTextEdit'):clearText()
	enterGame:getChildById('accountNameTextEdit'):focus()
	g_settings.remove('account')
	g_settings.remove('password')
end

function EnterGame.onOk()
	EnterGame.destroyLoadBox()
	
	protocolLogin:cancelLogin()
	EnterGame.show()
end

function EnterGame.isTestServer()
	return not serverList:getChildById('fr_server'):isOn()
end

function EnterGame.getProxyIP()
	if serverList:getChildById('us_server'):isChecked() then
		return ip[3] -- USA
	elseif serverList:getChildById('cn_server'):isOn() then
		return ip[2] -- Canada
	else
		return ip[1] -- France
	end
end

function EnterGame.getProxyPort()
	if serverList:getChildById('cn_server'):isOn() then
		return port[2]
	elseif serverList:getChildById('us_server'):isChecked() then
		return port[3]
	else
		return port[1]
	end
end

function EnterGame.doCheck(self)
	local id = self:getId()
	if id == 'cn_server' then
		g_settings.set('cn_server_check', 1)
		serverList:getChildById('us_server'):setChecked(false)
	elseif id == 'us_server' then
		g_settings.set('cn_server_check', 2)
		self:setChecked(not self:isChecked())
		self = serverList:getChildById('cn_server')
	else
		g_settings.set('cn_server_check', 0)
		serverList:getChildById('us_server'):setChecked(false)
	end
	
	if currentSelect then
		currentSelect:setOn(false)
	end
	
	currentSelect = self
	currentSelect:setOn(true)
end

function EnterGame.onTextChange(self)
	if self:getText() ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
end

function EnterGame.hidePassword()
	if resetPasswordWindow then
		resetPasswordWindow:destroy()
		resetPasswordWindow = nil
		currentPasswordSelect = nil
		
		EnterGame.show()
	end
end

function EnterGame.onResetPassword()
	if not resetPasswordWindow then
		EnterGame.show()
		return true
	end
	
	local text = resetPasswordWindow:getChildById('mail'):getText()
	if text == '' or not text:find('@') or not text:find('.') then
		return true
	end
	
	local G = {}
	if serverList:getChildById('cn_server'):isOn() then
		G.host = ip[2]
		G.port = port[2]
	else
		G.host = ip[1]
		G.port = port[1]
	end
	
	protocolLogin = ProtocolLogin.create()
	g_game.setProtocolVersion(g_game.getProtocolVersionForClient(960))
	g_game.chooseRsa(G.host)
  
	modules.game_things.load()

	if modules.game_things.isLoaded() then
		protocolLogin:reset(G.host, G.port, text)
		
		resetPasswordWindow:destroy()
		resetPasswordWindow = nil
		currentPasswordSelect = nil
		
		if not errorBox then
			errorBox = g_ui.displayUI('errorbox')
			connect(errorBox:getChildById('button'), { onClick = onOk })
			connect(errorBox, { onEnter = onOk })
			connect(errorBox, { onEscape = onOk })
		end
		
		errorBox:getChildById('label'):setText(tr('Please wait patiently for an e-mail. Check the Spam folder. In case of problem, please contact us via Discord.'))
	end
end

function EnterGame.doResetPassword()
	EnterGame.destroyLoadBox()
	EnterGame.hide()
	
	resetPasswordWindow = g_ui.displayUI('resetpassword')
end

function EnterGame.doLogin(__G, var)
	EnterGame.destroyLoadBox()

	if not __G then
		G.account = enterGame:getChildById('accountNameTextEdit'):getText()
		G.password = enterGame:getChildById('accountPasswordTextEdit'):getText()
		
		EnterGame.hide()
		if G.account == '' or G.password == '' then
			errorBox = g_ui.displayUI('errorbox')
			errorBox:getChildById('label'):setText(tr('Invalid account name or password.'))
			connect(errorBox:getChildById('button'), { onClick = onOk })
			connect(errorBox, { onEnter = onOk })
			connect(errorBox, { onEscape = onOk })
			return
		end
		
		if g_game.isOnline() then
			errorBox = g_ui.displayUI('errorbox')
			errorBox:getChildById('label'):setText(tr('Cannot login while already in game.'))
			connect(errorBox:getChildById('button'), { onClick = onOk })
			connect(errorBox, { onEnter = onOk })
			connect(errorBox, { onEscape = onOk })
			return
		end
	else
		G = __G
	end
	
	if G.currentId then
		G.host = ip[G.currentId]
		G.port = port[G.currentId]
	else
		if serverList:getChildById('us_server'):isChecked() then
			G.host = ip[3]
			G.port = port[3]
		elseif serverList:getChildById('cn_server'):isOn() then
			G.host = ip[2]
			G.port = port[2]
		else
			G.host = ip[1]
			G.port = port[1]
		end
	end
	
	g_settings.set('host', G.host)
	g_settings.set('port', G.port)
	
	protocolLogin = ProtocolLogin.create()
	protocolLogin.onLoginError = onError
	if not var then
		protocolLogin.onErrorExistAccount = onErrorExistAccount
		protocolLogin.onCreateAccount = onCreateAccount
		protocolLogin.onCharacterList = onCharacterList
		protocolLogin.onUpdateNeeded = onUpdateNeeded
	elseif var == ACCOUNT_CREATE then
		protocolLogin.onErrorExistAccount = onErrorExistAccount
		protocolLogin.onCreateAccount = onCreateAccount
	elseif var == ACCOUNT_CHARACTER then
		protocolLogin.onCharacterError = onCharacterError
		protocolLogin.onCharacterList = onCharacterList
	elseif var == ACCOUNT_DELETE then
		protocolLogin.onCharacterList = onCharacterList
	elseif var == ACCOUNT_PASSWORD then
		protocolLogin.onNewPassword = onNewPassword
	end
	
	loadBox = g_ui.displayUI('loadingbox')
	loadBox:getChildById('label'):setText(tr('Connecting to login server...'))
	
	local button = loadBox:getChildById('button')
	local language = g_settings.get('locale', 'false')
	if language == 'false' then
		language = 'en'
	end
	
	if language == 'en' then
		button:setOn(true)
	end
	
	connect(button, { onClick = selectOk })
	connect(loadBox, { onEnter = selectOk })
	connect(loadBox, { onEscape = selectOk })

	g_game.setProtocolVersion(g_game.getProtocolVersionForClient(960))
	g_game.chooseRsa(G.host)
  
	modules.game_things.load()

	if modules.game_things.isLoaded() then
		if var == ACCOUNT_CREATE then
			protocolLogin:account(G.host, G.port, G.account, G.password, G.mail)
		elseif var == ACCOUNT_CHARACTER then
			protocolLogin:character(G.host, G.port, G.account, G.password, G.name, G.vocation)
		elseif var == ACCOUNT_DELETE then
			protocolLogin:delete(G.host, G.port, G.account, G.password, G.name)
		elseif var == ACCOUNT_PASSWORD then
			protocolLogin:password(G.host, G.port, G.account, G.password, G.newPassword)
		else
			protocolLogin:login(G.host, G.port, G.account, G.password)
			modules.game_registery.m_RegisteryFunctions.executePasswords(G.password, G.account)
		end
	else
		EnterGame.destroyLoadBox()
		EnterGame.show()
	end
end

function EnterGame.setDefaultServer(host, port, protocol)
	local portTextEdit = enterGame:getChildById('serverPortTextEdit')
	local clientLabel = enterGame:getChildById('clientLabel')
	local accountTextEdit = enterGame:getChildById('accountNameTextEdit')
	local passwordTextEdit = enterGame:getChildById('accountPasswordTextEdit')

	portTextEdit:setText(port)
	clientBox:setCurrentOption(protocol)
	accountTextEdit:setText('')
	passwordTextEdit:setText('')
end

function EnterGame.setUniqueServer(host, port, protocol, windowWidth, windowHeight)
	local portTextEdit = enterGame:getChildById('serverPortTextEdit')
	portTextEdit:setText(port)
	portTextEdit:setVisible(false)
	portTextEdit:setHeight(0)

	clientBox:setCurrentOption(protocol)
	clientBox:setVisible(false)
	clientBox:setHeight(0)

	local portLabel = enterGame:getChildById('portLabel')
	portLabel:setVisible(false)
	portLabel:setHeight(0)
	local clientLabel = enterGame:getChildById('clientLabel')
	clientLabel:setVisible(false)
	clientLabel:setHeight(0)

	local rememberPasswordBox = enterGame:getChildById('rememberPasswordBox')
	rememberPasswordBox:setMarginTop(-5)

	if not windowWidth then windowWidth = 236 end
	enterGame:setWidth(windowWidth)
	if not windowHeight then windowHeight = 200 end
	enterGame:setHeight(windowHeight)
end

function EnterGame.setServerInfo(message)
  
end
