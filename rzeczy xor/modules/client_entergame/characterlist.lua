CharacterList = { }

-- private variables
local charactersWindow
local loadBox
local characterList
local errorBox
local waitingWindow
local updateWaitEvent
local resendWaitEvent
local cooldown

-- private functions
function onCharacterOk()
	if errorBox then
		errorBox:destroy()
		errorBox = nil
		
		CharacterList.show()
	end
end

function onLoginOk()
	if cooldown and cooldown > os.time() then
		return
	end
	
	loadBox:destroy()
	loadBox = nil
	g_game.cancelLogin()
	CharacterList.show()
end

local function tryLogin(charInfo, tries)
	tries = tries or 1

	if tries > 50 then
		return
	end

	if g_game.isOnline() then
		if tries == 1 then
			g_game.safeLogout()
		end
		scheduleEvent(function() tryLogin(charInfo, tries + 1) end, 100)
		return
	end
	
	CharacterList.hide()
	g_game.loginWorld(G.account, G.password, charInfo.worldName, charInfo.worldHost, charInfo.worldPort, charInfo.characterName, charInfo.language)

	loadBox = g_ui.displayUI('loadingbox')
	loadBox:getChildById('label'):setText(tr('Connecting to game server...'))
	cooldown = os.time() + 1
	
	local button = loadBox:getChildById('button')
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		button:setOn(true)
	end
	
	connect(button, { onClick = onLoginOk })
	connect(loadBox, { onEnter = onLoginOk })
	connect(loadBox, { onEscape = onLoginOk })

	-- save last used character
	g_settings.set('last-used-character', charInfo.characterName)
end

local function updateWait(timeStart, timeEnd)
	if waitingWindow then
		local time = g_clock.seconds()
		if time <= timeEnd then
			local percent = ((time - timeStart) / (timeEnd - timeStart)) * 100
			local timeStr = string.format("%.0f", timeEnd - time)
			
			local progressBar = waitingWindow:getChildById('progressBar')
			progressBar:setPercent(percent)
			
			local label = waitingWindow:getChildById('timeLabel')
			label:setText(tr('Trying to reconnect in %s seconds.', timeStr))
			
			updateWaitEvent = scheduleEvent(function() updateWait(timeStart, timeEnd) end, 1000 * progressBar:getPercentPixels() / 100 * (timeEnd - timeStart))
			return true
		end
	end

	if updateWaitEvent then
		updateWaitEvent:cancel()
		updateWaitEvent = nil
	end
end

local function resendWait()
	if waitingWindow then
		waitingWindow:destroy()
		waitingWindow = nil

		if updateWaitEvent then
			updateWaitEvent:cancel()
			updateWaitEvent = nil
		end

		if charactersWindow then
			local selected = characterList:getFocusedChild()
			if selected then
				local charInfo = { worldHost = selected.worldHost,
								worldPort = selected.worldPort,
								worldName = selected.worldName,
								characterName = selected.characterName,
								language = g_settings.get('locale', 'false') }
				tryLogin(charInfo)
			end
		end
	end
end

local function onLoginWait(message, time)
  CharacterList.destroyLoadBox()

  waitingWindow = g_ui.displayUI('waitinglist')

  local label = waitingWindow:getChildById('infoLabel')
  label:setText(message)

  updateWaitEvent = scheduleEvent(function() updateWait(g_clock.seconds(), g_clock.seconds() + time) end, 0)
  resendWaitEvent = scheduleEvent(resendWait, time * 1000)
end

function onGameLoginError(message)
	CharacterList.destroyLoadBox()
	
	errorBox = g_ui.displayUI('errorbox')
	errorBox:getChildById('label'):setText(message)
	cooldown = os.time() + 1
	connect(errorBox:getChildById('button'), { onClick = onCharacterOk })
	connect(errorBox, { onEnter = onCharacterOk })
	connect(errorBox, { onEscape = onCharacterOk })
end

function onGameConnectionError(message, code)
	CharacterList.destroyLoadBox()
	local text = translateNetworkError(code, g_game.getProtocolGame() and g_game.getProtocolGame():isConnecting(), message)
	
	errorBox = g_ui.displayUI('errorbox')
	errorBox:getChildById('label'):setText(text)
	cooldown = os.time() + 1
	connect(errorBox:getChildById('button'), { onClick = onCharacterOk })
	connect(errorBox, { onEnter = onCharacterOk })
	connect(errorBox, { onEscape = onCharacterOk })
end

function onGameUpdateNeeded(signature)
	CharacterList.destroyLoadBox()
	
	errorBox = g_ui.displayUI('errorbox')
	errorBox:getChildById('label'):setText(tr('Enter with your account again to update your client.'))
	cooldown = os.time() + 1
	connect(errorBox:getChildById('button'), { onClick = onCharacterOk })
	connect(errorBox, { onEnter = onCharacterOk })
	connect(errorBox, { onEscape = onCharacterOk })
end

-- public functions
function CharacterList.init()
	connect(g_game, { onLoginError = onGameLoginError })
	connect(g_game, { onUpdateNeeded = onGameUpdateNeeded })
	connect(g_game, { onConnectionError = onGameConnectionError })
	connect(g_game, { onGameStart = CharacterList.onGameStart })
	connect(g_game, { onLoginWait = onLoginWait })
	connect(g_game, { onGameEnd = CharacterList.showAgain })

	g_keyboard.bindKeyPress('Up', selectPreviousCharacter)
	g_keyboard.bindKeyPress('Down', selectNextCharacter)
	
	if G.characters then
		CharacterList.create(G.characters, G.characterAccount)
	end
end

function CharacterList.onGameStart()
	CharacterList.destroyLoadBox()
	EnterGame.hide()
	CharacterList.hide()
end

function CharacterList.destroyList()
	if charactersWindow then
		characterList = charactersWindow:getChildById('characters')
		characterList:destroyChildren()
		
		characterList:destroy()
		characterList = nil
		
		charactersWindow:destroy()
		charactersWindow = nil
	end
end

function CharacterList.terminate()
  disconnect(g_game, { onLoginError = onGameLoginError })
  disconnect(g_game, { onUpdateNeeded = onGameUpdateNeeded })
  disconnect(g_game, { onConnectionError = onGameConnectionError })
  disconnect(g_game, { onGameStart = CharacterList.onGameStart })
  disconnect(g_game, { onLoginWait = onLoginWait })
  disconnect(g_game, { onGameEnd = CharacterList.showAgain })

  g_keyboard.unbindKeyPress('Up')
  g_keyboard.unbindKeyPress('Down')

  CharacterList.destroyList()
  if loadBox then
    g_game.cancelLogin()
    loadBox:destroy()
    loadBox = nil
  end

  if waitingWindow then
    waitingWindow:destroy()
    waitingWindow = nil
  end

  if updateWaitEvent then
    updateWaitEvent:cancel()
    updateWaitEvent = nil
  end

  if resendWaitEvent then
    resendWaitEvent:cancel()
    resendWaitEvent = nil
  end

  CharacterList = nil
end

function selectPreviousCharacter()
	if CharacterList and CharacterList:isVisible() then
		local children = characterList:getChildren()
		local selectChar = false
		for i = #children, 1, -1 do
			local widget = children[i]
			if selectChar then
				widget:setOn(true)
				addEvent(function() characterList:ensureChildVisible(widget) end)
				break
			end
			
			if widget:isOn() and i > 1 then
				selectChar = true
				widget:setOn(false)
			end
		end
	end
end

function selectNextCharacter()
	if CharacterList and CharacterList:isVisible() then
		local children = characterList:getChildren()
		local selectChar, k = false, 0
		for i = 1, #children do
			k = k + 1
			local widget = children[i]
			if selectChar then
				widget:setOn(true)
				addEvent(function() characterList:ensureChildVisible(widget) end)
				break
			end
			
			if widget:isOn() and i < #children then
				selectChar = true
				widget:setOn(false)
			end
		end
	end
end

function selectCharacter(self)
	local children = characterList:getChildren()
	for _, v in pairs(children) do
		v:setOn(false)
	end
	
	self:setOn(true)
end

function CharacterList.doChangePassword()
	CharacterList.hide()
	modules.game_registery.m_RegisteryFunctions.onChangePassword()
end

function CharacterList.doRemoveCharacter(self)
	local name = self:getChildById('name'):getText()
	CharacterList.hide()
	modules.game_registery.m_RegisteryFunctions.onRemoveCharacter(name)
end

function CharacterList.doCreateNewCharacter()
	CharacterList.hide()
	modules.game_registery.m_RegisteryFunctions.onCreateAccount(true)
end

function CharacterList.updateLevel(localName, localVocation, level)
	local children = characterList:getChildren()
	for _, widget in pairs(children) do
		if widget.characterName == localName and localVocation > 0 and (widget.level ~= level or widget.vocation ~= localVocation) then
			local description = widget:getChildById('description')
			local avatar = widget:getChildById('avatar')
		
			local localVocation, avatarId = getVocationNameById(localVocation)
		
			description:setText(tr('Level ') .. level .. ', ' .. localVocation)
			avatar:setImageSource('/images/leaf/registery/list/' .. avatarId)
		end
	end
end

function CharacterList.create(characters, account, otui)
	if charactersWindow then
		charactersWindow:destroy()
		charactersWindow = nil
	end
	
	charactersWindow = g_ui.displayUI('characterlist')
	characterList = charactersWindow:getChildById('characters')
	
	local language = g_settings.get('locale', 'false')
	if language == 'false' then
		language = 'en'
	end
	
	if language == 'en' then
		charactersWindow:getChildById('new'):setOn(true)
		charactersWindow:getChildById('play'):setOn(true)
		charactersWindow:getChildById('password'):setOn(true)
	end
	
	-- characters
	G.characters = characters
	G.characterAccount = account
	
	characterList:destroyChildren()
	
	local focusLabel = nil
	for i = 1, #characters do
		local characterInfo = characters[i]
	
		local widget = g_ui.createWidget('CharacterWidget', characterList)
		local name = widget:getChildById('name')
		local description = widget:getChildById('description')
		local avatar = widget:getChildById('avatar')
		
		local vocationId, avatarId = getVocationNameById(characterInfo.vocationId)
		
		name:setText(characterInfo.name)
		description:setText(tr('Level ') .. characterInfo.level .. ', ' .. vocationId)
		avatar:setImageSource('/images/leaf/registery/list/' .. avatarId)
		
		widget.characterName = characterInfo.name
		widget.worldName = characterInfo.worldName
		widget.worldHost = EnterGame.getProxyIP()
		widget.worldPort = EnterGame.getProxyPort() + 1
		
		widget.level = characterInfo.level
		widget.vocation = characterInfo.vocationId
		
		connect(widget, { onDoubleClick = function () CharacterList.doLogin() return true end } )
		
		widget:setParent(characterList)
		
		if i == 1 or g_settings.get('last-used-character') == widget.characterName then
			focusLabel = widget
		end
	end
	
	if focusLabel then
		focusLabel:setOn(true)
		characterList:focusChild(focusLabel, KeyboardFocusReason)
		addEvent(function() if characterList then characterList:ensureChildVisible(focusLabel) end end)
	end
	
	characterList:setHeight(86 * math.min(4, #characters))
	
	EnterGame.hide()
end

function CharacterList.destroy()
  CharacterList.hide(true)

  if charactersWindow then
    characterList = nil
    charactersWindow:destroy()
    charactersWindow = nil
  end
end

function CharacterList.show()
	if loadBox or errorBox or not charactersWindow or g_game.isOnline() then
		return true
	end
	
	charactersWindow:show()
	charactersWindow:raise()
	charactersWindow:focus()
end

function CharacterList.hide(showLogin)
	if not charactersWindow then
		return
	end

	charactersWindow:hide()
	if showLogin and EnterGame and not g_game.isOnline() then
		EnterGame.show()
	end
end

function CharacterList.showAgain()
	if characterList and characterList:hasChildren() then
		CharacterList.show()
	end
end

function CharacterList.isVisible()
	if charactersWindow and charactersWindow:isVisible() then
		return true
	end
	
	return false
end

function CharacterList.doLogin()
	local selected = nil
	for _, v in pairs(characterList:getChildren()) do
		if v:isOn() then
			selected = v
			break
		end
	end
  
	if selected then
		local charInfo = { 	
							worldHost = selected.worldHost,
							worldPort = selected.worldPort,
							worldName = selected.worldName,
							characterName = selected.characterName,
							language = g_settings.get('locale', 'en')
							}
		charactersWindow:hide()
	
		tryLogin(charInfo)
	else
		CharacterList.hide()
		
		errorBox = g_ui.displayUI('errorbox')
		errorBox:getChildById('label'):setText(tr('Enter with your account again to update your client.'))
		cooldown = os.time() + 1
		connect(errorBox:getChildById('button'), { onClick = onCharacterOk })
		connect(errorBox, { onEnter = onCharacterOk })
		connect(errorBox, { onEscape = onCharacterOk })
	end
end

function CharacterList.destroyLoadBox()
  if loadBox then
    loadBox:destroy()
    loadBox = nil
  end
end

function CharacterList.cancelWait()
  if waitingWindow then
    waitingWindow:destroy()
    waitingWindow = nil
  end

  if updateWaitEvent then
      updateWaitEvent:cancel()
      updateWaitEvent = nil
  end

  if resendWaitEvent then
    resendWaitEvent:cancel()
    resendWaitEvent = nil
  end

  CharacterList.destroyLoadBox()
  CharacterList.showAgain()
end
