AddServer = {}

-- private variables
local addServerWindow = nil

-- public functions
function AddServer.init()
  addServerWindow = g_ui.displayUI('addserver')
end

function AddServer.terminate()
  addServerWindow:destroy()
end

function AddServer.add()
	local host = addServerWindow:getChildById('host'):getText()
	local port = addServerWindow:getChildById('port'):getText()

	local added, error = ServerList.add(host, port)
	if not added then
		errorBox = g_ui.displayUI('errorbox')
		errorBox:getChildById('label'):setText(tr(error))
		connect(errorBox:getChildById('button'), { onClick = EnterGame.onOk })
		connect(errorBox, { onEnter = EnterGame.onOk })
		connect(errorBox, { onEscape = EnterGame.onOk })
	else
		AddServer.hide()
	end
end

function AddServer.show()
  addServerWindow:show()
  addServerWindow:raise()
  addServerWindow:focus()
  addServerWindow:lock()
end

function AddServer.hide()
  addServerWindow:hide()
  addServerWindow:unlock()
end