m_HotkeyFunctions = {}
m_HotkeyList = {}

m_HotkeyFunctions.lastHotkeyUse = 0

m_HotkeyFunctions.HOTKEY_MANAGER_USE = 0
m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF = 1
m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET = 2
m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH = 3
m_HotkeyFunctions.HOTKEY_MANAGER_USE_ON_CREATURE = 4

m_HotkeyFunctions.hotkeyList = {}
m_HotkeyFunctions.colors = {
	text = '#888888',
	itemUse = '#8888FF',
	itemUseSelf = '#00FF00',
	itemUseTarget = '#FF0000',
	itemUseWith = '#F5B325',
	spellUse = '#B572E3'
}

m_HotkeyFunctions.destroyPopup = function()
	if m_HotkeyList.popup then
		m_HotkeyList.popup:destroy()
		m_HotkeyList.popup = nil
	end
	
	if m_HotkeyList.window then
		m_HotkeyList.window:setEnabled(true)
	end
end

m_HotkeyFunctions.getDistanceBetween = function(p1, p2)
	if not p1 or not p2 then
		return 0
	end
	
    return math.max(math.abs(p1.x - p2.x), math.abs(p1.y - p2.y))
end

m_HotkeyFunctions.displayPopup = function()
	m_HotkeyList.window:setEnabled(false)
	
	if not m_HotkeyList.popup then
		m_HotkeyList.popup = g_ui.displayUI('popup_hotkeys')
	end
end

m_HotkeyFunctions.reset = function()
	m_HotkeyFunctions.destroyPopup()
	m_HotkeyFunctions.unload()
	m_HotkeyFunctions.load(true)
	
	m_HotkeyList.currentHotkeyLabel = nil
end

m_HotkeyFunctions.destroy = function()
	m_HotkeyFunctions.destroyPopup()
	
	if m_HotkeyList.window then
		local hotkeys = {}
		for _, child in pairs(m_HotkeyList.list:getChildren()) do
			hotkeys[child.keyCombo] = {
				autoSend = child.autoSend,
				autoAttack = child.autoAttack,
				itemId = child.itemId,
				spell = child.spell,
				name = child.name,
				multiUse = child.multiUse,
				useType = child.useType,
				value = child.value
			}
		end
		
		m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()] = hotkeys
		m_HotkeyFunctions.save()
		
		hotkeyCaptureCancel()
		
		m_HotkeyList.currentHotkeyLabel = nil
		m_HotkeyList.list:destroyChildren()
		m_HotkeyList.window:destroy()
		
		m_HotkeyList = {}
	end
end

m_HotkeyFunctions.clearObject = function()
	m_HotkeyList.currentHotkeyLabel.useType = nil
	m_HotkeyList.currentHotkeyLabel:getChildById('item'):hide()
	m_HotkeyList.currentHotkeyLabel:getChildById('spell'):hide()
	m_HotkeyList.currentHotkeyLabel:setText(m_HotkeyList.currentHotkeyLabel.keyCombo .. ': ')
	m_HotkeyList.currentHotkeyLabel:setOn(false)
	m_HotkeyFunctions.setHotkeyMode(m_HotkeyList.currentHotkeyLabel.name, m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF)
	m_HotkeyList.currentHotkeyLabel.itemId = nil
	m_HotkeyList.currentHotkeyLabel.spell = nil
	m_HotkeyList.currentHotkeyLabel.multiUse = nil
	m_HotkeyList.currentHotkeyLabel.name = nil
	m_HotkeyList.currentHotkeyLabel.autoSend = nil
	m_HotkeyList.currentHotkeyLabel.autoAttack = nil
	m_HotkeyList.currentHotkeyLabel.value = nil
	
	m_HotkeyFunctions.updateHotkeyForm()
	m_HotkeyFunctions.updateHotkeyLabel(m_HotkeyList.currentHotkeyLabel)
end

m_HotkeyFunctions.updateHotkeyForm = function(id)
	if m_HotkeyList.currentHotkeyLabel then
		m_HotkeyList.removeHotkeyButton:show()
		m_HotkeyList.selectObjectButton:show()
		if m_HotkeyList.currentHotkeyLabel.multiUse then
			m_HotkeyList.useWith:show()
		else
			m_HotkeyList.useWith:hide()
		end
		
		if m_HotkeyList.currentHotkeyLabel.itemId then
			m_HotkeyList.hotkeyText:clearText()
			m_HotkeyList.hotkeyText:hide()
			m_HotkeyList.selectNextTarget:show()
			m_HotkeyList.selectNextTarget:setChecked(m_HotkeyList.currentHotkeyLabel.autoAttack)
			m_HotkeyList.sendAutomatically:hide()
			m_HotkeyList.clearObjectButton:show()
			
			m_HotkeyList.useOnSelf:show()
			m_HotkeyList.useOnTarget:show()
			
			if m_HotkeyList.currentHotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF then
				m_HotkeyList.useRadioGroup:selectWidget(m_HotkeyList.useOnSelf)
			elseif m_HotkeyList.currentHotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET then
				m_HotkeyList.useRadioGroup:selectWidget(m_HotkeyList.useOnTarget)
			elseif m_HotkeyList.currentHotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH then
				m_HotkeyList.useRadioGroup:selectWidget(m_HotkeyList.useWith)
			end
		else
			m_HotkeyList.useOnSelf:hide()
			m_HotkeyList.useOnTarget:hide()
			m_HotkeyList.useRadioGroup:clearSelected()
			m_HotkeyList.hotkeyText:show()
			m_HotkeyList.hotkeyText:focus()
			m_HotkeyList.hotkeyText:setText(m_HotkeyList.currentHotkeyLabel.value)
			m_HotkeyList.hotkeyText:setCursorPos(-1)
			m_HotkeyList.sendAutomatically:setChecked(m_HotkeyList.currentHotkeyLabel.autoSend)
			m_HotkeyList.sendAutomatically:setEnabled(m_HotkeyList.currentHotkeyLabel.value and #m_HotkeyList.currentHotkeyLabel.value > 0)
			m_HotkeyList.sendAutomatically:show()
			m_HotkeyList.selectNextTarget:setChecked(m_HotkeyList.currentHotkeyLabel.autoAttack)
			m_HotkeyList.selectNextTarget:show()
			m_HotkeyList.clearObjectButton:hide()
		end
	else
		m_HotkeyList.removeHotkeyButton:hide()
		m_HotkeyList.hotkeyText:hide()
		m_HotkeyList.hotkeyText:clearText()
		m_HotkeyList.sendAutomatically:hide()
		m_HotkeyList.sendAutomatically:setChecked(false)
		m_HotkeyList.selectNextTarget:hide()
		m_HotkeyList.selectNextTarget:setChecked(false)
		m_HotkeyList.selectObjectButton:hide()
		m_HotkeyList.clearObjectButton:hide()
		m_HotkeyList.useOnSelf:hide()
		m_HotkeyList.useOnTarget:hide()
		m_HotkeyList.useRadioGroup:clearSelected()
	end
end

m_HotkeyFunctions.getPathByUseType = function(useType)
	local path = '/images/leaf/spellWindow/'
	if useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH then
		path = path .. 'frame_yellow'
	elseif useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF then
		path = path .. 'frame_green'
	elseif useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET then
		path = path .. 'frame_red'
	else
		path = nil
	end
	
	return path
end

m_HotkeyFunctions.setHotkeyMode = function(name, useType, keyCombo)
	if not modules.game_spellbar or not name then
		return true
	end
	
	local attack = modules.game_spellbar.m_SpellBarFunctions.getAttackByName(name, true)
	if not attack then
		return true
	end

	if useType then
		attack.widget:getChildById('icon'):setIcon(m_HotkeyFunctions.getPathByUseType(useType))
		modules.game_spellbar.m_SpellBarFunctions.updateHotkey(name, keyCombo)
	else
		local hotkeys = m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()]
		if hotkeys then
			for key, v in pairs(hotkeys) do
				if v.name and v.name:lower() == name then
					attack.widget:getChildById('icon'):setIcon(m_HotkeyFunctions.getPathByUseType(v.useType))
					modules.game_spellbar.m_SpellBarFunctions.updateHotkey(name, key)
					return true
				end
			end
		end
	end
end

m_HotkeyFunctions.updateHotkeyLabel = function(hotkeyLabel)
	if not hotkeyLabel then 
		return true
	end
	
	if hotkeyLabel.spell then
		local description = hotkeyLabel.keyCombo .. ': (' .. tr('use ') .. hotkeyLabel.name
		if hotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF then
			description = description .. tr(' on yourself')
		elseif hotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET then
			description = description .. tr(' on target')
		elseif hotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH then
			description = description .. tr(' with crosshair')
		end

		description = description .. ')'
		hotkeyLabel:setText(description)
		local color = m_HotkeyFunctions.colors.spellUse
		
		if hotkeyLabel.itemId <= 15 or hotkeyLabel.itemId == 110 or hotkeyLabel.itemId == 121 or hotkeyLabel.itemId == 147 or hotkeyLabel.itemId == 148 or hotkeyLabel.itemId == 157 then
			color = '#DB7116'
		elseif hotkeyLabel.itemId <= 30 or hotkeyLabel.itemId == 113 or (hotkeyLabel.itemId >= 126 and hotkeyLabel.itemId <= 128) or hotkeyLabel.itemId == 142 or (hotkeyLabel.itemId >= 149 and hotkeyLabel.itemId <= 150) or (hotkeyLabel.itemId >= 163 and hotkeyLabel.itemId <= 166) or hotkeyLabel.itemId == 168 then
			color = '#B572E3'
		elseif hotkeyLabel.itemId <= 35 or hotkeyLabel.itemId == 91 or hotkeyLabel.itemId == 145 then
			color = '#31A516'
		elseif hotkeyLabel.itemId <= 50 or (hotkeyLabel.itemId >= 86 and hotkeyLabel.itemId <= 88) or (hotkeyLabel.itemId >= 92 and hotkeyLabel.itemId <= 94) or hotkeyLabel.itemId == 96 or hotkeyLabel.itemId == 143 or (hotkeyLabel.itemId >= 153 and hotkeyLabel.itemId <= 154) or hotkeyLabel.itemId == 171 then
			color = '#32AACD'
		elseif hotkeyLabel.itemId <= 52 then
			color = '#6C3F29'
		elseif hotkeyLabel.itemId <= 55 or (hotkeyLabel.itemId >= 155 and hotkeyLabel.itemId <= 156) then
			color = '#2C4863'
		elseif hotkeyLabel.itemId <= 60 or hotkeyLabel.itemId == 62 or hotkeyLabel.itemId == 111 or (hotkeyLabel.itemId >= 151 and hotkeyLabel.itemId <= 152) or hotkeyLabel.itemId == 162 or hotkeyLabel.itemId == 167 then
			color = '#63A50C'
		elseif hotkeyLabel.itemId <= 70 then
			color = '#B47319'
		elseif hotkeyLabel.itemId <= 72 or (hotkeyLabel.itemId >= 81 and hotkeyLabel.itemId <= 84) then
			color = '#8f866E'
		elseif hotkeyLabel.itemId <= 76 or hotkeyLabel.itemId == 109 or (hotkeyLabel.itemId >= 114 and hotkeyLabel.itemId <= 117) or hotkeyLabel.itemId == 168 then
			color = '#C11520'
		elseif hotkeyLabel.itemId <= 80 or hotkeyLabel.itemId == 108 or hotkeyLabel.itemId == 141 or hotkeyLabel.itemId == 144 or hotkeyLabel.itemId == 169 then
			color = '#FFDC4C'
		elseif hotkeyLabel.itemId == 85 or (hotkeyLabel.itemId >= 163 and hotkeyLabel.itemId <= 165) then
			color = '#45345E'
		elseif hotkeyLabel.itemId == 89 or hotkeyLabel.itemId == 95 or hotkeyLabel.itemId == 174 then
			color = '#EDDD2E'
		elseif hotkeyLabel.itemId == 97 or (hotkeyLabel.itemId >= 160 and hotkeyLabel.itemId <= 161) or hotkeyLabel.itemId == 170 then
			color = '#606040'
		elseif hotkeyLabel.itemId == 98 or (hotkeyLabel.itemId >= 122 and hotkeyLabel.itemId <= 125) or (hotkeyLabel.itemId >= 135 and hotkeyLabel.itemId <= 136) then
			color = '#802000'
		elseif hotkeyLabel.itemId <= 106 or (hotkeyLabel.itemId >= 118 and hotkeyLabel.itemId <= 120) or (hotkeyLabel.itemId >= 130 and hotkeyLabel.itemId <= 134) or (hotkeyLabel.itemId >= 137 and hotkeyLabel.itemId <= 140) or (hotkeyLabel.itemId >= 172 and hotkeyLabel.itemId <= 173) then
			color = '#A6CAF0'
		elseif hotkeyLabel.itemId == 107 or hotkeyLabel.itemId == 112 then
			color = '#000000'
		end
		
		hotkeyLabel:setColor(color)
		m_HotkeyFunctions.setHotkeyMode(hotkeyLabel.name, hotkeyLabel.useType, hotkeyLabel.keyCombo)
	elseif hotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF then
		hotkeyLabel:setText(tr('%s: (use object on yourself)', hotkeyLabel.keyCombo))
		hotkeyLabel:setColor(m_HotkeyFunctions.colors.itemUseSelf)
	elseif hotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET then
		hotkeyLabel:setText(tr('%s: (use object on target)', hotkeyLabel.keyCombo))
		hotkeyLabel:setColor(m_HotkeyFunctions.colors.itemUseTarget)
	elseif hotkeyLabel.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH then
		hotkeyLabel:setText(tr('%s: (use object with crosshair)', hotkeyLabel.keyCombo))
		hotkeyLabel:setColor(m_HotkeyFunctions.colors.itemUseWith)
	elseif hotkeyLabel.itemId then
		hotkeyLabel:setText(tr('%s: (use object)', hotkeyLabel.keyCombo))
		hotkeyLabel:setColor(m_HotkeyFunctions.colors.itemUse)
	else
		local text = hotkeyLabel.keyCombo .. ': '
		if hotkeyLabel.value then
			text = text .. tostring(hotkeyLabel.value)
		end
		
		hotkeyLabel:setText(text)
		if hotkeyLabel.autoSend then
			hotkeyLabel:setColor(m_HotkeyFunctions.colors.autoSend)
		else
			hotkeyLabel:setColor(m_HotkeyFunctions.colors.text)
		end
	end
end

m_HotkeyFunctions.setSpellIcon = function(id)
	m_HotkeyList.currentHotkeyLabel:getChildById('spell'):setImageClip(modules.game_spellbar.m_SpellBarFunctions.getImageClip(id))
end

m_HotkeyFunctions.onChooseItemMouseRelease = function(self, mousePosition, mouseButton)
	if mouseButton == MouseLeftButton and m_HotkeyList.currentHotkeyLabel then
		local widget = modules.game_interface.getRootPanel():recursiveGetChildByPos(mousePosition, false)
		if not widget then
			return true
		end
		
		local item = nil
		local className = widget:getClassName()
		if className == 'UIMap' then
			local tile = widget:getTile(mousePosition)
			if not tile then
				return true
			end
			
			local thing = tile:getTopMoveThing()
			if not thing or not thing:isItem() then
				return true
			end
			
			m_HotkeyList.currentHotkeyLabel:getChildById('item'):show()
			m_HotkeyList.currentHotkeyLabel:getChildById('spell'):hide()
			m_HotkeyList.currentHotkeyLabel:setOn(true)
			item = thing
		elseif className == 'UIItem' then
			if widget:isVirtual() then
				return true
			end
			
			m_HotkeyList.currentHotkeyLabel:getChildById('item'):show()
			m_HotkeyList.currentHotkeyLabel:getChildById('spell'):hide()
			m_HotkeyList.currentHotkeyLabel:setOn(true)
			item = widget:getItem()
		elseif className == 'UIWidget' then
			local id = widget:getId()
			if not id:find('spellIcon') then
				return true
			end
			
				
			attack = modules.game_spellbar.m_SpellBarFunctions.getSpellById(tonumber(id:sub(10, id:len())))
			if not attack then
				return true
			end
			
			m_HotkeyFunctions.clearObject()
			
			m_HotkeyList.currentHotkeyLabel:getChildById('item'):hide()
			m_HotkeyList.currentHotkeyLabel:getChildById('spell'):show()
			m_HotkeyList.currentHotkeyLabel:setOn(true)
			
			id = attack.data.iconId
			m_HotkeyFunctions.setSpellIcon(id)
			
			m_HotkeyList.currentHotkeyLabel.itemId = id
			m_HotkeyList.currentHotkeyLabel.name = attack.data.name
			m_HotkeyList.currentHotkeyLabel.spell = true
			m_HotkeyList.currentHotkeyLabel.multiUse = (attack.data.spellArea or 0) ~= 0
			if m_HotkeyList.currentHotkeyLabel.multiUse then
				m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH
			else
				m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF
			end
			
			m_HotkeyList.currentHotkeyLabel.value = nil
			m_HotkeyList.currentHotkeyLabel.autoSend = false
			m_HotkeyFunctions.updateHotkeyLabel(m_HotkeyList.currentHotkeyLabel)
			m_HotkeyFunctions.updateHotkeyForm()
		end
		
		if item and item:isPickupable() then
			m_HotkeyFunctions.clearObject()
			
			m_HotkeyList.currentHotkeyLabel.itemId = item:getId()
			if item:isMultiUse() then
				m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH
				m_HotkeyList.currentHotkeyLabel.multiUse = true
			else
				m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF
			end
			
			m_HotkeyList.currentHotkeyLabel:setOn(true)
			m_HotkeyList.currentHotkeyLabel.value = nil
			m_HotkeyList.currentHotkeyLabel.autoSend = false
			m_HotkeyFunctions.updateHotkeyLabel(m_HotkeyList.currentHotkeyLabel)
			m_HotkeyFunctions.updateHotkeyForm()
			
			local item = m_HotkeyList.currentHotkeyLabel:getChildById('item')
			item:show()
			item:setItemId(m_HotkeyList.currentHotkeyLabel.itemId)
		end
	end

	m_HotkeyFunctions.show()

	g_mouse.popCursor('target')
	self:ungrabMouse()
	return true
end

m_HotkeyFunctions.show = function()
	m_HotkeyList.window:show()
	m_HotkeyList.window:enable()
	m_HotkeyList.window:raise()
	m_HotkeyList.window:focus()
end

m_HotkeyFunctions.open = function()
	if m_HotkeyList.window then
		return true
	end
	
	m_HotkeyList.window = g_ui.displayUI('hotkeys_manager')

	m_HotkeyList.selectObjectButton = m_HotkeyList.window:getChildById('selectObjectButton')
	m_HotkeyList.removeHotkeyButton = m_HotkeyList.window:getChildById('removeHotkeyButton')
	m_HotkeyList.clearObjectButton = m_HotkeyList.window:getChildById('clearObjectButton')
	m_HotkeyList.sendAutomatically = m_HotkeyList.window:getChildById('sendAutomatically')
	m_HotkeyList.selectNextTarget = m_HotkeyList.window:getChildById('selectNextTarget')
	m_HotkeyList.useOnSelf = m_HotkeyList.window:getChildById('useOnSelf')
	m_HotkeyList.useOnTarget = m_HotkeyList.window:getChildById('useOnTarget')
	m_HotkeyList.useWith = m_HotkeyList.window:getChildById('useWith')
	m_HotkeyList.hotkeyText = m_HotkeyList.window:getChildById('hotkeyText')
	m_HotkeyList.list = m_HotkeyList.window:getChildById('list')
	
	m_HotkeyList.list.onChildFocusChange = function(self, hotkeyLabel) onSelectHotkeyLabel(hotkeyLabel) end
	g_keyboard.bindKeyPress('Down', function() m_HotkeyList.list:focusNextChild(KeyboardFocusReason) end, m_HotkeyList.window)
	g_keyboard.bindKeyPress('Up', function() m_HotkeyList.list:focusPreviousChild(KeyboardFocusReason) end, m_HotkeyList.window)
	
	m_HotkeyList.useRadioGroup = UIRadioGroup.create()
	m_HotkeyList.useRadioGroup:addWidget(m_HotkeyList.useOnSelf)
	m_HotkeyList.useRadioGroup:addWidget(m_HotkeyList.useOnTarget)
	m_HotkeyList.useRadioGroup:addWidget(m_HotkeyList.useWith)
	m_HotkeyList.useRadioGroup.onSelectionChange = function(self, selected) onChangeUseType(selected) end
	
	local hotkeys = m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()]
	if hotkeys then
		for key, v in pairs(hotkeys) do
			m_HotkeyFunctions.addKeyCombo(key, v, false, true)
		end
	end
end

m_HotkeyFunctions.save = function()
	g_settings.setNode('hotkeys', m_HotkeyFunctions.hotkeyList)
end

m_HotkeyFunctions.addDefaultKeys = function()
	local name = g_game.getCharacterName()
	local hotkeys = m_HotkeyFunctions.hotkeyList[name] or {}
	for i = 12, 1, -1 do
		hotkeys['F' .. i] = {
							autoSend = false,
							itemId = nil,
							spell = nil,
							name = nil,
							multiUse = false,
							useType = nil,
							value = nil
							}
		
		if g_game.isOnline() then
			m_HotkeyFunctions.addKeyCombo('F' .. i, nil, false)
		end
	end
	
	m_HotkeyFunctions.hotkeyList[name] = hotkeys
	m_HotkeyFunctions.save()
end

m_HotkeyFunctions.load = function(forceDefaults)
	m_HotkeyFunctions.hotkeysManagerLoaded = false
	
	if forceDefaults then
		for _, child in pairs(m_HotkeyList.list:getChildren()) do
			if child.name then
				modules.game_spellbar.m_SpellBarFunctions.updateHotkey(child.name, '')
			end
		end
		
		m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()] = {}
		m_HotkeyFunctions.addDefaultKeys()
		
		modules.game_spellbar.m_SpellBarFunctions.reset()
	else
		local hotkeySettings = g_settings.getNode('hotkeys')
		if hotkeySettings then
			m_HotkeyFunctions.hotkeyList = hotkeySettings
			
			local hotkeys = m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()]
			if hotkeys then
				for key, v in pairs(hotkeys) do
					m_HotkeyFunctions.addKeyCombo(key, v)
				end
			else
				m_HotkeyFunctions.addDefaultKeys()
			end
		else
			m_HotkeyFunctions.addDefaultKeys()
		end
	end
	
	m_HotkeyFunctions.hotkeysManagerLoaded = true
end

m_HotkeyFunctions.unload = function()
	local hotkeys = m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()]
	if hotkeys then
		for key, v in pairs(hotkeys) do
			g_keyboard.unbindKeyPress(key)
		end
	end
	
	if m_HotkeyList.list then
		m_HotkeyList.list:destroyChildren()
	end
end

m_HotkeyFunctions.addKeyCombo = function(keyCombo, keySettings, focus, ignoreBindkey)
	if not keyCombo or keyCombo == '' then
		return false
	end
	
	if m_HotkeyList.list then
		local widget = m_HotkeyList.list:getChildById(keyCombo)
		if not widget then
			widget = g_ui.createWidget('HotkeyListWidget')
			widget:setId(keyCombo)
			
			local children = m_HotkeyList.list:getChildren()
			children[#children + 1] = widget
			table.sort(children, function(a,b)
										if a:getId():len() < b:getId():len() then
											return true
										elseif a:getId():len() == b:getId():len() then
											return a:getId() < b:getId()
										else
											return false
										end
									end)
			
			for i = 1,#children do
				if children[i] == widget then
					m_HotkeyList.list:insertChild(i, widget)
					break
				end
			end

			if keySettings then
				m_HotkeyList.currentHotkeyLabel = widget
				widget.keyCombo = keyCombo
				widget.autoSend = keySettings.autoSend
				widget.autoAttack = keySettings.autoAttack
				widget.itemId = keySettings.itemId
				widget.spell = keySettings.spell
				widget.name = keySettings.name
				widget.useType = tonumber(keySettings.useType)
				widget.value = keySettings.value
				widget.multiUse = keySettings.multiUse
				
				local item = widget:getChildById('item')
				local spell = widget:getChildById('spell')
				if widget.spell then
					item:hide()
					spell:show()
					spell:setImageClip(modules.game_spellbar.m_SpellBarFunctions.getImageClip(widget.itemId))
					widget:setOn(true)
				elseif widget.itemId then
					spell:hide()
					item:show()
					item:setItemId(widget.itemId)
					widget:setOn(true)
				end
			else
				widget.keyCombo = keyCombo
				widget.autoSend = nil
				widget.autoAttack = nil
				widget.itemId = nil
				widget.spell = nil
				widget.name = nil
				widget.useType = nil
				widget.value = nil
			end

			m_HotkeyFunctions.updateHotkeyLabel(widget)
		end
	
		if focus then
			m_HotkeyList.list:focusChild(widget)
			m_HotkeyList.list:ensureChildVisible(widget)
			m_HotkeyFunctions.updateHotkeyForm()
		end
	end

	if not ignoreBindkey then
		g_keyboard.bindKeyPress(keyCombo, function() doKeyCombo(keyCombo) end)
	end
end

m_HotkeyFunctions.startChooseItem = function()
	if g_ui.isMouseGrabbed() then 
		return false
	end
	
	m_HotkeyList.window:hide()
	m_HotkeyFunctions.mouseGrabberWidget:grabMouse()
	g_mouse.pushCursor('target')
	m_HotkeyList.window:disable()
end

function onChangeUseType(useTypeWidget)
	if not m_HotkeyFunctions.hotkeysManagerLoaded then
		return false
	end
	
	if not m_HotkeyList.currentHotkeyLabel then
		return false
	end
	
	if useTypeWidget == m_HotkeyList.useOnSelf then
		m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF
	elseif useTypeWidget == m_HotkeyList.useOnTarget then
		m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET
	elseif useTypeWidget == m_HotkeyList.useWith then
		m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH
	else
		m_HotkeyList.currentHotkeyLabel.useType = m_HotkeyFunctions.HOTKEY_MANAGER_USE
	end
	
	m_HotkeyFunctions.updateHotkeyLabel(m_HotkeyList.currentHotkeyLabel)
end

function accept()
	toggle()
end

function onSelectHotkeyLabel(hotkeyLabel)
	m_HotkeyList.currentHotkeyLabel = hotkeyLabel
	m_HotkeyFunctions.updateHotkeyForm()
end

function toggle()
	if not m_HotkeyFunctions.hotkeysButton:isOn() then
		m_HotkeyFunctions.hotkeysButton:setOn(true)
		m_HotkeyFunctions.open()
	else
		m_HotkeyFunctions.hotkeysButton:setOn(false)
		m_HotkeyFunctions.destroy()
	end
end

function onHotkeyTextChange(self)
	local value = self:getText()
	if self:getText() ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
	
	if not m_HotkeyFunctions.hotkeysManagerLoaded then
		return false
	end
	
	if not m_HotkeyList.currentHotkeyLabel then
		return false
	end
	
	m_HotkeyList.currentHotkeyLabel.value = value
	if value == '' then
		m_HotkeyList.currentHotkeyLabel.autoSend = false
		m_HotkeyList.sendAutomatically:disable()
		m_HotkeyList.sendAutomatically:setChecked(false)
	else
		m_HotkeyList.sendAutomatically:enable()
	end
	
	m_HotkeyFunctions.updateHotkeyLabel(m_HotkeyList.currentHotkeyLabel)
end

function onSendAutomaticallyChange(autoSend)
	if not m_HotkeyFunctions.hotkeysManagerLoaded then
		return false
	end
	
	if not m_HotkeyList.currentHotkeyLabel then
		return false
	end
	
	if not m_HotkeyList.currentHotkeyLabel.value or #m_HotkeyList.currentHotkeyLabel.value == 0 then
		return false
	end
	
	m_HotkeyList.currentHotkeyLabel.autoSend = autoSend
	m_HotkeyFunctions.updateHotkeyLabel(m_HotkeyList.currentHotkeyLabel)
	m_HotkeyFunctions.updateHotkeyForm()
end

function onSelectNextTargetChange(autoAttack)
	if not m_HotkeyFunctions.hotkeysManagerLoaded then
		return false
	end
	
	if not m_HotkeyList.currentHotkeyLabel then
		return false
	end
	
	m_HotkeyList.currentHotkeyLabel.autoAttack = autoAttack
	m_HotkeyFunctions.updateHotkeyForm()
end

function hotkeyCaptureCancel()
	if m_HotkeyFunctions.assignWindow then
		m_HotkeyFunctions.assignWindow:destroy()
		m_HotkeyFunctions.assignWindow = nil
	end
end

function hotkeyCaptureOk(keyCombo)
	m_HotkeyFunctions.addKeyCombo(keyCombo, nil, true)
	hotkeyCaptureCancel()
end

function hotkeyCapture(assignWindow, keyCode, keyboardModifiers)
	local keyCombo = determineKeyComboDesc(keyCode, keyboardModifiers)
	local comboPreview = m_HotkeyFunctions.assignWindow:getChildById('comboPreview')
	comboPreview:setText(keyCombo)
	comboPreview.keyCombo = keyCombo
	comboPreview:resizeToText()
	m_HotkeyFunctions.assignWindow:getChildById('addButton'):enable()
	return true
end

function removeHotkey()
	if not m_HotkeyList.currentHotkeyLabel then
		return false
	end
	
	if m_HotkeyList.currentHotkeyLabel.name then
		modules.game_spellbar.m_SpellBarFunctions.updateHotkey(m_HotkeyList.currentHotkeyLabel.name, '')
	end
	
	g_keyboard.unbindKeyPress(m_HotkeyList.currentHotkeyLabel.keyCombo)
	m_HotkeyList.currentHotkeyLabel:destroy()
	m_HotkeyList.currentHotkeyLabel = nil
	
	local widget = m_HotkeyList.list:getFirstChild()
	widget:focus()
end

function addHotkey()
	if not m_HotkeyFunctions.assignWindow then
		m_HotkeyFunctions.assignWindow = g_ui.createWidget('HotkeyAssignWindow', rootWidget)
	else
		m_HotkeyFunctions.assignWindow:focus()
	end
	
	m_HotkeyFunctions.assignWindow:grabKeyboard()

	local comboLabel = m_HotkeyFunctions.assignWindow:getChildById('comboPreview')
	comboLabel.keyCombo = ''
	m_HotkeyFunctions.assignWindow.onKeyDown = hotkeyCapture
end

function doKeyCombo(keyCombo)
	if not g_game.isOnline() or m_HotkeyList.window then 
		return true
	end
	
	local hotkeys = m_HotkeyFunctions.hotkeyList[g_game.getCharacterName()]
	if not hotkeys then
		return true
	end
	
	local hotKey = hotkeys[keyCombo]
	if not hotKey then 
		return 
	end
	
	if timeMillis() - m_HotkeyFunctions.lastHotkeyUse < 100 then
		return
	end
	
	m_HotkeyFunctions.lastHotkeyUse = timeMillis()
	
	if modules.game_console.m_ConsoleFunctions.enabledChat() then
		local canUse = false
		for i = 1, 12 do
			if keyCombo:find('F' .. i) then
				canUse = true
			end
		end
		
		if not canUse then
			return
		end
	end
	
	local isMovementKey = false
	if hotKey.value == "Walk North" then
		g_game.walk(North)
		isMovementKey = true
	end
	
	if hotKey.value == 'Walk South' then
		g_game.walk(South)
		isMovementKey = true
	end
	
	if hotKey.value == 'Walk East' then
		g_game.walk(East)
		isMovementKey = true
	end
	
	if hotKey.value == 'Walk West' then
		g_game.walk(West)
		isMovementKey = true
	end
	
	if not isMovementKey then
		if hotKey.autoAttack then
			local fromPos = g_game.getLocalPlayer():getPosition()
			local distance = 255
			local target = nil
			
			local attackingCreature = g_game.getAttackingCreature()
			for _, creature in ipairs(g_map.getSpectators(fromPos, false)) do
				if creature ~= attackingCreature and not creature:isKnown() and not creature:isHideHealth() and creature:isMonster() and creature:getSkull() ~= SkullGreen and creature:getSkull() ~= SkullOrange then
					local range = m_HotkeyFunctions.getDistanceBetween(fromPos, creature:getPosition())
					if range < distance then
						target = creature
						distance = range
						if range <= 1 then
							break
						end
					end
				end
			end
			
			if target then
				g_game.attack(target)
			end
		end
		
		if hotKey.spell and hotKey.name then
			local useWith = true
			if modules.game_partypanel.getSelectedMember() then
				if modules.game_spellbar.m_SpellBarFunctions.isSupportSpellByName(hotKey.name) then
					useWith = false
				end
			end
			
			if hotKey.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH and useWith then
				local area, range, ignoreRange = modules.game_spellbar.m_SpellBarFunctions.getAreaByName(hotKey.name)
				modules.game_interface.startUseWith(hotKey, true, area, range, false, ignoreRange)
			else
				modules.game_spellbar.m_SpellBarFunctions.onClickSpell(hotKey.name, hotKey)
			end
		elseif not hotKey.itemId then
			if not hotKey.value or #hotKey.value == 0 then 
				return false
			end
			
			if hotKey.autoSend then
				modules.game_console.m_ConsoleFunctions.sendMessage(hotKey.value)
			else
				modules.game_console.m_ConsoleFunctions.setTextEditText(hotKey.value)
			end
		elseif hotKey.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USE then
			g_game.useInventoryItemWith(hotKey.itemId)
		elseif hotKey.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONSELF then
			g_game.useInventoryItemWith(hotKey.itemId, g_game.getLocalPlayer())
		elseif hotKey.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEONTARGET then
			local attackingCreature = g_game.getAttackingCreature()
			if not attackingCreature then
				return false
			end
			
			g_game.useInventoryItemWith(hotKey.itemId, attackingCreature)
		elseif hotKey.useType == m_HotkeyFunctions.HOTKEY_MANAGER_USEWITH then
			local item = Item.create(hotKey.itemId)
			modules.game_interface.startUseWith(item)
		end
	end
end

function onGameStart()
	m_HotkeyFunctions.load()
	
	if m_HotkeyFunctions.hotkeysButton then
		m_HotkeyFunctions.hotkeysButton:show()
	end
end

function onGameEnd()
	m_HotkeyFunctions.unload()
	m_HotkeyFunctions.destroy()
	
	m_HotkeyFunctions.hotkeyList = {}
	
	if m_HotkeyFunctions.hotkeysButton then
		m_HotkeyFunctions.hotkeysButton:hide()
	end
end

function onLoad()
	connect(g_game, { 
					onGameStart = onGameStart,
					onGameEnd = onGameEnd
					})
	
	g_keyboard.bindKeyDown('Ctrl+K', toggle)
	
	m_HotkeyFunctions.mouseGrabberWidget = g_ui.createWidget('UIWidget')
	m_HotkeyFunctions.mouseGrabberWidget:hide()
	m_HotkeyFunctions.mouseGrabberWidget:setFocusable(false)
	m_HotkeyFunctions.mouseGrabberWidget.onMouseRelease = m_HotkeyFunctions.onChooseItemMouseRelease
	
	if not m_HotkeyFunctions.hotkeysButton then
		m_HotkeyFunctions.hotkeysButton = modules.client_topmenu.addTopButton('HotkeyButton', 'hotkeysButton', tr('Hotkeys') .. ' (Ctrl+K)', toggle)
	end
	
	if g_game.isOnline() then
		m_HotkeyFunctions.load()
	end
end

function onUnload()
	disconnect(g_game, {
						onGameStart = onGameStart,
						onGameEnd = onGameEnd
						})

	g_keyboard.unbindKeyDown('Ctrl+K')
	
	onGameEnd()
	if m_HotkeyFunctions.hotkeysButton then
		m_HotkeyFunctions.hotkeysButton:destroy()
		m_HotkeyFunctions.hotkeysButton = nil
	end
	
	if m_HotkeyFunctions.mouseGrabberWidget then
		m_HotkeyFunctions.mouseGrabberWidget:destroy()
		m_HotkeyFunctions.mouseGrabberWidget = nil
	end
	
end
