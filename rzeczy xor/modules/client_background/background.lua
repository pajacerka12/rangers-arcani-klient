local background = nil
local clientVersionLabel = nil
local changeLogPanel = nil
local languagePanel = nil
local descriptionBox = nil
local dateBox = nil
local prevPage = nil
local nextPage = nil
local path = '/news/changelog.lua'

local PAGE_PREV = 1
local PAGE_NEXT = 2

local data = {}
local currentPage = 1

local function setDescription()
	local var = data[currentPage]
	if var then
		descriptionBox:destroyChildren()
		dateBox:setText(var[1])
		for i = 1, #var[2] do
			local line = var[2][i]
			local widget = g_ui.createWidget('ChangelogLabel', descriptionBox)
			if line:find('#') then
				line = string.gsub(line, '#', '')
				widget:setOn(true)
			end
			
			widget:setParent(descriptionBox)
			widget:setText(line)
		end
	end
	
	if currentPage <= 1 then
		prevPage:hide()
	else
		prevPage:show()
	end
	
	if currentPage >= #data then
		nextPage:hide()
	else
		nextPage:show()
	end
end

function updateCloseButton(value)
	background:getChildById('close'):setVisible(value)
end

function selectLanguage(self)
	if self == languagePanel then
		if not self:isOn() then
			self:setHeight(128)
			flag2:show()
		else
			self:setHeight(64)
			flag2:hide()
		end
		
		self:setOn(not self:isOn())
	else
		selectLanguage(languagePanel)
		if self:isOn() then
			modules.client_locales.selectFirstLocale('pl')
		else
			modules.client_locales.selectFirstLocale('en')
		end
		
		setTip(tr(onLoginTip))
	end
end

function setPage(var)
	if var == PAGE_NEXT then
		currentPage = math.min(#data, currentPage + 1)
	elseif var == PAGE_PREV then
		currentPage = math.max(1, currentPage - 1)
	end
	
	setDescription()
end

function init()
	background = g_ui.displayUI('background')
	background:lower()
	
	changeLogPanel = background:getChildById('changelog')
	descriptionBox = changeLogPanel:getChildById('description')
	dateBox = changeLogPanel:getChildById('date')
	prevPage = changeLogPanel:getChildById('prevPage')
	nextPage = changeLogPanel:getChildById('nextPage')
	
	languagePanel = background:getChildById('languagePanel')
	flag1 = languagePanel:getChildById('flag1')
	flag2 = languagePanel:getChildById('flag2')
	
	local description = g_resources.readFileContents(path)
	
	local strValue = description:explode('|')
	for i = #strValue, 1, -1 do
		local dataValue = strValue[i]:explode('@')
		local list = dataValue[2]:explode('\n')
		
		data[i] = {dataValue[1], list}
	end
	
	setDescription()
	
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		flag1:setOn(false)
		flag2:setOn(true)
	else
		flag1:setOn(true)
		flag2:setOn(false)
	end
	
	connect(g_game, { onGameStart = hide })
	connect(g_game, { onGameEnd = show })
end

function terminate()
	disconnect(g_game, { onGameStart = hide })
	disconnect(g_game, { onGameEnd = show })

	background:destroy()
	background = nil
	
	languagePanel = nil
	changeLogPanel = nil
	descriptionBox = nil
	dateBox = nil
end

function hide()
	background:hide()
end

function show()
	background:show()
end

function setAudio(value)
	setEnableAudio(value)
	modules.client_options.setOption('enableAudio', value, true)
end

function setEnableAudio(value)
	background:getChildById('audio'):setOn(value)
end

function setTip(description)
	background:getChildById('tip'):setText(description)
end

function showLogo()
	background:getChildById('logo'):show()
end

function hideLogo()
	background:getChildById('logo'):hide()
end
