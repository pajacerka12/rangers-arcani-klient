local labelWindow = nil
local containerList = {}

GetOnlyItem = 1
GetMoney = 2
GetAll = 3

ButtonGold = 1
ButtonBackpack = 2

function init()
	g_ui.importStyle('container')

	connect(Container, {
		onOpen = onContainerOpen,
		onClose = onContainerClose,
		onAddItem = onContainerAddItem,
		onUpdateItem = onContainerUpdateItem,
		onRemoveItem = onContainerRemoveItem,
		onUpdate = onUpdate
	})
	
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})

	reloadContainers()
end

function terminate()
	disconnect(Container, {
		onOpen = onContainerOpen,
		onClose = onContainerClose,
		onAddItem = onContainerAddItem,
		onUpdateItem = onContainerUpdateItem,
		onRemoveItem = onContainerRemoveItem,
		onUpdate = onUpdate
	})
	
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	if labelWindow then
		labelWindow:destroy()
		labelWindow = nil
	end
	
	onGameEnd()
end

function clean()
	for containerid, container in pairs(g_game.getContainers()) do
		destroy(container)
	end
	
	if labelWindow then
		labelWindow:destroy()
		labelWindow = nil
	end
end

function reloadContainers()
	clean()
	for _,container in pairs(g_game.getContainers()) do
		onContainerOpen(container)
	end
end

function onGameStart()
	containerList = {}
	containerList = g_settings.getNode('containerConfigList', false)
	
	if not containerList then
		containerList = {}
	end
end

function onGameEnd()
	local localPlayer = g_game.getLocalPlayer()
	if localPlayer then
		local name = localPlayer:getName()
		
		containerList[name] = {}
		for containerid, container in pairs(g_game.getContainers()) do
			if not container.window.isCorpseItem then
				containerList[name][tostring(containerid)] = {
					position = container.window:getPosition(),
					size = container.window:getSize(),
					parentId = container.window:getParent():getId(),
					spriteId = container:getContainerItem():getId()
				}
			end
		end
		
		g_settings.setNode('containerConfigList', containerList)
	end
	
	clean()
end

function isOpenQuestContainer()
	return questItemsContainers
end

function destroy(container)
	if container and container.window then
		if container:getContainerItem():getId() == 13648 then
			questItemsContainers = false
		end
		
		if container.window.isCorpseItem then
			g_settings.setNode('lootWindowPosition', container.window:getPosition())
			g_settings.set('lootWindowParent', container.window:getParent():getId())
		else
			local name = g_game.getLocalPlayer():getName()
			if containerList[name] then
				containerList[name][tostring(container:getId())] = nil
			end
		end
		
		container.window:destroy()
		container.window = nil
		container.itemsPanel = nil
	end
end

function setItemButton(widget, slot, item)
	local itemButton = g_ui.createWidget('ItemButon', widget)
	itemButton.onClick = function()
		g_game.sendRequestAction(item, GetOnlyItem)
	end
end

function getContainerItem(container, slot)
	return container:getItem(slot)
end

function refreshContainerItems(container, isCorpse)
	for slot = 0, container:getCapacity() - 1 do
		local item = getContainerItem(container, slot)
		
		local widget = container.itemsPanel:getChildById('item' .. slot)
		local label = widget:getChildById('label')
		
		local corner = widget:getChildById('corner')
		local star = widget:getChildById('star')
		if item then
			local color = item:getLabelColor()
			if item:isLabelable() and color ~= 0 then
				label:show()
				label:setImageColor(getOufitColor(77 + color))
				label:setText(item:getLabelSymbol())
			else
				label:hide()
			end
			
			updateItemQuality(widget, item:getClassId())
			modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem(item:getId())
			star:setVisible(item:isInFavorites())
		else
			label:hide()
			corner:hide()
			star:hide()
			widget:setImageClip(getFrameClipByClassId(ITEMCLASS_IGNORE))
		end
		
		widget:setItem(item)
		local itemButton = widget:getChildById('itemButton')
		if itemButton then
			itemButton:destroy()
		end
		
		if item and isCorpse then
			setItemButton(widget, slot, item)
		end
	end
end

local function onUpdateFavoriteStar(container, value)
	for slot = 0, container:getCapacity() - 1 do
		local item = getContainerItem(container, slot)
		
		local widget = container.itemsPanel:getChildById('item' .. slot)
		local star = widget:getChildById('star')
		if value and item then
			star:setVisible(item:isInFavorites())
		else
			star:hide()
		end
	end
end

local function onUpdateCorner(container, value)
	for slot = 0, container:getCapacity() - 1 do
		local item = getContainerItem(container, slot)
		
		local widget = container.itemsPanel:getChildById('item' .. slot)
		local corner = widget:getChildById('corner')
		if value and item then
			local classId = item:getClassId()
			if classId ~= ITEMCLASS_IGNORE then
				corner:setImageClip(getImageClipByClassId(classId))
				corner:show()
			else
				corner:hide()
			end
		else
			corner:hide()
		end
	end
end

local function onUpdateFrames(container, value)
	for slot = 0, container:getCapacity() - 1 do
		local item = getContainerItem(container, slot)
		local widget = container.itemsPanel:getChildById('item' .. slot)
		if item then
			widget:setImageClip(getFrameClipByClassId(value and item:getClassId() or ITEMCLASS_IGNORE))
		end
	end
end

function updateFavoriteStars(value)
	for _,container in pairs(g_game.getContainers()) do
		onUpdateFavoriteStar(container, value)
	end
end

function updateCorners(value)
	for _,container in pairs(g_game.getContainers()) do
		onUpdateCorner(container, value)
	end
end

function updateFrames(value)
	for _,container in pairs(g_game.getContainers()) do
		onUpdateFrames(container, value)
	end
end

function onContainerOpen(container, previousContainer, isCorpse, favorite)
	local containerWindow
	local id = container:getId()
	if isCorpse then
		previousContainer = false
	end
	
	if previousContainer then
		containerWindow = previousContainer.window
		previousContainer.window = nil
		previousContainer.itemsPanel = nil
	else
		containerWindow = g_ui.createWidget('ContainerWindow')
		if isCorpse then
			local parentId = g_settings.get('lootWindowParent', 'gameRootPanel')
			local parent = nil
			if parentId == 'gameRootPanel' then
				parent = modules.game_interface.getRootPanel()
			elseif parentId == 'gameRightPanel' then
				parent = modules.game_interface.getRightPanel()
			elseif parentId == 'gameNextRightPanel' then
				parent = modules.game_interface.getNextRightPanel()
			elseif parentId == 'gameLeftPanel' then
				parent = modules.game_interface.getLeftPanel()
			elseif parentId == 'gameNextLeftPanel' then
				parent = modules.game_interface.getNextLeftPanel()
			end
			
			containerWindow.isCorpseItem = true
			if parentId == 'gameRootPanel' then
				containerWindow:setParent(parent)
				local position = g_settings.getNode('lootWindowPosition', false)
				if not position then
					local size = g_window.getSize()
					position = {x = (size.width / 2) - (containerWindow:getWidth() / 2), y = size.height - 96}
				end
				
				containerWindow:setPosition(position)
			else
				local index = -1
				for i = 1, parent:getChildCount() do
					local widget = parent:getChildByIndex(i)
					if widget.isCorpseItem then
						index = i
						break
					end
				end
				
				if index == -1 then
					containerWindow:setParent(parent)
				else
					parent:insertChild(index, containerWindow)
				end
			end
		end
	end
	
	containerWindow:setId('container' .. id)
	
	local containerPanel = containerWindow:getChildById('contentsPanel')
	local containerItemWidget = containerWindow:getChildById('containerItemWidget')
	containerWindow.onClose = function()
		if containerWindow.quest then
			questItemsContainers = false
		end
		
		g_game.close(container)
		containerWindow:hide()
		if containerWindow.isCorpseItem then
			g_settings.setNode('lootWindowPosition', containerWindow:getPosition())
			g_settings.set('lootWindowParent', containerWindow:getParent():getId())
		end
	end

	-- this disables scrollbar auto hiding
	if isCorpse then
		for i = ButtonGold, ButtonBackpack do
			local widget = g_ui.createWidget((i == ButtonGold and 'ButtonGold' or 'ButtonBackpack'), containerWindow)
			widget:addAnchor(AnchorVerticalCenter, 'upButton', AnchorVerticalCenter)
			widget:addAnchor(AnchorRight, 'upButton', (container:hasParent() and AnchorLeft or AnchorRight))
			widget:setId('button' .. i)
			if i == ButtonBackpack then
				widget:setMarginRight(18)
			else
				widget:setMarginRight(2)
			end
			
			widget.onClick = function()
				g_game.sendRequestAction(getContainerItem(container, 0), i + 1)
			end
		end
	else
		for i = ButtonGold, ButtonBackpack do
			local widget = containerWindow:getChildById('button' .. i)
			if widget then
				widget:destroy()
			end
		end
	
		local scrollbar = containerWindow:getChildById('miniwindowScrollBar')
		scrollbar:mergeStyle({ ['$!on'] = { }})
	end
	
	local upButton = containerWindow:getChildById('upButton')
	upButton.onClick = function()
		g_game.openParent(container)
	end
	upButton:setVisible(container:hasParent())
	
	local name = container:getName()
	if name:len() > (upButton:isVisible() and 15 or 18) and name:find(' ') then
		containerWindow:setText(name:sub(1, name:find(' ') - 1) .. '...')
	else
		containerWindow:setText(name)
	end
	
	local containerId = container:getContainerItem()
	containerItemWidget:setItem(containerId)
	containerItemWidget:setPhantom(true)
	
	if containerId:getId() == 13648 then
		questItemsContainers = true
		containerWindow.quest = true
	end
	
	containerPanel:destroyChildren()
	for slot = 0, container:getCapacity() - 1 do
		local item = getContainerItem(container, slot)
		local widget = g_ui.createWidget('ContainerItem', containerPanel)
		widget:setId('item' .. slot)
		widget:setItem(item)
		widget:setMargin(0)
		widget.position = container:getSlotPosition(slot)
		
		local label = widget:getChildById('label')
		local corner = widget:getChildById('corner')
		local star = widget:getChildById('star')
		if item then
			local color = item:getLabelColor()
			if item:isLabelable() and color ~= 0 then
				label:show()
				label:setImageColor(getOufitColor(77 + color))
				label:setText(item:getLabelSymbol())
			else
				label:hide()
			end
			
			updateItemQuality(widget, item:getClassId())
			star:setVisible(item:isInFavorites())
		else
			label:hide()
			corner:hide()
			star:hide()
			widget:setImageClip(getFrameClipByClassId(ITEMCLASS_IGNORE))
		end
		
		if item and isCorpse then
			setItemButton(widget, slot, item)
		end
	end
	
	container.window = containerWindow
	container.itemsPanel = containerPanel
	
	local layout = containerPanel:getLayout()
	local cellSize = layout:getCellSize()
	local filledLines = math.max(math.ceil(container:getItemsCount() / 4), 1)
	containerWindow:setContentMinimumHeight(cellSize.height + 2)
	containerWindow:setContentMaximumHeight(cellSize.height * layout:getNumLines())
	
	if not previousContainer then
		local height = filledLines * cellSize.height
		containerWindow:setContentHeight(height + 2)
	end
	
	if not isCorpse then
		modules.game_interface.addToPanels(containerWindow, previousContainer)
	end
	
	containerWindow:setup()
	
	if not isCorpse then
		local config = containerList[g_game.getLocalPlayer():getName()]
		if config then
			local tmpContainer = config[tostring(id)]
			if tmpContainer then
				containerWindow:setSize(tmpContainer.size)
				
				local rootWidget = modules.game_interface.getRootPanel()
				if tmpContainer.position.x + containerWindow:getWidth() > rootWidget:getWidth() then
					tmpContainer.position.x = tmpContainer.position.x - containerWindow:getWidth()
				end
				
				if tmpContainer.position.y + containerWindow:getHeight() > rootWidget:getHeight() then
					tmpContainer.position.y = tmpContainer.position.y - containerWindow:getHeight()
				end
				
				containerWindow:setPosition(tmpContainer.position)
				
				local parent = nil
				if tmpContainer.parentId == 'gameRootPanel' then
					parent = rootWidget
				elseif tmpContainer.parentId == 'gameRightPanel' then
					parent = modules.game_interface.getRightPanel()
				elseif tmpContainer.parentId == 'gameNextRightPanel' then
					parent = modules.game_interface.getNextRightPanel()
				elseif tmpContainer.parentId == 'gameLeftPanel' then
					parent = modules.game_interface.getLeftPanel()
				elseif tmpContainer.parentId == 'gameNextLeftPanel' then
					parent = modules.game_interface.getNextLeftPanel()
				end
				
				containerWindow:setParent(parent)
			end
		end
	end
end

function onContainerClose(container)
	if not container.window then return end
	
	destroy(container)
end

function onContainerAddItem(container, slot, item, isCorpse, favorite)
	if not container.window then return end
	
	refreshContainerItems(container, isCorpse)
end

function onContainerUpdateItem(container, slot, item, oldItem, favorite)
	if not container.window then return end
	
	local widget = container.itemsPanel:getChildById('item' .. slot)
	widget:setItem(item)
	updateItemQuality(widget, item:getClassId())
	
	local star = widget:getChildById('star')
	if star then
		star:setVisible(favorite)
	end
end

function onContainerRemoveItem(container, slot, item, isCorpse)
	if not container.window then return end
	
	refreshContainerItems(container, isCorpse)
end

function onUpdate(container, isCorpse)
	if not container.window then return end
	
	refreshContainerItems(container, isCorpse)
end

local COLOURS = 16
local selectedColor = nil

function onAddLabel()
	if not labelWindow then return end
	
	g_game.removeLabel(labelWindow.item)
	g_game.addLabel(labelWindow.item, labelWindow.color, labelWindow.symbol)
	
	local color = labelWindow.item:getLabelColor()
	local symbol = labelWindow.item:getLabelSymbol()
	
	selectedColor = nil
	labelWindow:destroy()
	labelWindow = nil
end

function onRemoveLabel(item)
	g_game.removeLabel(item)
end

function selectColor(self)
	if selectedColor then
		selectedColor:getChildById('select'):hide()
	end
	
	selectedColor = self
	selectedColor:getChildById('select'):show()
	
	labelWindow.color = self.color
end

function onSymbolChange(self)
	local text = self:getText()
	if text:len() > 2 then
		self:setText(text:sub(2, 3):upper())
	else
		self:setText(text:upper())
	end
	
	if labelWindow then
		labelWindow.symbol = self:getText()
	end
end

function onCloseLabel()
	if labelWindow then
		selectedColor = nil
		
		labelWindow:destroy()
		labelWindow = nil
	end
end

function onCreateLabel(item)
	if not labelWindow then
		labelWindow = g_ui.displayUI('ContainerLabel')
		labelWindow.item = item
		
		local colors = labelWindow:getChildById('colors')
		for i = 1, COLOURS do
			local box = colors:getChildById('color' .. i)
			box:setBackgroundColor(getOufitColor(77 + i))
			box.color = i
			
			if i == 1 then
				selectColor(box)
			end
		end
	end
end
