m_PlayerDeathFunctions = {}
m_PlayerDeathList = {}

function onLoad()
	g_ui.importStyle('deathwindow')
	connect(g_game, {
		onDeath = onDeath,
		onGameEnd = onGameEnd
	})
end

function onUnload()
	disconnect(g_game, {
		onDeath = onDeath,
		onGameEnd = onGameEnd
	})
	
	onGameEnd()
end

function onGameEnd()
	if m_PlayerDeathList.window then
		m_PlayerDeathList.window:destroy()
		m_PlayerDeathList = {}
		
		modules.game_interface.getMapPanel():setDrawLights(true)
	end
end

function onDeath()
	m_PlayerDeathFunctions.display()
	m_PlayerDeathFunctions.open()
end

m_PlayerDeathFunctions.ok = function()
	CharacterList.doLogin()
	onGameEnd()
end

m_PlayerDeathFunctions.cancel = function()
	g_game.safeLogout()
	onGameEnd()
end

m_PlayerDeathFunctions.display = function()
	local advanceLabel = modules.game_interface.getRootPanel():recursiveGetChildById('middleCenterLabel')
	if advanceLabel:isVisible() then
		return true
	end
	
	modules.game_textmessage.displayGameMessage(tr('You are dead.'))
end

m_PlayerDeathFunctions.open = function()
	if m_PlayerDeathList.window then
		onGameEnd()
		return true
	end
	
	m_PlayerDeathList.window = g_ui.createWidget('DeathWindow', modules.game_interface.getRootPanel())
	m_PlayerDeathList.window:breakAnchors()
	modules.game_interface.getMapPanel():setDrawLights(false)
end
