MessageSettings = {
  none            = {},
  npcTo           = {color = TextColors.npcto, screenTarget='privateLabel'},
  npcFrom         = {color = TextColors.npcfrom, screenTarget='privateLabel'},
  
  consoleRed      = {color = TextColors.red,    consoleTab='Default' },
  consoleOrange   = {color = TextColors.orange, consoleTab='Default' },
  consoleBlue     = {color = TextColors.blue, 	consoleTab='Default' },
  centerRed       = {color = TextColors.red,	consoleTab='Server Log', screenTarget='lowCenterLabel' },
  centerGreen     = {color = TextColors.white, 	consoleTab='Server Log', screenTarget='middleCenterLabel',   consoleOption='showInfoMessagesInConsole'},
  centerWhite     = {color = TextColors.white, 	consoleTab='Server Log', screenTarget='middleCenterLabel', consoleOption='showEventMessagesInConsole'},
  bottomWhite     = {color = TextColors.white, 	consoleTab='Server Log', screenTarget='lowCenterLabel',       consoleOption='showEventMessagesInConsole'},
  status          = {color = TextColors.white, 	consoleTab='Server Log', screenTarget='lowCenterLabel',       consoleOption='showStatusMessagesInConsole'},
  statusSmall     = {color = TextColors.white, 	screenTarget='lowCenterLabel'},
  private         = {color = TextColors.lightblue, screenTarget='privateLabel'},

  darkGreen    	  = {color = TextColors.darkGreen, 	consoleTab='Server Log', screenTarget='highCenterLabel',   consoleOption='showInfoMessagesInConsole'},
  normGreen    	  = {color = TextColors.normGreen, 	consoleTab='Server Log', screenTarget='highCenterLabel',   consoleOption='showInfoMessagesInConsole'},
  normWhite    	  = {color = TextColors.normWhite, 		consoleTab='Server Log', screenTarget='highCenterLabel',   consoleOption='showInfoMessagesInConsole'},
  lightGreen      = {color = TextColors.lightGreen, consoleTab='Server Log', screenTarget='highCenterLabel',   consoleOption='showInfoMessagesInConsole'},
  whiteGreen      = {color = TextColors.whiteGreen, consoleTab='Server Log', screenTarget='highCenterLabel',   consoleOption='showInfoMessagesInConsole'},
  normOrange      = {color = TextColors.normOrange, consoleTab='Server Log', screenTarget='highCenterLabel',   consoleOption='showInfoMessagesInConsole'},
}

MessageTypes = {
  [MessageModes.NpcTo] = MessageSettings.none,
  [MessageModes.NpcFrom] = MessageSettings.npcFrom,
  
  [MessageModes.MonsterSay] = MessageSettings.consoleOrange,
  [MessageModes.MonsterYell] = MessageSettings.consoleOrange,
  [MessageModes.BarkLow] = MessageSettings.consoleOrange,
  [MessageModes.BarkLoud] = MessageSettings.consoleOrange,
  [MessageModes.Failure] = MessageSettings.statusSmall,
  [MessageModes.Login] = MessageSettings.bottomWhite,
  [MessageModes.Game] = MessageSettings.centerWhite,
  [MessageModes.Status] = MessageSettings.status,
  [MessageModes.Warning] = MessageSettings.centerRed,
  [MessageModes.Look] = MessageSettings.centerGreen,
  [MessageModes.Loot] = MessageSettings.centerGreen,
  -- [MessageModes.Red] = MessageSettings.consoleRed,
  -- [MessageModes.Blue] = MessageSettings.consoleBlue,
  [MessageModes.PrivateFrom] = MessageSettings.consoleBlue,
  
  -- [MessageModes.darkGreen] = MessageSettings.darkGreen,
  -- [MessageModes.normGreen] = MessageSettings.normGreen,
  -- [MessageModes.normWhite] = MessageSettings.normWhite,
  -- [MessageModes.lightGreen] = MessageSettings.lightGreen,
  -- [MessageModes.whiteGreen] = MessageSettings.whiteGreen,
  -- [MessageModes.normOrange] = MessageSettings.normOrange,

  [MessageModes.DamageDealed] = MessageSettings.status,
  [MessageModes.DamageReceived] = MessageSettings.status,
  [MessageModes.Heal] = MessageSettings.status,
  [MessageModes.Exp] = MessageSettings.status,

  [MessageModes.DamageOthers] = MessageSettings.none,
  [MessageModes.HealOthers] = MessageSettings.none,
  [MessageModes.ExpOthers] = MessageSettings.none,

  [MessageModes.TradeNpc] = MessageSettings.centerWhite,
  [MessageModes.Guild] = MessageSettings.centerWhite,
  [MessageModes.PartyManagement] = MessageSettings.centerWhite,
  [MessageModes.TutorialHint] = MessageSettings.centerWhite,
  [MessageModes.Market] = MessageSettings.centerWhite,
  [MessageModes.BeyondLast] = MessageSettings.centerWhite,
  [MessageModes.Report] = MessageSettings.consoleRed,
  [MessageModes.HotkeyUse] = MessageSettings.centerGreen,

  [254] = MessageSettings.private
}

local messages = {}
messages[MessagePosition.Right] = {label = nil, event = nil}
messages[MessagePosition.Left] = {label = nil, event = nil}

local messageHistory = 5

messagesPanel = nil

function init()
	connect(g_game, { 
		onTextMessage = displayServerMessage,
		onGameEnd = clearMessages
	})
	
	messagesPanel = g_ui.loadUI('textmessage', modules.game_interface.getRootPanel())
end

function terminate()
	disconnect(g_game, {
		onTextMessage = displayServerMessage,
		onGameEnd = clearMessages
	})
	
	clearMessages()
	messagesPanel:destroy()
end

function calculateVisibleTime(text)
	return math.min(math.max(#text * 100, 4000), 18000)
end

function getMessageTypeNameById(messageType)
	if messageType == MessageType.SummonDeal or messageType == MessageType.SummonDealCritical or messageType == MessageType.SummonDealDeathHit then
		return 'showSummonDamageDeal'
	elseif messageType == MessageType.SummonRecived or messageType == MessageType.SummonRecivedCritical or messageType == MessageType.SummonRecivedDeathHit then
		return 'showSummonDamageRecived'
	elseif messageType == MessageType.PlayerDeal or messageType == MessageType.PlayerDealCritical or messageType == MessageType.PlayerDealDeathHit then
		return 'showDamageDeal'
	elseif messageType == MessageType.HealHealth or messageType == MessageType.HealHealthCritical or messageType == MessageType.HealHealthDeathHit then
		return 'showHealingDamage'
	elseif messageType == MessageType.PlayerRecivedHealth then
		return 'showDamageRecived'
	elseif messageType == MessageType.PlayerRecivedMana then
		return 'showManaRecived'
	elseif messageType == MessageType.StolenHealth then
		return 'showStealHealth'
	elseif messageType == MessageType.StolenMana then
		return 'showStealMana'
	elseif messageType == MessageType.Loot then
		return 'showLootMessage'
	elseif messageType == MessageType.Experience then
		return 'showExperienceGain'
	elseif messageType == MessageType.Advance then
		return 'showAdvanceMessage'
	end
	
	return false
end

function displayServerMessage(mode, messageType, messagePosition, text, combatType)
	if mode == MessageModes.MessagePrivate then
		return true
	end
	
	displayMessage(mode, messageType, messagePosition, text, combatType)
end

function displayMessage(mode, messageType, messagePosition, text, combatType)
	if not g_game.isOnline() then 
		return false
	end
	
	if text:find('plL!|vip)H1fZZ/') then
		modules.game_house.sendMessage(text:sub(16, text:len()))
		return false
	elseif text:find('blL!|vip)H1fZZ/') then
		modules.game_store.sendMessage(text:sub(16, text:len()))
		return false
	elseif text:find('ydPyxZXe/N#F#Nu/') then
		modules.game_encyclopedia.m_BestiaryFunctions.sendMessage(text:sub(17, text:len()))
		return false
	elseif text:find('P6NHglaRBbiBu#H/') then
		modules.game_character.m_CharmsFunctions.sendMessage(text:sub(17, text:len()))
		return false
	end
	
	local name = getMessageTypeNameById(messageType)
	if name and not modules.client_options.getOption(name) then
		return false
	end
	
	local msgtype = {}
	if mode == MessageModes.MessageDefault then
		msgtype.consoleTab = 'Default'
	elseif mode == MessageModes.MessageServerLog then
		msgtype.consoleTab = 'Server Log'
	elseif mode == MessageModes.MessagePrivate then
		msgtype.screenTarget = 'privateLabel'
	end
	
	if messagePosition == MessagePosition.Top then
		msgtype.screenTarget = 'highCenterLabel'
	elseif messagePosition == MessagePosition.Middle then
		msgtype.screenTarget = 'middleCenterLabel'
	elseif messagePosition == MessagePosition.Bottom then
		msgtype.screenTarget = 'lowCenterLabel'
	end
	
	if messagePosition == MessagePosition.Right then
		msgtype.textTarget = 'rightTextMessagePanel'
	elseif messagePosition == MessagePosition.Left then
		msgtype.textTarget = 'leftTextMessagePanel'
	end
	
	if messageType == MessageType.None then
		msgtype.color = '#FFFFFF'
	elseif messageType == MessageType.SummonDeal or messageType == MessageType.SummonDealCritical or messageType == MessageType.SummonDealDeathHit then
		msgtype.color = '#FFA500'
		msgtype.subConsoleTab = 'Damage Log'
	elseif messageType == MessageType.SummonRecived or messageType == MessageType.SummonRecivedCritical or messageType == MessageType.SummonRecivedDeathHit then
		msgtype.color = '#553600'
		msgtype.subConsoleTab = 'Damage Log'
	elseif messageType == MessageType.PlayerDeal or messageType == MessageType.PlayerDealCritical or messageType == MessageType.PlayerDealDeathHit then
		msgtype.color = '#CC0000'
		msgtype.subConsoleTab = 'Damage Log'
	elseif messageType == MessageType.HealHealth or messageType == MessageType.HealHealthCritical or messageType == MessageType.HealHealthDeathHit then
		msgtype.color = '#FF8080'
	elseif messageType == MessageType.PlayerRecivedHealth then
		msgtype.color = '#FF3333'
		msgtype.subConsoleTab = 'Damage Log'
	elseif messageType == MessageType.PlayerRecivedMana then
		msgtype.color = '#4D4DFF'
	elseif messageType == MessageType.PlayerBlocked then
		msgtype.color = '#CCCC00'
	elseif messageType == MessageType.HealMana then
		msgtype.color = '#9A9AFF'
	elseif messageType == MessageType.StolenHealth then
		msgtype.color = '#FF8080'
	elseif messageType == MessageType.StolenMana then
		msgtype.color = '#9A9AFF'
	elseif messageType == MessageType.Loot then
		msgtype.color = '#00FF00'
		msgtype.subConsoleTab = 'Loot Log'
	elseif messageType == MessageType.Experience then
		msgtype.color = '#1AB1C3'
		msgtype.subConsoleTab = 'Loot Log'
	elseif messageType == MessageType.Advance then
		msgtype.color = '#A3A15D'
	elseif messageType == MessageType.Warning then
		msgtype.color = '#F55E5E'
	elseif messageType == MessageType.Orange then
		msgtype.color = '#F36500'
	elseif messageType == MessageType.Spell then
		msgtype.color = '#1E90FF'
	elseif messageType == MessageType.Blocked then
		msgtype.color = '#A52A49'
	elseif messageType == MessageType.Dodged then
		msgtype.color = '#2AA568'
	elseif messageType == MessageType.Private then
		msgtype.color = '#5FF7F7'
	end
	
	if msgtype.consoleTab and (not msgtype.consoleOption or modules.client_options.getOption(msgtype.consoleOption)) then
		-- msgtype.isCritical = isInArray(CriticalMessageTypes, messageType)
		-- msgtype.isDeathHit = isInArray(DeathHitMessageTypes, messageType)
		msgtype.combatType = combatType
		msgtype.messageType = messageType
		modules.game_console.m_ConsoleFunctions.addText(text, 0, msgtype, tr(msgtype.consoleTab), nil, msgtype.subConsoleTab, true)
	end
	
	if msgtype.textTarget then
		local var = messages[messagePosition]
		if #var >= messageHistory then
			var[#var].label:setParent(nil)
			var[#var].label:destroy()
			
			removeEvent(var[#var].event)
			table.remove(var, #var)
		end
		
		local textTargetMessagePanel = messagesPanel:recursiveGetChildById(msgtype.textTarget)
		local label = g_ui.createWidget('TextMessageLabel', textTargetMessagePanel)
		label:setText(text)
		label:setVisible(true)
		label:setColor(msgtype.color)
		label:setParent(textTargetMessagePanel)
		
		local info = {label = label, event = scheduleEvent(function()
			label:destroy()
			table.remove(messages[messagePosition], #var)
		end, calculateVisibleTime(text))}				
		table.insert(var, 1, info)
	end
	
	if msgtype.screenTarget then
		local widget = messagesPanel:recursiveGetChildById(msgtype.screenTarget)
		local label = widget:getChildById('label')
		label:destroyChildren()
		label:setColor(msgtype.color)
		
		if not name and text:find('c=#') then
			label:setFont('font-bookman-bold')
			text = g_game.setHighlightedText(label, text)
		end
		
		widget:show()
		removeEvent(widget.hideEvent)
		widget.hideEvent = scheduleEvent(function() widget:hide() end, calculateVisibleTime(text))
		widget:setHeight(math.max(80, label:getHeight()) + 16)
		label:clearText()
		label:setText(text)
		widget:setHeight(math.max(80, label:getHeight()) + 16)
	end
end

function updatePosition(move)
	local widget = messagesPanel:recursiveGetChildById('centerTextMessagePanel')
	if move then
		widget:setMarginTop(160)
	else
		widget:setMarginTop(72)
	end
end

function displayPrivateMessage(text)
	displayMessage(MessageModes.MessagePrivate, MessageType.Private, nil, text)
end

function displayStatusMessage(text)
	displayMessage(MessageModes.Status, nil, nil, text)
end

function displayFailureMessage(text)
	displayMessage(MessageModes.Failure, nil, nil, text)
end

function displayGameMessage(text)
	displayMessage(MessageModes.Game, nil, nil, text)
end

function displayBroadcastMessage(text)
	displayMessage(MessageModes.Warning, nil, nil, text)
end

function clearMessages()
	for _i,child in pairs(messagesPanel:recursiveGetChildren()) do
		if child:getId():match('Label') then
			child:hide()
			removeEvent(child.hideEvent)
		end
	end
end

function LocalPlayer:onAutoWalkFail(player)
	modules.game_textmessage.displayFailureMessage(tr('There is no way.'))
end