m_DestroyFunctions = {}
m_DestroyList = {}

m_DestroyFunctions.gui_atlas = '/ui/gui_atlas'
m_DestroyFunctions.language = LANGUAGE_ENGLISH

m_DestroyFunctions.amount = 3
for i = 1, m_DestroyFunctions.amount do
	m_DestroyList[i] = {}
end

m_DestroyFunctions.list = {}
m_DestroyFunctions.GameServerObtainItem = 87

m_DestroyFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_DestroyFunctions.language = LANGUAGE_ENGLISH
	else
		m_DestroyFunctions.language = LANGUAGE_POLISH
	end
end

m_DestroyFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_DestroyFunctions.GameServerObtainItem, m_DestroyFunctions.parseObtainItem)
end

m_DestroyFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_DestroyFunctions.GameServerObtainItem, m_DestroyFunctions.parseObtainItem)
end

m_DestroyFunctions.onMouseRelease = function(self)
	modules.game_questlog.m_QuestLogFunctions.onMouseRelease(self)
	m_DestroyFunctions.destroy(1)
end

m_DestroyFunctions.use = function(useThing)
	local itemId = useThing:getId()
	if itemId == 5706 then
		if not m_DestroyList[3] then
			m_DestroyList[3] = {}
		else
			m_DestroyFunctions.destroy(3)
		end
		
		m_DestroyList[3].window = g_ui.createWidget('TreasureMapPanel', modules.game_interface.getRootPanel())
		m_DestroyList[3].window:breakAnchors()
	elseif itemId == 18259 then
		if not m_DestroyList[3] then
			m_DestroyList[3] = {}
		else
			m_DestroyFunctions.destroy(3)
		end
		
		m_DestroyList[3].window = g_ui.createWidget('KynoheadsNotes', modules.game_interface.getRootPanel())
		m_DestroyList[3].window:breakAnchors()
	end
end

m_DestroyFunctions.parseObtainItem = function(protocol, msg)
	local id = msg:getU8()
	local clientId = msg:getU16()
	
	m_DestroyFunctions.showItemMessage(id, clientId)
end

m_DestroyFunctions.showItemMessage = function(id)
	local classId = ITEMCLASS_NONE
	local effect = 0
	if id == BLINK_OBTAINED_PERFECT then
		classId = ITEMCLASS_PERFECT
		effect = 232
	elseif id == BLINK_OBTAINED_LEGENDARY then
		classId = ITEMCLASS_LEGENDARY
		effect = 231
	elseif id == BLINK_OBTAINED_UNIQUE then
		classId = ITEMCLASS_UNIQUE
		effect = 230
	end
	
	if classId == ITEMCLASS_NONE then
		return true
	end
	
	m_DestroyFunctions.destroy(2)
	
	if not m_DestroyList[2] then
		m_DestroyList[2] = {}
	end
	
	m_DestroyList[2].window = g_ui.createWidget('ObtainedItemPanel', modules.game_interface.getRootPanel())
	m_DestroyList[2].effect = m_DestroyList[2].window:getChildById('effect')
	m_DestroyList[2].window:setImageColor(ITEM_NAME_COLOR[classId])
	m_DestroyList[2].window:getChildById('text'):setColor(ITEM_NAME_COLOR[classId])
	m_DestroyList[2].effect:setEffectId(effect)
	m_DestroyList[2].effect:setOn(effect ~= 231)
	g_effects.fadeIn(m_DestroyList[2].window, 250)
	
	m_DestroyList[2].event = scheduleEvent(function()
		if m_DestroyList[2].window then
			g_effects.fadeOut(m_DestroyList[2].window, 250, 0, true)
			m_DestroyList[2] = {}
		end
	end, 4000)
end

m_DestroyFunctions.showMessage = function(id, name, oldLevel, level)
	m_DestroyFunctions.destroy(1)
	
	if not m_DestroyList[1] then
		m_DestroyList[1] = {}
	end
	
	m_DestroyList[1].window = g_ui.createWidget(name, modules.game_interface.getRootPanel())
	m_DestroyList[1].name = m_DestroyList[1].window:getChildById('name')
	m_DestroyList[1].text = m_DestroyList[1].window:getChildById('text')
	g_effects.fadeIn(m_DestroyList[1].window, 250)
	
	if id == BLINK_QUEST_LOG then
		m_DestroyList[1].name:setText(tr('QUEST LOG UPDATED'))
		m_DestroyList[1].window.questId = oldLevel
		m_DestroyList[1].window.questName = level
		m_DestroyList[1].window:getChildById('leftAddMouseButton'):setOn(m_DestroyFunctions.language == LANGUAGE_POLISH)
	elseif id == BLINK_LEVEL_UP then
		m_DestroyList[1].name:setText(tr('CONGRATULATIONS'))
		m_DestroyList[1].text:setText(tr('YOU ADVANCED FROM LEVEL %d TO LEVEL %d', oldLevel, level))
	elseif id == BLINK_LEVEL_DOWN then
		m_DestroyList[1].name:setText(tr('UNFORTUNATELY'))
		m_DestroyList[1].text:setText(tr('YOU WERE DOWNGRADED FROM LEVEL %d TO LEVEL %d', oldLevel, level))
	end
	
	m_DestroyList[1].event = scheduleEvent(function()
		if m_DestroyList[1].window then
			g_effects.fadeOut(m_DestroyList[1].window, 250, 0, true)
			m_DestroyList[1] = {}
		end
	end, 8000)
end

m_DestroyFunctions.destroyWindows = function()
	modules.game_lookat.m_LookAtFunctions.destroy()
	modules.game_minimap.m_MinimapFunctions.destroyMaximizeMinimap()
	modules.game_character.m_CharacterFunctions.destroy()
	modules.game_encyclopedia.m_EncyclopediaFunctions.destroy()
	modules.game_questlog.m_QuestLogFunctions.destroy()
	modules.game_rewards.m_RewardsFunctions.destroy()
	modules.game_hotkeys.m_HotkeyFunctions.destroy()
	modules.game_npctrade.m_NpcTradeFunctions.destroy()
	modules.game_localtable.m_LocalTableFunctions.destroy()
	modules.game_task.m_TaskFunctions.destroy()
	modules.game_teleport.m_TeleportFunctions.destroy()
	modules.game_store.m_StoreFunctions.onCloseStore()
	modules.game_market.m_MarketFunctions.destroy()
	modules.game_outfit.destroy()
	modules.game_guild.destroy()
	modules.game_house.destroy()
	
	if modules.client_options.getOption('cancelsSelectedTarget') then
		g_game.cancelAttackAndFollow()
	end
end

m_DestroyFunctions.destroy = function(id)
	if m_DestroyList[id].window then
		if m_DestroyList[id].event then
			m_DestroyList[id].event:cancel()
		end
		
		m_DestroyList[id].window:destroy()
		m_DestroyList[id] = {}
	end
end

function onGameEnd()
	for i = 1, m_DestroyFunctions.amount do
		m_DestroyFunctions.destroy(i)
	end
	
	m_DestroyFunctions.unregisterProtocol()
end

function onGameStart()
	m_DestroyFunctions.loadLanguage()
	m_DestroyFunctions.registerProtocol()
end

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onBlinkButton = onBlinkButton,
		onClassItemChange = onClassItemChange
	})
	
	if g_game.isOnline() then
		m_DestroyFunctions.loadLanguage()
		m_DestroyFunctions.registerProtocol()
	end
	
	g_ui.importStyle('destroy')
	g_keyboard.bindKeyDown('Escape', m_DestroyFunctions.destroyWindows)
	g_textures.preload(m_DestroyFunctions.gui_atlas)
end

function onUnload()
	disconnect(g_game, { 
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onBlinkButton = onBlinkButton,
		onClassItemChange = onClassItemChange
	})
	
	onGameEnd()
	g_keyboard.unbindKeyDown('Escape')
end

m_DestroyFunctions.onBlink = function(var, delay)
	onBlinkButton(var, delay)
end

function onBlinkButton(var, delay)
	if var == BLINK_BLACK_SCREEN or var == BLINK_COLOR_SCREEN then
		local colorScreen, fadeIn, fadeOut = nil, 600, 600
		if var == BLINK_BLACK_SCREEN then
			colorScreen = g_ui.createWidget("ColorPanel", modules.game_interface.gameMapPanel)
			colorScreen:setBackgroundColor('#000000')
		else
			colorScreen = g_ui.createWidget("ColorPanel", modules.game_interface.getRootPanel())
			colorScreen:setBackgroundColor('#FAFAD2')
			fadeIn, fadeOut = 600, 300
		end
		g_effects.fadeIn(colorScreen, fadeIn)
		colorScreen:setVisible(true)
		return scheduleEvent(function()
								g_effects.fadeOut(colorScreen, fadeOut)
								scheduleEvent(function()
														colorScreen:destroy()
														colorScreen = nil 
													end, fadeOut) 
							end, delay or 1500)
	elseif var == BLINK_QUEST_LOG then
		m_DestroyFunctions.showMessage(var, 'QuestLogPanel')
	elseif var == BLINK_LEVEL_UP or var == BLINK_LEVEL_DOWN then
		m_DestroyFunctions.showMessage(var, 'AdvancedPanel')
	elseif var == BLINK_SPELL_POINTS then
		modules.game_bottompanel.m_MainFunctions.blink(5)
	elseif var == BLINK_CRATE then
		modules.game_bottompanel.m_MainFunctions.blink(2)
	elseif var == BLINK_QUEST_ITEMS then
		modules.game_inventory.blinkButton(var, true)
	elseif var == BLINK_OBTAINED_PERFECT or var == BLINK_OBTAINED_LEGENDARY or var == BLINK_OBTAINED_UNIQUE then
		m_DestroyFunctions.showItemMessage(var)
	elseif var == BLINK_OPEN_CONVERTER then
		modules.game_runes.m_RunesFunctions.open()
	elseif var == BLINK_CLOSE_CONVERTER then
		modules.game_runes.m_RunesFunctions.destroy()
	end
end

function onClassItemChange(item)
	local id = item:getId()
	local v = m_DestroyFunctions.list[id]
	if v then
		item:setCategoryId(v)
		return true
	end
	
	local it = modules.game_lookat.m_LookAtFunctions.getItemByClientId(id)
	if not it or not it.attributes then
		return true
	end
	
	local var = g_game.getItemType(it)
	if var ~= ITEMTYPE_NONE and it.id ~= 13192 and it.id ~= 13191 then
		item:setCategoryId(var)
		m_DestroyFunctions.list[id] = var
	end
end