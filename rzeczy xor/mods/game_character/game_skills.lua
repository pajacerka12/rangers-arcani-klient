m_SkillFunctions.warningMessage = tr('Do you really want reset all yours statistics?')
m_SkillFunctions.skillDescription = {
	[SkillStrength] = {
		{18, tr('+0.%d%% damage by one handed melee weapons per effective point (swords, axes and clubs)', 45)},
		{1, tr('+0.%d%% damage by two handed melee weapons per effective point (swords, axes and clubs)', 45)},
		{21, tr('+0.%d%% damage by throwing weapons per effective point (spears, small stones, throwing stars and knives)', 45)},
		{22, tr('+0.%d%% damage by shooting weapons per effective point (bows and crossbows)', 45)},
	},
	[SkillDexterity] = {
		{51, tr('+%s%% damage by traps per effective point', 0.05)},
		{35, tr('+%s%% summons damage per effective point', 0.2)},
		{38, tr('+%s%% damage over time per effective point', 0.2)},
		{24, tr('+%s%% chance dodge non melee attack per effective point', 0.02)},
	},
	[SkillCondition] = {
		{39, tr('+%d health points per effective point', 2)},
		{2, tr('+%s%% armor efficiency per effective point', 0.02)},
	},
	[SkillWisdom] = {
		{40, tr('+%d mana points per effective point', 2)},
		{28, tr('+0.%d%% magic damage per effective point', 40)},
		{27, tr('+0.%d%% healing efficiency per effective point', 35)},
		{38, tr('+%s%% damage over time per effective point', 0.1)},
		{35, tr('+%s%% summons damage per effective point', 0.35)},
	},
	[SkillVigor] = {
		{39, tr('+%d summon health points per effective point', 5)},
		{27, tr('+0.%d%% healing efficiency per effective point', 4)},
		{24, tr('+%s%% chance dodge melee attack per effective point', 0.04)},
	}
}

m_SkillFunctions.skillChange = {}
m_SkillFunctions.skills = {
	{tr('STRENGTH'), SkillStrength, '38 38 19 19'},
	{tr('DEXTERITY'), SkillDexterity, '38 114 19 19'},
	{tr('CONDITION'), SkillCondition, '38 57 18 19'},
	{tr('WISDOM'), SkillWisdom, '0 38 19 19'},
	{tr('VIGOR'), SkillVigor, '0 114 19 19'}
}

m_SkillFunctions.vocationMage = 1
m_SkillFunctions.vocationDruid = 2
m_SkillFunctions.vocationWizard = 3
m_SkillFunctions.vocationHunter = 4
m_SkillFunctions.vocationTracker = 5
m_SkillFunctions.vocationGrenadier = 6
m_SkillFunctions.vocationMercenary = 7
m_SkillFunctions.vocationPaladin = 8
m_SkillFunctions.vocationBarbarian = 9

m_SkillFunctions.getMultiplier = function(localPlayer, id)
	local vocationId = localPlayer:getVocation()
	if id == SkillStrength then
		if vocationId == m_SkillFunctions.vocationTracker then
			return 5
		elseif vocationId == m_SkillFunctions.vocationBarbarian then
			return 6
		elseif vocationId == m_SkillFunctions.vocationMercenary or vocationId == m_SkillFunctions.vocationPaladin or vocationId == m_SkillFunctions.vocationHunter or vocationId == m_SkillFunctions.vocationGrenadier then
			return 4
		end
	elseif id == SkillDexterity then
		if vocationId == m_SkillFunctions.vocationDruid or vocationId == m_SkillFunctions.vocationGrenadier or vocationId == m_SkillFunctions.vocationPaladin then
			return 2
		elseif vocationId == m_SkillFunctions.vocationWizard or vocationId == m_SkillFunctions.vocationBarbarian or vocationId == m_SkillFunctions.vocationTracker then
			return 3
		end
	elseif id == SkillCondition then
		if vocationId == m_SkillFunctions.vocationMage or vocationId == m_SkillFunctions.vocationDruid or vocationId == m_SkillFunctions.vocationWizard then
			return 2
		elseif vocationId == m_SkillFunctions.vocationHunter or vocationId == m_SkillFunctions.vocationMercenary then
			return 3
		elseif vocationId == m_SkillFunctions.vocationTracker or vocationId == m_SkillFunctions.vocationBarbarian then
			return 4
		elseif vocationId == m_SkillFunctions.vocationGrenadier or vocationId == m_SkillFunctions.vocationPaladin then
			return 5
		end
	elseif id == SkillWisdom then
		if vocationId == m_SkillFunctions.vocationTracker or vocationId == m_SkillFunctions.vocationGrenadier or vocationId == m_SkillFunctions.vocationPaladin or vocationId == m_SkillFunctions.vocationBarbarian then
			return 2
		elseif vocationId == m_SkillFunctions.vocationMage then
			return 4
		elseif vocationId == m_SkillFunctions.vocationDruid or vocationId == m_SkillFunctions.vocationWizard then
			return 5
		end
	elseif id == SkillVigor then
		if vocationId == m_SkillFunctions.vocationWizard or vocationId == m_SkillFunctions.vocationTracker or vocationId == m_SkillFunctions.vocationBarbarian then
			return 2
		elseif vocationId == m_SkillFunctions.vocationDruid or vocationId == m_SkillFunctions.vocationGrenadier or vocationId == m_SkillFunctions.vocationPaladin then
			return 3
		end
	end
	
	return 1
end

m_SkillFunctions.stopPoint = function()
	if m_SkillList.event then
		m_SkillList.event:cancel()
		m_SkillList.event = nil
	end
end

m_SkillFunctions.destroyDescription = function()
	if m_SkillList.window then
		for _, pid in pairs(m_SkillList.skillHoverList) do
			pid:destroy()
		end
		
		m_SkillList.skillHoverList = {}
	end
end

m_SkillFunctions.destroy = function()
	m_SkillFunctions.destroyDescription()
	m_SkillFunctions.destroyHover()
	
	if m_SkillList.window then
		m_SkillFunctions.destroyPopup()
		m_SkillList.skillList:destroyChildren()
		m_SkillList.skillAttributeList:destroyChildren()
		m_SkillList.skillEffectiveList:destroyChildren()
		m_SkillList.skillAddButtonList:destroyChildren()
		m_SkillList.skillRemoveButtonList:destroyChildren()
		m_SkillList.skillPointsList:destroyChildren()
		
		m_SkillFunctions.stopPoint()
		
		m_SkillList.window:destroy()
		m_SkillList.window = nil
		
		m_SkillList = {}
	end
end

m_SkillFunctions.open = function(parent)
	if m_SkillList.window then
		m_SkillFunctions.destroy()
	else
		m_SkillList.parent = parent:getParent()
		m_SkillList.window = g_ui.createWidget('SkillWindow', parent)
		m_SkillList.skillList = m_SkillList.window:getChildById('skillList')
		m_SkillList.skillAttributeList = m_SkillList.window:getChildById('skillAttributeList')
		m_SkillList.skillEffectiveList = m_SkillList.window:getChildById('skillEffectiveList')
		m_SkillList.skillAddButtonList = m_SkillList.window:getChildById('skillAddButtonList')
		m_SkillList.skillRemoveButtonList = m_SkillList.window:getChildById('skillRemoveButtonList')
		m_SkillList.skillPointsList = m_SkillList.window:getChildById('skillPointsList')
		m_SkillList.skillAcceptButton = m_SkillList.window:getChildById('acceptButton')
		m_SkillList.skillCancelButton = m_SkillList.window:getChildById('cancelButton')
		m_SkillList.skillResetButton = m_SkillList.window:getChildById('resetButton')
		m_SkillList.skillPoints = m_SkillList.window:getChildById('points')
		m_SkillList.skillDescription = m_SkillList.window:getChildById('description')
		
		m_SkillList.skillTotalPoints = {}
		m_SkillList.pointsList = {}
		m_SkillList.skillHoverList = {}
		
		m_SkillFunctions.execute()
		
		-- modules.game_inventory.setSkills(true)
	end
end

m_SkillFunctions.execute = function()
	local player = g_game.getLocalPlayer()
	if not player then 
		return false
	end
	
	m_SkillList.statisticsRestarts = player:getStatisticsRestarts()
	m_SkillList.skillResetButton:setText('Reset ( ' .. m_SkillList.statisticsRestarts .. ' )')
	m_SkillList.skillResetButton:setEnabled(m_SkillList.statisticsRestarts ~= 0)
	m_SkillList.points = player:getSkillPoints()
	m_SkillList.skillPoints:setText(m_SkillList.points)
	
	for i = 1, #m_SkillFunctions.skills do
		local name, skill, clip = m_SkillFunctions.skills[i][1], m_SkillFunctions.skills[i][2], m_SkillFunctions.skills[i][3]
		local multiplier = m_SkillFunctions.getMultiplier(player, skill)
		
		local widget = g_ui.createWidget('SkillBasicList', m_SkillList.skillList)
		widget:setText(name .. ' (x' .. multiplier .. ')')
		widget.skill = skill
		widget.onHoverChange = m_SkillFunctions.onDescriptionHoverChange
		widget:setIconClip(clip)
		
		local pid = m_SkillFunctions.skillChange[skill]
		
		local baseSkill = pid[1]
		local bonusSkill = pid[2]
		if bonusSkill > 0xFFF then
			bonusSkill = 0xFFF - bonusSkill
		end
		
		local color = '#FFFFFF'
		if bonusSkill > 0 then
			color = '#7CFC00'
		elseif bonusSkill < 0 then
			color = '#FF1414'
		end
		
		m_SkillList.skillTotalPoints[skill] = baseSkill
		
		local widget = g_ui.createWidget('SkillInfoList', m_SkillList.skillAttributeList)
		widget:setId(i)
		widget:setText((baseSkill + bonusSkill) .. (bonusSkill ~= 0 and ' (' .. (bonusSkill > 0 and '+' or '') .. bonusSkill .. ')' or ''))
		widget:setColor(color)
		
		local baseSkill = player:getSkillEffectiveLevel(skill)
		if baseSkill > 0xFFF then
			baseSkill = 0xFFF - baseSkill
		end
		
		local bonusSkill = baseSkill - player:getSkillEffectiveBaseLevel(skill)
		
		local widget = g_ui.createWidget('SkillInfoList', m_SkillList.skillEffectiveList)
		widget:setId(i)
		widget:setText(baseSkill)
		widget:setColor(color)
		
		local displayHints = modules.client_options.getOption('displayHints')
		local widget = g_ui.createWidget('SkillButton', m_SkillList.skillAddButtonList)
		widget.onMouseRelease = m_SkillFunctions.stopPoint
		widget.onHoverChange = m_SkillFunctions.onButtonHover
		widget.onMousePress = function(widget, mousePosition, mouseButton)
			if not m_SkillList.event and mouseButton == MouseLeftButton then
				m_SkillFunctions.addPoint(skill)
			end
		end
		
		local widget = g_ui.createWidget('SkillButton', m_SkillList.skillRemoveButtonList)
		widget:setOn(true)
		widget.onMouseRelease = m_SkillFunctions.stopPoint
		widget.onHoverChange = m_SkillFunctions.onButtonHover
		widget.onMousePress = function(widget, mousePosition, mouseButton)
			if not m_SkillList.event and mouseButton == MouseLeftButton then
				m_SkillFunctions.removePoint(skill)
			end
		end
		
		local widget = g_ui.createWidget('SkillBasicList', m_SkillList.skillPointsList)
		widget:setId(i)
		widget:setText(0)
		widget:setIconClip('0 0 0 0')
		widget:setOn(true)
	end
end

m_SkillFunctions.getIconIdByName = function(name)
	if name == 'STRENGTH' then
		return 19
	elseif name == 'DEXTERITY' then
		return 9
	elseif name == 'CONDITION' then
		return 27
	elseif name == 'WISDOM' then
		return 17
	elseif name == 'VIGOR' then
		return 49
	end
		
	return 16
end

m_SkillFunctions.addPoint = function(id)
	if m_SkillList.points < 1 then
		return true
	end
	
	local player = g_game.getLocalPlayer()
	if not player then 
		return false
	end
	
	if not m_SkillList.pointsList[id] then
		m_SkillList.pointsList[id] = 0
	end
	
	local maxAmount = 30 + (player:getLevel() * 3)
	
	local keyboardModifiers = g_keyboard.getModifiers()
	local amount = 1
	if keyboardModifiers == KeyboardCtrlModifier then
		amount = math.min(m_SkillList.points, maxAmount - m_SkillList.skillTotalPoints[id] - m_SkillList.pointsList[id])
	elseif keyboardModifiers == KeyboardShiftModifier then
		amount = math.min(math.min(10, m_SkillList.points), maxAmount - m_SkillList.skillTotalPoints[id] - m_SkillList.pointsList[id])
	end
	
	if m_SkillList.skillTotalPoints[id] + (m_SkillList.pointsList[id] or 0) >= maxAmount then
		return false
	end
	
	m_SkillList.pointsList[id] = m_SkillList.pointsList[id] + amount
	m_SkillList.points = m_SkillList.points - amount
	local widget = m_SkillList.skillPointsList:getChildById(id + 1)
	widget:setText(m_SkillList.pointsList[id])
	m_SkillList.skillPoints:setText(m_SkillList.points)
	
	if not m_SkillList.event then
		m_SkillList.event = cycleEvent(function() m_SkillFunctions.addPoint(id) end, 100)
	end
	
	for _, v in pairs(m_SkillList.pointsList) do
		if v > 0 then
			m_SkillList.skillAcceptButton:setEnabled(true)
			m_SkillList.skillCancelButton:setEnabled(true)
			break
		end
	end
	
	if m_SkillList.skillTotalPoints[id] + m_SkillList.pointsList[id] >= maxAmount then
		widget:setColor('#FF1414')
	else
		widget:setColor('#9E996E')
	end
end

m_SkillFunctions.removePoint = function(id)
	if not m_SkillList.pointsList[id] then
		m_SkillList.pointsList[id] = 0
	end
	
	local widget = m_SkillList.skillPointsList:getChildById(id + 1)
	if m_SkillList.pointsList[id] > 0 then
		local keyboardModifiers = g_keyboard.getModifiers()
		local amount = 1
		if keyboardModifiers == KeyboardCtrlModifier then
			amount = m_SkillList.pointsList[id] or 0
		elseif keyboardModifiers == KeyboardShiftModifier then
			amount = math.min(10, m_SkillList.pointsList[id] or 0)
		end
		
		m_SkillList.pointsList[id] = m_SkillList.pointsList[id] - amount
		m_SkillList.points = m_SkillList.points + amount
		
		widget:setText(m_SkillList.pointsList[id])
		m_SkillList.skillPoints:setText(m_SkillList.points)
		
		if not m_SkillList.event then
			m_SkillList.event = cycleEvent(function() m_SkillFunctions.removePoint(id) end, 100)
		end
	end
	
	widget:setColor('#9E996E')
	
	for _, v in pairs(m_SkillList.pointsList) do
		if v > 0 then
			return true
		end
	end
	
	m_SkillList.skillAcceptButton:setEnabled(false)
	m_SkillList.skillCancelButton:setEnabled(false)
end

m_SkillFunctions.start = function()
	for _, pid in pairs(m_SkillList.skillPointsList:getChildren()) do
		pid:setText(0)
		pid:setColor('#9E996E')
	end
	
	m_SkillList.pointsList = {}
	
	m_SkillList.skillAcceptButton:setEnabled(false)
	m_SkillList.skillCancelButton:setEnabled(false)
end

m_SkillFunctions.destroyPopup = function()
	if m_SkillList.popup then
		m_SkillList.popup:destroy()
		m_SkillList.popup = nil
		
		m_SkillList.parent:setEnabled(true)
		m_SkillList.parent:focus()
		m_SkillList.parent:raise()
	end
end

m_SkillFunctions.createPopup = function(name)
	m_SkillList.parent:setEnabled(false)
	m_SkillFunctions.destroyPopup()
	
	m_SkillList.popup = g_ui.displayUI(name)
	m_SkillList.popup:focus()
	m_SkillList.popup:raise()
end

m_SkillFunctions.reset = function()
	m_SkillFunctions.createPopup('popup_message')
	m_SkillList.popup:getChildById('description'):setText(m_SkillFunctions.warningMessage)
end

m_SkillFunctions.resetAccept = function()
	m_SkillFunctions.destroyPopup()
	
	local msg = OutputMessage.create()
	msg:addU8(82)
	msg:addU8(0)
	g_game.getProtocolGame():send(msg)
	
	m_SkillFunctions.start()
end

m_SkillFunctions.clear = function()
	for _, v in pairs(m_SkillList.pointsList) do
		m_SkillList.points = m_SkillList.points + v
	end
	
	m_SkillList.skillPoints:setText(m_SkillList.points)
	m_SkillFunctions.start()
end

m_SkillFunctions.accept = function()
	local msg = OutputMessage.create()
	msg:addU8(82)
	msg:addU8(1)
	
	for i = SkillStrength, SkillVigor do
		msg:addU16(m_SkillList.pointsList[i] or 0)
	end
	
	g_game.getProtocolGame():send(msg)
	m_SkillFunctions.start()
end

m_SkillFunctions.onGameStart = function()
	local player = g_game.getLocalPlayer()
	if not player then
		return false
	end
	
	for i = SkillStrength, SkillVigor do
		m_SkillFunctions.onSkillChange(player, i, player:getSkillBaseLevel(i), player:getSkillLevel(i) - player:getSkillBaseLevel(i), player:getSkillEffectiveBaseLevel(i), player:getSkillEffectiveLevel(i) - player:getSkillEffectiveBaseLevel(i))
	end
	
	m_SkillList.statisticsRestarts = player:getStatisticsRestarts()
	m_SkillList.points = player:getSkillPoints()
end

function onSkillChange(player, skillId, baseSkill, bonusSkill, effectiveBasicSkill, effectiveBonusSkill)
	m_SkillFunctions.onSkillChange(player, skillId, baseSkill, bonusSkill, effectiveBasicSkill, effectiveBonusSkill)
end

function onStatisticRestarts(player, amount)
	m_SkillFunctions.onStatisticRestarts(player, amount)
end

function onSkillPoints(player, amount)
	m_SkillFunctions.onSkillPoints(player, amount)
end

m_SkillFunctions.onLoad = function()
	connect(LocalPlayer, {
		onSkillChange = onSkillChange,
		onStatisticRestarts = onStatisticRestarts,
		onSkillPoints = onSkillPoints
	})
	
	if g_game.isOnline() then
		m_SkillFunctions.onGameStart()
	end
end

m_SkillFunctions.onUnload = function()
	disconnect(LocalPlayer, {
		onSkillChange = onSkillChange,
		onStatisticRestarts = onStatisticRestarts,
		onSkillPoints = onSkillPoints
	})
end

m_SkillFunctions.onStatisticRestarts = function(player, amount)
	if m_SkillList.skillResetButton then
		m_SkillList.skillResetButton:setText('Reset ( ' .. amount .. ' )')
		m_SkillList.skillResetButton:setEnabled(amount ~= 0)
	end
end

m_SkillFunctions.onSkillChange = function(player, skillId, baseSkill, bonusSkill, effectiveBasicSkill, effectiveBonusSkill)
	if modules.game_bottompanel then
		modules.game_bottompanel.m_MainFunctions.onSkillChange(player, skillId, baseSkill, bonusSkill)
	end
	
	m_SkillFunctions.skillChange[skillId] = {baseSkill, bonusSkill, effectiveBasicSkill, effectiveBonusSkill}
	if not m_SkillList.skillAttributeList then
		return true
	end
	
	m_SkillList.skillTotalPoints[skillId] = baseSkill
	
	local widget = m_SkillList.skillAttributeList:getChildById(skillId + 1)
	if not widget then
		return false
	end
	
	if bonusSkill > 0xFFF then
		bonusSkill = 0xFFF - bonusSkill
	end
	
	local color = '#FFFFFF'
	if bonusSkill > 0 then
		color = '#7CFC00'
	elseif bonusSkill < 0 then
		color = '#FF1414'
	end
	
	widget:setText((baseSkill + bonusSkill) .. (bonusSkill ~= 0 and ' (' .. (bonusSkill > 0 and '+' or '') .. bonusSkill .. ')' or ''))
	widget:setColor(color)
	
	widget = m_SkillList.skillEffectiveList:getChildById(skillId + 1)
	if not widget then
		return false
	end
	
	if effectiveBonusSkill > 0xFFF then
		effectiveBonusSkill = 0xFFF - effectiveBonusSkill
	end
	
	widget:setText((effectiveBasicSkill + effectiveBonusSkill))
	widget:setColor(color)
end

m_SkillFunctions.onSkillPoints = function(player, amount)
	m_SkillList.points = amount
	-- modules.game_inventory.setChecked(m_SkillList.points > 0)
	
	if m_SkillList.skillPoints then
		m_SkillList.skillPoints:setText(amount)
		m_SkillFunctions.start()
	end
end

m_SkillFunctions.destroyHover = function()
	if m_SkillList.hover then
		m_SkillList.hover:destroy()
		m_SkillList.hover = nil
	end
end

m_SkillFunctions.onButtonHover = function(self, hovered)
	if not modules.client_options.getOption('displayHints') then
		return true
	end
	
	if not hovered then
		m_SkillFunctions.destroyHover()
		return true
	end
	
	m_SkillList.hover = g_ui.createWidget('SkillsHoverWindow', m_SkillList.window)
	
	local pos = self:getPosition()
	pos.x = pos.x - (m_SkillList.hover:getWidth() / 2)
	pos.y = pos.y + self:getHeight() - 6
	
	m_SkillList.hover:setPosition(pos)
end

m_SkillFunctions.onDescriptionHoverChange = function(self, hovered)
	if not hovered then
		m_SkillFunctions.destroyDescription()
		return true
	end
	
	if #m_SkillList.skillHoverList > 0 then
		return true
	end
	
	local var = m_SkillFunctions.skillDescription[self.skill]
	if not var then
		return false
	end
	
	local list = m_SkillList.skillDescription:getChildById('list')
	for i = 1, #var do
		local iconid, description = var[i][1], var[i][2]
	
		local widget = g_ui.createWidget('SkillNameList', list)
		modules.game_lookat.m_LookAtFunctions.setIconImageType(widget:getChildById('icon'), iconid)
		
		widget:setParent(list)
		widget:setText(description)
		widget:resizeToText()
		
		table.insert(m_SkillList.skillHoverList, widget)
	end
end