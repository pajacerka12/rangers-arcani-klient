m_CharmsFunctions.config = {
	[1] = {name = 'Gut', description = 'Gutting the creature yields 10p% higher chance for creature products.', icon = '198 66 32 32', price = 800},
	[2] = {name = 'Low Blow', description = 'Adds 8% critical hit chance to attacks.', icon = '0 0 32 32', price = 1500},
	[3] = {name = 'Vampiric Embrace', description = 'Adds 4% Life Leech to attacks.', icon = '66 66 32 32', price = 1200},
	[4] = {name = 'Void\'s Call', description = 'Mana drain immunity.', icon = '99 33 32 32', price = 1300},
	[5] = {name = 'Dodge', description = 'Dodges an attack with a certain chance without taking any damage at all.', icon = '198 33 32 32', price = 700},
	[6] = {name = 'Parry', description = 'Any damage taken is reflected to the aggressor with a certain chance.', icon = '132 0 32 32', price = 900},
	
	[7] = {name = 'Wound', description = 'Lowers the enemy resistances by 6% with a certain chance.', icon = '66 99 32 32', price = 1100},
	[8] = {name = 'Numb', description = 'Adds 6% resistance.', icon = '99 99 32 32', price = 1500},
	[9] = {name = 'Divine Wrath', description = 'Attacks deal area damage (3x3) with a certain chance.', icon = '165 99 32 32', price = 1300},
	[10] = {name = 'Zap', description = 'Attacks restore 5 mana with a certain chance.', icon = '198 99 32 32', price = 900},
	[11] = {name = 'Adrenaline Burst', description = 'Bursts of adrenaline enhance your reflexes with a certain chance after you get hit and let you move faster for 10 seconds.', icon = '132 99 32 32', price = 500},
	[12] = {name = 'Cripple', description = 'Cripples the creature with a certain chance and slows it for 10 seconds.', icon = '33 99 32 32', price = 500},
}

m_CharmsFunctions.GameServerManageCharm = 91
m_CharmsFunctions.GameServerParseCharm = 50

m_CharmsFunctions.language = LANGUAGE_ENGLISH

m_CharmsFunctions.charmList = {}

m_CharmsFunctions.CHARM_UNLOCK = 1
m_CharmsFunctions.CHARM_SET = 2
m_CharmsFunctions.CHARM_REMOVE = 3
m_CharmsFunctions.CHARM_UPDATE = 4

m_CharmsFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_CharmsFunctions.language = LANGUAGE_ENGLISH
	else
		m_CharmsFunctions.language = LANGUAGE_POLISH
	end
end

m_CharmsFunctions.onGameStart = function()
	m_CharmsFunctions.loadLanguage()
	m_CharmsFunctions.registerProtocol()
end

m_CharmsFunctions.onGameEnd = function()
	m_CharmsFunctions.unregisterProtocol()
end

m_CharmsFunctions.onLoad = function()
	if g_game.isOnline() then
		m_CharmsFunctions.onGameStart()
	end
end

m_CharmsFunctions.onUnload = function()
	onGameEnd()
end

m_CharmsFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_CharmsFunctions.GameServerParseCharm, m_CharmsFunctions.parseCharms)
end

m_CharmsFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_CharmsFunctions.GameServerParseCharm, m_CharmsFunctions.parseCharms)
end

m_CharmsFunctions.destroy = function()
	m_CharmsFunctions.destroyPopup()
	
	if m_CharmsList.window then
		m_CharmsList.window:destroy()
		m_CharmsList = {}
	end
end

m_CharmsFunctions.updateBalance = function(balance)
	m_CharmsFunctions.balance = balance
	if m_CharmsList.balance then
		m_CharmsList.balance:setText(g_game.getMilharNumber(m_CharmsFunctions.balance))
	end
end

m_CharmsFunctions.sort = function(k, v)
	return k.description[m_CharmsFunctions.language] < v.description[m_CharmsFunctions.language]
end

m_CharmsFunctions.select = function(self)
	if m_CharmsList.currentSelect then
		m_CharmsList.currentSelect:getChildById('mask'):hide()
	end
	
	self:getChildById('mask'):show()
	m_CharmsList.currentSelect = self
	m_CharmsList.description:setText(m_CharmsFunctions.config[self.id].description)
	m_CharmsList.icon:setImageClip(self.icon)
	m_CharmsList.searchMonsterName:clearText()
	
	if self.outfit then
		m_CharmsList.creature:show()
		m_CharmsList.creature:setOutfit(self.outfit)
		m_CharmsList.price:setOn(true)
		m_CharmsList.price:setText(self.price)
		m_CharmsList.button:setText(tr('Remove'))
		m_CharmsList.actionId = m_CharmsFunctions.CHARM_REMOVE
		
		m_CharmsList.monsterList:destroyChildren()
		m_CharmsList.searchMonsterName:setEnabled(false)
		
		local widget = g_ui.createWidget('CharmsLabel', m_CharmsList.monsterList)
		widget:setText(self.name)
		widget:focus()
	else
		if self.bought then
			m_CharmsList.price:setOn(true)
			m_CharmsList.price:setText(0)
			m_CharmsList.button:setText(tr('Select'))
			m_CharmsList.actionId = m_CharmsFunctions.CHARM_SET
			
			if m_CharmsList.monsterList:getChildCount() <= 1 then
				m_CharmsList.monsterList:destroyChildren()
				
				local list = modules.game_lookat.m_LookAtFunctions.getMonstersNameList()
				table.sort(list, m_CharmsFunctions.sort)
				for i = 1, #list do
					local widget = g_ui.createWidget('CharmsLabel', m_CharmsList.monsterList)
					widget:setText(list[i].description[m_CharmsFunctions.language])
					widget.name = list[i].name
					if i == 1 then
						widget:focus()
					end
				end
			end
			
			m_CharmsList.searchMonsterName:setEnabled(true)
		else
			m_CharmsList.price:setOn(false)
			m_CharmsList.price:setText(self.price)
			m_CharmsList.button:setText(tr('Unlock'))
			m_CharmsList.actionId = m_CharmsFunctions.CHARM_UNLOCK
			
			m_CharmsList.monsterList:destroyChildren()
			m_CharmsList.searchMonsterName:setEnabled(false)
		end
		
		m_CharmsList.creature:hide()
	end
end

m_CharmsFunctions.pressButton = function()
	if m_CharmsList.actionId == m_CharmsFunctions.CHARM_UNLOCK and m_CharmsList.currentSelect.price > (m_CharmsFunctions.charmPoints or 0) then
		m_CharmsFunctions.sendMessage(tr('You don\'t have enough charm points.'))
		return true
	elseif m_CharmsList.actionId == m_CharmsFunctions.CHARM_REMOVE and m_CharmsList.currentSelect.price > (m_CharmsFunctions.balance or 0) then
		m_CharmsFunctions.sendMessage(tr('You don\'t have enough money.'))
		return true
	end
	
	local msg = OutputMessage.create()
	msg:addU8(m_CharmsFunctions.GameServerManageCharm)
	msg:addU8(m_CharmsList.actionId)
	msg:addU8(m_CharmsList.currentSelect.id)
	
	if m_CharmsList.actionId == m_CharmsFunctions.CHARM_SET then
		local widget = m_CharmsList.monsterList:getFocusedChild()
		if not widget then
			msg:addString("")
		else
			msg:addString(widget.name)
		end
	end
	
	g_game.getProtocolGame():send(msg)
end

m_CharmsFunctions.openWindow = function()
	local msg = OutputMessage.create()
	msg:addU8(m_CharmsFunctions.GameServerManageCharm)
	msg:addU8(m_CharmsFunctions.CHARM_UPDATE)
	g_game.getProtocolGame():send(msg)
end

m_CharmsFunctions.destroyPopup = function()
	if m_CharmsList.popup then
		m_CharmsList.popup:destroy()
		m_CharmsList.popup = nil
	end
end

m_CharmsFunctions.createPopup = function(name)
	m_CharmsFunctions.destroyPopup()
	
	m_CharmsList.popup = g_ui.displayUI(name)
	m_CharmsList.popup:focus()
	m_CharmsList.popup:raise()
end

m_CharmsFunctions.sendMessage = function(text)
	m_CharmsFunctions.createPopup('popup_charm')
	m_CharmsList.popup:getChildById('description'):setText(text)
end

m_CharmsFunctions.getPrice = function()
	return 5000 + (g_game.getLocalPlayer():getLevel() * 255)
end

m_CharmsFunctions.execute = function()
	m_CharmsList.charms:setText(m_CharmsFunctions.charmPoints)
	
	local found = not m_CharmsList.currentSelect
	for i = 1, 6 do
		local id = ((m_CharmsList.page - 1) * 6) + i
		
		local v = m_CharmsFunctions.config[id]
		local widget = m_CharmsList.window:getChildById('charm' .. i)
		local var = m_CharmsFunctions.charmList[id] or {bought = false}
		widget:getChildById('name'):setText(v.name)
		widget:getChildById('icon'):setImageClip(v.icon)
		widget:getChildById('creature'):hide()
		widget:getChildById('icon'):setOpacity(1.0)
		widget.id = id
		widget.icon = v.icon
		
		widget.bought = var.bought
		widget.outfit = false
		widget.price = false
		
		if var.name and var.name ~= '' then
			local outfit = modules.game_lookat.m_LookAtFunctions.getMonsterByName(var.name).look
			local price = m_CharmsFunctions.getPrice()
			widget:getChildById('creature'):show()
			widget:getChildById('creature'):setOutfit(outfit)
			widget:getChildById('price'):setText(g_game.getMilharNumber(price))
			widget:getChildById('price'):setOn(true)
			widget.outfit = outfit
			widget.name = var.name
			widget.price = price
		elseif var.bought then
			widget:getChildById('price'):setText(0)
			widget:getChildById('price'):setOn(true)
		else
			widget:getChildById('price'):setText(g_game.getMilharNumber(v.price))
			widget:getChildById('price'):setOn(false)
			widget:getChildById('icon'):setOpacity(0.25)
			widget.price = v.price
		end
		
		if i == 1 and found then
			m_CharmsFunctions.select(widget)
		end
	end
	
	if not found then
		m_CharmsFunctions.select(m_CharmsList.currentSelect)
	end
end

m_CharmsFunctions.selectPage = function(self)
	local id = self:getId()
	if id == 'page1' then
		m_CharmsList.page = 1
	elseif id == 'page2' then
		m_CharmsList.page = 2
	end
	
	if m_CharmsList.lastPage then
		m_CharmsList.lastPage:setOn(false)
	end
	
	self:setOn(true)
	m_CharmsList.lastPage = self
	m_CharmsFunctions.execute()
end

m_CharmsFunctions.open = function(parent)
	if m_CharmsList.window then
		m_CharmsFunctions.destroy()
	else
		m_CharmsList.window = g_ui.createWidget('CharmWindow', parent)
		m_CharmsList.description = m_CharmsList.window:getChildById('description')
		m_CharmsList.creature = m_CharmsList.window:getChildById('creature')
		m_CharmsList.price = m_CharmsList.window:getChildById('price')
		m_CharmsList.icon = m_CharmsList.window:getChildById('icon')
		m_CharmsList.button = m_CharmsList.window:getChildById('button')
		m_CharmsList.balance = m_CharmsList.window:getChildById('balance')
		m_CharmsList.charms = m_CharmsList.window:getChildById('charms')
		m_CharmsList.searchMonsterName = m_CharmsList.window:getChildById('searchMonsterName')
		m_CharmsList.monsterList = m_CharmsList.window:getChildById('monsterList')
		m_CharmsList.pages = {
			m_CharmsList.window:getChildById('page1'),
			m_CharmsList.window:getChildById('page2')
		}
		m_CharmsList.page = 1
		m_CharmsList.lastPage = m_CharmsList.pages[1]
		m_CharmsList.pages[1]:setOn(true)
		
		m_CharmsList.searchMonsterName.onTextChange = function(self, text, oldText)
			local list = m_CharmsList.monsterList:getChildren()
			if not text:find(oldText) then
				for _, v in pairs(list) do
					if v:getText():lower():find(text:lower()) then
						v:show()
					end
				end
			end
			
			for _, v in pairs(list) do
				local name = v:getText()
				local tmpList = {
					['�'] = '�',
					['�'] = '�',
					['�'] = '�',
					['�'] = '�'
				}
				
				for k, v in pairs(tmpList) do
					name = name:gsub(k, v)
				end
				
				if not name:lower():find(text:lower()) then
					v:hide()
				end
			end
		end
		
		for i = 1, 6 do
			local v = m_CharmsFunctions.config[i]
			local widget = m_CharmsList.window:getChildById('charm' .. i)
			widget:getChildById('name'):setText(v.name)
			widget:getChildById('icon'):setImageClip(v.icon)
			widget.id = i
			widget.icon = v.icon
		end
		
		if m_CharmsFunctions.balance then
			m_CharmsList.balance:setText(g_game.getMilharNumber(m_CharmsFunctions.balance))
		end
		
		if m_CharmsFunctions.charmPoints then
			m_CharmsList.charms:setText(m_CharmsFunctions.charmPoints)
		end
		
		m_CharmsFunctions.openWindow()
	end
end

m_CharmsFunctions.parseCharms = function(protocol, msg)
	m_CharmsFunctions.charmList = {}
	local actionId = msg:getU8()
	m_CharmsFunctions.charmPoints = msg:getU16()
	if actionId == 2 then
		local size = msg:getU8()
		for i = 1, size do
			local id = msg:getU8()
			m_CharmsFunctions.charmList[id] = {}
			
			m_CharmsFunctions.charmList[id].bought = msg:getU8() == 1
			if m_CharmsFunctions.charmList[id].bought then
				m_CharmsFunctions.charmList[id].name = msg:getString()
			end
		end
	end
	
	m_CharmsFunctions.execute()
end
