SkillsHoverWindow < AtlasWidget
  image-clip: 365 119 153 176
  image-border: 4
  size: 200 95
  phantom: true
  
  LeftMouseButtonImage
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    margin: 6
    !text: tr('1 point')
  
  ShiftButtonImage
    id: shift
    anchors.top: prev.bottom
    anchors.left: parent.left
    !text: ' + '
    width: 58
    margin: 6
  
  LeftMouseButtonImage
    anchors.verticalcenter: prev.verticalcenter
    anchors.left: prev.right
    anchors.right: parent.right
    margin-left: 4
    !text: tr('10 points')
  
  CtrlButtonImage
    anchors.top: shift.bottom
    anchors.left: parent.left
    !text: ' + '
    width: 54
    margin: 6
  
  LeftMouseButtonImage
    anchors.verticalcenter: prev.verticalcenter
    anchors.left: prev.right
    anchors.right: parent.right
    margin-left: 4
    !text: tr('all points')

SkillInfoList < UIWidget
  margin: 4
  text-offset: 6 0
  height: 30
  text-align: left
  background-color: alpha
  $hover:
    background-color: #313131

SkillBasicList < AtlasWidget
  height: 34
  image-clip: 365 119 153 176
  image-border: 4
  text-align: left
  text-offset: 28 4
  font: perfect-sans-11px
  color: #9E996E
  icon-size: 19 19
  icon-align: left
  icon-offset: 6 8
  icon-source: /images/leaf/spellWindow/icons
  $!first:
    margin-top: 5
  $on:
    text-align: center
    text-offset: 0 4
    
SkillNameList < UIWidget
  margin: 4
  height: 30
  background-color: alpha
  text-offset: 24 0
  text-wrap: true
  text-align: left
  font: perfect-sans-11px
  color: #9E996E
  $hover:
    background-color: #313131
  $first:
    margin-top: 12
  
  IconWidget
    id: icon
    anchors.left: parent.left
    anchors.verticalcenter: parent.verticalcenter
    phantom: true

AttributeLabels < UIWidget
  size: 150 16
  font: perfect-sans-11px
  color: #E4CEBA

AttributeScrollablePanel < ScrollablePanel
  anchors.verticalcenter: next.verticalcenter
  anchors.left: next.right
  margin-left: 5
  size: 165 190
  image-source: /ui/gui_atlas
  image-clip: 365 119 153 176
  image-border: 8
  layout:
    type: verticalBox

AttributeScrollableBlankPanel < ScrollablePanel
  anchors.verticalcenter: next.verticalcenter
  anchors.left: next.right
  margin-left: 5
  size: 165 190
  layout:
    type: verticalBox

SkillButton < AtlasButton
  margin: 4
  image-border: 0
  image-clip: 1035 1165 27 27
  size: 30 30
  $on:
    image-clip: 1035 1189 27 27

SkillWindow < UIWindow
  anchors.top: parent.top
  anchors.bottom: parent.bottom
  anchors.horizontalcenter: parent.horizontalcenter
  width: 620
  phantom: true
  draggable: false
  
  AttributeScrollableBlankPanel
    id: skillPointsList
    width: 38
  
  AttributeScrollableBlankPanel
    id: skillRemoveButtonList
    width: 38
  
  AttributeScrollableBlankPanel
    id: skillAddButtonList
    width: 38
  
  AttributeScrollablePanel
    id: skillEffectiveList
  
  AttributeScrollablePanel
    id: skillAttributeList
  
  ScrollablePanel
    id: skillList
    anchors.top: parent.top
    anchors.left: parent.left
    size: 140 190
    margin: 5
    margin-top: 28
    layout:
      type: verticalBox
  
  AttributeLabels
    anchors.bottom: prev.top
    anchors.horizontalcenter: skillAttributeList.horizontalcenter
    margin-bottom: 5
    !text: tr('BASIC VALUE')
  
  AttributeLabels
    anchors.top: prev.top
    anchors.horizontalcenter: skillEffectiveList.horizontalcenter
    !text: tr('EFFECTIVE VALUE')
  
  AtlasWidget
    id: points
    anchors.top: skillEffectiveList.bottom
    anchors.right: parent.right
    image-clip: 365 119 153 176
    image-border: 8
    margin-top: 8
    margin-right: 6
    size: 50 30
    font: perfect-sans-11px
    !text: '0'
    text-offset: 0 4
  
  AtlasButton
    id: resetButton
    anchors.horizontalcenter: parent.horizontalcenter
    anchors.top: prev.top
    @onClick: modules.game_character.m_SkillFunctions.reset()
    width: 130
  
  AtlasButton
    id: acceptButton
    anchors.top: prev.top
    anchors.right: prev.left
    margin-right: 5
    width: 130
    enabled: false
    !text: tr('Accept')
    @onClick: modules.game_character.m_SkillFunctions.accept()
  
  AtlasButton
    id: cancelButton
    anchors.top: prev.top
    anchors.left: resetButton.right
    margin-left: 5
    width: 130
    enabled: false
    !text: tr('Clear')
    @onClick: modules.game_character.m_SkillFunctions.clear()
    
  AtlasWidget
    id: description
    anchors.top: prev.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    image-clip: 365 119 153 176
    image-border: 8
    phantom: true
    margin-top: 36
  
    ScrollablePanel
      id: list
      anchors.fill: parent
      layout:
        type: verticalBox
      margin: 2
  
  AtlasWidget
    image-clip: 1076 1169 32 32
    size: 32 32
    anchors.horizontalcenter: prev.horizontalcenter
    anchors.verticalcenter: prev.top
    margin-bottom: 8