m_CharacterFunctions = {}

m_SkillList = {}
m_SkillFunctions = {}

m_RunesList = {}
m_RunesFunctions = {}

m_StatsList = {}
m_StatsFunctions = {}

m_SpellsList = {}
m_SpellsFunctions = {}

m_CharmsList = {}
m_CharmsFunctions = {}

ID_SKILLS = 1
ID_SPELLS = 2
ID_STATS = 3
ID_RUNES = 4
ID_CHARMS = 5

m_CharacterFunctions.config = {
	[ID_SKILLS] = {'2511 389 40 40', tr('SKILLS')},
	[ID_SPELLS] = {'2420 389 40 40', tr('SPELLS')},
	[ID_STATS] = {'2556 389 40 40', tr('STATS')},
	[ID_RUNES] = {'2466 389 40 40', tr('RUNES')},
	[ID_CHARMS] = {'2706 410 40 40', tr('CHARMS')},
}

m_CharacterFunctions.destroySelect = function()
	if m_CharacterFunctions.selectId == ID_SKILLS then
		m_SkillFunctions.destroy()
	elseif m_CharacterFunctions.selectId == ID_RUNES then
		m_RunesFunctions.destroy()
	elseif m_CharacterFunctions.selectId == ID_STATS then
		m_StatsFunctions.destroy()
	elseif m_CharacterFunctions.selectId == ID_SPELLS then
		m_SpellsFunctions.destroy()
	elseif m_CharacterFunctions.selectId == ID_CHARMS then
		m_CharmsFunctions.destroy()
	end
end

m_CharacterFunctions.updateIcon = function()
	local v = m_CharacterFunctions.config[m_CharacterFunctions.selectId]
	m_CharacterFunctions.icon:setImageClip(v[1])
	m_CharacterFunctions.title:setText(v[2])
end

m_CharacterFunctions.select = function(self, id, ignore)
	id = tonumber(id)
	if not ignore and m_CharacterFunctions.selectId == id then
		return true
	end

	m_CharacterFunctions.destroySelect()
	m_CharacterFunctions.selectId = id
	if id == ID_SKILLS then
		m_SkillFunctions.open(m_CharacterFunctions.description)
	elseif id == ID_RUNES then
		m_RunesFunctions.open(m_CharacterFunctions.description)
	elseif id == ID_STATS then
		m_StatsFunctions.open(m_CharacterFunctions.description)
	elseif id == ID_SPELLS then
		m_SpellsFunctions.open(m_CharacterFunctions.description)
	elseif id == ID_CHARMS then
		m_CharmsFunctions.open(m_CharacterFunctions.description)
	end
	
	if m_CharacterFunctions.current then
		m_CharacterFunctions.current:setOn(false)
	end
	
	m_CharacterFunctions.updateIcon()
	m_CharacterFunctions.current = self
	self:setOn(true)
end

m_CharacterFunctions.instantSelect = function(id)
	if id == ID_SKILLS then
		m_CharacterFunctions.select(m_CharacterFunctions.window:getChildById('1'), id, true)
	elseif id == ID_SPELLS then
		m_CharacterFunctions.select(m_CharacterFunctions.window:getChildById('2'), id, true)
	elseif id == ID_STATS then
		m_CharacterFunctions.select(m_CharacterFunctions.window:getChildById('3'), id, true)
	elseif id == ID_RUNES then
		m_CharacterFunctions.select(m_CharacterFunctions.window:getChildById('4'), id, true)
	elseif id == ID_CHARMS then
		m_CharacterFunctions.select(m_CharacterFunctions.window:getChildById('5'), id, true)
	end
end

m_CharacterFunctions.selectById = function(id)
	if not m_CharacterFunctions.window then
		m_CharacterFunctions.create(true)
	end
	
	m_CharacterFunctions.instantSelect(id)
end

m_CharacterFunctions.create = function(ignore, id)
	if m_CharacterFunctions.window then
		m_CharacterFunctions.destroy()
	else
		modules.game_encyclopedia.m_EncyclopediaFunctions.destroy()
		modules.game_rewards.m_RewardsFunctions.destroy()
		modules.game_questlog.m_QuestLogFunctions.destroy()
		modules.game_store.m_StoreFunctions.onCloseStore()
		modules.game_market.m_MarketFunctions.destroy()
		
		m_CharacterFunctions.window = g_ui.displayUI('game_character')
		m_CharacterFunctions.window.onMouseRelease = m_SpellsFunctions.updateSpellShadowMouseRelease
		m_CharacterFunctions.description = m_CharacterFunctions.window:getChildById('descriptionWindow')
		m_CharacterFunctions.icon = m_CharacterFunctions.window:getChildById('icon')
		m_CharacterFunctions.title = m_CharacterFunctions.window:getChildById('title')
		
		if not ignore then
			if id then
				m_CharacterFunctions.instantSelect(id)
			elseif m_CharacterFunctions.selectId then
				m_CharacterFunctions.instantSelect(m_CharacterFunctions.selectId)
			else
				m_CharacterFunctions.select(m_CharacterFunctions.window:getChildById('1'), ID_SKILLS)
			end
		end
		
		modules.game_bottompanel.m_MainFunctions.open(WINDOW_STATS)
	end
end

m_CharacterFunctions.destroy = function()
	if m_CharacterFunctions.window then
		m_CharacterFunctions.destroySelect()
		
		m_CharacterFunctions.window:destroy()
		m_CharacterFunctions.window = nil
		
		m_CharacterFunctions.description = nil
		m_CharacterFunctions.icon = nil
		m_CharacterFunctions.title = nil
		m_CharacterFunctions.current = nil
		
		modules.game_bottompanel.m_MainFunctions.close(WINDOW_STATS)
	end
end

function onGameEnd()
	m_CharacterFunctions.destroy()
	m_CharmsFunctions.onGameEnd()
end

function onGameStart()
	m_SkillFunctions.onGameStart()
	m_RunesFunctions.onGameStart()
	m_StatsFunctions.onGameStart()
	m_SpellsFunctions.onGameStart()
	m_CharmsFunctions.onGameStart()
end

function onLoad()
	g_ui.importStyle('game_skills')
	g_ui.importStyle('game_runes')
	g_ui.importStyle('game_stats')
	g_ui.importStyle('game_spells')
	g_ui.importStyle('game_charms')
	
	m_SkillFunctions.onLoad()
	m_RunesFunctions.onLoad()
	m_StatsFunctions.onLoad()
	m_SpellsFunctions.onLoad()
	m_CharmsFunctions.onLoad()
	
	connect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
end

function onUnload()
	m_CharacterFunctions.destroy()
	
	m_SkillFunctions.onUnload()
	m_RunesFunctions.onUnload()
	m_StatsFunctions.onUnload()
	m_SpellsFunctions.onUnload()
	m_CharmsFunctions.onUnload()
	
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
end