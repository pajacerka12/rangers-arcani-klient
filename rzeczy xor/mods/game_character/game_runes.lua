m_RunesFunctions.helpDescription = {
				{tr('Rune Slots:'), tr('Every 5 levels we are granted a new slot for a rune, the maximum ammount of slots is 10.')},
				{'\n' .. tr('Unknown Rune:'), tr('Every monster has a chance to drop an unkown rune which we are able to identify and get one crystal crate to roll one of the 40 possible runes. The only exceptions are some Bosses which drop a specified rune.\nThe chances to obtain an unkown rune are the following:\nBelow 200 experience - 0.08%%\nFrom 201 to 650 experience - 0.12%%\nFrom 651 to 1750 experience - 0.18%%\nAbove 1751 experience - 0.25%%')},
				{'\n' .. tr('Rune level:'), list = 
									{
									top = tr('The identified rune may have different levels specified with the following colours:'),
									{percent = '0,2% - 0,4% -', color = 'desolate', chance = ', - 19.40% ' .. tr('Chance')},
									{percent = '0,5% - 0,8% -', color = 'damaged', chance = ', - 22.93% ' .. tr('Chance')},
									{percent = '0,9% - 1,2% -', color = 'normal', chance = ', - 44.09% ' .. tr('Chance')},
									{percent = '1,3% - 1,6% -', color = 'perfect', chance = ', - 12.35% ' .. tr('Chance')},
									{percent = '1,7% - 1,9% -', color = 'legendary', chance = ', - 0.88% ' .. tr('Chance')},
									{percent = '2,0% - 2,0% -', color = 'unique', chance = ', - 0.35% ' .. tr('Chance')}
									}},
				{'\n' .. tr('Rune Words:'), tr('To create a rune word you will need 5 runes arranged in a specified order. After arranging the rune word the colour of the dot in our menu will change from orange to green.\nAfter clicking on the dot we can find out what rune word we have unlocked. You can only have one rune word equipped at once.')},
				{'\n' .. tr('Important:'), tr('We can lose runes the same way as we can drop our equipement (5%% chance to lose a single rune).')}
				}

function onChangeRuneWord(id, word, list)
	m_RunesFunctions.onChangeRuneWord(id, word, list)
end

m_RunesFunctions.onLoad = function()
	connect(g_game, {
		onChangeRuneWord = onChangeRuneWord
	})
end

m_RunesFunctions.onUnload = function()
	disconnect(g_game, {
		onChangeRuneWord = onChangeRuneWord
	})
end

m_RunesFunctions.onGameStart = function()
	
end

m_RunesFunctions.execute = function()
	local player = g_game.getLocalPlayer()
	if not player then
		return false
	end
	
	for i = InventoryRune1, InventoryRune10 do
		local widget = m_RunesList.window:getChildById('slot' .. i)
		if player:getLevel() < (i - InventoryRune1 + 1) * 5 then
			widget:setIcon('/images/leaf/spellWindow/question_mark')
			widget:setTooltip(tr('Available from level ') .. ((i - InventoryRune1 + 1) * 5))
		end
		
		if g_game.isOnline() then
			modules.game_inventory.onInventoryChange(player, i, player:getInventoryItem(i))
		else
			modules.game_inventory.onInventoryChange(player, i, nil)
		end
	end
	
	local msg = OutputMessage.create()
	msg:addU8(74)
	g_game.getProtocolGame():send(msg)
end

m_RunesFunctions.destroy = function()
	m_RunesFunctions.hoverDestroy()
	
	if m_RunesList.window then
		m_RunesList.attributes:destroyChildren()
	
		m_RunesList.window:destroy()
		m_RunesList.window = nil
		
		m_RunesList = {}
	end
end

m_RunesFunctions.open = function(parent)
	if m_RunesList.window then
		m_RunesFunctions.destroy()
	else
		m_RunesList.window = g_ui.createWidget('RunesWindow', parent)
		m_RunesList.attributes = m_RunesList.window:getChildById('list')
		m_RunesList.window:getChildById('helpButton').onHoverChange = m_RunesFunctions.hover
		
		m_RunesFunctions.execute()
	end
end

m_RunesFunctions.hoverDestroy = function()
	if m_RunesList.hover then
		m_RunesList.hover:getChildById('list'):destroyChildren()
		
		m_RunesList.hover:destroy()
		m_RunesList.hover = nil
	end
end

m_RunesFunctions.hoverCreate = function(self)
	if m_RunesList.hover then
		return false
	end
	
	m_RunesList.hover = g_ui.createWidget('RunesHoverWindow', m_RunesList.window)
	
	local height = 16
	local list = m_RunesList.hover:getChildById('list')
	for i = 1, #m_RunesFunctions.helpDescription do
		for j = 1, #m_RunesFunctions.helpDescription[i] do
			height = height + m_RunesFunctions.addLabel(m_RunesFunctions.helpDescription[i][j], j == 1, list)
			
			local description = m_RunesFunctions.helpDescription[i].list
			if description then
				height = height + m_RunesFunctions.addLabel(description.top, false, list)
				for k = 1, #description do
					local widget = g_ui.createWidget('RuneLabel', parent)
					
					local percent = widget:getChildById('percent')
					local color = widget:getChildById('color')
					local chance = widget:getChildById('chance')
					
					percent:setText(description[k].percent)
					color:setText(tr(description[k].color))
					color:setColor(ITEM_NAME_COLOR[description[k].color])
					chance:setText(description[k].chance)
					
					widget:setText(' ')
					widget:setParent(list)
					
					height = height + widget:getHeight()
				end
				
				break
			end
		end
	end
	
	m_RunesList.hover:setHeight(height)
	
	local pos = m_RunesList.window:getPosition()
	pos.x = pos.x + m_RunesList.window:getWidth() - m_RunesList.hover:getWidth() - 12
	pos.y = pos.y + m_RunesList.window:getHeight() - m_RunesList.hover:getHeight() - 12
	
	m_RunesList.hover:setPosition(pos)
end

m_RunesFunctions.addLabel = function(label, font, parent)
	local widget = g_ui.createWidget('RunesLabel', parent)	
	if font then
		widget:setOn(true)
	end
				
	widget:setText(label)
	widget:setParent(parent)
	return widget:getHeight()
end

m_RunesFunctions.hover = function(widget, hovered)
	if hovered then
		m_RunesFunctions.hoverCreate(widget)
	else
		m_RunesFunctions.hoverDestroy()
	end
end

m_RunesFunctions.getRunesWindow = function()
	return m_RunesList.window
end

m_RunesFunctions.onChangeRuneWord = function(id, word, list)
	if not m_RunesList.window then
		return false
	end
	
	local self = nil
	local widget = m_RunesList.window:getChildById('description' .. id)
	if word ~= '' then
		if #list > 0 then
			widget:setOn(true)
			widget:setImageClip(torect('35 19 15 15'))
			self = widget
		else
			widget:setOn(false)
			widget:setImageClip(torect('18 19 15 15'))
			m_RunesFunctions.destroyList(widget)
		end
	else
		widget:setImageClip(torect('1 19 15 15'))
	end
	
	local widget = m_RunesList.window:getChildById('rune' .. id)
	if word == '' then
		word = tr('- None')
	end
	
	widget:setText(word)
	
	if self then
		m_RunesFunctions.show(self, list, word)
	end
end

m_RunesFunctions.destroyList = function(self)
	local id = string.gsub(self:getId(), 'description', '')
	id = tonumber(id)
	if not id then
		return false
	end
	
	for _, pid in pairs(m_RunesList.attributes:getChildren()) do
		local fromId = tonumber(pid:getId())
		if fromId == id or fromId == 3 then
			pid:destroy()
		end
	end
end

m_RunesFunctions.destroyListById = function(id)
	for _, pid in pairs(m_RunesList.attributes:getChildren()) do
		if tonumber(pid:getId()) == id then
			pid:destroy()
		end
	end
end

m_RunesFunctions.separator = function()
	local widget = g_ui.createWidget('RunesAttributesSeparator', m_RunesList.attributes)
	widget:setParent(m_RunesList.attributes)
	widget:setId(3)
	widget:setMarginRight(18)
end

m_RunesFunctions.show = function(self, list, word)
	if not self:isOn() then
		return false
	end
	
	local id = string.gsub(self:getId(), 'description', '')
	id = tonumber(id)
	if not id then
		return false
	end
	
	m_RunesFunctions.destroyListById(id)
	if #m_RunesList.attributes:getChildren() > 0 then
		m_RunesFunctions.separator()
	end
	
	for i = 1, #list do
		m_RunesFunctions.setIconLabel(m_RunesList.attributes, list[i], id)
	end
end

m_RunesFunctions.setIconLabel = function(parent, var, id)
	local widget = g_ui.createWidget('RunesAttribute', parent)
	widget:setParent(parent)
	widget:setId(id)
	
	local icon = widget:getChildById('icon')
	local name = widget:getChildById('label')
	
	local attribute, value, combat, monster, skill, chance = unpack(var)
	local description, attributeType = '', ''
	
	value = (value > 0xFFF and -(value - 0xFFF) or value)
	if value ~= 0 then
		if attribute == ITEM_COUNT_STOLEN_MANA or attribute == ITEM_COUNT_STOLEN_LIFE then
			value = value / 100
		elseif attribute == ITEM_CRITICAL or attribute == ITEM_ABSORB or attribute == ITEM_INCREMENT then
			value = value / 10
		end
	
		description = (value > 0 and '+' or '') .. value
	end
	
	if combat == COMBAT_BLEEDDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 37)
		attributeType = 'bleed '
	elseif combat == COMBAT_DEATHDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 35)
		attributeType = 'death '
	elseif combat == COMBAT_EARTHDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 34)
		attributeType = 'earth '
	elseif combat == COMBAT_ENERGYDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 31)
		attributeType = 'energy '
	elseif combat == COMBAT_FIREDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 32)
		attributeType = 'fire '
	elseif combat == COMBAT_HOLYDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 36)
		attributeType = 'holy '
	elseif combat == COMBAT_ICEDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 33)
		attributeType = 'ice '
	elseif combat == COMBAT_PHYSICALDAMAGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 30)
		attributeType = 'physical '
	else
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 16)
	end
	
	if skill == SKILL_STRENGTH then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 19)
		attributeType = 'strength '
	elseif skill == SKILL_DEXTERITY then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 9)
		attributeType = 'dexterity '
	elseif skill == SKILL_CONDITION then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 27)
		attributeType = 'condition '
	elseif skill == SKILL_WISDOM then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 17)
		attributeType = 'wisdom '
	elseif skill == SKILL_VIGOR then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 49)
		attributeType = 'vigor '
	end
	
	local percent = true
	if attribute == ITEM_INCREMENT then
		attributeType = tr(attributeType .. 'damage')
	elseif attribute == ITEM_REFLECT then
		attributeType = tr('%s%% chance to reflect %s%%', description, chance) .. tr(' ' .. attributeType .. 'damage')
		description = nil
	elseif attribute == ITEM_ABSORB then
		attributeType = tr(attributeType .. 'absorb')
	elseif attribute == ITEM_BONUS_DAMAGE then
		attributeType = tr('additional ' .. attributeType .. 'damage')
	elseif attribute == ITEM_CRITICAL then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 12)
		attributeType = attributeType .. tr('critical hit chance')
	elseif attribute == ITEM_CRITICAL_HIT_MULTIPLIER then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 12)
		attributeType = attributeType .. tr('critical hit multiplier')
	elseif attribute == ITEM_DOUBLE_HIT then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 11)
		attributeType = attributeType .. tr('double hit chance')
	elseif attribute == ITEM_MAXMANA then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 40)
		attributeType = attributeType .. tr('mana points')
	elseif attribute == ITEM_MAXHEALTH then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 39)
		attributeType = attributeType .. tr('health points')
	elseif attribute == ITEM_COUNT_STOLEN_MANA then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 45)
		attributeType = attributeType .. tr('manasteal')
	elseif attribute == ITEM_COUNT_STOLEN_LIFE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 13)
		attributeType = attributeType .. tr('lifesteal')
	elseif attribute == ITEM_LUCK then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 41)
		attributeType = attributeType .. tr('luck')
	elseif attribute == ITEM_DODGE_MELEE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 51)
		attributeType = attributeType .. tr('chance to dodge melee attacks')
	elseif attribute == ITEM_DODGE_RANGE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 52)
		attributeType = attributeType .. tr('chance to dodge range attacks')
	elseif attribute == ITEM_MONSTER_DAMAGE then
		attributeType = attributeType .. tr('damage aganist ')
	elseif attribute == ITEM_SKILL then
		percent = false
	elseif attribute == ITEM_IGNORE_ARMOR then
		percent = false
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 14)
		attributeType = attributeType .. tr('armor penetration')
	elseif attribute == ITEM_ARMOR then
		percent = false
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 2)
		attributeType = attributeType .. tr('armor')
	elseif attribute == ITEM_SPEED then
		percent = false
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 24)
		attributeType = attributeType .. tr('movement speed')
	elseif attribute == ITEM_NO_MANA then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 40)
		description = '-100'
		attributeType = attributeType .. tr('mana points')
	elseif attribute == ITEM_DECREASE_ARMOR then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 2)
		description = '-100'
		attributeType = attributeType .. tr('armor')
	elseif attribute == ITEM_SPAWN_COIN then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 15)
		description = ''
		percent = false
		attributeType = tr('Generate copper coins around target')
	elseif attribute == ITEM_DEATH_HIT then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 35)
		attributeType = tr('death hit')
	elseif attribute == ITEM_HIT_CHANCE then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, 47)
		attributeType = attributeType .. tr('hit chance')
	elseif attribute == ITEM_SUPPRESS then
		if value == CONDITION_DRUNK then
			attributeType = tr('Drunkenness immunity')
		elseif value == CONDITION_FEAR then
			attributeType = tr('Fear immunity')
		elseif value == CONDITION_STUN then
			attributeType = tr('Stun immunity')
		elseif value == CONDITION_PUSH then
			attributeType = tr('Push immunity')
		end
		
		description = ''
		percent = false
	else
		attributeType = '[ERROR]'
	end
	
	if monster == MONSTER_HUMAN then
		attributeType = attributeType .. tr('humans')
	elseif monster == MONSTER_UNDEAD then
		attributeType = attributeType .. tr('undeads')
	elseif monster == MONSTER_DEMON then
		attributeType = attributeType .. tr('demons')
	elseif monster == MONSTER_GIANT then
		attributeType = attributeType .. tr('giants')
	elseif monster == MONSTER_ORC then
		attributeType = attributeType .. tr('orcs')
	elseif monster == MONSTER_MINOTAUR then
		attributeType = attributeType .. tr('minotaurs')
	elseif monster == MONSTER_WIZARD then
		attributeType = attributeType .. tr('wizards')
	elseif monster == MONSTER_DRAGON then
		attributeType = attributeType .. tr('dragons')
	end
	
	if description then
		if percent then
			description = description .. '%'
			attributeType = ' ' .. attributeType
		else
			description = description
		end
		
		name:setText(description .. attributeType)
	else
		name:setText(attributeType)
	end
	
	widget:setHeight(name:getHeight())
end
