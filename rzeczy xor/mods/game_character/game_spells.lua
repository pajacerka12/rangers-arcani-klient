m_SpellsFunctions.warningMessage = tr('Do you really want reset all yours spells?')
m_SpellsFunctions.helpDescription = {
				{tr('Spells points'), tr('Each new level of experience gives 1 point which can be spend on spells and their upgrades. You will receive points until level 120, in total you can spend 121 points. To be able to buy a spell you need to meet specific requirements like level of your character, vocation and previous spells from the list. Purchased spell will be automatically added to the spell bar, but only if option \'Add to the spell bar\' is selected.')},
				{'\n' .. tr('Adding spells'), tr('You can add all purchased spells to the spell bar by moving it from spell list to free slot on spell bar with your left mouse button. You can also double click on your spell to add it automatically, spell icon will appear in the first free slot on the bar.')},
				{'\n' .. tr('Removing spells'), tr('To delete a spell from the spell bar, you need to right click on it and choose \'Delete\', the spell will be removed from the spell bar by freeing the slot.')},
				{'\n' .. tr('Description of a spell'), tr('To learn about details of a spell, hover cursor over it icon. You can also right click on spell to open a new window with spell details.')},
				{'\n' .. tr('Spells categories'), tr('Every vocation got it own, unique spell tree and \'Utility\' tree. To move between categories, all you need to do is select the group that interests you on the left side of the window.')},
				{'\n' .. tr('Select vocation'), tr('Each skill basic skill tree has two paths of development, which will define which vocation should you become. To choose a vocation you need to spend a point into the vocation spell which will automatically promote you to your desired one, reset all of your spell points and it will update your skills and their active effects (strength, dexterity, condition, wisdom, vigor).')}
				}

m_SpellsFunctions.GameServerUpdateShootRange = 54
m_SpellsFunctions.GameServerUpdateSpellList = 148

m_SpellsFunctions.GameServerSendSpells = 62

m_SpellsFunctions.left = 1
m_SpellsFunctions.right = 2
m_SpellsFunctions.bottom = 3

m_SpellsFunctions.config = {
	[m_SpellsFunctions.right] = {
		[0] = '2686 286 47 16',
		[1] = '2357 29 47 16',
		[2] = '2462 29 47 16',
		[3] = '2247 86 47 16',
		[4] = '2356 85 47 16',
		[5] = '2462 82 47 16',
		[6] = '2246 141 47 16',
		[7] = '2356 139 47 16',
		[8] = '2464 137 47 16',
		[9] = '2244 194 47 16',
		[10] = '2357 192 47 16',
		[11] = '2465 190 47 16',
		[12] = '2244 303 47 16',
		[13] = '2466 247 47 16',
		[14] = '2244 248 47 16',
		[15] = '2357 248 47 16'
	},
	[m_SpellsFunctions.left] = {
		[0] = '2682 258 47 16',
		[1] = '2353 8 47 16',
		[2] = '2458 8 47 16',
		[3] = '2243 65 47 16',
		[4] = '2352 64 47 16',
		[5] = '2458 61 47 16',
		[6] = '2242 120 47 16',
		[7] = '2352 118 47 16',
		[8] = '2460 116 47 16',
		[9] = '2240 173 47 16',
		[10] = '2353 171 47 16',
		[11] = '2461 169 47 16',
		[12] = '2240 282 47 16',
		[13] = '2462 226 47 16',
		[14] = '2240 227 47 16',
		[15] = '2353 227 47 16'
	},
	[m_SpellsFunctions.bottom] = {
		[0] = '2630 260 16 47',
		[1] = '2313 5 16 47',
		[2] = '2418 5 16 47',
		[3] = '2203 62 16 47',
		[4] = '2312 61 16 47',
		[5] = '2418 58 16 47',
		[6] = '2202 117 16 47',
		[7] = '2312 115 16 47',
		[8] = '2420 113 16 47',
		[9] = '2200 170 16 47',
		[10] = '2313 168 16 47',
		[11] = '2421 166 16 47',
		[12] = '2200 279 16 47',
		[13] = '2422 223 16 47',
		[14] = '2200 224 16 47',
		[15] = '2313 224 16 47'
	}
}

function onPlayerStats(points)
	m_SpellsFunctions.onPlayerStats(points)
end

function onVocationChange(localPlayer, vocation)
	m_SpellsFunctions.onVocationChange(localPlayer, vocation)
end

m_SpellsFunctions.onLoad = function()
	g_ui.importStyle('SpellWindows/Mage')
	g_ui.importStyle('SpellWindows/Extended_Mage')
	g_ui.importStyle('SpellWindows/Hunter')
	g_ui.importStyle('SpellWindows/Extended_Hunter')
	g_ui.importStyle('SpellWindows/Mercenary')
	g_ui.importStyle('SpellWindows/Extended_Mercenary')
	
	g_ui.importStyle('SpellWindows/Druid')
	g_ui.importStyle('SpellWindows/Extended_Druid')
	g_ui.importStyle('SpellWindows/Wizard')
	g_ui.importStyle('SpellWindows/Extended_Wizard')
	
	g_ui.importStyle('SpellWindows/Grenadier')
	g_ui.importStyle('SpellWindows/Extended_Grenadier')
	g_ui.importStyle('SpellWindows/Tracker')
	g_ui.importStyle('SpellWindows/Extended_Tracker')
	
	g_ui.importStyle('SpellWindows/Paladin')
	g_ui.importStyle('SpellWindows/Extended_Paladin')
	g_ui.importStyle('SpellWindows/Barbarian')
	g_ui.importStyle('SpellWindows/Extended_Barbarian')
	
	g_ui.importStyle('SpellWindows/Utilities')
	g_ui.importStyle('SpellWindows/HunterUtilities')
	
	m_SpellsFunctions.spellList = {}
	
	m_SpellsFunctions.points = 0
	m_SpellsFunctions.restarts = 0
	m_SpellsFunctions.shootRange = 1
	
	m_SpellsFunctions.showAreaSpells = true
	m_SpellsFunctions.addSpellToBar = true
	
	ProtocolGame.registerOpcode(m_SpellsFunctions.GameServerUpdateSpellList, m_SpellsFunctions.parseSpellList)
	ProtocolGame.registerOpcode(m_SpellsFunctions.GameServerUpdateShootRange, m_SpellsFunctions.parseShootRange)
	connect(g_game, {
		onPlayerStats = onPlayerStats
	})
	
	connect(LocalPlayer, {
		onVocationChange = onVocationChange
	})
end

m_SpellsFunctions.onUnload = function()
	modules.client_options.setOption('showAreaSpells', m_SpellsFunctions.showAreaSpells, true)
	modules.client_options.setOption('addSpellToBar', m_SpellsFunctions.addSpellToBar, true)
	
	ProtocolGame.unregisterOpcode(m_SpellsFunctions.GameServerUpdateSpellList, m_SpellsFunctions.parseSpellList)
	ProtocolGame.unregisterOpcode(m_SpellsFunctions.GameServerUpdateShootRange, m_SpellsFunctions.parseShootRange)
	disconnect(g_game, {
		onPlayerStats = onPlayerStats
	})
	
	disconnect(LocalPlayer, {
		onVocationChange = onVocationChange
	})
end

m_SpellsFunctions.onGameStart = function()
	m_SpellsFunctions.spellList = {}
	m_SpellsFunctions.currentTypePanel = nil
end

m_SpellsFunctions.updateRestart = function()
	if m_SpellsList.resetButton then
		m_SpellsList.resetButton:setText('Reset ( ' .. m_SpellsFunctions.restarts .. ' )')
		m_SpellsList.resetButton:setEnabled(m_SpellsFunctions.restarts ~= 0)
	end
end

m_SpellsFunctions.execute = function()
	m_SpellsList.window:getChildById('addToSpellbar'):setChecked(m_SpellsFunctions.addSpellToBar)
	m_SpellsList.window:getChildById('showAreaSpells'):setChecked(m_SpellsFunctions.showAreaSpells)
	
	m_SpellsList.pointsLabel:setText(m_SpellsFunctions.points)
	m_SpellsFunctions.onVocationCheck()
	m_SpellsFunctions.updateRestart()
end

m_SpellsFunctions.destroyDescription = function()
	if m_SpellsList.descriptionWindow then
		m_SpellsList.descriptionWindow:destroy()
		m_SpellsList.descriptionWindow = nil
	end
end

m_SpellsFunctions.destroyUpgrade = function()
	if m_SpellsList.upgradeWindow then
		m_SpellsList.upgradeWindow:destroy()
		m_SpellsList.upgradeWindow = nil
	end
end

m_SpellsFunctions.destroyShadow = function()
	if m_SpellsList.spellShadow then
		m_SpellsList.spellShadow:destroy()
		m_SpellsList.spellShadow = nil
	end
end

m_SpellsFunctions.destroyCurrent = function()
	if m_SpellsList.current then
		m_SpellsList.current:destroy()
		m_SpellsList.current = nil
	end
end

m_SpellsFunctions.destroy = function()
	m_SpellsFunctions.destroyPopup()
	m_SpellsFunctions.destroyShadow()
	m_SpellsFunctions.destroyDescription()
	m_SpellsFunctions.destroyUpgrade()
	m_SpellsFunctions.destroyCurrent()
	m_SpellsFunctions.hoverDestroy()
	
	if m_SpellsList.window then
		m_SpellsList.window:destroy()
		m_SpellsList.window = nil
		
		m_SpellsList.pointsLabel = nil
		
		m_SpellsList = {}
	end
end

m_SpellsFunctions.addHelpLabel = function(label, font, parent)
	local widget = g_ui.createWidget('HoverSpellLabel', parent)
	if font then
		widget:setOn(true)
	end
	
	widget:setText(label)
	widget:setParent(parent)
	return widget:getHeight()
end

m_SpellsFunctions.hoverCreate = function(self)
	if m_SpellsList.hover then
		return false
	end
	
	m_SpellsList.hover = g_ui.createWidget('SpellsHoverWindow', m_SpellsList.window)
	
	local height = 16
	local list = m_SpellsList.hover:getChildById('list')
	for i = 1, #m_SpellsFunctions.helpDescription do
		for j = 1, #m_SpellsFunctions.helpDescription[i] do
			height = height + m_SpellsFunctions.addHelpLabel(m_SpellsFunctions.helpDescription[i][j], j == 1, list)
			
			local description = m_SpellsFunctions.helpDescription[i].list
			if description then
				height = height + m_SpellsFunctions.addHelpLabel(description.top, false, list)
				for k = 1, #description do
					local widget = g_ui.createWidget('SpellHoverLabel', list)
					
					local percent = widget:getChildById('percent')
					local color = widget:getChildById('color')
					local chance = widget:getChildById('chance')
					
					percent:setText(description[k].percent)
					color:setText(tr(description[k].color))
					color:setColor(ITEM_NAME_COLOR[description[k].color])
					chance:setText(description[k].chance)
					
					widget:setText(' ')
					widget:setParent(list)
					
					height = height + widget:getHeight()
				end
				
				break
			end
		end
	end
	
	m_SpellsList.hover:setHeight(height)
	
	local pos = m_SpellsList.window:getPosition()
	pos.x = pos.x + m_SpellsList.window:getWidth() - m_SpellsList.hover:getWidth() - 12
	pos.y = pos.y + m_SpellsList.window:getHeight() - m_SpellsList.hover:getHeight() - 12
	
	m_SpellsList.hover:setPosition(pos)
end

m_SpellsFunctions.hoverDestroy = function()
	if m_SpellsList.hover then
		m_SpellsList.hover:getChildById('list'):destroyChildren()
		
		m_SpellsList.hover:destroy()
		m_SpellsList.hover = nil
	end
end

m_SpellsFunctions.hover = function(widget, hovered)
	if hovered then
		m_SpellsFunctions.hoverCreate(widget)
	else
		m_SpellsFunctions.hoverDestroy()
	end
end

m_SpellsFunctions.open = function(parent)
	if m_SpellsList.window then
		m_SpellsFunctions.destroy()
	else
		m_SpellsList.parent = parent:getParent()
		m_SpellsList.window = g_ui.createWidget('SpellsWindow', parent)
		m_SpellsList.pointsLabel = m_SpellsList.window:getChildById('points')
		m_SpellsList.pointsSpent = m_SpellsList.window:getChildById('pointsSpent')
		m_SpellsList.resetButton = m_SpellsList.window:getChildById('resetButton')
		m_SpellsList.window:getChildById('helpButton').onHoverChange = m_SpellsFunctions.hover
		
		m_SpellsFunctions.execute()
	end
end

m_SpellsFunctions.onPlayerStats = function(points)
	m_SpellsFunctions.points = points
	if m_SpellsList.pointsLabel then
		m_SpellsList.pointsLabel:setText(points)
	end
end

m_SpellsFunctions.setAddSpell = function(check)
	m_SpellsFunctions.addSpellToBar = check
end

m_SpellsFunctions.canAddSpellToBar = function()
	return m_SpellsFunctions.addSpellToBar
end

m_SpellsFunctions.setVisibleArea = function(check)
	m_SpellsFunctions.showAreaSpells = check
end

m_SpellsFunctions.isShowingArea = function()
	return m_SpellsFunctions.showAreaSpells
end

m_SpellsFunctions.getImageClip = function(id)
	return (id < 12 and id or id - (math.floor(id / 12) * MAX_ATTACKS)) * ICON_SIZE .. ' ' .. (math.floor(id / MAX_ATTACKS)) * ICON_SIZE .. ' ' .. ICON_SIZE .. ' ' .. ICON_SIZE
end

m_SpellsFunctions.onDescriptionMouseRelease = function(self, mousePosition, mouseButton)
	local data = m_SpellsList.currentSpellList[self.name]
	if not data then
		return true
	end
	
	if mouseButton == MouseRightButton then
		modules.game_spellbar.m_SpellBarFunctions.createLookSpell(data)
	elseif mouseButton == MouseLeftButton then
		if (m_SpellsFunctions.spellList[self.name] or 0) == 0 then
			return false
		end
		
		if not m_SpellsList.delay or m_SpellsList.delay <= os.time() then
			m_SpellsList.delay = os.time() + 1
		elseif not data.passive then
			modules.game_spellbar.m_SpellBarFunctions.addToSpellBar(data, true)
			m_SpellsList.delay = 0
		end
	end
end

m_SpellsFunctions.onDescriptionMousePress = function(self, mousePosition, mouseButton)
	local data = m_SpellsList.currentSpellList[self.name]
	if not data or data.passive then
		return true
	end
	
	if mouseButton ~= MouseLeftButton or (m_SpellsFunctions.spellList[self.name] or 0) == 0 then
		return true
	end
	
	if not m_SpellsList.spellShadow then
		m_SpellsList.spellShadow = g_ui.displayUI('SpellWindows/spell_shadow')
	end
	
	m_SpellsList.spellShadow.data = data
	m_SpellsList.spellShadow:setImageClip(m_SpellsFunctions.getImageClip(data.iconId))
	
	mousePosition.x = mousePosition.x - (m_SpellsList.spellShadow:getWidth() / 2)
	mousePosition.y = mousePosition.y - (m_SpellsList.spellShadow:getHeight() / 2)
	m_SpellsList.spellShadow:setPosition(mousePosition)
end

m_SpellsFunctions.updateSpellShadowPosition = function(mousePosition)
	if not m_SpellsList.spellShadow then
		return true
	end
	
	mousePosition.x = mousePosition.x - (m_SpellsList.spellShadow:getWidth() / 2)
	mousePosition.y = mousePosition.y - (m_SpellsList.spellShadow:getHeight() / 2)
	m_SpellsList.spellShadow:setPosition(mousePosition)
	
	modules.game_spellbar.m_SpellBarFunctions.updateSpellPosition()
end

m_SpellsFunctions.updateSpellShadowMouseRelease = function()
	if m_SpellsList.spellShadow then
		modules.game_spellbar.m_SpellBarFunctions.updateSpellShadowMouseRelease(m_SpellsList.spellShadow.data)
		
		m_SpellsFunctions.destroyShadow()
	end
end

m_SpellsFunctions.onVocationCheck = function()
	local player = g_game.getLocalPlayer()
	if not player then
		return true
	end
	
	local vocationId = player:getVocation()
	local list = {}
	local current = ''
	if vocationId <= 3 then
		list = {1, 2, 3, 4}
		id = 1
		current = 'Mage'
	elseif vocationId <= 6 then
		list = {5, 6, 7, 8}
		id = 5
		current = 'Hunter'
	else
		list = {9, 10, 11, 4}
		id = 9
		current = 'Mercenary'
	end
	
	if not m_SpellsFunctions.currentTypePanel then
		m_SpellsFunctions.setCategory(m_SpellsList.window:getChildById('button_' .. id), current)
	elseif m_SpellsFunctions.m_CurrentId then
		m_SpellsFunctions.setCategory(m_SpellsList.window:getChildById(m_SpellsFunctions.m_CurrentId), m_SpellsFunctions.currentTypePanel, true)
	end
	
	for _, v in pairs(list) do
		m_SpellsList.window:getChildById('button_' .. v):show()
	end
end

m_SpellsFunctions.addLabel = function(parent, name, description, iconId)
	local widget = g_ui.createWidget(name, parent)
	widget.onText = function(self, coords, text, iconCoords)
		local i = 1
		for _, v in pairs(iconCoords) do
			local id, icon = unpack(v)
			if icon.x ~= 0 and icon.y ~= 0 then
				local pos = self:getPosition()
				local offset = {x = icon.x - pos.x - 1, y = icon.y - pos.y - 1}
				
				local skillIcon = g_ui.createWidget('SkillIconWidget', widget)
				if id == 0 then
					id = 19
				elseif id == 1 then
					id = 51
				elseif id == 2 then
					id = 27
					offset.x = offset.x - 8
				elseif id == 3 then
					id = 17
				elseif id == 4 then
					id = 49
				end
				
				skillIcon:setMarginTop(offset.y)
				skillIcon:setMarginLeft(offset.x)
				
				i = i + 1
				modules.game_lookat.m_LookAtFunctions.setIconImageType(skillIcon, id)
			end
		end
	end
	
	if iconId then
		modules.game_lookat.m_LookAtFunctions.setIconImageType(widget:getChildById('icon'), iconId)
	end
	
	widget:setText(description)
	return widget
end

m_SpellsFunctions.getSpeciesSpell = function(type, species)
	local description = ''
	if species == TYPE_OFFENSIVE then
		description = tr('offensive spell')
	elseif species == TYPE_STRENGTH then
		description = tr('strengthening spell')
	elseif species == TYPE_WEAK then
		description = tr('weakening spell')
	elseif species == TYPE_DEFENSIVE then
		description = tr('defensive spell')
	elseif species == TYPE_SUMMON then
		description = tr('summoning spell')
	elseif species == TYPE_PASSIVE then
		description = tr('passive skill')
		
	elseif species == TYPE_SUPPORT then
		description = tr('support skill')
	elseif species == TYPE_AURA then
		description = tr('aura skill')
	elseif species == TYPE_TAUNT then
		description = tr('taunt skill')
	end
	
	description = description .. ', '
	if type == SINGLE_SHOT then
		description = description .. tr('single')
	elseif type == AOE_AREA then
		description = description .. tr('area')
	end
	
	return description, '#846F60'
end

m_SpellsFunctions.getSpellRange = function()
	return m_SpellsFunctions.shootRange
end

m_SpellsFunctions.updateUpgrade = function(data, player, parent, prev)
	local list = SpelllistSettings[data.name]
	local points = math.max(0, data.points - (prev and 1 or 0))
	if type(list.mana) == 'function' then
		data.mana = list.mana(points)
	else
		data.mana = list.mana or 0
	end
	
	if list.ignoreRange then
		data.ignoreRange = true
	end
	
	if type(list.cooldown) == 'function' then
		data.cooldown = list.cooldown(points)
	else
		data.cooldown = list.cooldown or 0
	end
	
	if type(list.level) == 'function' then
		data.level = list.level(points)
	else
		data.level = list.level or points
	end
	
	if type(list.duration) == 'function' then
		data.duration = list.duration(points)
	else
		data.duration = list.duration
	end
	
	local widget = m_SpellsFunctions.addLabel(parent, 'SpellDescriptionLabel', data.level .. tr(' level'), 16)
	local tmpHeight = widget:getHeight()
	if player:getLevel() < data.level then
		widget:setColor('#B00101')
	end
	
	if not data.passive then
		widget = m_SpellsFunctions.addLabel(parent, 'SpellDescriptionLabel', (data.mana <= 0 and tr('variable') or data.mana) .. tr(' mana points'), 40)
		tmpHeight = tmpHeight + widget:getHeight() + 10
		if player:getMaxMana() < data.mana then
			widget:setColor('#B00101')
		end
	end
	
	if data.cooldown > 0 then
		widget = m_SpellsFunctions.addLabel(parent, 'SpellDescriptionLabel', data.cooldown .. ' ' .. tr('seconds of cooldown'), 38)
		tmpHeight = tmpHeight + widget:getHeight() + 10
	end
	
	if data.duration then
		local minutes = 0
		if data.duration > 60 then
			local seconds = data.duration
			while(seconds >= 60) do
				minutes = minutes + 1
				seconds = seconds - 60
			end
		end
		
		widget = m_SpellsFunctions.addLabel(parent, 'SpellDescriptionLabel', tr('Duration') .. ' ' .. (minutes > 0 and (minutes .. ' ' .. tr('minutes')) or (data.duration .. ' ' .. tr('seconds'))), 27)
		tmpHeight = tmpHeight + widget:getHeight() + 10
	end
	
	if data.hideSpell then
		local active, description = getRequiredVocationNameById(player:getVocation(), data.vocation)
		widget = m_SpellsFunctions.addLabel(parent, 'SpellDescriptionLabel', description, 46)
		if not active then
			widget:setColor('#B00101')
		end
		
		tmpHeight = tmpHeight + widget:getHeight() + 10
	end
	
	if data.blocking then
		widget = m_SpellsFunctions.addLabel(parent, 'SpellDescriptionLabel', tr('Possible to buy only for residents of Cirith.'), 52)
		tmpHeight = tmpHeight + widget:getHeight() + 10
		
		if player:getTownId() == TOWN_CHAECK then
			widget:setColor('#B00101')
		end
	end
	
	if widget then
		widget:getChildById('separator'):hide()
	end
	
	return tmpHeight
end

m_SpellsFunctions.onDescriptionHover = function(self, hovered)
	if not hovered then
		if m_SpellsList.descriptionWindow then	
			m_SpellsList.descriptionWindow:hide()
		end
		
		return true
	end
	
	local data = m_SpellsList.currentSpellList[self.name]
	if not data then
		return true
	end
	
	if not m_SpellsList.descriptionWindow then
		m_SpellsList.descriptionWindow = g_ui.displayUI('SpellWindows/info_panel')
	else
		m_SpellsList.descriptionWindow:show()
	end
	
	local icon = m_SpellsList.descriptionWindow:getChildById('icon')
	icon:setImageClip(m_SpellsFunctions.getImageClip(data.iconId))
	
	local list = m_SpellsList.descriptionWindow:getChildById('detail')
	local name = data.name:sub(1, 1):upper() .. data.name:sub(2, data.name:len())
	local height = 96
	
	list:destroyChildren()
	m_SpellsFunctions.addLabel(list, 'SpellNameLabel', name .. ' (' .. data.points .. '/' .. data.spellLevel .. ')')
	
	-- set spell species
	local description, color = m_SpellsFunctions.getSpeciesSpell(data.spellType, data.spellSpecies)
	local widget = m_SpellsFunctions.addLabel(list, 'SpellDetailsLabel', description)
	widget:setColor(color)
	
	-- set spell upgrade
	local leftAddMouseButton = m_SpellsList.descriptionWindow:getChildById('leftAddMouseButton')
	local rightMouseButton = m_SpellsList.descriptionWindow:getChildById('rightMouseButton')
	local leftMouseButton = m_SpellsList.descriptionWindow:getChildById('leftMouseButton')
	local tmpSeparator = m_SpellsList.descriptionWindow:getChildById('tmpSeparator')
	local separator = m_SpellsList.descriptionWindow:getChildById('separator')
	local upgradePrev = m_SpellsList.descriptionWindow:getChildById('upgradePrev')
	local upgradeNext = m_SpellsList.descriptionWindow:getChildById('upgradeNext')
	local upgradeArrow = m_SpellsList.descriptionWindow:getChildById('upgradeArrow')
	
	local displayHints = modules.client_options.getOption('displayHints')
	if not displayHints then
		rightMouseButton:hide()
		leftMouseButton:hide()
		leftAddMouseButton:hide()
		separator:hide()
		height = height - 32
	else
		rightMouseButton:show()
		rightMouseButton:setOn(data.points == 0)
		leftMouseButton:setVisible(data.points > 0)
		leftAddMouseButton:setVisible(data.points > 0)
		if data.points > 0 then
			height = height + 84
		else
			height = height + 14
		end
	end
	
	upgradePrev:show()
	upgradeNext:show()
	
	if displayHints then
		separator:show()
	end
	
	upgradePrev:destroyChildren()
	upgradeNext:destroyChildren()
	
	local player = g_game.getLocalPlayer()
	if data.points < 1 or data.points >= data.spellLevel then
		upgradeArrow:hide()
	elseif data.points < data.spellLevel then
		upgradeArrow:show()
		m_SpellsFunctions.updateUpgrade(data, player, upgradeNext, false)
	end
	
	local parent = prevData and upgradeNext or upgradePrev
	if data.points >= data.spellLevel then
		parent = upgradePrev
	end
	
	local tmpHeight = m_SpellsFunctions.updateUpgrade(data, player, parent, true) + 6
	upgradePrev:setHeight(tmpHeight)
	upgradeNext:setHeight(tmpHeight)
	height = height + tmpHeight + 24
	
	local level = m_SpellsList.descriptionWindow:getChildById('level')
	level:destroyChildren()
	for i = 1, data.spellLevel do
		local widget = m_SpellsFunctions.addLabel(level, 'LevelIcon', '')
		widget:setOn(data.points >= i)
	end
	
	if data.description then
		local language = g_settings.get('locale', 'en')
		if language == 'en' then
			language = LANGUAGE_ENGLISH
		else
			language = LANGUAGE_POLISH
		end
		
		local description
		local spellLevel = math.max(0, data.points - 1)
		if data.description[0] then
			if data.value then
				local value = data.value(spellLevel)
				description = data.description[spellLevel][language]:format(unpack(value))
			else
				description = data.description[spellLevel][language]
			end
		else
			if data.value then
				local value = data.value(spellLevel)
				description = data.description[language]:format(unpack(value))
			else
				description = data.description[language]
			end
		end
		
		if description then
			local descriptionBox = m_SpellsList.descriptionWindow:getChildById('description')
			
			descriptionBox:destroyChildren()
			local widget = m_SpellsFunctions.addLabel(descriptionBox, 'SpellLabel', description)
			descriptionBox:setHeight(widget:getHeight())
			height = height + widget:getHeight() + 5
		end
	end
	
	m_SpellsList.descriptionWindow:setHeight(height)
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 8
	pos.y = pos.y + self:getHeight() - 8
	
	local rootWidget = modules.game_interface.getRootPanel()
	if pos.x + m_SpellsList.descriptionWindow:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_SpellsList.descriptionWindow:getWidth()
	end
	
	if pos.y + m_SpellsList.descriptionWindow:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_SpellsList.descriptionWindow:getHeight()
	end
	
	m_SpellsList.descriptionWindow:focus()
	m_SpellsList.descriptionWindow:setPosition(pos)
end

m_SpellsFunctions.onDescriptionHoverChange = function(self, hovered)
	if not hovered then
		if m_SpellsList.upgradeWindow then
			m_SpellsList.upgradeWindow:hide()
		end
		
		return true
	end
	
	local data = m_SpellsList.currentSpellList[self.name]
	if not data then
		return true
	end
	
	if self.reverse then
		data.points = math.min(data.points, data.spellLevel)
	else
		data.points = math.max(data.points, 0)
	end
	
	local language = g_settings.get('locale', 'false')
	if language == 'false' then
		language = 'en'
	end
		
	if language == 'en' then
		language = LANGUAGE_ENGLISH
	else
		language = LANGUAGE_POLISH
	end
	
	local descriptionBox = nil
	if not m_SpellsList.upgradeWindow then
		m_SpellsList.upgradeWindow = g_ui.displayUI('SpellWindows/spell_panel')
		
		descriptionBox = m_SpellsList.upgradeWindow:getChildById('description')
		descriptionBox.onText = function(self, coords, text, iconCoords)
			descriptionBox:destroyChildren()
			
			local i = 1
			for _, v in pairs(iconCoords) do
				local id, icon = unpack(v)
				if icon.x ~= 0 and icon.y ~= 0 then
					local pos = self:getPosition()
					local offset = {x = icon.x - pos.x, y = icon.y - pos.y}
					
					local skillIcon = g_ui.createWidget('SkillIconWidget', descriptionBox)
					if id == 0 then
						id = 19
					elseif id == 1 then
						id = 51
					elseif id == 2 then
						id = 27
						offset.x = offset.x - 8
					elseif id == 3 then
						id = 17
					elseif id == 4 then
						id = 49
					end
					
					skillIcon:setMarginTop(offset.y)
					skillIcon:setMarginLeft(offset.x)
					
					i = i + 1
					modules.game_lookat.m_LookAtFunctions.setIconImageType(skillIcon, id)
				end
			end
		end
	else
		m_SpellsList.upgradeWindow:show()
		
		descriptionBox = m_SpellsList.upgradeWindow:getChildById('description')
	end
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 2
	pos.y = pos.y + self:getHeight() - 2
	
	local rootWidget = modules.game_interface.getRootPanel()
	if pos.x + m_SpellsList.upgradeWindow:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_SpellsList.upgradeWindow:getWidth()
	end
	
	if pos.y + m_SpellsList.upgradeWindow:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_SpellsList.upgradeWindow:getHeight()
	end
	
	m_SpellsList.upgradeWindow:focus()
	m_SpellsList.upgradeWindow:setPosition(pos)
	
	if descriptionBox then
		descriptionBox:clearText()
		for i = 1, 2 do
			m_SpellsList.upgradeWindow:getChildById('skillIcon' .. i):hide()
		end
		
		local description
		local spellLevel = data.points
		if self.reverse then
			spellLevel = math.max(0, spellLevel - 2)
		end
		
		if data.description[0] then
			if data.value then
				local value = data.value(spellLevel)
				description = data.description[spellLevel][language]:format(unpack(value))
			else
				description = data.description[spellLevel][language]
			end
		else
			if data.value then
				local value = data.value(spellLevel)
				description = data.description[language]:format(unpack(value))
			else
				description = data.description[language]
			end
		end
		
		if description then
			descriptionBox:setText(description)
		
			local size = descriptionBox:getSize()
			size.width = m_SpellsList.upgradeWindow:getWidth()
			size.height = size.height + 20
			m_SpellsList.upgradeWindow:setSize(size)
		end
	end
end

m_SpellsFunctions.getSpell = function(name, list)
	for k, v in pairs(m_SpellsList.currentSpellList) do
		if v.spellRequied then
			if isInArray(v.spellRequied, name) then
				if not isInArray(list, k) then
					table.insert(list, k)
					list = m_SpellsFunctions.getSpell(k, list)
				end
			end
		end
	end
	
	return list
end

m_SpellsFunctions.removeSpell = function(name)
	for i = 1, #m_SpellsList.investedPoints do
		if m_SpellsList.investedPoints[i] == name then
			table.remove(m_SpellsList.investedPoints, i)
			break
		end
	end
end

m_SpellsFunctions.destroyPopup = function()
	if m_SpellsList.popup then
		m_SpellsList.popup:destroy()
		m_SpellsList.popup = nil
		
		m_SpellsList.parent:setEnabled(true)
		m_SpellsList.parent:focus()
		m_SpellsList.parent:raise()
	end
end

m_SpellsFunctions.createPopup = function(name)
	m_SpellsList.parent:setEnabled(false)
	m_SpellsFunctions.destroyPopup()
	
	m_SpellsList.popup = g_ui.displayUI(name)
	m_SpellsList.popup:focus()
	m_SpellsList.popup:raise()
end

m_SpellsFunctions.restart = function()
	if m_SpellsFunctions.restarts == 0 then
		m_SpellsList.resetButton:setEnabled(false)
		return true
	end
	
	m_SpellsFunctions.createPopup('popup_window')
	m_SpellsList.popup:getChildById('description'):setText(m_SpellsFunctions.warningMessage)
end

m_SpellsFunctions.resetAccept = function()
	m_SpellsFunctions.destroyPopup()
	
	local msg = OutputMessage.create()
	msg:addU8(m_SpellsFunctions.GameServerSendSpells)
	msg:addU8(2)
	
	g_game.getProtocolGame():send(msg)
	m_SpellsFunctions.reset()
end

m_SpellsFunctions.reset = function(restart)
	m_SpellsList.pointsList = {}
	m_SpellsList.investedPoints = {}
	
	m_SpellsFunctions.pointsSpent = 0
	m_SpellsList.pointsSpent:setText(0)
	m_SpellsList.blocking = false
	
	if not restart then
		m_SpellsList.pointsLabel:setText(m_SpellsFunctions.points)
	end
end

m_SpellsFunctions.clear = function()
	for name, var in pairs(m_SpellsList.currentSpellList) do
		m_SpellsFunctions.updateIconSpell(name, m_SpellsFunctions.spellList[name] or 0, true)
	end
	
	m_SpellsFunctions.reset()
end

m_SpellsFunctions.accept = function()
	local msg = OutputMessage.create()
	msg:addU8(m_SpellsFunctions.GameServerSendSpells)
	msg:addU8(1)
	msg:addU16(#m_SpellsList.investedPoints)
	for i = 1, #m_SpellsList.investedPoints do
		local name = m_SpellsList.investedPoints[i]
		msg:addString(name)
		msg:addU16(m_SpellsList.pointsList[name])
		m_SpellsFunctions.updateIconSpell(name, 0, true)
	end
	
	g_game.getProtocolGame():send(msg)
	m_SpellsFunctions.reset(true)
end

m_SpellsFunctions.setCategory = function(self, category, ignore)
	if not ignore and m_SpellsFunctions.currentTypePanel == category then
		return true
	end
	
	local player = g_game.getLocalPlayer()
	if not player then
		return true
	end
	
	if m_SpellsList.m_CurrentButton then
		m_SpellsList.m_CurrentButton:getChildById('blink'):hide()
	end
	
	m_SpellsFunctions.m_CurrentId = self:getId()
	m_SpellsList.m_CurrentButton = self
	self:getChildById('blink'):show()
	
	m_SpellsFunctions.destroyCurrent()
	m_SpellsList.current = g_ui.createWidget('Extended_' .. category, m_SpellsList.window)
	
	m_SpellsFunctions.currentTypePanel = category
	
	local vocationId = 0
	if category == 'Mage' then
		vocationId = 1
	elseif category == 'Druid' then
		vocationId = 2
	elseif category == 'Wizard' then
		vocationId = 3
	elseif category == 'Hunter' then
		vocationId = 4
	elseif category == 'Tracker' then
		vocationId = 5
	elseif category == 'Grenadier' then
		vocationId = 6
	elseif category == 'Mercenary' then
		vocationId = 7
	elseif category == 'Paladin' then
		vocationId = 8
	elseif category == 'Barbarian' then
		vocationId = 9
	end
	
	if vocationId == 0 then
		vocationId = player:getVocation()
	end
	
	m_SpellsList.currentSpellList = {}
	m_SpellsFunctions.reset()
	for k, v in pairs(SpelllistSettings) do
		if v.ignoreVocation or not v.vocation or isInArray(v.vocation, vocationId) then
			m_SpellsFunctions.addSpell(k, v)
			if v.utility then
				m_SpellsFunctions.addActiveSpell(k, 1)
			end
		end
	end
	
	for k, v in pairs(m_SpellsFunctions.spellList) do
		m_SpellsFunctions.addActiveSpell(k, v, true)
	end
end

m_SpellsFunctions.onVocationChange = function(localPlayer, vocation)
	if not m_SpellsList.currentSpellList then
		return true
	end
	
	for k, v in pairs(m_SpellsList.currentSpellList) do
		local widget = m_SpellsList.current:getChildById(k)
		if widget then
			local tmpWidget = widget:getChildById('visibleMask')
			if v.vocation and not isInArray(v.vocation, vocation) then
				widget:getChildById('plus'):hide()
				widget:getChildById('minus'):hide()
				tmpWidget:show()
			else
				tmpWidget:hide()
			end
		end
	end
end

m_SpellsFunctions.setImageLineType = function(widget, type, active)
	if not widget then
		return true
	end
	
	local id = m_SpellsFunctions.bottom
	if widget:isOn() then
		id = m_SpellsFunctions.right
	elseif widget:isChecked() then
		id = m_SpellsFunctions.left
	end
	
	if not active then
		type = 0
	end
	
	widget:setImageClip(m_SpellsFunctions.config[id][type])
end

m_SpellsFunctions.updateIconSpell = function(name, points, first)
	local widget = m_SpellsList.current:getChildById(name)
	if not widget then
		return false
	end
	
	local data = m_SpellsList.currentSpellList[name]
	if not data then
		return false
	end
	
	local enabled = points > 0
	m_SpellsFunctions.setImageLineType(m_SpellsList.current:getChildById('line ' .. name), data.type, enabled)
	m_SpellsFunctions.setImageLineType(m_SpellsList.current:getChildById('line1 ' .. name), data.type, enabled)
	
	local buttonP = widget:getChildById('plus')
	local buttonM = widget:getChildById('minus')
	if points == data.spellLevel then
		buttonP:hide()
	else
		buttonP:show()
	end
	
	if first then
		buttonM:hide()
	else
		buttonM:show()
	end
	
	if data.spellRequied then
		local available = true
		for i = 1, #data.spellRequied do
			local var = m_SpellsFunctions.spellList[data.spellRequied[i]] or 0
			if var == 0 then
				available = false
			end
			
			local id = data.spellRequied[i]
			m_SpellsFunctions.setImageLineType(m_SpellsList.current:getChildById('requied ' .. name), m_SpellsList.currentSpellList[id].type, enabled)
		end
		
		if points == 0 and not available then
			buttonP:hide()
			buttonM:hide()
		end
	end
	
	if points == 0 then
		for k, v in pairs(m_SpellsList.currentSpellList) do
			if v.spellRequied and isInArray(v.spellRequied, name) then
				m_SpellsList.current:getChildById(k):getChildById('plus'):hide()
			end
		end
	end
	
	local icon = widget:getChildById('icon')
	if enabled then
		icon:setImageSource('/images/leaf/spellWindow/spellIcons')
	else
		icon:setImageSource('/images/leaf/spellWindow/spellIcons_black')
	end
	
	local tmpWidget = widget:getChildById('visibleMask')
	if data.vocation and not isInArray(data.vocation, g_game.getLocalPlayer():getVocation()) then
		tmpWidget:show()
		buttonP:hide()
		buttonM:hide()
	else
		tmpWidget:hide()
	end
		
	widget:getChildById('label'):setText(points .. '/' .. data.spellLevel)
	m_SpellsList.currentSpellList[name] = m_SpellsFunctions.getData(name, SpelllistSettings[name], points)
	return m_SpellsList.currentSpellList[name]
end

m_SpellsFunctions.addActiveSpell = function(name, points, ignore)
	if not ignore then
		m_SpellsFunctions.spellList[name] = points
	end
	
	local data = m_SpellsFunctions.getData(name, SpelllistSettings[name], points)
	if not m_SpellsList.current then
		return data
	end
	
	m_SpellsFunctions.updateIconSpell(name, points, true)
	
	if points > 0 then
		for k, v in pairs(m_SpellsList.currentSpellList) do
			if v.spellRequied and isInArray(v.spellRequied, name) then
				local continue = true
				for i = 1, #v.spellRequied do
					if v.spellRequied[i] ~= name and ((m_SpellsFunctions.spellList[v.spellRequied[i]] or 0) + (m_SpellsList.pointsList[v.spellRequied[i]] or 0)) == 0 then
						continue = false
						break
					end
				end
				
				if continue then
					local var = m_SpellsList.currentSpellList[k]
					if var and (m_SpellsFunctions.spellList[k] or 0) + (m_SpellsList.pointsList[k] or 0) < var.spellLevel then
						if not var.vocation or isInArray(var.vocation, g_game.getLocalPlayer():getVocation()) then
							m_SpellsList.current:getChildById(k):getChildById('plus'):show()
						end
					end
				end
			end
		end
	end
	
	return data
end

m_SpellsFunctions.addSpell = function(name, var)
	if not m_SpellsList.current then
		return true
	end
	
	local widget = m_SpellsList.current:getChildById(name)
	if not widget then
		return true
	end
	
	local icon = widget:getChildById('icon')
	icon:setImageSource('/images/leaf/spellWindow/spellIcons_black')
	icon:setImageClip(m_SpellsFunctions.getImageClip(var.iconId or 0))
	icon.name = name
	
	icon.onHoverChange = m_SpellsFunctions.onDescriptionHover
	icon.onMouseRelease = m_SpellsFunctions.onDescriptionMouseRelease
	icon.onMousePress = m_SpellsFunctions.onDescriptionMousePress
	
	widget:getChildById('label'):setText('0/' .. (var.spellLevel or 1))

	local data = m_SpellsFunctions.getData(name, var, 0)
	m_SpellsList.currentSpellList[name] = data
	
	local buttonP = widget:getChildById('plus')
	local buttonM = widget:getChildById('minus')
	buttonP.name = name
	buttonP.reverse = false
	buttonP.onHoverChange = m_SpellsFunctions.onDescriptionHoverChange
	buttonM.name = name
	buttonM.reverse = true
	buttonM.onHoverChange = m_SpellsFunctions.onDescriptionHoverChange
	
	buttonM:hide()
	if data.spellRequied then
		for i = 1, #data.spellRequied do
			local var = m_SpellsFunctions.spellList[data.spellRequied[i]] or 0
			if var == 0 then
				buttonP:hide()
				break
			end
		end
	end
	
	if data.hideSpell then
		widget:getChildById('visibleMask'):show()
		buttonP:hide()
		buttonM:hide()
	end
end

m_SpellsFunctions.parseShootRange = function(protocol, msg)
	m_SpellsFunctions.shootRange = msg:getU16()
end

m_SpellsFunctions.parseSpellList = function(protocol, msg)
	m_SpellsFunctions.onPlayerStats(msg:getU16())
	m_SpellsFunctions.restarts = msg:getU16()
	m_SpellsFunctions.updateRestart()
	
	local size = msg:getU16()
	if size == 0 then
		return true
	end
	
	for i = 1, size do
		local name = msg:getString()
		local points = msg:getU16()
		
		m_SpellsFunctions.spellList[name] = points
		modules.game_spellbar.onAddAttack(name, points, false)
	end
end

m_SpellsFunctions.getData = function(name, var, points)
	if not var then
		return false
	end
	
	local data = {}
	data.points = points
	data.name = name
	
	local points = math.max(0, math.min(var.spellLevel - 1, points))
	local tmpPoints = math.max(0, math.min(var.spellLevel - 1, data.points - 1))
	local vPoints = math.max(0, math.min(var.spellLevel, data.points))
	if type(var.duration) == 'function' then
		data.duration = var.duration(tmpPoints)
	else
		data.duration = var.duration or 0
	end
	
	if type(var.mana) == 'function' then
		data.mana = var.mana(tmpPoints)
	else
		data.mana = var.mana or 0
	end
	
	if type(var.cooldown) == 'function' then
		data.cooldown = var.cooldown(tmpPoints)
	else
		data.cooldown = var.cooldown or 0
	end
	
	if type(var.level) == 'function' then
		data.level = var.level(points)
	else
		data.level = var.level or points
	end
	
	if type(var.spellArea) == 'function' then
		data.spellArea = var.spellArea(vPoints)
	else
		data.spellArea = var.spellArea or 0
	end
	
	if type(var.area) == 'function' then
		data.area = var.area(vPoints)
	else
		data.area = var.area or 0
	end
	
	if type(var.range) == 'function' then
		data.range = var.range(vPoints)
	else
		data.range = var.range or 0
	end
	
	if var.ignoreRange then
		data.ignoreRange = true
	end
	
	data.hideSpell = var.vocation and not isInArray(var.vocation, g_game.getLocalPlayer():getVocation())
	data.type = var.type
	data.passive = var.passive
	data.spellType = var.spellType
	data.spellSpecies = var.spellSpecies
	data.value = var.value
	data.effectId = var.effectId
	data.spellRequied = var.spellRequied
	data.vocation = var.vocation
	data.spellLevel = var.spellLevel
	data.iconId = var.iconId
	data.description = var.description
	data.blocking = var.blocking
	return data
end

m_SpellsFunctions.onClickButton = function(self, parent, value)
	if m_SpellsFunctions.points < 1 or not m_SpellsList.current then
		return false
	end
	
	local points = 0
	local activeSpellPoints = 0
	local currentSpellPoints = 0
	local data = {}
	local name = parent:getId()
	local player = g_game.getLocalPlayer()
	if value then
		if m_SpellsFunctions.pointsSpent >= m_SpellsFunctions.points then
			return true
		end
		
		activeSpellPoints = m_SpellsFunctions.spellList[name] or 0
		currentSpellPoints = m_SpellsList.pointsList[name] or 0
		
		points = activeSpellPoints + currentSpellPoints
		data = m_SpellsFunctions.getData(name, SpelllistSettings[name], points)
		
		if points >= data.spellLevel or (data.blocking and m_SpellsList.blocking and m_SpellsList.blocking ~= name) then
			return true
		end
		
		if player:getLevel() < data.level or (data.vocation and not isInArray(data.vocation, player:getVocation())) then
			return false
		end
	else
		data = m_SpellsList.currentSpellList[name]
		
		activeSpellPoints = m_SpellsFunctions.spellList[name] or 0
		currentSpellPoints = m_SpellsList.pointsList[name] or 0
		
		points = activeSpellPoints + currentSpellPoints
		if points <= activeSpellPoints then
			return true
		end
	end
	
	points = points + (value and 1 or -1)
	
	m_SpellsFunctions.updateIconSpell(name, points, false)
	
	local widget = m_SpellsList.current:getChildById(name)
	local buttonP = widget:getChildById('plus')
	local buttonM = widget:getChildById('minus')
	if value then
		if data.blocking then
			m_SpellsList.blocking = name
		end
		
		m_SpellsFunctions.pointsSpent = (m_SpellsFunctions.pointsSpent or 0) + 1
		if points > 0 and points > currentSpellPoints then
			buttonM:show()
			
			local vocationId = player:getVocation()
			for k, v in pairs(m_SpellsList.currentSpellList) do
				if not v.vocation or isInArray(v.vocation, vocationId) then
					if v.spellRequied and isInArray(v.spellRequied, name) then
						local continue = true
						for i = 1, #v.spellRequied do
							if v.spellRequied[i] ~= name and ((m_SpellsFunctions.spellList[v.spellRequied[i]] or 0) + (m_SpellsList.pointsList[v.spellRequied[i]] or 0)) == 0 then
								continue = false
								break
							end
						end
						
						if continue then
							local var = m_SpellsList.currentSpellList[k]
							if var and (m_SpellsFunctions.spellList[k] or 0) + (m_SpellsList.pointsList[k] or 0) < var.spellLevel then
								m_SpellsList.current:getChildById(k):getChildById('plus'):show()
							end
						end
					end
				end
			end
		end
		
		if points == data.spellLevel then
			buttonP:hide()
		end
	else
		if data.blocking then
			m_SpellsList.blocking = false
		end
		
		m_SpellsFunctions.pointsSpent = (m_SpellsFunctions.pointsSpent or 0) - 1
		if points == 0 then
			buttonM:hide()
			m_SpellsFunctions.removeSpell(name)
			
			local list = {}
			list = m_SpellsFunctions.getSpell(name, list)
			
			for k, v in pairs(list) do
				m_SpellsFunctions.pointsSpent = (m_SpellsFunctions.pointsSpent or 0) - (m_SpellsList.pointsList[v] or 0)
				m_SpellsFunctions.removeSpell(v)
				m_SpellsList.pointsList[v] = 0
				m_SpellsFunctions.updateIconSpell(v, m_SpellsFunctions.spellList[v] or 0, false)
			end
		elseif points == activeSpellPoints then
			buttonM:hide()
		end
		
		if points < data.spellLevel then
			buttonP:show()
		end
	end
	
	if (m_SpellsList.pointsList[name] or 0) == 0 and not isInArray(m_SpellsList.investedPoints, name) then
		table.insert(m_SpellsList.investedPoints, name)
	end
	
	m_SpellsList.pointsList[name] = currentSpellPoints + (value and 1 or -1)
	
	m_SpellsList.currentSpellList[name] = m_SpellsFunctions.getData(name, SpelllistSettings[name], points)
	m_SpellsList.pointsLabel:setText(m_SpellsFunctions.points - m_SpellsFunctions.pointsSpent)
	m_SpellsList.pointsSpent:setText(m_SpellsFunctions.pointsSpent > 0 and -m_SpellsFunctions.pointsSpent or m_SpellsFunctions.pointsSpent)
end