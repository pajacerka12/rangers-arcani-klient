m_StatsFunctions.config = {
	[1] = {
		{id = ITEM_MAXHEALTH, name = 'Health', icon = 39},
		{id = ITEM_REGEN_HEALTH, name = 'Health Regeneration', icon = 39},
		{id = ITEM_COUNT_STOLEN_LIFE, name = 'Lifesteal', icon = 13, digital = '%', multiplier = 0.1},
		{id = ITEM_MAXMANA, name = 'Mana', icon = 40},
		{id = ITEM_REGEN_MANA, name = 'Mana Regeneration', icon = 40},
		{id = ITEM_COUNT_STOLEN_MANA, name = 'Manasteal', icon = 45, digital = '%', multiplier = 0.1},
		{id = ITEM_SPEED, name = 'Movement Speed', icon = 24},
		{id = ITEM_ATTACK_SPEED, name = 'Attack Speed', icon = 20, digital = 'ms'},
		{id = ITEM_CRITICAL_HIT_MULTIPLIER, name = 'Critical Hit Multiplier', icon = 12, digital = '%', multiplier = 0.1},
		{id = ITEM_CRITICAL, name = 'Critical hit chance', icon = 12, digital = '%', multiplier = 0.01},
		{id = ITEM_DOUBLE_HIT, name = 'Double Hit chance', icon = 11, digital = '%', multiplier = 0.01},
		{id = ITEM_LUCK, name = 'Luck', icon = 41, digital = '%', multiplier = 0.01},
		{id = ITEM_HIT_CHANCE, name = 'Hit chance', icon = 47, digital = '%', multiplier = 0.1},
		{id = ITEM_DODGE_MELEE, name = 'Dodge melee attack', icon = 51, digital = '%', multiplier = 0.01},
		{id = ITEM_DODGE_RANGE, name = 'Dodge range attack', icon = 52, digital = '%', multiplier = 0.01},
		{id = ITEM_DEATH_HIT, name = 'Death hit', icon = 35, digital = '%', multiplier = 0.01},
		{id = ITEM_ARMOR, name = 'Armor', icon = 5},
		{id = ITEM_DEFENSE, name = 'Defense', icon = 6},
	},
	[2] = { id = ITEM_ABSORB,
		{id = COMBAT_PHYSICALDAMAGE, name = 'Physical Absorb', icon = 25, digital = '%', multiplier = 0.1},
		{id = COMBAT_BLEEDDAMAGE, name = 'Bleed Absorb', icon = 39, digital = '%', multiplier = 0.1},
		{id = COMBAT_FIREDAMAGE, name = 'Fire Absorb', icon = 32, digital = '%', multiplier = 0.1},
		{id = COMBAT_EARTHDAMAGE, name = 'Earth Absorb', icon = 34, digital = '%', multiplier = 0.1},
		{id = COMBAT_ENERGYDAMAGE, name = 'Energy Absorb', icon = 31, digital = '%', multiplier = 0.1},
		{id = COMBAT_ICEDAMAGE, name = 'Ice Absorb', icon = 33, digital = '%', multiplier = 0.1},
		{id = COMBAT_DEATHDAMAGE, name = 'Death Absorb', icon = 35, digital = '%', multiplier = 0.1},
		{id = COMBAT_HOLYDAMAGE, name = 'Holy Absorb', icon = 36, digital = '%', multiplier = 0.1},
		{id = COMBAT_LIFEDRAIN, name = 'Lifedrain Absorb', icon = 13, digital = '%', multiplier = 0.1},
		{id = COMBAT_MANADRAIN, name = 'Manadrain Absorb', icon = 45, digital = '%', multiplier = 0.1},
		{id = COMBAT_DROWNDAMAGE, name = 'Drown Absorb', icon = 23, digital = '%', multiplier = 0.1},
		{id = COMBAT_WINDDAMAGE, name = 'Wind Absorb', icon = 16, digital = '%', multiplier = 0.1},
	},
	[3] = { id = ITEM_INCREMENT,
		{id = COMBAT_PHYSICALDAMAGE, name = 'Physical Increment', icon = 25, digital = '%', multiplier = 0.1},
		{id = COMBAT_BLEEDDAMAGE, name = 'Bleed Increment', icon = 39, digital = '%', multiplier = 0.1},
		{id = COMBAT_FIREDAMAGE, name = 'Fire Increment', icon = 32, digital = '%', multiplier = 0.1},
		{id = COMBAT_EARTHDAMAGE, name = 'Earth Increment', icon = 34, digital = '%', multiplier = 0.1},
		{id = COMBAT_ENERGYDAMAGE, name = 'Energy Increment', icon = 31, digital = '%', multiplier = 0.1},
		{id = COMBAT_ICEDAMAGE, name = 'Ice Increment', icon = 33, digital = '%', multiplier = 0.1},
		{id = COMBAT_DEATHDAMAGE, name = 'Death Increment', icon = 35, digital = '%', multiplier = 0.1},
		{id = COMBAT_HOLYDAMAGE, name = 'Holy Increment', icon = 36, digital = '%', multiplier = 0.1},
		{id = COMBAT_LIFEDRAIN, name = 'Lifedrain Increment', icon = 13, digital = '%', multiplier = 0.1},
		{id = COMBAT_MANADRAIN, name = 'Manadrain Increment', icon = 45, digital = '%', multiplier = 0.1},
		{id = COMBAT_DROWNDAMAGE, name = 'Drown Increment', icon = 23, digital = '%', multiplier = 0.1},
		{id = COMBAT_WINDDAMAGE, name = 'Wind Increment', icon = 16, digital = '%', multiplier = 0.1},
	},
	[4] = { id = ITEM_BONUS_DAMAGE,
		{id = COMBAT_PHYSICALDAMAGE, name = 'Physical Bonus Damage', icon = 25, digital = '%', multiplier = 0.1},
		{id = COMBAT_BLEEDDAMAGE, name = 'Bleed Bonus Damage', icon = 39, digital = '%', multiplier = 0.1},
		{id = COMBAT_FIREDAMAGE, name = 'Fire Bonus Damage', icon = 32, digital = '%', multiplier = 0.1},
		{id = COMBAT_EARTHDAMAGE, name = 'Earth Bonus Damage', icon = 34, digital = '%', multiplier = 0.1},
		{id = COMBAT_ENERGYDAMAGE, name = 'Energy Bonus Damage', icon = 31, digital = '%', multiplier = 0.1},
		{id = COMBAT_ICEDAMAGE, name = 'Ice Bonus Damage', icon = 33, digital = '%', multiplier = 0.1},
		{id = COMBAT_DEATHDAMAGE, name = 'Death Bonus Damage', icon = 35, digital = '%', multiplier = 0.1},
		{id = COMBAT_HOLYDAMAGE, name = 'Holy Bonus Damage', icon = 36, digital = '%', multiplier = 0.1},
		{id = COMBAT_LIFEDRAIN, name = 'Lifedrain Bonus Damage', icon = 13, digital = '%', multiplier = 0.1},
		{id = COMBAT_MANADRAIN, name = 'Manadrain Bonus Damage', icon = 45, digital = '%', multiplier = 0.1},
		{id = COMBAT_DROWNDAMAGE, name = 'Drown Bonus Damage', icon = 23, digital = '%', multiplier = 0.1},
		{id = COMBAT_WINDDAMAGE, name = 'Wind Bonus Damage', icon = 16, digital = '%', multiplier = 0.1},
	},
}

m_StatsFunctions.titles = { tr('Type'), tr('Total'), tr('Spells'), tr('Equipment'), tr('Skills'), tr('Basic') }
m_StatsFunctions.statisticList = {}
m_StatsFunctions.page = 1

function onStatsUpdate(id, byEquipment, bySpell, bySkill, combatType)
	m_StatsFunctions.onStatsUpdate(id, byEquipment, bySpell, bySkill, combatType)
end

function onSkillInfo(var, armor, defense, defenseEffective)
	m_StatsFunctions.onSkillInfo(var, armor, defense, defenseEffective)
end

m_StatsFunctions.onLoad = function()
	connect(g_game, {
		onStatsUpdate = onStatsUpdate,
		onSkillInfo = onSkillInfo,
	})
end

m_StatsFunctions.onUnload = function()
	disconnect(g_game, {
		onStatsUpdate = onStatsUpdate,
		onSkillInfo = onSkillInfo
	})
end

m_StatsFunctions.onGameStart = function()
	m_StatsFunctions.statisticList = {}
	m_StatsFunctions.page = 1
end

m_StatsFunctions.destroy = function()
	if m_StatsList.window then
		m_StatsList.window:destroy()
		m_StatsList.window = nil
		m_StatsList.currentSelect = nil
		
		m_StatsList = {}
	end
end

m_StatsFunctions.open = function(parent)
	if m_StatsList.window then
		m_StatsFunctions.destroy()
	else
		m_StatsList.window = g_ui.createWidget('StatsWindow', parent)
		m_StatsList.list = m_StatsList.window:getChildById('list')
		m_StatsList.nextButton = m_StatsList.window:getChildById('nextButton')
		m_StatsList.prevButton = m_StatsList.window:getChildById('prevButton')
		m_StatsList.pageLabel = m_StatsList.window:getChildById('pageLabel')
		m_StatsList.nextButton:setEnabled(m_StatsFunctions.page < 4)
		m_StatsList.prevButton:setEnabled(m_StatsFunctions.page > 1)
		
		m_StatsList.width = (parent:getWidth() - 260) / 5
		
		m_StatsFunctions.execute()
	end
end

m_StatsFunctions.getList = function(list, byEquipment, bySpell, bySkill, byBasic, combatType)
	list.equipment = m_StatsFunctions.checkValue(byEquipment)
	list.spell = m_StatsFunctions.checkValue(bySpell)
	list.skill = m_StatsFunctions.checkValue(bySkill)
	list.var = combatType
	if byBasic then
		list.basic = m_StatsFunctions.checkValue(byBasic)
	end
	
	return list
end

m_StatsFunctions.execute = function()
	local msg = OutputMessage.create()
	msg:addU8(68)
	g_game.getProtocolGame():send(msg)
	
	local widget = g_ui.createWidget('SkillList', m_StatsList.list)
	widget:setEnabled(false)
	
	local list = {
					widget:getChildById('name'),
					widget:getChildById('byTotal'),
					widget:getChildById('bySpell'),
					widget:getChildById('byEquipment'),
					widget:getChildById('bySkill'),
					widget:getChildById('byBasic')
					}
	
	for i = 1, #m_StatsFunctions.titles do
		list[i]:setText(m_StatsFunctions.titles[i])
		if i ~= 1 then
			list[i]:setWidth(m_StatsList.width)
		end
	end
	
	m_StatsFunctions.updatePage()
end

m_StatsFunctions.checkValue = function(var)
	if not var or type(var) == 'table' then
		return var
	end
	
	return var > 0xFFFF and -(var - 0xFFFF) or var
end

m_StatsFunctions.select = function(self)
	if not self:isEnabled() or m_StatsList.currentSelect == self then
		return false
	end
	
	if m_StatsList.currentSelect then
		m_StatsList.currentSelect:setOn(false)
	end
	
	self:setOn(true)
	m_StatsList.currentSelect = self
end

m_StatsFunctions.onStatsUpdate = function(id, byEquipment, bySpell, bySkill, combatType, byBasic)
	local list = {}
	if combatType and combatType ~= 0 then
		if not m_StatsFunctions.statisticList[id] then
			m_StatsFunctions.statisticList[id] = {}
		end
		
		list = m_StatsFunctions.getList(m_StatsFunctions.statisticList[id][combatType] or {}, byEquipment, bySpell, bySkill, byBasic, combatType)
		m_StatsFunctions.statisticList[id][combatType] = list
		
		if m_StatsFunctions.page == 1 then
			return true
		end
	else
		list = m_StatsFunctions.getList(m_StatsFunctions.statisticList[id] or {}, byEquipment, bySpell, bySkill, byBasic)
		m_StatsFunctions.statisticList[id] = list
	end
	
	local configId = m_StatsFunctions.config[m_StatsFunctions.page]
	if  configId.id and id ~= configId.id then
		return true
	end
	
	m_StatsFunctions.createLabel(id, false, false, list.basic, list.equipment, list.spell, list.skill, list.var)
end

m_StatsFunctions.getArmorEffective = function(armor)
	local localPlayer = g_game.getLocalPlayer()
	local condition = localPlayer:getSkillEffectiveLevel(SkillCondition)
	local multiplier = 1 + (condition * 0.0002) * (1 + (armor * 0.01))
	local armor = math.max(0, armor) * multiplier
	if armor > 3 then
		return {math.ceil(armor / 2), math.ceil(armor - (armor % 2 + 1))}
	elseif armor > 0 then
		return {1, 1}
	end
	
	return {0, 0}
end

m_StatsFunctions.getDefenseEffective = function(defense)
	return {math.ceil(defense / 2), defense}
end

m_StatsFunctions.onSkillInfo = function(var, armor, defense, defenseEffective)
	for i = 1, #var do
		local id, byBasic, byEquipment, bySpell, bySkill = unpack(var[i])
		m_StatsFunctions.onStatsUpdate(id, byEquipment, bySpell, bySkill, false, byBasic)
	end
	
	if m_StatsFunctions.page == 1 then
		local armor = {armor, m_StatsFunctions.getArmorEffective(armor)}
		m_StatsFunctions.createLabel(ITEM_ARMOR, 'Armor', 5, 0, armor, 0, 0)
		m_StatsFunctions.statisticList[ITEM_ARMOR] = m_StatsFunctions.getList({}, armor, 0, 0, 0)
		
		local defense = {defense, m_StatsFunctions.getDefenseEffective(defenseEffective)}
		m_StatsFunctions.createLabel(ITEM_DEFENSE, 'Defense', 6, 0, defense, 0, 0)
		m_StatsFunctions.statisticList[ITEM_DEFENSE] = m_StatsFunctions.getList({}, defense, 0, 0, 0)
	end
end

m_StatsFunctions.createLabel = function(id, name, icon, byBasic, byEquipment, bySpell, bySkill, combatType)
	if not m_StatsList.list then
		return true
	end
	
	if combatType then
		id = combatType
	end
	
	local digital = ''
	local multiplier = 1
	local tmpId = false
	if not name or not icon then
		local configId = m_StatsFunctions.config[m_StatsFunctions.page]
		tmpId = configId.id
		
		for i = 1, #configId do
			local v = configId[i]
			if v.id == id then
				name = v.name
				icon = v.icon
				digital = v.digital or ''
				multiplier = v.multiplier or 1
				break
			end
		end
	end
	
	if not name or not icon then
		return true
	end
	
	local widget = m_StatsList.list:getChildById(id)
	if not widget then
		widget = g_ui.createWidget('SkillListWidget', m_StatsList.list)
		widget:setId(id)
	end
	
	local list = {
		widget:getChildById('icon'),
		widget:getChildById('name'),
		widget:getChildById('byTotal'),
		widget:getChildById('bySpell'),
		widget:getChildById('byEquipment'),
		widget:getChildById('bySkill'),
		widget:getChildById('byBasic')
	}
	
	byBasic = byBasic or 0
	byEquipment = byEquipment or 0
	bySpell = bySpell or 0
	bySkill = bySkill or 0
	
	local total = 0
	if (id == ITEM_ARMOR or id == ITEM_DEFENSE) and type(byEquipment) == 'table' then
		total = byEquipment[2][1] .. ' - ' .. byEquipment[2][2]
		list[3]:setText(total)
		
		byEquipment = byEquipment[1]
	else
		if not tmpId and id == ITEM_ATTACK_SPEED then
			total = (byBasic - (byEquipment + bySpell + bySkill)) * multiplier
		else
			total = (byBasic + byEquipment + bySpell + bySkill) * multiplier
		end
		
		if m_StatsFunctions.page == 1 then
			if (id == ITEM_CRITICAL or id == ITEM_DOUBLE_HIT) and total > 50 then
				total = 50 .. digital .. ' (' .. total .. digital .. ')'
				list[3]:setColor('#F9000E')
			elseif id == ITEM_CRITICAL_HIT_MULTIPLIER and total > 200 then
				total = 200 .. digital .. ' (' .. total .. digital .. ')'
				list[3]:setColor('#F9000E')
			elseif (id == ITEM_DODGE_MELEE or id == ITEM_DODGE_RANGE) and total > 50 then
				total = 50 .. digital .. ' (' .. total .. digital .. ')'
				list[3]:setColor('#F9000E')
			elseif id == ITEM_ATTACK_SPEED and total < 1000 then
				total = 1000 .. digital .. ' (' .. total .. digital .. ')'
				list[3]:setColor('#F9000E')
			else
				total = total .. digital
				list[3]:setColor('#22B14C')
			end
		elseif m_StatsFunctions.page == 2 then
			if total > 75 then
				total = 75 .. digital .. ' (' .. total .. digital .. ')'
				list[3]:setColor('#F9000E')
			else
				total = total .. digital
				list[3]:setColor('#22B14C')
			end
		else
			total = total .. digital
			list[3]:setColor('#22B14C')
		end
		
		list[3]:setText(total)
	end
	
	list[1]:setImageClip(modules.game_lookat.m_LookAtFunctions.getImageClip(icon))
	list[2]:setText(name)
	list[4]:setText((bySpell * multiplier) .. digital)
	list[5]:setText((byEquipment * multiplier) .. digital)
	list[6]:setText((bySkill * multiplier) .. digital)
	list[7]:setText((byBasic * multiplier) .. digital)
	for i = 3, #list do
		list[i]:setWidth(m_StatsList.width)
	end
end

m_StatsFunctions.next = function(self)
	m_StatsFunctions.page = m_StatsFunctions.page + 1
	if m_StatsFunctions.page == 4 then
		self:setEnabled(false)
	elseif m_StatsFunctions.page >= 2 then
		m_StatsList.prevButton:setEnabled(true)
	end
	
	m_StatsFunctions.updatePage()
end

m_StatsFunctions.previous = function(self)
	m_StatsFunctions.page = m_StatsFunctions.page - 1
	if m_StatsFunctions.page == 1 then
		self:setEnabled(false)
	elseif m_StatsFunctions.page <= 3 then
		m_StatsList.nextButton:setEnabled(true)
	end
	
	m_StatsFunctions.updatePage()
end

m_StatsFunctions.updatePage = function()
	m_StatsList.pageLabel:setText(m_StatsFunctions.page .. ' / 4')
	
	local childrens = m_StatsList.list:getChildren()
	for i = 2, #childrens do
		childrens[i]:destroy()
	end
	
	local configId = m_StatsFunctions.config[m_StatsFunctions.page]
	local tmpId = configId.id
	for i = 1, #configId do
		local v = configId[i]
		
		local var = nil
		if tmpId then
			local list = m_StatsFunctions.statisticList[tmpId]
			if list then
				var = list[v.id]
			end
		else
			var = m_StatsFunctions.statisticList[v.id]
		end
		
		if var then
			m_StatsFunctions.onStatsUpdate(tmpId and tmpId or v.id, var.equipment, var.spell, var.skill, tmpId and v.id, var.basic)
		else
			m_StatsFunctions.onStatsUpdate(tmpId and tmpId or v.id, 0, 0, 0, tmpId and v.id, 0)
		end
	end
end
