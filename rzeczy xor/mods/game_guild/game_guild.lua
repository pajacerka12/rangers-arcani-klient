m_GuildFunctions = {}
m_GuildList = {}

m_GuildFunctions.GameServerSendGuild = 86

m_GuildFunctions.GameServerUpdateGuildList = 96
m_GuildFunctions.GameServerUpdateGuildMessages = 117
m_GuildFunctions.GameServerUpdateGuildInviteList = 118
m_GuildFunctions.GameServerUpdateGuildInvites = 119

m_GuildFunctions.OPEN_GUILD_PANEL = 0
m_GuildFunctions.OPEN_GUILD_INVITES = 1

m_GuildFunctions.ALLOW_NONE = 0
m_GuildFunctions.ALLOW_UPDATEMOTD = 1
m_GuildFunctions.ALLOW_INVITEPLAYERS = 2
m_GuildFunctions.ALLOW_REVOKEINVITES = 4
m_GuildFunctions.ALLOW_REVOKEPLAYERS = 8
m_GuildFunctions.ALLOW_MANAGERANKS = 16
m_GuildFunctions.ALLOW_LEADER = 32

m_GuildFunctions.warningSetRank = tr('This player already have this rank.')
m_GuildFunctions.warningRankAmount = tr('You cannot add next rank. Limit of maximum ranks is 8.')
m_GuildFunctions.warningUpdateDescription = tr('You can update guild description only once per minute.')
m_GuildFunctions.editingDescription = tr('You are already editing options for rank name.')

m_GuildFunctions.allowList = {
							[m_GuildFunctions.ALLOW_UPDATEMOTD] = 'allowWriteOnGuildChat',
							[m_GuildFunctions.ALLOW_INVITEPLAYERS] = 'allowInvitePlayers',
							[m_GuildFunctions.ALLOW_REVOKEINVITES] = 'allowRevokeInvites',
							[m_GuildFunctions.ALLOW_REVOKEPLAYERS] = 'allowRevokePlayers',
							[m_GuildFunctions.ALLOW_MANAGERANKS] = 'allowManageRanks'
							}

m_GuildFunctions.open = function(var)
	if var == m_GuildFunctions.OPEN_GUILD_PANEL then
		if m_GuildList.invitesWindow then
			m_GuildList.invitesWindow = nil
			destroy(true)
		end
		
		if m_GuildList.window then
			m_GuildList.list:destroyChildren()
			m_GuildList.invites:destroyChildren()
		else
			m_GuildList.window = g_ui.displayUI('game_guild')
			m_GuildList.list = m_GuildList.window:getChildById('list')
			m_GuildList.title = m_GuildList.window:getChildById('title')
			m_GuildList.messages = m_GuildList.window:getChildById('messsagesList')
			m_GuildList.invites = m_GuildList.window:getChildById('inviteList')
			m_GuildList.textEdit = m_GuildList.window:getChildById('newMessage')
			m_GuildList.newMember = m_GuildList.window:getChildById('newMember')
			m_GuildList.buttonInvite = m_GuildList.window:getChildById('buttonInvite')
			m_GuildList.buttonRank = m_GuildList.window:getChildById('buttonRank')
			m_GuildList.buttonMotd = m_GuildList.window:getChildById('buttonMotd')
			m_GuildList.ranks = {}
		end
	elseif var == m_GuildFunctions.OPEN_GUILD_INVITES then
		m_GuildList.window = g_ui.displayUI('guild_invites')
		m_GuildList.list = m_GuildList.window:getChildById('list')
		m_GuildList.invitesWindow = true
	end
	
	modules.game_inventory.setGuild(true)
end

m_GuildFunctions.send = function(parent, id)
	local text = parent:getText()
	if text == '' then
		return false
	end

	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(id)
	msg:addString(text)
	g_game.getProtocolGame():send(msg)
	
	if id == 3 then
		parent:destroy()
	else
		parent:clearText()
	end
end

m_GuildFunctions.setRank = function(rankId, playerName, name)
	if rankId == name then
		m_GuildFunctions.displayWarning(m_GuildFunctions.warningSetRank)
		return false
	end
	
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(11)
	msg:addString(name)
	msg:addString(playerName)
	g_game.getProtocolGame():send(msg)
end

m_GuildFunctions.addMember = function(msg, playerName, i, online)
	local guid = msg:getU32()
	local name = msg:getString()
	local vocationId = msg:getU8()
	local isPremium = msg:getU8() == 1
	local level = msg:getU32()
	local rankId = msg:getString()
	local online = online or msg:getU8() == 1
	
	local widget = g_ui.createWidget('GuildMember')
	local vocationName, avatarId = getVocationNameById(vocationId)
	
	m_GuildList.list:addChild(widget)
	
	widget.name = name
	widget.rankId = rankId
	widget:setId(guid)
	widget:getChildById('avatarId'):setImageSource('/images/leaf/registery/list/' .. avatarId)
	widget:getChildById('name'):setText(name .. ' (' .. rankId .. ')')
	widget:getChildById('vocation'):setText(vocationName)
	widget:getChildById('level'):setText('Level ' .. level)
	
	if name == playerName then
		m_GuildList.rankId = rankId
	end
	
	if isPremium then
		widget:getChildById('premiumId'):show()
	else
		widget:getChildById('premiumId'):hide()
	end
	
	local premium = widget:getChildById('premium')
	premium:setText(isPremium and 'Lord' or 'Peasant')
	premium:setOn(isPremium)
	
	local status = widget:getChildById('status')
	status:setText(online and 'ONLINE' or 'OFFLINE')
	status:setOn(online)
	
	widget.onMouseRelease = function(self, mousePosition, mouseButton)
									if mouseButton == MouseRightButton and hasbit(m_GuildList.flags, m_GuildFunctions.ALLOW_MANAGERANKS) then
										local menu = g_ui.createWidget('PopupMenu')
										menu:setGameMenu(true)
										
										for name, flags in pairs(m_GuildList.ranks) do
											menu:addOption(tr('Set \'' .. name .. '\' rank for ' .. widget.name .. '.'), function() return m_GuildFunctions.setRank(widget.rankId, widget.name, name) end)
										end
										
										menu:display(mousePosition)
										return true
									end
									
									return false
								end
	
	return widget
end

m_GuildFunctions.hide = function()
	m_GuildList.textEdit:setEnabled(false)
	
	m_GuildList.buttonRank:hide()
	m_GuildList.newMember:hide()
	m_GuildList.buttonInvite:setOn(true)
	
	m_GuildList.messages:setOn(true)
end

m_GuildFunctions.parseGuildList = function(protocol, msg)
	local playerName = g_game.getLocalPlayer():getName()
	if msg:getU8() == 1 then
		m_GuildFunctions.open(m_GuildFunctions.OPEN_GUILD_PANEL)
		m_GuildList.title:setText(tr('GUILD PANEL') .. ' - ' .. msg:getString())
		
		local size = msg:getU16()
		for i = 1, size do
			if msg:getU8() == 1 then
				m_GuildFunctions.addMember(msg, playerName, i, false)
			end
		end
	else
		local widget = m_GuildFunctions.addMember(msg, playerName, m_GuildList.list:getChildCount() + 1, true)
		if widget.name == playerName then
			m_GuildFunctions.updateButtons()
		end
	end
end

m_GuildFunctions.addMessage = function(msg)
	local widget = g_ui.createWidget('GuildMessage')
	m_GuildList.messages:insertChild(1, widget)
	
	local text = widget:getChildById('text')
	text:setText(msg:getString())
	widget:setHeight(math.max(32, text:getHeight()))
	
	widget:getChildById('date'):setText(msg:getString())
end

m_GuildFunctions.update = function(msg)
	local guid = msg:getU32()
	local vocationId = msg:getU8()
	local isPremium = msg:getU8() == 1
	local level = msg:getU32()
	local rankId = msg:getString()
	local online = msg:getU8() == 1
	
	local widget = m_GuildList.list:getChildById(guid)
	if widget then
		local vocationName, avatarId = getVocationNameById(vocationId)
		
		widget.rankId = rankId
		widget:getChildById('avatarId'):setImageSource('/images/leaf/registery/list/' .. avatarId)
		widget:getChildById('name'):setText(widget.name .. ' (' .. rankId .. ')')
		widget:getChildById('vocation'):setText(vocationName)
		widget:getChildById('level'):setText('Level ' .. level)
		
		if widget.name == g_game.getLocalPlayer():getName() and m_GuildList.rankId ~= rankId then
			m_GuildList.rankId = rankId
			m_GuildList.flags = m_GuildList.ranks[rankId]
			m_GuildFunctions.updateButtons()
		end
		
		if isPremium then
			widget:getChildById('premiumId'):show()
		else
			widget:getChildById('premiumId'):hide()
		end
		
		local premium = widget:getChildById('premium')
		premium:setText(isPremium and 'Lord' or 'Peasant')
		premium:setOn(isPremium)
		
		local status = widget:getChildById('status')
		status:setText(online and 'ONLINE' or 'OFFLINE')
		status:setOn(online)
	end
end

m_GuildFunctions.updateButtons = function()
	if hasbit(m_GuildList.flags, m_GuildFunctions.ALLOW_UPDATEMOTD) then
		m_GuildList.textEdit:setEnabled(true)
		m_GuildList.buttonMotd:show()
	else
		m_GuildList.textEdit:setEnabled(false)
		m_GuildList.buttonMotd:hide()
	end
	
	if hasbit(m_GuildList.flags, m_GuildFunctions.ALLOW_INVITEPLAYERS) then
		m_GuildList.buttonInvite:setOn(false)
		m_GuildList.newMember:show()
	else
		m_GuildList.buttonInvite:setOn(true)
		m_GuildList.newMember:hide()
	end
	
	if hasbit(m_GuildList.flags, m_GuildFunctions.ALLOW_MANAGERANKS) then
		m_GuildList.textEdit:setOn(false)
		m_GuildList.buttonRank:show()
	else
		m_GuildList.textEdit:setOn(true)
		m_GuildList.buttonRank:hide()
	end
	
	if hasbit(m_GuildList.flags, m_GuildFunctions.ALLOW_REVOKEINVITES) then
		for _, pid in pairs(m_GuildList.invites:getChildren()) do
			pid:getChildById('remove'):show()
		end
	else
		for _, pid in pairs(m_GuildList.invites:getChildren()) do
			pid:getChildById('remove'):hide()
		end
	end
	
	if hasbit(m_GuildList.flags, m_GuildFunctions.ALLOW_REVOKEPLAYERS) then
		for _, pid in pairs(m_GuildList.list:getChildren()) do
			pid:getChildById('removeButton'):show()
		end
	else
		local playerName = g_game.getLocalPlayer():getName()
		for _, pid in pairs(m_GuildList.list:getChildren()) do
			if pid.name ~= playerName then
				pid:getChildById('removeButton'):hide()
			end
		end
	end
end

m_GuildFunctions.addRank = function(name, flags)
	m_GuildList.ranks[name] = flags
	
	if name == m_GuildList.rankId then
		m_GuildList.flags = flags
		m_GuildFunctions.updateButtons()
	end
	
	if not m_GuildList.rankList then
		return true
	end
	
	local widget = g_ui.createWidget('GuildRank')
	m_GuildList.rankList:addChild(widget)
	
	widget:setText(name)
	widget.flags = flags
	
	manageSingleRank(widget)
end

m_GuildFunctions.parseMotd = function(msg)
	m_GuildList.textEdit:setText(msg:getString())
end

m_GuildFunctions.parseRankList = function(msg)
	if msg:getU8() == 1 then
		if m_GuildList.rankList then
			m_GuildList.ranks = {}
			
			m_GuildList.current = nil
			m_GuildList.rankList:destroyChildren()
		end
		
		local size = msg:getU16()
		for i = 1, size do
			m_GuildFunctions.addRank(msg:getString(), msg:getU32())
		end
	else
		m_GuildFunctions.addRank(msg:getString(), msg:getU32())
	end
end

m_GuildFunctions.displayWarning = function(text)
	if not m_GuildList.warning then
		m_GuildList.warning = g_ui.displayUI('popup_message')
	end
	
	m_GuildList.window:setEnabled(false)
	m_GuildList.warning:getChildById('description'):setText(text)
end

m_GuildFunctions.parseGuildMessages = function(protocol, msg)
	local id = msg:getU16()
	if id == 1 then
		m_GuildFunctions.addMessage(msg)
	elseif id == 2 then
		m_GuildFunctions.displayWarning(msg:getString())
	elseif id == 3 then
		destroy(true)
	elseif id == 4 then
		m_GuildFunctions.destroyInvite(msg:getU32())
	elseif id == 5 then
		m_GuildFunctions.update(msg)
	elseif id == 6 then
		m_GuildFunctions.parseRankList(msg)
	elseif id == 7 then
		m_GuildFunctions.parseMotd(msg)
	else
		m_GuildList.messages:destroyChildren()
		local size = msg:getU16()
		for i = 1, size do
			m_GuildFunctions.addMessage(msg)
		end
	end
end

m_GuildFunctions.destroyInvite = function(id)
	local widget = m_GuildList.list:getChildById(id)
	if widget then
		widget:destroy()
	end
end

m_GuildFunctions.addInvite = function(name)
	local widget = g_ui.createWidget('GuildInvite')
	m_GuildList.invites:insertChild(1, widget)
	
	widget:setText(name)
end

m_GuildFunctions.parseGuildInviteList = function(protocol, msg)
	if msg:getU8() == 1 then
		m_GuildList.invites:destroyChildren()
		
		local size = msg:getU16()
		for i = 1, size do
			m_GuildFunctions.addInvite(msg:getString())
		end
	else
		m_GuildFunctions.addInvite(msg:getString())
	end
end

m_GuildFunctions.addInvites = function(msg)
	local widget = g_ui.createWidget('GuildPrivateInvite')
	m_GuildList.list:insertChild(1, widget)
	
	widget:setId(msg:getU32())
	widget:setText(msg:getString())
end

m_GuildFunctions.parseGuildInvites = function(protocol, msg)
	m_GuildFunctions.open(m_GuildFunctions.OPEN_GUILD_INVITES)
	
	if msg:getU8() == 1 then
		m_GuildList.list:destroyChildren()
		
		local size = msg:getU16()
		for i = 1, size do
			m_GuildFunctions.addInvites(msg)
		end
	else
		m_GuildFunctions.addInvites(msg)
	end
end

m_GuildFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_GuildFunctions.GameServerUpdateGuildList, m_GuildFunctions.parseGuildList)
	ProtocolGame.registerOpcode(m_GuildFunctions.GameServerUpdateGuildMessages, m_GuildFunctions.parseGuildMessages)
	ProtocolGame.registerOpcode(m_GuildFunctions.GameServerUpdateGuildInviteList, m_GuildFunctions.parseGuildInviteList)
	ProtocolGame.registerOpcode(m_GuildFunctions.GameServerUpdateGuildInvites, m_GuildFunctions.parseGuildInvites)
end

m_GuildFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_GuildFunctions.GameServerUpdateGuildList, m_GuildFunctions.parseGuildList)
	ProtocolGame.unregisterOpcode(m_GuildFunctions.GameServerUpdateGuildMessages, m_GuildFunctions.parseGuildMessages)
	ProtocolGame.unregisterOpcode(m_GuildFunctions.GameServerUpdateGuildInviteList, m_GuildFunctions.parseGuildInviteList)
	ProtocolGame.unregisterOpcode(m_GuildFunctions.GameServerUpdateGuildInvites, m_GuildFunctions.parseGuildInvites)
end

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	if g_game.isOnline() then
		m_GuildFunctions.registerProtocol()
	end
end

function onUnload()
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	destroy()
	m_GuildFunctions.unregisterProtocol()
end

function onGameStart()
	m_GuildFunctions.registerProtocol()
end

function onGameEnd()
	destroy()
	m_GuildFunctions.unregisterProtocol()
end

function accept()
	if m_GuildList.warning then
		m_GuildList.warning:destroy()
		m_GuildList.warning = nil
	end
	
	if not m_GuildList.manage then
		m_GuildList.window:setEnabled(true)
		m_GuildList.window:focus()
	end
end

function destroy(ignore)
	if m_GuildList.window then
		m_GuildList.window:destroy()
		
		if not ignore then
			local msg = OutputMessage.create()
			msg:addU8(m_GuildFunctions.GameServerSendGuild)
			msg:addU8(5)
			g_game.getProtocolGame():send(msg)
		end
	end
	
	if m_GuildList.warning then
		m_GuildList.warning:destroy()
	end
	
	if m_GuildList.manage then
		m_GuildList.manage:destroy()
	end
	
	m_GuildList = {}
	
	if modules.game_inventory then
		modules.game_inventory.setGuild(false)
	end
end

function send()
	if m_GuildList.newRank and m_GuildList.newRank:isFocused() then
		addNewRank()
	elseif m_GuildList.newMember and m_GuildList.newMember:isFocused() then
		m_GuildFunctions.send(m_GuildList.newMember, 2)
	end
end

function addNewRank()
	if m_GuildList.rankList:getChildCount() >= 8 then
		m_GuildFunctions.displayWarning(m_GuildFunctions.warningRankAmount)
		return false
	end
	
	m_GuildFunctions.send(m_GuildList.newRank, 8)
end

function removeInvite(self, id)
	m_GuildFunctions.send(self, id)
end

function removeInvites(self, id)
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(id)
	msg:addU32(self:getId())
	g_game.getProtocolGame():send(msg)
	
	self:destroy()
end

function join(self, id)
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(id)
	msg:addU32(self:getId())
	g_game.getProtocolGame():send(msg)
end

function revoke(self, id)
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(id)
	msg:addString(self.name)
	g_game.getProtocolGame():send(msg)
end

function open()
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(0)
	g_game.getProtocolGame():send(msg)
end

function destroyRank()
	if m_GuildList.manage then
		m_GuildList.manage:destroy()
		m_GuildList.manage = nil
		
		m_GuildList.rankList = nil
		m_GuildList.newRank = nil
		m_GuildList.current = nil
	end
	
	m_GuildList.window:setEnabled(true)
end

function hasbit(x, p)
	return x % (p + p) >= p       
end

function manageSingleRank(self)
	local widget = m_GuildList.manage:getChildById('name')
	
	if m_GuildList.current then
		m_GuildList.current:getChildById('blink'):hide()
	end
	
	m_GuildList.current = self
	self:getChildById('blink'):show()
	m_GuildList.manage.name = self:getText()
	widget:setText(m_GuildFunctions.editingDescription .. '\n' .. m_GuildList.manage.name)
	
	local list = m_GuildList.manage:getChildById('list')
	for id, name in pairs(m_GuildFunctions.allowList) do
		list:getChildById(name):setChecked(hasbit(self.flags, id))
	end
end

function manageRank()
	m_GuildList.window:setEnabled(false)
	
	if m_GuildList.manage then
		return true
	end
	
	m_GuildList.manage = g_ui.displayUI('guild_manage')
	m_GuildList.rankList = m_GuildList.manage:getChildById('rankList')
	m_GuildList.newRank = m_GuildList.manage:getChildById('newRank')
	
	for name, flags in pairs(m_GuildList.ranks) do
		m_GuildFunctions.addRank(name, flags)
	end
end

function selectAll()
	if not m_GuildList.manage then
		return false
	end
	
	local list = m_GuildList.manage:getChildById('list')
	for _, pid in pairs(list:getChildren()) do
		pid:setChecked(true)
	end
end

function removeRank(self)
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(10)
	msg:addString(self:getText())
	g_game.getProtocolGame():send(msg)
end

function acceptRank(self)
	local list = self:getChildById('list')
	
	local flags = 0
	for id, name in pairs(m_GuildFunctions.allowList) do
		if list:getChildById(name):isChecked() then
			flags = flags + id
		end
	end
	
	if self.flags == flags then
		return true
	end
	
	m_GuildList.current = nil
	
	local msg = OutputMessage.create()
	msg:addU8(m_GuildFunctions.GameServerSendGuild)
	msg:addU8(9)
	msg:addU32(flags)
	msg:addString(self.name)
	g_game.getProtocolGame():send(msg)
end

function updateGuildDescription()
	if m_GuildFunctions.delay and m_GuildFunctions.delay > os.time() then
		m_GuildFunctions.displayWarning(m_GuildFunctions.warningUpdateDescription)
		return false
	end
	
	m_GuildFunctions.delay = os.time() + 60
	m_GuildFunctions.send(m_GuildList.textEdit, 1)
end
