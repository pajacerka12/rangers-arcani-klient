local id = 0
local MAP_SHADERS = {
					  {name = 'Foggy', frag = 'shaders/truefog.frag'},
					  {name = 'Default', frag = 'shaders/default.frag'},
					  {name = 'Bloom', frag = 'shaders/bloom.frag'},
					  {name = 'Sepia', frag ='shaders/sepia.frag'},
					  {name = 'Grayscale', frag ='shaders/grayscale.frag'},
					  {name = 'Pulse', frag = 'shaders/pulse.frag'},
					  {name = 'Old Tv', frag = 'shaders/oldtv.frag'},
					  {name = 'Fog', frag = 'shaders/fog.frag', tex1 = 'images/clouds.png'},
					  {name = 'Party', frag = 'shaders/party.frag'},
					  {name = 'Radial Blur', frag ='shaders/radialblur.frag'},
					  {name = 'Zomg', frag ='shaders/zomg.frag'},
					  {name = 'Heat', frag ='shaders/heat.frag'},
					  {name = 'Noise', frag ='shaders/noise.frag'},
					  
					  {name = 'Amplitude', frag ='shaders/amplitude.frag'},
					  {name = 'SparklingBlade', frag ='shaders/sparklingBlade.frag'},
					  {name = 'ShallowShader', frag ='shaders/shallowShader.frag'},
					  {name = 'LegendaryShader', frag ='shaders/LegendaryShader.frag'},
					  {name = 'UniqueShader', frag ='shaders/UniqueShader.frag'},
					  -- {name = 'OnTargetShader', frag ='shaders/OnTargetShader.frag'},
					}
 
function init()
	if not g_graphics.canUseShaders() then
		return false
	end
	
	for _i, opts in pairs(MAP_SHADERS) do
		local shader = g_shaders.createFragmentShader(opts.name, opts.frag)
		if opts.tex1 then
			shader:addMultiTexture(opts.tex1)
		end
		
		if opts.tex2 then
			shader:addMultiTexture(opts.tex2)
		end
	end
	
	-- connect(g_game, {
						-- onSetShader = onSetShader,
						-- onGameStart = onGameStart,
						-- onGameEnd = onGameEnd
						-- })
 
	-- local map = modules.game_interface.getMapPanel()
	-- map:setMapShader(g_shaders.getShader('Default'))
	-- if g_game.isOnline() then
		-- onSetShader(18)
	-- end
end
 
function terminate()
	-- disconnect(g_game, {
						-- onSetShader = onSetShader,
						-- onGameStart = onGameStart,
						-- onGameEnd = onGameEnd
						-- })
	
	-- onSetShader(2)
end

function onGameEnd()
	-- onSetShader(1)
end

function onGameStart()
	-- onSetShader(18)
end

function onSetShader(newId)
	-- if id == newId then
		-- return true
	-- end
	
	-- id = newId
	-- local name = MAP_SHADERS[id].name
	-- local map = modules.game_interface.getMapPanel()
	
	-- map:setMapShader(g_shaders.getShader('Test'), 0, 0)
end
