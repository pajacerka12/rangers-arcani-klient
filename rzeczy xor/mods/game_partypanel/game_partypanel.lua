m_PartyPanelFunctions = {}
m_PartyPanelList = {}

m_PartyPanelFunctions.icons = {
	{path = '/images/leaf/states/earth'},
	{path = '/images/leaf/states/fire'},
	{path = '/images/leaf/states/energy'},
	{path = '/images/leaf/states/drunk'},
	{path = '/images/leaf/states/barrer'},
	{path = '/images/leaf/states/paralyze'},
	{path = '/images/leaf/states/haste'},
	{path = '/images/leaf/states/fight'},
	{path = '/images/leaf/states/drown'},
	{path = '/images/leaf/states/ice'},
	{path = '/images/leaf/states/holy'},
	{path = '/images/leaf/states/death'},
	{path = '/images/leaf/states/protection'},
	{path = '/images/leaf/states/protection'},
	{path = '/images/leaf/states/protection'},
	{path = '/images/leaf/states/bleed'},
	{path = '/images/leaf/states/skull'},
	{path = '/images/leaf/states/potion'}
}

function onLoad()
	g_ui.importStyle('game_partypanel')
	connect(Creature, {
		onHealthPercentChange = onHealthPercentChange,
		onManaPercentChange = onManaPercentChange,
		onCreatureStatesChange = onCreatureStatesChange
	})
	
	connect(g_game, {
		onGameEnd = onGameEnd,
		onUpdatePartyLevel = onUpdatePartyLevel,
		onAddPartyMember = onAddPartyMember,
		onRemovePartyMember = onRemovePartyMember
	})
end

function onUnload()
	disconnect(Creature, {
		onHealthPercentChange = onHealthPercentChange,
		onManaPercentChange = onManaPercentChange,
		onCreatureStatesChange = onCreatureStatesChange
	})
	
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onUpdatePartyLevel = onUpdatePartyLevel,
		onAddPartyMember = onAddPartyMember,
		onRemovePartyMember = onRemovePartyMember
	})
	
	m_PartyPanelFunctions.destroy()
end

function onGameEnd()
	m_PartyPanelFunctions.destroy()
end

function onAddPartyMember(playerId, name, health, mana, vocationId, level, leader, newMember, conditions)
	m_PartyPanelFunctions.addMember(playerId, name, health, mana, vocationId, level, leader, newMember, conditions)
end

function onRemovePartyMember(name)
	m_PartyPanelFunctions.removeMember(name)
end

function onHealthPercentChange(creature, health)
	local partyId = m_PartyPanelList.window and m_PartyPanelList.list[creature:getName()]
	if partyId then
		m_PartyPanelFunctions.updateHealth(partyId, health)
	end
end

function onManaPercentChange(creature, mana)
	local partyId = m_PartyPanelList.window and m_PartyPanelList.list[creature:getName()]
	if partyId then
		m_PartyPanelFunctions.updateMana(partyId, mana)
	end
end

function onUpdatePartyLevel(name, level)
	-- local partyId = m_PartyPanelList.window and m_PartyPanelList.list[name]
	-- if partyId then
		-- partyId.level:setText(level)
	-- end
end

function getSelectedMember()
	return m_PartyPanelList.currentMember and m_PartyPanelList.currentMember.id
end

m_PartyPanelFunctions.removeMember = function(name)
	local partyId = m_PartyPanelList.window and m_PartyPanelList.list[name]
	if partyId then
		if m_PartyPanelList.currentMember == partyId.widget then
			m_PartyPanelList.currentMember = nil
		end
		
		partyId.widget:destroy()
		m_PartyPanelList.list[name] = nil
		
		m_PartyPanelFunctions.updateHeight()
	end
end

function onCreatureStatesChange(localPlayer, name, icon, id, description, removeIcon, status, endTime)
	if status == 2 then
		return false
	end
	
	local partyId = m_PartyPanelList.window and m_PartyPanelList.list[name]
	if partyId then
		m_PartyPanelFunctions.addIcon(icon, id, description, endTime == 0)
		
		if not partyId.widget.conditions then
			partyId.widget.conditions = {}
		else
			for i = 1, #partyId.widget.conditions do
				local tmpIcon, tmpId, tmpDescription = unpack(partyId.widget.conditions[i])
				if id == tmpId and icon == tmpIcon then
					if endTime == 0 then
						table.remove(partyId.widget.conditions, i)
					end
					
					return true
				end
			end
		end
		
		if endTime ~= 0 then
			table.insert(partyId.widget.conditions, {icon, id, description})
		end
	end
end

m_PartyPanelFunctions.updateSize = function()
	local count = m_PartyPanelList.hover:getChildCount()
	m_PartyPanelList.hover:setHeight((math.ceil(count / 8) * 18) + 12)
	if count < 8 then
		m_PartyPanelList.hover:setWidth((count * 18) + 12)
	else
		m_PartyPanelList.hover:setWidth(192)
	end
end

m_PartyPanelFunctions.addIcon = function(icon, id, description, removeIcon)
	if not m_PartyPanelList.hover or description == '' then
		return true
	end
	
	local widget = m_PartyPanelList.hover:getChildById(id)
	if widget then
		if removeIcon then
			widget:destroy()
			m_PartyPanelFunctions.updateSize()
		end
		
		return true
	end
	
	local widget = g_ui.createWidget('PartyConditionIcon')
	widget:setParent(m_PartyPanelList.hover)
	widget:setId(id)
	widget.description = description
	widget.onHoverChange = m_PartyPanelFunctions.onHoverChange
	
	if icon < 0xFF then
		modules.game_spellbar.m_SpellBarFunctions.setIconImageType(widget, icon)
	else
		widget:setImageSource(m_PartyPanelFunctions.icons[icon - 0xFF].path)
	end
end

m_PartyPanelFunctions.onBuffHoverChange = function(self, hovered)
	if not hovered then
		return true
	end
	
	local parent = self:getParent()
	if not parent.conditions or #parent.conditions == 0 then
		return true
	end
	
	if not m_PartyPanelList.hover then
		m_PartyPanelList.hover = g_ui.createWidget('PartyPlayerInfoLabel', modules.game_interface.getRootPanel())
		m_PartyPanelList.hover.onHoverChange = function(self, hovered)
			if not hovered then
				if m_PartyPanelList.hover then
					m_PartyPanelList.hover:destroy()
					m_PartyPanelList.hover = nil
				end
			end
		end
	else
		m_PartyPanelList.hover:destroyChildren()
	end
	
	for i = 1, #parent.conditions do
		local icon, id, description = unpack(parent.conditions[i])
		m_PartyPanelFunctions.addIcon(icon, id, description, false)
	end
	
	m_PartyPanelFunctions.updateSize()
	
	local pos = self:getPosition()
	if pos.x + m_PartyPanelList.hover:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_PartyPanelList.hover:getWidth()
	end
	
	if pos.y + m_PartyPanelList.hover:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_PartyPanelList.hover:getHeight()
	end
	
	m_PartyPanelList.hover:setPosition(pos)
end

m_PartyPanelFunctions.addMember = function(playerId, name, health, mana, vocationId, level, leader, newMember, conditions)
	if not m_PartyPanelList.window then
		m_PartyPanelList.window = g_ui.createWidget('GameInfoPartyWindow', modules.game_interface.getRootPanel())
		m_PartyPanelList.window:setup()
		
		m_PartyPanelList.list = {}
	end
	
	local partyId = m_PartyPanelList.list[name]
	if not partyId and newMember then
		partyId = {}
		partyId.widget = g_ui.createWidget('PartyPlayerInfoWindow', m_PartyPanelList.window)
		partyId.widget.id = playerId
		partyId.widget.conditions = conditions
		partyId.name = partyId.widget:getChildById('name')
		partyId.buffList = partyId.widget:getChildById('buffList')
		
		partyId.health = partyId.widget:getChildById('health')
		partyId.healthBar = partyId.health:getChildById('value')
		partyId.healthLabel = partyId.health:getChildById('label')
		partyId.healthSize = partyId.health:getSize()
		
		partyId.mana = partyId.widget:getChildById('mana')
		partyId.manaBar = partyId.mana:getChildById('value')
		partyId.manaLabel = partyId.mana:getChildById('label')
		partyId.manaSize = partyId.mana:getSize()
		
		if name == g_game.getLocalPlayer():getName() then
			partyId.healthLabel:setOn(true)
		end
		
		local clip
		if vocationId == 1 then
			clip = '57 57 19 19'
		elseif vocationId == 2 then
			clip = '19 76 19 19'
		elseif vocationId == 3 then
			clip = '113 57 19 19'
		elseif vocationId == 4 then
			clip = '38 19 19 19'
		elseif vocationId == 5 then
			clip = '0 19 19 19'
		elseif vocationId == 6 then
			clip = '76 38 19 19'
		elseif vocationId == 7 then
			clip = '19 0 19 19'
		elseif vocationId == 8 then
			clip = '94 0 19 19'
		elseif vocationId == 9 then
			clip = '57 0 19 19'
		end
		
		partyId.name:setText(name)
		partyId.name:setIconClip(clip)
		
		m_PartyPanelFunctions.updateHealth(partyId, math.min(100, health))
		m_PartyPanelFunctions.updateMana(partyId, math.min(100, mana))
		
		partyId.name.onMouseRelease = function(self, mousePosition, mouseButton)
			if mouseButton == MouseLeftButton then
				m_PartyPanelFunctions.selectMember(self:getParent())
				return true
			end
			
			return false
		end
		
		partyId.widget.onMouseRelease = function(self, mousePosition, mouseButton)
			if mouseButton == MouseRightButton then
				local menu = g_ui.createWidget('PopupMenu')
				menu:setGameMenu(true)
				menu:addOption(tr('Remove from Party panel'), function() g_game.partyPanelRemove(partyId.widget.id) end)
				menu:display(mousePosition)
				return true
			end
			
			return false
		end
		
		partyId.buffList.onHoverChange = m_PartyPanelFunctions.onBuffHoverChange
	end
	
	if partyId then
		partyId.leader = leader
		partyId.widget:getChildById('leader'):setVisible(leader)
		m_PartyPanelList.list[name] = partyId
		
		if leader then
			partyId.widget:setParent(nil)
			m_PartyPanelList.window:insertChild(1, partyId.widget)
		end
	end
	
	m_PartyPanelFunctions.updateHeight()
end

m_PartyPanelFunctions.updateHealth = function(partyId, value)
	local width = math.ceil(partyId.healthSize.width * math.min(1, value / 100))
	partyId.healthBar:setWidth(width)
	
	local width = math.ceil(162 * math.min(1, value / 100))
	partyId.healthBar:setImageClip('2221 1880 ' .. width .. ' 18')
	partyId.healthLabel:setText(value .. '%')
end

m_PartyPanelFunctions.updateMana = function(partyId, value)
	local width = math.ceil(partyId.manaSize.width * math.min(1, value / 100))
	partyId.manaBar:setWidth(width)
	
	local width = math.ceil(162 * math.min(1, value / 100))
	partyId.manaBar:setImageClip('2221 1825 ' .. width .. ' 18')
	partyId.manaLabel:setText(value .. '%')
end

m_PartyPanelFunctions.selectMember = function(self)
	if m_PartyPanelList.currentMember then
		m_PartyPanelList.currentMember:getChildById('check'):hide()
		
		if m_PartyPanelList.currentMember == self then
			m_PartyPanelList.currentMember = nil
			return true
		end
	end
	
	m_PartyPanelList.currentMember = self
	self:getChildById('check'):show()
end

m_PartyPanelFunctions.createLabel = function(parent, iconId, description)
	local widget = g_ui.createWidget('SpellAttributeLabel', parent)
	local icon = widget:getChildById('icon')
	
	modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, iconId)
	widget:setText(description)
	widget:setParent(parent)
	return widget
end

m_PartyPanelFunctions.onHoverChange = function(self, hovered)
	if not hovered then
		if m_PartyPanelList.description then
			m_PartyPanelList.description:destroy()
			m_PartyPanelList.description = nil
		end
		
		return true
	end
	
	local rootWidget = modules.game_interface.getRootPanel()
	if not m_PartyPanelList.description then
		m_PartyPanelList.description = g_ui.createWidget('SpellInfoHoverPanel', rootWidget)
	end
	
	local list = m_PartyPanelList.description:getChildById('list')
	if list then
		local height = 16
		
		local uid = m_PartyPanelFunctions.createLabel(list, 17, self.description)
		height = height + uid:getHeight()
		
		m_PartyPanelList.description.id = self:getId()
		m_PartyPanelList.description:setHeight(height)
	end
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 8
	pos.y = pos.y + self:getHeight() - 8
	
	if pos.x + m_PartyPanelList.description:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_PartyPanelList.description:getWidth()
	end
	
	if pos.y + m_PartyPanelList.description:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_PartyPanelList.description:getHeight()
	end
	
	m_PartyPanelList.description:setPosition(pos)
end

m_PartyPanelFunctions.updateHeight = function()
	if m_PartyPanelList.window then
		local i = 0
		for _, pid in pairs(m_PartyPanelList.list) do
			i = i + 1
		end
		
		if i > 0 then
			m_PartyPanelList.window:setHeight(i * 58)
		else
			m_PartyPanelFunctions.destroy()
		end
	end
end

m_PartyPanelFunctions.destroy = function()
	if m_PartyPanelList.window then
		m_PartyPanelList.window:destroyChildren()
		
		if m_PartyPanelList.description then
			m_PartyPanelList.description:destroy()
		end
		
		if m_PartyPanelList.hover then
			m_PartyPanelList.hover:destroy()
		end
		
		m_PartyPanelList.window:destroy()
		m_PartyPanelList = {}
	end
end
