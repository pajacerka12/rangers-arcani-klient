m_StoreFunctions = {}
m_StoreList = {}

m_StoreFunctions.GameServerSendStore = 87

m_StoreFunctions.red = '#B31B1B'
m_StoreFunctions.disabled = '#BBBBBB88'
m_StoreFunctions.digital = {string.char(128), '$'}
m_StoreFunctions.config = {
	[1] = {
		{cost = 1, points = 50, clip = '0 0 60 60'},
		{cost = 4, points = 250, additional = 25, clip = '60 0 60 60'},
		{cost = 8, points = 500, additional = 75, clip = '120 0 60 60'},
		{cost = 16, points = 1000, additional = 200, clip = '180 0 60 60'},
		{cost = 24, points = 1500, additional = 375, clip = '0 60 60 60'},
		{cost = 40, points = 2500, additional = 750, clip = '60 60 60 60'},
		{cost = 80, points = 5000, additional = 1750, clip = '120 60 60 60'},
		{cost = 160, points = 10000, additional = 4000, clip = '180 60 60 60'}
	},
	[2] = {
		{cost = 1, points = 50, clip = '0 0 60 60'},
		{cost = 4, points = 250, additional = 25, clip = '60 0 60 60'},
		{cost = 8, points = 500, additional = 75, clip = '120 0 60 60'},
		{cost = 16, points = 1000, additional = 200, clip = '180 0 60 60'},
		{cost = 24, points = 1500, additional = 375, clip = '0 60 60 60'},
		{cost = 40, points = 2500, additional = 750, clip = '60 60 60 60'},
		{cost = 80, points = 5000, additional = 1750, clip = '120 60 60 60'},
		{cost = 160, points = 10000, additional = 4000, clip = '180 60 60 60'}
	}
}

m_StoreFunctions.offerClip = {
	[1] = '3051 525 47 47',
	[2] = '3128 527 47 47',
	[3] = '3202 529 47 47'
}

m_StoreFunctions.features = {
	'� You will be able to buy house,\n',
	'� Make guilds,\n',
	'� Post 10 market offers at once (free account 3),\n',
	'� Deposit 4000 items (free account 1000),\n',
	'� Pick 10 items for auto loot (free account 4),\n',
	'� Choose new spell effect for some of spells,\n',
	'� Dress in new premium outfit,\n',
	'� Add 35 VIPs (free account 20),\n',
	'� Rent extra slot in forge for crafting,\n',
	'� Use horse and boat transport for free.'
}

m_StoreFunctions.createPopup = function(name)
	m_StoreList.window:setEnabled(false)
	m_StoreFunctions.destroyPopup()
	
	m_StoreList.popup = g_ui.displayUI(name)
	m_StoreList.popup:focus()
	m_StoreList.popup:raise()
end

m_StoreFunctions.premiumCancel = function()
	if m_StoreList.window then
		m_StoreList.window:setEnabled(true)
		m_StoreFunctions.destroyPopup()
	end
end

m_StoreFunctions.premiumAccept = function()
	local id = tonumber(m_StoreList.premiumSelected:getId())
	local player = g_game:getLocalPlayer()
	
	local text = WEBSITE_GETCOINS[1]
	local tmpList
	if modules.client_entergame.EnterGame.isTestServer() then
		text = text .. CURRENCY_BR .. WEBSITE_GETCOINS[2]
		text = text .. IP_BR_ID
		tmpList = m_StoreFunctions.config[2]
	else
		text = text .. CURRENCY_EU .. WEBSITE_GETCOINS[2]
		text = text .. IP_EU_ID
		tmpList = m_StoreFunctions.config[1]
	end
	
	text = text .. WEBSITE_GETCOINS[3]
	g_game.goToWebSite(text:format(tmpList[id].cost, player:getAccountId()))
end

m_StoreFunctions.destroyPopup = function()
	if m_StoreList.popup then
		m_StoreList.popup:destroy()
		m_StoreList.popup = nil
		
		m_StoreList.premiumSelected = nil
		m_StoreList.window:setEnabled(true)
		m_StoreList.window:focus()
		m_StoreList.window:raise()
		
		if m_StoreList.event then
			m_StoreList.event:cancel()
			m_StoreList.event = nil
		end
	end
end

m_StoreFunctions.buy = function()
	if not m_StoreList.selected then
		return true
	end
	
	local var = m_StoreList.selected.var
	local id = tonumber(m_StoreList.selected:getId())
	var.description = tr(storeIndex[m_StoreList.selected.id].title, var.name)
	m_StoreFunctions.onBuy(id, var)
end

m_StoreFunctions.onBuy = function(id, offer)
	m_StoreFunctions.createPopup('popup_store')
	
	local tmpWidget = m_StoreList.popup:getChildById('productLabelImage')
	tmpWidget:setVisible(offer.image)
	if offer.image then
		tmpWidget:setImageSource(offer.image)
	end
	
	local tmpWidget = m_StoreList.popup:getChildById('outfit')
	tmpWidget:setVisible(offer.outfitId)
	if offer.outfitId then
		m_StoreFunctions.playerOutfit.type = offer.outfitId
		m_StoreFunctions.playerOutfit.addons = m_StoreFunctions.getAddons()
		tmpWidget:setOutfit(m_StoreFunctions.playerOutfit)
	end
	
	local productItemId = m_StoreList.popup:getChildById('productItemId')
	productItemId:setVisible(offer.itemId)
	if offer.itemId then
		productItemId:setItemId(offer.itemId)
		
		if offer.amount then
			productItemId:setItemCount(offer.amount)
		end
	end
	
	m_StoreList.popup.id = id
	m_StoreList.popup.itemId = tonumber(m_StoreList.categorySelected:getId())
	m_StoreList.popup.price = offer.price
	m_StoreList.popup:getChildById('description'):setText(offer.description)
end

m_StoreFunctions.selectPremium = function(self)
	if self == m_StoreList.premiumSelected then
		return true
	end
	
	if m_StoreList.premiumSelected then
		m_StoreList.premiumSelected:getChildById('blink'):hide()
	end
	
	self:getChildById('blink'):show()
	m_StoreList.premiumSelected = self
	m_StoreList.popup:getChildById('buy'):setEnabled(true)
end

m_StoreFunctions.getCoins = function()
	m_StoreFunctions.createPopup('buypremium')
	
	local list = m_StoreList.popup:getChildById('list')
	local tmpList
	if modules.client_entergame.EnterGame.isTestServer() then
		tmpList = m_StoreFunctions.config[2]
	else
		tmpList = m_StoreFunctions.config[1]
	end
	
	for i = 1, #tmpList do
		local v = tmpList[i]
		local widget = g_ui.createWidget('PremiumButton', list)
		widget:setId(i)
		
		local productLabelTokenPrice = widget:getChildById('productLabelTokenPrice')
		if v.additional then
			productLabelTokenPrice:setText(v.points .. ' + ' .. v.additional)
		else
			productLabelTokenPrice:setText(v.points)
		end
		
		local productLabelImage = widget:getChildById('productLabelImage')
		productLabelImage:setVisible(v.clip)
		if v.clip then
			productLabelImage:setImageClip(v.clip)
		end
		
		if modules.client_entergame.EnterGame.isTestServer() then
			widget:getChildById('productLabelTitle'):setText(v.cost .. m_StoreFunctions.digital[2])
		else
			widget:getChildById('productLabelTitle'):setText(v.cost .. m_StoreFunctions.digital[1])
		end
	end
end

m_StoreFunctions.setAddon = function(self, addon)
	if self:isChecked() then
		if addon == 1 then
			m_StoreList.addons = m_StoreList.addons == 2 and 3 or 1
		elseif addon == 2 then
			m_StoreList.addons = m_StoreList.addons == 1 and 3 or 2
		end
	else
		if addon == 1 then
			m_StoreList.addons = m_StoreList.addons == 3 and 2 or 0
		elseif addon == 2 then
			m_StoreList.addons = m_StoreList.addons == 3 and 1 or 0
		end
	end
	
	for _, pid in pairs(m_StoreList.productList:getChildren()) do
		local tmpWidget = pid:getChildById('outfit')
		if tmpWidget.id then
			m_StoreFunctions.playerOutfit.type = tmpWidget.id
			m_StoreFunctions.playerOutfit.addons = m_StoreList.addons
			tmpWidget:setOutfit(m_StoreFunctions.playerOutfit)
		end
	end
	
	if m_StoreList.outfit.id then
		m_StoreFunctions.playerOutfit.type = m_StoreList.outfit.id
		m_StoreFunctions.playerOutfit.addons = m_StoreList.addons
		m_StoreList.outfit:setOutfit(m_StoreFunctions.playerOutfit)
	end
end

m_StoreFunctions.getAddons = function()
	local addons = 0
	if m_StoreList.addon1:isChecked() then
		addons = 1
	end
	
	if m_StoreList.addon2:isChecked() then
		addons = addons + 2
	end
	
	return addons
end

m_StoreFunctions.onChooseCategory = function(id)
	m_StoreFunctions.destroyPopup()
	m_StoreList.selected = nil
	
	if id == 4 then
		m_StoreList.addon1:show()
		m_StoreList.addon2:show()
	else
		m_StoreList.addon1:hide()
		m_StoreList.addon2:hide()
	end
	
	if m_StoreList.categorySelected then
		m_StoreList.categorySelected:getChildById('blink'):hide()
	end
	
	m_StoreList.categorySelected = m_StoreList.index:getChildById(id)
	m_StoreList.categorySelected:getChildById('blink'):show()
	m_StoreFunctions.playerOutfit = g_game.getLocalPlayer():getOutfit()
	
	local var = storeIndex[id]
	m_StoreList.productList:destroyChildren()
	
	for j = 1, #var.list do
		local v = var.list[j]
		local widget = g_ui.createWidget('ProductStoreWidget', m_StoreList.productList)
		widget:setId(j)
		widget.var = v
		widget.id = id
		
		if id == 1 then
			widget.onHoverChange = onFeature
		elseif id == 4 then
			widget.onHoverChange = onHoverChange
		end
		
		local tmpWidget = widget:getChildById('image')
		tmpWidget:setVisible(v.image)
		if v.image then
			tmpWidget:setImageSource(v.image)
		end
		
		local tmpWidget = widget:getChildById('outfit')
		tmpWidget:setVisible(v.outfitId)
		if v.outfitId then
			tmpWidget.id = v.outfitId
			tmpWidget.offerId = j
			
			m_StoreFunctions.playerOutfit.type = v.outfitId
			m_StoreFunctions.playerOutfit.addons = m_StoreFunctions.getAddons()
			tmpWidget:setOutfit(m_StoreFunctions.playerOutfit)
		end
		
		local tmpWidget = widget:getChildById('item')
		tmpWidget:setVisible(v.itemId)
		if v.itemId then
			tmpWidget:setItemId(v.itemId)
			
			if v.amount then
				tmpWidget:setItemCount(v.amount)
			end
		end
		
		if v.offer then
			local tmpWidget = widget:getChildById('offer')
			tmpWidget:show()
			tmpWidget:setImageClip(m_StoreFunctions.offerClip[v.offer])
		end
		
		widget:getChildById('price'):setText(g_game.getMilharNumber(v.price))
	end
end

m_StoreFunctions.select = function(self)
	if self == m_StoreList.selected then
		return true
	end
	
	m_StoreList.image:setVisible(self.var.image)
	if self.var.image then
		m_StoreList.image:setImageSource(self.var.image)
	end
	
	m_StoreList.outfit:setVisible(self.var.outfitId)
	if self.var.outfitId then
		m_StoreList.outfit.id = self.var.outfitId
		m_StoreList.outfit.offerId = tonumber(self:getId())
		
		m_StoreFunctions.playerOutfit.type = self.var.outfitId
		m_StoreFunctions.playerOutfit.addons = m_StoreFunctions.getAddons()
		m_StoreList.outfit:setOutfit(m_StoreFunctions.playerOutfit)
	end
	
	m_StoreList.currentName:setText(self.var.name)
	m_StoreList.item:setVisible(self.var.itemId)
	if self.var.itemId then
		m_StoreList.item:setItemId(self.var.itemId)
		
		if self.var.amount then
			m_StoreList.item:setItemCount(self.var.amount)
		else
			m_StoreList.item:setItemCount(1)
		end
		
		local language = g_settings.get('locale', 'en')
		if language == 'en' then
			language = LANGUAGE_ENGLISH
		else
			language = LANGUAGE_POLISH
		end
		
		m_StoreList.currentName:setText(modules.game_lookat.m_LookAtFunctions.getItemNameByClientId(self.var.itemId, language))
	end
	
	m_StoreList.button:setEnabled(true)
	m_StoreList.selected = self
	m_StoreList.price:setText(g_game.getMilharNumber(self.var.price))
end

m_StoreFunctions.sendToDepot = function(checked)
	if m_StoreList.sendToCharacter then
		m_StoreList.sendToCharacter:setChecked(not checked)
	end
end

m_StoreFunctions.sendToCharacter = function(checked)
	if m_StoreList.sendToDepot then
		m_StoreList.sendToDepot:setChecked(not checked)
	end
end

m_StoreFunctions.onOpenStore = function()
	modules.game_character.m_CharacterFunctions.destroy()
	modules.game_encyclopedia.m_EncyclopediaFunctions.destroy()
	modules.game_rewards.m_RewardsFunctions.destroy()
	modules.game_questlog.m_QuestLogFunctions.destroy()
	modules.game_market.m_MarketFunctions.destroy()
	
	m_StoreList.window = g_ui.displayUI('game_store')
	m_StoreList.index = m_StoreList.window:getChildById('indexList')
	m_StoreList.balance = m_StoreList.window:getChildById('balance')
	m_StoreList.productList = m_StoreList.window:getChildById('productList')
	m_StoreList.sendToCharacter = m_StoreList.window:getChildById('sendToCharacter')
	m_StoreList.sendToDepot = m_StoreList.window:getChildById('sendToDepot')
	m_StoreList.addon1 = m_StoreList.window:getChildById('addon1')
	m_StoreList.addon2 = m_StoreList.window:getChildById('addon2')
	m_StoreList.currentName = m_StoreList.window:getChildById('currentName')
	m_StoreList.currentOffer = m_StoreList.window:getChildById('currentOffer')
	m_StoreList.item = m_StoreList.currentOffer:getChildById('item')
	m_StoreList.outfit = m_StoreList.currentOffer:getChildById('outfit')
	m_StoreList.image = m_StoreList.currentOffer:getChildById('image')
	m_StoreList.price = m_StoreList.currentOffer:getChildById('price')
	m_StoreList.button = m_StoreList.window:getChildById('buyButton')
	m_StoreList.window:getChildById('coinButton'):setOn(g_settings.get('locale', 'en') ~= 'en')
	
	if m_StoreFunctions.balance then
		m_StoreList.balance:setText(g_game.getMilharNumber(m_StoreFunctions.balance))
	end
	
	for i = 1, #storeIndex do
		local widget = g_ui.createWidget('CategoryStoreWidget', m_StoreList.index)
		widget:setOn(i > 1)
		widget:setId(i)
		widget:setText(storeIndex[i].name)
		
		local tmpWidget = widget:getChildById('blink')
		tmpWidget:setOn(i > 1)
		
		local tmpWidget = widget:getChildById('icon')
		tmpWidget:setOn(i > 1)
		tmpWidget:setImageSource(storeIndex[i].imageSource)
	end
	
	m_StoreFunctions.onChooseCategory(1)
	modules.game_bottompanel.m_MainFunctions.open(WINDOW_STORE)
end

m_StoreFunctions.onCloseStore = function()
	m_StoreFunctions.destroyPopup()
	m_StoreFunctions.hoverDestroy()
	
	if m_StoreList.window then
		m_StoreList.window:destroy()
		m_StoreList = {}
	end
	
	modules.game_bottompanel.m_MainFunctions.close(WINDOW_STORE)
end

m_StoreFunctions.hoverDestroy = function()
	if m_StoreList.hover then
		m_StoreList.hover:destroy()
		m_StoreList.hover = nil
		m_StoreList.label = nil
	end
end

m_StoreFunctions.blink = function()
	if m_StoreList.blink then
		m_StoreList.balance:setColor(m_StoreFunctions.disabled)
	else
		m_StoreList.balance:setColor(m_StoreFunctions.red)
	end
	
	m_StoreList.blink = not m_StoreList.blink
end

m_StoreFunctions.acceptStore = function()
	if not m_StoreFunctions.balance then
		m_StoreFunctions.balance = 0
	end
	
	if m_StoreFunctions.balance >= m_StoreList.popup.price then
		g_game.buyPremiumItem(m_StoreList.popup.id, m_StoreList.popup.itemId, m_StoreList.sendToCharacter:isChecked())
		m_StoreFunctions.destroyPopup()
	elseif not m_StoreList.event then
		m_StoreList.event = cycleEvent(m_StoreFunctions.blink, 500)
	end
end

function onLoad()
	connect(g_game, {
		onGameEnd = onGameEnd,
		onUpdateBalance = onUpdateBalance
	})
end

function onUnload()
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onUpdateBalance = onUpdateBalance
	})
	
	m_StoreFunctions.onCloseStore()
end

function onUpdateBalance(balance)
	m_StoreFunctions.balance = balance
	if m_StoreList.window then
		m_StoreList.balance:setText(g_game.getMilharNumber(balance))
	end
end

function onGameEnd()
	m_StoreFunctions:onCloseStore()
end

function toggle()
	if m_StoreList.window then
		m_StoreFunctions.onCloseStore()
	else
		m_StoreFunctions.onOpenStore()
	end
end

function sendMessage(text)
	m_StoreFunctions.createPopup('popup_message')
	m_StoreList.popup:getChildById('description'):setText(text)
end

function onHoverChange(widget, hovered)
	if hovered then
		if m_StoreList.hover then
			return true
		end
		
		m_StoreList.hover = g_ui.displayUI('popup_outfit')
		m_StoreFunctions.playerOutfit.type = widget.var.outfitId
		m_StoreFunctions.playerOutfit.addons = m_StoreFunctions.getAddons()
		m_StoreList.hover:getChildById('outfit'):setOutfit(m_StoreFunctions.playerOutfit)
		
		local pos = widget:getPosition()
		pos.x = pos.x + widget:getWidth() - 16
		pos.y = pos.y + widget:getHeight() - 16
		
		local rootWidget = modules.game_interface.getRootPanel()
		if pos.x + m_StoreList.hover:getWidth() >= rootWidget:getWidth() then
			pos.x = pos.x - widget:getWidth()
		end
		
		if pos.y + m_StoreList.hover:getHeight() >= rootWidget:getHeight() then
			pos.y = pos.y - widget:getHeight()
		end
		
		m_StoreList.hover:setPosition(pos)
	else
		m_StoreFunctions.hoverDestroy()
	end
end

function onFeature(widget, hovered)
	if hovered then
		if not m_StoreFunctions.featuresList then
			m_StoreFunctions.featuresList = ''
			for i = 1, #m_StoreFunctions.features do
				m_StoreFunctions.featuresList = m_StoreFunctions.featuresList .. m_StoreFunctions.features[i]
			end
		end
		
		if m_StoreList.hover then
			m_StoreList.label:setText(m_StoreFunctions.featuresList)
			m_StoreList.hover:setWidth(m_StoreList.label:getWidth() + 16)
			m_StoreList.hover:setHeight(m_StoreList.label:getHeight() + 16)
			return false
		end
		
		m_StoreList.hover = g_ui.displayUI('popup_window')
		m_StoreList.label = m_StoreList.hover:getChildById('label')
		
		m_StoreList.label:setText(m_StoreFunctions.featuresList)
		m_StoreList.hover:setWidth(m_StoreList.label:getWidth() + 16)
		m_StoreList.hover:setHeight(m_StoreList.label:getHeight() + 16)
		
		local pos = widget:getPosition()
		pos.x = pos.x + widget:getWidth() - 48
		pos.y = pos.y + widget:getHeight() - 48
		
		local rootWidget = modules.game_interface.getRootPanel()
		if pos.x + m_StoreList.hover:getWidth() >= rootWidget:getWidth() then
			pos.x = pos.x - widget:getWidth()
		end
		
		if pos.y + m_StoreList.hover:getHeight() >= rootWidget:getHeight() then
			pos.y = pos.y - widget:getHeight()
		end
		
		m_StoreList.hover:setPosition(pos)
	else
		m_StoreFunctions.hoverDestroy()
	end
end
