m_VipFunctions = {}
m_VipList = {}

m_VipFunctions.trueHeight = 435
m_VipFunctions.falseHeight = 155

m_VipFunctions.STATUS_ONLINE = 1
m_VipFunctions.STATUS_PENDING = 2
m_VipFunctions.STATUS_BUSY = 3
m_VipFunctions.STATUS_OFFLINE = 4
m_VipFunctions.clip = {
	[m_VipFunctions.STATUS_ONLINE] = {tr('Online'), '1106 58 12 12'},
	[m_VipFunctions.STATUS_PENDING] = {tr('Pending'), '1138 58 12 12'},
	[m_VipFunctions.STATUS_BUSY] = {tr('Busy'), '1089 58 12 12'},
	[m_VipFunctions.STATUS_OFFLINE] = {tr('Offline'), '1123 58 12 12'}
}

m_VipFunctions.destroyAddVip = function()
	if m_VipList.addVip then
		m_VipList.addVip:destroy()
		m_VipList.addVip = nil
	end
end

m_VipFunctions.destroy = function()
	m_VipFunctions.destroyAddVip()
	
	if m_VipFunctions.button then
		m_VipFunctions.button:setOn(false)
	end
	
	if m_VipList.window then
		m_VipList.window:hide()
	end
end

m_VipFunctions.sortByName = function(x, y)
	return x[2] < y[2]
end

m_VipFunctions.create = function()
	m_VipList.window = g_ui.createWidget('VipWindow', modules.game_interface.getRootPanel())
	m_VipList.window:setup()
	m_VipList.window:show()
	
	m_VipList.name = m_VipList.window:getChildById('name')
	m_VipList.status = m_VipList.window:getChildById('status')
	m_VipList.search = m_VipList.window:getChildById('search')
	m_VipList.list = m_VipList.window:getChildById('list')
	m_VipList.online = m_VipList.window:getChildById('online')
	
	local player = g_game.getLocalPlayer()
	if player then
		m_VipList.name:setText(player:getName():upper())
		m_VipFunctions.status(nil, player:getVipStatus(), true)
	end
	
	if not m_VipFunctions.VIPlist then
		m_VipFunctions.VIPlist = {}
		for i = m_VipFunctions.STATUS_ONLINE, m_VipFunctions.STATUS_OFFLINE do
			m_VipFunctions.VIPlist[i] = {}
		end
		
		for k, v in pairs(g_game.getVips()) do
			table.insert(m_VipFunctions.VIPlist[v[2]], {k, v[1], v[2]})
		end
	end
	
	m_VipFunctions.reset()
	if m_VipFunctions.button then
		m_VipFunctions.button:setOn(true)
	end
end

m_VipFunctions.toggle = function()
	if m_VipFunctions.button:isOn() then
		m_VipFunctions.destroy()
	else
		m_VipFunctions.create()
	end
end

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onAddVip = onAddVip,
		onVipStateChange = onVipStateChange
	})
	
	g_keyboard.bindKeyDown('Ctrl+P', m_VipFunctions.toggle)
	g_ui.importStyle('game_viplist')
	
	if g_game.isOnline() then
		m_VipFunctions.create()
		onGameStart()
	end
end

function onUnload()
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onAddVip = onAddVip,
		onVipStateChange = onVipStateChange
	})

	g_keyboard.unbindKeyDown('Ctrl+P')
	m_VipFunctions.destroy()
	if m_VipList.window then
		m_VipList.window:destroy()
		m_VipList.window = {}
	end
end

m_VipFunctions.createAddWindow = function()
	m_VipList.addVip = g_ui.displayUI('addvip')
end

m_VipFunctions.add = function(self)
	m_VipFunctions.createAddWindow()
end

m_VipFunctions.addVip = function()
	g_game.addVip(m_VipList.addVip:getChildById('name'):getText())
	m_VipFunctions.destroyAddVip()
end

m_VipFunctions.status = function(self, status, ignore)
	if m_VipList.VIPstatus == status then
		return true
	end
	
	local var = m_VipFunctions.clip[status]
	m_VipList.status:setText(var[1])
	m_VipList.status:setIconClip(var[2])
	m_VipList.VIPstatus = status
	
	if not ignore then
		g_game.editVipStatus(status)
	end
end

m_VipFunctions.slide = function(self)
	local menu = g_ui.createWidget('PopupMenu')
	menu:setGameMenu(true)
	menu:addOption(tr('Online'), function() m_VipFunctions.status(self, m_VipFunctions.STATUS_ONLINE) end)
	menu:addOption(tr('Pending'), function() m_VipFunctions.status(self, m_VipFunctions.STATUS_PENDING) end)
	menu:addOption(tr('Busy'), function() m_VipFunctions.status(self, m_VipFunctions.STATUS_BUSY) end)
	menu:addOption(tr('Offline'), function() m_VipFunctions.status(self, m_VipFunctions.STATUS_OFFLINE) end)
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 3
	pos.y = pos.y + self:getHeight() - 3
	menu:display(pos)
end

m_VipFunctions.editName = function(self)
	if self:getText() ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
end

m_VipFunctions.editSearch = function(self)
	local text = self:getText()
	if text ~= '' then
		self:getChildById('mask'):hide()
		
		local list = m_VipList.list:getChildren()
		for _, v in pairs(list) do
			local name = v:getChildById('name'):getText()
			if not name:lower():find(text:lower()) then
				v:hide()
			else
				v:show()
			end
		end
	else
		local list = m_VipList.list:getChildren()
		for _, v in pairs(list) do
			v:show()
		end
		
		self:getChildById('mask'):show()
	end
end

m_VipFunctions.show = function(self)
	if self:isOn() then
		m_VipList.window:setHeight(m_VipFunctions.falseHeight)
		m_VipList.window:setImageClip('1097 97 256 ' .. m_VipFunctions.falseHeight)
		self:setOn(false)
		self:rotate(0)
	else
		m_VipList.window:setHeight(m_VipFunctions.trueHeight)
		m_VipList.window:setImageClip('1097 97 256 ' .. m_VipFunctions.trueHeight)
		self:setOn(true)
		self:rotate(180)
	end
end

m_VipFunctions.remove = function(widget)
	if widget then
		local id = tonumber(widget:getId())
		for i = 1, #m_VipFunctions.VIPlist[widget.status] do
			if m_VipFunctions.VIPlist[widget.status][i][1] == id then
				table.remove(m_VipFunctions.VIPlist[widget.status], i)
				break
			end
		end
		
		g_game.removeVip(widget:getId())
		m_VipList.list:removeChild(widget)
		m_VipFunctions.reset(true)
	end
end

m_VipFunctions.send = function(self)
	g_game.openPrivateChannel(self:getChildById('name'):getText())
end

m_VipFunctions.reset = function(ignore)
	if not ignore then
		m_VipList.list:destroyChildren()
	end
	
	local total = 0
	for i = m_VipFunctions.STATUS_ONLINE, m_VipFunctions.STATUS_OFFLINE do
		if not ignore then
			table.sort(m_VipFunctions.VIPlist[i], m_VipFunctions.sortByName)
			for j = 1, #m_VipFunctions.VIPlist[i] do
				local id, name, status = unpack(m_VipFunctions.VIPlist[i][j])
				onAddVip(id, name, status, true)
			end
		end
		
		total = total + #m_VipFunctions.VIPlist[i]
	end
	
	m_VipList.online:setText('(' .. #m_VipFunctions.VIPlist[m_VipFunctions.STATUS_ONLINE] .. ' / ' .. total .. ')')
end

function onGameStart()
	m_VipFunctions.button = modules.client_topmenu.addTopButton('VipListButton', 'vipListButton', tr('Friend List') .. ' (Ctrl+P)', m_VipFunctions.toggle)
end

function onGameEnd()
	m_VipFunctions.destroy()
end

function onAddVip(id, name, status, reset)
	if not m_VipList.window then
		return false
	end
	
	if not reset then
		for i = m_VipFunctions.STATUS_ONLINE, m_VipFunctions.STATUS_OFFLINE do
			local done = false
			for j = 1, #m_VipFunctions.VIPlist[i] do
				if m_VipFunctions.VIPlist[i][j][1] == id then
					table.remove(m_VipFunctions.VIPlist[i], j)
					done = true
					break
				end
			end
			
			if done then
				break
			end
		end
		
		table.insert(m_VipFunctions.VIPlist[status], {id, name, status})
		m_VipFunctions.reset()
		return true
	end
	
	local widget = g_ui.createWidget('VipMember', m_VipList.list)
	widget:getChildById('name'):setText(name)
	widget:getChildById('name'):setOn(status ~= m_VipFunctions.STATUS_OFFLINE)
	widget:getChildById('message'):setEnabled(status ~= m_VipFunctions.STATUS_OFFLINE)
	widget:setId(id)
	widget.status = status
	widget.name = name
	
	widget.onMouseRelease = function(widget, mousePosition, mouseButton)
		if mouseButton == MouseRightButton then
			local menu = g_ui.createWidget('PopupMenu')
			menu:setGameMenu(true)
			menu:addOption(tr('Remove %s', widget:getChildById('name'):getText()), function() m_VipFunctions.remove(widget) end)
			menu:display(mousePosition)
		end
	end
	
	local vipStatus = widget:getChildById('status')
	local var = m_VipFunctions.clip[status]
	vipStatus:setText(var[1])
	vipStatus:setIconClip(var[2])
	
	m_VipList.list:moveChildToIndex(widget, m_VipList.list:getChildCount())
end

function onVipStateChange(id, status)
	if not m_VipList.list then
		return true
	end
	
	local widget = m_VipList.list:getChildById(id)
	if not widget then
		return true
	end
	
	for i = 1, #m_VipFunctions.VIPlist[widget.status] do
		if m_VipFunctions.VIPlist[widget.status][i][1] == id then
			table.remove(m_VipFunctions.VIPlist[widget.status], i)
			break
		end
	end
	
	table.insert(m_VipFunctions.VIPlist[status], {id, widget.name, status})
	m_VipList.list:removeChild(widget)
	m_VipFunctions.reset()
end