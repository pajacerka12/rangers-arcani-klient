m_DungeonList = {}
m_DungeonFunctions = {}

m_DungeonFunctions.ID_EARTH = 1
m_DungeonFunctions.ID_FIRST = m_DungeonFunctions.ID_EARTH
m_DungeonFunctions.ID_ICE = 2
m_DungeonFunctions.ID_STONE = 3
m_DungeonFunctions.ID_WOOD = 4
m_DungeonFunctions.ID_SAND = 5
m_DungeonFunctions.ID_DARKNESS = 6
m_DungeonFunctions.ID_LEAF = 7
m_DungeonFunctions.ID_LAVA = 8
m_DungeonFunctions.ID_FIRE = 9
m_DungeonFunctions.ID_LAST = m_DungeonFunctions.ID_FIRE

m_DungeonFunctions.size = {tr('Small'), tr('Medium'), tr('Big'), tr('Huge')}
m_DungeonFunctions.clip = {
	[m_DungeonFunctions.ID_EARTH] = {clip = {10, 126}, clipHover = {215, 126}, clipDungeonType = {156, 1603}, name = tr('Earth')},
	[m_DungeonFunctions.ID_ICE] = {clip = {138, 8}, clipHover = {343, 8}, clipDungeonType = {371, 1605}, name = tr('Icy')},
	[m_DungeonFunctions.ID_STONE] = {clip = {74, 8}, clipHover = {279, 8}, clipDungeonType = {588, 1607}, name = tr('Stone')},
	[m_DungeonFunctions.ID_WOOD] = {clip = {10, 67}, clipHover = {215, 67}, clipDungeonType = {1714, 1660}, name = tr('Wooden')},
	[m_DungeonFunctions.ID_SAND] = {clip = {74, 126}, clipHover = {279, 126}, clipDungeonType = {1022, 1626}, name = tr('Sand')},
	[m_DungeonFunctions.ID_DARKNESS] = {clip = {135, 67}, clipHover = {340, 67}, clipDungeonType = {1955, 1659}, name = tr('Darkness')},
	[m_DungeonFunctions.ID_LEAF] = {clip = {10, 8}, clipHover = {215, 8}, clipDungeonType = {1462, 1649}, name = tr('Leaf')},
	[m_DungeonFunctions.ID_LAVA] = {clip = {138, 126}, clipHover = {343, 126}, clipDungeonType = {1255, 1639}, name = tr('Hellfire')},
	[m_DungeonFunctions.ID_FIRE] = {clip = {74, 67}, clipHover = {279, 67}, clipDungeonType = {813, 1617}, name = tr('Fiery')}
}

m_DungeonFunctions.DIFFICULTY_EASY = 1
m_DungeonFunctions.DIFFICULTY_MEDIUM = 2
m_DungeonFunctions.DIFFICULTY_HARD = 3

m_DungeonFunctions.GameServerDungeon = 55
m_DungeonFunctions.GameServerPartyList = 56
m_DungeonFunctions.GameServerSendDungeon = 88

m_DungeonFunctions.messages = {
	tr('Minimum required amount of players'),
}

m_DungeonFunctions.memberList = {}
m_DungeonFunctions.estimatedTime = {20, 60, 90, 180}
m_DungeonFunctions.pricePerSize = {100000, 350000, 650000, 1000000}
m_DungeonFunctions.priceMultiplier = {
	[m_DungeonFunctions.DIFFICULTY_EASY] = 0.25,
	[m_DungeonFunctions.DIFFICULTY_MEDIUM] = 1.0,
	[m_DungeonFunctions.DIFFICULTY_HARD] = 1.75,
}

m_DungeonFunctions.items = {
	[m_DungeonFunctions.DIFFICULTY_EASY] = {3035, 3036, 3037, 3038, 3039, 3041, 3052, 13557, 6528},
	[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {3007, 17416, 3734, 13566, 10742},
	[m_DungeonFunctions.DIFFICULTY_HARD] = {9538, 3043, 13510, 10869, 12034},
}

m_DungeonFunctions.biomes = {
	{id = m_DungeonFunctions.ID_EARTH,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Groveller', 'Zombie', 'Bonebeast', 'Frightening Slime', 'Vampire', 'Souleater', 'Banshee', 'Vampire Bride', 'Vampire Viscount'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Grunt', 'Souleater', 'Banshee', 'Vampire Bride', 'Vampire Viscount', 'The Fallen Servant', 'Betrayed Wraith', 'Grim Reaper', 'Plaguesmith'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Overlord', 'The Fallen Servant', 'Betrayed Wraith', 'Grim Reaper', 'Plaguesmith', 'Skeletor', 'Vampire Count', 'Ghastly Dragon', 'Undead Dragon'
			},
		}
	},
	{id = m_DungeonFunctions.ID_ICE,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Maimer', 'Ice Witch', 'Crystal Spider', 'Blue Djinn', 'Marid', 'Troll Ice', 'Troll Champion Ice', 'Frost Dragon Hatchling', 'Frost Dragon'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Reaver', 'Troll Ice', 'Troll Champion Ice', 'Frost Dragon Hatchling', 'Frost Dragon', 'Crystal Spider', 'Frost Dragon', 'Water Elemental', 'Massive Water Elemental'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Destroyer', 'Crystal Spider', 'Frost Dragon', 'Water Elemental', 'Massive Water Elemental', 'Deepling Spellsinger', 'Deepling Warrior', 'Deepling Guard', 'Sapphire Dragon'
			},
		}
	},
	{id = m_DungeonFunctions.ID_STONE,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Brawler', 'Stone Golem', 'Gargoyle', 'Golem', 'Cyclops Drone', 'Cyclops Smith', 'Cyclops Elite', 'Earth Elemental', 'Massive Earth Elemental'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Scourge', 'Cyclops Smith', 'Cyclops Elite', 'Earth Elemental', 'Massive Earth Elemental', 'Plague Bearer', 'Earth Destroyer', 'Primeval', 'Ogre'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Berserker', 'Plague Bearer', 'Earth Destroyer', 'Primeval', 'Ogre', 'Destroyer', 'Behemoth', 'Cliff Strider', 'Juggernaut'
			},
		}
	},
	{id = m_DungeonFunctions.ID_WOOD,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Fiend', 'Dark Apprentice', 'Dark Magician', 'Witch', 'Pirate Marauder', 'Pirate Buccaneer', 'Teracian', 'Pirate Buccaneer', 'Pirate Corsair'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Demon', 'Pirate Buccaneer', 'Teracian', 'Pirate Buccaneer', 'Pirate Corsair', 'Outlaw Bleed', 'Outcast Bleed', 'Hero', 'Black Knight'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Condemned', 'Outlaw Bleed', 'Outcast Bleed', 'Hero', 'Black Knight', 'Knight Templar', 'Commodore of the Kingdom', 'Fallen Island Warlock', 'Warlock'
			},
		}
	},
	{id = m_DungeonFunctions.ID_SAND,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Battler', 'Rotworm Queen', 'Lancer Beetle', 'Dustrunner', 'Wailing Widow', 'Giant Spider', 'Ancient Scarab', 'Royal Minotaur', 'Royal Minotaur Warrior'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Blackling', 'Giant Spider', 'Ancient Scarab', 'Royal Minotaur', 'Royal Minotaur Warrior', 'Sandstone Scorpion', 'Sandstone Cobra', 'Serpent Spawn', 'Hydra'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Lord', 'Sandstone Scorpion', 'Sandstone Cobra', 'Serpent Spawn', 'Hydra', 'Warrior Yuan-ti', 'Shooter Yuan-ti', 'Mage Yuan-ti', 'Defender Yuan-ti'
			},
		}
	},
	{id = m_DungeonFunctions.ID_DARKNESS,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Leader', 'Aggressive Insect', 'Tarantula', 'Lizard Snakecharmer', 'Lizard Sentinel', 'Lizard Templar', 'Lizard Legionnaire', 'Lizard High Guard', 'Lizard Dragon Priest'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Prince', 'Lizard Templar', 'Lizard Legionnaire', 'Lizard High Guard', 'Lizard Dragon Priest', 'Lost Soul', 'Blightwalker', 'Draken Warmaster', 'Draken Spellweaver'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Slave', 'Lost Soul', 'Blightwalker', 'Draken Warmaster', 'Draken Spellweaver', 'Draken Elite', 'Draken Abomination', 'Abomination Creature', 'Ghastly Dragon'
			},
		}
	},
	{id = m_DungeonFunctions.ID_LEAF,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Phantom', 'Forest Spirit', 'Carniphila', 'Slime', 'Bog Raider', 'Leaf Golem', 'Brimstone Bug', 'Troll Leaf', 'Troll Champion Leaf'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Brimstone', 'Leaf Golem', 'Brimstone Bug', 'Troll Leaf', 'Troll Champion Leaf', 'Serpent Spawn', 'Medusa', 'Ogre', 'Ent'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Wrath', 'Serpent Spawn', 'Medusa', 'Ogre', 'Ent', 'Plague Bearer', 'Earth Destroyer', 'Defiler', 'Treebeard'
			},
		}
	},
	{id = m_DungeonFunctions.ID_LAVA,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Devil', 'Fire Devil', 'Black Devil', 'Troll Fire', 'Troll Champion Fire', 'Shaburak Lord', 'Shaburak Demon', 'Spectre', 'Nightmare'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Terror', 'Shaburak Lord', 'Shaburak Demon', 'Spectre', 'Nightmare', 'Hellhound', 'Hand of Cursed Fate', 'Fury', 'Dark Torturer'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Scorn', 'Hellhound', 'Hand of Cursed Fate', 'Fury', 'Dark Torturer', 'Hellfire Fighter', 'Chimera', 'Demon', 'Arch Devil'
			},
		}
	},
	{id = m_DungeonFunctions.ID_FIRE,
		targets = {
			[m_DungeonFunctions.DIFFICULTY_EASY] = {
				'Pit Hunter', 'Fire Devil', 'Nightmare Scion', 'Fire Elemental', 'Massive Fire Elemental', 'Dragon Hatchling', 'Green Dragon', 'Dragon Lord Hatchling', 'Red Dragon'
			},
			[m_DungeonFunctions.DIFFICULTY_MEDIUM] = {
				'Pit Savant', 'Dragon Hatchling', 'Green Dragon', 'Dragon Lord Hatchling', 'Red Dragon', 'Efreet', 'Red Djinn', 'Hellspawn', 'Infernalist'
			},
			[m_DungeonFunctions.DIFFICULTY_HARD] = {
				'Pit Rage', 'Efreet', 'Red Djinn', 'Hellspawn', 'Infernalist', 'Flicker of Death', 'Spark of the Phoenix', 'Hellfire Fighter', 'Diabolic Imp'
			},
		}
	},
}

m_DungeonFunctions.addMember = function(name, leader)
	m_DungeonFunctions.removeMember(name)
	m_DungeonFunctions.memberList[name] = leader
	
	if not m_DungeonList.window then
		return true
	end
	
	local widget = g_ui.createWidget('DungeonPartyLabel')
	widget:setText(name)
	widget.name = name
	
	if leader == 1 then
		m_DungeonList.partyMembers:insertChild(1, widget)
		widget:getChildById('shield'):setOn(true)
	else
		widget:setParent(m_DungeonList.partyMembers)
	end
	
	m_DungeonFunctions.updateMembersStatus()
end

m_DungeonFunctions.removeMember = function(name)
	m_DungeonFunctions.memberList[name] = false
	
	if not m_DungeonList.window then
		return true
	end
	
	for _, pid in pairs(m_DungeonList.partyMembers:getChildren()) do
		if pid.name == name then
			pid:destroy()
			break
		end
	end
end

m_DungeonFunctions.selectDifficulty = function(self)
	if m_DungeonList.currentDifficulty then
		if m_DungeonList.currentDifficulty == self then
			return true
		end
		
		m_DungeonList.currentDifficulty:setOn(false)
	end
	
	m_DungeonList.currentDifficulty = self
	self:setOn(true)
	
	if self == m_DungeonList.skullWhite then
		m_DungeonList.difficultyLevel = m_DungeonFunctions.DIFFICULTY_EASY
		m_DungeonFunctions.addLabel('difficulty', '80 0 20 20', 'Difficulty level: EASY')
	elseif self == m_DungeonList.skullRed then
		m_DungeonList.difficultyLevel = m_DungeonFunctions.DIFFICULTY_MEDIUM
		m_DungeonFunctions.addLabel('difficulty', '100 0 20 20', 'Difficulty level: MEDIUM')
	elseif self == m_DungeonList.skullBlack then
		m_DungeonList.difficultyLevel = m_DungeonFunctions.DIFFICULTY_HARD
		m_DungeonFunctions.addLabel('difficulty', '120 0 20 20', 'Difficulty level: HARD')
	else
		return true
	end
	
	m_DungeonFunctions.updateDifficulty()
	m_DungeonFunctions.updateTargets()
	m_DungeonFunctions.updatePrice()
	m_DungeonFunctions.updateBalance()
	m_DungeonFunctions.updateBoss()
	m_DungeonFunctions.updateItems()
end

m_DungeonFunctions.updateDifficulty = function()
	if m_DungeonList.difficultyLevel == m_DungeonFunctions.DIFFICULTY_EASY then
		m_DungeonFunctions.addLabel('difficulty', '80 0 20 20', 'Difficulty level: EASY', true)
	elseif m_DungeonList.difficultyLevel == m_DungeonFunctions.DIFFICULTY_MEDIUM then
		m_DungeonFunctions.addLabel('difficulty', '100 0 20 20', 'Difficulty level: MEDIUM', true)
	elseif m_DungeonList.difficultyLevel == m_DungeonFunctions.DIFFICULTY_HARD then
		m_DungeonFunctions.addLabel('difficulty', '120 0 20 20', 'Difficulty level: HARD', true)
	end
end

m_DungeonFunctions.updateItems = function()
	m_DungeonList.itemList:destroyChildren()
	
	for i = m_DungeonList.difficultyLevel, m_DungeonFunctions.DIFFICULTY_EASY, -1 do
		local list = m_DungeonFunctions.items[i]
		for j = 1, #list do
			local widget = g_ui.createWidget('DungeonItem', m_DungeonList.itemList)
			widget:setItemId(list[j])
		end
	end
end

m_DungeonFunctions.updateTargets = function()
	if not m_DungeonList.currentSelect or not m_DungeonList.currentSelect.targets then
		return true
	end
	
	m_DungeonList.monsterList:destroyChildren()
	
	local list = m_DungeonList.currentSelect.targets[m_DungeonList.difficultyLevel]
	for i = 2, #list do
		local v = modules.game_lookat.m_LookAtFunctions.getMonsterByName(list[i])
		if v then
			local widget = g_ui.createWidget('DungeonCreature', m_DungeonList.monsterList)
			widget:getChildById('creature'):setOutfit(v.look)
			widget:getChildById('name'):setText(v.description[m_DungeonFunctions.language])
		end
	end
end

m_DungeonFunctions.select = function(self)
	if m_DungeonList.currentSelect then
		if m_DungeonList.currentSelect == self then
			return true
		end
		
		m_DungeonList.currentSelect:getParent():getChildById('blink'):hide()
	end
	
	m_DungeonList.currentSelect = self
	self:getParent():getChildById('blink'):show()
	
	m_DungeonList.dungeonSizeScrollbar:setEnabled(true)
	
	local value = m_DungeonList.dungeonSizeScrollbar:getValue()
	m_DungeonFunctions.updateBiomeName()
	m_DungeonFunctions.changeSize(value)
	m_DungeonFunctions.updateTargets()
	m_DungeonFunctions.updateItems()
end

m_DungeonFunctions.addLabel = function(id, clip, text, update)
	local widget = m_DungeonList.descriptionBox:getChildById(id)
	if not widget then
		widget = g_ui.createWidget('DungeonLabel', m_DungeonList.descriptionBox)
		widget:setId(id)
		widget:getChildById('icon'):setImageClip(clip)
	elseif update then
		widget:getChildById('icon'):setImageClip(clip)
	end
	
	widget:setText(text)
end

m_DungeonFunctions.updateMembersAmount = function()
	m_DungeonList.memberAmount = m_DungeonList.dungeonSizeScrollbar:getValue() * 2
	local text = m_DungeonFunctions.messages[1] .. ': '  .. m_DungeonList.memberAmount
	
	m_DungeonFunctions.addLabel('players', '0 0 20 20', text)
	m_DungeonFunctions.updateMembersStatus()
end

m_DungeonFunctions.updatePrice = function()
	local value = m_DungeonList.dungeonSizeScrollbar:getValue()
	local text = 'Total cost ' .. g_game.getMilharNumber(m_DungeonFunctions.pricePerSize[value] * m_DungeonFunctions.priceMultiplier[m_DungeonList.difficultyLevel]) .. ' copper coins'
	
	m_DungeonFunctions.addLabel('price', '20 0 20 20', text)
end

m_DungeonFunctions.updateDuration = function()
	local value = m_DungeonList.dungeonSizeScrollbar:getValue()
	local text = 'Estimated time: ' .. m_DungeonFunctions.estimatedTime[value] .. ' minutes'
	
	m_DungeonFunctions.addLabel('duration', '40 0 20 20', text)
end

m_DungeonFunctions.updateBalance = function(balance)
	if balance then
		m_DungeonFunctions.balance = g_game.getMilharNumber(balance)
	end
	
	if not m_DungeonList.window then
		return true
	end
	
	local text = 'Your balance: ' .. (m_DungeonFunctions.balance or 0) .. ' copper coins'
	
	m_DungeonFunctions.addLabel('balance', '140 0 20 20', text)
end

m_DungeonFunctions.updateBiomeName = function()
	local text = m_DungeonList.currentSelect.name .. ' biome type'
	
	m_DungeonFunctions.addLabel('name', '60 0 20 20', text)
end

m_DungeonFunctions.updateMembersStatus = function()
	local available = m_DungeonList.partyMembers:getChildCount() < m_DungeonList.memberAmount
	for _, pid in pairs(m_DungeonList.partyMembers:getChildren()) do
		pid:setOn(available)
	end
end

m_DungeonFunctions.changeSize = function(value)
	m_DungeonList.dungeonSize:setImageClip(m_DungeonList.currentSelect.clip[1] .. ' ' .. m_DungeonList.currentSelect.clip[2] .. ' 174 229')
	m_DungeonList.dungeonSizeScrollbar:setText(m_DungeonFunctions.size[value])
	
	m_DungeonFunctions.updatePrice()
	m_DungeonFunctions.updateBalance()
	m_DungeonFunctions.updateMembersAmount()
	m_DungeonFunctions.updateDuration()
	m_DungeonFunctions.updateBoss()
end

m_DungeonFunctions.updateBoss = function()
	if not m_DungeonList.currentSelect or not m_DungeonList.currentSelect.targets then
		return true
	end
	
	local list = m_DungeonList.currentSelect.targets[m_DungeonList.difficultyLevel]
	local v = modules.game_lookat.m_LookAtFunctions.getMonsterByName(list[1])
	if v then
		local widget = g_ui.createWidget('DungeonCreature', m_DungeonList.monsterList)
		m_DungeonList.boss:getChildById('creature'):setOutfit(v.look)
		m_DungeonList.boss:getChildById('name'):setText(v.description[m_DungeonFunctions.language])
	end
end

m_DungeonFunctions.execute = function()
	for _, v in pairs(m_DungeonFunctions.biomes) do
		if v.id ~= m_DungeonFunctions.ID_FIRE then
			local widget = g_ui.createWidget('BiomeButton', m_DungeonList.biomeList)
			local biome = widget:getChildById('biome')
			local tmpClip = m_DungeonFunctions.clip[v.id]
			biome:setImageClip(tmpClip.clip[1] .. ' ' .. tmpClip.clip[2] .. ' 55 55')
			biome.targets = v.targets
			biome.id = v.id
			biome.name = tmpClip.name
			biome.clip = tmpClip.clipDungeonType
			
			m_DungeonList.dungeonSizeScrollbar:setValue(1)
			m_DungeonList.dungeonSizeScrollbar:setText(m_DungeonFunctions.size[1])
			m_DungeonFunctions.updateDifficulty()
			
			biome.onHoverChange = function(self, hovered)
				if hovered then
					biome:setImageClip(tmpClip.clipHover[1] .. ' ' .. tmpClip.clipHover[2] .. ' 55 55')
				else
					biome:setImageClip(tmpClip.clip[1] .. ' ' .. tmpClip.clip[2] .. ' 55 55')
				end
			end
			
			if v.id == m_DungeonFunctions.ID_FIRST then
				m_DungeonFunctions.select(biome)
			end
		end
	end
	
	for name, var in pairs(m_DungeonFunctions.memberList) do
		if var then
			m_DungeonFunctions.addMember(name, var)
		end
	end
end

m_DungeonFunctions.create = function()
	if m_DungeonList.window then
		-- m_DungeonFunctions.destroy()
	else
		m_DungeonList.window = g_ui.displayUI('game_dungeons')
		m_DungeonList.biomeList = m_DungeonList.window:getChildById('biomeList')
		m_DungeonList.monsterList = m_DungeonList.window:getChildById('monsterList')
		m_DungeonList.partyMembers = m_DungeonList.window:getChildById('partyMembers')
		m_DungeonList.dungeonSizeScrollbar = m_DungeonList.window:getChildById('dungeonSizeScrollbar')
		m_DungeonList.itemList = m_DungeonList.window:getChildById('itemList')
		m_DungeonList.boss = m_DungeonList.window:getChildById('boss')
		
		local value = m_DungeonList.dungeonSizeScrollbar:getChildById('valueLabel')
		value:setFont('perfect-sans-8px')
		value:setColor('#AC9B8A')
		
		m_DungeonList.descriptionBox = m_DungeonList.window:getChildById('descriptionBox')
		
		m_DungeonList.dungeonSize = m_DungeonList.window:getChildById('dungeonSize')
		m_DungeonList.skullBlack = m_DungeonList.dungeonSize:getChildById('skullBlack')
		m_DungeonList.skullRed = m_DungeonList.dungeonSize:getChildById('skullRed')
		m_DungeonList.skullWhite = m_DungeonList.dungeonSize:getChildById('skullWhite')
		
		
		m_DungeonList.currentDifficulty = m_DungeonList.skullWhite
		m_DungeonList.difficultyLevel = m_DungeonFunctions.DIFFICULTY_EASY
		
		m_DungeonFunctions.execute()
	end
end

m_DungeonFunctions.destroy = function()
	if m_DungeonList.window then
		m_DungeonList.biomeList:destroyChildren()
		m_DungeonList.monsterList:destroyChildren()
		
		m_DungeonList.window:destroy()
		m_DungeonList = {}
	end
end

m_DungeonFunctions.parseDungeon = function(protocol, msg)
	local id = msg:getU8()
	if id == 1 then
		m_DungeonFunctions.destroy()
	else
		m_DungeonFunctions.create()
	end
end

m_DungeonFunctions.parsePartyList = function(protocol, msg)
	local size = msg:getU16()
	if m_DungeonList.partyMembers then
		if size ~= 1 then
			m_DungeonList.partyMembers:destroyChildren()
		elseif size == 1 then
			for _, pid in pairs(m_DungeonList.partyMembers:getChildren()) do
				if pid:getChildById('shield'):isOn() then
					pid:getChildById('shield'):setOn(false)
					break
				end
			end
		end
	end
	
	for i = 1, size do
		m_DungeonFunctions.addMember(msg:getString(), i == size and 1 or 2)
	end
end

m_DungeonFunctions.accept = function()
	if not m_DungeonList.currentSelect then
		return true
	end
	
	local msg = OutputMessage.create()
	msg:addU8(m_DungeonFunctions.GameServerSendDungeon)
	msg:addU8(m_DungeonList.difficultyLevel)
	msg:addU8(m_DungeonList.currentSelect.id)
	msg:addU8(m_DungeonList.dungeonSizeScrollbar:getValue())
	g_game.getProtocolGame():send(msg)
	
	-- m_DungeonFunctions.destroy()
end

m_DungeonFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_DungeonFunctions.GameServerDungeon, m_DungeonFunctions.parseDungeon)
	ProtocolGame.registerOpcode(m_DungeonFunctions.GameServerPartyList, m_DungeonFunctions.parsePartyList)
end

m_DungeonFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_DungeonFunctions.GameServerDungeon, m_DungeonFunctions.parseDungeon)
	ProtocolGame.unregisterOpcode(m_DungeonFunctions.GameServerPartyList, m_DungeonFunctions.parsePartyList)
end

function onGameStart()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_DungeonFunctions.language = LANGUAGE_ENGLISH
	else
		m_DungeonFunctions.language = LANGUAGE_POLISH
	end
	
	m_DungeonFunctions.registerProtocol()
end

function onGameEnd()
	m_DungeonFunctions.destroy()
	m_DungeonFunctions.unregisterProtocol()
end

function onLoad()
	connect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart,
	})
	
	if g_game.isOnline() then
		onGameStart()
	end
end

function onUnload()
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart,
	})
	
	onGameEnd()
end