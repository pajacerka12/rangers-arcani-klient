m_HighscoresList = {}
m_HighscoresFunctions = {}

m_HighscoresFunctions.GameServerOpenHighscores = 86
m_HighscoresFunctions.ClientOpenHighscores = 80

m_HighscoresFunctions.page = 1
m_HighscoresFunctions.resultsPerPage = 10

m_HighscoresFunctions.rankId = {
	[1] = 9209,
	[2] = 9210,
	[3] = 9211,
}

m_HighscoresFunctions.categories = {
	tr('Experience level'), tr('Achievements stars'), tr('Obtained legendaries'), tr('Completed bestiary')
}

m_HighscoresFunctions.send = function(highscoreId, vocationId)
	local msg = OutputMessage.create()
	msg:addU8(m_HighscoresFunctions.ClientOpenHighscores)
	msg:addU8(highscoreId)
	msg:addU8(vocationId)
	msg:addU16(m_HighscoresFunctions.page)
	g_game.getProtocolGame():send(msg)
end

m_HighscoresFunctions.create = function()
	if m_HighscoresList.window then
		m_HighscoresFunctions.destroy()
	else
		m_HighscoresFunctions.button:setOn(true)
		m_HighscoresFunctions.page = 1
		
		m_HighscoresList.window = g_ui.displayUI('game_highscores')
		m_HighscoresList.list = m_HighscoresList.window:getChildById('list')
		m_HighscoresList.topList = m_HighscoresList.window:getChildById('topList')
		m_HighscoresList.categoryBox = m_HighscoresList.window:getChildById('categoryBox')
		m_HighscoresList.vocationBox = m_HighscoresList.window:getChildById('vocationBox')
		m_HighscoresList.pageIndex = m_HighscoresList.window:getChildById('pageIndex')
		m_HighscoresList.lastUpdateTime = m_HighscoresList.window:getChildById('lastUpdateTime')
		m_HighscoresList.currentPosition = m_HighscoresList.window:getChildById('currentPosition')
		m_HighscoresList.online = m_HighscoresList.window:getChildById('online')
		
		-- m_HighscoresList.outfit = m_HighscoresList.currentPosition:getChildById('outfit')
		m_HighscoresList.rankId = m_HighscoresList.currentPosition:getChildById('rankId')
		
		m_HighscoresList.buttons = {}
		table.insert(m_HighscoresList.buttons, m_HighscoresList.window:getChildById('buttonPrev'))
		table.insert(m_HighscoresList.buttons, m_HighscoresList.window:getChildById('buttonBegin'))
		table.insert(m_HighscoresList.buttons, m_HighscoresList.window:getChildById('buttonNext'))
		table.insert(m_HighscoresList.buttons, m_HighscoresList.window:getChildById('buttonEnd'))
		
		for i = 0, 9 do
			if i == 0 then
				m_HighscoresList.vocationBox:addOption(tr('All'))
			else
				m_HighscoresList.vocationBox:addOption(getVocationNameById(i))
			end
		end
		
		for i = 1, #m_HighscoresFunctions.categories do
			m_HighscoresList.categoryBox:addOption(m_HighscoresFunctions.categories[i])
		end
		
		m_HighscoresFunctions.send(0, 0)
	end
end

m_HighscoresFunctions.next = function(switch)
	if switch then
		m_HighscoresFunctions.page = m_HighscoresFunctions.totalPages
	else
		if m_HighscoresFunctions.page < m_HighscoresFunctions.totalPages then
			m_HighscoresFunctions.page = m_HighscoresFunctions.page + 1
		end
	end
	
	m_HighscoresFunctions.submit(true)
end

m_HighscoresFunctions.prev = function(switch)
	if switch then
		m_HighscoresFunctions.page = 1
	else
		if m_HighscoresFunctions.page > 1 then
			m_HighscoresFunctions.page = m_HighscoresFunctions.page - 1
		end
	end
	
	m_HighscoresFunctions.submit(true)
end

m_HighscoresFunctions.submit = function(ignore)
	if not ignore then
		m_HighscoresFunctions.page = 1
	end
	
	m_HighscoresFunctions.send(m_HighscoresList.categoryBox:getCurrentIndex() - 1, m_HighscoresList.vocationBox:getCurrentIndex() - 1)
end

m_HighscoresFunctions.destroy = function()
	if m_HighscoresList.window then
		m_HighscoresFunctions.button:setOn(false)
		m_HighscoresList.window:destroy()
		m_HighscoresList = {}
	end
end

m_HighscoresFunctions.updateTime = function(seconds)
	local minutes = 0
	while(seconds >= 60) do
		minutes = minutes + 1
		seconds = seconds - 60
	end
	
	local hours = 0
	while(minutes >= 60) do
		hours = hours + 1
		minutes = minutes - 60
	end
	
	local description = 'Last Update: '
	if hours > 0 then
		description = description .. hours .. ' hour' .. (hours > 1 and 's' or '')
	end
	
	if minutes == 0 then
		minutes = 1
	end
	
	if hours > 0 then
		if seconds > 0 then
			description = description .. ', '
		else
			description = description .. ' and '
		end
	end
		
	description = description .. minutes .. ' minute' .. (minutes > 1 and 's' or '') .. ' ago.'
	m_HighscoresList.lastUpdateTime:setText(description)
end

m_HighscoresFunctions.parseCharacter = function(protocol, msg, useOutfit)
	local name = msg:getString()
	local vocationId = msg:getU8()
	local level = msg:getU16()
	local outfit = useOutfit and protocol:getOutfit(msg)
	return {name, vocationId, level, outfit}
end

m_HighscoresFunctions.onMouseRelease = function(self, mousePosition, mouseButton)
	if mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		menu:addOption(tr('Copy name'), function() g_window.setClipboardText(self:getId()) end)
		menu:display(menuPosition)
	end
end

m_HighscoresFunctions.addPlayer = function(parent, id, list, rankId, rank)
	local widget = g_ui.createWidget(id, parent)
	widget.onMouseRelease = m_HighscoresFunctions.onMouseRelease
	widget:setId(list[1])
	if list[4] then
		widget:getChildById('outfit'):setOutfit(list[4])
		widget:getChildById('rank'):setText(rank)
	end
	
	local tmpName = widget:getChildById('name')
	tmpName:setText(list[1])
	
	local tmpVocation = widget:getChildById('vocation')
	local clip
	local vocationId = list[2]
	if vocationId == 1 then
		clip = '57 57 19 19'
	elseif vocationId == 2 then
		clip = '19 76 19 19'
	elseif vocationId == 3 then
		clip = '113 57 19 19'
	elseif vocationId == 4 then
		clip = '38 19 19 19'
	elseif vocationId == 5 then
		clip = '0 19 19 19'
	elseif vocationId == 6 then
		clip = '76 38 19 19'
	elseif vocationId == 7 then
		clip = '19 0 19 19'
	elseif vocationId == 8 then
		clip = '94 0 19 19'
	elseif vocationId == 9 then
		clip = '57 0 19 19'
	end
	
	tmpVocation:setTooltip(getVocationNameById(vocationId))
	tmpVocation:setImageClip(clip)
	
	local tmpLevel = widget:getChildById('level')
	tmpLevel:setText(list[3])
	
	if rank == rankId then
		local tmpVocation = widget:getChildById('vocation')
		if tmpVocation then
			tmpVocation:setOn(true)
		end
		
		local tmpRank = widget:getChildById('rank')
		if tmpRank then
			tmpRank:setOn(true)
		end
		
		tmpName:setOn(true)
		tmpLevel:setOn(true)
	end
	
	return widget
end

m_HighscoresFunctions.parseHighscores = function(protocol, msg)
	m_HighscoresList.list:destroyChildren()
	m_HighscoresList.topList:destroyChildren()
	m_HighscoresFunctions.updateTime(math.ceil(msg:getU32() / 1000))
	
	local rankId = msg:getU16()
	m_HighscoresFunctions.totalPages = math.ceil(msg:getU16() / m_HighscoresFunctions.resultsPerPage)
	m_HighscoresFunctions.page = msg:getU16()
	
	if m_HighscoresList.pageIndex then
		m_HighscoresList.pageIndex:setText(m_HighscoresFunctions.page .. ' / ' .. m_HighscoresFunctions.totalPages)
		
		if m_HighscoresFunctions.totalPages > 1 then
			for i = 1, 2 do
				m_HighscoresList.buttons[i]:setEnabled(m_HighscoresFunctions.page > 1)
			end
			
			for i = 3, #m_HighscoresList.buttons do
				m_HighscoresList.buttons[i]:setEnabled(m_HighscoresFunctions.page < m_HighscoresFunctions.totalPages)
			end
		else
			for i = 1, #m_HighscoresList.buttons do
				m_HighscoresList.buttons[i]:setEnabled(false)
			end
		end
	end
	
	-- m_HighscoresList.outfit:setOutfit(g_game.getLocalPlayer():getOutfit())
	m_HighscoresList.rankId:setText(rankId == 0 and '?' or rankId)
	
	local size = msg:getU8()
	for i = 1, size do
		local v = m_HighscoresFunctions.parseCharacter(protocol, msg, true)
		if m_HighscoresList.window then
			local id = ((m_HighscoresFunctions.page - 1) * m_HighscoresFunctions.resultsPerPage) + i
			m_HighscoresFunctions.addPlayer(m_HighscoresList.list, 'HighscoresPlayer', v, rankId, id)
		end
	end
	
	local size = msg:getU16()
	for i = 1, size do
		local v = m_HighscoresFunctions.parseCharacter(protocol, msg, false)
		if m_HighscoresList.window then
			m_HighscoresFunctions.addPlayer(m_HighscoresList.topList, 'HighscoresOnlinePlayer', v, rankId, i)
		end
	end
	
	m_HighscoresList.online:setText(tr('Online players') .. ': ' .. size)
end

m_HighscoresFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_HighscoresFunctions.GameServerOpenHighscores, m_HighscoresFunctions.parseHighscores)
	g_keyboard.bindKeyDown('Ctrl+H', m_HighscoresFunctions.create)
end

m_HighscoresFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_HighscoresFunctions.GameServerOpenHighscores, m_HighscoresFunctions.parseHighscores)
	g_keyboard.unbindKeyDown('Ctrl+H')
end

function onGameStart()
	m_HighscoresFunctions.registerProtocol()
	m_HighscoresFunctions.button = modules.client_topmenu.addTopButton('HighscoresButton', 'HighscoresButton', tr('Highscores') .. ' (Ctrl+H)', m_HighscoresFunctions.create)
end

function onGameEnd()
	m_HighscoresFunctions.unregisterProtocol()
	m_HighscoresFunctions.destroy()
	
	if m_HighscoresFunctions.button then
		m_HighscoresFunctions.button:destroy()
		m_HighscoresFunctions.button = nil
	end
end

function onLoad()
	connect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart,
	})
	
	if g_game.isOnline() then
		onGameStart()
	end
end

function onUnload()
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart,
	})
	
	onGameEnd()
end