m_RunesFunctions = {}
m_RunesList = {}

m_RunesFunctions.GameServerCombineRunes = 90
m_RunesFunctions.crystalId = 18426
m_RunesFunctions.balance = 0

m_RunesFunctions.descriptionText = {
	{tr('5 rune dusts for 1 blank rune'), '#FFFFFF'},
	{tr('5 {desolate} runes for 1 rune dust'), '#848482'},
	{tr('5 {damaged} runes for 3 rune dusts'), '#E5E5E5'},
	{tr('5 {normal} runes for 1 blank rune'), '#32CD32'},
	{tr('5 {perfect} runes for 3 blank runes'), '#1E90FF'},
	{tr('5 {legendary} runes for 5 blank runes'), '#A020F0'},
	{tr('5 {unique} runes for 10 blank runes'), '#FFD700'}
}

function onGameEnd()
	m_RunesFunctions.destroy()
end

function onGameStart()
	
end

function onLoad()
	if g_game.isOnline() then
		onGameStart()
	end
	
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
end

function onUnload()
	m_RunesFunctions.destroy()
	
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
end

m_RunesFunctions.destroy = function()
	if m_RunesList.window then
		m_RunesList.description:destroyChildren()
		m_RunesList.window:destroy()
		m_RunesList = {}
		
		m_RunesFunctions.send(1)
	end
end

m_RunesFunctions.open = function()
	if m_RunesList.window then
		return false
	end
	
	m_RunesList.window = g_ui.displayUI('game_runes')
	m_RunesList.button = m_RunesList.window:getChildById('button')
	m_RunesList.description = m_RunesList.window:getChildById('description')
	m_RunesList.balance = m_RunesList.window:getChildById('balance')
	m_RunesList.window:getChildById('item'):setItemId(m_RunesFunctions.crystalId)
	m_RunesList.balance:setText(g_game.getMilharNumber(m_RunesFunctions.balance))
	
	for i = 1, #m_RunesFunctions.descriptionText do
		local widget = g_ui.createWidget('RuneCombineLabel', m_RunesList.description)
		g_game.setHighlightedText(widget, m_RunesFunctions.descriptionText[i][1], m_RunesFunctions.descriptionText[i][2])
	end
end

m_RunesFunctions.updateBalance = function(balance)
	m_RunesFunctions.balance = balance
	if m_RunesList.balance then
		m_RunesList.balance:setText(g_game.getMilharNumber(m_RunesFunctions.balance))
	end
end

m_RunesFunctions.updateConvert = function()
	local lastClassId = -1
	for i = InventoryRuneConvert1, InventoryRuneConvert5 do
		local widget = m_RunesList.window:getChildById('slot' .. i)
		local item = widget:getItem()
		if not item then
			m_RunesList.button:setEnabled(false)
			return true
		end
		
		local id = item:getClassId()
		if lastClassId == -1 then
			lastClassId = id
		elseif lastClassId ~= id then
			m_RunesList.button:setEnabled(false)
			return true
		end
	end
	
	m_RunesList.button:setEnabled(true)
end

m_RunesFunctions.combine = function()
	if not m_RunesList.button:isEnabled() then
		return true
	end
	
	if m_RunesFunctions.balance < 5000 then
		m_RunesList.balance:setColor('#F9000E')
		return scheduleEvent(function()
			if m_RunesList.balance then
				m_RunesList.balance:setColor('#846F60')
			end
		end, 2000)
	end
	
	m_RunesFunctions.send(0)
end

m_RunesFunctions.send = function(id)
	local msg = OutputMessage.create()
	msg:addU8(m_RunesFunctions.GameServerCombineRunes)
	msg:addU8(id)
	g_game.getProtocolGame():send(msg)
end

m_RunesFunctions.onInventoryChange = function(player, slot, item, oldItem, z, favorite)
	if not m_RunesList.window then
		return true
	end
	
	local widget = m_RunesList.window:getChildById('slot' .. slot)
	if item then
		widget:setItem(item)
		updateItemQuality(widget, item:getClassId())
	else
		if modules.game_lookitemmove and widget:getItem() then
			modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem(widget:getItem():getId())
		end
		
		widget:setItem(nil)
		widget:setImageClip('32 1753 34 35')
		widget:getChildById('corner'):hide()
	end
	
	m_RunesFunctions.updateConvert()
end