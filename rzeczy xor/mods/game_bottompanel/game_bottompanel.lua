m_MainList = {}
m_MainFunctions = {}

m_MainFunctions.open = function(var)
	local widget = m_MainList.window:getChildById('button' .. var)
	if widget then
		widget:getChildById('blink'):show()
		widget:getChildById('animatedSquare'):hide()
	end
end

m_MainFunctions.close = function(var)
	local widget = m_MainList.window:getChildById('button' .. var)
	if widget then
		widget:getChildById('blink'):hide()
	end
end

m_MainFunctions.blink = function(var)
	local widget = m_MainList.window:getChildById('button' .. var)
	if widget then
		widget:getChildById('animatedSquare'):show()
	end
end

m_MainFunctions.destroy = function()
	if m_MainList.event then
		m_MainList.event:cancel()
		m_MainList.event = nil
	end
	
	if m_MainList.window then
		m_MainList.window:hide()
	end
end

m_MainFunctions.execute = function()
	local player = g_game.getLocalPlayer()
	if not player then
		return false
	end
	
	for i = SkillStrength, SkillVigor do
		m_MainFunctions.onSkillChange(player, i, player:getSkillBaseLevel(i), player:getSkillLevel(i) - player:getSkillBaseLevel(i))
	end
end

m_MainFunctions.setVisiblePanel = function(visible)
	m_MainList.window:setVisible(visible)
	modules.game_interface.getRightPanel():setChecked(not visible)
end

m_MainFunctions.destroyHover = function()
	if m_MainList.hover then
		m_MainList.hover:destroy()
		m_MainList.hover = nil
	end
end

m_MainFunctions.onHoverChange = function(self, hovered)
	if not hovered then
		m_MainFunctions.destroyHover()
		return true
	end
	
	if not m_MainFunctions.tooltip then
		return true
	end
	
	m_MainList.hover = g_ui.createWidget('RightBottomPanelHoverWindow', modules.game_interface.getRootPanel())
	m_MainList.hover:setText(m_MainFunctions.tooltip)
	
	local pos = self:getPosition()
	if self.center then
		pos.x = pos.x + (self:getWidth() / 2) - (m_MainList.hover:getWidth() / 2)
		pos.y = pos.y - m_MainList.hover:getHeight() + 12
	else
		pos.x = pos.x - 9 - m_MainList.hover:getWidth() + self:getWidth()
		pos.y = pos.y - 9 - m_MainList.hover:getHeight() + self:getHeight()
	end
	
	m_MainList.hover:setPosition(pos)
end

m_MainFunctions.create = function()
	if m_MainList.window then
		m_MainList.window:show()
		return true
	end
	
	m_MainList.window = g_ui.createWidget('RightBottomPanelWindow', modules.game_interface.getRootPanel())
	m_MainList.window:setVisible(modules.client_options.getOption('displayRightBottomPanel'))
	m_MainList.balance = m_MainList.window:getChildById('balance')
	m_MainList.levelLabel = m_MainList.window:getChildById('level')
	m_MainList.skillWidget = m_MainList.window:getChildById('skillWidget')
	m_MainList.size = 284
end

m_MainFunctions.onSkillChange = function(player, skillId, baseSkill, bonusSkill)
	local widget = m_MainList.window:getChildById('skill' .. skillId)
	if widget then
		if bonusSkill > 0xFFF then
			bonusSkill = 0xFFF - bonusSkill
		end
		
		local color = '#FFFFFF'
		if bonusSkill > 0 then
			color = '#7CFC00'
		elseif bonusSkill < 0 then
			color = '#FF1414'
		end
		
		widget:setText(baseSkill + bonusSkill)
		widget:setColor(color)
	end
end

m_MainFunctions.onUpdateBalance = function(balance)
	m_MainList.balance:setText(g_game.getMilharNumber(balance))
end

function onGameStart()
	m_MainFunctions.create()
	
	local player = g_game.getLocalPlayer()
	if player then
		g_window.setTitle(g_app.getName() .. ' - ' .. player:getName())
	end
end

function onGameEnd()
	m_MainFunctions.destroy()
	
	g_window.setTitle(g_app.getName())
end

function onUpdateBalance(balance)
	m_MainFunctions.onUpdateBalance(balance)
end

function onLoad()
	g_ui.importStyle('game_bottompanel')
	m_MainFunctions.create()
	connect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart,
		onUpdateBalance = onUpdateBalance
	})
	
	if g_game.isOnline() then
		m_MainFunctions.execute()
	end
	
	g_keyboard.bindKeyDown('Ctrl+R', toggle)
end

function onUnload()
	m_MainFunctions.destroy()
	m_MainList = {}
	
	disconnect(g_game, {
		onGameEnd   = onGameEnd,
		onGameStart = onGameStart,
		onUpdateBalance = onUpdateBalance
	})
	
	g_keyboard.unbindKeyDown('Ctrl+R', toggle)
end

function toggle()
	local visible = not m_MainList.window:isVisible()
	modules.client_options.setOption('displayRightBottomPanel', visible)
	m_MainFunctions.setVisiblePanel(visible)
end
