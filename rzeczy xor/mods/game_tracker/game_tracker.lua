m_TrackerFunctions = {}
m_TrackerList = {}

m_TrackerFunctions.position = 0
m_TrackerFunctions.autoClearOldLoot = false
m_TrackerFunctions.autoClearLoot = false
m_TrackerFunctions.lockBoxEnabled = true
m_TrackerFunctions.ignoreList = {}

m_TrackerFunctions.MODE_TASK = 1
m_TrackerFunctions.MODE_BESTIARY = 2

m_TrackerFunctions.bestiaryMonsterList = {}
m_TrackerFunctions.taskMonsterList = {}
m_TrackerFunctions.trackerMode = m_TrackerFunctions.MODE_BESTIARY

m_TrackerFunctions.language = LANGUAGE_ENGLISH
m_TrackerFunctions.GameServerBestiaryKilled = 62
m_TrackerFunctions.ClientSendTrackList = 89

function onLoad()
	connect(g_game, { 
		onLoot = onLoot,
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
	
	m_TrackerFunctions.lockBoxEnabled = toboolean(g_settings.get('lootLock', true))
	m_TrackerFunctions.size = g_settings.getNode('TrackSize')
	
	if g_game.isOnline() then
		onGameStart()
	end
	
	m_TrackerFunctions.registerProtocol()
end

function onUnload()
	disconnect(g_game, {
		onLoot = onLoot,
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
	
	onGameEnd()
	m_TrackerFunctions.unregisterProtocol()
end

function onGameStart()
	m_TrackerFunctions.bestiaryMonsterList = {}
	m_TrackerFunctions.taskMonsterList = {}
	m_TrackerFunctions.loadLanguage()
	
	local list = modules.game_encyclopedia.m_BestiaryFunctions.getTrackList()
	if list then
		local tmpList = {}
		for k, v in pairs(list) do
			table.insert(tmpList, k)
		end
		
		if #tmpList > 0 then
			m_TrackerFunctions.sendTrackList(tmpList, 0)
		end
	end
	
	local localPlayer = g_game.getLocalPlayer()
	if localPlayer then
		m_TrackerFunctions.ignoreList = {}
		m_TrackerFunctions.name = localPlayer:getName()
		local list = g_settings.getNode('TrackIgnoreList' .. m_TrackerFunctions.name) or {}
		for k, v in pairs(list) do
			table.insert(m_TrackerFunctions.ignoreList, v)
		end
	end
end

function onGameEnd()
	m_TrackerFunctions.closeIgnoreList()
	m_TrackerFunctions.removeEvents()
	
	if m_TrackerList.window then
		m_TrackerFunctions.size = {position = m_TrackerList.window:getPosition(), window = m_TrackerList.window:getSize(), bestiaryList = m_TrackerList.bestiaryList:getSize(), lootList = m_TrackerList.lootList:getSize()}
		m_TrackerList.window:destroy()
	end
	
	m_TrackerList = {}
	m_TrackerFunctions.position = 0
	
	modules.client_options.setOption('autoClearLoot', m_TrackerFunctions.autoClearLoot, true)
	modules.client_options.setOption('autoClearOldLoot', m_TrackerFunctions.autoClearOldLoot, true)
	g_settings.set('lootLock', m_TrackerFunctions.lockBoxEnabled)
	
	if m_TrackerFunctions.size then
		g_settings.setNode('TrackSize', m_TrackerFunctions.size)
	end
	
	if not m_TrackerFunctions.name then
		m_TrackerFunctions.name = g_game.getLocalPlayer():getName()
	end
	
	if #m_TrackerFunctions.ignoreList > 0 then
		g_settings.setNode('TrackIgnoreList' .. m_TrackerFunctions.name, m_TrackerFunctions.ignoreList)
	end
end

function onLoot(list)
	if not modules.client_options.getOption('showLootMessage') then
		return true
	end
	
	local panel = nil
	if m_TrackerList.window then
		panel = m_TrackerList.lootList:getChildById('list')
		for _, widget in pairs(panel:getChildren()) do
			widget:setColor('#FFFFFF')
			widget:setText(widget.description or '')
		end
		
		if m_TrackerFunctions.autoClearOldLoot then
			panel:destroyChildren()
			m_TrackerList.list = {}
		end
	end
	
	for i = 1, #list do
		m_TrackerFunctions.addLoot(unpack(list[i]))
	end

	if not m_TrackerList.window then
		return true
	end
	
	if not panel then
		panel = m_TrackerList.lootList:getChildById('list')
	end
	
	m_TrackerList.window:setHeight(math.max(128, m_TrackerList.window:getHeight()))
end

m_TrackerFunctions.sendTrackList = function(list, add)
	local msg = OutputMessage.create()
	msg:addU8(m_TrackerFunctions.ClientSendTrackList)
	msg:addU8(add)
	msg:addU16(#list)
	for i = 1, #list do
		msg:addString(list[i])
	end
	
	g_game.getProtocolGame():send(msg)
end

m_TrackerFunctions.updateByBestiary = function(name, add)
	if not add then
		m_TrackerFunctions.update(name, false, 0)
	end
	
	m_TrackerFunctions.sendTrackList({name}, add and 0 or 1)
end

m_TrackerFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_TrackerFunctions.GameServerBestiaryKilled, m_TrackerFunctions.parseBestiaryKilled)
end

m_TrackerFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_TrackerFunctions.GameServerBestiaryKilled, m_TrackerFunctions.parseBestiaryKilled)
end

m_TrackerFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_TrackerFunctions.language = LANGUAGE_ENGLISH
	else
		m_TrackerFunctions.language = LANGUAGE_POLISH
	end
end

m_TrackerFunctions.lock = function(self)
	if not m_TrackerList.window then
		return false
	end
	
	if self then
		m_TrackerFunctions.lockBoxEnabled = not self:isOn()
		self:setOn(m_TrackerFunctions.lockBoxEnabled)
	end
	
	if not m_TrackerFunctions.lockBoxEnabled then
		m_TrackerList.window:setPhantom(true)
		m_TrackerList.window:setOpacity(0.7)
	else
		m_TrackerList.window:setPhantom(false)
		m_TrackerList.window:setOpacity(1.0)
	end
end

m_TrackerFunctions.isInIgnoreList = function(id)
	for i = 1, #m_TrackerFunctions.ignoreList do
		if m_TrackerFunctions.ignoreList[i] == tonumber(id) then
			return true
		end
	end
	
	return false
end

m_TrackerFunctions.removeFromIgnoreList = function(id)
	for i = 1, #m_TrackerFunctions.ignoreList do
		if m_TrackerFunctions.ignoreList[i] == tonumber(id) then
			table.remove(m_TrackerFunctions.ignoreList, i)
			m_TrackerFunctions.updateIgnoreList(id, false)
			break
		end
	end
end

m_TrackerFunctions.addToIgnoreList = function(id)
	if not m_TrackerFunctions.isInIgnoreList(id) then
		table.insert(m_TrackerFunctions.ignoreList, tonumber(id))
		m_TrackerFunctions.updateIgnoreList(id, true)
	end
end

m_TrackerFunctions.closeIgnoreList = function()
	if not m_TrackerList.ignoreWidnow then
		return true
	end
	
	if m_TrackerList.ignoreButton then
		m_TrackerList.ignoreButton:setOn(false)
	end
	
	m_TrackerList.ignoreWidnow:getChildById('list'):destroyChildren()
	m_TrackerList.ignoreWidnow:destroy()
	m_TrackerList.ignoreWidnow = nil
end

m_TrackerFunctions.updateIgnoreList = function(id, add)
	if not m_TrackerList.ignoreWidnow then
		return true
	end
	
	local list = m_TrackerList.ignoreWidnow:getChildById('list')
	local widget = list:getChildById(id)
	if add then
		if widget then
			return true
		end
		
		m_TrackerFunctions.addIgnoreItem(list, id)
	elseif not add then
		if not widget then
			return true
		end
		
		widget:destroy()
	end
end

m_TrackerFunctions.addIgnoreItem = function(parent, id)
	local widget = g_ui.createWidget('TrackerIgnoreItem', parent)
	widget:setId(id)
	widget:setText(modules.game_lookat.m_LookAtFunctions.getItemNameByClientId(id, m_TrackerFunctions.language))
	
	local item = widget:getChildById('item')
	item:setItemId(id)
	m_TrackerFunctions.updateMouseRelease(item)
end

m_TrackerFunctions.openIgnoreList = function(self)
	local v = not self:isOn()
	self:setOn(v)
	
	if v then
		m_TrackerList.ignoreWidnow = g_ui.displayUI('popup_ignore')
		
		local list = m_TrackerList.ignoreWidnow:getChildById('list')
		for _, v in pairs(m_TrackerFunctions.ignoreList) do
			m_TrackerFunctions.addIgnoreItem(list, v)
		end
	else
		if m_TrackerList.ignoreWidnow then
			m_TrackerList.ignoreWidnow:getChildById('list'):destroyChildren()
			m_TrackerList.ignoreWidnow:destroy()
			m_TrackerList.ignoreWidnow = nil
		end
	end
end

m_TrackerFunctions.updateMouseRelease = function(widget)
	widget.onMouseRelease = function(self, mousePosition, mouseButton)
		if m_TrackerList.cancelNextRelease then
			m_TrackerList.cancelNextRelease = false
			return false
		end
		
		if (g_mouse.isPressed(MouseLeftButton) and mouseButton == MouseRightButton) or (g_mouse.isPressed(MouseRightButton) and mouseButton == MouseLeftButton) then
			m_TrackerList.cancelNextRelease = true
			modules.game_lookat.m_LookAtFunctions.inspectItem(self:getItem(), classId, false, true)
			return true
		elseif mouseButton == MouseRightButton then
			local menu = g_ui.createWidget('PopupMenu')
			menu:setGameMenu(true)
			menu:addOption(tr('Look'), function() return modules.game_lookat.m_LookAtFunctions.inspectItem(self:getItem(), classId, false, true) end)
			menu:addOption(tr('Create chat widget'), function() g_game.createLookWindow(self:getItem()) end)
			
			local itemId = self:getItem():getId()
			if m_TrackerFunctions.isInIgnoreList(itemId) then
				menu:addOption(tr('Remove from ignore list'), function() return m_TrackerFunctions.removeFromIgnoreList(itemId) end)
			else
				menu:addOption(tr('Add to ignore list'), function() return m_TrackerFunctions.addToIgnoreList(itemId) end)
			end
			
			menu:display(mousePosition)
		end
		
		return false
	end
end

m_TrackerFunctions.addLoot = function(id, count, name, classId)
	if m_TrackerFunctions.isInIgnoreList(id) then
		return true
	end
	
	m_TrackerFunctions.open()
	
	local list = m_TrackerList.lootList:getChildById('list')
	local widget = list:getChildById(id)
	if not widget then
		widget = g_ui.createWidget('TrackerLootItem')
		widget:setId(id)
		list:insertChild(1, widget)
	else
		list:moveChildToIndex(widget, 1)
	end
	
	updateItemQuality(widget, classId)
	
	local found = true
	if not m_TrackerList.list[id] then
		m_TrackerList.list[id] = 0
		found = false
	end
	
	m_TrackerList.list[id] = m_TrackerList.list[id] + count
	
	local description = name .. ': ' .. (m_TrackerList.list[id] or count)
	widget:setText(description .. (found and ' (+' .. count .. ')' or ''))
	widget.description = description
	widget:setColor('#90EE90')
	
	local item = widget:getChildById('item')
	item:setItemId(id)
	item:getItem():setClassId(classId)
	m_TrackerFunctions.updateMouseRelease(item)
	
	if m_TrackerFunctions.autoClearLoot then
		m_TrackerFunctions.position = m_TrackerFunctions.position + 1
		local toPos = m_TrackerFunctions.position
		m_TrackerList.events[toPos] = scheduleEvent(function()
			if not m_TrackerList.events then
				return true
			end
			
			table.remove(m_TrackerList.events, toPos)
			
			if not m_TrackerList.window then
				m_TrackerList.list = {}
				return false
			end
			
			if not m_TrackerFunctions.autoClearLoot or not m_TrackerList.list[id] then
				return false
			end
			
			m_TrackerList.list[id] = m_TrackerList.list[id] - count
			
			local list = m_TrackerList.lootList:getChildById('list')
			local widget = list:getChildById(id)
			if m_TrackerList.list[id] < 1 then
				if widget then
					widget:destroy()
				end
				
				if #list:getChildren() < 1 then
					m_TrackerList.list = {}
				end
			elseif widget then
				widget:setText(name .. ': ' .. m_TrackerList.list[id])
			end
		end, 15000)
	end
end

m_TrackerFunctions.destroy = function()
	onGameEnd()
end

m_TrackerFunctions.removeEvents = function()
	if m_TrackerList.events then
		for i = 1, #m_TrackerList.events do
			removeEvent(m_TrackerList.events[i])
		end
	end
	
	m_TrackerList.events = {}
end

m_TrackerFunctions.setOption = function(checked)
	m_TrackerFunctions.autoClearLoot = checked
	
	if m_TrackerFunctions.autoClearLoot and m_TrackerList.window then
		if m_TrackerList.lootList then
			m_TrackerList.lootList:getChildById('list'):destroyChildren()
		end
		
		m_TrackerList.list = {}
	elseif not m_TrackerFunctions.autoClearLoot then
		m_TrackerFunctions.removeEvents()
	end
end

m_TrackerFunctions.setClearOption = function(checked)
	m_TrackerFunctions.autoClearOldLoot = checked
	
	local widget = m_TrackerList.window:getChildById('checkButton')
	if m_TrackerFunctions.autoClearOldLoot and m_TrackerList.window then
		if m_TrackerList.lootList then
			m_TrackerList.lootList:getChildById('list'):destroyChildren()
		end
		
		m_TrackerList.list = {}
	elseif not m_TrackerFunctions.autoClearOldLoot then
		m_TrackerFunctions.removeEvents()
	end
end

m_TrackerFunctions.open = function()
	if not m_TrackerList.window then
		m_TrackerList.window = g_ui.displayUI('game_tracker')
		m_TrackerList.window:getChildById('checkButton'):setChecked(m_TrackerFunctions.autoClearLoot)
		m_TrackerList.window:getChildById('cleanButton'):setChecked(m_TrackerFunctions.autoClearOldLoot)
		m_TrackerList.window:getChildById('lock'):setOn(m_TrackerFunctions.lockBoxEnabled)
		m_TrackerList.window:setup()
		m_TrackerList.window:open()
		
		m_TrackerList.list = {}
		m_TrackerList.events = {}
		
		m_TrackerList.bestiary = m_TrackerList.window:getChildById('bestiary')
		m_TrackerList.bestiaryList = m_TrackerList.window:getChildById('bestiaryList')
		m_TrackerList.bestiaryResize = m_TrackerList.bestiaryList:getChildById('resize')
		m_TrackerList.bestiaryResize:setMinimum(50)
		m_TrackerList.bestiaryResize:setMaximum(300)
		
		m_TrackerList.lootList = m_TrackerList.window:getChildById('lootList')
		m_TrackerList.lootResize = m_TrackerList.lootList:getChildById('resize')
		m_TrackerList.lootResize:setMinimum(30)
		m_TrackerList.lootResize:setMaximum(510)
		
		m_TrackerList.ignoreButton = m_TrackerList.window:getChildById('loot'):getChildById('ignoreButton')
		
		if m_TrackerFunctions.size then
			m_TrackerList.window:setSize(m_TrackerFunctions.size.window)
			m_TrackerList.window:setPosition(m_TrackerFunctions.size.position)
			m_TrackerList.bestiaryList:setSize(m_TrackerFunctions.size.bestiaryList)
			m_TrackerList.lootList:setSize(m_TrackerFunctions.size.lootList)
			
			if m_TrackerFunctions.size.bestiaryList.height == 0 then
				m_TrackerList.window:getChildById('bestiary'):getChildById('button'):setOn(true)
			end
			
			if m_TrackerFunctions.size.lootList.height == 0 then
				m_TrackerList.window:getChildById('loot'):getChildById('button'):setOn(true)
			end
		end
		
		m_TrackerFunctions.lock()
		m_TrackerFunctions.updateTrack()
	end
end

m_TrackerFunctions.sort = function(k, v)
	return k[3] > v[3]
end

m_TrackerFunctions.updateTrack = function()
	if m_TrackerFunctions.trackerMode == m_TrackerFunctions.MODE_TASK then
		local panel = m_TrackerList.bestiaryList:getChildById('list')
		for name, var in pairs(m_TrackerFunctions.taskMonsterList) do
			local v = modules.game_task.m_TaskFunctions.getTaskByName(name)[var[2]]
			local widget = m_TrackerFunctions.addBestiary(panel, name, v.class[m_TrackerFunctions.language], v.count)
			m_TrackerFunctions.updateCount(widget, panel, name, m_TrackerFunctions.taskMonsterList)
		end
	else
		local list = modules.game_encyclopedia.m_BestiaryFunctions.getTrackList()
		local tmpList = {}
		for k, v in pairs(list) do
			if v then
				local var = modules.game_lookat.m_LookAtFunctions.getMonsterByName(k)
				if var then
					local amount = m_TrackerFunctions.bestiaryMonsterList[k] or 0
					table.insert(tmpList, {k, amount, amount / var.killAmount[3]})
				end
			end
		end
		
		table.sort(tmpList, m_TrackerFunctions.sort)
		for i = 1, #tmpList do
			m_TrackerFunctions.update(tmpList[i][1], true, tmpList[i][2])
		end
	end
end

m_TrackerFunctions.expand = function(self)
	local v = not self:isOn()
	self:setOn(v)
	
	local id = self:getParent():getId() .. 'List'
	local panel = m_TrackerList.window:getChildById(id)
	if v then
		panel.height = panel:getHeight()
		panel:setHeight(0)
	else
		panel:setHeight(panel.height or (panel:getChildCount() * 30))
	end
	
	panel:setOn(v)
end

m_TrackerFunctions.addBestiary = function(panel, id, name, amount)
	local widget = g_ui.createWidget('TrackerBestiaryMonster', panel)
	widget:getChildById('name'):setText(name)
	widget:setId(id)
	widget.amount = amount
	return widget
end

m_TrackerFunctions.update = function(name, add, amount)
	if not m_TrackerList.window then
		m_TrackerFunctions.open()
	end
	
	if add then
		m_TrackerFunctions.bestiaryMonsterList[name] = amount
	else
		m_TrackerFunctions.bestiaryMonsterList[name] = nil
	end
	
	if m_TrackerFunctions.trackerMode ~= m_TrackerFunctions.MODE_BESTIARY then
		return true
	end
	
	local v = modules.game_lookat.m_LookAtFunctions.getMonsterByName(name)
	if not v then
		return true
	end
	
	local panel = m_TrackerList.bestiaryList:getChildById('list')
	local widget = panel:getChildById(name)
	if widget then
		if not add then
			widget:destroy()
			return true
		end
	elseif not add then
		return true
	end
	
	if not widget then
		widget = m_TrackerFunctions.addBestiary(panel, name, v.description[m_TrackerFunctions.language], v.killAmount[3])
		widget.onMouseRelease = m_TrackerFunctions.onMouseRelease
	end
	
	m_TrackerFunctions.updateCount(widget, panel, name, m_TrackerFunctions.bestiaryMonsterList)
end

m_TrackerFunctions.onMouseRelease = function(self, mousePosition, mouseButton)
	if mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		menu:addOption(tr('Remove'), function() modules.game_encyclopedia.m_BestiaryFunctions.setTrack(false, self:getId()) end)
		menu:addOption(tr('Go to bestiary'), function() modules.game_encyclopedia.m_BestiaryFunctions.selectMonsterByName(self:getId(), modules.game_lookat.m_LookAtFunctions.getMonsterByName(self:getId()).classId) end)
		menu:display(mousePosition)
		return true
	end
end

m_TrackerFunctions.updateCount = function(self, panel, name, list)
	local widget = self:getChildById('amount')
	local amount = list[name] or 0
	if type(amount) == 'table' then
		amount = amount[1]
	end
	
	local v = amount / self.amount
	widget:setText(amount .. ' / ' .. self.amount)
	
	local widget = self:getChildById('progress')
	widget:setImageClip('980 2807 ' .. math.ceil(math.min(160, 160 * v)) .. ' 25')
	
	local width = m_TrackerList.window:getWidth() - 30
	widget:setWidth(math.ceil(math.min(width, width * v)))
	
	local id = panel:getChildIndex(self)
	if id > 1 then
		for i = id, 1, -1 do
			local widget = panel:getChildByIndex(i)
			local amount = list[widget:getId()] or 0
			if type(amount) == 'table' then
				amount = amount[1]
			end
			
			if v >= amount / widget.amount then
				panel:moveChildToIndex(self, i)
			end
		end
	end
end

m_TrackerFunctions.onBestiaryListChange = function(self)
	if m_TrackerList.window:getWidth() ~= 240 then
		m_TrackerList.window:setWidth(240)
	end
	
	local height = 114 + m_TrackerList.lootList:getHeight() + m_TrackerList.bestiaryList:getHeight()
	m_TrackerList.window:setHeight(height)
end

m_TrackerFunctions.updateByTask = function(name, taskId, amount)
	if not m_TrackerList.window then
		m_TrackerFunctions.open()
	end

	m_TrackerFunctions.taskMonsterList[name] = {amount, taskId}
	if m_TrackerFunctions.trackerMode ~= m_TrackerFunctions.MODE_TASK then
		return true
	end
	
	local panel = m_TrackerList.bestiaryList:getChildById('list')
	local widget = panel:getChildById(name)
	if not widget then
		local v = modules.game_task.m_TaskFunctions.getTaskByName(name)[taskId]
		widget = m_TrackerFunctions.addBestiary(panel, name, v.class[m_TrackerFunctions.language], v.count)
		widget.onMouseRelease = m_TrackerFunctions.onMouseRelease
	end
	
	m_TrackerFunctions.updateCount(widget, panel, name, m_TrackerFunctions.taskMonsterList)
end

m_TrackerFunctions.manageTracker = function(self)
	local panel = m_TrackerList.bestiaryList:getChildById('list')
	panel:destroyChildren()
	if m_TrackerFunctions.trackerMode == m_TrackerFunctions.MODE_BESTIARY then
		m_TrackerFunctions.trackerMode = m_TrackerFunctions.MODE_TASK
		self:setOn(true)
		m_TrackerList.bestiary:setOn(true)
		
		for name, var in pairs(m_TrackerFunctions.taskMonsterList) do
			local v = modules.game_task.m_TaskFunctions.getTaskByName(name)[var[2]]
			local widget = m_TrackerFunctions.addBestiary(panel, name, v.class[m_TrackerFunctions.language], v.count)
			m_TrackerFunctions.updateCount(widget, panel, name, m_TrackerFunctions.taskMonsterList)
		end
	elseif m_TrackerFunctions.trackerMode == m_TrackerFunctions.MODE_TASK then
		m_TrackerFunctions.trackerMode = m_TrackerFunctions.MODE_BESTIARY
		self:setOn(false)
		m_TrackerList.bestiary:setOn(false)
		
		for name, amount in pairs(m_TrackerFunctions.bestiaryMonsterList) do
			if amount then
				local v = modules.game_lookat.m_LookAtFunctions.getMonsterByName(name)
				local widget = m_TrackerFunctions.addBestiary(panel, name, v.description[m_TrackerFunctions.language], v.killAmount[3])
				widget.onMouseRelease = m_TrackerFunctions.onMouseRelease
				m_TrackerFunctions.updateCount(widget, panel, name, m_TrackerFunctions.bestiaryMonsterList)
			end
		end
	end
end

m_TrackerFunctions.destroyTaskWidget = function(name)
	m_TrackerFunctions.taskMonsterList[name] = {}
	if m_TrackerList.bestiaryList then
		local widget = m_TrackerList.bestiaryList:getChildById('list'):getChildById(name)
		if widget then
			widget:destroy()
		end
	end
end

m_TrackerFunctions.parseBestiaryKilled = function(protocol, msg)
	local id = msg:getU8()
	if id == 1 then -- single tracker monster
		local name = msg:getString()
		local amount = msg:getU32()
		m_TrackerFunctions.update(name, true, amount)
	elseif id == 2 then -- pack of tracker monsters
		local size = msg:getU16()
		for i = 1, size do
			local name = msg:getString()
			local amount = msg:getU32()
			m_TrackerFunctions.update(name, true, amount)
		end
	elseif id == 3 then -- task single id
		local name = msg:getString()
		local taskId = msg:getU16()
		local amount = msg:getU32()
		if taskId == 0 then
			m_TrackerFunctions.destroyTaskWidget(name)
		else
			m_TrackerFunctions.updateByTask(name, taskId, amount)
		end
	end
end