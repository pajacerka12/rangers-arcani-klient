m_SpellBarFunctions = {}
m_SpellBarList = {}

m_SpellBarFunctions.hotkeysCache = {}
m_SpellBarFunctions.potionList = {0, 0}

m_SpellBarFunctions.visibleBars = true

m_SpellBarFunctions.GameServerUpdatePotions = 153

m_SpellBarFunctions.POSITION_VERTICAL = 0
m_SpellBarFunctions.POSITION_HORIZONTAL = 1

m_SpellBarFunctions.USENONE = 0
m_SpellBarFunctions.USEONSELF = 1
m_SpellBarFunctions.USEONTARGET = 2
m_SpellBarFunctions.USEWITH = 3
m_SpellBarFunctions.USEONCREATURE = 4

m_SpellBarFunctions.SIZE_MINIMIZE = -1
m_SpellBarFunctions.SIZE_MAXIMIZE = 1

m_SpellBarFunctions.currentPage = 1

m_SpellBarFunctions.supportSpells = {'lay on hands', 'blessing of the earth', 'blessing of heavens', 'sacrifice', 'minor heal friend', 'wild instinct', 'source of life', 'quick heal', 'heal friend', 'healing bomb'}

m_SpellBarFunctions.selectEffect = {}

m_SpellBarFunctions.changePage = function(prev)
	m_SpellBarList.spellData.points = math.max(1, m_SpellBarList.spellData.points or 0)
	if prev then
		if m_SpellBarList.spellData.points <= 1 then
			return true
		end
		
		m_SpellBarList.nextButton:setEnabled(true)
		m_SpellBarList.spellData.points = m_SpellBarList.spellData.points - 1
		if m_SpellBarList.spellData.points <= 1 then
			m_SpellBarList.prevButton:setEnabled(false)
		end
	else
		if m_SpellBarList.spellData.points >= m_SpellBarList.spellData.spellLevel then
			return true
		end
		
		m_SpellBarList.prevButton:setEnabled(true)
		m_SpellBarList.spellData.points = m_SpellBarList.spellData.points + 1
		if m_SpellBarList.spellData.points >= m_SpellBarList.spellData.spellLevel then
			m_SpellBarList.nextButton:setEnabled(false)
		end
	end
	
	local data = modules.game_character.m_SpellsFunctions.getData(m_SpellBarList.spellData.name, SpelllistSettings[m_SpellBarList.spellData.name], m_SpellBarList.spellData.points)
	m_SpellBarList.page:setText('Lv ' .. m_SpellBarList.spellData.points)
	m_SpellBarFunctions.createLookSpell(data, true)
end

m_SpellBarFunctions.setVisibleBars = function(visible)
	m_SpellBarFunctions.visibleBars = visible
	
	if g_game.isOnline() then
		m_SpellBarList.background:setVisible(visible)
		m_SpellBarList.experienceBackground:setVisible(visible)
		m_SpellBarList.experienceFrame:setVisible(visible)
		m_SpellBarList.experienceLabel:setVisible(visible)
		m_SpellBarList.goodPanel:setVisible(visible)
		m_SpellBarList.badPanel:setVisible(visible)
		m_SpellBarList.manaWidget:setVisible(visible)
		m_SpellBarList.manaPotion:setVisible(visible)
		m_SpellBarList.manaPotionAmount:setVisible(visible)
		m_SpellBarList.healthWidget:setVisible(visible)
		m_SpellBarList.healthPotion:setVisible(visible)
		m_SpellBarList.healthPotionAmount:setVisible(visible)
		m_SpellBarList.barrerWidget:setVisible(visible)
		
		m_SpellBarList.window:getChildById('image'):setOn(not visible)
		m_SpellBarList.window:getChildById('rotate'):setOn(not visible)
		if visible then
			m_SpellBarList.window:setHeight(m_SpellBarList.window:getHeight() + 68)
		else
			m_SpellBarList.window:setHeight(m_SpellBarList.window:getHeight() - 68)
		end
	end
end

m_SpellBarFunctions.currentRotate = m_SpellBarFunctions.POSITION_HORIZONTAL
m_SpellBarFunctions.config = {
	spellsInLine = 12,
	maxAttacks = 14,
	iconSize = 32,
	separator = 4,
	pages = 3,
	notManaColor = '#0D405E'
}

m_SpellBarFunctions.updateCooldown = function(name, cooldown, isLogin)
	local attack = m_SpellBarFunctions.getAttackByName(name, true)
	if attack then
		if cooldown == 0 then
			m_SpellBarFunctions.clearCooldown(attack)
		else
			if isLogin or (cooldown >= (attack.data.m_cooldown or 0)) then
				attack.data.m_cooldown = cooldown
				
				m_SpellBarFunctions.setIconImageType(attack.widget:getChildById('icon'), attack.data.iconId, true)
				attack.widget:getChildById('progress'):setPercent(100)
				attack.data.m_realCooldown = attack.data.cooldown * 10
				
				if not m_SpellBarList.cooldownList then
					m_SpellBarList.cooldownList = {}
				end
				
				if not isInArray(m_SpellBarList.cooldownList, name) then
					table.insert(m_SpellBarList.cooldownList, name)
				end
				
				if not m_SpellBarList.cooldownEvent then
					m_SpellBarList.cooldownEvent = cycleEvent(checkCooldown, 100)
				end
			end
		end
	end
end

m_SpellBarFunctions.destroyHover = function()
	if m_SpellBarList.hover then
		m_SpellBarList.hover:destroy()
		m_SpellBarList.hover = nil
	end
end

m_SpellBarFunctions.onMouseRelease = function(widget, mousePosition, mouseButton)
	if mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		menu:addOption(tr('Reset experience counter'), function() m_SpellBarFunctions.resetExperience() end)
		menu:display(mousePosition)
	end
end

m_SpellBarFunctions.onHoverChange = function(self, hovered)
	if not hovered then
		m_SpellBarFunctions.destroyHover()
		return true
	end
	
	if not m_SpellBarFunctions.tooltip then
		return true
	end
	
	m_SpellBarList.hover = g_ui.createWidget('RightBottomPanelHoverWindow', modules.game_interface.getRootPanel())
	m_SpellBarList.hover:setText(m_SpellBarFunctions.tooltip)
	
	local pos = self:getPosition()
	if self.center then
		pos.x = pos.x + (self:getWidth() / 2) - (m_SpellBarList.hover:getWidth() / 2)
		pos.y = pos.y - m_SpellBarList.hover:getHeight() + 12
	else
		pos.x = pos.x - 9 - m_SpellBarList.hover:getWidth() + self:getWidth()
		pos.y = pos.y - 9 - m_SpellBarList.hover:getHeight() + self:getHeight()
	end
	
	m_SpellBarList.hover:setPosition(pos)
end

m_SpellBarFunctions.onStatesChange = function(localPlayer, now, old, id, description, removeIcon, iconType, delay)
	if m_SpellBarList.goodPanel and m_SpellBarList.badPanel then
		modules.game_healthinfo.m_HealthInfoFunctions.addIcon(now, id, description, removeIcon, iconType, delay, {m_SpellBarList.window, m_SpellBarList.goodPanel, m_SpellBarList.badPanel, m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL})
	end
end

m_SpellBarFunctions.parseUpdatePotions = function(protocol, msg)
	local id = msg:getU8()
	local amount = msg:getU32()
	
	m_SpellBarFunctions.potionList[id] = amount
	if m_SpellBarList.manaPotionAmount and m_SpellBarList.healthPotionAmount then
		if id == 1 then
			m_SpellBarList.manaPotionAmount:setText(amount)
		else
			m_SpellBarList.healthPotionAmount:setText(amount)
		end
	end
end

m_SpellBarFunctions.clearCooldown = function(attack)
	attack.widget:getChildById('cooldown'):clearText()
	attack.widget:getChildById('progress'):setPercent(100)
	attack.data.m_cooldown = nil
	m_SpellBarFunctions.setManaShadow(attack, attack.data.iconId, attack.data.mana > g_game.getLocalPlayer():getMana())
end

function checkCooldown()
	if not m_SpellBarList.cooldownList or #m_SpellBarList.cooldownList == 0 then
		if m_SpellBarList.cooldownEvent then
			m_SpellBarList.cooldownEvent:cancel()
			m_SpellBarList.cooldownEvent = nil
		end
		
		m_SpellBarList.cooldownList = {}
		return true
	end
	
	for i = #m_SpellBarList.cooldownList, 1, -1 do
		local attack = m_SpellBarFunctions.getAttackByName(m_SpellBarList.cooldownList[i], true)
		if attack then
			if attack.data.m_cooldown and attack.data.m_cooldown > 0 then
				attack.data.m_cooldown = attack.data.m_cooldown - 1
				
				local cooldown = attack.data.m_cooldown / 10
				if cooldown >= 60 then
					attack.widget:getChildById('cooldown'):setText('<' .. math.ceil(cooldown / 60) .. 'm')
				else
					if attack.data.m_cooldown % 10 == 0 or attack.data.m_cooldown % 10 > 9 then
						attack.widget:getChildById('cooldown'):setText(cooldown .. '.0')
					else
						attack.widget:getChildById('cooldown'):setText(cooldown)
					end
				end

				local percent = math.ceil(attack.data.m_cooldown / attack.data.m_realCooldown * 100)
				attack.widget:getChildById('progress'):setPercent(100 - percent)
			else
				m_SpellBarFunctions.clearCooldown(attack)
				table.remove(m_SpellBarList.cooldownList, i)
			end
		end
	end
end

m_SpellBarFunctions.maximize = function()
	m_SpellBarFunctions.currentPage = m_SpellBarFunctions.currentPage + 1
	
	if m_SpellBarFunctions.currentPage >= m_SpellBarFunctions.config.pages then
		m_SpellBarList.maximize:hide()
		m_SpellBarFunctions.currentPage = m_SpellBarFunctions.config.pages
	end
	
	m_SpellBarList.minimize:show()
	
	if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
		m_SpellBarList.window:setWidth(m_SpellBarList.window:getWidth() + m_SpellBarFunctions.config.iconSize + m_SpellBarFunctions.config.separator)
	else
		m_SpellBarList.window:setHeight(m_SpellBarList.window:getHeight() + m_SpellBarFunctions.config.iconSize + m_SpellBarFunctions.config.separator)
	end
	
	for id = 1, m_SpellBarFunctions.currentPage * m_SpellBarFunctions.config.maxAttacks do
		local attack = m_SpellBarFunctions.getSpellById(id)
		if attack then
			attack.widget:show()
			if id > 1 and id % m_SpellBarFunctions.config.maxAttacks == 1 then
				if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
					attack.widget:addAnchor(AnchorLeft, 'spellIcon' .. (id - 1), AnchorRight)
				else
					attack.widget:addAnchor(AnchorBottom, 'spellIcon' .. (id - 1), AnchorTop)
				end
			elseif id > m_SpellBarFunctions.config.maxAttacks then
				local iconId = 1
				for i = m_SpellBarFunctions.config.pages, 1, -1 do
					if id > i * m_SpellBarFunctions.config.maxAttacks then
						iconId = i * m_SpellBarFunctions.config.maxAttacks
						break
					end
				end
				
				if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
					attack.widget:addAnchor(AnchorLeft, 'spellIcon' .. iconId, AnchorRight)
				else
					attack.widget:addAnchor(AnchorBottom, 'spellIcon' .. iconId, AnchorTop)
				end
			end
		end
	end
end

m_SpellBarFunctions.minimize = function()
	m_SpellBarFunctions.currentPage = m_SpellBarFunctions.currentPage - 1
	if m_SpellBarFunctions.currentPage <= 1 then
		m_SpellBarList.minimize:hide()
		m_SpellBarFunctions.currentPage = 1
	end
	
	m_SpellBarList.maximize:show()
	
	if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
		m_SpellBarList.window:setWidth(m_SpellBarList.window:getWidth() - m_SpellBarFunctions.config.iconSize - m_SpellBarFunctions.config.separator)
	else
		m_SpellBarList.window:setHeight(m_SpellBarList.window:getHeight() - m_SpellBarFunctions.config.iconSize - m_SpellBarFunctions.config.separator)
	end
	
	for id = m_SpellBarFunctions.currentPage * m_SpellBarFunctions.config.maxAttacks + 1, (m_SpellBarFunctions.currentPage + 1) * m_SpellBarFunctions.config.maxAttacks do
		local attack = m_SpellBarFunctions.getSpellById(id)
		if attack then
			attack.widget:hide()
			if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
				attack.widget:addAnchor(AnchorLeft, 'parent', AnchorLeft)
			else
				attack.widget:addAnchor(AnchorBottom, 'parent', AnchorBottom)
			end
		end
	end
end

m_SpellBarFunctions.onChangeSize = function(sizeMode)
	if sizeMode == m_SpellBarFunctions.SIZE_MINIMIZE then
		m_SpellBarFunctions.minimize()
	else
		m_SpellBarFunctions.maximize()
	end
end

m_SpellBarFunctions.isSupportSpellByName = function(name)
	local attack = m_SpellBarFunctions.getAttackByName(name, true)
	if not attack then
		return false
	end
	
	if attack.data.m_cooldown then
		return true
	end
	
	return isInArray(m_SpellBarFunctions.supportSpells, attack.data.name)
end

m_SpellBarFunctions.onClickSpell = function(name, var, direction)
	if m_SpellBarList.descriptionWindow then
		m_SpellBarList.descriptionWindow:getChildById('list'):destroyChildren()
		m_SpellBarList.descriptionWindow:destroy()
		m_SpellBarList.descriptionWindow = nil
	end
	
	if name ~= '' then
		local attack = m_SpellBarFunctions.getAttackByName(name, true)
		if not attack then
			return true
		end
		
		if attack.data.m_cooldown and attack.data.m_cooldown > 0 then
			return true
		end
		
		if (not var or var.useType ~= m_SpellBarFunctions.USEONSELF) and attack.data.iconId and isInArray(m_SpellBarFunctions.supportSpells, attack.data.name) then
			local selectedMember = modules.game_partypanel.getSelectedMember()
			if selectedMember then
				var = {}
				var.useType = m_SpellBarFunctions.USEWITH
				var.pos = nil
				var.target = nil
				var.id = selectedMember
			end
		end
		
		if m_SpellBarList.descriptionWindow then
			m_SpellBarList.descriptionWindow:getChildById('list'):destroyChildren()
			m_SpellBarList.descriptionWindow:destroy()
			m_SpellBarList.descriptionWindow = nil
		end
		
		local effect = m_SpellBarFunctions.selectEffect[name] or 1
		if not var then
			g_game.sendAttackCast(name, m_SpellBarFunctions.USENONE, effect)
		elseif var.useType == m_SpellBarFunctions.USEONSELF then
			g_game.sendAttackCast(name, m_SpellBarFunctions.USEONSELF, effect)
		elseif var.useType == m_SpellBarFunctions.USEONTARGET then
			g_game.sendAttackCast(name, m_SpellBarFunctions.USEONTARGET, effect)
		elseif var.useType == m_SpellBarFunctions.USEWITH then
			if var.pos then
				g_game.sendAttackCastToPosition(name, m_SpellBarFunctions.USEWITH, var.pos, direction, effect)
			elseif var.target then
				g_game.sendAttackCastOnCreature(name, m_SpellBarFunctions.USEONCREATURE, var.target, direction, effect)
			elseif var.id then
				g_game.sendAttackCastOnCreatureById(name, m_SpellBarFunctions.USEONCREATURE, var.id, direction, effect)
			end
		end
	end
end

m_SpellBarFunctions.clearAttack = function(id)
	local attack = m_SpellBarFunctions.getSpellById(id)
	if not attack then
		print('Spell id: ' .. id .. ' not found.')
		return false
	end
	
	if attack.data and attack.data.name ~= '' then
		m_SpellBarFunctions.hotkeysCache[attack.data.name] = 0
	end
	
	attack.data = {}
	attack.data.name = ''
	attack.data.iconId = 0
	attack.widget.hotkey = nil
	attack.widget:getChildById('cooldown'):clearText()
	attack.widget:getChildById('hotkey'):clearText()
	attack.widget:getChildById('icon'):setImageColor('#FFFFFFFF')
	attack.widget:getChildById('icon'):setIcon(nil)
	attack.widget:getChildById('progress'):setPercent(100)
	m_SpellBarFunctions.setIconImageType(attack.widget:getChildById('icon'), 0)
end

m_SpellBarFunctions.getAttackByName = function(name, checkAll)
	if checkAll then
		for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
			local attack = m_SpellBarFunctions.getSpellById(id)
			if attack and attack.data.name:lower() == name:lower() then
				return attack
			end
		end
	
		return false
	end
	
	for id = (m_SpellBarFunctions.currentPage - 1) * m_SpellBarFunctions.config.maxAttacks + 1, (m_SpellBarFunctions.currentPage - 1) * m_SpellBarFunctions.config.maxAttacks + m_SpellBarFunctions.config.maxAttacks do
		local attack = m_SpellBarFunctions.getSpellById(id)
		if attack and attack.data.name:lower() == name:lower() then
			return attack
		end
	end
	
	return nil
end

m_SpellBarFunctions.getIconByPosition = function()
	local mousePos = g_window.getMousePosition()
	if m_SpellBarFunctions.currentPage > 1 then
		for id = 1, m_SpellBarFunctions.currentPage * m_SpellBarFunctions.config.maxAttacks do
			local attack = m_SpellBarFunctions.getSpellById(id)
			if mousePos.x > attack.widget:getX() and mousePos.x < attack.widget:getX() + attack.widget:getWidth() and
				mousePos.y > attack.widget:getY() and mousePos.y < attack.widget:getY() + attack.widget:getHeight() then
				return attack
			end
		end
	else
		for id = (m_SpellBarFunctions.currentPage - 1) * m_SpellBarFunctions.config.maxAttacks + 1, (m_SpellBarFunctions.currentPage - 1) * m_SpellBarFunctions.config.maxAttacks + m_SpellBarFunctions.config.maxAttacks do
			local attack = m_SpellBarFunctions.getSpellById(id)
			if mousePos.x > attack.widget:getX() and mousePos.x < attack.widget:getX() + attack.widget:getWidth() and
				mousePos.y > attack.widget:getY() and mousePos.y < attack.widget:getY() + attack.widget:getHeight() then
				return attack
			end
		end
	end
	
	return false
end

m_SpellBarFunctions.getAttackByNameId = function(name, checkAll)
	if checkAll then
		for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
			local attack = m_SpellBarFunctions.getSpellById(id)
			if attack and attack.widget:getId() == name then
				return attack
			end
		end
	
		return false
	end
	
	for id = (m_SpellBarFunctions.currentPage - 1) * m_SpellBarFunctions.config.maxAttacks + 1, (m_SpellBarFunctions.currentPage - 1) * m_SpellBarFunctions.config.maxAttacks + m_SpellBarFunctions.config.maxAttacks do
		local attack = m_SpellBarFunctions.getSpellById(id)
		if attack and attack.widget:getId() == name then
			return attack
		end
	end
	
	return nil
end

m_SpellBarFunctions.setIconImageType = function(widget, id, black)
	if black then
		widget:setImageSource('/images/leaf/spellWindow/spellIcons_black')
	else
		widget:setImageSource('/images/leaf/spellWindow/spellIcons')
	end
	
	widget:setImageClip(m_SpellBarFunctions.getImageClip(id))
end

m_SpellBarFunctions.setManaShadow = function(attack, id, enoughMana)
	m_SpellBarFunctions.setIconImageType(attack.widget:getChildById('icon'), id, enoughMana)
	attack.widget:getChildById('icon'):setImageColor(enoughMana and m_SpellBarFunctions.config.notManaColor or '#FFFFFFFF')
end

m_SpellBarFunctions.getSpellById = function(id)
	return m_SpellBarList.attackConfigs[id]
end

m_SpellBarFunctions.getImageClip = function(id)
	return (id < m_SpellBarFunctions.config.spellsInLine and id or id - (math.floor(id / m_SpellBarFunctions.config.spellsInLine) * m_SpellBarFunctions.config.spellsInLine)) * m_SpellBarFunctions.config.iconSize .. ' ' .. (math.floor(id / m_SpellBarFunctions.config.spellsInLine)) * m_SpellBarFunctions.config.iconSize .. ' ' .. m_SpellBarFunctions.config.iconSize .. ' ' .. m_SpellBarFunctions.config.iconSize
end

m_SpellBarFunctions.updateSpellPosition = function()
	m_SpellBarList.updatePosition = true
end

m_SpellBarFunctions.updateSpellShadowMouseRelease = function(data)
	if m_SpellBarList.updatePosition then
		m_SpellBarList.updatePosition = nil
		m_SpellBarList.updateDataSpell = data
		
		for _, v in pairs(m_SpellBarList.attackConfigs) do
			v.widget:getChildById('icon'):setImageColor('#FFFFFFFF')
		end
		
		if m_SpellBarList.currentSpellCell then
			for _, v in pairs(m_SpellBarList.attackConfigs) do
				if v.data.name == data.name then
					local id = v.widget:getId()
					id = id:gsub('spellIcon', '')
					m_SpellBarFunctions.clearAttack(tonumber(id))
				end
			end
			
			local id = m_SpellBarList.currentSpellCell:getId()
			id = id:gsub('spellIcon', '')
			m_SpellBarFunctions.parseAttack(tonumber(id), m_SpellBarList.updateDataSpell)
			
			m_SpellBarList.updateDataSpell = nil
			m_SpellBarList.currentSpellCell = nil
		end
	end
end

m_SpellBarFunctions.reset = function()
	for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		local attack = m_SpellBarFunctions.getSpellById(id)
		if attack then
			attack.widget.hotkey = false
			attack.widget:getChildById('icon'):setIcon(nil)
			attack.widget:getChildById('hotkey'):clearText()
		end
	end
end

m_SpellBarFunctions.updateHotkey = function(name, keyCombo)
	local attack = m_SpellBarFunctions.getAttackByName(name, true)
	if not attack then
		return true
	end
	
	attack.widget.hotkey = keyCombo
	if keyCombo and keyCombo ~= '' then
		keyCombo = string.gsub(keyCombo, 'Shift', 'Sh')
		keyCombo = string.gsub(keyCombo, 'Ctrl', 'Ct')
	else
		attack.widget:getChildById('icon'):setIcon(nil)
	end
	
	attack.widget:getChildById('hotkey'):setText(keyCombo)
end

m_SpellBarFunctions.onSpellBarHover = function(self, hovered)
	if m_SpellBarList.updatePosition then
		if hovered then
			self:getChildById('icon'):setImageColor('#006400')
			m_SpellBarList.currentSpellCell = self
		else
			self:getChildById('icon'):setImageColor('#FFFFFFFF')
			m_SpellBarList.currentSpellCell = nil
		end
	end
end

m_SpellBarFunctions.hideInfo = function()
	m_SpellBarList.spellInfoWindow:hide()
	m_SpellBarList.currentSpell = nil
end

m_SpellBarFunctions.onRotate = function()
	local var = {}
	for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		local attack = m_SpellBarFunctions.getSpellById(id)
		if attack.data.name ~= '' then
			table.insert(var, attack.data)
		else
			table.insert(var, {name = ''})
		end
	end
	
	if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
		m_SpellBarFunctions.currentRotate = m_SpellBarFunctions.POSITION_HORIZONTAL
	else
		m_SpellBarFunctions.currentRotate = m_SpellBarFunctions.POSITION_VERTICAL
	end
	
	m_SpellBarFunctions.destroy()
	m_SpellBarList.attackConfigs = {}
	m_SpellBarFunctions.create(true)
	
	for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		local v = var[id]
		if v.name ~= '' then
			m_SpellBarFunctions.parseAttack(id, v)
			m_SpellBarFunctions.updateCooldown(v.name, v.m_cooldown or 0)
		end
	end
end

m_SpellBarFunctions.replace = function(from, to)
	local attack_from = table.copy(m_SpellBarList.attackConfigs[from])
	local attack_to = table.copy(m_SpellBarList.attackConfigs[to])

	m_SpellBarFunctions.clearAttack(from)
	m_SpellBarFunctions.clearAttack(to)
	if attack_from.data.name ~= '' then
		m_SpellBarFunctions.parseAttack(to, attack_from.data)
	end
	
	if attack_to.data.name ~= '' then
		m_SpellBarFunctions.parseAttack(from, attack_to.data)
	end

	local mana = g_game.getLocalPlayer():getMana()
	if attack_from.data.m_cooldown then
		attack_to.widget:getChildById('cooldown'):setText(attack_from.data.m_cooldown)
		m_SpellBarFunctions.setIconImageType(attack_to.widget:getChildById('icon'), attack_from.data.iconId, true)
	else
		m_SpellBarFunctions.setManaShadow(attack_to, attack_from.data.iconId, attack_from.data.mana > mana)
	end
	
	if attack_to.data.mana then
		if attack_to.data.m_cooldown then
			attack_from.widget:getChildById('cooldown'):setText(attack_to.data.m_cooldown)
			m_SpellBarFunctions.setIconImageType(attack_from.widget:getChildById('icon'), attack_to.data.iconId, true)
		else
			m_SpellBarFunctions.setManaShadow(attack_from, attack_to.data.iconId, attack_to.data.mana > mana)
		end
	else
		-- m_SpellBarFunctions.clearAttack(from)
	end
end

m_SpellBarFunctions.parseAttack = function(id, data)
	m_SpellBarFunctions.clearAttack(id)
	
	local attack = m_SpellBarFunctions.getSpellById(id)
	attack.data = data
	m_SpellBarList.attackConfigs[id] = attack
	modules.game_hotkeys.m_HotkeyFunctions.setHotkeyMode(data.name)
	
	m_SpellBarFunctions.setIconImageType(attack.widget:getChildById('icon'), attack.data.iconId)
	m_SpellBarFunctions.setManaShadow(attack, attack.data.iconId, attack.data.mana > g_game.getLocalPlayer():getMana())
	m_SpellBarFunctions.hotkeysCache[data.name] = id
end

m_SpellBarFunctions.addToSpellBar = function(data, instant)
	if instant then
		local attack = m_SpellBarFunctions.getAttackByName(data.name, true)
		if attack then
			return false
		end
	end
	
	if instant or modules.game_character.m_SpellsFunctions.canAddSpellToBar() then
		for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
			local attack = m_SpellBarFunctions.getSpellById(id)
			if attack and attack.data.name == '' then
				m_SpellBarFunctions.parseAttack(id, data)
				break
			end
		end
	end
end

m_SpellBarFunctions.getAreaByName = function(name)
	local attack = m_SpellBarFunctions.getAttackByName(name, true)
	if not attack then
		return 0
	end
	
	return attack.data.spellArea or 0, attack.data.range, attack.data.ignoreRange
end

m_SpellBarFunctions.createLookSpell = function(spell, update)
	if not m_SpellBarList.spellInfoWindow then
		return false
	end
	
	if not update and ((m_SpellBarList.spellInfoWindow:isVisible() and m_SpellBarList.currentSpell == spell.name) or spell.name == '') then
		m_SpellBarList.spellInfoWindow:hide()
		m_SpellBarList.currentSpell = nil
	else
		local attack = m_SpellBarList.attackConfigs[i]
		m_SpellBarList.spellInfoWindow:show()
		m_SpellBarList.spellInfoWindow:setText(spell.name)
		m_SpellBarList.currentSpell = spell.name
		m_SpellBarFunctions.setIconImageType(m_SpellBarList.spellMiniature, spell.iconId)
		local language = g_settings.get('locale', 'en')
		if language == 'en' then
			language = LANGUAGE_ENGLISH
		else
			language = LANGUAGE_POLISH
		end
		
		local description = ''
		local spellLevel = math.max(0, spell.points - 1)
		local var = SpelllistSettings[spell.name]
		if var.description[0] then
			if var.value then
				local value = var.value(spellLevel)
				description = var.description[spellLevel][language]:format(unpack(value))
			else
				description = var.description[spellLevel][language]
			end
		else
			if var.value then
				local value = var.value(spellLevel)
				description = var.description[language]:format(unpack(value))
			else
				description = var.description[language]
			end
		end
		
		for i = 1, 4 do
			m_SpellBarList.skillIcon[i]:hide()
		end
		
		if not update then
			spell.points = math.max(1, spell.points)
			m_SpellBarList.spellData = spell
			m_SpellBarList.prevButton:setEnabled(spell.points > 1)
			m_SpellBarList.nextButton:setEnabled(spell.points < spell.spellLevel)
			m_SpellBarList.page:setText('Lv ' .. spell.points)
		end
		
		m_SpellBarList.effectPanelVariant1:destroyChildren()
		m_SpellBarList.effectPanelVariant2:destroyChildren()
		m_SpellBarList.effectPanelVariant3:destroyChildren()
		
		if description then
			m_SpellBarList.descriptionBox:setText(description)
		end
		
		local height = 140 + m_SpellBarList.descriptionBox:getHeight()
		if not var.area then
			m_SpellBarList.spellInfoWindow:setWidth(255)
			m_SpellBarList.effectPanelVariant1:hide()
			m_SpellBarList.effectPanelVariant2:hide()
			m_SpellBarList.effectPanelVariant3:hide()
			
			m_SpellBarList.blink1:hide()
			m_SpellBarList.blink2:hide()
			m_SpellBarList.blink3:hide()
		else
			local premium = g_game.getLocalPlayer():isPremium()
			if premium then
				m_SpellBarList.maskVariant2:hide()
			else
				m_SpellBarList.maskVariant2:show()
				if m_SpellBarFunctions.selectEffect[spell.name] == 2 then
					m_SpellBarFunctions.selectEffect[spell.name] = 1
				end
			end
			
			local effect = g_game.getLocalPlayer():isSpellEffect()
			if effect then
				m_SpellBarList.maskVariant3:hide()
			else
				m_SpellBarList.maskVariant3:show()
				if m_SpellBarFunctions.selectEffect[spell.name] == 3 then
					m_SpellBarFunctions.selectEffect[spell.name] = 1
				end
			end
			
			m_SpellBarList.effectPanelVariant1:show()
			m_SpellBarList.effectPanelVariant2:show()
			m_SpellBarList.effectPanelVariant3:show()
			if m_SpellBarFunctions.selectEffect[spell.name] == 3 then
				m_SpellBarList.blink1:hide()
				m_SpellBarList.blink2:hide()
				m_SpellBarList.blink3:show()
				
				m_SpellBarList.selected = m_SpellBarList.blink3
			elseif m_SpellBarFunctions.selectEffect[spell.name] == 2 then
				m_SpellBarList.blink1:hide()
				m_SpellBarList.blink2:show()
				m_SpellBarList.blink3:hide()
				
				m_SpellBarList.selected = m_SpellBarList.blink2
			else
				m_SpellBarList.blink1:show()
				m_SpellBarList.blink2:hide()
				m_SpellBarList.blink3:hide()
				
				m_SpellBarList.selected = m_SpellBarList.blink1
			end
			
			local layout = m_SpellBarList.effectPanelVariant1:getLayout()
			
			layout:setNumColumns(#spell.area[1])
			layout:setNumLines(#spell.area)
			
			local layout = m_SpellBarList.effectPanelVariant2:getLayout()
			layout:setNumColumns(#spell.area[1])
			layout:setNumLines(#spell.area)
			
			local layout = m_SpellBarList.effectPanelVariant3:getLayout()
			layout:setNumColumns(#spell.area[1])
			layout:setNumLines(#spell.area)
			
			for i = 1, #spell.area do
				for j = 1, #spell.area[i] do
					local widget = g_ui.createWidget('SpellEffect', m_SpellBarList.effectPanelVariant1)
					widget:setParent(m_SpellBarList.effectPanelVariant1)
					
					if spell.area[i][j] == 1 then
						widget:setEffectId(spell.effectId[1])
					elseif type(spell.area[i][j]) == 'table' then
						widget:setEffectId(spell.area[i][j][1])
					end
					
					local widget = g_ui.createWidget('SpellEffect', m_SpellBarList.effectPanelVariant2)
					widget:setParent(m_SpellBarList.effectPanelVariant2)
					
					if spell.area[i][j] == 1 then
						widget:setEffectId(spell.effectId[2])
					elseif type(spell.area[i][j]) == 'table' then
						widget:setEffectId(spell.area[i][j][2])
					end
					
					local widget = g_ui.createWidget('SpellEffect', m_SpellBarList.effectPanelVariant3)
					widget:setParent(m_SpellBarList.effectPanelVariant3)
					
					if spell.area[i][j] == 1 then
						widget:setEffectId(spell.effectId[3])
					elseif type(spell.area[i][j]) == 'table' then
						widget:setEffectId(spell.area[i][j][3])
					end
				end
			end
			
			local currentHeight = (#spell.area * 32) + 32
			local currentWidth = ((#spell.area[1] * 32) + 32) * 3
			m_SpellBarList.effectPanelVariant1:setSize({width = currentWidth / 3, height = currentHeight})
			m_SpellBarList.effectPanelVariant2:setSize({width = currentWidth / 3, height = currentHeight})
			m_SpellBarList.effectPanelVariant3:setSize({width = currentWidth / 3, height = currentHeight})
			height = math.max(height, currentHeight + 45)
			
			m_SpellBarList.spellInfoWindow:setWidth(255 + currentWidth)
		end
		
		m_SpellBarList.textMana:setVisible(not spell.passive)
		m_SpellBarList.textCooldown:setVisible(not spell.passive)
		m_SpellBarList.textDuration:setVisible(not spell.passive)
		
		if not spell.passive then
			m_SpellBarList.textMana:setText(spell.mana)
			m_SpellBarList.textCooldown:setText(spell.cooldown .. 's')
			m_SpellBarList.textDuration:setVisible(spell.duration and spell.duration > 0)
			
			if spell.duration and spell.duration > 0 then
				local minutes = 0
				if spell.duration > 60 then
					local seconds = spell.duration
					while(seconds >= 60) do
						minutes = minutes + 1
						seconds = seconds - 60
					end
				end
				
				m_SpellBarList.textDuration:setText(minutes > 0 and (minutes .. 'm') or (spell.duration .. 's'))
			end
		end
		
		m_SpellBarList.spellInfoWindow:setHeight(height)
		m_SpellBarList.spellInfoWindow:focus()
	end
end

m_SpellBarFunctions.addLabel = function(parent, name, description)
	local widget = g_ui.createWidget(name, parent)
	widget:setParent(parent)
	widget:setText(description)
	return widget
end

m_SpellBarFunctions.createLabel = function(parent, iconId, description)
	local widget = g_ui.createWidget('SpellAttributeLabel', parent)
	local icon = widget:getChildById('icon')
	
	modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, iconId)
	widget:setText(description)
	widget:setParent(parent)
	return widget
end

m_SpellBarFunctions.clearCache = function()
	for id = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		m_SpellBarFunctions.clear(id)
	end
end

m_SpellBarFunctions.onSaveCache = function()
	if m_SpellBarList.name ~= '' then
		g_settings.setNode('SpellList' .. m_SpellBarList.name, m_SpellBarFunctions.hotkeysCache)
		g_settings.setNode('SpellEffects' .. m_SpellBarList.name, m_SpellBarFunctions.selectEffect)
		g_settings.save()
	end
end

m_SpellBarFunctions.onLoadCache = function()
	m_SpellBarFunctions.hotkeysCache = g_settings.getNode('SpellList' .. m_SpellBarList.name) or {}
	m_SpellBarFunctions.selectEffect = g_settings.getNode('SpellEffects' .. m_SpellBarList.name) or {}
end

m_SpellBarFunctions.create = function(rotate)
	if not rotate then
		m_SpellBarFunctions.currentRotate = tonumber(g_settings.get('SpellBarOrientation')) or m_SpellBarFunctions.POSITION_HORIZONTAL
		m_SpellBarFunctions.currentPage = tonumber(g_settings.get('SpellBarRows', 1))
	end
	
	local var = 'Vertical'
	if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL then
		var = 'Horizontal'
	else
		m_SpellBarFunctions.currentRotate = m_SpellBarFunctions.POSITION_VERTICAL
	end

	local rootPanel = modules.game_interface.getRootPanel()
	m_SpellBarList.window = g_ui.createWidget('SpellBar' .. var, rootPanel)
	m_SpellBarList.window:setPosition({x = (rootPanel:getWidth() / 2) - 250, y = rootPanel:getHeight() - 126})
	m_SpellBarList.window:setup()
	
	m_SpellBarList.minimize = m_SpellBarList.window:getChildById('minimize')
	m_SpellBarList.maximize = m_SpellBarList.window:getChildById('maximize')
	
	m_SpellBarList.barrerWidget = m_SpellBarList.window:getChildById('barrerWidget')
	if m_SpellBarList.barrerWidget then
		m_SpellBarList.barrer = m_SpellBarList.barrerWidget:getChildById('barrer')
		m_SpellBarList.barrerLabel = m_SpellBarList.barrerWidget:getChildById('barrerLabel')
		m_SpellBarList.currentBarrerClip = 183
		
		if not m_SpellBarFunctions.visibleBars then
			m_SpellBarList.barrerWidget:hide()
		end
	end
	
	m_SpellBarList.healthWidget = m_SpellBarList.window:getChildById('healthWidget')
	if m_SpellBarList.healthWidget then
		m_SpellBarList.healthPotion = m_SpellBarList.window:getChildById('healthPotion')
		m_SpellBarList.healthPotionAmount = m_SpellBarList.window:getChildById('healthPotionAmount')
		m_SpellBarList.health = m_SpellBarList.healthWidget:getChildById('health')
		m_SpellBarList.healthLabel = m_SpellBarList.healthWidget:getChildById('healthLabel')
		m_SpellBarList.healthPotion:getChildById('item'):setItemId(236)
		m_SpellBarList.healthPotionAmount:setText(m_SpellBarFunctions.potionList[2])
		m_SpellBarList.currentHealthClip = 194
		
		if not m_SpellBarFunctions.visibleBars then
			m_SpellBarList.healthWidget:hide()
			m_SpellBarList.healthPotion:hide()
			m_SpellBarList.healthPotionAmount:hide()
		end
	end
	
	m_SpellBarList.manaWidget = m_SpellBarList.window:getChildById('manaWidget')
	if m_SpellBarList.manaWidget then
		m_SpellBarList.manaPotion = m_SpellBarList.window:getChildById('manaPotion')
		m_SpellBarList.manaPotionAmount = m_SpellBarList.window:getChildById('manaPotionAmount')
		m_SpellBarList.mana = m_SpellBarList.manaWidget:getChildById('mana')
		m_SpellBarList.manaLabel = m_SpellBarList.manaWidget:getChildById('manaLabel')
		m_SpellBarList.manaPotion:getChildById('item'):setItemId(237)
		m_SpellBarList.manaPotionAmount:setText(m_SpellBarFunctions.potionList[1])
		m_SpellBarList.currentManaClip = 194
		
		if not m_SpellBarFunctions.visibleBars then
			m_SpellBarList.manaWidget:hide()
			m_SpellBarList.manaPotion:hide()
			m_SpellBarList.manaPotionAmount:hide()
		end
	end
	
	m_SpellBarList.list = {}
	m_SpellBarList.background = m_SpellBarList.window:getChildById('background')
	m_SpellBarList.experienceBackground = m_SpellBarList.background:getChildById('label')
	m_SpellBarList.experienceFrame = m_SpellBarList.window:getChildById('experienceFrame')
	if m_SpellBarList.experienceFrame then
		m_SpellBarList.experienceFrame.center = true
		m_SpellBarList.experienceFrame.onHoverChange = m_SpellBarFunctions.onHoverChange
		m_SpellBarList.experienceFrame.onMouseRelease = m_SpellBarFunctions.onMouseRelease
	end
	
	m_SpellBarList.experienceLabel = m_SpellBarList.window:getChildById('experienceLabel')
	m_SpellBarList.goodPanel = m_SpellBarList.window:getChildById('goodConditionPanel')
	m_SpellBarList.badPanel = m_SpellBarList.window:getChildById('badConditionPanel')
	
	if not m_SpellBarFunctions.visibleBars then
		m_SpellBarList.background:hide()
		m_SpellBarList.experienceBackground:hide()
		m_SpellBarList.experienceFrame:hide()
		m_SpellBarList.experienceLabel:hide()
		m_SpellBarList.goodPanel:hide()
		m_SpellBarList.badPanel:hide()
	end
	
	if m_SpellBarList.iconList then
		if #m_SpellBarList.iconList[1] > 0 then
			for _, pid in pairs(m_SpellBarList.iconList[1]) do
				pid:setParent(m_SpellBarList.goodPanel)
				modules.game_healthinfo.m_HealthInfoFunctions.updateIcon(pid:getId(), {m_SpellBarList.window, m_SpellBarList.goodPanel, m_SpellBarList.badPanel, m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL})
			end
		end
		
		if m_SpellBarList.goodPanel:getChildCount() > 10 then
			if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL then
				m_SpellBarList.goodPanel:setHeight(36)
			else
				m_SpellBarList.goodPanel:setWidth(36)
			end
		end
		
		if #m_SpellBarList.iconList[2] > 0 then
			for _, pid in pairs(m_SpellBarList.iconList[2]) do
				pid:setParent(m_SpellBarList.badPanel)
				modules.game_healthinfo.m_HealthInfoFunctions.updateIcon(pid:getId(), {m_SpellBarList.window, m_SpellBarList.goodPanel, m_SpellBarList.badPanel, m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL})
			end
		end
		
		if m_SpellBarList.badPanel:getChildCount() > 10 then
			if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL then
				m_SpellBarList.badPanel:setHeight(36)
			else
				m_SpellBarList.badPanel:setWidth(36)
			end
		end
		
		m_SpellBarList.iconList = false
	end
	
	if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
		m_SpellBarList.manaWidget:rotate(90)
		m_SpellBarList.healthWidget:rotate(90)
		m_SpellBarList.barrerWidget:rotate(90)
		m_SpellBarList.background:rotate(90)
	end
	
	m_SpellBarList.spellInfoWindow = g_ui.displayUI('spell_info')
	m_SpellBarList.spellMiniature = m_SpellBarList.spellInfoWindow:getChildById('spellMiniature')
	m_SpellBarList.textMana = m_SpellBarList.spellInfoWindow:getChildById('textMana')
	m_SpellBarList.textCooldown = m_SpellBarList.spellInfoWindow:getChildById('textCooldown')
	m_SpellBarList.textDuration = m_SpellBarList.spellInfoWindow:getChildById('textDuration')
	m_SpellBarList.descriptionBox = m_SpellBarList.spellInfoWindow:getChildById('descriptionBox')
	
	m_SpellBarList.skillIcon = {}
	m_SpellBarList.skillIcon[1] = m_SpellBarList.descriptionBox:getChildById('skillIcon1')
	m_SpellBarList.skillIcon[2] = m_SpellBarList.descriptionBox:getChildById('skillIcon2')
	m_SpellBarList.skillIcon[3] = m_SpellBarList.descriptionBox:getChildById('skillIcon3')
	m_SpellBarList.skillIcon[4] = m_SpellBarList.descriptionBox:getChildById('skillIcon4')
	
	m_SpellBarList.page = m_SpellBarList.spellInfoWindow:getChildById('page')
	m_SpellBarList.exitButton = m_SpellBarList.spellInfoWindow:getChildById('exitButton')
	m_SpellBarList.prevButton = m_SpellBarList.spellInfoWindow:getChildById('prevButton')
	m_SpellBarList.nextButton = m_SpellBarList.spellInfoWindow:getChildById('nextButton')
	m_SpellBarList.removeButton = m_SpellBarList.spellInfoWindow:getChildById('removeButton')
	m_SpellBarList.effectPanelVariant1 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant1')
	m_SpellBarList.effectPanelVariant2 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant2')
	m_SpellBarList.effectPanelVariant3 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant3')
	m_SpellBarList.blink1 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant1blink')
	m_SpellBarList.blink2 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant2blink')
	m_SpellBarList.blink3 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant3blink')
	m_SpellBarList.maskVariant2 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant2mask')
	m_SpellBarList.maskVariant3 = m_SpellBarList.spellInfoWindow:getChildById('effectPanelVariant3mask')
	
	m_SpellBarList.effectPanelVariant1.onMouseRelease = modules.game_spellbar.select
	m_SpellBarList.effectPanelVariant2.onMouseRelease = modules.game_spellbar.select
	m_SpellBarList.effectPanelVariant3.onMouseRelease = modules.game_spellbar.select
	
	m_SpellBarList.selected = m_SpellBarList.blink1
	
	if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_HORIZONTAL then
		local localPlayer = g_game.getLocalPlayer()
		onHealthChange(localPlayer, localPlayer:getHealth(), localPlayer:getMaxHealth())
		onManaChange(localPlayer, localPlayer:getMana(), localPlayer:getMaxMana())
		onBarrerChange(localPlayer, localPlayer:getBarrer(), localPlayer:getMaxBarrer())
	end
	
	m_SpellBarList.window.onMouseMove = function(widget, mousePos, mouseMoved)
		if m_SpellBarList.moveSpell then
			local mousepos = g_window.getMousePosition()
			if(mousepos.x < m_SpellBarList.window:getX() or mousepos.y < m_SpellBarList.window:getY() or
			   mousepos.x > m_SpellBarList.window:getX() + m_SpellBarList.window:getWidth() or
			   mousepos.y > m_SpellBarList.window:getY() + m_SpellBarList.window:getHeight()) then

				m_SpellBarList.moveSpell = nil
				m_SpellBarList.moveSpellEvent = nil
				m_SpellBarList.moveSpellShadow:hide()
				return true
			end
			
			mousepos.x = mousepos.x - (m_SpellBarList.moveSpellShadow:getWidth() / 2)
			mousepos.y = mousepos.y - (m_SpellBarList.moveSpellShadow:getHeight() / 2)
			m_SpellBarList.moveSpellShadow:setPosition(mousepos)
		end
	end
	
	m_SpellBarList.window.onMouseRelease = function(widget, mousePosition, mouseButton)
		if mouseButton == MouseLeftButton and m_SpellBarList.moveSpell then
			m_SpellBarList.moveSpell = nil
			m_SpellBarList.moveSpellEvent = nil
			m_SpellBarList.moveSpellShadow:hide()
		end
	end
	
	m_SpellBarList.descriptionBox.onText = function(self, coords, text, iconCoords)
								local i = 1
								for _, v in pairs(iconCoords) do
									local id, icon = unpack(v)
									if icon.x ~= 0 and icon.y ~= 0 then
										local pos = self:getPosition()
										local offset = {x = icon.x - pos.x, y = icon.y - pos.y - 1}
										if id == 2 or id == 1 then
											offset.x = offset.x - 10
										end
										
										local skillIcon = m_SpellBarList.skillIcon[i]
										skillIcon:show()
										skillIcon:setMarginTop(offset.y)
										skillIcon:setMarginLeft(offset.x)
										skillIcon:setId(id)
										if id == 0 or id == 1 or id == 3 then
											skillIcon.onHoverChange = function(self, hovered)
												if hovered then
													local pos = self:getPosition()
													pos.x = pos.x + self:getWidth() - 2
													pos.y = pos.y + self:getHeight() - 2
													
													m_SpellBarList.skillIconInfo = g_ui.displayUI('icon_panel')
													m_SpellBarList.skillIconInfo:setPosition(pos)
												elseif m_SpellBarList.skillIconInfo then
													m_SpellBarList.skillIconInfo:destroy()
													m_SpellBarList.skillIconInfo = nil
												end
											end
										else
											skillIcon.onHoverChange = nil
										end
										
										local name = ''
										if id == 0 then
											id = 19
											name = 'Basic Attack'
										elseif id == 1 then
											id = 51
											name = 'Basic Attack'
										elseif id == 2 then
											id = 27
											name = 'Condition'
										elseif id == 3 then
											id = 17
											name = 'Wisdom'
										elseif id == 4 then
											id = 49
											name = 'Vigor'
										end
										
										i = i + 1
										modules.game_lookat.m_LookAtFunctions.setIconImageType(skillIcon, id)
										skillIcon:setTooltip(name)
									end
								end
							end
	
	m_SpellBarList.exitButton.onClick = function()
								m_SpellBarList.spellInfoWindow:hide()
								m_SpellBarList.currentSpell = nil
							end

	m_SpellBarList.removeButton.onClick = function()
								for i = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
									if m_SpellBarList.currentSpell and m_SpellBarList.currentSpell == m_SpellBarList.attackConfigs[i].data.name then
										m_SpellBarFunctions.clearAttack(i)
									end
								end
							end
	
	for i = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		local widget = g_ui.createWidget('AttackIcon' .. var, m_SpellBarList.window)
		widget:setId('spellIcon' .. i)
		
		if i % m_SpellBarFunctions.config.maxAttacks ~= 1 then
			if m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
				widget:addAnchor(AnchorTop, 'spellIcon' .. (i - 1), AnchorBottom)
				widget:setOn(false)
			else
				widget:addAnchor(AnchorLeft, 'spellIcon' .. (i - 1), AnchorRight)
				widget:setOn(true)
			end
			
			widget:setMarginLeft(5)
		elseif m_SpellBarFunctions.currentRotate == m_SpellBarFunctions.POSITION_VERTICAL then
			widget:setMarginLeft(5)
		end
		
		if i > m_SpellBarFunctions.config.maxAttacks then
			widget:hide()
		end
		
		m_SpellBarList.attackConfigs[i] = { widget = widget, spellid = i }
		widget.onHoverChange = function(self, hovered)
									if m_SpellBarList.updatePosition then
										m_SpellBarFunctions.onSpellBarHover(self, hovered)
										return true
									end
									
									local attack = m_SpellBarFunctions.getAttackByNameId(self:getId(), true)
									if not attack or attack.data.name == '' then
										return true
									end
									
									if not hovered then
										if m_SpellBarList.descriptionWindow then
											m_SpellBarList.descriptionWindow:getChildById('list'):destroyChildren()
											m_SpellBarList.descriptionWindow:destroy()
											m_SpellBarList.descriptionWindow = nil
										end
										
										return true
									end
									
									local rootWidget = modules.game_interface.getRootPanel()
									m_SpellBarList.descriptionWindow = g_ui.createWidget('SpellHoverPanel', rootWidget)
									
									local list = m_SpellBarList.descriptionWindow:getChildById('list')
									if list then
										local height = 90
										if not modules.client_options.getOption('displayHints') then
											height = 12
											
											for i = 1, 3 do
												m_SpellBarList.descriptionWindow:getChildById('mouse' .. i):hide()
											end
										end
										
										local name = attack.data.name:sub(1, 1):upper() .. attack.data.name:sub(2, attack.data.name:len())
										local widget = m_SpellBarFunctions.addLabel(list, 'SpellBarHoverLabel', name)
										height = height + widget:getHeight() + 2
										
										widget = m_SpellBarFunctions.createLabel(list, 40, attack.data.mana .. ' mana')
										height = height + widget:getHeight() + 2
										
										local text = attack.widget.hotkey
										if text then
											widget = m_SpellBarFunctions.createLabel(list, 52, 'Hotkey: ' .. text)
											height = height + widget:getHeight() + 2
										end
										
										m_SpellBarList.descriptionWindow:setHeight(height)
									end
									
									local pos = self:getPosition()
									pos.x = pos.x + self:getWidth() - 8
									pos.y = pos.y + self:getHeight() - 8
									
									if pos.x + m_SpellBarList.descriptionWindow:getWidth() >= rootWidget:getWidth() then
										pos.x = pos.x - m_SpellBarList.descriptionWindow:getWidth()
									end
									
									if pos.y + m_SpellBarList.descriptionWindow:getHeight() >= rootWidget:getHeight() then
										pos.y = pos.y - m_SpellBarList.descriptionWindow:getHeight()
									end
									
									m_SpellBarList.descriptionWindow:setPosition(pos)
								end
		
		widget.onMousePress = function(self, mousePosition, mouseButton)
									if m_SpellBarList.attackConfigs[i].data.name == '' or mouseButton ~= MouseLeftButton then
										return true
									end
									
									m_SpellBarList.moveSpellEvent = scheduleEvent(function()
															m_SpellBarList.moveSpell = m_SpellBarList.attackConfigs[i]
															m_SpellBarList.moveSpellEvent = nil
															m_SpellBarFunctions.setIconImageType(m_SpellBarList.moveSpellShadow, m_SpellBarList.moveSpell.data.iconId)
															m_SpellBarList.moveSpellShadow:show()
															local mousePos = g_window.getMousePosition()
															mousePos.x = mousePos.x - (m_SpellBarList.moveSpellShadow:getWidth() / 2)
															mousePos.y = mousePos.y - (m_SpellBarList.moveSpellShadow:getHeight() / 2)
															m_SpellBarList.moveSpellShadow:setPosition(mousePos)
														end, 200)
								end
								
		widget.onMouseRelease = function(self, mousePosition, mouseButton)
									if mouseButton == MouseLeftButton then
										if m_SpellBarList.moveSpellEvent then
											if not m_SpellBarList.updatePosition then
												m_SpellBarFunctions.onClickSpell(m_SpellBarList.attackConfigs[i].data.name)
											end
											
											m_SpellBarList.moveSpellEvent:cancel()
										elseif m_SpellBarList.moveSpell then
											local toIcon = m_SpellBarFunctions.getIconByPosition()
											if toIcon and m_SpellBarList.moveSpell ~= toIcon then
												m_SpellBarFunctions.replace(m_SpellBarList.moveSpell.spellid, toIcon.spellid)
												m_SpellBarList.moveSpell = nil
												m_SpellBarList.moveSpellShadow:hide()
											end
										end
									elseif mouseButton == MouseRightButton then
										local menu = g_ui.createWidget('PopupMenu')
										menu:setGameMenu(true)
										menu:addOption(tr('Look'), function() m_SpellBarFunctions.createLookSpell(m_SpellBarList.attackConfigs[i].data) end)
										menu:addOption(tr('Remove'), function() m_SpellBarFunctions.clearAttack(i) end)
										menu:display(mousePosition)
									end
								end

		m_SpellBarFunctions.clearAttack(i)
	end
	
	if m_SpellBarFunctions.currentPage > 1 then
		local size = m_SpellBarFunctions.currentPage
		
		m_SpellBarFunctions.currentPage = 1
		for i = 1, math.min(size, m_SpellBarFunctions.config.pages) - 1 do
			m_SpellBarFunctions.maximize()
		end
	end
	
	if g_game.isOnline() then
		local localPlayer = g_game.getLocalPlayer()
		onHealthChange(localPlayer, localPlayer:getHealth(), localPlayer:getMaxHealth())
		onManaChange(localPlayer, localPlayer:getMana(), localPlayer:getMaxMana())
		onBarrerChange(localPlayer, localPlayer:getBarrer(), localPlayer:getMaxBarrer())
		onLevelChange(localPlayer, localPlayer:getLevel(), localPlayer:getLevelPercent())
		onExperienceChange(localPlayer, localPlayer:getExperience())
			
		m_SpellBarList.name = localPlayer:getName()
		m_SpellBarFunctions.onLoadCache()
	end
	
	m_SpellBarList.moveSpellShadow = g_ui.createWidget('SpellShadow', m_SpellBarList.window)
end

m_SpellBarFunctions.destroy = function(ignore)
	local list = {}
	if m_SpellBarList.goodPanel then
		list[1] = {}
		
		for _, pid in pairs(m_SpellBarList.goodPanel:getChildren()) do
			m_SpellBarList.goodPanel:removeChild(pid)
			table.insert(list[1], pid)
		end
	end
	
	if m_SpellBarList.badPanel then
		list[2] = {}
		for _, pid in pairs(m_SpellBarList.badPanel:getChildren()) do
			m_SpellBarList.badPanel:removeChild(pid)
			table.insert(list[2], pid)
		end
	end
	
	m_SpellBarFunctions.destroyHover()
	
	if m_SpellBarList.descriptionWindow then
		m_SpellBarList.descriptionWindow:destroy()
		m_SpellBarList.descriptionWindow = nil
	end
	
	if not ignore then
		if m_SpellBarList.cooldownEvent then
			m_SpellBarList.cooldownEvent:cancel()
			m_SpellBarList.cooldownEvent = nil
		end
		
		if m_SpellBarList.spellInfoWindow then
			m_SpellBarList.spellInfoWindow:destroy()
			m_SpellBarList.spellInfoWindow = nil
		end
		
		if m_SpellBarList.skillIconInfo then
			m_SpellBarList.skillIconInfo:destroy()
			m_SpellBarList.skillIconInfo = nil
		end
		
		if m_SpellBarList.window then
			if m_SpellBarList.moveSpellShadow then
				m_SpellBarList.moveSpellShadow:destroy()
				m_SpellBarList.moveSpellShadow = nil
			end
			
			m_SpellBarList.goodPanel:destroyChildren()
			m_SpellBarList.badPanel:destroyChildren()
			
			m_SpellBarList.window:destroyChildren()
			m_SpellBarList.window:destroy()
			m_SpellBarList = {}
			
			m_SpellBarList.cooldownList = {}
			m_SpellBarList.iconList = list
		end
	end
end

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onAddAttack = onAddAttack,
		onAttackCooldown = onAttackCooldown
	})
	
	connect(LocalPlayer, {
		onHealthChange = onHealthChange,
		onBarrerChange = onBarrerChange,
		onLevelChange = onLevelChange,
		onExperienceChange = onExperienceChange,
		onVocationChange = onVocationChange
	})
	
	g_ui.importStyle('game_spellbar')
	
	m_SpellBarList.attackConfigs = {}
	
	if g_game.isOnline() then
		onGameStart()
	end
	
	ProtocolGame.registerOpcode(m_SpellBarFunctions.GameServerUpdatePotions, m_SpellBarFunctions.parseUpdatePotions)
end

function onUnload()
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onAddAttack = onAddAttack,
		onAttackCooldown = onAttackCooldown
	})

	disconnect(LocalPlayer, {
		onHealthChange = onHealthChange,
		onBarrerChange = onBarrerChange,
		onLevelChange = onLevelChange,
		onExperienceChange = onExperienceChange,
		onVocationChange = onVocationChange
	})
	
	m_SpellBarFunctions.destroy()
	ProtocolGame.unregisterOpcode(m_SpellBarFunctions.GameServerUpdatePotions, m_SpellBarFunctions.parseUpdatePotions)
	
	if m_SpellBarList.moveSpellShadow then
		m_SpellBarList.moveSpellShadow:destroy()
		m_SpellBarList.moveSpellShadow = nil
	end
	
	if m_SpellBarFunctions.event then
		m_SpellBarFunctions.event:cancel()
		m_SpellBarFunctions.event = nil
	end
end

function onGameStart()
	m_SpellBarFunctions.create(false)
	for i = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		m_SpellBarFunctions.clearAttack(i)
	end
	
	local localPlayer = g_game.getLocalPlayer()
	if localPlayer then
		m_SpellBarList.name = localPlayer:getName()
		m_SpellBarFunctions.onLoadCache()
	end
	
	m_SpellBarList.window:show()
	m_SpellBarList.cooldownList = {}
	
	for k, v in pairs(SpelllistSettings) do
		if v.utility then
			onAddAttack(k, 1, true)
		end
	end
	
	if m_SpellBarFunctions.event then
		m_SpellBarFunctions.event:cancel()
	end
	
	m_SpellBarFunctions.event = cycleEvent(m_SpellBarFunctions.checkExpSpeed, 2000)
end

function onGameEnd()
	m_SpellBarFunctions.onSaveCache()
	
	if m_SpellBarList.descriptionWindow then
		m_SpellBarList.descriptionWindow:destroy()
		m_SpellBarList.descriptionWindow = nil
	end
	
	m_SpellBarList.window:hide()
	for i = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		m_SpellBarFunctions.clearAttack(i)
	end
	
	g_settings.set('SpellBarOrientation', m_SpellBarFunctions.currentRotate)
	g_settings.set('SpellBarRows', m_SpellBarFunctions.currentPage)
	if m_SpellBarList.cooldownEvent then
		m_SpellBarList.cooldownEvent:cancel()
		m_SpellBarList.cooldownEvent = nil
	end
	
	m_SpellBarList.cooldownList = {}
	if m_SpellBarList.spellInfoWindow:isVisible() then
		m_SpellBarList.spellInfoWindow:hide()
	end
	
	if m_SpellBarFunctions.event then
		m_SpellBarFunctions.event:cancel()
		m_SpellBarFunctions.event = nil
	end
	
	m_SpellBarList.goodPanel:destroyChildren()
	m_SpellBarList.badPanel:destroyChildren()
	m_SpellBarFunctions.destroy(true)
end

function onVocationChange(localPlayer, vocation)
	for i = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		local attack = m_SpellBarList.attackConfigs[i]
		if attack.data.vocation and not isInArray(attack.data.vocation, vocation) then
			m_SpellBarFunctions.clearAttack(i)
		end
	end
end

function onManaChange(player, mana, maxMana)
	if m_SpellBarList.mana then
		if mana > maxMana then
			mana = maxMana
		end
		
		local width = math.min(194, math.ceil(194 * mana / maxMana))
		m_SpellBarList.mana:setWidth(width)
		m_SpellBarList.mana:setImageClip('2783 1873 ' .. width .. ' 21')
		m_SpellBarList.manaLabel:setText(mana .. '/' .. maxMana)
	end
	
	for i = 1, m_SpellBarFunctions.config.maxAttacks * m_SpellBarFunctions.config.pages do
		local attack = m_SpellBarList.attackConfigs[i]
		if attack and attack.data.mana and not attack.data.m_cooldown then
			m_SpellBarFunctions.setManaShadow(attack, attack.data.iconId, attack.data.mana > mana)
		end
	end
end

function onHealthChange(localPlayer, health, maxHealth, oldHealth, oldMaxHealth)
	if health == 0 and maxHealth == 0 then
		return true
	end
	
	if m_SpellBarList.health then
		if health > maxHealth then
			health = maxHealth
		end
		
		local width = math.min(194, math.ceil(194 * health / maxHealth))
		m_SpellBarList.health:setWidth(width)
		m_SpellBarList.health:setImageClip(2781 + (194 - width) .. ' 1902 ' .. width .. ' 21')
		m_SpellBarList.healthLabel:setText(health .. '/' .. maxHealth)
	end
end

function onBarrerChange(localPlayer, barrer, barrerMax)
	if barrer == 0 and barrerMax == 0 then
		return true
	end
	
	if m_SpellBarList.barrer then
		if barrer > barrerMax then
			barrer = barrerMax
		end
		
		local width = math.min(183, math.ceil(183 * barrer / barrerMax))
		m_SpellBarList.barrer:setWidth(width)
		m_SpellBarList.barrer:setImageClip((2769 + (183 - width)) .. ' 1941 ' .. width .. ' 13')
		m_SpellBarList.barrerLabel:setText(barrer .. '/' .. barrerMax)
	end
end

m_SpellBarFunctions.getExpForLevel = function(lv)
	local amount = (50 * lv * lv * lv) - (150 * lv * lv) + (400 * lv)
	if lv > 110 then
		if lv < 190 then
			local multiplier = (lv - 110) * (lv - 110) * 0.025
			amount = math.ceil(((50 + multiplier) * lv * lv * lv) - (150 * lv * lv) + ((400 + multiplier) * lv))
		else
			amount = (210 * lv * lv * lv) - (150 * lv * lv) + (560 * lv)
		end
	end
	
	-- local amount = (50 * lv * lv * lv) - (150 * lv * lv) + (400 * lv)
	-- if lv > 50 then
		-- local multiplier = (lv - 50) * (lv - 50) * 0.008
		-- amount = math.ceil(((50 + multiplier) * lv * lv * lv) - (150 * lv * lv) + ((400 + multiplier) * lv))
	-- end
	
	return amount
end

function onExperienceChange(localPlayer, experience)
	m_SpellBarFunctions.checkExpSpeed()
	onLevelChange(localPlayer, localPlayer:getLevel(), localPlayer:getLevelPercent())
end

function onLevelChange(localPlayer, level, percent)
	if m_SpellBarList.experienceBackground then
		local height = math.min(62, math.ceil(62 * percent / 100))
		m_SpellBarList.experienceBackground:setHeight(height)
		m_SpellBarList.experienceBackground:setImageClip('2545 ' .. 1735 + (62 - height) .. ' 62 ' .. height)
		m_SpellBarList.experienceLabel:setText(level)
	end
	
	local expForNextLevel = m_SpellBarFunctions.getExpForLevel(level)
	local nextLevelExp = expForNextLevel - localPlayer:getExperience()
	local text = tr('You have %s percent to go', 100 - percent) .. '\n' ..
				tr('%s of experience left', g_game.getMilharNumber(nextLevelExp))
	if localPlayer.expSpeed then
		local expPerHour = math.floor(localPlayer.expSpeed * 3600)
		if expPerHour > 0 then
			local hoursLeft = nextLevelExp / expPerHour
			local minutesLeft = math.floor((hoursLeft - math.floor(hoursLeft)) * 60)
			hoursLeft = math.floor(hoursLeft)
			text = text .. '\n' .. tr('%s of experience per hour', g_game.getMilharNumber(expPerHour))
			text = text .. '\n' .. tr('Next level in %d hours and %d minutes', hoursLeft, minutesLeft)
		end
	end
	
	m_SpellBarFunctions.tooltip = text
	if m_SpellBarList.hover then
		m_SpellBarList.hover:setText(m_SpellBarFunctions.tooltip)
	end
end

m_SpellBarFunctions.resetExperience = function()
	local localPlayer = g_game.getLocalPlayer()
	if not localPlayer then
		return true
	end
	
	localPlayer.lastExps = nil
	localPlayer.expSpeed = nil
	onLevelChange(localPlayer, localPlayer:getLevel(), localPlayer:getLevelPercent())
end

m_SpellBarFunctions.checkExpSpeed = function()
	local localPlayer = g_game.getLocalPlayer()
	if not localPlayer then
		return true
	end
	
	local currentExp = localPlayer:getExperience()
	if currentExp < 0 then
		return true
	end
	
	local currentTime = g_clock.seconds()
	if localPlayer.lastExps then
		localPlayer.expSpeed = (currentExp - localPlayer.lastExps[1][1]) / (currentTime - localPlayer.lastExps[1][2])
		onLevelChange(localPlayer, localPlayer:getLevel(), localPlayer:getLevelPercent())
	else
		localPlayer.lastExps = {}
	end
	
	table.insert(localPlayer.lastExps, {currentExp, currentTime})
	if #localPlayer.lastExps > 1800 then
		table.remove(localPlayer.lastExps, 1)
	end
end

function onAddAttack(name, points, checkHotkeys)
	local name = name:lower()
	local data = modules.game_character.m_SpellsFunctions.addActiveSpell(name, points)
	if not data or data.passive then
		return true
	end
	
	local attack = m_SpellBarFunctions.getAttackByName(name, true)
	if data.points == 0 then
		if attack then
			m_SpellBarFunctions.clearAttack(attack.spellid)
		end
		
		return true
	end
	
	if attack then
		attack.data = data
		m_SpellBarList.attackConfigs[attack.spellid] = attack
		return false
	end
	
	local player, cache = g_game.getLocalPlayer(), -1
	if player then
		if m_SpellBarFunctions.hotkeysCache and m_SpellBarFunctions.hotkeysCache[name] then
			cache = m_SpellBarFunctions.hotkeysCache[name]
		end
	end
	
	if cache > 0 then
		local attack = m_SpellBarFunctions.getSpellById(cache)
		if attack and attack.data.name == '' then
			m_SpellBarFunctions.parseAttack(cache, data)
			return true
		end
	elseif (not modules.game_character.m_SpellsList.window and cache == 0) or checkHotkeys then
		return true
	end
	
	m_SpellBarFunctions.addToSpellBar(data)
end

function onAttackCooldown(list, isLogin)
	for i = 1, #list do
		local name, cooldown = unpack(list[i])
		m_SpellBarFunctions.updateCooldown(name, cooldown, isLogin)
	end
end

function select(self, mousePosition, mouseButton)
	local widget = m_SpellBarList.spellInfoWindow:getChildById(self:getId() .. 'blink')
	if not widget then
		return false
	end
	
	local pid = m_SpellBarList.spellInfoWindow:getChildById(self:getId() .. 'mask')
	if pid and pid:isVisible() then
		return false
	end
	
	if m_SpellBarList.selected then
		if widget == m_SpellBarList.selected then
			return false
		end
		
		m_SpellBarList.selected:hide()
	end
	
	if m_SpellBarList.currentSpell then
		m_SpellBarFunctions.selectEffect[m_SpellBarList.currentSpell] = self:getId() == 'effectPanelVariant1' and 1 or self:getId() == 'effectPanelVariant2' and 2 or 3
	end
	
	m_SpellBarList.selected = widget
	widget:show()
end