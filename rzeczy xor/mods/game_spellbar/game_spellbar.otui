SpellHoverLabels < Label
  color: #9E996E
  font: perfect-sans-11px
  text-wrap: true
  text-auto-resize: true
  text-align: left
  height: 12

SpellAttributeLabel < Label
  text-wrap: true
  text-auto-resize: true
  text-align: left
  text-offset: 24 0
  
  IconWidget
    id: icon
    anchors.left: parent.left
    anchors.verticalcenter: parent.verticalcenter

SpellBarHoverLabel < Label
  font: perfect-sans-16px
  text-wrap: true
  text-auto-resize: true

SpellHoverPanel < UIMiniWindow
  focusable: false
  phantom: true
  size: 300 90
  
  UIWidget
    anchors.fill: parent
    image-source: /ui/gui_atlas
    image-clip: 365 119 153 176
    image-border: 8
    opacity: 0.85
    phantom: true
  
  ScrollablePanel
    id: list
    anchors.fill: parent
    margin: 8
    layout:
      type: verticalBox
  
  SpellInfoSeparator
    id: separator
    anchors.bottom: next.top

  LeftMouseButtonImage
    id: mouse1
    anchors.bottom: next.top
    anchors.left: next.left
    anchors.right: next.right
    !text: tr('Hold left mouse button to move spell')
    margin-bottom: 2

  LeftMouseButtonImage
    id: mouse2
    anchors.bottom: next.top
    anchors.left: next.left
    anchors.right: next.right
    !text: tr('Left mouse button to use spell')
    margin-bottom: 2

  RightMouseButtonImage
    id: mouse3
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    !text: tr('Right mouse button to open context menu')
    margin: 4

SpellBarVertical < UIMiniWindow
  id: protected12
  focusable: false
  draggable: true
  size: 150 500
  &save: true

  AtlasWidget
    anchors.fill: parent
    image-clip: 1467 749 45 497
    image-border-left: 6
    image-border-right: 6
    margin-right: 104
    phantom: true
  
  AtlasWidget
    id: background
    anchors.verticalcenter: parent.verticalcenter
    anchors.horizontalcenter: parent.right
    margin-right: 72
    image-clip: 2545 1803 62 62
    size: 62 62
    phantom: true
  
    AtlasWidget
      id: label
      anchors.bottom: parent.bottom
      anchors.horizontalcenter: parent.horizontalcenter
      image-clip: 2545 1735 62 1
      size: 62 1
      image-color: #DAA520
  
  AtlasWidget
    id: experienceFrame
    anchors.centerIn: background
    image-clip: 2451 1728 75 75
    size: 75 75
    margin-left: 1

  SpellBarInformationLabel
    id: experienceLabel
    anchors.fill: background
    !text: '1'
    margin-top: 2
    phantom: true

  AtlasWidget
    id: manaWidget
    anchors.horizontalcenter: background.left
    anchors.top: background.bottom
    image-clip: 2784 1751 194 21
    size: 194 21
    margin-top: 108
    margin-left: 8
    phantom: true
    
    AtlasWidget
      id: mana
      image-clip: 2783 1873 194 21
      anchors.left: parent.left
      anchors.bottom: parent.bottom
      size: 194 21
      phantom: true
    
    SpellBarInformationLabel
      id: manaLabel
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      margin-right: 20
      margin-bottom: 2
      !text: '100/100'
  
  Panel
    id: goodConditionPanel
    anchors.top: background.bottom
    anchors.left: prev.horizontalcenter
    size: 18 180
    margin-top: 31
    margin-left: 10
    layout:
      type: grid
      cell-size: 18 18
      flow: true
      num-lines: 10

  AtlasWidget
    id: manaPotion
    anchors.horizontalcenter: background.left
    anchors.top: background.bottom
    image-clip: 2696 1816 37 37
    size: 37 37
    margin-left: 14
    margin-top: -6
    phantom: true
    
    UIItem
      id: item
      size: 32 32
      image-source: /ui/blank
      anchors.centerIn: parent
      virtual: true
      enabled: false
      phantom: true
      margin-bottom: 4
      margin-right: 4

  SpellBarInformationLabel
    id: manaPotionAmount
    anchors.bottom: prev.bottom
    anchors.horizontalcenter: prev.left
    margin-left: 6
    !text: '0'

  AtlasWidget
    id: barrerWidget
    anchors.bottom: next.bottom
    anchors.horizontalcenter: next.horizontalcenter
    image-clip: 2786 1809 183 13
    size: 183 13
    margin-bottom: -5
    margin-left: 17
    phantom: true

    AtlasWidget
      id: barrer
      image-clip: 2952 1941 1 13
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      size: 1 13
      phantom: true
    
    SpellBarInformationLabel
      id: barrerLabel
      anchors.left: parent.left
      anchors.verticalcenter: parent.verticalcenter
      margin-left: 18
      margin-top: 2
      !text: '0/50'
      font: perfect-sans-8px

  AtlasWidget
    id: healthWidget
    anchors.horizontalcenter: background.left
    anchors.bottom: background.top
    image-clip: 2781 1809 194 21
    size: 194 21
    margin-bottom: 108
    margin-left: 8
    phantom: true
    
    AtlasWidget
      id: health
      image-clip: 2781 1902 194 21
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      size: 194 21
      phantom: true
    
    SpellBarInformationLabel
      id: healthLabel
      anchors.left: parent.left
      anchors.bottom: parent.bottom
      margin-left: 20
      margin-bottom: 2
      !text: '250/250'

  Panel
    id: badConditionPanel
    anchors.bottom: background.top
    anchors.left: prev.horizontalcenter
    size: 18 180
    margin-bottom: 12
    margin-left: 23
    layout:
      type: grid
      cell-size: 18 18
      flow: true
      num-lines: 10
  
  AtlasWidget
    id: healthPotion
    anchors.horizontalcenter: background.left
    anchors.bottom: background.top
    image-clip: 2696 1816 37 37
    size: 37 37
    margin-left: 14
    margin-bottom: -6
    phantom: true
    
    UIItem
      id: item
      size: 32 32
      image-source: /ui/blank
      anchors.centerIn: parent
      virtual: true
      enabled: false
      phantom: true
      margin-bottom: 4
      margin-right: 4

  SpellBarInformationLabel
    id: healthPotionAmount
    anchors.bottom: prev.bottom
    anchors.horizontalcenter: prev.left
    margin-left: 6
    !text: '0'

  AtlasButton
    id: minimize
    anchors.horizontalcenter: next.horizontalcenter
    anchors.top: next.bottom
    size: 16 16
    image-clip: 1518 975 16 16
    visible: false
    @onClick: modules.game_spellbar.m_SpellBarFunctions.onChangeSize(-1)
    !tooltip: 'Minimize'
    margin-top: 6

  AtlasButton
    id: rotate
    anchors.verticalcenter: parent.verticalcenter
    anchors.right: parent.right
    size: 16 16
    image-clip: 1518 663 16 16
    @onClick: modules.game_spellbar.m_SpellBarFunctions.onRotate()
    !tooltip: 'Rotate'
    margin-right: 100

  AtlasButton
    id: maximize
    anchors.horizontalcenter: prev.horizontalcenter
    anchors.bottom: prev.top
    size: 16 16
    image-clip: 1521 800 16 16
    @onClick: modules.game_spellbar.m_SpellBarFunctions.onChangeSize(1)
    !tooltip: 'Maximize'
    margin-bottom: 6

SpellBarInformationLabel < Label
  phantom: true
  font: perfect-sans-11px-bold
  color: #E4CEBA
  text-align: center
  text-horizontal-auto-resize: true
  phantom: false

SpellBarHorizontal < UIMiniWindow
  id: protected12
  focusable: false
  draggable: true
  size: 500 114
  &save: true
  
  AtlasWidget
    id: image
    anchors.fill: parent
    image-clip: 1266 684 497 45
    image-border-top: 6
    image-border-bottom: 6
    margin-top: 68
    phantom: true
    $on:
      margin-top: 0
    
  AtlasWidget
    id: background
    anchors.horizontalcenter: parent.horizontalcenter
    anchors.verticalcenter: parent.top
    margin-top: 36
    image-clip: 2545 1803 62 62
    size: 62 62
    phantom: true
  
    AtlasWidget
      id: label
      anchors.bottom: parent.bottom
      anchors.horizontalcenter: parent.horizontalcenter
      image-clip: 2545 1735 62 1
      size: 62 1
      image-color: #DAA520
  
  AtlasWidget
    id: experienceFrame
    anchors.centerIn: background
    image-clip: 2451 1728 75 75
    size: 75 75
    margin-left: 1

  SpellBarInformationLabel
    id: experienceLabel
    anchors.fill: background
    !text: '1'
    margin-top: 2
    phantom: true

  AtlasWidget
    id: manaWidget
    anchors.bottom: background.bottom
    anchors.left: background.right
    image-clip: 2784 1751 194 21
    size: 194 21
    margin-left: 24
    phantom: true
    
    AtlasWidget
      id: mana
      image-clip: 2783 1873 194 21
      anchors.left: parent.left
      anchors.bottom: parent.bottom
      size: 194 21
      phantom: true
    
    SpellBarInformationLabel
      id: manaLabel
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      margin-right: 20
      margin-bottom: 2
      !text: '100/100'
  
  Panel
    id: goodConditionPanel
    anchors.bottom: prev.top
    anchors.left: prev.left
    size: 180 18
    margin-left: 4
    margin-bottom: 1
    layout:
      type: grid
      cell-size: 18 18
      flow: true
      num-columns: 10
    
  AtlasWidget
    id: manaPotion
    anchors.top: background.verticalcenter
    anchors.horizontalcenter: background.right
    image-clip: 2696 1816 37 37
    size: 37 37
    margin-left: 12
    margin-top: -2
    phantom: true
    
    UIItem
      id: item
      size: 32 32
      image-source: /ui/blank
      anchors.centerIn: parent
      virtual: true
      enabled: false
      phantom: true
      margin-bottom: 4
      margin-right: 4

  SpellBarInformationLabel
    id: manaPotionAmount
    anchors.bottom: prev.bottom
    anchors.left: prev.left
    margin-left: 8
    !text: '0'

  AtlasWidget
    id: barrerWidget
    anchors.bottom: next.bottom
    anchors.right: next.right
    image-clip: 2786 1809 183 13
    size: 183 13
    margin-bottom: 18
    phantom: true

    AtlasWidget
      id: barrer
      image-clip: 2952 1941 1 13
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      size: 1 13
      phantom: true
    
    SpellBarInformationLabel
      id: barrerLabel
      anchors.left: parent.left
      anchors.verticalcenter: parent.verticalcenter
      margin-left: 18
      !text: '0/50'
      font: perfect-sans-8px

  AtlasWidget
    id: healthWidget
    anchors.bottom: background.bottom
    anchors.right: background.left
    image-clip: 2781 1809 194 21
    size: 194 21
    margin-right: 24
    phantom: true
    
    AtlasWidget
      id: health
      image-clip: 2781 1902 194 21
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      size: 194 21
      phantom: true
    
    SpellBarInformationLabel
      id: healthLabel
      anchors.left: parent.left
      anchors.bottom: parent.bottom
      margin-left: 20
      margin-bottom: 2
      !text: '250/250'
  
  Panel
    id: badConditionPanel
    anchors.bottom: prev.top
    anchors.right: background.left
    size: 180 18
    margin-right: 18
    margin-bottom: 11
    layout:
      type: grid
      cell-size: 18 18
      flow: true
      num-columns: 10
  
  AtlasWidget
    id: healthPotion
    anchors.top: background.verticalcenter
    anchors.horizontalcenter: background.left
    image-clip: 2696 1816 37 37
    size: 37 37
    margin-right: 12
    margin-top: -2
    phantom: true
    
    UIItem
      id: item
      size: 32 32
      image-source: /ui/blank
      anchors.centerIn: parent
      virtual: true
      enabled: false
      phantom: true
      margin-bottom: 4
      margin-right: 4

  SpellBarInformationLabel
    id: healthPotionAmount
    anchors.bottom: prev.bottom
    anchors.right: prev.right
    margin-right: 8
    !text: '0'
    
  AtlasButton
    id: minimize
    anchors.verticalcenter: next.verticalcenter
    anchors.left: next.right
    size: 16 16
    image-clip: 1522 773 16 16
    visible: false
    @onClick: modules.game_spellbar.m_SpellBarFunctions.onChangeSize(-1)
    !tooltip: 'Minimize'
    margin-left: 6

  AtlasButton
    id: rotate
    anchors.horizontalcenter: parent.horizontalcenter
    anchors.top: parent.top
    size: 16 16
    image-clip: 1518 663 16 16
    @onClick: modules.game_spellbar.m_SpellBarFunctions.onRotate()
    !tooltip: 'Rotate'
    margin-top: 64
    $on:
      margin-top: 0

  AtlasButton
    id: maximize
    anchors.verticalcenter: prev.verticalcenter
    anchors.right: prev.left
    size: 16 16
    image-clip: 1491 662 16 16
    @onClick: modules.game_spellbar.m_SpellBarFunctions.onChangeSize(1)
    !tooltip: 'Maximize'
    margin-right: 6

AttackIcon < UIWidget
  anchors.left: parent.left
  size: 32 32
  phantom: false
  image-source: /images/leaf/blank
  $on:
    margin-bottom: 5
  
  UIProgressRect
    id: progress
    anchors.fill: parent
    background: #006ad1
    percent: 100
    phantom: true
  
  UIWidget
    id: icon
    anchors.fill: parent
    margin: 2
    image-source: /images/leaf/spellWindow/spellIcons
    phantom: true
    icon-size: 28 28
  
  Label
    id: hotkey
    anchors.left: parent.left
    anchors.top: parent.top
    color: #FF0000
    margin-top: 3
    margin-left: 4
    text-auto-resize: true
    font: verdana-11px-rounded
  
  Label
    id: cooldown
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    margin-bottom: 2
    margin-right: 2
    text-auto-resize: true
    font: verdana-11px-rounded
    color: #E4CEBA

AttackIconVertical < AttackIcon
  anchors.top: parent.top
  margin-top: 4

AttackIconHorizontal < AttackIcon
  anchors.bottom: parent.bottom
  margin-bottom: 5

SpellShadow < UIWidget
  image-source: /images/leaf/spellWindow/spellIcons
  visible: false
  focusable: false
  size: 26 26
  opacity: 0.80