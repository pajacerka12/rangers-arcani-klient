monsters = {
	{ name = 'Calamary', file = 'Cirith/Bosses/Quest/Deepling Servant.xml', look = {type = 696},
		description = {'Ka�amarnica', 'Calamary'}, experience = 0, health = 7500, healthMax = 7500
	},
	{ name = 'Jellyfish', file = 'Cirith/Bosses/Quest/Deepling Servant.xml', look = {type = 697},
		description = {'Meduza', 'Jellyfish'}, experience = 0, health = 7500, healthMax = 7500
	},
	{ name = 'Fish', file = 'Cirith/Bosses/Quest/Deepling Servant.xml', look = {type = 699},
		description = {'Ryba', 'Fish'}, experience = 0, health = 7500, healthMax = 7500
	},
	{ name = 'Northern Pike', file = 'Cirith/Bosses/Quest/Deepling Servant.xml', look = {type = 698},
		description = {'Szczupak P�nocny', 'Northern Pike'}, experience = 0, health = 7500, healthMax = 7500
	},
	{ name = 'Gashadokuro', file = 'Cirith/Bosses/Gashadokuro.xml', look = {type = 292}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Gashadokuro', 'Gashadokuro'}, experience = 100000, health = 200000, healthMax = 200000,
		elements = {
			{ type = 'death', value = 25 },
			{ type = 'physical', value = 5 },
			{ type = 'bleed', value = -5 },
			{ type = 'holy', value = 10 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 18338, count = 3, chance = 50000}, -- red gem
			{id = 11694, count = 3, chance = 25000}, -- fine fabric
			{id = 17941, count = 3, chance = 25000}, -- radiating powder
			{id = 5878, count = 3, chance = 25000}, -- hardened leather
			{id = 9971, count = 3, chance = 25000}, -- gold ignot
			{id = 5887, count = 3, chance = 25000}, -- piece of royal steel
			{id = 5892, count = 3, chance = 25000}, -- steel
			
			{id = {2096, 2097}, chance = 10000}, -- pumpkin
			
			{id = 17940, chance = 10000}, -- pure mithril
			{id = 14504, chance = 10000}, -- magical thread
			{id = 14493, chance = 10000}, -- forbund
			{id = 13163, chance = 10000}, -- steel edge
			{id = 13164, chance = 10000}, -- royal steel edge
			{id = 13165, chance = 10000}, -- golden edge
			
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			
			{id = 13165, chance = 2500}, -- mithril edge
			{id = 18046, chance = 2500}, -- mithril shard
			{id = 14494, chance = 2500}, -- dark mithril
			{id = 10503, chance = 5000}, -- present
			{id = 16356, chance = 5000}, -- bag
		}
	},
	{ name = 'Kerharezos', file = 'Cirith/Bosses/Quest/Kerharezos.xml', look = {type = 660, addons = 3},
		description = {'Kerharezos', 'Kerharezos'}, experience = 100000, health = 200000, healthMax = 200000,
		elements = {
			{ type = 'death', value = 100 },
			{ type = 'physical', value = 5 },
			{ type = 'drown', value = 5 },
			{ type = 'bleed', value = 5 },
			{ type = 'holy', value = 5 },
			{ type = 'toxic', value = 5 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 5 },
			{ type = 'wind', value = 5 },
		},
		loot = {
			{id = 2157, chance = 6140}, -- gold nugget
			{id = 11217, chance = 6970}, -- essence of a bad dream
			{id = 13728, chance = 5180}, -- dirty turban
			{id = 10562, chance = 6830}, -- book of prayers
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 8844, count = 3, chance = 31990}, -- pepper
			{id = 8841, count = 2, chance = 25120}, -- lemon
			{id = 14493, chance = 4150}, -- forbund
			{id = 2172, chance = 1150}, -- bronze amulet
			{id = 6569, count = {2, 12}, chance = 41150}, -- candy
			{id = 14346, chance = 200, uniqueDrop = 1}, -- djinn amulet
			{id = 2195, chance = 4500}, --  boh
			{id = 11694, chance = 27730}, -- fine fabric
			{id = 13001, chance = 3440}, -- void legs
			{id = 13999, chance = 4150, uniqueDrop = 1}, -- noble armor
			{id = 2537, chance = 5150}, -- haunted helmet
			{id = 17705, chance = 1500, uniqueDrop = 1}, -- bone of a fire staff
			{id = 2446, chance = 1820}, -- pharaoh sword
			{id = {11680, 11810}, chance = 2500, uniqueDrop = 1}, -- lightning bow/fiery bow
			{id = 2440, chance = 3200}, -- arydian axe
			{id = 13952, chance = 5000, uniqueDrop = 1}, -- crystal spear
			{id = 2144, chance = 36770}, -- black pearl
			{id = {7838, 7840, 11443, 11440, 11435, 11436}, count = 15, chance = 36760}, -- flash/flaming arrow/flash bolt/crystalline arrow/bolt
			{id = 7620, count = 3, chance = 11670}, -- mana potion
			{id = 7618, count = 4, chance = 24180}, -- health potion
			{id = 1869, chance = 53030}, -- red tapestry
			{id = 5911, count = 2, chance = 22730}, -- red piece of cloth
			{id = 2156, chance = 9520}, -- red gem
			{id = 2150, count = 3, chance = 19870}, -- small amethyst
			{id = 2147, count = 4, chance = 21700}, -- small ruby
			{id = 2159, count = 6, chance = 46190}, -- scarab coin
			{id = 2152, count = 2, chance = 95100}, -- silver
			{id = 2148, count = {25, 90}, chance = 92410}, -- copper coin
		}
	},
	{ name = 'Obujos', file = 'Cirith/Bosses/Quest/Obujos.xml', look = {type = 504}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Obujos', 'Obujos'}, experience = 150000, health = 800000, healthMax = 800000,
		elements = {
			{ type = 'earth', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'death', value = 35 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 14494, chance = 1000, uniqueDrop = 1}, -- dark mithril
			{id = 2361, chance = 500, uniqueDrop = 1}, -- frozen starlight
			{id = 15748, chance = 7240}, -- deepling crossbow
			{id = 2168, chance = 92480}, -- life ring
			{id = 11800, chance = 5000, uniqueDrop = 1}, -- frozen heart backpack
			{id = 16193, chance = 3200}, -- frozen shield
			{id = 2145, count = {5, 12}, chance = 85190}, -- small diamond
			{id = 7591, count = 15, chance = 39800}, -- GHP
			{id = 16781, chance = 10000, uniqueDrop = 1}, -- obujos eye
			{id = 7590, count = 15, chance = 44670}, -- GMP
			{id = {14026, 14027, 14028, 14029, 14030, 14031, 14032}, chance = 15000}, -- mechanical fish
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 13991, chance = 1200, uniqueDrop = 1}, -- mythril armor
			{id = 2240, chance = 71890}, -- trash
			{id = 2226, chance = 72190}, -- trash
			{id = 2146, count = {5, 15}, chance = 95480}, -- small sapphire
			{id = 17329, chance = 9240}, -- reef blade
			{id = 2168, chance = 62500}, -- life ring
			{id = 13299, chance = 18500}, -- glass spear
			{id = 13894, chance = 23500}, -- frosty rapier
			{id = 18267, chance = 500, uniqueDrop = 1}, -- crystal shield
			{id = 2152, count = {5, 15}, chance = 95100}, -- silver
			{id = 2128, count = 6, chance = 50100}, -- crystal coin
			{id = 5895, chance = 99510}, -- fish fin
			{id = 13996, chance = 10400, uniqueDrop = 1}, -- crystalline armor
			{id = 11397, chance = 13100, uniqueDrop = 1}, -- diamond armor
			{id = 16189, chance = 17200}, -- frosty boots
			{id = 16136, chance = 2500, uniqueDrop = 1}, -- golden boots
			{id = 13063, chance = 1000, uniqueDrop = 1}, -- golden bellows of austerity
		}
	},
	
	{ name = 'Tanjis Distance Servant', file = 'Cirith/Bosses/Quest/Tanjis Distance Servant.xml', look = {type = 526},
		description = {'S�uga Tanjisa', 'Tanjis Servant'}, experience = 0, health = 6000, healthMax = 6000,
		elements = {
			{ type = 'death', value = 10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
	},
	{ name = 'Tanjis Melee Servant', file = 'Cirith/Bosses/Quest/Tanjis Melee Servant.xml', look = {type = 525},
		description = {'S�uga Tanjisa', 'Tanjis Servant'}, experience = 0, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'death', value = 10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
	},
	{ name = 'Tanjis', file = 'Cirith/Bosses/Quest/Tanjis.xml', look = {type = 505}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Tanjis', 'Tanjis'}, experience = 150000, health = 800000, healthMax = 800000,
		elements = {
			{ type = 'earth', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'death', value = 35 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 2128, count = 6, chance = 50100}, -- crystal coin
			{id = 16164, chance = 15700}, -- black steel shield
			{id = 13928, chance = 15300, uniqueDrop = 1}, -- shield of invaders
			{id = 7697, chance = 15200, uniqueDrop = 1}, -- ruby ring
			{id = 2124, chance = 15600}, -- crystal ring
			{id = 13950, chance = 24600}, -- frosty spear
			{id = 2168, chance = 62500}, -- life ring
			{id = 13060, chance = 1000, uniqueDrop = 1}, -- golden anvil of quality
			{id = 2177, count = {3, 35}, chance = 34600}, -- life crystal
			{id = 2146, count = {5, 45}, chance = 95480}, -- small sapphire
			{id = 2145, count = {5, 45}, chance = 85190}, -- small diamond
			{id = {7632, 7633}, count = {2, 6}, chance = 45400}, -- giant pearl
			{id = 14494, chance = 1000, uniqueDrop = 1}, -- dark mithril
			{id = 14028, count = 6, chance = 35190}, -- strength fish
			{id = 2361, chance = 500, uniqueDrop = 1}, -- frozen starlight
			{id = 17637, chance = 1000, uniqueDrop = 1}, -- rare weapon
			{id = 14479, chance = 35600}, -- strange helmet
		}
	},
	
	{ name = 'Jaul', file = 'Cirith/Bosses/Quest/Jaul.xml', look = {type = 506}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Jaul', 'Jaul'}, experience = 150000, health = 800000, healthMax = 800000,
		elements = {
			{ type = 'earth', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'death', value = 35 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 2128, count = 6, chance = 50100}, -- crystal coin
			{id = 13794, chance = 15700}, -- titan helmet
			{id = 13009, chance = 15300, uniqueDrop = 1}, -- Dragon rider helmet
			{id = 12954, chance = 15200, uniqueDrop = 1}, -- marksman legs
			{id = 16190, chance = 15600}, -- frosty legs
			{id = 2168, chance = 62500}, -- life ring
			{id = 16194, chance = 14600, uniqueDrop = 1}, -- frosty mace
			{id = 13057, chance = 1000, uniqueDrop = 1}, -- golden clever of fortune
			{id = 2177, count = {3, 35}, chance = 34600}, -- life crystal
			{id = 2146, count = {5, 45}, chance = 95480}, -- small sapphire
			{id = 2145, count = {5, 45}, chance = 85190}, -- small diamond
			{id = {7632, 7633}, count = {2, 6}, chance = 45400}, -- giant pearl
			{id = 14494, chance = 1000, uniqueDrop = 1}, -- dark mithril
			{id = 14030, count = 6, chance = 35190}, -- condition fish
			{id = 2361, chance = 500, uniqueDrop = 1}, -- frozen starlight
			{id = 13303, chance = 1000, uniqueDrop = 1}, -- rare weapon
			{id = 13952, chance = 35600}, -- crystal spear
		}
	},
	
	
	
	
	
	
	{ name = 'Trainer', file = 'Aa/Trainer.xml', look = {type = 0, auxType = 5787, typeEx = 5787},
		description = {'Trainer', 'Trainer'}, experience = 0, health = 25000, healthMax = 25000 },
	{ name = 'Trainer 800 armor', file = 'Aa/Trainer 800.xml', look = {type = 0, auxType = 5787, typeEx = 5787},
		description = {'Trainer 800 armor', 'Trainer 800 armor'}, experience = 0, health = 2500000, healthMax = 2500000 },
	
	{ name = 'Grynch Clan Goblin', file = 'Cirith/Bosses/Quest/Grynch Clan Goblin.xml', look = {type = 61}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Grynch Clan Goblin', 'Grynch Clan Goblin'}, experience = 0, health = 500, healthMax = 500,
		loot = {
			{id = 2560, chance = 70}, -- mirror
			{id = 2120, chance = 80}, -- rope
			{id = 1685, chance = 80}, -- pillow
			{id = 2114, chance = 80}, -- piggy bank
			{id = {1854, 1853}, chance = 80}, -- picture
			{id = 2072, chance = 10}, -- lute
			{id = 5887, chance = 100}, -- piece of royal steel
			{id = 2159, chance = 110}, -- scarab coin
			{id = 17017, chance = 120}, -- dice
			{id = 2162, chance = 120}, -- magic light wand
			{id = 2260, chance = 120}, -- rune
			{id = 5892, chance = 130}, -- steel
			{id = 17941, chance = 150}, -- radiating powder
			{id = 16356, chance = 260}, -- bag
			{id = 11694, chance = 270}, -- fine fabric
			{id = 9971, chance = 280}, -- gold ignot
			{id = 5878, chance = 300}, -- hardened leather
			{id = 2679, count = 4, chance = 360}, -- cherry
			{id = 6501, count = 2, chance = 370}, -- gingerbreadman
			{id = 5890, count = 5, chance = 390}, -- chicken feather
			{id = 14696, chance = 410}, -- honeycomb
			{id = 18338, chance = 420}, -- red gem
			{id = 2324, chance = 430}, -- broom
			{id = 2134, chance = 470}, -- silver brooch
			{id = 2007, chance = 560}, -- bottle
			{id = 5894, count = 3, chance = 700}, -- bat wing
			{id = 2661, chance = 810}, -- scarf
			{id = 11118, count = 5, chance = 920}, -- snowman package
			{id = 7910, count = 5, chance = 980}, -- peanut
			{id = 2688, count = 3, chance = 1010}, -- candy cane
			{id = 2695, count = 2, chance = 1020}, -- egg
			{id = 7909, count = 5, chance = 2050}, -- walnut
			{id = 2675, count = 3, chance = 4980}, -- orange
			{id = 2687, count = 5, chance = 8130}, -- cookie
			{id = 2674, count = 3, chance = 10220}, -- apple
			{id = 2111, count = 5, chance = 14890}, -- snowball
			{id = 6527, chance = 14890}, -- chrismas token
			{id = 2148, count = 22, chance = 29890}, -- copper coins
		}
	},
	
	-- Spell
	{ name = 'Igris', file = 'Cirith/Bosses/Spells/Magic Friend/Igris.xml', look = {type = 702},
		description = {'Igris', 'Igris'}, experience = 0, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Holy Magic Friend', file = 'Cirith/Bosses/Spells/Magic Friend/Holy Magic Friend.xml', look = {type = 180},
		description = {'Magiczny Przyjaciel', 'Magic Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Death Magic Friend', file = 'Cirith/Bosses/Spells/Magic Friend/Death Magic Friend.xml', look = {type = 183},
		description = {'Magiczny Przyjaciel', 'Magic Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Energy Magic Friend', file = 'Cirith/Bosses/Spells/Magic Friend/Energy Magic Friend.xml', look = {type = 181},
		description = {'Magiczny Przyjaciel', 'Magic Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Earth Magic Friend', file = 'Cirith/Bosses/Spells/Magic Friend/Earth Magic Friend.xml', look = {type = 182},
		description = {'Magiczny Przyjaciel', 'Magic Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Fire Magic Friend', file = 'Cirith/Bosses/Spells/Magic Friend/Fire Magic Friend.xml', look = {type = 435},
		description = {'Magiczny Przyjaciel', 'Magic Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Ice Magic Friend', file = 'Cirith/Bosses/Spells/Magic Friend/Ice Magic Friend.xml', look = {type = 423},
		description = {'Magiczny Przyjaciel', 'Magic Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Wolf Friend', file = 'Cirith/Bosses/Spells/Wolf Friend.xml', look = {type = 645},
		description = {'Wilk', 'Wolf Friend'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'drown', value = 45 },
			{ type = 'bleed', value = 45 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = 45 },
			{ type = 'toxic', value = 45 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 45 },
		} },
	{ name = 'Earth Barricade', file = 'Cirith/Bosses/Spells/Earth Barricade.xml', look = {type = 0, typeEx = 1499, auxType = 2130},
		description = {'Le�na Barykada', 'Earth Barricade'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'fire', value = -5 },
		} },
	
	{ name = 'Terrifying Totem', file = 'Cirith/Bosses/Spells/Terrifying Totem.xml', look = {type = 0, typeEx = 18448, auxType = 15726},
		description = {'Przera�aj�cy Totem', 'Terrifying Totem'}, experience = 0, health = 150, healthMax = 150 },
	{ name = 'Wolf Familiar', file = 'Cirith/Bosses/Spells/wolf familiar.xml', look = {type = 27},
		description = {'Wilczy Chowaniec', 'Wolf Familiar'}, experience = 0, health = 1000000, healthMax = 1000000,
		elements = {
			{ type = 'physical', value = 85 },
			{ type = 'fire', value = -10 },
		} },
	{ name = 'Fire Familiar', file = 'Cirith/Bosses/Spells/fire familiar.xml', look = {type = 49},
		description = {'Ognisty Chowaniec', 'Fire Familiar'}, experience = 0, health = 1000000, healthMax = 1000000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -30 },
		} },
	{ name = 'Ice Wall', file = 'Cirith/Bosses/Spells/ice wall.xml', look = {type = 0, typeEx = 11444, auxType = 10528},
		description = {'Lodowa �ciana', 'Ice Wall'}, experience = 0, health = 150000, healthMax = 150000 },
	{ name = 'Elemental Wolf', file = 'Cirith/Bosses/Spells/elemental wolf.xml', look = {type = 423},
		description = {'Wilk �ywio��w', 'Elemental Wolf'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Explosion Wolf', file = 'Cirith/Bosses/Spells/explosion wolf.xml', look = {type = 435},
		description = {'Ognisty Wilk', 'Explosion Wolf'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Bird', file = 'Cirith/Bosses/Spells/Bird.xml', look = {type = 223},
		description = {'Ptak', 'Bird'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Forest Wall', file = 'Cirith/Bosses/Spells/forest wall.xml', look = {type = 0, typeEx = 1499, auxType = 2130},
		description = {'�ciana Lasu', 'Forest Wall'}, experience = 0, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'fire', value = -5 },
		} },
	{ name = 'Fallen Hare', level = 1, file = 'Cirith/Bosses/Spells/fallen hare.xml', look = {type = 74},
		description = {'Op�tany Zaj�c', 'Fallen Hare'}, experience = 0, health = 500, healthMax = 500 },
	{ name = 'Fallen Knight', level = 1, file = 'Cirith/Bosses/Spells/fallen knight.xml', look = {type = 73},
		description = {'Op�tany Rycerz', 'Fallen Knight'}, experience = 0, health = 650, healthMax = 650 },
	{ name = 'Wild Wolf', level = 1, file = 'Cirith/Bosses/Spells/Wild Wolf.xml', look = {type = 3},
		description = {'Zdzicza�y Wilk', 'Wild Wolf'}, experience = 0, health = 3000, healthMax = 3000 },
	
	{ name = 'Barricade', file = 'Cirith/Bosses/Spells/barricade.xml', look = {type = 0, typeEx = 1499, auxType = 2130},
		description = {'Barykada', 'Barricade'}, experience = 0, health = 100000, healthMax = 100000 },
	{ name = 'Familiar', file = 'Cirith/Bosses/Spells/familiar.xml', look = {type = 469},
		description = {'Chowaniec', 'Familiar'}, experience = 0, health = 100000, healthMax = 100000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 65 },
		} },
	{ name = 'Undead Familiar', file = 'Cirith/Bosses/Spells/undead familiar.xml', look = {type = 101},
		description = {'Nieumar�y Chowaniec', 'Undead Familiar'}, experience = 0, health = 100000, healthMax = 100000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = -25 },
			{ type = 'death', value = 90 },
			{ type = 'earth', value = 90 },
			{ type = 'ice', value = 50 },
		} },
	{ name = 'Energy Familiar', file = 'Cirith/Bosses/Spells/energy familiar.xml', look = {type = 293},
		description = {'Energetyczny Chowaniec', 'Energy Familiar'}, experience = 0, health = 1000000, healthMax = 1000000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -30 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 10 },
		} },
	{ name = 'Earth Familiar', file = 'Cirith/Bosses/Spells/earth familiar.xml', look = {type = 301},
		description = {'Ziemny Chowaniec', 'Earth Familiar'}, experience = 0, health = 100000, healthMax = 100000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 90 },
			{ type = 'earth', value = 90 },
			{ type = 'fire', value = -25 },
			{ type = 'ice', value = 85 },
		} },
	{ name = 'Mechanical Spider', file = 'Cirith/Bosses/Spells/mechanical spider.xml', look = {type = 30},
		description = {'Mechaniczny Paj�k', 'Mechanical Spider'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Deadly Shadow', file = 'Cirith/Bosses/Spells/deadly shadow.xml', look = {type = 530},
		description = {'Cie�', 'Deadly Shadow'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	
	-- Alammred
	{ name = 'Fire Guardian', file = 'Cirith/Bosses/Quest/Alammred/Fire Guardian.xml', look = {type = 242},
		description = {'Obro�ca Ognia', 'Fire Guardian'}, experience = 15000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 35 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
		} },
	{ name = 'Icy Guardian', file = 'Cirith/Bosses/Quest/Alammred/Icy Guardian.xml', look = {type = 674},
		description = {'Obro�ca Lodu', 'Icy Guardian'}, experience = 15000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = 35 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 100 },
		} },
	{ name = 'Stone Guardian', file = 'Cirith/Bosses/Quest/Alammred/Stone Guardian.xml', look = {type = 285},
		description = {'Obro�ca Ziemi', 'Stone Guardian'}, experience = 15000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 85 },
		} },
	{ name = 'Energy Guardian', file = 'Cirith/Bosses/Quest/Alammred/Energy Guardian.xml', look = {type = 290},
		description = {'Obro�ca Energii', 'Energy Guardian'}, experience = 15000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		} },
	
	-- Castle Demon
	{ name = 'Askarak Guide', file = 'Cirith/Bosses/Quest/Castle Demon/Askarak Guide.xml', look = {type = 630, head = 98, body = 118, legs = 99, feet = 78},
		description = {'Askarak Guide', 'Askarak Guide'}, experience = 8000, health = 12000, healthMax = 12000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 70 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = -15 },
			{ type = 'wind', value = 50 },
		} },
	{ name = 'Shaburak Guide', file = 'Cirith/Bosses/Quest/Castle Demon/Shaburak Guide.xml', look = {type = 630, head = 94, body = 96, legs = 94, feet = 77},
		description = {'Shaburak Guide', 'Shaburak Guide'}, experience = 8000, health = 12000, healthMax = 12000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 70 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
			{ type = 'wind', value = 50 },
		} },
	
	{ name = 'Pillar', file = 'Cirith/Bosses/Quest/Pillar.xml', look = {type = 0, typeEx = 17760, auxType = 16838},
		description = {'Filar', 'Pillar'}, experience = 0, health = 1, healthMax = 50000 },
	{ name = 'Altar Servant', file = 'Cirith/Bosses/Quest/Altar Servant.xml', look = {type = 639},
		description = {'S�uga O�tarza', 'Altar Servant'}, experience = 450000, health = 400000, healthMax = 400000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'drown', value = 25 },
			{ type = 'bleed', value = 25 },
			{ type = 'holy', value = 25 },
			{ type = 'death', value = 25 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 25 },
			{ type = 'wind', value = 25 },
		} },
	
	-- Snake Sceptre Quest
	{ name = 'Criredric', file = 'Cirith/Bosses/Quest/Criredric.xml', look = {type = 68}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Criredric', 'Criredric'}, experience = 2000, health = 4000, healthMax = 4000,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
		},
		loot = {
			{id = 982, count = 3, chance = 1000}, -- dark talon
			{id = 6300, chance = 7580}, -- death ring
			{id = 2238, chance = 16970}, -- worn leather boots
			{id = 7588, count = 3, chance = 13480}, -- SHP
			{id = 2747, chance = 14700}, -- grave flower
			{id = 2229, count = 3, chance = 22980}, -- skull
			{id = 10601, chance = 10000}, -- vamp teeth
			{id = 5905, chance = 100000}, -- vamp dust
			{id = 11654, chance = 100000}, -- part of snake sceptre
			{id = 2152, count = {2, 4}, chance = 11550}, -- silver
			{id = 2148, count = {19, 40}, chance = 11550}, -- gold
		}
	},
	{ name = 'Tri Poison Lamprey', file = 'Cirith/Bosses/Quest/Tri Poison Lamprey.xml', look = {type = 632, addons = 7}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Tri Poison Lamprey', 'Tri Poison Lamprey'}, experience = 5000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'ice', value = 40 },
		},
		loot = {
			{id = 7589, chance = 56480}, -- SMP
			{id = 2214, chance = 41170}, -- RoH
			{id = 11193, chance = 90220}, -- hydra head
			{id = 2195, chance = 2000}, -- boots of haste
			{id = 2146, chance = 54920}, -- small sapphire
			{id = 2536, chance = 1000}, -- medusa shield
			{id = 11653, chance = 100000}, -- part of snake sceptre
			{id = 2666, count = {4, 6}, chance = 70000}, -- meat
			{id = 2152, count = {2, 4}, chance = 11550}, -- silver
			{id = 2148, count = {19, 40}, chance = 11550}, -- gold
		}
	},
	
	{ name = 'Glitterscale', file = 'Cirith/Bosses/Quest/Glitterscale.xml', look = {type = 562}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Glitterscale', 'Glitterscale'}, experience = 4500, health = 9000, healthMax = 9000,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2455, chance = 61000}, -- crossbow
			{id = 7588, chance = 8990}, -- SHP
			{id = 5877, chance = 67920}, -- green dragon leather
			{id = 2187, chance = 2500}, --WoI
			{id = 2145, chance = 9350}, -- small diamond
			{id = 5882, chance = 78480}, -- red dragon scale
			{id = 2672, chance = 64780}, -- dragon ham
			{id = 2546, count = 10, chance = 4540}, -- burst arrow
			{id = 11652, chance = 100000}, -- part of snake sceptre
			{id = 2152, count = {2, 4}, chance = 11550}, -- silver
			{id = 2148, count = {19, 40}, chance = 11550}, -- gold
		}
	},
	
	-- Elemental Quest
	{ name = 'Nesh', file = 'Cirith/Bosses/Quest/Elemental Quest/Nesh.xml', look = {type = 242},
		description = {'Nesh', 'Nesh'}, experience = 7500, health = 41000, healthMax = 41000,
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 16203, chance = 500}, -- hellfire helmet
			{id = 11679, chance = 500}, -- burning bow
			{id = 10519, chance = 1000}, -- orange backpack
			{id = 13995, chance = 1440}, -- lavos armor
			{id = 2187, chance = 3520}, -- woi
			{id = 2156, chance = 4520}, -- red gem
			{id = 2392, chance = 5380}, -- fire sword
			{id = 10218, chance = 14530, subType = 10}, -- hellfire lavos amulet
			{id = 2147, count = 3, chance = 15000}, -- small ruby
			{id = 7590, count = 3, chance = 26500}, -- GMP
			{id = 7589, count = 2, chance = 28790}, -- SMP
			{id = 7840, count = 21, chance = 46760}, -- flaming arrow
			{id = 2152, count = 2, chance = 51380}, -- silver coin
			{id = 2148, count = 65, chance = 98900}, -- gold
			{id = 10552, chance = 100000}, -- fiery heart
			{id = 8304, chance = 100000}, -- eternal flames
		}
	},
	{ name = 'Greas', file = 'Cirith/Bosses/Quest/Elemental Quest/Greas.xml', look = {type = 285},
		description = {'Greas', 'Greas'}, experience = 7500, health = 41000, healthMax = 41000,
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 16208, chance = 500}, -- meteorite helmet
			{id = 8856, chance = 500}, -- bettlebow
			{id = 1998, chance = 1000}, -- green backpack
			{id = 16167, chance = 1440}, -- swamplair armor
			{id = 2155, chance = 4520}, -- green gem
			{id = 7435, chance = 5380}, -- impaler
			{id = 10219, chance = 14530, subType = 10}, -- sacred tree amulet
			{id = 2149, count = 3, chance = 15000}, -- small emerald
			{id = 7590, count = 3, chance = 26500}, -- GMP
			{id = 7589, count = 2, chance = 28790}, -- SMP
			{id = 7850, count = 21, chance = 46760}, -- earth arrow
			{id = 2152, count = 2, chance = 51380}, -- silver coin
			{id = 2148, count = 65, chance = 98900}, -- gold
			{id = 11216, chance = 100000}, -- lump of earth
			{id = 8305, chance = 100000}, -- mother soil
		}
	},
	{ name = 'Shiva', file = 'Cirith/Bosses/Quest/Elemental Quest/Shiva.xml', look = {type = 11},
		description = {'Shiva', 'Shiva'}, experience = 7500, health = 41000, healthMax = 41000,
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 16192, chance = 500}, -- frozen helmet
			{id = 8858, chance = 500}, -- icicle bow
			{id = 2002, chance = 1000}, -- blue backpack
			{id = 14005, chance = 1440}, -- wizard's robe
			{id = 14486, chance = 3520}, -- glacier hood
			{id = 2158, chance = 4520}, -- blue gem
			{id = 13894, chance = 5380}, -- frosty rapier
			{id = 10220, chance = 14530, subType = 10}, -- leviathan amulet
			{id = 2146, count = 3, chance = 15000}, -- small sapphire
			{id = 7590, count = 3, chance = 26500}, -- GMP
			{id = 7589, count = 2, chance = 28790}, -- SMP
			{id = 7839, count = 21, chance = 46760}, -- shiver arrow
			{id = 2152, count = 2, chance = 51380}, -- silver coin
			{id = 2148, count = 65, chance = 98900}, -- gold
			{id = 7441, chance = 99710}, -- ice cube
			{id = 8300, chance = 100000}, -- flawless ice crystal
		}
	},
	{ name = 'Deleder', file = 'Cirith/Bosses/Quest/Elemental Quest/Deleder.xml', look = {type = 290},
		description = {'Deleder', 'Deleder'}, experience = 7500, health = 41000, healthMax = 41000,
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 14166, chance = 500}, -- sallet
			{id = 11680, chance = 500}, -- lightning bow
			{id = 2001, chance = 1000}, -- purple backpack
			{id = 14009, chance = 1440}, -- sparkling cape
			{id = 8920, chance = 3520}, -- thunderstorm rod
			{id = 2153, chance = 4520}, -- violet gem
			{id = 13938, chance = 5380}, -- thunderstorm crossbow
			{id = 10221, chance = 14530, subType = 10}, -- shockwave amulet
			{id = 2150, count = 3, chance = 15000}, -- small amethyst
			{id = 7590, count = 3, chance = 26500}, -- GMP
			{id = 7589, count = 2, chance = 28790}, -- SMP
			{id = 7838, count = 21, chance = 46760}, -- flash arrow
			{id = 2152, count = 2, chance = 51380}, -- silver coin
			{id = 2148, count = 65, chance = 98900}, -- gold
			{id = 8306, chance = 100000}, -- pure energy
		}
	},
	{ name = 'Fire Defender', file = 'Cirith/Bosses/Quest/Elemental Quest/Fire Defender.xml', look = {type = 49},
		description = {'Nesh', 'Nesh'}, experience = 0, health = 55000, healthMax = 55000,
		elements = {
			{ type = 'fire', value = 100 },
		} },
	{ name = 'Earth Defender', file = 'Cirith/Bosses/Quest/Elemental Quest/Earth Defender.xml', look = {type = 301},
		description = {'Greas', 'Greas'}, experience = 0, health = 55000, healthMax = 55000,
		elements = {
			{ type = 'earth', value = 100 },
		} },
	{ name = 'Ice Defender', file = 'Cirith/Bosses/Quest/Elemental Quest/Ice Defender.xml', look = {type = 286},
		description = {'Shiva', 'Shiva'}, experience = 0, health = 55000, healthMax = 55000,
		elements = {
			{ type = 'ice', value = 100 },
		} },
	{ name = 'Energy Defender', file = 'Cirith/Bosses/Quest/Elemental Quest/Energy Defender.xml', look = {type = 293},
		description = {'Deleder', 'Deleder'}, experience = 0, health = 55000, healthMax = 55000,
		elements = {
			{ type = 'energy', value = 100 },
		} },
	{ name = 'Fire Guard', file = 'Cirith/Bosses/Quest/Elemental Quest/Fire Guard.xml', look = {type = 49},
		description = {'�ywio�ak Ognia', 'Fire Elemental'}, experience = 500, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13981, chance = 200, subType = 120}, -- magma amulet
			{id = 14447, chance = 490}, -- woi
			{id = 13951, chance = 40}, -- spear
			{id = 2147, chance = 5900}, -- small ruby
			{id = 2152, chance = 25900}, -- silver
			{id = 2148, count = 5, chance = 74760}, -- gold
		}
	},
	{ name = 'Earth Guard', file = 'Cirith/Bosses/Quest/Elemental Quest/Earth Guard.xml', look = {type = 301},
		description = {'�ywio�ak Ziemi', 'Earth Elemental'}, experience = 500, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 85 },
		},
		loot = {
			{id = 13980, chance = 200, subType = 120}, -- terra amulet
			{id = 13948, chance = 40}, -- spear
			{id = 2149, chance = 5900}, -- small ruby
			{id = 2152, chance = 25900}, -- silver
			{id = 2148, count = 5, chance = 74760}, -- gold
		}
	},
	{ name = 'Ice Guard', file = 'Cirith/Bosses/Quest/Elemental Quest/Ice Guard.xml', look = {type = 286},
		description = {'�ywio�ak Lodu', 'Ice Elemental'}, experience = 500, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = -5 },
			{ type = 'fire', value = 90 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 13982, chance = 200, subType = 120}, -- glacier amulet
			{id = 13950, chance = 40}, -- spear
			{id = 2146, chance = 5900}, -- small ruby
			{id = 2152, chance = 25900}, -- silver
			{id = 2148, count = 5, chance = 74760}, -- gold
		}
	},
	{ name = 'Energy Guard', file = 'Cirith/Bosses/Quest/Elemental Quest/Energy Guard.xml', look = {type = 293},
		description = {'�ywio�ak Energii', 'Energy Elemental'}, experience = 500, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 13983, chance = 200, subType = 120}, -- lightning amulet
			{id = 14459, chance = 490}, -- woi
			{id = 13952, chance = 40}, -- spear
			{id = 2150, chance = 5900}, -- small ruby
			{id = 2152, chance = 25900}, -- silver
			{id = 2148, count = 5, chance = 74760}, -- gold
		}
	},
	{ name = 'Fire Attacker', file = 'Cirith/Bosses/Quest/Elemental Quest/Fire Attacker.xml', look = {type = 242},
		description = {'�ywio�ak Ognia', 'Fire Elemental'}, experience = 0, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 30 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
		} },
	{ name = 'Earth Attacker', file = 'Cirith/Bosses/Quest/Elemental Quest/Earth Attacker.xml', look = {type = 285},
		description = {'�ywio�ak Ziemi', 'Earth Elemental'}, experience = 0, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 45 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
		} },
	{ name = 'Ice Attacker', file = 'Cirith/Bosses/Quest/Elemental Quest/Ice Attacker.xml', look = {type = 11},
		description = {'�ywio�ak Lodu', 'Ice Elemental'}, experience = 0, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 55 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 100 },
		} },
	{ name = 'Energy Attacker', file = 'Cirith/Bosses/Quest/Elemental Quest/Energy Attacker.xml', look = {type = 290},
		description = {'�ywio�ak Energii', 'Energy Elemental'}, experience = 0, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -15 },
			{ type = 'ice', value = 35 },
		} },
	{ name = 'Aggressive Fire Elemental', file = 'Cirith/Bosses/Quest/Elemental Quest/Aggressive Fire Elemental.xml', look = {type = 49},
		description = {'�ywio�ak Ognia', 'Fire Elemental'}, experience = 0, health = 3700, healthMax = 3700,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
		} },
	{ name = 'Aggressive Earth Elemental', file = 'Cirith/Bosses/Quest/Elemental Quest/Aggressive Earth Elemental.xml', look = {type = 301},
		description = {'�ywio�ak Ziemi', 'Earth Elemental'}, experience = 0, health = 3700, healthMax = 3700,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 40 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 85 },
		} },
	{ name = 'Aggressive Ice Elemental', file = 'Cirith/Bosses/Quest/Elemental Quest/Aggressive Ice Elemental.xml', look = {type = 286},
		description = {'�ywio�ak Lodu', 'Ice Elemental'}, experience = 0, health = 3700, healthMax = 3700,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 100 },
		} },
	{ name = 'Aggressive Energy Elemental', file = 'Cirith/Bosses/Quest/Elemental Quest/Aggressive Energy Elemental.xml', look = {type = 293},
		description = {'�ywio�ak Energii', 'Energy Elemental'}, experience = 0, health = 3700, healthMax = 3700,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -15 },
			{ type = 'ice', value = 35 },
		} },
	
	--  Madmen or Fools Quest 
	{ name = 'Ghrobar', file = 'Cirith/Bosses/Quest/Madmen of Fools/Ghrobar.xml', look = {type = 478, head = 78, body = 59, legs = 61, feet = 115},
		description = {'Ghrobar', 'Ghrobar'}, experience = 5000, health = 7500, healthMax = 7500,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 18015, chance = 100000}, -- ghrobar key
			{id = 13991, chance = 890}, -- mythril armor
			{id = 14321, chance = 25150}, -- royal steel legs
			{id = 14170, chance = 70400}, -- armor
			{id = 14350, chance = 82590}, -- iron legs
			{id = 14349, chance = 84150}, -- iron helmet
			{id = 15713, chance = 84200}, -- longsword
			{id = 2691, chance = 91990}, -- bread
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 2148, count = 15, chance = 98000}, -- coin
		}
	},
	{ name = 'Groburr', file = 'Cirith/Bosses/Quest/Madmen of Fools/Groburr.xml', look = {type = 478, head = 114, body = 47, legs = 35, feet = 115, addons = 3},
		description = {'Groburr', 'Groburr'}, experience = 5000, health = 4500, healthMax = 4500,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 7438, chance = 1500}, -- elvish bow
			{id = 2195, chance = 10150}, -- boh
			{id = 14010, chance = 41390}, -- knight armor
			{id = 11435, count = 41, chance = 75000}, -- crystalline arrow
			{id = 14461, chance = 84150}, -- soldier helm
			{id = 2691, chance = 91990}, -- bread
			{id = 2152, chance = 92420}, -- silver
			{id = 15741, chance = 94200}, -- bow
			{id = 2148, count = 16, chance = 98000}, -- coin
		}
	},
	{ name = 'Nargass', file = 'Cirith/Bosses/Quest/Madmen of Fools/Nargass.xml', look = {type = 541, head = 26, body = 45, legs = 39, feet = 115, addons = 3},
		description = {'Nargass', 'Nargass'}, experience = 5000, health = 1900, healthMax = 1900,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 25 },
		},
		loot = {
			{id = 14005, chance = 1650}, -- wizard's robe
			{id = 16307, chance = 980}, -- enhanced leather legs
			{id = 14459, chance = 43290}, -- magic rod
			{id = 8844, chance = 91990}, -- pepper
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 11, chance = 98000}, -- coin
		}
	},
	{ name = 'Asthrog', file = 'Cirith/Bosses/Quest/Madmen of Fools/Asthrog.xml', look = {type = 567, head = 38, body = 58, legs = 57, feet = 115},
		description = {'Asthrog', 'Asthrog'}, experience = 5000, health = 7500, healthMax = 7500,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 16157, chance = 1150}, -- black steel armor
			{id = 14362, chance = 30400}, -- steel armor
			{id = 14327, chance = 42890}, -- steel legs
			{id = 14363, chance = 28900}, -- steel helmet
			{id = 15729, chance = 65900}, -- double axe
			{id = 2691, chance = 91990}, -- bread
			{id = 2690, count = 2, chance = 66790}, -- roll
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 9, chance = 98000}, -- coin
		}
	},
	{ name = 'Valhida', file = 'Cirith/Bosses/Quest/Madmen of Fools/Valhida.xml', look = {type = 569, head = 96, body = 97, legs = 40, feet = 20},
		description = {'Valhida', 'Valhida'}, experience = 5000, health = 2900, healthMax = 2900,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14320, chance = 11150}, -- steel boots
			{id = 16306, chance = 20390}, -- enhanced leather armor
			{id = 2171, chance = 31400}, -- platinum amulet
			{id = 15752, chance = 42590}, -- brass legs
			{id = 14488, chance = 54150}, -- visor
			{id = 15741, chance = 4200}, -- reflective bow
			{id = 2691, chance = 81990}, -- bread
			{id = 7839, count = 19, chance = 53530}, -- shiver arrow
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 9, chance = 98000}, -- coin
		}
	},
	{ name = 'Grymasza', file = 'Cirith/Bosses/Quest/Madmen of Fools/Grymasza.xml', look = {type = 597, head = 114, body = 22, legs = 21, feet = 20},
		description = {'Grymasza', 'Grymasza'}, experience = 5000, health = 3500, healthMax = 3500,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 11234, chance = 11150}, -- expedition backpack
			{id = 14004, chance = 20390}, -- trapper boots
			{id = 2171, chance = 12900}, -- platinum amulet
			{id = 13914, chance = 31900}, -- boar loincloth
			{id = 14353, chance = 54150}, -- bronze armor
			{id = 13924, chance = 4200}, -- light crossbow
			{id = 2674, chance = 81990}, -- apple
			{id = 2690, chance = 54900}, -- roll
			{id = 11436, count = 17, chance = 53530}, -- crystalline bolt
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 2148, count = 11, chance = 98000}, -- coin
		}
	},
	{ name = 'Ghrobar Fate', file = 'Cirith/Bosses/Quest/Madmen of Fools/Ghrobar Fate.xml', look = {type = 478, head = 78, body = 59, legs = 61, feet = 115},
		description = {'Ghrobar', 'Ghrobar'}, experience = 6500, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 13991, chance = 890}, -- mythril armor
			{id = 14321, chance = 25150}, -- royal steel legs
			{id = 14170, chance = 70400}, -- armor
			{id = 14350, chance = 82590}, -- iron legs
			{id = 14349, chance = 84150}, -- iron helmet
			{id = 15713, chance = 84200}, -- longsword
			{id = 2691, chance = 91990}, -- bread
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 12, chance = 98000}, -- coin
		}
	},
	{ name = 'Erthil Fate', file = 'Cirith/Bosses/Quest/Madmen of Fools/Erthil Fate.xml', look = {type = 541, head = 114, body = 58, legs = 38, feet = 96, addons = 2},
		description = {'Erthil', 'Erthil'}, experience = 6500, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 35 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 8902, chance = 4190}, -- spellbook of mind control
			{id = 14009, chance = 150}, -- sparkling cape
			{id = 2123, chance = 980}, -- diamond ring
			{id = 16305, chance = 9860}, -- enhanced leather hood
			{id = 13910, chance = 14200}, -- wradon's staff
			{id = 2792, chance = 87900}, -- dark mushroom
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 4, chance = 98000}, -- coin
		}
	},
	{ name = 'Erthil', file = 'Cirith/Bosses/Quest/Madmen of Fools/Erthil.xml', look = {type = 541, head = 114, body = 58, legs = 38, feet = 96, addons = 2},
		description = {'Erthil', 'Erthil'}, experience = 6500, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 35 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 18017, chance = 100000}, -- erthil key
			{id = 8902, chance = 4190}, -- spellbook of mind control
			{id = 14009, chance = 150}, -- sparkling cape
			{id = 2123, chance = 980}, -- diamond ring
			{id = 16305, chance = 9860}, -- enhanced leather hood
			{id = 13910, chance = 14200}, -- wradon's staff
			{id = 2792, chance = 87900}, -- dark mushroom
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 2148, count = 4, chance = 98000}, -- coin
		}
	},
	{ name = 'Khazalth', file = 'Cirith/Bosses/Quest/Madmen of Fools/Khazalth.xml', look = {type = 413, head = 58, body = 40, legs = 20, feet = 0},
		description = {'Khazalth', 'Khazalth'}, experience = 5000, health = 7500, healthMax = 7500,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 16157, chance = 1150}, -- black steel armor
			{id = 14362, chance = 30400}, -- steel armor
			{id = 14327, chance = 42890}, -- steel legs
			{id = 14363, chance = 28900}, -- steel helmet
			{id = 15729, chance = 65900}, -- double axe
			{id = 2691, chance = 91990}, -- bread
			{id = 2690, count = 2, chance = 66790}, -- roll
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 19, chance = 98000}, -- coin
		}
	},
	{ name = 'Brundar', file = 'Cirith/Bosses/Quest/Madmen of Fools/Brundar.xml', look = {type = 194, head = 39, body = 37, legs = 77, feet = 0},
		description = {'Brundar', 'Brundar'}, experience = 5000, health = 2900, healthMax = 2900,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14320, chance = 11150}, -- steel boots
			{id = 16306, chance = 20390}, -- enhanced leather armor
			{id = 2171, chance = 31400}, -- platinum amulet
			{id = 15752, chance = 42590}, -- brass legs
			{id = 14488, chance = 54150}, -- visor
			{id = 15741, chance = 4200}, -- reflective bow
			{id = 2691, chance = 81990}, -- bread
			{id = 7839, count = 19, chance = 53530}, -- shiver arrow
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 17, chance = 98000}, -- coin
		}
	},
	{ name = 'Sorbaga', file = 'Cirith/Bosses/Quest/Madmen of Fools/Sorbaga.xml', look = {type = 264, head = 78, body = 40, legs = 20, feet = 0},
		description = {'Sorbaga', 'Sorbaga'}, experience = 5000, health = 1900, healthMax = 1900,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 14005, chance = 1650}, -- wizard's robe
			{id = 16307, chance = 980}, -- enhanced leather legs
			{id = 14459, chance = 43290}, -- magic rod
			{id = 8844, chance = 91990}, -- pepper
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 2148, count = 11, chance = 98000}, -- coin
		}
	},
	{ name = 'Leonar', file = 'Cirith/Bosses/Quest/Madmen of Fools/Leonar.xml', look = {type = 194, head = 42, body = 124, legs = 117, feet = 10},
		description = {'Leonar', 'Leonar'}, experience = 5000, health = 3500, healthMax = 3500,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 11234, chance = 11150}, -- expedition backpack
			{id = 14004, chance = 20390}, -- trapper boots
			{id = 2171, chance = 12900}, -- platinum amulet
			{id = 13914, chance = 31900}, -- boar loincloth
			{id = 14353, chance = 54150}, -- bronze armor
			{id = 13924, chance = 4200}, -- light crossbow
			{id = 2674, chance = 81990}, -- apple
			{id = 2690, chance = 54900}, -- roll
			{id = 11436, count = 17, chance = 53530}, -- crystalline bolt
			{id = 2152, chance = 92420}, -- silver
			{id = 2148, count = 21, chance = 98000}, -- coin
		}
	},
	{ name = 'He', file = 'Cirith/Bosses/Quest/He.xml', look = {type = 630, head = 0, body = 33, legs = 72, feet = 114},
		description = {'On', 'He'}, experience = 6666, health = 4200, healthMax = 4200,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
		} },
	
	{ name = 'Pit Groveller', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 76, body = 95, legs = 114, feet = 104},
		description = {'Pit Groveller', 'Pit Groveller'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Grunt', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 76, body = 95, legs = 114, feet = 104, addons = 1},
		description = {'Pit Grunt', 'Pit Grunt'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Overlord', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 76, body = 95, legs = 114, feet = 104, addons = 3},
		description = {'Pit Overlord', 'Pit Overlord'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Maimer', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 86, body = 86, legs = 68, feet = 85},
		description = {'Pit Maimer', 'Pit Maimer'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Reaver', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 86, body = 86, legs = 68, feet = 85, addons = 1},
		description = {'Pit Reaver', 'Pit Reaver'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Destroyer', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 86, body = 86, legs = 68, feet = 85, addons = 3},
		description = {'Pit Destroyer', 'Pit Destroyer'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Brawler', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 38, body = 0, legs = 38, feet = 118},
		description = {'Pit Brawler', 'Pit Brawler'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Scourge', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 38, body = 0, legs = 38, feet = 118, addons = 1},
		description = {'Pit Scourge', 'Pit Scourge'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Berserker', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 38, body = 0, legs = 38, feet = 118, addons = 3},
		description = {'Pit Berserker', 'Pit Berserker'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Fiend', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 41, body = 44, legs = 118, feet = 8},
		description = {'Pit Fiend', 'Pit Fiend'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Demon', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 41, body = 44, legs = 118, feet = 8, addons = 1},
		description = {'Pit Demon', 'Pit Demon'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Condemned', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 41, body = 44, legs = 118, feet = 8, addons = 3},
		description = {'Pit Condemned', 'Pit Condemned'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Battler', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 58, body = 40, legs = 59, feet = 79},
		description = {'Pit Battler', 'Pit Battler'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Blackling', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 58, body = 40, legs = 59, feet = 79, addons = 1},
		description = {'Pit Blackling', 'Pit Blackling'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Lord', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 58, body = 40, legs = 59, feet = 79, addons = 3},
		description = {'Pit Lord', 'Pit Lord'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Leader', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 39, body = 58, legs = 58, feet = 1},
		description = {'Pit Leader', 'Pit Leader'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Prince', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 39, body = 58, legs = 58, feet = 1, addons = 1},
		description = {'Pit Prince', 'Pit Prince'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Slave', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 39, body = 58, legs = 58, feet = 1, addons = 3},
		description = {'Pit Slave', 'Pit Slave'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Phantom', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 80, body = 81, legs = 81, feet = 80},
		description = {'Pit Phantom', 'Pit Phantom'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Brimstone', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 80, body = 81, legs = 81, feet = 80, addons = 1},
		description = {'Pit Brimstone', 'Pit Brimstone'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Wrath', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 80, body = 81, legs = 81, feet = 80, addons = 3},
		description = {'Pit Wrath', 'Pit Wrath'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Devil', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 75, body = 94, legs = 94, feet = 77},
		description = {'Pit Devil', 'Pit Devil'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Terror', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 75, body = 94, legs = 94, feet = 77, addons = 1},
		description = {'Pit Terror', 'Pit Terror'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Scorn', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 75, body = 94, legs = 94, feet = 77, addons = 3},
		description = {'Pit Scorn', 'Pit Scorn'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	{ name = 'Pit Hunter', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 75, body = 75, legs = 75, feet = 0},
		description = {'Pit Hunter', 'Pit Hunter'}, experience = 10000, health = 10000, healthMax = 10000,
	},
	{ name = 'Pit Savant', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 75, body = 75, legs = 75, feet = 0, addons = 1},
		description = {'Pit Savant', 'Pit Savant'}, experience = 25000, health = 25000, healthMax = 25000,
	},
	{ name = 'Pit Rage', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 669, head = 75, body = 75, legs = 75, feet = 0, addons = 3},
		description = {'Pit Rage', 'Pit Rage'}, experience = 50000, health = 50000, healthMax = 50000,
	},
	
	
	
	{ name = 'Elder Dragon of Storm', level = 46, file = 'Cirith/Bosses/Quest/Elder Dragon of Storm.xml', look = {type = 607}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Starszy Smok Burzy', 'Elder Dragon of Storm'}, experience = 180000, health = 300000, healthMax = 300000,
		elements = {
			{ type = 'physical', value = 5},
			{ type = 'holy', value = -5},
			{ type = 'death', value = -5},
			{ type = 'energy', value = 90},
			{ type = 'earth', value = 25},
			{ type = 'fire', value = 30},
			{ type = 'ice', value = -5},
		},
		loot = {
			{id = {2440, 13888, 2446}, chance = 8900}, -- lightning axe
			{id = 18338, chance = 9100}, -- red gem
			{id = {11680, 13938}, chance = 3000, uniqueDrop = 1}, -- electro bow/xbow
			{id = 8841, count = 4, chance = 25120}, -- lemon
			{id = 15745, chance = 1200, uniqueDrop = 1}, -- dragon legs
			{id = 7889, chance = 6590, uniqueDrop = 1}, -- beta shanir
			{id = 17941, count = 2, chance = 8090}, -- radiating powder
			{id = 2209, chance = 16570}, -- power ring
			{id = {2001, 16268}, chance = 3500, uniqueDrop = 1}, -- thunderstruck backpack
			{id = 10221, chance = 8700, subType = 10}, -- shockwave amulet
			{id = 2143, count = 3, chance = 280}, -- white pearl
			{id = 2195, chance = 2500}, -- boots of haste
			{id = 10581, count = 5, chance = 97910}, -- wyrm scale
			{id = 5809, count = 3, chance = 51060}, -- magical element
			{id = 2150, count = 7, chance = 96130}, -- small amethyst
			{id = {8920, 18073}, chance = 4190}, -- ethereal staff
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 16164, chance = 7300}, -- black steel shield
			{id = {7838, 11443, 2547}, count = {6, 14}, chance = 29800}, -- flash/power bolt/arrow
			{id = 2672, count = {2, 8}, chance = 64640}, -- dragon ham
			{id = 2152, count = {5, 15}, chance = 91690}, -- silver
			{id = 2148, count = {35, 100}, chance = 97020}, -- coin
		} 
	},
	{ name = 'Drillgrub', file = 'Cirith/Bosses/Quest/Drillgrub.xml', look = {type = 622},
		description = {'Czerwobal', 'Drillgrub'}, experience = 22000, health = 40000, healthMax = 40000,
		elements = {
			{ type = 'earth', value = 40 },
			{ type = 'energy', value = 10 },
			{ type = 'fire', value = -10 },
			{ type = 'physical', value = 5 },
			{ type = 'ice', value = -10 },
			{ type = 'holy', value = 5 },
		},
		loot = {
			{id = 17941, count = 3, chance = 8090}, -- radiating powder
			{id = 14175, count = 4, chance = 36420}, -- coal
			{id = 9967, chance = 18140}, -- Glob of acid
			{id = 2796, count = {5, 8}, chance = 92280}, -- green mushroom
			{id = {2545, 11437, 7850}, count = {8, 48}, chance = 41390}, -- poison arrow
			{id = 2159, count = 2, chance = 7980}, -- scarab coin
			{id = {17726, 11402, 14352, 14456}, chance = 8500}, -- massive wooden bow
			{id = {2172, 11804}, chance = 8500}, -- bronze amulet
			{id = 17686, chance = 25370}, -- arydian shield
			{id = 15758, chance = 25370}, -- snake boots
			{id = 14493, chance = 5450}, -- forbund
			{id = 2155, chance = 92140}, -- green gem
			{id = 2631, chance = 10280}, -- knight legs
			{id = {14010, 14488}, chance = 9990}, -- knight armor
			{id = {16167, 16166}, chance = 4150}, -- swamplair armor
			{id = 2152, count = 4, chance = 71500}, -- silver coin
			{id = 2149, count = 2, chance = 78600}, -- small emerald
			{id = 2148, count = {38, 90}, chance = 97690}, -- gold
		}
	},
	{ name = 'Ghazdris', file = 'Cirith/Bosses/Quest/Ghazdris.xml', look = {type = 84},
		description = {'Ghazdris', 'Ghazdris'}, experience = 8500, health = 15000, healthMax = 15000,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'death', value = 100 },
		},
		loot = {
			{id = 13592, chance = 15000}, -- cursed spear
			{id = 2134, chance = 9910}, -- silver brooch
			{id = 16933, count = {2, 6}, chance = 27650}, -- small mana potion
			{id = 7618, count = 4, chance = 18880}, -- light mana potion
			{id = 11227, count = 2, chance = 8530}, -- unholy bone
			{id = 5925, count = 4, chance = 9350}, -- hardened bone
			{id = 2145, count = 6, chance = 21830}, -- small diamond
			{id = 1999, chance = 30000}, -- yellow backpack
			{id = 2136, chance = 1500}, -- demonbone amulet
			{id = 13759, chance = 29990}, -- pelvis bone
			{id = {13797, 14325}, chance = 7990}, -- steel blade
			{id = 14455, chance = 6230}, -- the creeper of darkness
			{id = 14322, chance = 4150}, -- royal steel helmet
			{id = {14010, 16138}, chance = 1800}, -- golden armor
			{id = 10565, count = {3, 7}, chance = 89950}, -- gauze bandage
			{id = 2152, count = 2, chance = 65200}, -- silver coin
			{id = 2148, count = {25, 90}, chance = 99180}, -- gold
		}
	},
	{ name = 'Xattarak', file = 'Cirith/Bosses/Quest/Xattarak.xml', look = {type = 668},
		description = {'Xattarak', 'Xattarak'}, experience = 8000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'bleed', value = 15 },
			{ type = 'death', value = 35 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 2171, chance = 13110}, -- platinum amulet
			{id = 2169, chance = 25530}, -- time ring
			{id = 13597, chance = 1000}, -- ogre ring
			{id = 2177, chance = 12650}, -- life crystal
			{id = 2159, chance = 21500}, -- scarab coin
			{id = 2643, chance = 7500}, -- breastplate
			{id = 15730, chance = 14450}, -- war axe
			{id = 13796, chance = 3190}, -- silver shield
			{id = 5809, chance = 51060}, -- magical element
			{id = 14488, chance = 14150}, -- visor
			{id = 5887, chance = 10200}, -- piece of royal steel
			{id = 13888, chance = 50000}, -- lightning axe
			{id = 2319, chance = 2500}, -- strange symbol
			{id = 14455, chance = 5120}, --necrotic rod
			{id = 2682, count = 3, chance = 99990}, -- watermelon
			{id = 13748, chance = 22150}, -- noble turban
			{id = 7618, count = 9, chance = 94180}, -- health potion
			{id = 2152, count = {3, 15}, chance = 82100}, -- silver coin
			{id = 2148, count = {15, 45}, chance = 99180}, -- gold
		}
	},
	{ name = 'Contrabandist Bandit', file = 'Cirith/Bosses/Quest/Contrabandist Bandit.xml', look = {type = 397, addons = 3},
		description = {'Przemytnik', 'Smuggler'}, experience = 2000, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 2195, chance = 10}, -- boh
			{id = 5896, chance = 1190}, -- bear paw
			{id = 2130, chance = 1200, subType = 40}, -- golden amulet
			{id = 11435, count = {7, 15}, chance = 5490}, -- crystalline arrow
			{id = 15738, chance = 3290}, -- long bow
			{id = 14003, chance = 5390}, -- light boots
			{id = 13754, chance = 67170}, -- rope belt
			{id = 14326, chance = 550}, -- steel boots
			{id = 14171, chance = 4700}, -- berynit armor
			{id = 13942, chance = 2150}, -- repeating crossbow
			{id = 2661, chance = 61640}, -- scarf
			{id = 5892, chance = 32140}, -- steel
			{id = 15609, chance = 1400}, -- massive ornamented bow
			{id = 7588, chance = 43160}, -- health potion
			{id = 2690, count = 2, chance = 11360}, -- roll
			{id = 2456, chance = 94750}, -- bow
			{id = 7365, count = {5, 20}, chance = 88580}, -- arrow
			{id = 14779, chance = 76440}, -- blade
			{id = 14336, chance = 60920}, -- hood
			{id = 2152, count = 4, chance = 92420}, -- silver
			{id = 2148, count = 29, chance = 56290}, -- coin
		}
	},
	{ name = 'Contrabandist', file = 'Cirith/Bosses/Quest/Contrabandist.xml', look = {type = 631},
		description = {'Przemytnik', 'Smuggler'}, experience = 4000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 16157, chance = 805}, -- black steel armor
			{id = 16158, chance = 920}, -- black steel helmet
			{id = 5887, chance = 7400}, -- piece of royal steel
			{id = 14012, chance = 5320}, -- crimson sword
			{id = 2145, count = 2, chance = 21830}, -- small diamond
			{id = 14362, chance = 9390}, -- steel armor
			{id = 14327, chance = 7100}, -- steel legs
			{id = 5892, chance = 89140}, -- steel
			{id = 14351, chance = 7350}, -- iron boots
			{id = 13754, chance = 86290}, -- rope belt
			{id = 13985, chance = 5170}, -- armor of loyalists
			{id = 14324, chance = 9800}, -- steel shield
			{id = 14336, chance = 24430}, -- hood
			{id = 15776, chance = 39780}, -- short sword
			{id = 2050, chance = 50040}, -- torch
			{id = 2152, count = {3, 12}, chance = 92420}, -- silver
			{id = 2148, count = {36, 71}, chance = 93150}, -- coin
		}
	},
	{ name = 'Smuggler', file = 'Cirith/Bosses/Quest/Smuggler.xml', look = {type = 96},
		description = {'Przemytnik', 'Smuggler'}, experience = 1600, health = 800, healthMax = 800,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 35 },
		},
		loot = {
			{id = 17655, chance = 25890}, -- smugglers boots
			{id = 5885, chance = 6320}, -- warrior's sweat
			{id = 2661, chance = 85010}, -- scarf
			{id = 14007, chance = 7650}, -- guardian shield
			{id = 2114, chance = 5140}, -- piggy bank
			{id = 14349, chance = 14150}, -- iron helmet
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 2691, chance = 91990}, -- bread
			{id = 2152, count = {2, 8}, chance = 92420}, -- silver
			{id = 2148, count = 32, chance = 98000}, -- coin
		}
	},
	{ name = 'Unhurried Acarid', file = 'Cirith/Bosses/Quest/Unhurried Acarid.xml', look = {type = 494}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Unhurried Acarid', 'Unhurried Acarid'}, experience = 150000, health = 400000, healthMax = 400000,
		elements = {
			{ type = 'holy', value = 40 },
			{ type = 'death', value = 25 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = {16207, 13992}, chance = 2500, uniqueDrop = 1}, -- meteorite/bronze armor
			{id = {11802, 11798}, chance = 5000, uniqueDrop = 1}, -- backpack
			{id = 11387, chance = 5200, uniqueDrop = 1}, -- luck amulet
			{id = {13056, 13071, 13059, 13062}, chance = 50000}, -- silver crafting additives
			{id = {8850, 8904}, chance = 7900, uniqueDrop = 1}, -- heavy crossbow
			{id = {16210, 16209}, chance = 15000, uniqueDrop = 1}, -- dragon claw
			{id = 16166, chance = 16910, uniqueDrop = 1}, -- swamplair legs
			{id = 11397, chance = 5000, uniqueDrop = 1}, -- diamond armor
			{id = 16165, chance = 48530}, -- swamplair boots
			{id = 17726, chance = 500, uniqueDrop = 1}, -- plague spear
			{id = 13794, chance = 28700, uniqueDrop = 1}, -- titan helmet
			{id = 15761, chance = 98900}, -- snake blade
			{id = 15755, chance = 73190}, -- snake helmet
			{id = 13004, chance = 10200, uniqueDrop = 1}, -- leaf shield
			{id = 14339, chance = 83210}, -- iron shield
			{id = 15720, chance = 82210}, -- broadsword
			{id = 2155, chance = 92140}, -- green gem
			{id = 8901, chance = 48690}, -- SB of warding
			{id = 7590, count = 3, chance = 82580}, -- GMP
			{id = 5944, count = 4, chance = 69470}, -- soul orb
			{id = 2149, count = 11, chance = 78600}, -- small emerald
			{id = 2152, count = 2, chance = 89790}, -- silver coin
			{id = 2148, count = 21, chance = 95300}, -- copper coin
		}
	},
	{ name = 'Aranthus', file = 'Cirith/Bosses/Quest/Aranthus.xml', look = {type = 263}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Aranthus', 'Aranthus'}, experience = 39000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'death', value = 65 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 2171, chance = 3110}, -- platinum amulet
			{id = 2169, chance = 10530}, -- time ring
			{id = 14362, chance = 6980}, -- plate armor
			{id = 18033, chance = 2000}, -- teal dye
			{id = 14007, chance = 1990}, -- guardian shield
			{id = 11955, chance = 150}, -- jewelled backpack
			{id = 14005, chance = 1500}, -- wizard's robe
			{id = 14486, chance = 460}, -- glacier hood
			{id = 19464, chance = 800}, -- Frostbite amulet
			{id = 13299, chance = 9950}, -- glass spear
			{id = 13894, chance = 320}, -- frosty rapier
			{id = 14321, chance = 1780}, -- royal steel legs
			{id = 2124, chance = 3000}, -- crystal ring
			{id = 7290, chance = 1500}, -- shard
			{id = 7441, chance = 49030}, -- ice cube
			{id = 2152, count = 2, chance = 51890}, -- silver coin
			{id = 2148, count = 29, chance = 99190}, -- coin
		}
	},
	{ name = 'Xuj\'Hozy', file = 'Cirith/Bosses/Quest/Xuj\'Hozy.xml', look = {type = 640}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Xuj\'Hozy', 'Xuj\'Hozy'}, experience = 100000, health = 450000, healthMax = 450000,
		elements = {
			{ type = 'fire', value = 100 },
			{ type = 'energy', value = 70 },
			{ type = 'earth', value = 15 },
			{ type = 'physical', value = 40 },
			{ type = 'death', value = 30 },
			{ type = 'holy', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 14617, chance = 100000}, -- premium coin
			{id = 17077, chance = 10000}, -- card
			{id = 3954, chance = 1000, uniqueDrop = 1}, -- demon doll
			{id = 11810, chance = 8000}, -- fiery bow
			{id = 15740, chance = 4290}, -- skull bow
			{id = 11401, chance = 1000, uniqueDrop = 1}, -- berserker armor
			{id = 17221, chance = 1500, uniqueDrop = 1}, -- demonic shield
			{id = 16158, chance = 9500}, -- black steel helmet
			{id = 16208, chance = 3120}, -- meteorite helmet
			{id = 14478, chance = 25120}, -- berynit helmet
			{id = 7384, chance = 2560}, -- mystic blade
			{id = 8918, chance = 7340, uniqueDrop = 1}, -- spellbook of black magic
			{id = 7389, chance = 2560}, -- dragon slayer
			{id = 2260, chance = 10000}, -- rune
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 16137, chance = 4050}, -- golden legs
			{id = {2150, 2147, 9970, 2149}, count = 10-15, chance = 47970}, -- small amethyst
			{id = {2143, 2144}, count = 8-12, chance = 65390}, -- pearls
			{id = 6500, count = {5, 12}, chance = 100000}, -- demonic essence
			{id = 2151, count = {4, 10}, chance = 99470}, -- talon
			{id = 11679, chance = 6000}, -- hellfire bow
			{id = 2647, chance = 7500}, -- red robe
			{id = 13796, chance = 10500}, -- silver shield
			{id = 16157, chance = 7150}, -- black steel armor
			{id = 16205, chance = 2660}, -- meteorite boots
			{id = 18338, chance = 49100}, -- red gem
			{id = 13597, chance = 2500}, -- ogre ring
			{id = 13939, chance = 5450}, -- hellfire crossbow
			{id = 12957, chance = 2000, uniqueDrop = 1}, -- marksman crossbow
			{id = {13057, 13072, 13060, 13063}, chance = 2000}, -- golden crafting additives
			{id = 8904, chance = 1900}, -- spellscroll of prophecies
			{id = 13991, chance = 5000, uniqueDrop = 1}, -- mythril plate armor
			{id = 2134, chance = 9910}, -- silver brooch
			{id = 14483, chance = 1900, uniqueDrop = 1}, -- luck hat
			{id = 13001, chance = 4440}, -- void legs
			{id = 15742, chance = 5200}, -- sapphire ring
			{id = 13951, chance = 7490}, -- hellfire spear
			{id = 14157, chance = 2000}, -- demonic finger
			{id = 14166, chance = 1500}, -- sallet
			{id = 6529, count = {15, 75}, chance = 65280}, -- infernal bolt
			{id = 2214, chance = 91870}, -- ring of healing
			{id = 2164, chance = 99000, subType = 500}, -- might ring
			{id = {2472, 14765}, chance = 2500}, -- magic plate armor
			{id = 18054, chance = 100000, uniqueDrop = 1}, -- xuj'hozy fist
			{id = 18049, chance = 100000, uniqueDrop = 1}, -- xuj'hozy shell
			{id = 8472, count = 13, chance = 98940}, -- UMP
			{id = 8473, count = 13, chance = 97940}, -- UHP
			{id = 2152, count = {23, 90}, chance = 99610}, -- silver
			{id = 2148, count = {19, 90}, chance = 97170}, -- gold
			
		}
	},
	{ name = 'Khadidas', file = 'Cirith/Bosses/Khadidas.xml', look = {type = 374}, classId = 11, killAmount = 15, charmPoints = 50,
		description = {'Khadidas', 'Khadidas'}, experience = 200000, health = 500000, healthMax = 500000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 2152, count = 2, chance = 99610}, -- silver
			{id = 2148, count = 16, chance = 97170}, -- gold
		}
	},
	
	{ name = 'Velokass', file = 'Cirith/Bosses/Velokass.xml', look = {type = 630, head = 98, body = 118, legs = 99, feet = 78}, classId = 11, killAmount = 15, charmPoints = 50,
		description = {'Velokass', 'Velokass'}, experience = 50000, health = 45000, healthMax = 45000,
		elements = {
			{ type = 'physical', value = 99 },
			{ type = 'drown', value = 99 },
			{ type = 'bleed', value = 99 },
			{ type = 'holy', value = 99 },
			{ type = 'death', value = 99 },
			{ type = 'toxic', value = 99 },
			{ type = 'energy', value = 99 },
			{ type = 'earth', value = 99 },
			{ type = 'fire', value = 99 },
			{ type = 'ice', value = 99 },
			{ type = 'wind', value = 99 },
		},
		loot = {
			{id = 8857, chance = 1000}, -- silkweaver bow
			{id = 8904, chance = 900}, -- spellscroll of prophecies
			{id = 13990, chance = 8790}, -- emerald armor
			{id = 13991, chance = 4100}, -- mythril plate armor
			{id = 16166, chance = 31000}, -- swamplair legs
			{id = 15755, chance = 96800}, -- snake helmet
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 15761, chance = 83460}, -- snake blade
			{id = 16208, chance = 4800}, -- meteorite helmet
			{id = 13004, chance = 12000}, -- leaf shield
			{id = 14007, chance = 75300}, -- guardian shield
			{id = 11387, chance = 5000}, -- luck amulet
			{id = 2260, chance = 15000}, -- rune
			{id = 17063, chance = 50000}, -- card
			{id = 2168, chance = 85000}, -- life ring
			{id = 2168, chance = 65000}, -- life ring
			{id = 2168, chance = 45000}, -- life ring
			{id = 15760, chance = 88890}, -- snake bow
			{id = 14771, chance = 41980}, -- cure potion
			{id = 7591, count = 9, chance = 72240}, -- SHP
			{id = 7590, count = 9, chance = 72240}, -- SMP
			{id = 3976, count = 29, chance = 92090}, -- worm
			{id = 2149, count = 11, chance = 89000}, -- small emerald
			{id = 2152, count = 2, chance = 74980}, -- silver coin
			{id = 2148, count = 28, chance = 94980}, -- copper coin
		}
	},
	{ name = 'Velokass Minion', file = 'Cirith/Bosses/Velokass Minion.xml', look = {type = 406},
		description = {'Velokass Minion', 'Velokass Minion'}, experience = 0, health = 800, healthMax = 800 },
	{ name = 'Frozen Nightmare', file = 'Cirith/Bosses/Quest/Frozen Nightmare.xml', look = {type = 639},
		description = {'Lodowa Mara', 'Frozen Nightmare'}, experience = 20000, health = 4000, healthMax = 4000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = -5 },
			{ type = 'fire', value = 90 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = {7589, 7590}, count = 5, chance = 86500}, -- SMP
			{id = 14486, chance = 5000}, -- glacier hood
			{id = 14494, chance = 10}, -- amber spider
			{id = 13937, chance = 4650}, -- icey crossbow
			{id = 14171, chance = 14350}, -- berynit armor
			{id = {13055, 13070, 13058, 13061}, chance = 25000}, -- bronze crafting additives
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 18033, chance = 1000}, -- teal dye
			{id = 11955, chance = 1000}, -- jewelled backpack
			{id = 13894, chance = 8750}, -- frosty rapier
			{id = 2123, chance = 20000}, -- diamond ring
			{id = 2125, chance = 69800, subType = 400}, -- crystal necklace
			{id = 7441, chance = 49300}, -- ice cube
			{id = 7290, chance = 75000}, -- shard
			{id = 2152, chance = 54900}, -- silver coin
			{id = 2148, count = 21, chance = 75900}, -- coin
		}
	},
	{ name = 'Primeval Quest', file = 'Cirith/Bosses/Quest/primeval.xml', look = {type = 345},
		description = {'Przedwieczny', 'Primeval'}, experience = 2600, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 85 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 15747, chance = 50}, -- dragon axe
			{id = 14175, chance = 6420}, -- coal
			{id = 15721, chance = 150}, -- ragged blade
			{id = 11333, chance = 700}, --clay lump
			{id = 11221, chance = 790}, -- shiny stone
			{id = 14319, chance = 140}, -- royal steel shield
			{id = 5880, chance = 1650}, -- iron ore
			{id = 15734, chance = 10}, -- vile axe
			{id = 11319, chance = 10050}, -- spiked iron ball
			{id = 10548, chance = 20210}, -- ancient stone
			{id = 1294, count = 10, chance = 30220}, -- small stone
			{id = 2152, chance = 99610}, -- silver
			{id = 2148, count = 22, chance = 99940}, -- gold
		}
	},
	{ name = 'Primeval Behemoth', file = 'Cirith/Bosses/Quest/primeval behemoth.xml', look = {type = 345},
		description = {'Przedwieczny', 'Primeval'}, experience = 2600, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 85 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 15747, chance = 50}, -- dragon axe
			{id = 14175, chance = 7420}, -- coal
			{id = 15721, chance = 150}, -- ragged blade
			{id = 11333, chance = 700}, --clay lump
			{id = 11221, chance = 790}, -- shiny stone
			{id = 14319, chance = 140}, -- royal steel shield
			{id = 5880, chance = 1650}, -- iron ore
			{id = 15734, chance = 10}, -- vile axe
			{id = 11319, chance = 10050}, -- spiked iron ball
			{id = 10548, chance = 20210}, -- ancient stone
			{id = 1294, count = 10, chance = 30220}, -- small stone
			{id = 2152, chance = 99610}, -- silver
			{id = 2148, count = 22, chance = 99940}, -- gold
		}
	},
	{ name = 'Onis', file = 'Cirith/Bosses/Quest/Onis.xml', look = {type = 660, addons = 3}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Onis', 'Onis'}, experience = 180000, health = 250000, healthMax = 250000,
		elements = {
			{ type = 'death', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 75 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 14346, chance = 700, uniqueDrop = 1}, -- djinn amulet
			{id = 14018, chance = 820, uniqueDrop = 1}, -- royal blade
			{id = 15728, chance = 1050, uniqueDrop = 1}, -- skeleton striking face
			{id = 14008, chance = 4000, uniqueDrop = 1}, -- ornamented shield
			{id = 17876, chance = 1790}, -- mutilating blade
			{id = 11679, chance = 2500}, -- hellfire bow
			{id = 16211, chance = 7000, uniqueDrop = 1}, -- meteorite shield
			{id = 11436, count = {5, 45}, chance = 90000}, -- crystalline bolt
			{id = 13924, chance = 2510}, -- light crossbow
			{id = 13952, chance = 9000}, -- crystal spear
			{id = 14432, chance = 3630}, -- cobalt helmet
			{id = 2165, chance = 76010}, -- stealth ring
			{id = 2138, chance = 37700, subType = 300}, -- sapphire amulet
			{id = 7840, count = {24, 65}, chance = 91420}, -- flaming arrow
			{id = 17705, chance = 1520, uniqueDrop = 1}, -- bone of a fire staff
			{id = 2156, count = 2, chance = 72100}, -- red gem
			{id = 5911, count = 3, chance = 72730}, -- red piece of cloth
			{id = 1869, chance = 73030}, -- red tapestry
			{id = 2147, count = 6, chance = 79840}, -- small ruby
			{id = 2150, count = 5, chance = 96130}, -- small amethyst
			{id = 2146, count = 7, chance = 86130}, -- small sapphire
			{id = 16933, count = {5, 21}, chance = 87650}, -- small mana potion
			{id = 7620, count = {4, 15}, chance = 88880}, -- light mana potion
			{id = 2152, count = {5, 35}, chance = 89950}, -- silver
			{id = 2148, count = {30, 90}, chance = 99940}, -- gold
		}
	},
	{ name = 'Kartaxella', file = 'Cirith/Bosses/Quest/Kartaxella.xml', look = {type = 537},
		description = {'Kartaxella', 'Kartaxella'}, experience = 35000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 35 },
			{ type = 'ice', value = 40 },
		},
		loot = {
			{id = {13055, 13070, 13058, 13061}, chance = 30000}, -- bronze crafting additives
			{id = 13592, chance = 95000}, -- cursed spear
			{id = 17161, chance = 35000}, -- death enchanted in the staff
			{id = 982, count = 3, chance = 50000}, -- dark talon
		}
	},
	{ name = 'Alcott \'Corsair\' Victor', file = 'Cirith/Bosses/Quest/Alcott Corsair Victor.xml', look = {type = 98},
		description = {'Alcott \'Corsair\' Victor', 'Alcott \'Corsair\' Victor'}, experience = 15000, health = 7500, healthMax = 7500,
		elements = {
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 35 },
			{ type = 'ice', value = 40 },
			{ type = 'bleed', value = 100 },
		},
		loot = {
			{id = {13055, 13070, 13058, 13061}, chance = 30000}, -- bronze crafting additives
			{id = {6095, 6096, 5918, 5810}, chance = 95000}, -- pirate items
			{id = 14479, chance = 35000}, -- strange helmet
			{id = 5462, chance = 50000}, -- pirate boots
		}
	},
	{ name = 'Zatryx', file = 'Cirith/Bosses/Quest/Zatryx.xml', look = {type = 569, head = 13, body = 116, legs = 60, feet = 39, addons = 2},
		description = {'Zatryx', 'Zatryx'}, experience = 21500, health = 10000, healthMax = 10000,
		loot = {
			{id = {13055, 13070, 13058, 13061}, chance = 25000}, -- bronze crafting additives
			{id = 13991, chance = 890}, -- mythril armor
			{id = 14321, chance = 25150}, -- royal steel legs
			{id = 14170, chance = 70400}, -- armor
			{id = 14350, chance = 82590}, -- iron legs
			{id = 14349, chance = 84150}, -- iron helmet
			{id = 15713, chance = 84200}, -- longsword
			{id = 2691, chance = 91990}, -- bread
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 2148, count = 21, chance = 98000}, -- coin
		}
	},
	{ name = 'Satreon', file = 'Cirith/Bosses/Quest/Satreon.xml', look = {type = 555, head = 60, body = 21, legs = 20, feet = 49},
		description = {'Satreon', 'Satreon'}, experience = 20000, health = 8500, healthMax = 8500,
		loot = {
			{id = {13055, 13070, 13058, 13061}, chance = 25000}, -- bronze crafting additives
			{id = 7438, chance = 9500}, -- elvish bow
			{id = 2195, chance = 10150}, -- boh
			{id = 14010, chance = 41390}, -- knight armor
			{id = 11435, count = 41, chance = 75000}, -- crystalline arrow
			{id = 14461, chance = 84150}, -- soldier helm
			{id = 2691, chance = 91990}, -- bread
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 15741, chance = 94200}, -- bow
			{id = 2148, count = 17, chance = 98000}, -- coin
		}
	},
	{ name = 'Yakina', file = 'Cirith/Bosses/Quest/Yakina.xml', look = {type = 408, head = 117, body = 22, legs = 49, feet = 114, addons = 1},
		description = {'Yakina', 'Yakina'}, experience = 20000, health = 9500, healthMax = 9500,
		loot = {
			{id = {13055, 13070, 13058, 13061}, chance = 25000}, -- bronze crafting additives
			{id = 14320, chance = 11150}, -- steel boots
			{id = 14010, chance = 20390}, -- knight armor
			{id = 2171, chance = 31400}, -- platinum amulet
			{id = 15752, chance = 42590}, -- brass legs
			{id = 14488, chance = 54150}, -- visor
			{id = 15716, chance = 84200}, -- knight axe
			{id = 2691, chance = 81990}, -- bread
			{id = 2152, count = 2, chance = 92420}, -- silver
			{id = 2148, count = 23, chance = 98000}, -- coin
		}
	},
	{ name = 'Kuzlac', file = 'Cirith/Bosses/Quest/Kuzlac.xml', look = {type = 513, head = 0, body = 129, legs = 0, feet = 0},
		description = {'Kuzlac', 'Kuzlac'}, experience = 15000, health = 6000, healthMax = 6000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 9019, chance = 1000}, -- vampire doll
			{id = 13978, chance = 2550}, -- bloody crusher
			{id = 12956, chance = 4460}, -- marksman helmet
			{id = 2534, chance = 10330}, -- vampire shield
			{id = 16161, chance = 25010}, -- black steel blade
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 5911, chance = 35050}, -- red poc
			{id = 6300, chance = 50500}, -- death ring
			{id = 2145, count = 10, chance = 81340}, -- small diamond
			{id = 7591, count = 2, chance = 84820}, -- GHP
			{id = 7590, count = 2, chance = 86010}, -- GMP
			{id = 2144, count = 2, chance = 86770}, -- black pearl
			{id = 7364, count = 2, chance = 90870}, -- sniper arrow
			{id = 13736, chance = 93140}, -- blood
			{id = 13619, chance = 96770}, -- vampire's cape chain
			{id = 10601, chance = 97560}, -- vampire teeth
			{id = 5944, chance = 99010}, -- soul orb
			{id = 6500, chance = 99720}, -- de
			{id = 2152, count = 3, chance = 99960}, -- silver
			{id = 2148, count = 4, chance = 93010}, -- gold
		}
	},
	{ name = 'Barrier', file = 'Cirith/Bosses/Quest/Barrier.xml', look = {type = 0, typeEx = 1499, auxType = 2130},
		description = {'Barrier', 'Barrier'}, experience = 0, health = 4000, healthMax = 4000,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'fire', value = -50 },
		} },
	{ name = 'J\'Avi', file = 'Cirith/Bosses/Quest/J\'Avi.xml', look = {type = 506}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'J\'Avi', 'J\'Avi'}, experience = 150000, health = 600000, healthMax = 600000,
		elements = {
			{ type = 'earth', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'death', value = 50 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 2361, chance = 500}, -- frozen starlight
			{id = 14494, chance = 1000}, -- dark mithril
			{id = 16435, chance = 2500, uniqueDrop = 1}, -- ball gown
			{id = 11800, chance = 5000, uniqueDrop = 1}, -- frozen heart backpack
			{id = 15748, chance = 8000}, -- deepling crossbow
			{id = 16193, chance = 9000}, -- frozen shield
			{id = 16191, chance = 20000}, -- frozen armor
			{id = 16285, chance = 52500}, -- blood herb
			{id = 16192, chance = 19500}, -- frozen helmet
			{id = {13056, 13071, 13059, 13062}, chance = 10000}, -- silver crafting additives
			{id = 16398, count = {5, 10}, chance = 50000}, -- crystal
			{id = 14486, chance = 95000}, -- glacier hood
			{id = 13937, chance = 61730, uniqueDrop = 1}, -- icey crossbow
			{id = 2240, chance = 71890}, -- trash
			{id = 2226, chance = 72190}, -- trash
			{id = 13894, chance = 98600}, -- frosty rapier
			{id = 2168, chance = 92480}, -- life ring
			{id = 2146, count = {9, 20}, chance = 86130}, -- small sapphire
			{id = 7839, count = {43, 99}, chance = 92400}, -- shiver arrow
			{id = 2152, count = {3, 10}, chance = 95100}, -- silver
			{id = 2148, count = {24, 90}, chance = 95100}, -- gold
			{id = 5895, chance = 99510}, -- fish fin
			{id = 13072, chance = 1000, uniqueDrop = 1}, -- golden smith's hammer of urgency
		}
	},
	{ name = 'Eldur', file = 'Cirith/Bosses/Quest/Eldur.xml', look = {type = 242}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Eldur', 'Eldur'}, experience = 10000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'physical', value = 99 },
			{ type = 'drown', value = 99 },
			{ type = 'bleed', value = 99 },
			{ type = 'holy', value = 99 },
			{ type = 'death', value = 99 },
			{ type = 'toxic', value = 99 },
			{ type = 'energy', value = 99 },
			{ type = 'earth', value = 99 },
			{ type = 'fire', value = 99 },
			{ type = 'ice', value = -15 },
			{ type = 'wind', value = 99 },
		},
		loot = {
			{id = 13893, chance = 50}, -- fire axe
			{id = 16203, chance = 320}, -- hellfire helmet
			{id = 8902, chance = 550}, -- SB of mind control
			{id = 2392, chance = 1170}, -- fire sword
			{id = 9810, chance = 2130}, -- rusty armor
			{id = 2147, count = 2, chance = 6130}, -- small ruby
			{id = 16493, chance = 100000}, -- fire heart
			{id = 7840, count = 12, chance = 31420}, -- flaming arrow
			{id = 2148, count = 5, chance = 85100}, -- gold
		}
	},
	{ name = 'Erde', file = 'Cirith/Bosses/Quest/Erde.xml', look = {type = 285}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Erde', 'Erde'}, experience = 12000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'physical', value = 99 },
			{ type = 'drown', value = 99 },
			{ type = 'bleed', value = 99 },
			{ type = 'holy', value = 99 },
			{ type = 'death', value = 99 },
			{ type = 'toxic', value = 99 },
			{ type = 'energy', value = 99 },
			{ type = 'earth', value = 99 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 99 },
			{ type = 'wind', value = 99 },
		},
		loot = {
			{id = 7435, chance = 50}, -- impaler
			{id = 16167, chance = 320}, -- swamplair armor
			{id = 8902, chance = 550}, -- SB of mind control
			{id = 14352, chance = 1160}, -- iron blade
			{id = 9810, chance = 2130}, -- rusty armor
			{id = 9970, count = 2, chance = 6130}, -- small topaz
			{id = 16495, chance = 100000}, -- earth heart
			{id = 1294, count = 12, chance = 25190}, -- small stones
			{id = 11216, chance = 40020}, -- lump of earth
			{id = 2148, count = 5, chance = 85100}, -- gold
		}
	},
	{ name = 'Elektro', file = 'Cirith/Bosses/Quest/Elektro.xml', look = {type = 290}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Elektro', 'Elektro'}, experience = 14000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'physical', value = 99 },
			{ type = 'drown', value = 99 },
			{ type = 'bleed', value = 99 },
			{ type = 'holy', value = 99 },
			{ type = 'death', value = 99 },
			{ type = 'toxic', value = 99 },
			{ type = 'energy', value = 99 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 99 },
			{ type = 'ice', value = 99 },
			{ type = 'wind', value = 99 },
		},
		loot = {
			{id = 13794, chance = 50}, -- titan helmet
			{id = 2542, chance = 330}, -- lightning shield
			{id = 8901, chance = 960}, -- SB of warding
			{id = 18075, chance = 1000}, -- purple dye
			{id = 8920, chance = 1150}, -- wand of starstorm
			{id = 9810, chance = 2130}, -- rusty armor
			{id = 2150, count = 2, chance = 6130}, -- small amethyst
			{id = 16505, chance = 100000}, -- energy heart
			{id = 7838, count = 13, chance = 22400}, -- flash arrow
			{id = 2148, count = 5, chance = 85100}, -- gold
		}
	},
	{ name = 'Vesi', file = 'Cirith/Bosses/Quest/Vesi.xml', look = {type = 11}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Vesi', 'Vesi'}, experience = 16000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'physical', value = 99 },
			{ type = 'drown', value = 99 },
			{ type = 'bleed', value = 99 },
			{ type = 'holy', value = 99 },
			{ type = 'death', value = 99 },
			{ type = 'toxic', value = 99 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = 99 },
			{ type = 'fire', value = 99 },
			{ type = 'ice', value = 99 },
			{ type = 'wind', value = 99 },
		},
		loot = {
			{id = 16191, chance = 50}, -- frosty armor
			{id = 13937, chance = 350}, -- icey crossbow
			{id = 13894, chance = 910}, -- frosty rapier
			{id = 14486, chance = 1250}, -- glacier hood
			{id = 13299, chance = 3150}, -- glass spear
			{id = 18033, chance = 800}, -- teal dye
			{id = 9810, chance = 2130}, -- rusty armor
			{id = 2146, count = 2, chance = 6130}, -- small sapphire
			{id = 16494, chance = 100000}, -- icey heart
			{id = 7839, count = 13, chance = 22400}, -- shiver arrow
			{id = 2148, count = 5, chance = 85100}, -- gold
		}
	},
	{ name = 'Treebeard Quest', file = 'Cirith/Bosses/Quest/Treebeard Quest.xml', look = {type = 480},
		description = {'Praojciec', 'Forefather'}, experience = 25000, health = 9000, healthMax = 9000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'toxic', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'wind', value = -10 },
		},
		loot = {
			{id = 16171, chance = 10}, -- ruby legs
			{id = 14356, chance = 20}, -- bronze boots
			{id = 11387, chance = 50}, -- luck amulet
			{id = 2123, chance = 1000}, -- rots
			{id = 16208, chance = 1080}, -- meteorite helmet
			{id = 13848, chance = 2100}, -- leaf armor
			{id = 13991, chance = 2220}, -- mythril armor
			{id = 13940, chance = 3330}, -- crossbow of wrath nature
			{id = 16167, chance = 4600}, -- swamplair armor
			{id = 15734, chance = 1700}, -- vile axe
			{id = 13849, chance = 5720}, -- leaf helmet
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 2168, chance = 9960}, -- life ring
			{id = 12953, chance = 9980}, -- marksman boots
			{id = 12975, chance = 9990}, -- Earthy lunar crystal, puncture
			{id = 10219, chance = 9960, subType = 10}, -- sacred tree amulet
			{id = 15756, chance = 11090}, -- snake armor
			{id = 13999, chance = 13150}, -- noble armor
			{id = 13585, chance = 10120}, -- a foul rod
			{id = 13990, chance = 15530}, -- emerald armor
			{id = 15758, chance = 25370}, -- snake boots
			{id = 12964, chance = 34000}, -- clear lunar crystal
			{id = 15730, chance = 34450}, -- war axe
			{id = 2155, chance = 45500}, -- green gem
			{id = 2150, count = 4, chance = 46590}, -- small amethyst
			{id = 2144, count = 3, chance = 56780}, -- black pearl
			{id = 15755, chance = 58510}, -- snake helmet
			{id = 2214, chance = 59000}, -- roh
			{id = 2149, count = 6, chance = 59220}, -- small emerald
			{id = 8472, count = 2, chance = 60900}, -- UMP
			{id = 8473, count = 2, chance = 61000}, -- UHP
			{id = 2170, chance = 95290, subType = 200}, -- silver amulet
			{id = 11216, chance = 90980}, -- lump of earth
			{id = 1294, count = 31, chance = 92180}, -- small stone
			{id = 2152, count = 2, chance = 89600}, -- silver
			{id = 2148, count = 9, chance = 94190}, -- coin
		}
	},
	{ name = 'Basilisk', file = 'Cirith/Bosses/Quest/Basilisk.xml', look = {type = 528},
		description = {'Bazyliszek', 'Basilisk'}, experience = 100000, health = 14000, healthMax = 14000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = 80 },
			{ type = 'death', value = 35 },
			{ type = 'earth', value = 95 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		} },
	{ name = 'Motrehenesth', file = 'Cirith/Bosses/Quest/Motrehenesth.xml', look = {type = 85}, classId = 11, killAmount = 15, charmPoints = 100,
		description = {'Motrehenesth', 'Motrehenesth'}, experience = 250000, health = 300000, healthMax = 300000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 35 },
			{ type = 'fire', value = 50 },
			{ type = 'ice', value = 25 },
		},
		loot = {
			{id = 13895, chance = 290}, -- cursed blade
			{id = 13979, chance = 3160}, -- crusher
			{id = 13899, chance = 590}, -- bastard sword
			{id = 15729, chance = 13300}, -- war axe
			{id = 13307, chance = 8990}, -- magma spear
			{id = 17869, chance = 2790}, -- bloody blade
			{id = 13890, chance = 500}, -- bow of the miser
			{id = 13725, chance = 10}, -- ballista
			{id = 13951, chance = 5490}, -- hellfire spear
			{id = 6529, count = {5, 75}, chance = 35280}, -- infernal bolt
			{id = 7840, count = {10, 55}, chance = 97220}, -- flaming arrow
			{id = 17161, chance = 4080}, -- death enchanted in the staff
			{id = 16139, chance = 200}, -- golden helmet
			{id = 13994, chance = 7410, uniqueDrop = 1}, -- magma armor
			{id = 14001, chance = 55480}, -- dark armor
			{id = 14321, chance = 800}, -- royal steel legs
			{id = 2195, chance = 5100}, -- boots of haste
			{id = 16204, chance = 1780, uniqueDrop = 1}, -- hellfire shield
			{id = 2521, chance = 22250}, -- dark shield
			{id = 10518, chance = 2520}, -- demon backpack
			{id = 1986, chance = 41270}, -- red tome
			{id = 2177, count = 3, chance = 28650}, -- life crystal
			{id = 2033, chance = 93110}, -- golden mug
			{id = 2153, count = 3, chance = 59160}, -- violet gem
			{id = 2114, chance = 89000}, -- piggy bank
			{id = {13056, 13071, 13059, 13062}, count = 2, chance = 4000}, -- silver crafting additives
			{id = 9971, count = {2, 4}, chance = 11790}, -- gold ignot
			{id = 2130, chance = 2160, subType = 500}, -- golden amulet
			{id = 6300, chance = 2200}, -- death ring
			{id = 7708, chance = 9000}, -- signet
			{id = 2147, count = 7, chance = 52080}, -- small ruby
			{id = 2150, count = 4, chance = 22260}, -- small amethyst
			{id = 2145, count = 5, chance = 25330}, -- small diamond
			{id = 2146, count = {3, 4}, chance = 71500}, -- small sapphire
			{id = 2149, chance = 55600}, -- small emerald
			{id = 9970, count = {2, 3}, chance = 47950}, -- small topaz
			{id = 5954, count = 2, chance = 94340}, -- demon horn
			{id = 6500, count = {2, 5}, chance = 69090}, -- demonic rssence
			{id = 2152, count = {25, 65}, chance = 89600}, -- silver
			{id = 2148, count = {35, 95}, chance = 94190}, -- coin
		}
	},
	{ name = 'Last Druid Boss', file = 'Cirith/Bosses/Quest/Last Druid Boss.xml', look = {type = 489, head = 0, body = 94, legs = 94, feet = 94, addons = 2},
		description = {'Ostatni Druid', 'Last Druid'}, experience = 10000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 13945, chance = 1000}, -- black moon hood
			{id = 2123, chance = 4000}, -- rots
			{id = {2656, 3982}, chance = 7000}, -- blue/green robe
			{id = 14009, chance = 8000}, -- lightining robe
			{id = 2124, chance = 16900}, -- crystal ring
			{id = 2197, chance = 22320, subType = 5}, -- ssa
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 2260, chance = 25000}, -- rune
			{id = 2260, chance = 25000}, -- rune
			{id = 2260, chance = 25000}, -- rune
			{id = 2108, chance = 40000}, -- doll
			{id = 2167, chance = 52120}, -- energy ring
			{id = 2792, chance = 53070}, -- dark mushroom
			{id = 2237, chance = 59040}, -- dirty cape
			{id = 2151, chance = 61100}, -- talon
			{id = 7590, count = 7, chance = 64770}, -- GMP
			{id = 8843, count = 9, chance = 65000}, -- onion	
			{id = 7591, count = 2, chance = 65100}, -- GHP
			{id = 2678, count = 4, chance = 69680}, -- cocount
			{id = 2600, chance = 69940}, -- inkwell
			{id = 14005, chance = 71450}, -- wizard's robe
			{id = {15761, 16143}, chance = 77760}, -- snake blade
			{id = 2146, count = 15, chance = 91180}, -- small sapphire
			{id = 2149, count = 15, chance = 91180}, -- small emerald
			{id = 2152, count = 2, chance = 41690}, -- silver
			{id = 2148, count = 16, chance = 99380}, -- gold
			{id = {1686, 1687, 1688, 1689}, chance = 100000}, -- pillows
		}
	},
	{ name = 'Last Druid Immortal', file = 'Cirith/Bosses/Quest/Last Druid Immortal.xml', look = {type = 489, head = 0, body = 94, legs = 94, feet = 94, addons = 2}},
	{ name = 'Last Druid Clone', file = 'Cirith/Bosses/Quest/Last Druid Clone.xml', look = {type = 489, head = 0, body = 94, legs = 94, feet = 94, addons = 2},
		description = {'Ostatni Druid', 'Last Druid'}, experience = 0, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'ice', value = 5 },
		} },
	{ name = 'Last Druid', file = 'Cirith/Bosses/Quest/Last Druid.xml', look = {type = 489, head = 0, body = 94, legs = 94, feet = 94, addons = 2},
		description = {'Ostatni Druid', 'Last Druid'}, experience = 0, health = 1, healthMax = 100,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Last Druid Bonebeast', file = 'Cirith/Bosses/Quest/Last Druid Bonebeast.xml', look = {type = 101},
		description = {'Ko�ciana Bestia', 'Bonebeast'}, experience = 0, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'holy', value = -20 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
		} },
	{ name = 'Boar Quest', file = 'Cirith/Bosses/Quest/Boar Quest.xml', look = {type = 384},
		description = {'Dzik', 'Boar'}, experience = 10000, health = 1200, healthMax = 1200,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13914, chance = 6900}, -- boar loincloth
			{id = 13917, chance = 41500}, -- boar hooves
			{id = 13915, chance = 51000}, -- boar tusks
			{id = 14338, count = 2, chance = 99790}, -- Haunch of Boar
		}
	},
	{ name = 'Drekavac Imp', file = 'Cirith/Bosses/Quest/Drekavac Imp.xml', look = {type = 237}, classId = 11, killAmount = 15, charmPoints = 50,
		description = {'Drekavac', 'Drekavac'}, experience = 150000, health = 100000, healthMax = 100000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 80 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 11411, chance = 10, uniqueDrop = 1}, -- doll
			{id = 13925, chance = 3200, uniqueDrop = 1}, -- cursed shield
			{id = 14356, chance = 4500}, -- bronze boots
			{id = 13725, chance = 3000}, -- ballista
			{id = 8903, chance = 14170}, -- spellbook of lost souls
			{id = 12953, chance = 15860}, -- marksman boots
			{id = 2625, chance = 500, uniqueDrop = 1}, -- imp armor
			{id = 12989, chance = 27560}, -- dark lunar crystal
			{id = 15733, chance = 29200}, -- devastator
			{id = 16137, chance = 39880, uniqueDrop = 1}, -- golden legs
			{id = 2260, chance = 50000}, -- rune
			{id = {13055, 13070, 13058, 13061, 13056, 13071, 13059, 13062}, chance = 5000}, -- bronze/silver crafting additives
			{id = {13057, 13072, 13060, 13063}, chance = 1000, uniqueDrop = 1}, -- golden crafting additives
			{id = 2154, chance = 50410}, -- yellow gem
			{id = 13592, chance = 5170}, -- cursed spear
			{id = 13886, chance = 53490}, -- haunted blade
			{id = 13307, chance = 7500}, -- magma spear
			{id = 14001, chance = 55480}, -- dark armor
			{id = 6300, chance = 59060}, -- death ring
			{id = 13986, chance = 60670}, -- amber plate
			{id = 9971, chance = 61830}, -- gold ignot
			{id = 5944, count = {8, 11}, chance = 64200}, -- soul orb
			{id = 6500, count = {3, 5}, chance = 69090}, -- demonic rssence
			{id = 7591, count = {3, 4}, chance = 70200}, -- ghp
			{id = 7590, count = {3, 5}, chance = 75290}, -- gmp
			{id = 18076, chance = 1000, uniqueDrop = 1}, -- grey dye
			{id = 6558, chance = 5180}, -- concentrated demonic blood
			{id = 6558, chance = 3370}, -- concentrated demonic blood
			{id = 2144, count = {7, 12}, chance = 82650}, -- pearl
			{id = 2152, count = {3, 20}, chance = 90450}, -- silver
			{id = 2148, count = {9, 99}, chance = 92780}, -- gold
		}
	},
	{ name = 'Drekavac', file = 'Cirith/Bosses/Quest/Drekavac.xml', look = {type = 313},
		description = {'Drekavac', 'Drekavac'}, experience = 150000, health = 150000, healthMax = 150000,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
		} },
	{ name = 'Qanilon Minion', file = 'Cirith/Bosses/Quest/Qanilon Minion.xml', look = {type = 20},
		description = {'Qanilon Minion', 'Qanilon Minion'}, experience = 500, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 13794, chance = 50}, -- titan helmet
			{id = 16191, chance = 10}, -- frosty armor
			{id = 13894, chance = 670}, -- frosty rapier
			{id = 7591, chance = 1040}, -- shp
			{id = 5895, chance = 1950}, -- fish fin
			{id = 15729, chance = 3070}, -- double axe
			{id = 2670, count = 9, chance = 4980}, -- shrimp
			{id = 2145, count = 10, chance = 5330}, -- small diamond
			{id = 13950, chance = 400}, -- frosty spear
			{id = 13753, chance = 29000}, -- quara bone
			{id = 13749, chance = 34560}, -- quara tentacle
			{id = 2148, count = 3, chance = 98250}, -- coin
		}
	},
	{ name = 'Qanilon', file = 'Cirith/Bosses/Quest/Qanilon.xml', look = {type = 46},
		description = {'Qanilon', 'Qanilon'}, experience = 15000, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 13794, chance = 900}, -- titan helmet
			{id = 16191, chance = 1800}, -- frosty armor
			{id = 13894, chance = 6700}, -- frosty rapier
			{id = {13055, 13070, 13058, 13061}, chance = 50000}, -- bronze crafting additives
			{id = 7591, chance = 10400}, -- shp
			{id = 5895, chance = 19500}, -- fish fin
			{id = 15729, chance = 30700}, -- double axe
			{id = 2670, count = 9, chance = 49800}, -- shrimp
			{id = 2145, count = 10, chance = 53300}, -- small diamond
			{id = 13950, chance = 9070}, -- frosty spear
			{id = 13753, chance = 90000}, -- quara bone
			{id = 13749, chance = 94560}, -- quara tentacle
			{id = 2148, count = 6, chance = 98250}, -- coin
		}
	},
	{ name = 'Pawahtuun Rat', file = 'Cirith/Bosses/Quest/Pawahtuun Rat.xml', look = {type = 21},
		description = {'Szczur', 'Rat'}, experience = 0, health = 20, healthMax = 20,
		loot = {
			{id = {2235, 8368}, chance = 11560}, -- cheese
			{id = 3976, chance = 26780}, -- worm
			{id = 2148, count = 2, chance = 37690}, -- coin
		}
	},
	{ name = 'Pawahtuun', file = 'Cirith/Bosses/Quest/Pawahtuun.xml', look = {type = 193},
		description = {'Pawahtuun', 'Pawahtuun'}, experience = 500, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 3982, chance = 10}, -- green robe
			{id = {13848, 13847}, chance = 120}, -- leaf legs/armor
			{id = 16356, chance = 350}, -- bag
			{id = 2114, chance = 390}, -- piggy bank
			{id = 2656, chance = 490}, -- blue robe
			{id = 1998, chance = 500}, -- green backpack
			{id = 2171, chance = 810}, -- platinum amulet
			{id = 5669, chance = 1110}, -- voodoo skull
			{id = 11955, chance = 1140}, -- jewelled backpack
			{id = {15760, 16169}, chance = 1250}, -- snake/swamplair bow
			{id = 5810, chance = 2310}, -- pirate voodoo doll
			{id = 14351, chance = 3320}, -- iron boots
			{id = 13940, chance = 5390}, -- snake crossbow
			{id = 12952, chance = 15440}, -- bronze helmet
			{id = 2129, chance = 16490}, -- wolf tooth chain
			{id = 13980, chance = 16500, subType = 70}, -- earth amulet
			{id = 14323, chance = 16590}, -- royal steel armor
			{id = 2167, chance = 18600}, -- energy ring
			{id = 1976, chance = 2930}, -- book
			{id = {2149, 2145}, chance = 21000}, -- small emerald
			{id = 13948, chance = 2200}, -- hunting spear
			{id = 14000, chance = 23330}, -- light armor
			{id = 13581, chance = 24190}, -- creature products
			{id = 7588, chance = 27000}, -- shp
			{id = 7589, chance = 28600}, -- SMP
			{id = 13577, chance = 37590}, -- creature products
			{id = 10554, chance = 50350}, -- cultish mask
			{id = 2152, count = 2, chance = 73450}, -- silver
			{id = 2148, count = 13, chance = 99730}, -- gold
		}
	},
	{ name = 'The Fallen Ghost', file = 'Cirith/Bosses/Quest/the fallen ghost.xml', look = {type = 319},
		description = {'Upad�y', 'The Fallen'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Dezathres', file = 'Cirith/Bosses/Quest/dezathres.xml', look = {type = 391},
		description = {'Dezathres', 'Dezathres'}, experience = 7500, health = 1150, healthMax = 1150,
		loot = {
			{id = 15716, chance = 2000}, -- knight axe
			{id = 2145, chance = 2300}, -- small diamond
			{id = 14349, chance = 9000}, -- iron helm
			{id = 15786, chance = 4300}, -- miedziana zbroja
			{id = 13985, chance = 700}, -- armor of loyalists
			{id = 16356, chance = 5000}, -- container
			{id = 15792, chance = 9100}, -- gwiazda trojramienna
			{id = 15751, chance = 8300}, -- brass armor
			{id = 14334, chance = 15700}, -- ciezki topor
			{id = 15788, chance = 12300}, -- miedziane buty
			{id = 15753, chance = 10500}, -- brass boots
			{id = 16239, chance = 7400}, -- ticket
			{id = 14484, chance = 2000}, -- helmet
			{id = 14336, chance = 44300}, -- hood
			{id = 15772, chance = 81000}, -- skorzany helm
			{id = 15776, chance = 97800}, -- short sword
			{id = 2050, chance = 30040}, -- torch
			{id = 2148, count = 23, chance = 83150}, -- coin
		}
	},
	{ name = 'Slime Quest', file = 'Cirith/Bosses/Quest/slime quest.xml', look = {type = 238},
		description = {'Szlam', 'Slime'}, experience = 500, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 30 },
		} },
	{ name = 'Slime Minion', file = 'Cirith/Bosses/Quest/slime minion.xml', look = {type = 19},
		description = {'Szlam', 'Slime'}, experience = 240, health = 200, healthMax = 200,
		elements = {
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 35 },
		} },
	{ name = 'Outcast Weak', file = 'Cirith/Bosses/Quest/outcast weak.xml', look = {type = 391},
		description = {'Wygnaniec', 'Outcast'}, experience = 37, health = 50, healthMax = 50,
		loot = {
			{id = 15792, chance = 1120}, -- gwiazda trojramienna
			{id = 15751, chance = 910}, -- brass armor
			{id = 14334, chance = 1370}, -- ciezki topor
			{id = 15788, chance = 400}, -- miedziane buty
			{id = 16239, chance = 410}, -- ticket
			{id = 14336, chance = 6190}, -- hood
			{id = 15772, chance = 2500}, -- skorzany helm
			{id = 15776, chance = 780}, -- short sword
			{id = 2050, chance = 30040}, -- torch
			{id = 2148, count = 3, chance = 88150}, -- coin
		}
	},
	{ name = 'Outlaw Weak', file = 'Cirith/Bosses/Quest/outlaw weak.xml', look = {type = 346},
		description = {'Banita', 'Outlaw'}, experience = 31, health = 40, healthMax = 40,
		loot = {
			{id = 15786, chance = 570}, -- miedziany pancerz
			{id = 15784, chance = 250}, -- miedziana tarcza
			{id = 15752, chance = 410}, -- brass legs
			{id = 15774, chance = 2100}, -- skorzane spodnie
			{id = 14336, chance = 4180}, -- hood
			{id = 2695, count = 2, chance = 9820}, -- egg
			{id = 15783, chance = 1100}, -- rebacz
			{id = 12963, chance = 1750}, -- lampart shield
			{id = 2691, chance = 13190}, -- bread
			{id = 15779, chance = 9100}, -- skuller
			{id = 2148, count = 2, chance = 59230}, -- coin
		}
	},
	{ name = 'Slinger Weak', file = 'Cirith/Bosses/Quest/slinger weak.xml', look = {type = 397},
		description = {'Procarz', 'Slinger'}, experience = 26, health = 30, healthMax = 30,
		loot = {
			{id = 7397, chance = 10}, -- deer trophy
			{id = 14354, chance = 540}, -- bronze helmet
			{id = 15753, chance = 200}, -- brass boots
			{id = 8840, count = 5, chance = 4940}, -- raspberry
			{id = 15793, chance = 650}, -- sword
			{id = 2379, chance = 1090}, -- blade
			{id = 15775, chance = 2650}, -- skorzane buty
			{id = 15776, chance = 200}, -- short sword
			{id = 2691, chance = 3190}, -- bread
			{id = 1294, count = 2, chance = 1700}, -- small stone
			{id = 15774, chance = 3760}, -- skorzane nogawice
			{id = 2148, count = 3, chance = 49690}, -- coin
		}
	},
	{ name = 'Villain Weak', file = 'Cirith/Bosses/Quest/villain weak.xml', look = {type = 397, addons = 1},
		description = {'�otr', 'Villain'}, experience = 30, health = 35, healthMax = 35,
		loot = {
			{id = 5896, chance = 180}, -- bear paw
			{id = 16239, chance = 200}, -- ticket
			{id = 2456, chance = 1900}, -- bow
			{id = 2690, chance = 15200}, -- roll
			{id = 2379, chance = 9700}, -- blade
			{id = 14336, chance = 11000}, -- hood
			{id = 2544, count = 3, chance = 48580}, -- arrow
			{id = 2148, count = 2, chance = 66290}, -- coin
		}
	},
	{ name = 'Dragon Quest Lord', file = 'Cirith/Bosses/Quest/red dragon.xml', look = {type = 562},
		description = {'Czerwony Smok', 'Red Dragon'}, experience = 5000, health = 2100, healthMax = 2100,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16140, chance = 600}, -- dragon slayer
			{id = 7399, chance = 5000}, -- dragon lord trophy
			{id = 13987, chance = 500}, -- DSM
			{id = 13341, chance = 25000}, -- dragon lord claw
			{id = 14319, chance = 2250}, -- tower shield
			{id = 14480, chance = 2000}, -- spartan helmet
			{id = 16356, chance = 25000}, -- container
			{id = 2392, chance = 6280}, -- fire sword
			{id = 14479, chance = 8250}, -- strange helmet
			{id = 2177, chance = 28650}, -- life crystal
			{id = 7588, chance = 88970}, -- SHP
			{id = 5948, chance = 99020}, -- red dragon leather
			{id = 5882, chance = 99940}, -- red dragon scale
			{id = 2033, chance = 93110}, -- golden mug
			{id = 2146, chance = 90120}, -- small sapphire
			{id = 2547, count = 17, chance = 76620}, --power bolt
			{id = 13952, chance = 6500}, -- crystal spear
			{id = 1976, chance = 88980}, -- book
			{id = 2796, chance = 92280}, -- green mushroom
			{id = 2672, count = 5, chance = 80200}, -- dragon ham
			{id = 2152, chance = 91690}, -- silver
			{id = 2148, count = 52, chance = 95260}, -- coin
		}
	},
	{ name = 'Jacquard', file = 'Cirith/Bosses/Quest/jacquard.xml', look = {type = 454},
		description = {'Jacquard', 'Jacquard'}, experience = 0, health = 40, healthMax = 40 },
	{ name = 'Vaclav Statue', file = 'Cirith/Bosses/Quest/vaclav statue.xml', look = {type = 0, typeEx = 7825, auxType = 748},
		description = {'Vaclav', 'Vaclav'}, experience = 0, health = 100, healthMax = 100, immunity = true,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Vaclav', file = 'Cirith/Bosses/Quest/vaclav.xml', look = {type = 0, typeEx = 7825, auxType = 748},
		description = {'Vaclav', 'Vaclav'}, experience = 400000, health = 200000, healthMax = 200000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 35 },
			{ type = 'earth', value = 75 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = 15 },
		} },
	{ name = 'Vaclav Illusion', file = 'Cirith/Bosses/Quest/vaclav illusion.xml', look = {type = 0, typeEx = 7825, auxType = 748},
		description = {'Vaclav', 'Vaclav'}, experience = 0, health = 100, healthMax = 100 },
	{ name = 'Bloody Stone 1', file = 'Cirith/Bosses/Quest/bloody stone 1.xml', look = {type = 0, typeEx = 15160, auxType = 14238},
		description = {'Kamien Krwi', 'Bloody Stone'}, experience = 0, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
		} },
	{ name = 'Bloody Stone 2', file = 'Cirith/Bosses/Quest/bloody stone 2.xml', look = {type = 0, typeEx = 15164, auxType = 14242}},
	{ name = 'Beautiful Demon', file = 'Cirith/Bosses/Quest/beautiful demon.xml', look = {type = 150, head = 78, body = 114, legs = 91, feet = 91, addons = 3},
		description = {'Przepiekny Demon', 'Beautiful Demon'}, experience = 1080, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 70 },
		},
		loot = {
			{id = 14447, chance = 10}, --rod
			{id = 14484, chance = 310}, -- helmet
			{id = 14352, chance = 620}, -- iron sword
			{id = 14324, chance = 930}, -- plate shield
			{id = 7588, chance = 930}, -- shp
			{id = 14362, chance = 2170}, -- plate armor
			{id = 13981, chance = 2170, subType = 200}, -- amulet
			{id = 2146, count = 4, chance = 5900}, -- sapphire
			{id = 2152, chance = 8070}, -- silver
			{id = 2148, count = 5, chance = 97990}, -- gold
		}
	},
	{ name = 'Macabre Demon', file = 'Cirith/Bosses/Quest/macabre demon.xml', look = {type = 279, head = 78, body = 21, legs = 40, feet = 59, addons = 3},
		description = {'Makabryczny Demon', 'Macabre Demon'}, experience = 1080, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'fire', value = 70 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 14455, chance = 10}, --necrotic rod
			{id = 14484, chance = 310}, -- helmet
			{id = 14352, chance = 620}, -- iron sword
			{id = 14324, chance = 930}, -- plate shield
			{id = 7588, chance = 930}, -- shp
			{id = 14362, chance = 2170}, -- plate armor
			{id = 13983, chance = 2170, subType = 200}, -- amulet
			{id = 2149, count = 4, chance = 5900}, -- emerald
			{id = 2152, chance = 8070}, -- silver
			{id = 2148, count = 5, chance = 97990}, -- gold
		}
	},
	{ name = 'Life', file = 'Cirith/Bosses/Quest/life.xml', look = {type = 319},
		description = {'�ycie', 'Life'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'holy', value = 100 },
		} },
	{ name = 'Death', file = 'Cirith/Bosses/Quest/death.xml', look = {type = 319},
		description = {'�mier�', 'Death'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'death', value = 100 },
		} },
	{ name = 'Xargor', file = 'Cirith/Bosses/Quest/xargor.xml', look = {type = 422, addons = 1},
		description = {'Xargor', 'Xargor'}, experience = 8000, health = 2100, healthMax = 2100,
		loot = {
			{id = 14010, chance = 2590}, -- knight armor
			{id = 16356, chance = 5000}, -- container
			{id = 15752, chance = 8900}, -- brass legs
			{id = 15718, chance = 9890}, -- bronze axe
			{id = 2015, chance = 17050}, -- beer
			{id = 2015, chance = 17050}, -- beer
			{id = 2691, chance = 29800}, -- bread
			{id = 15720, chance = 3630}, -- ragged blade
			{id = 2152, chance = 25190}, -- silver
			{id = 2148, count = 7, chance = 98000}, -- coin
		}
	},
	{ name = 'Karlakh', file = 'Cirith/Bosses/Quest/karlakh.xml', look = {type = 419, addons = 3},
		description = {'Karlakh', 'Karlakh'}, experience = 10000, health = 8000, healthMax = 8000,
		loot = {
			{id = 14320, chance = 4000}, -- Royal steel boots
			{id = 16356, chance = 5000}, -- container
			{id = 14010, chance = 6590}, -- knight armor
			{id = 2171, chance = 12000}, -- platinum amulet
			{id = 15752, chance = 18900}, -- brass legs
			{id = 14488, chance = 19000}, -- visor
			{id = 15716, chance = 29890}, -- knight axe
			{id = 2691, chance = 29800}, -- bread
			{id = 15717, chance = 30630}, -- glorious axe
			{id = 2152, chance = 25190}, -- silver
			{id = 2148, count = 11, chance = 98000}, -- coin
		}
	},
	{ name = 'Specter', file = 'Cirith/Bosses/Quest/specter.xml', look = {type = 235},
		description = {'Zjawa', 'Specter'}, experience = 8000, health = 1250, healthMax = 1250,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 1 },
		},
		loot = {
			{id = 2136, chance = 320}, -- demonbone amulet
			{id = 15740, chance = 130}, -- skull bow
			{id = 2165, chance = 190}, -- stealth ring
			{id = 6300, chance = 290}, -- death ring
			{id = 15734, chance = 520}, -- vile axe
			{id = 2134, chance = 890}, -- silver brooch
			{id = 7590, chance = 900}, -- GMP
			{id = 11221, chance = 990}, -- shiny stone
			{id = 2152, count = 7, chance = 3740}, -- silver coin
			{id = 5909, chance = 3800}, -- white piece of cloth
			{id = 5944, chance = 5900}, -- soul orb
			{id = 6500, chance = 6170}, -- demonic essence
			{id = 2071, chance = 9600}, -- lyre
			{id = 14459, chance = 9790}, --woce
			{id = 2152, chance = 41690}, -- silver
			{id = 2148, count = 17, chance = 99680}, -- gold
		}
	},
	{ name = 'Bandit', file = 'Cirith/Bosses/Quest/bandit.xml', look = {type = 346},
		description = {'Bandyta', 'Bandit'}, experience = 88, health = 390, healthMax = 390,
		loot = {
			{id = 15718, chance = 10}, -- bronze axe
			{id = 15786, chance = 350}, -- miedziany pancerz
			{id = 2110, chance = 470}, -- doll
			{id = 15784, chance = 940}, -- miedziana tarcza
			{id = 15752, chance = 2640}, -- brass legs
			{id = 15753, chance = 5030}, -- brass boots
			{id = 15774, chance = 6890}, -- skorzane spodnie
			{id = 14336, chance = 8290}, -- hood
			{id = 2695, count = 2, chance = 9820}, -- egg
			{id = 15783, chance = 9980}, -- rebacz
			{id = 12963, chance = 17050}, -- lampart shield
			{id = 2691, chance = 29800}, -- bread
			{id = 15779, chance = 30630}, -- skuller
			{id = 2148, count = 7, chance = 49230}, -- coin
		}
	},
	{ name = 'Bandit_1', file = 'Cirith/Bosses/Quest/bandit_1.xml', look = {type = 491, addons = 2},
		description = {'Bandyta', 'Bandit'}, experience = 1320, health = 6500, healthMax = 6500,
		loot = {
			{id = 14320, chance = 150}, -- steel boots
			{id = 14010, chance = 390}, -- knight armor
			{id = 2171, chance = 400}, -- platinum amulet
			{id = 15752, chance = 2590}, -- brass legs
			{id = 14488, chance = 4150}, -- visor
			{id = 15716, chance = 4200}, -- knight axe
			{id = 2691, chance = 11990}, -- bread
			{id = 2152, chance = 12420}, -- silver
			{id = 2148, count = 11, chance = 98000}, -- coin
		}
	},
	{ name = 'Bandit_2', file = 'Cirith/Bosses/Quest/bandit_2.xml', look = {type = 490, addons = 3},
		description = {'Bandyta', 'Bandit'}, experience = 1100, health = 4000, healthMax = 4000,
		loot = {
			{id = 2195, chance = 150}, -- boh
			{id = 14010, chance = 390}, -- knight armor
			{id = 14461, chance = 4150}, -- soldier helmet
			{id = 15741, chance = 4200}, -- bow
			{id = 2691, chance = 11990}, -- bread
			{id = 2152, chance = 12420}, -- silver
			{id = 2148, count = 12, chance = 98000}, -- coin
		}
	},
	{ name = 'Bandit_3', file = 'Cirith/Bosses/Quest/bandit_3.xml', look = {type = 476, addons = 2},
		description = {'Bandyta', 'Bandit'}, experience = 1450, health = 6980, healthMax = 6980,
		loot = {
			{id = 13991, chance = 10}, -- mythril armor
			{id = 14321, chance = 150}, -- royal steel legs
			{id = 14170, chance = 400}, -- armor
			{id = 14350, chance = 2590}, -- iron legs
			{id = 14349, chance = 4150}, -- iron helmet
			{id = 15713, chance = 4200}, -- longsword
			{id = 2691, chance = 11990}, -- bread
			{id = 2152, chance = 12420}, -- silver
			{id = 2148, count = 9, chance = 98000}, -- coin
		}
	},
	{ name = 'Bandit_4', file = 'Cirith/Bosses/Quest/bandit_4.xml', look = {type = 473, addons = 1},
		description = {'Bandyta', 'Bandit'}, experience = 1620, health = 8000, healthMax = 8000,
		loot = {
			{id = 14320, chance = 150}, -- steel boots
			{id = 14323, chance = 390}, -- royal steel armor
			{id = 14322, chance = 4150}, -- royal steel helmet
			{id = 15729, chance = 4200}, -- double axe
			{id = 2691, chance = 11990}, -- bread
			{id = 2152, chance = 12420}, -- silver
			{id = 2148, count = 11, chance = 98000}, -- coin
		}
	},
	{ name = 'Kentaru', file = 'Cirith/Bosses/Quest/kentaru.xml', look = {type = 397},
		description = {'Kentaru', 'Kentaru'}, experience = 15000, health = 9000, healthMax = 9000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14319, chance = 2000}, -- royal steel shield
			{id = 14320, chance = 2150}, -- royal steel boots
			{id = 14323, chance = 2390}, -- royal steel armor
			{id = 14321, chance = 2500}, -- royal steel legs
			{id = 14322, chance = 4150}, -- royal steel helmet
			{id = 16356, chance = 5000}, -- container
			{id = 15713, chance = 7990}, -- longsword
			{id = {13055, 13070, 13058, 13061}, chance = 100000}, -- bronze crafting additives
			{id = 2666, chance = 40050}, -- meat
			{id = 2152, count = 5, chance = 12420}, -- silver
			{id = 2148, count = 3, chance = 98000}, -- coin
		}
	},
	{ name = 'Polifem', file = 'Cirith/Bosses/Quest/polifem.xml', look = {type = 22},
		description = {'Polifem', 'Polifem'}, experience = 8000, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 5 },
			{ type = 'physical', value = 10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 40 },
		},
		loot = {
			{id = 7398, chance = 2500}, -- cyclops trophy
			{id = 14354, chance = 29210}, -- bronze helmet
			{id = 16356, chance = 5000}, -- container
			{id = 15780, chance = 46200, subType = 800}, -- zebiasty
			{id = 14349, chance = 35990}, -- iron helmet
			{id = 7618, chance = 96020}, -- health potion
			{id = 15713, chance = 15990}, -- longsword
			{id = 12963, chance = 72590}, -- lampart shield
			{id = 15784, chance = 61420}, -- copper shield
			{id = 10573, chance = 95050}, -- cyclops toe
			{id = 15776, chance = 27940}, -- short sword
			{id = 2666, chance = 40050}, -- meat
			{id = 2148, count = 9, chance = 92130}, -- gold
		}
	},
	{ name = 'TrineTarget', file = 'Cirith/Bosses/Quest/target.xml', look = {type = 0, typeEx = 5777, auxType = 5777},
		description = {'Target', 'Target'}, experience = 0, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Training', file = 'Cirith/Bosses/Quest/training.xml', look = {type = 0, typeEx = 5787, auxType = 5787}, 
		description = {'Target', 'Target'}, experience = 0, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Mimic', file = 'Cirith/Bosses/Quest/mimic.xml', look = {type = 523},
		description = {'Mimic', 'Mimic'}, experience = 0, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 80 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 5 },
		} },
	{ name = 'Mimic Crate Boss', file = 'Cirith/Bosses/Quest/mimic behemoth.xml', look = {type = 523},
		description = {'Mimik', 'Mimic'}, experience = 15000, health = 7000, healthMax = 7000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 80 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 5 },
		},
		loot = {
			{id = 16435, chance = 50}, -- bal gown
			{id = 13890, chance = 500}, -- bow of the miser
			{id = 16269, chance = 1000}, -- crystal spike backpack
			{id = 14006, chance = 2500}, -- cape of fear
			{id = 16305, chance = 5700}, -- enhanced leather hood
			{id = 12964, chance = 7000}, -- lunar crystal
			{id = {11258, 9837}, chance = 7250}, -- dragon/velvet tapestry
			{id = {13055, 13070, 13058, 13061}, chance = 5000}, -- bronze crafting additives
			{id = 14358, chance = 11250}, -- bronze axe
			{id = {2143, 2144}, count = 2, chance = 12580}, -- pearls
			{id = 2145, count = 3, chance = 13140}, -- small diamond
			{id = {2146, 2147, 2149, 2150}, count = 5, chance = 15600}, -- small crystals
			{id = 2152, count = 5, chance = 19400}, -- talon
			{id = 2120, chance = 21150}, -- rope
			{id = {2153, 2154, 2155, 2156, 2158}, chance = 25000}, -- gem
			{id = 5889, count = 2, chance = 31320}, -- piece of steel
			{id = 8309, count = 5, chance = 25890}, -- nail
			{id = 2050, chance = 42340}, -- torch
			{id = 14014, chance = 56350}, -- robe
			{id = 14765, chance = 61900}, -- cape
			{id = 2560, chance = 92340}, -- mirror
			{id = 2152, count = 2, chance = 41690}, -- silver
		}
	},
	{ name = 'Szegesfehevar', file = 'Cirith/Bosses/Quest/szegesfehevar.xml', look = {type = 285},
		description = {'Szegesfehevar', 'Szegesfehevar'}, experience = 20000, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 85 },
		} },
	{ name = 'Mulch Invisible', file = 'Cirith/Bosses/Quest/mulch invisible.xml', look = {type = 161},
		description = {'Chocho�', 'Mulch'}, experience = 0, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Mulch', file = 'Cirith/Bosses/Quest/mulch.xml', look = {type = 246}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Chocho�', 'Mulch'}, experience = 30000, health = 21000, healthMax = 21000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 70 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = 90 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 16197, chance = 1020}, -- druid cape
			{id = 16143, chance = 5180}, -- golden blade
			{id = 16167, chance = 5000}, --swamplair armor
			{id = 6300, chance = 21330}, -- death ring
			{id = 16478, chance = 900}, -- lifeleecher backpack
			{id = 13800, chance = 8500}, -- nepethes seeds
			{id = 13886, chance = 4800}, --haunted blade
			{id = 16356, chance = 10000}, -- container
			{id = 2199, chance = 12270, subType = 300}, -- garlic necklacke
			{id = 15757, chance = 8690}, --snake legs
			{id = 3983, chance = 2970}, -- bast skirt
			{id = 2550, chance = 3260}, --scythe
			{id = 7732, chance = 3810}, -- seeds
			{id = 12976, chance = 4000}, -- Earthy lunar crystal (disarmament)
			{id = {7632, 7633}, chance = 34330}, --giant pearl
			{id = 9971, chance = 15400}, -- gold ingot
			{id = 10219, chance = 46160, subType = 8}, -- sacreed tree amulet
			{id = 9810,	chance = 7390}, -- rusty armor(rare)
			{id = 8473, count = 2, chance = 94370}, -- GHP
			{id = 10604, chance = 85100}, -- bundle of cursed straw
			{id = 5944, chance = 84620}, -- soul orb
			{id = 6500, chance = 99660}, -- demonic essence
			{id = 8472, count = 3, chance = 50750}, -- GMP
			{id = 2214, chance = 64900}, -- roh
			{id = 2545, count = 49, chance = 41390}, -- poison arrow
			{id = 2694, chance = 79950}, -- bunch of wheat
			{id = 2168, chance = 99900}, -- life ring
			{id = 2152, count = 2, chance = 78900}, -- silver
			{id = 2148, count = 13, chance = 97520}, -- gold
		}
	},
	{ name = 'Mulch Spawn', file = 'Cirith/Bosses/Quest/mulch spawn.xml', look = {type = 74},
		description = {'Pos�aniec Chocho�a', 'Mulch Spawn'}, experience = 25, health = 100, healthMax = 100,
		loot = {
			{id = 2684, count = 2, chance = 1710}, -- carrot
			{id = 2666, chance = 4540}, -- meat
		}
	},
	{ name = 'Wild Growth', file = 'Cirith/Bosses/Quest/wild growth.xml', look = {type = 0, typeEx = 1499, auxType = 2130},
		description = {'Wild Growth', 'Wild Growth'}, experience = 0, health = 10, healthMax = 10,
		elements = {
			{ type = 'fire', value = -25 },
			{ type = 'ice', value = -25 },
			{ type = 'physical', value = -25 },
		} },
	{ name = 'Suicide', file = 'Cirith/Bosses/Quest/suicide.xml', look = {type = 378},
		description = {'Krwawnik', 'Suicide'}, experience = 5, health = 325, healthMax = 325 },
	{ name = 'Hardouin Monster', file = 'Cirith/Bosses/Quest/Hardouin.xml', look = {type = 493},
		description = {'Hardouin', 'Hardouin'}, experience = 0, health = 560, healthMax = 560,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'death', value = -15 },
			{ type = 'wind', value = -5 },
		},
		loot = {
			{id = 16356, chance = 5000}, -- container
			{id = {15789, 15790}, chance = 35000}, -- copper sword/axe
			{id = 15751, chance = 58760}, -- brass armor
			{id = 7618, count = 4, chance = 99700}, -- health potion
			{id = 2671, count = 2, chance = 84370}, -- ham
			{id = 14337, chance = 96480}, -- orc shield
			{id = 2152, chance = 87900}, -- silver gold
			{id = 2148, count = 13, chance = 87900}, -- gold
		}
	},
	{ name = 'Gustliq', file = 'Cirith/Bosses/Quest/Gustliq.xml', look = {type = 549, head = 22, body = 28, legs = 6, feet = 45},
		description = {'Gustliq', 'Gustliq'}, experience = 8500, health = 3400, healthMax = 3400,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'death', value = -15 },
			{ type = 'energy', value = 10 },
		},
		loot = {
			{id = 16307, chance = 3100}, -- legs
			{id = 2656, chance = 5000}, -- blue robe
			{id = 16143, chance = 14280}, -- golden blade
			{id = 14003, chance = 20310}, -- light boots
			{id = 14765, chance = 49550}, -- cape
			{id = 16932, count = 6, chance = 93000}, -- shp
			{id = 2152, count = 2, chance = 41690}, -- silver
			{id = 2148, count = 25, chance = 97990}, -- gold
		}
	},
	{ name = 'Curse Blob', file = 'Cirith/Bosses/Quest/curse blob.xml', look = {type = 316},
		description = {'Szlam', 'Slime'}, experience = 0, health = 300, healthMax = 300,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = 65 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 15 },
		} },
	{ name = 'Swamply Blob', file = 'Cirith/Bosses/Quest/swamply blob.xml', look = {type = 314},
		description = {'Szlam', 'Slime'}, experience = 0, health = 300, healthMax = 300,
		elements = {
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		} },
	{ name = 'Death Blob', file = 'Cirith/Bosses/Quest/death blob.xml', look = {type = 315},
		description = {'Szlam', 'Slime'}, experience = 0, health = 300, healthMax = 300,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		} },
	{ name = 'Deadly Blob', file = 'Cirith/Bosses/Quest/deadly blob.xml', look = {type = 315}},
	{ name = 'Smoke Blob', file = 'Cirith/Bosses/Quest/smoke blob.xml', look = {type = 315},
		description = {'Szlam', 'Slime'}, experience = 0, health = 300, healthMax = 300,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		} },
	{ name = 'Bloody Spider', file = 'Cirith/Bosses/Quest/bloody spider.xml', look = {type = 536}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Krwawy Paj�k', 'Bloody Spider'}, experience = 5, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'energy', value = 35 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 14244, chance = 250}, -- bag (quest)
			{id = 11794, chance = 50}, -- rose armor
			{id = 14327, chance = 40}, -- plate legs
			{id = 14362, chance = 950}, -- plate armor
			{id = 15717, chance = 5250}, -- glorious axe
			{id = 14432, chance = 110}, -- cobalt helmet
			{id = 14355, chance = 8330}, -- bronze legs
			{id = 7435, chance = 550}, -- impaler
			{id = 5879, chance = 1090}, -- gss
			{id = 7887, chance = 300}, -- toxic amulet
			{id = 7588, chance = 3460}, -- SHP
			{id = 8859, chance = 6970}, -- spider fang
			{id = 2545, count = 12, chance = 12160}, -- poison arrows
			{id = 2148, count = 3, chance = 99900}, -- coin
		}
	},
	{ name = 'Cursed Spider', file = 'Cirith/Bosses/Quest/cursed spider.xml', look = {type = 535}, classId = 28, killAmount = 500, charmPoints = 15,
		description = {'Przekl�ty Paj�k', 'Cursed Spider'}, experience = 0, health = 1500, healthMax = 1500,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
		},
		loot = {
			{id = 7887, chance = 90}, -- toxic amulet
			{id = 11192, chance = 99700}, -- tarantula egg
		}
	},
	
	-- Animals
	{ name = 'Azure Frog', level = 1, file = 'Cirith/Animals/azure frog.xml', look = {type = 226, head = 69, body = 66, legs = 69, feet = 66}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Bagienna �aba', 'Azure Frog'}, experience = 0, health = 9, healthMax = 9,
		loot = {
			{id = 3976, chance = 9760}, -- worm
		}
	},
	{ name = 'Bat', level = 2, file = 'Cirith/Animals/bat.xml', look = {type = 122}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Nietoperz', 'Bat'}, experience = 15, health = 30, healthMax = 30,
		elements = {
			{ type = 'earth', value = 5 },
		},
		loot = {
			{id = 6492, chance = 10}, -- bat decoration
			{id = 5894, chance = 2140}, -- bat wing
			{id = 2148, chance = 2140}, -- coin
			{id = 13155, chance = 1890}, -- untreated leather
			{id = 13706, chance = 1640}, -- beastly claw
		}
	},
	{ name = 'Bear', level = 4, file = 'Cirith/Animals/bear.xml', look = {type = 16}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Nied�wied�', 'Bear'}, experience = 40, health = 80, healthMax = 80,
		boss = 'Animalus', chance = 100,
		elements = {
			{ type = 'death', value = -5 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 14696, chance = 1470}, -- honeycomb
			{id = 5896, chance = 5960}, -- bear paw
			{id = 2671, count = 3, chance = 19820}, -- ham
			{id = 13706, chance = 2930}, -- beastly claw
			{id = 2666, count = 4, chance = 39730}, -- meat
			{id = 13155, chance = 7800}, -- untreated leather
		}
	},
	{ name = 'Black Sheep', level = 1, file = 'Cirith/Animals/black sheep.xml', look = {type = 13}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Czarna Owca', 'Black Sheep'}, experience = 0, health = 30, healthMax = 30,
		loot = {
			{id = 2666, count = 4, chance = 69730}, -- meat
			{id = 13155, chance = 3410}, -- untreated leather
			{id = 13735, chance = 970}, -- black wool
		}
	},
	{ name = 'Mammoth', level = 15, file = 'Cirith/Animals/mammoth.xml', look = {type = 199}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Mamut', 'Mammoth'}, experience = 400, health = 825, healthMax = 825,
		boss = 'Animalus', chance = 50,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'physical', value = 20 },
			{ type = 'bleed', value = 15 },
			{ type = 'ice', value = 70 },
		},
		loot = {
			{id = 13584, chance = 1800}, -- primitive spear
			{id = 12962, chance = 970}, -- lampart spear
			{id = 14004, chance = 30}, -- trapper boots
			{id = 13706, count = 2, chance = 1990}, -- beastly claw
			{id = 2671, count = 3, chance = 39820}, -- ham
			{id = 11232, chance = 1020}, -- mammoth tusk
			{id = 11218, chance = 5340}, -- thick fur
		}
	},
	{ name = 'Blazing Rotworm', level = 15, file = 'Cirith/Animals/blazing rotworm.xml', look = {type = 626}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Ognista Poczwara', 'Blazing Rotworm'}, experience = 170, health = 325, healthMax = 325,
		boss = 'Old Queen', chance = 80,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 55 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 8900, chance = 70}, -- spellbook
			{id = 18072, chance = 110}, -- blazing stick
			{id = 14012, chance = 220}, -- crimson sword
			{id = {13055, 13070, 13058, 13061}, chance = 120}, -- bronze crafting additives
			{id = 10608, chance = 10550}, -- lump of dirt
			{id = 2795, chance = 310}, -- fire mushroom
			{id = 14353, chance = 600}, -- bronze armor
			{id = 3976, count = 4, chance = 21870}, -- worm
			{id = 11186, chance = 9280}, -- carrion worm fang
			{id = 2666, count = 2, chance = 18240}, -- meat
			{id = 2148, count = 18, chance = 21760}, -- coin
		}
	},
	{ name = 'Wasp', level = 3, file = 'Cirith/Animals/wasp.xml', look = {type = 44}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Olbrzymia Osa', 'Giant Wasp'}, experience = 19, health = 35, healthMax = 35,
		boss = 'Waspero', chance = 20,
		elements = {
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 14696, chance = 8840}, -- honeycomb
		}
	},
	{ name = 'Blue Butterfly', level = 1, file = 'Cirith/Animals/blue butterfly.xml', look = {type = 227}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Niebieski Motyl', 'Blue Butterfly'}, experience = 0, health = 2, healthMax = 2 },
	{ name = 'Boar', level = 4, file = 'Cirith/Animals/boar.xml', look = {type = 384}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Dzik', 'Boar'}, experience = 40, health = 85, healthMax = 85,
		boss = 'Animalus', chance = 100,
		loot = {
			{id = 13914, chance = 490}, -- boar loincloth
			{id = 13917, chance = 2150}, -- boar hooves
			{id = 13915, chance = 2100}, -- boar tusks
			{id = 13155, chance = 9980}, -- untreated leather
			{id = 14338, count = 2, chance = 19790}, -- Haunch of Boar
		}
	},
	{ name = 'Brown Horse', level = 1, file = 'Cirith/Animals/brown horse.xml', look = {type = 452},
		description = {'Br�zowy Ko�', 'Brown Horse'}, experience = 0, health = 100, healthMax = 100 },
	{ name = 'Carrion Worm', level = 8, file = 'Cirith/Animals/carrion worm.xml', look = {type = 192}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Poczwara Jaskiniowa', 'Carrion Worm'}, experience = 105, health = 200, healthMax = 200,
		boss = 'Old Queen', chance = 50,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 15793, chance = 2000}, -- sword
			{id = 14357, chance = 390}, -- bronze shield
			{id = 15738, chance = 850}, -- long bow
			{id = 11186, chance = 990}, -- carrion worm fang
			{id = 14175, chance = 17840}, -- coal
			{id = 13707, chance = 730}, -- cuprite
			{id = 10559, chance = 3120}, -- horn
			{id = 5880, chance = 900}, -- iron ore
			{id = 13706, chance = 1090}, -- beastly claw
			{id = 3976, count = 4, chance = 12000}, -- worm
			{id = 2671, count = 3, chance = 19820}, -- ham
			{id = 2148, count = 14, chance = 43430}, -- gold
		}
	},
	{ name = 'Cave Rat', level = 3, file = 'Cirith/Animals/cave rat.xml', look = {type = 56}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Szczur Jaskiniowy', 'Cave Rat'}, experience = 15, health = 30, healthMax = 30,
		boss = 'Rattus', chance = 80,
		elements = {
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2687, chance = 590}, -- cookie
			{id = 3976, count = 2, chance = 4890}, -- worm
			{id = 13706, chance = 1900}, -- beastly claw
			{id = 8368, chance = 19500}, -- cheese
			{id = 2148, chance = 21890}, -- coin
		}
	},
	{ name = 'Centipede', level = 3, file = 'Cirith/Animals/centipede.xml', look = {type = 124}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Skolopendra', 'Centipede'}, experience = 40, health = 70, healthMax = 70,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 11212, chance = 10150}, -- centipede leg
			{id = 2148, chance = 80170}, -- gold
		}
	},
	{ name = 'Chicken', level = 1, file = 'Cirith/Animals/chicken.xml', look = {type = 111}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Kura', 'Chicken'}, experience = 0, health = 30, healthMax = 30,
		loot = {
			{id = 2695, chance = 880}, -- egg
			{id = 3976, chance = 7690}, -- worm
			{id = 5890, chance = 15090}, -- chicken feather
			{id = 2666, chance = 21450}, -- meat
		}
	},
	{ name = 'Coral Frog', level = 1, file = 'Cirith/Animals/coral frog.xml', look = {type = 226, head = 114, body = 98, legs = 97, feet = 114}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Koralowa �aba', 'Coral Frog'}, experience = 30, health = 60, healthMax = 60,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 3976, chance = 13730}, -- worm
			{id = 2148, chance = 77340}, -- copper coin
		}
	},
	{ name = 'Corym Charlatan', level = 18, file = 'Cirith/Animals/corym charlatan.xml', look = {type = 571, head = 0, body = 78, legs = 97, feet = 0}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Szczurak Szarlatan', 'Corym Charlatan'}, experience = 120, health = 180, healthMax = 180,
		boss = 'Voltar The Thief', chance = 80,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 10 },
		},
		loot = {
			{id = 2129, chance = 210}, -- Wolf Tooth Chain
			{id = 17267, chance = 10}, -- doll
			{id = 15722, chance = 300}, -- scimitar
			{id = 13706, count = 2, chance = 1760}, -- beastly claw
			{id = 17866, chance = 290}, -- mantreads
			{id = 5880, chance = 970}, -- iron ore
			{id = 14175, chance = 7000}, -- coal
			{id = 2628, chance = 120}, -- the armor of hardened leather
			{id = {15752, 15751}, chance = 510}, -- brass legs/armor
			{id = 14484, chance = 180}, -- guard's helmet
			{id = {7618, 7620}, chance = 250}, -- light health/mana potion
			{id = 14317, chance = 90}, -- enchanced oak shield
			{id = {2235, 8368}, chance = 9660}, -- cheese
			{id = 2148, count = 12, chance = 81310}, -- gold
		}
	},
	{ name = 'Corym Skirmisher', level = 20, file = 'Cirith/Animals/corym skirmisher.xml', look = {type = 572, head = 0, body = 95, legs = 83, feet = 0}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Szczurak Harcownik', 'Corym Skirmisher'}, experience = 130, health = 200, healthMax = 200,
		boss = 'Voltar The Thief', chance = 80,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 10 },
		},
		loot = {
			{id = 17267, chance = 10}, -- doll
			{id = 15753, chance = 200}, -- brass boots
			{id = {15752, 15751}, chance = 280}, -- brass legs/armor
			{id = 13706, chance = 1990}, -- beastly claw
			{id = 14175, chance = 6990}, -- coal
			{id = 14355, chance = 290}, -- bronze legs
			{id = 14317, chance = 200}, -- enchanced oak shield
			{id = 15722, chance = 400}, -- scimitar
			{id = 5880, chance = 1000}, -- iron ore
			{id = 17866, chance = 370}, -- mantreads
			{id = 14765, chance = 1590}, -- cape
			{id = {13055, 13070, 13058, 13061}, chance = 10}, -- bronze crafting additives
			{id = 2643, chance = 190}, -- breastplate
			{id = 14357, chance = 250}, -- bronze shield
			{id = {7618, 7620}, chance = 490}, -- light health/mana potion
			{id = {2235, 8368}, chance = 19700}, -- cheese
			{id = 2148, count = 16, chance = 78080}, -- gold
		}
	},
	{ name = 'Corym Vanguard', level = 23, file = 'Cirith/Animals/corym vanguard.xml', look = {type = 573, head = 0, body = 57, legs = 102, feet = 0}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Szczurak Rozrabiaka', 'Corym Vanguard'}, experience = 165, health = 270, healthMax = 270,
		boss = 'Voltar The Thief', chance = 80,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 15753, chance = 320}, -- brass boots
			{id = {15787, 14353}, chance = 680}, -- copper legs/bronze armor
			{id = 14461, chance = 290}, -- soldier helmet
			{id = 5880, chance = 950}, -- iron ore
			{id = 17267, chance = 10}, -- doll
			{id = 14339, chance = 10}, -- iron shield
			{id = 13706, chance = 1010}, -- beastly claw
			{id = 15738, chance = 250}, -- long bow
			{id = 15776, chance = 200}, -- killer's blade
			{id = 5878, chance = 650}, -- hardener leather
			{id = 15782, chance = 290}, -- steel spear
			{id = {7618, 7620}, chance = 400}, -- light health/mana potion
			{id = 2643, chance = 490}, -- breastplate
			{id = {2235, 8368}, chance = 14520}, -- cheese
			{id = 2148, count = 18, chance = 90100}, -- gold
		}
	},
	{ name = 'Crimson Frog', level = 1, file = 'Cirith/Animals/crimson frog.xml', look = {type = 226, head = 94, body = 78, legs = 94, feet = 78}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Kazmazynowa �aba', 'Crimson Frog'}, experience = 30, health = 60, healthMax = 60,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 3976, chance = 8950}, -- worm
			{id = 2148, chance = 73630}, -- copper coin
		}
	},
	{ name = 'Crocodile', level = 10, file = 'Cirith/Animals/crocodile.xml', look = {type = 119}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Krokodyl', 'Crocodile'}, experience = 60, health = 105, healthMax = 105,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 11190, chance = 20050}, -- piece of crocodile leather
			{id = 14000, chance = 10}, -- light armor
			{id = 2671, chance = 39780}, -- ham
			{id = 2148, count = 9, chance = 49960}, -- gold
		}
	},
	{ name = 'Dark Brown Horse', level = 1, file = 'Cirith/Animals/dark brown horse.xml', look = {type = 454},
		description = {'Dark Brown Horse', 'Dark Brown Horse'}, experience = 0, health = 100, healthMax = 100 },
	{ name = 'White Deer', level = 1, file = 'Cirith/Animals/white deer.xml', look = {type = 426}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Bia�y Jele�', 'White Deer'}, experience = 0, health = 600, healthMax = 600,
		loot = {
			{id = 14160, chance = 100000}, -- antlers
			{id = 14161, chance = 9700}, -- white deer fur
			{id = 2671, count = 3, chance = 19680}, -- ham
			{id = 2666, count = 3, chance = 42120}, -- meat
		}
	},
	{ name = 'Deer', level = 1, file = 'Cirith/Animals/deer.xml', look = {type = 31}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Jele�', 'Deer'}, experience = 0, health = 15, healthMax = 15,
		loot = {
			{id = 11208, chance = 1870}, -- antlers
			{id = 2671, count = 2, chance = 9680}, -- ham
			{id = 2666, count = 3, chance = 42120}, -- meat
			{id = 13155, chance = 3190}, -- untreated leather
		}
	},
	{ name = 'Dirtbite', level = 10, file = 'Cirith/Animals/dirtbite.xml', look = {type = 484}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Ziemiogryz', 'Dirtbite'}, experience = 225, health = 335, healthMax = 335,
		loot = {
			{id = 15782, chance = 200}, -- steel spear
			{id = 3976, count = 4, chance = 21870}, -- worm
			{id = 10608, chance = 990}, -- lump of dirt
			{id = 10559, chance = 3120}, -- horn
			{id = 2671, chance = 7650}, -- ham
			{id = 5880, chance = 5000}, -- iron ore
			{id = 14175, chance = 6290}, -- coal
			{id = 14324, chance = 50}, -- steel shield
			{id = 2666, chance = 18240}, -- meat
			{id = 1294, count = 3, chance = 4170}, -- small stone
			{id = 2148, count = 20, chance = 28760}, -- coin
		}
	},
	{ name = 'Green Frog', level = 1, file = 'Cirith/Animals/green frog.xml', look = {type = 224}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Zielona �aba', 'Green Frog'}, experience = 0, health = 20, healthMax = 20 },
	{ name = 'Grey Horse', level = 1, file = 'Cirith/Animals/grey horse.xml', look = {type = 450},
		description = {'Szary Ko�', 'Grey Horse'}, experience = 0, health = 100, healthMax = 100 },
	{ name = 'Hyaena', level = 5, file = 'Cirith/Animals/hyaena.xml', look = {type = 94}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Hiena', 'Hyaena'}, experience = 30, health = 60, healthMax = 60,
		loot = {
			{id = 3976, count = 3, chance = 50130}, -- worm
			{id = 2666, count = 2, chance = 30740}, -- meat
		}
	},
	{ name = 'Lion', level = 13, file = 'Cirith/Animals/lion.xml', look = {type = 41}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Lew', 'Lion'}, experience = 40, health = 80, healthMax = 80,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -8 },
			{ type = 'earth', value = 20 },
			{ type = 'ice', value = -15 },
		},
		loot = {
			{id = 10607, chance = 1250}, -- lion mane
			{id = 2666, count = 4, chance = 45380}, -- meat
			{id = 2671, count = 2, chance = 18770}, -- ham
		}
	},
	{ name = 'Orchid Frog', level = 1, file = 'Cirith/Animals/orchid frog.xml', look = {type = 226, head = 109, body = 14, legs = 109, feet = 114}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Purpurowa �aba', 'Orchid Frog'}, experience = 30, health = 60, healthMax = 60,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 3976, chance = 7580}, -- worm
			{id = 2148, chance = 73180}, -- copper coin
		}
	},
	{ name = 'Panda', level = 10, file = 'Cirith/Animals/panda.xml', look = {type = 123}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Panda', 'Panda'}, experience = 40, health = 80, healthMax = 80,
		elements = {
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 6568, chance = 1}, -- plushy panda
			{id = 13732, chance = 10690}, -- bamboo stick
			{id = 2666, count = 4, chance = 68970}, -- meat
			{id = 2671, count = 2, chance = 40670}, -- ham
		}
	},
	{ name = 'Parrot', level = 1, file = 'Cirith/Animals/parrot.xml', look = {type = 217}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Papuga', 'Parrot'}, experience = 0, health = 25, healthMax = 25 },
	{ name = 'Penguin', level = 1, file = 'Cirith/Animals/penguin.xml', look = {type = 250}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Pingwin', 'Penguin'}, experience = 1, health = 33, healthMax = 33,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'ice', value = 1 },
		},
		loot = {
			{id = 7158, chance = 60}, -- rainbow trout
			{id = 2159, chance = 130}, -- green perch
			{id = 2667, count = 2, chance = 7650}, -- fish
		}
	},
	{ name = 'Pig', level = 1, file = 'Cirith/Animals/pig.xml', look = {type = 60}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'�winia', 'Pig'}, experience = 0, health = 25, healthMax = 25,
		loot = {
			{id = 10609, chance = 3210}, -- pig foot
			{id = 2666, count = 4, chance = 51240}, -- meat
			{id = 2671, count = 2, chance = 47890}, -- ham
			{id = 13155, chance = 1890}, -- untreated leather
		}
	},
	{ name = 'Pink Butterfly', level = 1, file = 'Cirith/Animals/pink butterfly.xml', look = {type = 213}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'R�owy Motyl', 'Pink Butterfly'}, experience = 0, health = 2, healthMax = 2 },
	{ name = 'Polar Bear', level = 12, file = 'Cirith/Animals/polar bear.xml', look = {type = 42}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Nied�wied� Polarny', 'Polar Bear'}, experience = 45, health = 85, healthMax = 85,
		boss = 'Animalus', chance = 100,
		elements = {
			{ type = 'death', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 10566, chance = 930}, -- polar bear paw
			{id = 2671, count = 2, chance = 50810}, -- ham
			{id = 2666, count = 4, chance = 50710}, -- meat
		}
	},
	{ name = 'Rabbit', level = 1, file = 'Cirith/Animals/rabbit.xml', look = {type = 74}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Kr�lik', 'Rabbit'}, experience = 0, health = 5, healthMax = 5,
		loot = {
			{id = 2684, count = 2, chance = 8710}, -- carrot
			{id = 2666, chance = 74540}, -- meat
			{id = 2218, chance = 20}, -- paw amulet
			{id = 13155, chance = 2560}, -- untreated leather
		}
	},
	{ name = 'Rat', level = 3, file = 'Cirith/Animals/rat.xml', look = {type = 21}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Szczur', 'Rat'}, experience = 10, health = 20, healthMax = 20,
		boss = 'Rattus', chance = 50,
		elements = {
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = {2235, 8368}, chance = 11560}, -- cheese
			{id = 3976, chance = 26780}, -- worm
			{id = 13706, chance = 760}, -- beastly claw
			{id = 2148, count = 2, chance = 37690}, -- coin
		}
	},
	{ name = 'Red Butterfly', level = 1, file = 'Cirith/Animals/red butterfly.xml', look = {type = 228}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Czerwony Motyl', 'Red Butterfly'}, experience = 0, health = 2, healthMax = 2},
	{ name = 'Rotworm Queen', level = 24, file = 'Cirith/Animals/rotworm queen.xml', look = {type = 295}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Kr�lowa Poczwar', 'Rotworm Queen'}, experience = 375, health = 530, healthMax = 530,
		boss = 'Old Queen', chance = 150,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = -10 },
			{ type = 'wind', value = -5 },
		},
		loot = {
			{id = 14326, chance = 50}, -- steel boots
			{id = 14357, chance = 190}, -- bronze shield
			{id = 8971, chance = 10}, -- gland
			{id = 14175, chance = 380}, -- coal
			{id = 15718, chance = 290}, -- chopper
			{id = 14015, chance = 500}, -- short bow
			{id = 15782, chance = 110}, -- stalowy oszczep
			{id = 15751, chance = 1000}, -- brass armor
			{id = 5880, chance = 4510}, -- iron ore
			{id = 11186, chance = 2110}, -- carrion worm fang
			{id = 2791, count = 2, chance = 3010}, -- wood mushroom
			{id = 14334, chance = 4100}, -- heavy axe
			{id = 17656, chance = 680}, -- copper blade
			{id = 2671, count = 3, chance = 19820}, -- ham
			{id = 14175, count = 2, chance = 11900}, -- coal
			{id = 2666, count = 9, chance = 25490}, -- meat
			{id = 14353, chance = 1020}, -- bronze armor
			{id = 3976, count = 4, chance = 12000}, -- worm
			{id = 2148, count = 21, chance = 68000}, -- gold
		}
	},
	{ name = 'Rotworm', level = 15, file = 'Cirith/Animals/rotworm.xml', look = {type = 26}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Poczwara', 'Rotworm'}, experience = 35, health = 65, healthMax = 65,
		boss = 'Old Queen', chance = 40,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = {15793, 15783}, chance = 2210}, -- sword/breaker
			{id = 15780, chance = 300, subType = 400}, -- tooth necklace
			{id = 3976, count = 4, chance = 21870}, -- worm
			{id = 2791, chance = 1010}, -- wood mushroom
			{id = 5880, chance = 1000}, -- iron ore
			{id = 13707, chance = 490}, -- cuprite
			{id = 2671, chance = 7650}, -- ham
			{id = 14175, chance = 16590}, -- coal
			{id = 11186, chance = 9280}, -- carrion worm fang
			{id = 10608, chance = 10550}, -- lump of dirt
			{id = 13706, chance = 1150}, -- beastly claw
			{id = 2666, chance = 18240}, -- meat
			{id = 2148, count = 11, chance = 21760}, -- coin
		}
	},
	{ name = 'Seagull', level = 1, file = 'Cirith/Animals/seagull.xml', look = {type = 223}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Mewa', 'Seagull'}, experience = 0, health = 100, healthMax = 100 },
	{ name = 'Sheep', level = 1, file = 'Cirith/Animals/sheep.xml', look = {type = 14}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Owca', 'Sheep'}, experience = 0, health = 10, healthMax = 10,
		elements = {
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2671, count = 2, chance = 44560}, -- ham
			{id = 2666, count = 4, chance = 79780}, -- meat
			{id = 11230, chance = 85160}, -- wool
			{id = 13155, chance = 3150}, -- untreated leather
		}
	},
	{ name = 'Silver Rabbit', level = 1, file = 'Cirith/Animals/silver rabbit.xml', look = {type = 262}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Srebrny Zaj�c', 'Silver Rabbit'}, experience = 0, health = 60, healthMax = 60,
		loot = {
			{id = 2684, chance = 11010}, -- carrot
			{id = 11203, chance = 29240}, -- silky fur
			{id = 2666, count = 2, chance = 85490}, -- meat
		}
	},
	{ name = 'Squirrel', level = 1, file = 'Cirith/Animals/squirrel.xml', look = {type = 74}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'Wiewi�rka', 'Squirrel'}, experience = 0, health = 8, healthMax = 8,
		loot = {
			{id = 7909, chance = 470}, -- walnut
			{id = 7910, chance = 1420}, -- peanut
			{id = 11207, chance = 28790}, -- acorn
		}
	},
	{ name = 'Stampor', level = 40, file = 'Cirith/Animals/stampor.xml', look = {type = 325}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Nosoro�ec', 'Stampor'}, experience = 680, health = 1200, healthMax = 1200,
		elements = {
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14318, chance = 50}, -- royal steel blade
			{id = 13991, chance = 30}, -- mythril armor
			{id = 15721, chance = 110}, -- ragged blade
			{id = 11667, chance = 9530}, -- stampor horn
			{id = 11668, chance = 11420}, -- stampor hoof
			{id = 7588, count = 2, chance = 860}, -- SHP
			{id = 7589, count = 2, chance = 530}, -- SMP
			{id = 9970, count = 2, chance = 7950}, -- small topaz
			{id = 2152, chance = 41690}, -- silver
			{id = 2671, count = 2, chance = 45130}, -- ham
			{id = 2148, count = {10, 35}, chance = 94920}, -- gold
		}
	},
	{ name = 'Starving Bear', level = 28, file = 'Cirith/Animals/starving bear.xml', look = {type = 91}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Wyg�odnia�y Nied�wied�', 'Starving Bear'}, experience = 180, health = 330, healthMax = 330,
		boss = 'Animalus', chance = 180,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 14696, chance = 6470}, -- honeycomb
			{id = 13913, chance = 5100}, -- bear skull
			{id = 13155, chance = 6390}, -- untreated leather
			{id = 5896, chance = 3960}, -- bear paw
			{id = 13706, chance = 6580}, -- beastly claw
			{id = 13343, chance = 1200}, -- fur paw
			{id = 13912, chance = 8410}, -- bear fur
			{id = 2671, count = 3, chance = 39820}, -- ham
			{id = 2666, count = 4, chance = 69730}, -- meat
		}
	},
	{ name = 'Starving Boar', level = 30, file = 'Cirith/Animals/starving boar.xml', look = {type = 90}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Wyg�odnia�y Dzik', 'Starving Boar'}, experience = 125, health = 230, healthMax = 230,
		boss = 'Animalus', chance = 180,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'holy', value = 15 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13914, chance = 690}, -- boar loincloth
			{id = 13917, chance = 4150}, -- boar hooves
			{id = 13915, chance = 5100}, -- boar tusks
			{id = 14338, count = 2, chance = 19790}, -- Haunch of Boar
		}
	},
	{ name = 'Starving Wolf', level = 25, file = 'Cirith/Animals/starving wolf.xml', look = {type = 92}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Wyg�odnia�y Wilk', 'Starving Wolf'}, experience = 70, health = 125, healthMax = 125,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -1 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13916, chance = 290}, -- wolf amulet
			{id = 7394, chance = 320}, -- wolf trophy
			{id = 14335, chance = 890}, -- wolf helmet
			{id = 5897, chance = 3470}, -- wolf paw
			{id = 11229, chance = 10690}, -- warwolf fur
			{id = 2666, count = 3, chance = 76200}, -- meat
		}
	},
	{ name = 'Tiger', level = 15, file = 'Cirith/Animals/tiger.xml', look = {type = 125}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Tygrys', 'Tiger'}, experience = 40, health = 75, healthMax = 75,
		elements = {
			{ type = 'death', value = -10 },
		},
		loot = {
			{id = 11204, chance = 10600}, -- striped fur
			{id = 2666, count = 4, chance = 32830}, -- meat
		}
	},
	{ name = 'Toad', level = 8, file = 'Cirith/Animals/toad.xml', look = {type = 222}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Ropucha', 'Toad'}, experience = 70, health = 135, healthMax = 135,
		elements = {
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 2148, count = 10, chance = 23450}, -- gold
			{id = 2667, count = 2, chance = 7590}, -- fish
			{id = 15714, chance = 2310}, -- club
			{id = 14000, chance = 90}, -- wooden armor
			{id = 2143, chance = 280}, -- white pearl
		}
	},
	{ name = 'Tortoise', level = 16, file = 'Cirith/Animals/tortoise.xml', look = {type = 197}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'��w', 'Tortoise'}, experience = 100, health = 185, healthMax = 185,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 6131, chance = 10}, -- tortoise shield
			{id = 5899, chance = 1010}, -- turtle shell
			{id = 12963, chance = 2910}, -- lampart shield
			{id = 2667, chance = 4440}, -- fish
			{id = 2148, count = 17, chance = 59930}, -- gold
		}
	},
	{ name = 'War Wolf', level = 14, file = 'Cirith/Animals/war wolf.xml', look = {type = 3}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Agresywny Wilk', 'Aggressive Wolf'}, experience = 74, health = 140, healthMax = 140,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -1 },
			{ type = 'earth', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13916, chance = 50}, -- wolf amulet
			{id = 7394, chance = 60}, -- wolf trophy
			{id = 5897, chance = 990}, -- wolf paw
			{id = 14452, chance = 10}, -- staff of wind
			{id = 11229, chance = 5030}, -- warwolf fur
			{id = 2671, count = 2, chance = 38580}, -- ham
		}
	},
	{ name = 'Winter Wolf', level = 18, file = 'Cirith/Animals/winter wolf.xml', look = {type = 52}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'�nie�ny Wilk', 'Winter Wolf'}, experience = 260, health = 500, healthMax = 500,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -1 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16458, chance = 80}, -- winter wolf trophy
			{id = 11206, chance = 10080}, -- winter wolf fur
			{id = 14335, chance = 250}, -- wolf helmet
			{id = 11621, chance = 10}, -- sphere frost
			{id = 13834, chance = 2500}, -- wolf heart
			{id = 2666, count = 2, chance = 30150}, -- meat
		}
	},
	{ name = 'Wolf', level = 8, file = 'Cirith/Animals/wolf.xml', look = {type = 27}, classId = 4, killAmount = 250, charmPoints = 5,
		description = {'Wilk', 'Wolf'}, experience = 14, health = 25, healthMax = 25,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2666, count = 2, chance = 41890}, -- meat
			{id = 13916, chance = 50}, -- wolf amulet
			{id = 14335, chance = 10}, -- wolf hood
			{id = 13706, chance = 2090}, -- beastly claw
			{id = 5897, chance = 990}, -- wolf paw
			{id = 14335, chance = 150}, -- wolf helmet
			{id = 13155, chance = 4640}, -- untreated leather
		}
	},
	{ name = 'Yellow Butterfly', level = 1, file = 'Cirith/Animals/yellow butterfly.xml', look = {type = 10}, classId = 4, killAmount = 25, charmPoints = 1,
		description = {'��ty Motyl', 'Yellow Butterfly'}, experience = 0, health = 2, healthMax = 2 },
	
	-- Apes
	{ name = 'Kongra', level = 22, file = 'Cirith/Apes/kongra.xml', look = {type = 116}, classId = 13, killAmount = 500, charmPoints = 15,
		description = {'Kongra', 'Kongra'}, experience = 115, health = 340, healthMax = 340,
		elements = {
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 15 },
			{ type = 'physical', value = 20 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 16400, chance = 1}, -- plushy kongra
			{id = 7618, chance = 540}, -- heatlh potion
			{id = 14362, chance = 160}, -- plate armor
			{id = 15754, chance = 980, subType = 100}, -- brass amulet
			{id = 5883, chance = 980}, -- ape fur
			{id = {13055, 13070, 13058, 13061}, chance = 140}, -- bronze crafting additives
			{id = 2676, count = 12, chance = 30450}, -- banana
			{id = 2148, count = 21, chance = 88020}, -- gold
		}
	},
	{ name = 'Merlkin', level = 24, file = 'Cirith/Apes/merlkin.xml', look = {type = 117}, classId = 13, killAmount = 500, charmPoints = 15,
		description = {'Merlkin', 'Merlkin'}, experience = 195, health = 235, healthMax = 235,
		elements = {
			{ type = 'fire', value = 20 },
			{ type = 'energy', value = 10 },
			{ type = 'holy', value = 10 },
			{ type = 'ice', value = -5 },
			{ type = 'death', value = -5 },
		},
		loot = {
			{id = 3966, chance = 10}, -- banana staff
			{id = 2150, chance = 260}, -- small amethyst
			{id = 7620, chance = 670}, -- mana potion
			{id = 5883, chance = 990}, -- ape fur
			{id = 14459, chance = 970}, -- wand
			{id = 13740, chance = 1770}, --banana sash
			{id = 2675, count = 5, chance = 1040}, -- orange
			{id = 2676, count = 11, chance = 30310}, -- banana
			{id = 2148, count = 22, chance = 82930}, -- gold
		}
	},
	{ name = 'Sibang', level = 20, file = 'Cirith/Apes/sibang.xml', look = {type = 118}, classId = 13, killAmount = 500, charmPoints = 15,
		description = {'Sibang', 'Sibang'}, experience = 135, health = 225, healthMax = 225,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2682, chance = 1000}, -- melon
			{id = 5883, chance = 1000}, -- ape fur
			{id = 14457, chance = 50}, -- spruce staff of destruction
			{id = 2678, count = 3, chance = 1980}, -- coconut
			{id = 13740, chance = 4990}, -- banana sash
			{id = 2675, count = 5, chance = 19870}, -- orange
			{id = 1294, count = 3, chance = 29990}, -- small stone
			{id = 2676, count = 11, chance = 30450}, -- banana
			{id = 2148, count = 18, chance = 88780}, -- gold
		}
	},
	
	-- Arachnids
	{ name = 'Dragonfly', level = 80, file = 'Cirith/Arachnids/dragonfly.xml', look = {type = 602}, classId = 28, killAmount = 2500, charmPoints = 50,
		description = {'Smocza Wa�ka', 'Dragonfly'}, experience = 3900, health = 6800, healthMax = 6800,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 80 },
			{ type = 'death', value = 15 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 2123, chance = 220}, -- diamond ring
			{id = 14433, chance = 350}, -- leaden helmet
			{id = 2156, chance = 290}, -- red gem
			{id = 13707, chance = 650}, -- cuprite
			{id = 16431, chance = 110}, -- black forest bow
			{id = 2160, chance = 90}, -- gold
			{id = 16356, chance = 10}, -- container
			{id = 13002, chance = 110}, -- void boots
			{id = 17941, chance = 1190}, -- radiating powder
			{id = 14504, chance = 440}, -- magical thread
			{id = 2121, chance = 60}, -- wedding ring
			{id = 2656, chance = 310}, -- blue robe
			{id = 2124, chance = 80}, -- crystal ring
			{id = 2165, chance = 910}, -- stealth ring
			{id = 2143, count = 2, chance = 1800}, -- white pearl
			{id = 2158, chance = 120}, -- blue gem
			{id = 2133, chance = 980, subType = 200}, -- ruby necklace
			{id = 2146, count = 2, chance = 2510}, -- small sapphire
			{id = 2149, count = 2, chance = 3910}, -- small emerald
			{id = {7588, 7589}, count = 2, chance = 430}, -- GHP/GMP
			{id = 2152, count = {2, 5}, chance = 7240}, -- silver
			{id = 2148, count = {15, 38}, chance = 95090}, -- gold
		}
	},
	{ name = 'Aggressive Insect', level = 18, file = 'Cirith/Arachnids/aggressive insect.xml', look = {type = 467}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Agresywny Insekt', 'Aggressive Insect'}, experience = 195, health = 345, healthMax = 345,
		boss = 'Waspero', chance = 60,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 14696, chance = 12450}, -- honeycomb
			{id = 17941, chance = 1150}, -- radiating powder
			{id = 13849, chance = 10}, -- leaf helmet
		}
	},
	{ name = 'Ancient Scarab', level = 40, file = 'Cirith/Arachnids/ancient scarab.xml', look = {type = 79}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Antyczny Skarabeusz', 'Ancient Scarab'}, experience = 560, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2440, chance = 60}, -- daramarian waraxe
			{id = 16168, chance = 10}, -- swamplair helmet
			{id = 13990, chance = 130}, -- emerald armor
			{id = 7588, chance = 740}, -- SHP
			-- {id = 2142, chance = 2700, subType = 100}, -- ancient amulet
			-- {id = 2135, chance = 3550, subType = 80}, -- scarab amulet
			{id = 14348, chance = 1670}, -- iron plate
			{id = 10547, chance = 4510}, -- scarab pincers
			{id = 2149, count = 3, chance = 5910}, -- small emerald
			{id = 2150, count = 4, chance = 6040}, -- small amethyst
			{id = 10548, chance = 7320}, -- scarab pincers
			{id = 2159, count = 2, chance = 7980}, -- scarab coin
			{id = 2162, chance = 10330}, -- magic light wand
			{id = 2148, count = 49, chance = 91820}, -- copper coin
		}
	},
	{ name = 'Brimstone Bug', level = 44, file = 'Cirith/Arachnids/brimstone bug.xml', look = {type = 367}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Opancerzony Robak', 'Brimstone Bug'}, experience = 700, health = 1300, healthMax = 1300,
		boss = 'Analarak', chance = 150,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13980, chance = 100, subType = 200}, -- terra amulet
			{id = 5904, chance = 540}, -- magic sulphur
			{id = 2149, count = 4, chance = 2800}, -- small emerald
			{id = 7589, chance = 810}, -- SHP
			{id = 7588, chance = 960}, -- SMP
			{id = 11226, chance = 14960}, -- sulphurous stone
			{id = 11216, chance = 19850}, -- lump of earth
			{id = 10556, chance = 49660}, -- poisonous slime
			{id = 2152, chance = 41690}, -- silver
		}
	},
	{ name = 'Bug', level = 5, file = 'Cirith/Arachnids/bug.xml', look = {type = 45}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'�uk', 'Bug'}, experience = 17, health = 29, healthMax = 29,
		boss = 'Analarak', chance = 50,
		elements = {
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2148, chance = 4980}, -- copper coin
			{id = 2679, chance = 4870}, -- cherry
			{id = 14175, chance = 1010}, -- coal
			{id = 3976, count = 2, chance = 12090}, -- worm
		}
	},
	{ name = 'Crystal Spider', level = 45, file = 'Cirith/Arachnids/crystal spider.xml', look = {type = 263}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Kryszta�owy Paj�k', 'Crystal Spider'}, experience = 700, health = 1250, healthMax = 1250,
		elements = {
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 11955, chance = 170}, -- jewelled backpack
			{id = 2171, chance = 110}, -- platinum amulet
			{id = 13894, chance = 120}, -- frosty rapier
			{id = 14486, chance = 160}, -- glacier hood
			{id = 14010, chance = 170}, -- knight armor
			{id = 7290, chance = 750}, -- shard
			{id = 13299, chance = 250}, -- glass spear
			{id = 14321, chance = 290}, -- royal steel legs
			{id = 16910, chance = 245}, -- a crystal rod of frost
			{id = 2169, chance = 1530}, -- time ring
			{id = 2124, chance = 1580}, -- crystal ring
			{id = 2125, chance = 1660}, -- crystal necklace
			{id = 5879, chance = 1960}, -- GSS
			{id = 15720, chance = 530}, -- broad sword
			{id = 14362, chance = 980}, -- plate armor
			{id = 7441, chance = 4930}, -- ice cube
			{id = 14478, chance = 1160}, -- steel helmet
			{id = 7364, count = 6, chance = 5850}, -- sniper arrows
			{id = 7589, chance = 810}, -- SMP
			{id = 2148, count = 28, chance = 99990}, -- coin
		}
	},
	{ name = 'Dustrunner', level = 30, file = 'Cirith/Arachnids/dustrunner.xml', look = {type = 364}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Dustrunner', 'Dustrunner'}, experience = 720, health = 1350, healthMax = 1350,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -5 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 16169, chance = 50}, -- swamplair bow
			{id = 5904, chance = 120}, -- magic sulphur
			{id = 13992, chance = 10}, -- bronze plate
			{id = 14320, chance = 10}, -- royal steel boots
			{id = 15721, chance = 50}, -- ragged blade
			{id = {13055, 13070, 13058, 13061}, chance = 180}, -- bronze crafting additives
			{id = 15791, chance = 960, subType = 170}, -- bursztynowe oko
			{id = 7588, chance = 1040}, -- SHP
			{id = 15716, chance = 650}, -- knight axe
			{id = 14327, chance = 30}, -- plate legs
			{id = 2149, count = 2, chance = 3280}, -- small emerald
			{id = 15713, chance = 840}, -- longsword
			{id = 14000, chance = 920}, -- wooden armor
			{id = 2671, count = 3, chance = 12160}, -- ham
			{id = 2148, count = 21, chance = 86030}, -- gold
		}
	},
	{ name = 'Giant Spider', level = 44, file = 'Cirith/Arachnids/giant spider.xml', look = {type = 38}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Olbrzymi Paj�k', 'Giant Spider'}, experience = 760, health = 1300, healthMax = 1300,
		boss = 'Nemen\'akh', chance = 80,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 2171, chance = 300}, -- amulet
			{id = 14321, chance = 250}, -- royal steel legs
			{id = 14010, chance = 140}, -- knight armor
			{id = 13924, chance = 100}, -- light crossbow
			{id = 14327, chance = 310}, -- plate legs
			{id = 14362, chance = 290}, -- plate armor
			{id = 17726, chance = 10}, -- plague spear
			{id = 5879, chance = 2090}, -- gss
			{id = 7588, chance = 760}, -- SHP
			{id = 5886, chance = 200}, -- spool of yarn
			{id = 14478, chance = 810}, -- steel helmet
			{id = 15609, chance = 150}, -- massive ornamented bow
			{id = 15713, chance = 5250}, -- longsword
			{id = 8859, chance = 6970}, -- spider fang
			{id = 15752, chance = 8330}, -- brass legs
			{id = 2545, count = 12, chance = 12160}, -- poison arrows
			{id = 2152, count = 1, chance = 38610}, -- silver
			{id = 2148, count = {22, 49}, chance = 99900}, -- coin
		}
	},
	{ name = 'Lancer Beetle', level = 30, file = 'Cirith/Arachnids/lancer beetle.xml', look = {type = 348}, classId = 28, killAmount = 1000, charmPoints = 25,
		description = {'Stepowy Robak', 'Lancer Beetle'}, experience = 215, health = 400, healthMax = 400,
		boss = 'Analarak', chance = 100,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'death', value = 50 },
			{ type = 'earth', value = 100 },
		},
		loot = {
			{id = 2150, count = {2, 4}, chance = 270}, -- small amethyst
			{id = 11368, chance = 880}, --  beetle necklace
			{id = 5892, chance = 530}, -- steel
			{id = 10608, chance = 3950}, -- lump of dirt
			{id = 10556, chance = 8150}, -- poisonous slime
			{id = 11366, chance = 14720}, -- lancer beetle shell
			{id = 2148, count = 11, chance = 82760}, -- coin
		}
	},
	{ name = 'Larva', level = 12, file = 'Cirith/Arachnids/larva.xml', look = {type = 82}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Larwa', 'Larva'}, experience = 40, health = 70, healthMax = 70,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2666, chance = 14930}, -- meat
			{id = 2148, count = 2, chance = 64730}, -- copper coin
		}
	},
	{ name = 'Poison Spider', level = 10, file = 'Cirith/Arachnids/poison spider.xml', look = {type = 36}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Toksyczny Paj�k', 'Poison Spider'}, experience = 20, health = 36, healthMax = 36,
		boss = 'Arachnid', chance = 70,
		elements = {
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 5886, chance = 10}, -- spool of yarn
			{id = 7887, chance = 300}, -- toxic amulet
			{id = 14175, chance = 1990}, -- coal
			{id = 13747, chance = 11650}, -- spider shell
			{id = 8859, chance = 17160}, -- spider fang
		}
	},
	{ name = 'Sandcrawler', level = 12, file = 'Cirith/Arachnids/sandcrawler.xml', look = {type = 350}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Paj�czy �uk', 'Sandcrawler'}, experience = 85, health = 165, healthMax = 165,
		boss = 'Arachnid', chance = 60,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'energy', value = 10 },
			{ type = 'fire', value = -18 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 15715, chance = 180}, -- Cranial basher
			{id = 2149, chance = 370}, -- small emerald
			{id = 2150, chance = 520}, -- small amethyst
			{id = 17941, chance = 950}, -- radiating powder
			{id = 11367, chance = 6500}, -- sandcrawler shell
			{id = 2789, count = 2, chance = 40080}, -- brown mushroom
			{id = 2148, count = 9, chance = 86920}, -- copper coin
		}
	},
	{ name = 'Sandstone Scorpion', level = 34, file = 'Cirith/Arachnids/sandstone scorpion.xml', look = {type = 425}, classId = 28, killAmount = 500, charmPoints = 15,
		description = {'Pustynny Scorpion', 'Sandstone Scorpion'}, experience = 1410, health = 2700, healthMax = 2700,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -5 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 2154, chance = 320}, -- yellow gem
			{id = 2151, chance = 190}, -- talon
			{id = 2446, chance = 20}, -- pharaoh sword
			{id = 14446, chance = 290}, -- seeing eye
			{id = 6534, chance = 280}, -- flying trident
			{id = 1999, chance = 50}, -- golden backpack
			{id = 7887, chance = 10}, -- toxic amulet
			{id = 2169, chance = 300}, -- time ring
			{id = 2164, chance = 510, subType = 16}, -- might ring
			{id = 14348, chance = 600}, -- iron plate
			{id = 14324, chance = 420}, -- steel shield
			{id = 2547, count = 7, chance = 5000}, -- power bolt
			{id = 13707, chance = 650}, -- cuprite
			{id = 5904, chance = 380}, -- magic sulphur
			{id = 2671, count = 3, chance = 14310}, -- ham
			{id = 2149, count = 2, chance = 3215}, -- small emerald
			{id = 2148, count = {12, 40}, chance = 99320}, -- gold
		}
	},
	{ name = 'Scarab', level = 24, file = 'Cirith/Arachnids/scarab.xml', look = {type = 83}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Skarabeusz', 'Scarab'}, experience = 165, health = 320, healthMax = 320,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -18 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 14012, chance = 10}, -- crimson sword
			{id = 2149, chance = 350}, -- small emerald
			{id = 2150, chance = 510}, -- small amethyst
			{id = 2159, chance = 1100}, -- scarab coin
			{id = 10557, chance = 5020}, -- piece of scarab shell
			{id = 2666, count = 2, chance = 39970}, -- meat
			{id = 2148, count = 12, chance = 87270}, -- copper coin
		}
	},
	{ name = 'Scorpion', level = 6, file = 'Cirith/Arachnids/scorpion.xml', look = {type = 43}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Skorpion', 'Scorpion'}, experience = 45, health = 65, healthMax = 65,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 10567, chance = 5150}, -- scorpion tail
		}
	},
	{ name = 'Swarmer', level = 15, file = 'Cirith/Arachnids/swarmer.xml', look = {type = 566}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Szkodnik', 'Swarmer'}, experience = 120, health = 215, healthMax = 215,
		boss = 'Tarantulos', chance = 40,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'energy', value = 75 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -3 },
		},
		loot = {
			{id = 7435, chance = 10}, -- impaler
			{id = 2149, chance = 970}, -- small emerald
			{id = 2143, chance = 1210}, -- white pearl
			{id = 17247, chance = 11980}, -- compound eye
			{id = 17246, chance = 15370}, -- swarmer antena
			{id = 13706, count = 2, chance = 1900}, -- beastly claw
			{id = 2148, count = {11, 20}, chance = 62500}, -- copper coin
		}
	},
	{ name = 'Spider', level = 4, file = 'Cirith/Arachnids/spider.xml', look = {type = 30}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Paj�k', 'Spider'}, experience = 15, health = 30, healthMax = 30,
		boss = 'Arachnid', chance = 50,
		elements = {
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 9006, chance = 1}, -- spider doll
			{id = 5886, chance = 10}, -- spool of yarn
			{id = 7887, chance = 150}, -- toxic amulet
			{id = 14175, chance = 980}, -- coal
			{id = 8859, chance = 17600}, -- spider fang
		}
	},
	{ name = 'Tarantula', level = 20, file = 'Cirith/Arachnids/tarantula.xml', look = {type = 219}, classId = 28, killAmount = 250, charmPoints = 5,
		description = {'Tarantula', 'Tarantula'}, experience = 135, health = 225, healthMax = 225,
		boss = 'Tarantulos', chance = 90,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 5886, chance = 80}, -- spool of yarn
			{id = 7887, chance = 220}, -- toxic amulet
			{id = 2169, chance = 1200}, -- time ring
			{id = 17941, chance = 1090}, -- radiating powder
			{id = 14015, chance = 200}, -- short bow
			{id = 8859, chance = 4870}, -- spider fang
			{id = 11192, chance = 9970}, -- tarantula egg
			{id = 2148, count = {13, 20}, chance = 6480}, -- coin
		}
	},
	{ name = 'Wailing Widow', level = 38, file = 'Cirith/Arachnids/wailing widow.xml', look = {type = 347}, classId = 28, killAmount = 500, charmPoints = 15,
		description = {'Paj�cza Wdowa', 'Wailing Widow'}, experience = 450, health = 850, healthMax = 850,
		boss = 'Tarantulos', chance = 100,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 5886, chance = 10}, -- spool of yarn
			{id = 11323, chance = 1800}, -- wailing widow's necklace
			{id = 15761, chance = 10}, -- snake blade
			{id = 14317, chance = 1050}, -- enchanced oak shield
			{id = 2796, chance = 3170}, -- green mushroom
			{id = 7435, chance = 700}, -- impaler
			{id = 7887, chance = 300}, -- toxic amulet
			{id = 7618, chance = 780}, -- health potion
			{id = 7620, chance = 1110}, -- mana potion
			{id = 11322, chance = 20170}, -- widow mandibles
			{id = 2148, count = 24, chance = 97130}, -- coin
		}
	},
	
	-- Bonelords
	{ name = 'Seer', level = 80, file = 'Cirith/Bonelords/Seer.xml', look = {type = 198, head = 21, body = 68, legs = 76, feet = 67}, classId = 15, killAmount = 2500, charmPoints = 50,
		description = {'Widz�cy', 'Seer'}, experience = 2200, health = 4250, healthMax = 4250,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'bleed', value = -5 },
			{ type = 'holy', value = 15 },
			{ type = 'death', value = 35 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'ice', value = 10 },
			{ type = 'fire', value = 20 },
		},
		loot = {
			{id = 13795, chance = 40}, -- bonelord hood
			{id = 8900, chance = 910}, -- spellbook
			{id = 13902, chance = 170}, -- hero legs
			{id = 13993, chance = 100}, -- shadow armor
			{id = 14347, chance = 80}, -- seer amulet
			{id = 16269, chance = 90}, -- crystal spike backpack
			{id = 14504, chance = 3990}, -- magical thread
			{id = 13741, chance = 7910}, -- small flask of eyedrops
			{id = 11435, count = {5, 10}, chance = 4400}, -- crystalline arrow
			{id = 14155, chance = 4230}, -- lettuce
			{id = 16398, chance = 990}, -- crystal of teleport
			{id = 16932, chance = 1640}, -- shp
			{id = 16933, chance = 2000}, -- smp
			{id = 13001, chance = 130}, -- void legs
			{id = 2146, count = 4, chance = 9920}, -- small sapphire
			{id = 11436, count = {3, 9}, chance = 3270}, -- crystalline bolt
			{id = 2169, chance = 4100}, -- time ring
			{id = 11191, chance = 2140}, -- giant eye
			{id = 2177, chance = 1520}, -- life crystal
			{id = 2152, count = 3, chance = 49950}, -- silver
			{id = 2148, count = {10, 18}, chance = 96240}, -- copper coin
		}
	},
	{ name = 'Young Bonelord', level = 18, file = 'Cirith/Bonelords/young bonelord.xml', look = {type = 109}, classId = 15, killAmount = 250, charmPoints = 5,
		description = {'M�odszy W�adca Ko�ci', 'Young Bonelord'}, experience = 70, health = 120, healthMax = 120,
		boss = 'Puppet Master', chance = 30,
		elements = {
			{ type = 'energy', value = 11 },
			{ type = 'ice', value = 20 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 16198, chance = 10}, -- druid hood
			{id = 14016, chance = 50}, -- holy shield
			{id = 5809, chance = 1060}, -- magical element
			{id = 13741, chance = 3380}, -- small flask of eyedrops
			{id = 2148, count = {3, 9}, chance = 99320}, -- gold
		}
	},
	{ name = 'Bonelord', level = 22, file = 'Cirith/Bonelords/bonelord.xml', look = {type = 17}, classId = 15, killAmount = 500, charmPoints = 15,
		description = {'W�adca Ko�ci', 'Bonelord'}, experience = 150, health = 260, healthMax = 260,
		boss = 'Puppet Master', chance = 80,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13795, chance = 210}, -- bonelord hood
			{id = 7620, chance = 280}, -- mana potion
			{id = 13910, chance = 570}, -- wradon's staff
			{id = 5809, chance = 2890}, -- magical element
			{id = {13906, 13907, 13908, 13909, 13911}, chance = 320}, -- Wradon's set
			{id = 2155, chance = 90}, -- green gem
			{id = 2143, count = 2, chance = 1990}, -- white pearl
			{id = 2376, chance = 450}, -- runed blade
			{id = 16148, chance = 50}, -- a dirty eye of despair
			{id = 13741, chance = 4970}, -- small flask of eyedrops
			{id = 5898, chance = 960}, -- beholder eye
			{id = 2149, count = 2, chance = 2760}, -- small emerald
			{id = 2148, count = {14, 23}, chance = 99370}, -- gold
		}
	},
	{ name = 'Elder Bonelord', level = 45, file = 'Cirith/Bonelords/elder bonelord.xml', look = {type = 108}, classId = 15, killAmount = 1000, charmPoints = 25,
		description = {'Starszy Wladca Kosci', 'Elder Bonelord'}, experience = 280, health = 500, healthMax = 500,
		boss = 'Puppet Master', chance = 120,
		elements = {
			{ type = 'death', value = 30 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 30 },
		},
		loot = {
			{id = 13795, chance = 20}, -- bonelord hood
			{id = 13980, chance = 100, subType = 300}, -- amulet
			{id = 15755, chance = 10}, -- snake helmet
			{id = 2157, chance = 2140}, -- gold nugget
			{id = 11191, chance = 820}, -- giant eye
			{id = 7589, chance = 920}, -- SMP
			{id = 2143, chance = 5170}, -- white pearl
			{id = 5898, chance = 1870}, -- beholder eye
			{id = 16148, chance = 50}, -- a dirty eye of despair
			{id = 15784, chance = 2350}, -- copper shield
			{id = 15723, chance = 2750}, -- two handed sword
			{id = 2149, count = 2, chance = 4130}, -- small emerald
			{id = 7364, count = 4, chance = 8790}, -- sniper arrow
			{id = 13741, chance = 10230}, -- small flask of eyedrops
			{id = 11187, chance = 21050}, -- elder beholder tentacle
			{id = 2148, count = {18, 31}, chance = 99460}, -- gold
		}
	},
	{ name = 'Arch Bonelord', level = 65, file = 'Cirith/Bonelords/braindeath.xml', look = {type = 256}, classId = 15, killAmount = 1000, charmPoints = 25,
		description = {'Arcy W�adca Ko�ci', 'Arch Bonelord'}, experience = 685, health = 1225, healthMax = 1225,
		boss = 'Puppet Master', chance = 200,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 15732, chance = 380}, -- knight axe
			{id = 15757, chance = 310}, -- snake legs
			{id = 13795, chance = 290}, -- bonelord hood
			{id = 2155, chance = 110}, -- green gem
			{id = 8900, chance = 800}, -- spellbook
			{id = 16148, chance = 175}, -- a dirty eye of despair
			{id = 13896, chance = 690}, -- giant sword
			{id = 15715, chance = 2170}, -- mace
			{id = 2144, count = 2, chance = 7150}, -- black pearl
			{id = 5898, chance = 3330}, -- beholder eye
			{id = 10579, chance = 4590}, -- piece of dead brain
			{id = 15784, chance = 5950}, -- copper shield
			{id = 7364, count = 4, chance = 8880}, -- sniper arrow
			{id = 15739, chance = 275}, -- necrotic bow
			{id = 15779, chance = 15420}, -- skuller
			{id = 2148, count = 24, chance = 99650}, -- gold
		}
	},
	
	--  Bosses 
	--  Proclay Task
	{ name = 'Steno', file = 'Cirith/Bosses/Proclay Tasks/Steno.xml', look = {type = 634, addons = 7}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Steno', 'Steno'}, experience = 38000, health = 19000, healthMax = 19000,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'ice', value = 40 },
		},
		loot = {
			{id = 13980, chance = 49090, subType = 200}, -- terra amulet
			{id = 8857, chance = 500, uniqueDrop = 1}, -- silkweaver bow
			{id = 16196, chance = 1000, uniqueDrop = 1}, -- druid legs
			{id = 15756, chance = 38860}, -- snake armor
			{id = 15749, chance = 1020, uniqueDrop = 1}, -- dragon executor
			{id = 7589, chance = 56480}, -- SMP
			{id = 2214, chance = 41170}, -- RoH
			{id = 15757, chance = 14410}, -- snake legs
			{id = 10219, chance = 27820, subType = 5}, -- sacred tree amulet
			{id = 11193, chance = 90220}, -- hydra head
			{id = 2195, chance = 12000, uniqueDrop = 1}, -- boots of haste
			{id = 8473, count = 5, chance = 69360}, -- UHP
			{id = 7590, count = 5, chance = 70160}, -- GMP
			{id = 2146, count = {4, 10}, chance = 94920}, -- small sapphire
			{id = 2536, chance = 9000, uniqueDrop = 1}, -- medusa shield
			{id = 2666, count = {4, 6}, chance = 70000}, -- meat
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2152, count = {4, 10}, chance = 89950}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Last Breath', level = 80, file = 'Cirith/Bosses/Proclay Tasks/Last Breath.xml', look = {type = 366}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Last Breath', 'Last Breath'}, experience = 64000, health = 32000, healthMax = 32000,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'fire', value = 15 },
			{ type = 'death', value = 75 },
			{ type = 'energy', value = 10 },
			{ type = 'ice', value = -5 },
			{ type = 'toxic', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'earth', value = 70 },
		},
		loot = {
			{id = 2195, chance = 6100, uniqueDrop = 1}, -- boots of haste
			{id = 15730, chance = 5100}, --  war axe
			{id = 2197, chance = 13050, subType = 10}, -- ssa
			{id = 16140, chance = 1000, uniqueDrop = 1}, -- golden axe
			{id = 17161, chance = 980, uniqueDrop = 1}, -- death enchanted in the staff
			{id = 14482, chance = 10000}, -- ancient helmet
			{id = 13990, chance = 2790, uniqueDrop = 1}, -- emerald armor
			{id = 14452, chance = 4250}, -- staff of wind
			{id = 16206, chance = 510}, -- meteorite legs
		    {id = 13000, chance = 510}, -- void cape
            {id = 13001, chance = 510}, -- void legs
			{id = 9971, chance = 9060}, -- gold ingot
			{id = 6300, chance = 25180}, -- death ring
			{id = 15733, chance = 3130, uniqueDrop = 1}, --devastator
			{id = 13796, chance = 5880}, -- silver shield
			{id = 9810, chance = 99390}, -- rusty armor(rare)
			{id = 14433, chance = 2970}, -- leaden helmet
			{id = 6500, chance = 98650}, -- demonic essence
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2152, count = {4, 10}, chance = 89950}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Harbinger', file = 'Cirith/Bosses/Proclay Tasks/Harbinger.xml', look = {type = 604}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Harbinger', 'Harbinger'}, experience = 50000, health = 25000, healthMax = 25000,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'physical', value = 15 },
			{ type = 'energy', value = 30 },
			{ type = 'ice', value = 35 },
			{ type = 'earth', value = 20 },
			{ type = 'death', value = 100 },
		},
		loot = {
			{id = 13953, chance = 1000, uniqueDrop = 1}, -- the fallen horse
			{id = {16208, 16205}, chance = 910, uniqueDrop = 1}, -- meteorite helmet
			{id = 6300, chance = 21140}, -- death ring
			{id = 2125, chance = 9520}, -- crystal necklace
			{id = 2178, chance = 10540}, -- mind stone
			{id = 16162, chance = 1590, uniqueDrop = 1}, -- black steel hammer
			{id = 16161, chance = 4100}, -- black steel sword
			{id = 14320, chance = 9180, uniqueDrop = 1}, -- plate boots
			{id = 7591, chance = 91080}, -- GHP
			{id = 14362, chance = 22920}, -- plate armor
			{id = 15607, chance = 7200, uniqueDrop = 1}, -- skullcrusher bow
			{id = 2553, chance = 56250, uniqueDrop = 1}, -- pick
			{id = 5944, chance = 27030}, -- soul orb
			{id = 2150, count = 2, chance = 97560}, -- small amethyst
			{id = 14001, chance = 32280}, -- dark armor
			{id = 2546, count = 12, chance = 81350}, -- burst arrow
			{id = 6500, chance = 71830}, -- demonic essence
			{id = 2666, count = 6, chance = 84530}, -- meat
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2152, count = {4, 10}, chance = 89950}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Electro', file = 'Cirith/Bosses/Proclay Tasks/Electro.xml', look = {type = 290}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Electro', 'Electro'}, experience = 36000, health = 18000, healthMax = 18000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 50 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 16207, chance = 910, uniqueDrop = 1}, -- meteorite armor
			{id = 13888, chance = 9220}, -- lightning axe
			{id = 13794, chance = 1250, uniqueDrop = 1}, -- titan helmet
			{id = 14009, chance = 910, uniqueDrop = 1}, -- sparkling robe
			{id = 14005, chance = 5520}, -- wizard's robe
			{id = 2542, chance = 1530, uniqueDrop = 1}, -- lightning shield
			{id = 10221, chance = 8700, subType = 10}, -- shockwave amulet
			{id = 16162, chance = 7760}, -- hammer of black steel
			{id = 8901, chance = 25960}, -- SB of warding
			{id = 8920, chance = 31150}, -- wand of starstorm
			{id = 7889, chance = 21200}, -- lightning pendant
			{id = 2158, chance = 24130}, -- blue gem
			{id = 2168, chance = 82300}, -- life ring
			{id = 14317, chance = 63300}, -- enchanced oak shield
			{id = 2189, chance = 84150}, -- seeing eye
			{id = 2150, count = 10, chance = 98500}, -- small amethyst
			{id = 7590, count = {3, 6}, chance = 99000}, -- GMP
			{id = 7838, count = 13, chance = 92000}, -- flash arrow
			{id = 7589, count = {3, 6}, chance = 98000}, -- SMP
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2152, count = {4, 10}, chance = 89950}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Hellfire Warrior', file = 'Cirith/Bosses/Proclay Tasks/Hellfire Warrior.xml', look = {type = 515}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Hellfire Warrior', 'Hellfire Warrior'}, experience = 40000, health = 20000, healthMax = 20000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 15 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2539, chance = 1000, uniqueDrop = 1}, -- phoenix shield
			{id = 13897, chance = 10, uniqueDrop = 1}, -- fire hammer
			{id = 16141, chance = 350, uniqueDrop = 1}, -- golden bow
			{id = 13994, chance = 2310, uniqueDrop = 1}, -- magma armor
			{id = 13939, chance = 450}, -- hellfire crossbow
			{id = 13995, chance = 4000, uniqueDrop = 1}, -- lavos armor
			{id = 13796, chance = 1500}, -- silver shield
			{id = {10519, 10522}, chance = 6250}, -- orange/crown backpack
			{id = 16204, chance = 580, uniqueDrop = 1}, -- hellfire shield
			{id = 16201, chance = 2000, uniqueDrop = 1}, -- hellfire legs
			{id = 2187, chance = 90900}, -- woi
			{id = 10218, chance = 19000, subType = 10}, -- hellfire lavos amulet
			{id = 16200, chance = 7690}, -- hellfire boots
			{id = 13588, chance = 500, uniqueDrop = 1}, -- fire axe
			{id = 2136, chance = 5800, uniqueDrop = 1}, -- Demonbone Amulet
			{id = 2147, count = {4, 10}, chance = 61450}, -- small ruby
			{id = 2144, count = {2, 4}, chance = 63450}, -- black pearl
			{id = 2156, chance = 25490}, -- red gem
			{id = 6500, chance = 94990}, -- demonic essence
			{id = 10552, chance = 99930}, -- fierly heart
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2152, count = {4, 10}, chance = 89950}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Giant Nepethes', file = 'Cirith/Bosses/Proclay Tasks/Giant Nepethes.xml', look = {type = 0, auxType = 3885, typeEx = 4149}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Giant Nepethes', 'Giant Nepethes'}, experience = 18000, health = 9000, healthMax = 9000,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
			{ type = 'energy', value = 25 },
			{ type = 'physical', value = 5 },
		},
		loot = {
			{id = 13849, chance = 1000, uniqueDrop = 1}, -- leaf helmet
			{id = 13848, chance = 2000, uniqueDrop = 1}, -- leaf armor
			{id = 13847, chance = 1500, uniqueDrop = 1}, -- leaf legs
			{id = 13800, chance = 24200}, -- nepethes seeds
			{id = 7732, chance = 91050}, -- seeds
			{id = 2155, chance = 99190}, -- green gem
			{id = 5803, chance = 1900, uniqueDrop = 1}, -- arbalest
			{id = 14453, chance = 43990}, -- poison rod
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 11225, chance = 95200}, -- nettle blossom
			{id = 7590, chance = 96670}, -- GMP
			{id = 2800, chance = 87100}, -- star herb
			{id = 7850, count = 21, chance = 98990}, -- earth arrow
			{id = 2149, count = 5, chance = 97290}, -- small emerald
			{id = 2803, chance = 88190}, -- powder herb
			{id = 2804, chance = 65800}, -- shadow herb
			{id = 2799, chance = 29880}, -- stone herb
			{id = 2802, count = 2, chance = 87500}, -- sling herb
		}
	},
	{ name = 'The Chosen One', file = 'Cirith/Bosses/Proclay Tasks/The Chosen One.xml', look = {type = 338}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'The Chosen One', 'The Chosen One'}, experience = 16000, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 11330, chance = 1010, uniqueDrop = 1}, -- lizard trophy
			{id = 16157, chance = 3020, uniqueDrop = 1}, -- black steel armor
			{id = 13949, chance = 1500, uniqueDrop = 1}, -- golden spear
			{id = 16155, chance = 9120, uniqueDrop = 1}, -- black steel shoes
			{id = 15729, chance = 5220}, -- double axe
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 13151, chance = 9320}, -- lizard tail
			{id = 5876, chance = 9970}, -- lizard leather
			{id = 5881, chance = 9970}, -- lizard scale
			{id = 16356, chance = 1000, uniqueDrop = 1}, -- container
			{id = 2145, count = 2, chance = 51980}, -- small diamond
			{id = 13948, chance = 25290}, -- hunting spear
			{id = 15761, chance = 9220}, -- snake blade
			{id = 11328, chance = 71920}, -- legionnaire flags
			{id = 11239, chance = 41930}, -- bunch of ripe rice
			{id = 7588, chance = 73890}, -- SHP
			{id = 11329, chance = 94880}, -- broken halberd
			{id = 2152, count = {3, 9}, chance = 89950}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Legion', file = 'Cirith/Bosses/Proclay Tasks/Legion.xml', look = {type = 530}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Legion', 'Legion'}, experience = 7600, health = 3800, healthMax = 3800,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 40 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 65 },
		},
		loot = {
			{id = 16356, chance = 2000, uniqueDrop = 1}, -- container
			{id = 10576, chance = 95080}, -- mystical hourglass
			{id = 2143, count = 3, chance = 81480}, -- white pearl
			{id = 2144, count = 3, chance = 81480}, -- black pearl
			{id = {13945, 13946, 13944}, chance = 1500, uniqueDrop = 1}, -- ashbringer set
			{id = 13993, chance = 200, uniqueDrop = 1}, -- shadow armor
			{id = 6300, chance = 9240}, -- death ring
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2521, chance = 21130}, -- dark shield
			{id = 2162, chance = 84970}, -- magic lightwand
			{id = 16909, chance = 1030, uniqueDrop = 1}, -- sickle of the wind
			{id = 6500, chance = 61680}, -- demonic essence
			{id = 2537, chance = 5150, uniqueDrop = 1}, -- haunted helmet
			{id = 15739, chance = 560, uniqueDrop = 1}, -- necrotic bow
			{id = 2152, count = {2, 5}, chance = 89950}, -- silver
			{id = 2148, count = {40, 65}, chance = 98830}, -- gold coin
		}
	},
	{ name = 'Spiderus', file = 'Cirith/Bosses/Proclay Tasks/Spiderus.xml', look = {type = 657}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Spiderus', 'Spiderus'}, experience = 10000, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 16356, chance = 1000, uniqueDrop = 1}, -- container
			{id = 16363, chance = 2000, uniqueDrop = 1}, -- swamplair backpack
			{id = 13794, chance = 200, uniqueDrop = 1}, -- titan helmet
			{id = 13990, chance = 2130}, -- emerald armor
			{id = 14321, chance = 3150}, -- royal steel legs
			{id = 14010, chance = 8140}, -- knight armor
			{id = 5879, chance = 92090}, -- gss
			{id = 13924, chance = 3910}, -- light crossbow
			{id = 14327, chance = 9140}, -- plate legs
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 14362, chance = 89190}, -- plate armor
			{id = 5886, chance = 4200}, -- spool of yarn
			{id = 15609, chance = 9150, uniqueDrop = 1}, -- massive ornamented bow
			{id = 8859, chance = 96970}, -- spider fang
			{id = 2545, count = {8, 19}, chance = 92160}, -- poison arrows
			{id = 2152, count = {2, 8}, chance = 89950}, -- silver
			{id = 2148, count = {22, 49}, chance = 99900}, -- coin
		}
	},
	{ name = 'Shlamer', file = 'Cirith/Bosses/Proclay Tasks/Shlamer.xml', look = {type = 238}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Shlamer', 'Shlamer'}, experience = 5000, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 15741, chance = 1000, uniqueDrop = 1}, -- reflective bow
			{id = 16166, chance = 1300, uniqueDrop = 1}, -- swamplair legs
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2155, chance = 6520}, -- green gem
			{id = 2154, chance = 7630}, -- yellow gem
			{id = 2631, chance = 2280}, -- knight legs
			{id = 15756, chance = 3300}, -- snake armor
			{id = 2145, count = {2, 3}, chance = 72510}, -- small diamond
			{id = 2147, count = {2, 3}, chance = 83640}, -- small ruby
			{id = 2151, chance = 44860}, -- talon
			{id = 2149, count = {2, 3}, chance = 95480}, -- small emerald
			{id = 9967, chance = 92600}, -- glob of acid slime
			{id = 2152, count = {2, 4}, chance = 89950}, -- silver
			{id = 2148, count = {35, 55}, chance = 100000}, -- gold
		}
	},
	{ name = 'Puppet Master', file = 'Cirith/Bosses/Proclay Tasks/Puppet Master.xml', look = {type = 17}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Puppet Master', 'Puppet Master'}, experience = 3800, health = 1900, healthMax = 1900,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 16168, chance = 500, uniqueDrop = 1}, -- swamplair helmet
			{id = {15760, 16169}, chance = 2080, uniqueDrop = 1}, -- snake/swamplair bow
			{id = 15715, chance = 21700}, -- cranial basher
			{id = 2155, chance = 7100}, -- green gem
			{id = 18076, chance = 1000}, -- grey dye
			{id = 17866, chance = 800, uniqueDrop = 1}, -- Mantreads
			{id = 15757, chance = 1200, uniqueDrop = 1}, -- snake legs
			{id = 15755, chance = 900}, -- snake helmet
			{id = 8902, chance = 6000, uniqueDrop = 1}, -- spellbook
			{id = {13056, 13071, 13059, 13062}, chance = 6000}, -- silver crafting additives
			{id = 2144, count = 2, chance = 31500}, -- black pearl
			{id = 5898, chance = 63300}, -- beholder eye
			{id = 13795, chance = 800, uniqueDrop = 1}, -- bonelord hood
			{id = 15779, chance = 95420}, -- skuller
			{id = 2152, count = {3, 5}, chance = 89950}, -- silver
			{id = 2148, count = {29, 45}, chance = 99650}, -- gold
		}
	},
	{ name = 'All A Din', file = 'Cirith/Bosses/Proclay Tasks/All a Din.xml', look = {type = 660, addons = 2}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'All A Din', 'All A Din'}, experience = 7000, health = 3500, healthMax = 3500,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -15 },
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = 50 },
			{ type = 'ice', value = -10 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 14346, chance = 90, uniqueDrop = 1}, -- djinn amulet
			{id = 8931, chance = 1790}, -- fire blade
			{id = 11810, chance = 900, uniqueDrop = 1}, -- fiery bow
			{id = 2647, chance = 3500, uniqueDrop = 1}, -- red robe
			{id = 13796, chance = 2500}, -- silver shield
			{id = 11436, count = {15, 45}, chance = 90000}, -- crystalline bolt
			{id = 13924, chance = 12510}, -- light crossbow
			{id = 13952, chance = 5000}, -- crystal spear
			{id = 14432, chance = 3630, uniqueDrop = 1}, -- cobalt helmet
			{id = 2165, chance = 76010}, -- stealth ring
			{id = 2138, chance = 37700, subType = 300}, -- sapphire amulet
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 5911, chance = 72730}, -- red piece of cloth
			{id = 1869, chance = 73030}, -- red tapestry
			{id = 2147, count = {2, 4}, chance = 79840}, -- small ruby
			{id = 2165, chance = 36280}, -- stealth ring
			{id = 2146, count = {2, 4}, chance = 86130}, -- small sapphire
			{id = 7620, count = {2, 5}, chance = 88880}, -- light mana potion
			{id = 2152, count = {3, 6}, chance = 89950}, -- silver
			{id = 2148, count = {30, 50}, chance = 99940}, -- gold
		}
	},
	{ name = 'Snaker', file = 'Cirith/Bosses/Proclay Tasks/Snaker.xml', look = {type = 28}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Snaker', 'Snaker'}, experience = 1500, health = 300, healthMax = 300,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -10 },
		},
	},
	{ name = 'Aqualius', file = 'Cirith/Bosses/Proclay Tasks/Aqualius.xml', look = {type = 699}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Aqualius', 'Aqualius'}, experience = 30000, health = 15000, healthMax = 15000,
		elements = {
			{ type = 'earth', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'death', value = 50 },
			{ type = 'ice', value = 100 },
			{ type = 'fire', value = 80 },
		},
		loot = {
			{id = 14504, chance = 4590}, -- magical thread
			{id = 11800, chance = 250, uniqueDrop = 1}, -- frozen heart backpack
			{id = 15748, chance = 2140, uniqueDrop = 1}, -- deepling crossbow
			{id = 14486, chance = 5240}, -- glacier hood
			{id = 16398, count = 2, chance = 25000}, -- crystal
			{id = 13164, chance = 1990}, -- royal steel blade
			{id = 16910, chance = 1000, uniqueDrop = 1}, -- a crystal rod of frost
			{id = 10529, chance = 500, uniqueDrop = 1}, -- sea serpent doll
			{id = 14432, chance = 3200, uniqueDrop = 1}, -- cobalt helmet
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 13299, chance = 21890}, -- glass spear
			{id = 2226, chance = 72190}, -- trash
			{id = 13894, chance = 18600}, -- frosty rapier
			{id = 2168, chance = 92480}, -- life ring
			{id = 2146, count = 5, chance = 86130}, -- small sapphire
			{id = 7839, count = {5, 15}, chance = 92400}, -- shiver arrow
			{id = 2152, count = 5, chance = 75100}, -- silver
			{id = 2148, count = {15, 25}, chance = 95100}, -- gold
			{id = 5895, chance = 99510}, -- fish fin
		}
	},
	{ name = 'Animalus', file = 'Cirith/Bosses/Proclay Tasks/Animalus.xml', look = {type = 384}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Animalus', 'Animalus'}, experience = 2400, health = 1200, healthMax = 1200,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16195, chance = 500, uniqueDrop = 1}, -- druid boots
			{id = 14004, chance = 5070, uniqueDrop = 1}, -- trapper boots
			{id = 2628, chance = 24500}, -- The Armor of Hardened Leather
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 13914, chance = 8900}, -- boar loincloth
			{id = 13917, chance = 71500}, -- boar hooves
			{id = 13915, chance = 91000}, -- boar tusks
			{id = 14338, count = 2, chance = 99790}, -- Haunch of Boar
		}
	},
	{ name = 'Oxoroo', file = 'Cirith/Bosses/Oxoroo.xml', look = {type = 622}, classId = 11, killAmount = 15, charmPoints = 50,
		description = {'Oxoroo', 'Oxoroo'}, experience = 12500, health = 25000, healthMax = 25000,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -5 },
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = 5 },
		},
		loot = {
			{id = 10048, chance = 500, uniqueDrop = 1}, -- bucket hat
			{id = 11402, chance = 7500}, -- massive wooden bow
			{id = 13585, chance = 2150}, -- foul rod
			{id = 13707, count = {2, 3}, chance = 72730}, -- cuprite
			{id = 14326, chance = 9000, uniqueDrop = 1}, -- steel boots
			{id = 7887, chance = 12200}, -- toxic amulet
			{id = 7618, count = 5, chance = 96020}, -- health potion
			{id = 2666, count = 35, chance = 60050}, -- meat
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 5880, count = {4, 6}, chance = 65510}, --  iron ore
			{id = 14175, count = {5, 9}, chance = 64900}, -- coal
			{id = 13988, chance = 7020, uniqueDrop = 1}, -- plate of underwater
			{id = 14317, chance = 12190}, -- enhanced oak shield
			{id = 2152, count = {5, 10}, chance = 92140}, -- silver coin
			{id = 2148, count = {32, 74}, chance = 86130}, -- gold
		}
	},
	{ name = 'Beruk', file = 'Cirith/Bosses/Beruk.xml', look = {type = 631}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Beruk', 'Beruk'}, experience = 25000, health = 50000, healthMax = 50000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 25 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 35 },
			{ type = 'ice', value = 15 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 17653, chance = 500, uniqueDrop = 1}, -- Smugglers armor
			{id = 16157, chance = 500}, -- black steel armor
			{id = 16158, chance = 1000}, -- black steel helmet
			{id = 5887, chance = 4000}, -- piece of royal steel
			{id = 2195, chance = 1000, uniqueDrop = 1}, -- boh
			{id = 15732, chance = 11240}, -- knight axe
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 2145, count = 5, chance = 98830}, -- small diamond
			{id = 14362, chance = 7390}, -- steel armor
			{id = 14327, chance = 3210}, -- steel legs
			{id = 5892, chance = 89140}, -- steel
			{id = 14351, chance = 4350}, -- iron boots
			{id = 13754, count = 3, chance = 96290}, -- rope belt
			{id = 13985, chance = 8170}, -- armor of loyalists
			{id = 15776, chance = 39780}, -- short sword
			{id = 2050, chance = 30040}, -- torch
			{id = 14348, chance = 15000}, -- iron plate
			{id = 2152, count = {5, 10}, chance = 92140}, -- silver coin
			{id = 2148, count = {36, 71}, chance = 83150}, -- coin
		}
	},
	{ name = 'Goldmoon', file = 'Cirith/Bosses/Proclay Tasks/Goldmoon.xml', look = {type = 381}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Goldmoon', 'Goldmoon'}, experience = 32000, health = 16000, healthMax = 16000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 25 },
			{ type = 'death', value = -5 },
			{ type = 'toxic', value = 15 },
			{ type = 'energy', value = 35 },
			{ type = 'earth', value = 15 },
			{ type = 'ice', value = 35 },
			{ type = 'fire', value = 30 },
		},
		loot = {
			{id = 13998, chance = 500, uniqueDrop = 1}, -- Master warrior's armor
			{id = 17728, chance = 5180, uniqueDrop = 1}, -- sanctified spear
			{id = 13928, chance = 850, uniqueDrop = 1}, -- shield of invaders
			{id = 2195, chance = 10000, uniqueDrop = 1}, -- boh
			{id = 14480, chance = 920}, -- royal helmet
			{id = 12998, chance = 800, uniqueDrop = 1}, -- rider of light
			{id = 14009, chance = 1200}, -- lightning robe
			{id = {16161, 16159, 16162}, chance = 2090, uniqueDrop = 1}, -- black steel sword/axe/hammer
			{id = 14320, chance = 5140, uniqueDrop = 1}, -- steel boots
			{id = 14322, chance = 8140}, -- royal steel helmet
			{id = 16157, chance = 5190}, -- black steel armor
			{id = {16138, 16137, 16136, 16143, 13949, 16141, 16140, 16139}, chance = 510, uniqueDrop = 1}, -- golden set
			{id = 2171, chance = 51210}, -- platinum amulet
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 15732, chance = 11240}, -- knight axe
			{id = 16158, chance = 7310}, -- black steel helmet
			{id = 14321, chance = 5420}, -- royal plate legs
			{id = 14007, chance = 21750}, -- guardian shield
			{id = 5887, chance = 9400}, -- piece of royal steel
			{id = 14327, chance = 32320}, -- plate legs
			{id = {8473, 8472}, count = 2, chance = 2900}, -- UHP
			{id = 16143, chance = 9580}, -- golden knife
			{id = 13944, chance = 3290, uniqueDrop = 1}, -- ashbringer boots
			{id = 2170, chance = 44410, subType = 100}, -- silver amulet
			{id = {7591, 7590}, count = {3, 8}, chance = 50490}, -- GHP
			{id = 14348, chance = 65000}, -- iron plate
			{id = 2666, count = 2, chance = 91310}, -- meat
			{id = 2152, count = {3, 16}, chance = 92140}, -- silver coin
			{id = 2148, count = {42, 68}, chance = 97540}, -- gold
		}
	},
	{ name = 'Illusionary Hypnotizer-lurker', file = 'Cirith/Bosses/Proclay Tasks/Illusionary Hypnotizer-lurker.xml', look = {type = 489}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Illusionary Hypnotizer-lurker', 'Illusionary Hypnotizer-lurker'}, experience = 11200, health = 5600, healthMax = 5600,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 2539, chance = 1000, uniqueDrop = 1}, -- phoenix shield
			{id = 9980, chance = 2900}, -- piggy bank
			{id = 16202, chance = 500, uniqueDrop = 1}, -- hellfire armor
			{id = 13939, chance = 850}, -- hellfire crossbow
			{id = 1985, chance = 170, uniqueDrop = 1}, -- grey tome
			{id = 16203, chance = 500, uniqueDrop = 1}, -- hellfire helmet
			{id = 16174, chance = 300, uniqueDrop = 1}, -- ruby spear
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 8902, chance = 2480}, -- spellbook of mind control
			{id = 16143, chance = 5560}, -- golden blade
			{id = 9971, chance = 25000}, -- gold ingot
			{id = 10519, chance = 16250}, -- orange backpack
			{id = 2167, chance = 88880}, -- energy ring
			{id = 14449, chance = 19780}, -- wand of draconia
			{id = 5911, chance = 94500}, -- red poc
			{id = 7590, count = {2, 4}, chance = 88980}, -- GMP
			{id = 8472, count = 3, chance = 39090}, -- UMP
			{id = 13588, chance = 10000, uniqueDrop = 1}, -- fire axe
			{id = 2152, count = {2, 6}, chance = 98900}, -- silver
			{id = 2148, count = {19, 44}, chance = 97270}, -- gold
		}
	},
	{ name = 'Wounded Moothan', file = 'Cirith/Bosses/Proclay Tasks/Wounded Moothan.xml', look = {type = 29}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Wounded Moothan', 'Wounded Moothan'}, experience = 7000, health = 3500, healthMax = 3500,
		loot = {
			{id = {14353, 14354, 14355, 14357}, chance = 12050}, -- bronze set
			{id = 14352, chance = 1290}, -- iron blade
			{id = 14324, chance = 5500}, -- steel shield
			{id = 7401, chance = 5660}, -- trophy
			{id = 2532, chance = 5000, uniqueDrop = 1}, -- legs of the minotaur's skin
			{id = 15792, chance = 88790}, -- star-spoke
			{id = 7435, chance = 6160, uniqueDrop = 1}, -- impaler
			{id = 14358, chance = 33590}, -- bronze axe
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 2050, chance = 61750}, -- torch
			{id = 5878, chance = 100000}, -- mino leather
			{id = 15715, chance = 9290}, -- cranial basher
			{id = 7618, count = 10, chance = 89570}, -- health potion
			{id = 2152, count = 5, chance = 92650}, -- silver
			{id = 2148, count = {45, 86}, chance = 78690}, -- coin
		}
	},
	{ name = 'Nobbu', file = 'Cirith/Bosses/Proclay Tasks/Nobbu.xml', look = {type = 561}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Nobbu', 'Nobbu'}, experience = 8400, health = 4200, healthMax = 4200,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 14170, chance = 7690}, -- armorour legionnaires
			{id = 11404, chance = 2600, uniqueDrop = 1}, -- heavy crossbow
			{id = 16307, chance = 1500, uniqueDrop = 1}, -- enhanced leather legs
			{id = 14349, chance = 900, uniqueDrop = 1}, -- iron helmet
			{id = 13888, chance = 760, uniqueDrop = 1}, -- lightning axe
			{id = 14487, chance = 85900}, -- chain helmet
			{id = 15717, chance = 59000}, -- glorious axe
			{id = 14333, chance = 24140}, -- runed sword
			{id = 7618, count = 9, chance = 89570}, -- health potion
			{id = 2152, count = 5, chance = 92650}, -- silver
			{id = 2148, count = 58, chance = 78690}, -- coin
		}
	},
	{ name = 'Forbidden Hunger Mage', file = 'Cirith/Bosses/Proclay Tasks/Forbidden Hunger Mage.xml', look = {type = 492, addons = 2}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Forbidden Hunger Mage', 'Forbidden Hunger Mage'}, experience = 10000, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 18076, chance = 250, uniqueDrop = 1}, -- grey dye
			{id = 14006, chance = 9720}, -- cape of fear
			{id = 2153, chance = 3220}, -- violet gem
			{id = 2154, chance = 5270}, -- yellow gem
			{id = 14448, chance = 25430}, -- necrotic rod
			{id = 13592, chance = 1590}, -- cursed spear
			{id = 2796, chance = 91460}, -- green mushroom
			{id = 18338, chance = 1500}, -- red gem
			{id = {16145, 16147, 16146, 16144, 16148}, chance = 1500, uniqueDrop = 1}, -- fallen set
			{id = {13945, 13946}, chance = 3250, uniqueDrop = 1}, -- ashbringer hood
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 13989, chance = 1590, uniqueDrop = 1}, -- robe of the underworld
			{id = 15737, chance = 1980, uniqueDrop = 1}, -- bone bow
			{id = 2147, count = 3, chance = 72890}, -- small ruby
			{id = 17683, chance = 2250}, -- cursed blade
			{id = 2150, count = 3, chance = 83250}, -- small amethyst
			{id = 2144, count = 3, chance = 25370}, -- black pearl
			{id = 11231, chance = 10120}, -- book of necromantic rituals
			{id = 7589, count = {3, 5}, chance = 85510}, -- SMP
			{id = 2152, count = {2, 5}, chance = 92650}, -- silver
			{id = 2148, count = {22, 42}, chance = 99890}, -- gold
		}
	},
	{ name = 'Ellort', file = 'Cirith/Bosses/Proclay Tasks/Ellort.xml', look = {type = 589, head = 87, body = 71, legs = 110, feet = 0}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Ellort', 'Ellort'}, experience = 6200, health = 3100, healthMax = 3100,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 80 },
		},
		loot = {
			{id = {14353, 14354, 14355, 14357}, chance = 18750}, -- bronze set
			{id = 14446, chance = 1290}, -- seeing eye
			{id = 13911, chance = 1500, uniqueDrop = 1}, -- wiradon's spellbook
			{id = 14170, chance = 15000, uniqueDrop = 1}, -- armorour legionnaires
			{id = 15727, chance = 300, uniqueDrop = 1}, -- minotaur hammer
			{id = 15790, chance = 88790}, -- copper axe
			{id = 2376, chance = 1160, uniqueDrop = 1}, -- runed blade
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 14358, chance = 23590}, -- bronze axe
			{id = 2050, chance = 61750}, -- torch
			{id = 15609, chance = 1290, uniqueDrop = 1}, -- massive ornamented bow
			{id = 7620, count = 9, chance = 89570}, -- mana potion
			{id = 2152, count = 4, chance = 92650}, -- silver
			{id = 2148, count = {58, 80}, chance = 78690}, -- coin
		}
	},
	{ name = 'Wrath of Nature', file = 'Cirith/Bosses/Proclay Tasks/Wrath of Nature.xml', look = {type = 358}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Wrath of Nature', 'Wrath of Nature'}, experience = 4200, health = 2100, healthMax = 2100,
		elements = {
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 35 },
		},
		loot = {
			{id = 2154, chance = 8050}, -- yellow gem
			{id = 2747, chance = 7870}, -- gave flower
			{id = 15775, chance = 69950}, -- leather boots
			{id = 2177, chance = 6970}, -- life crystal
			{id = 2600, chance = 91010}, -- inkwell
			{id = 14459, chance = 49140}, -- wand of cosmic energy
			{id = 13981, chance = 20000, subType = 100}, -- amulet
			{id = {13906, 13907, 13908, 13909, 13911}, chance = 10590}, -- Wradon's set
			{id = 5922, chance = 20070}, -- holy orchid
			{id = 11811, chance = 800, uniqueDrop = 1}, -- elvish soldier bow
			{id = 7438, chance = 5000, uniqueDrop = 1}, -- elvish bow
			{id = 2047, chance = 32080}, -- candlestick
			{id = 7589, chance = 73000}, -- SMP
			{id = 7618, count = {3, 7}, chance = 94040}, -- health potion
			{id = {13056, 13071, 13059, 13062}, chance = 3500}, -- silver crafting additives
			{id = 2802, chance = 4940}, -- sling herb
			{id = 7364, count = {8, 13}, chance = 66050}, -- sniper arrow
			{id = 18073, chance = 1950, uniqueDrop = 1}, -- ethereal staff
			{id = 10551, chance = 70110}, -- elvish talisman
			{id = 2689, chance = 53750}, -- bread
			{id = 2682, chance = 42380}, -- melon
			{id = 1949, chance = 60500}, -- scroll
			{id = 2152, count = 4, chance = 90230}, -- silver coin
			{id = 2148, count = {30, 70}, chance = 97060}, -- gold
		}
	},
	{ name = 'Waspero', file = 'Cirith/Bosses/Proclay Tasks/Waspero.xml', look = {type = 467}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Waspero', 'Waspero'}, experience = 4000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2168, chance = 25000}, -- life ring
			{id = 13004, chance = 500, uniqueDrop = 1}, -- leaf shield
			{id = 14696, chance = 92450}, -- honeycomb
			{id = 2545, count = 15, chance = 72160}, -- poison arrows
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 17941, chance = 15150}, -- radiating powder
			{id = 13849, chance = 750}, -- leaf helmet
		}
	},
	{ name = 'Captain Hook', file = 'Cirith/Bosses/Proclay Tasks/Captain Hook.xml', look = {type = 98}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Captain Hook', 'Captain Hook'}, experience = 10000, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14319, chance = 920, uniqueDrop = 1}, -- royal steel shield
			{id = 16157, chance = 750, uniqueDrop = 1}, -- black steel armor
			{id = 5462, chance = 1500, uniqueDrop = 1}, -- pirate boots
			{id = 5812, chance = 1110}, -- skull candle
			{id = 2114, chance = 520}, -- piggy bank
			{id = 5926, chance = 2090, uniqueDrop = 1}, -- pirate backpack
			{id = 16293, chance = 5100, uniqueDrop = 1}, -- boots of the dawn
			{id = 2143, chance = 33480}, -- white pearl
			{id = 13184, chance = 1000, uniqueDrop = 1}, -- bronze chest
			{id = 13185, chance = 200, uniqueDrop = 1}, -- silver chest
			{id = 2157, chance = 8930}, -- gold nugget
			{id = 6126, chance = 9560}, -- peg leg
			{id = 7588, chance = 5790}, -- SHP
			{id = 2521, chance = 31170}, -- dark shield
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 8841, chance = 25120}, -- lemon
			{id = 6096, chance = 4090, uniqueDrop = 1}, -- pirate hat
			{id = 10558, chance = 94100}, -- spookey blue eye
			{id = 14001, chance = 10300}, -- dark armor
			{id = 14358, chance = 9900}, -- bronze axe
			{id = 11213, chance = 90570}, -- compass
			{id = 2152, count = {2, 6}, chance = 90230}, -- silver coin
			{id = 2148, count = {20, 65}, chance = 99890}, -- gold
		}
	},
	{ name = 'Demonplague Consumer', file = 'Cirith/Bosses/Proclay Tasks/Demonplague Consumer.xml', look = {type = 296}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Demonplague Consumer', 'Demonplague Consumer'}, experience = 5600, health = 2800, healthMax = 2800,
		elements = {
			{ type = 'holy', value = 5 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -5 },
		},
		loot = {
			{id = 7697, chance = 150, uniqueDrop = 1}, -- signet
			{id = 15714, chance = 24820}, -- club
			{id = 2235, chance = 76900}, -- cheese
			{id = 5880, chance = 32190}, -- iron ore
			{id = 5878, chance = 9990}, -- hardener leather
			{id = 14324, chance = 4000, uniqueDrop = 1}, -- steel shield
			{id = 1294, count = 10, chance = 50010}, -- small stone
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 2667, count = 3, chance = 72500}, -- fish
			{id = 14452, chance = 2450, uniqueDrop = 1}, -- staff of wind
			{id = 2386, chance = 9810}, -- goblin's chopper
			{id = 2230, chance = 12950}, -- bone
			{id = 5891, chance = 5190}, -- enchanted chicken wing
			{id = 2379, chance = 27420}, -- blade
			{id = 15774, chance = 82900}, -- leather legs
			{id = 2152, count = 3, chance = 90230}, -- silver coin
			{id = 2148, count = {20, 25}, chance = 99890}, -- gold
		}
	},
	{ name = 'Ner\'zhul', file = 'Cirith/Bosses/Proclay Tasks/Ner\'zhul.xml', look = {type = 492, addons = 2}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Ner\'zhul', 'Ner\'zhul'}, experience = 24000, health = 12000, healthMax = 12000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'death', value = 50 },
			{ type = 'earth', value = 15 },
		},
		loot = {
			{id = 2624, chance = 1, uniqueDrop = 1}, -- ner'zhul helmet
			{id = 17161, chance = 860, uniqueDrop = 1}, -- death enchanted in the staff
			{id = 14006, chance = 25720}, -- cape of fear
			{id = 2153, chance = 9020}, -- violet gem
			{id = 2154, chance = 23470}, -- yellow gem
			{id = 16356, chance = 9700, uniqueDrop = 1}, -- container
			{id = 13592, chance = 1590, uniqueDrop = 1}, -- cursed spear
			{id = 2796, chance = 31460}, -- green mushroom
			{id = 18338, chance = 11100}, -- red gem
			{id = 2123, chance = 2120, uniqueDrop = 1}, -- diamond ring
			{id = 13989, chance = 990, uniqueDrop = 1}, -- robe of the underworld
			{id = 15737, chance = 980, uniqueDrop = 1}, -- bone bow
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 2147, count = {3, 7}, chance = 72890}, -- small ruby
			{id = 17683, chance = 1550}, -- cursed blade
			{id = 5809, count = 3, chance = 25990}, -- magical element
			{id = 11694, chance = 77730}, -- fine fabric
			{id = 2150, count = {3, 10}, chance = 93250}, -- small amethyst
			{id = 2144, count = 3, chance = 25370}, -- black pearl
			{id = 11231, chance = 80120}, -- book of necromantic rituals
			{id = 7589, count = {3, 5}, chance = 75510}, -- SMP
			{id = 2152, count = {3, 10}, chance = 90230}, -- silver coin
			{id = 2148, count = {45, 90}, chance = 99890}, -- gold
		}
	},
	{ name = 'Infernal Oozer-orb', file = 'Cirith/Bosses/Proclay Tasks/Infernal Oozer-orb.xml', look = {type = 130, head = 0, body = 77, legs = 91, feet = 115, addons = 1}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Infernal Oozer-orb', 'Infernal Oozer-orb'}, experience = 78000, health = 39000, healthMax = 39000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 35 },
		},
		loot = {
			{id = 18071, chance = 1100, uniqueDrop = 1}, -- electric rod
			{id = 16356, chance = 2000, uniqueDrop = 1}, -- bag
			{id = 2123, chance = 1550, uniqueDrop = 1}, -- rots
			{id = 2114, chance = 4550}, -- piggy bank
			{id = 16138, chance = 510, uniqueDrop = 1}, -- golden armor
			{id = 13754, count = {3, 6}, chance = 96700}, -- rope belt
			{id = 1985, chance = 80}, -- grey tome
			{id = 18073, chance = 5250, uniqueDrop = 1}, -- ethereal staff
			{id = 2197, chance = 5320, subType = 6}, -- ssa
			{id = 2124, chance = 9990}, -- crystal ring
			{id = {13056, 13071, 13059, 13062}, chance = 3000}, -- silver crafting additives
			{id = 2600, chance = 14940}, -- inkwell
			{id = 14009, chance = 2050, uniqueDrop = 1}, -- lightining robe
			{id = 12964, chance = 2900}, -- clear lunar crystal
			{id = 2151, chance = 28100}, -- talon
			{id = 14450, chance = 2450, uniqueDrop = 1}, -- staff of powerful energy
			{id = 2146, count = {3, 10}, chance = 91180}, -- small sapphire
			{id = 14005, chance = 5350}, -- wizard's robe
			{id = 2047, chance = 51450}, -- candlestick
			{id = 14457, chance = 700, uniqueDrop = 1}, -- grim wand
			{id = 2178, chance = 72120}, -- mind stone
			{id = 2167, chance = 62120}, -- energy ring
			{id = 2792, chance = 73070}, -- dark mushroom
			{id = 14325, chance = 17730, uniqueDrop = 1}, -- steel blade
			{id = 7590, count = 3, chance = 84770}, -- GMP
			{id = 7591, count = 3, chance = 95100}, -- GHP
			{id = 15761, chance = 9760}, -- snake blade
			{id = 2689, chance = 79040}, -- bread
			{id = 2690, count = 2, chance = 89680}, -- roll
			{id = 14460, chance = 400, uniqueDrop = 1}, -- death wolf staff
			{id = 2152, count = {3, 10}, chance = 90230}, -- silver coin
			{id = 2148, count = {45, 90}, chance = 97240}, -- coin
		}
	},
	{ name = 'Kiri Norol', file = 'Cirith/Bosses/Proclay Tasks/Kiri Norol.xml', look = {type = 373}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Kiri Norol', 'Kiri Norol'}, experience = 100000, health = 50000, healthMax = 50000,
		elements = {
			{ type = 'holy', value = 5 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 25 },
		},
		loot = {
			{id = 16480, chance = 150, uniqueDrop = 1}, -- draken doll
			{id = 16173, chance = 250, uniqueDrop = 1}, -- ruby helmet
			{id = 13895, chance = 190, uniqueDrop = 1}, -- cursed blade
			{id = 13989, chance = 950, uniqueDrop = 1}, -- robe of underground
			{id = 11734, chance = 510, uniqueDrop = 1}, -- elite draken mail
			{id = 16158, chance = 7010, uniqueDrop = 1}, -- black steel helmet
			{id = 16159, chance = 2020}, -- black steel axe
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 16157, chance = 10150}, -- black steel armor
			{id = 16174, chance = 600, uniqueDrop = 1}, -- ruby spear
			{id = 16205, chance = 1260, uniqueDrop = 1}, -- meteorite boots
			{id = 15734, chance = 2020}, -- vile axe
			{id = 16156, chance = 5800}, -- black steel legs
			{id = 11732, chance = 5100, uniqueDrop = 1}, -- twiceslicer
			{id = 5904, chance = 92040}, -- magic sulphur
			{id = 2145, count = {4, 10}, chance = 92460}, -- small diamond
			{id = 10028, chance = 97990}, -- draken sulphur
			{id = 7590, count = {3, 9}, chance = 99050}, -- GMP
			{id = 8473, count = {3, 9}, chance = 99340}, -- UHP
			{id = 11733, chance = 94800}, -- broken slicer
			{id = 2666, chance = 99920}, -- meat
			{id = 2152, count = {5, 25}, chance = 90230}, -- silver coin
			{id = 2148, count = {45, 90}, chance = 97240}, -- coin
		}
	},
	{ name = 'Throer', file = 'Cirith/Bosses/Proclay Tasks/Throer.xml', look = {type = 695}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Throer', 'Throer'}, experience = 16000, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 13707, count = 3, chance = 91400}, -- cuprite
			{id = 2158, chance = 5290}, -- blue gem
			{id = 14349, chance = 6560, uniqueDrop = 1}, -- iron helmet
			{id = 13991, chance = 50, uniqueDrop = 1}, -- mythril armor
			{id = 2213, chance = 22710}, -- dwarven ring
			{id = 12952, chance = 41300}, -- bronze helmet
			{id = 14326, chance = 5140, uniqueDrop = 1}, -- steel boots
			{id = {13056, 13071, 13059, 13062}, chance = 3000}, -- silver crafting additives
			{id = 2157, chance = 9250}, -- gold nugget
			{id = 15729, chance = 9190}, -- double axe
			{id = 17870, chance = 9920, uniqueDrop = 1}, -- melted axe
			{id = 9970, chance = 99720}, -- small topaz
			{id = 11693, chance = 50450}, -- mithril ore
			{id = 5880, chance = 94200}, -- iron ore
			{id = 7591, count = 2, chance = 92520}, -- strong health potion
			{id = 14175, count = {2, 6}, chance = 92140}, -- coal
			{id = 2789, count = 2, chance = 95050}, -- brown mushroom
			{id = 2152, count = {3, 6}, chance = 00220}, -- silver
			{id = 2148, count = {35, 90}, chance = 92380}, -- gold
		}
	},
	{ name = 'Durin', file = 'Cirith/Bosses/Proclay Tasks/Durin.xml', look = {type = 70}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Durin', 'Durin'}, experience = 3300, health = 1650, healthMax = 1650,
		loot = {
			{id = 2213, chance = 25000}, -- dwarven ring
			{id = 2214, chance = 9320, uniqueDrop = 1}, -- ring of endurance
			{id = 2643, chance = 25860}, -- breastplate
			{id = 14432, chance = 1000, uniqueDrop = 1}, -- cobalt helmet
			{id = 9971, chance = 2500}, -- gold ignot
			{id = 8849, chance = 2500, uniqueDrop = 1}, -- modified crossbow
			{id = 17870, chance = 15000}, -- melted axe
			{id = 5892, chance = 8950}, -- steel
			{id = 2157, chance = 5300}, -- gold nugget
			{id = 2150, chance = 91600}, -- small amethyst
			{id = {13056, 13071, 13059, 13062}, chance = 1500}, -- silver crafting additives
			{id = 5887, chance = 2030, uniqueDrop = 1}, -- royal steel
			{id = 2440, chance = 760, uniqueDrop = 1}, -- daramanian axe
			{id = 14175, chance = 92300}, -- coal
			{id = 7618, chance = 99340}, -- health potion
			{id = 5880, chance = 94470}, -- iron ore
			{id = 2148, count = {20, 90}, chance = 90190}, -- gold
			{id = 2787, count = 2, chance = 55540}, -- white mushroom
		}
	},
	{ name = 'Alucard', file = 'Cirith/Bosses/Proclay Tasks/Alucard.xml', look = {type = 513, head = 0, body = 129, legs = 0, feet = 0}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Alucard', 'Alucard'}, experience = 18000, health = 9000, healthMax = 9000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 9019, chance = 500, uniqueDrop = 1}, -- vampire doll
			{id = 13978, chance = 900, uniqueDrop = 1}, -- bloody crusher
			{id = 12956, chance = 2000, uniqueDrop = 1}, -- marksman helmet
			{id = 2534, chance = 3200}, -- sleep shield
			{id = 7890, chance = 800, uniqueDrop = 1}, -- bloodstone
			{id = 16161, chance = 4000, uniqueDrop = 1}, -- black steel blade
			{id = 5911, chance = 35050}, -- red poc
			{id = 6300, chance = 50500}, -- death ring
			{id = 2145, count = {10, 16}, chance = 81340}, -- small diamond
			{id = 7591, count = {2, 4}, chance = 84820}, -- GHP
			{id = 7590, count = {2, 4}, chance = 86010}, -- GMP
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 2144, count = {2, 3}, chance = 86770}, -- black pearl
			{id = 7364, count = {9, 18}, chance = 90870}, -- sniper arrow
			{id = 13736, chance = 93140}, -- blood
			{id = 13619, chance = 96770}, -- vampire's cape chain
			{id = 10601, chance = 97560}, -- vampire teeth
			{id = 5944, chance = 99010}, -- soul orb
			{id = 6500, chance = 99720}, -- de
			{id = 2152, count = 3, chance = 99960}, -- silver
			{id = 2148, count = {4, 9}, chance = 93010}, -- gold
		}
	},
	{ name = 'Nemrod', file = 'Cirith/Bosses/Proclay Tasks/Nemrod.xml', look = {type = 397, addons = 3}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Nemrod', 'Nemrod'}, experience = 7000, health = 3500, healthMax = 3500,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13892, chance = 3900, uniqueDrop = 1}, -- composite bow
			{id = 2195, chance = 1200, uniqueDrop = 1}, -- boh
			{id = 14173, chance = 1900, uniqueDrop = 1}, -- armor of leaf clan
			{id = 7365, count = 24, chance = 12490}, -- onyx arrow
			{id = 2050, chance = 99000}, -- torch
			{id = 14015, chance = 12900}, -- short bow
			{id = 2379, chance = 61610}, -- blade
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 14335, chance = 6810}, -- wolf hood
			{id = 14336, chance = 25000}, -- hood
			{id = 2456, chance = 89570}, -- bow
			{id = 2152, chance = 9800}, -- silver
			{id = 2148, count = {32, 74}, chance = 51680}, -- coin
		}
	},
	{ name = 'Old Queen', file = 'Cirith/Bosses/Proclay Tasks/Old Queen.xml', look = {type = 622}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Old Queen', 'Old Queen'}, experience = 7000, health = 3500, healthMax = 3500,
		elements = {
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 19466, chance = 600, uniqueDrop = 1}, -- toxic ring
			{id = 14326, chance = 3265, uniqueDrop = 1}, -- steel boots
			{id = 7618, chance = 96020}, -- health potion
			{id = 2666, chance = 60050}, -- meat
			{id = 5880, count = {4, 6}, chance = 65510}, --  iron ore
			{id = {13056, 13071, 13059, 13062}, chance = 2000}, -- silver crafting additives
			{id = 14353, chance = 32020}, -- bronze armor
			{id = 14175, count = {5, 9}, chance = 44900}, -- coal
			{id = 2148, count = {32, 74}, chance = 86130}, -- gold
			{id = 14357, chance = 12190}, -- bronze shield
			{id = 13707, count = {2, 3}, chance = 32730}, -- cuprite
		}
	},
	{ name = 'X\'men\'us', file = 'Cirith/Bosses/Proclay Tasks/Xmenus.xml', look = {type = 318}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'X\'men\'us', 'X\'men\'us'}, experience = 9000, health = 4500, healthMax = 4500,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 5 },
			{ type = 'death', value = 65 },
			{ type = 'earth', value = 15 },
			{ type = 'energy', value = 5 },
		},
		loot = {
			{id = 17223, chance = 3200, uniqueDrop = 1}, -- claw
			{id = 2168, chance = 75000}, -- life ring
			{id = 6534, chance = 600, uniqueDrop = 1}, -- flying trident
			{id = 2226, chance = 90620}, -- fishbone
			{id = 13592, chance = 1500, uniqueDrop = 1}, -- cursed spear
			{id = 14318, chance = 2000, uniqueDrop = 1}, -- royal steel blade
			{id = 11404, chance = 1830, uniqueDrop = 1}, -- heavy crossbow
			{id = 11222, chance = 91140}, -- sabretooth
			{id = {13056, 13071, 13059, 13062}, chance = 3000}, -- silver crafting additives
			{id = 7588, count = 2, chance = 95700}, -- health potion
			{id = 11204, chance = 80750}, -- striped fur
			{id = 2666, count = 3, chance = 85780}, -- meat
			{id = 2152, count = {2, 4}, chance = 53900}, -- silver
			{id = 2148, count = {23, 70}, chance = 92750}, -- gold
		}
	},
	{ name = 'Hand Faerie', file = 'Cirith/Bosses/Proclay Tasks/Hand Faerie.xml', look = {type = 158, head = 102, body = 104, legs = 47, feet = 42, addons = 3}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Hand Faerie', 'Hand Faerie'}, experience = 13000, health = 6500, healthMax = 6500,
		elements = {
			{ type = 'physical', value = -15 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 25 },
			{ type = 'energy', value = 80 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 5 },
		},
		loot = {
			{id = 11387, chance = 500, uniqueDrop = 1}, -- luck amulet
			{id = 3982, chance = 3050, uniqueDrop = 1}, -- green robe
			{id = {13848, 13847}, chance = 2000}, -- leaf legs/armor
			{id = 16356, chance = 2500, uniqueDrop = 1}, -- bag
			{id = 1998, chance = 7130}, -- green backpack
			{id = {15760 ,16169}, chance = 3080}, -- snake/swamplair bow
			{id = 14351, chance = 6620}, -- iron boots
			{id = {13056, 13071, 13059 ,13062}, chance = 5000}, -- silver crafting additives
			{id = 13940, chance = 4290, uniqueDrop = 1}, -- snake crossbow
			{id = 16481, chance = 700, uniqueDrop = 1}, -- luck ring
			{id = 2129, chance = 9490}, -- wolf tooth chain
			{id = 13980, chance = 91500, subType = 70}, -- earth amulet
			{id = 14323, chance = 5310, uniqueDrop = 1}, -- royal steel armor
			{id = 13743, chance = 75260}, -- colourful feather
			{id = {2149, 2145}, chance = 100000}, -- small emerald
			{id = 13948, chance = 88450}, -- hunting spear
			{id = 14000, chance = 48330}, -- light armor
			{id = 13581, chance = 84190}, -- creature products
			{id = 7588, chance = 67000}, -- shp
			{id = 13577, chance = 97590}, -- creature products
			{id = 2152, count = 9, chance = 53450}, -- silver
			{id = 2148, count = {35, 90}, chance = 99730}, -- gold
		}
	},
	{ name = 'Tarantulos', file = 'Cirith/Bosses/Proclay Tasks/Tarantulos.xml', look = {type = 219}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Tarantulos', 'Tarantulos'}, experience = 6000, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 8859, chance = 96970}, -- spider fang
			{id = 15752, chance = 98330}, -- brass legs
			{id = 14010, chance = 850, uniqueDrop = 1}, -- knight armor
			{id = 15713, chance = 75250}, -- longsword
			{id = 13924, chance = 1500, uniqueDrop = 1}, -- light crossbow
			{id = 14327, chance = 2140, uniqueDrop = 1}, -- plate legs
			{id = 7588, chance = 93460}, -- SHP
			{id = 14478, chance = 38810}, -- steel helmet
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 2171, chance = 1300, uniqueDrop = 1}, -- amulet
			{id = 14362, chance = 2000, uniqueDrop = 1}, -- plate armor
			{id = 15609, chance = 1490, uniqueDrop = 1}, -- massive ornamented bow
			{id = 2545, count = 12, chance = 12160}, -- poison arrows
			{id = 2152, count = {3, 5}, chance = 90650}, -- silver
			{id = 2148, count = {62, 98}, chance = 78690}, -- coin
		}
	},
	{ name = 'Mar\'Tal The Leader', file = 'Cirith/Bosses/Proclay Tasks/Mar\'Tal The Leader.xml', look = {type = 463}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Mar\'Tal The Leader', 'Mar\'Tal The Leader'}, experience = 40000, health = 20000, healthMax = 20000,
		elements = {
			{ type = 'physical', value = 1 },
			{ type = 'energy', value = 3 },
			{ type = 'earth', value = 10 },
			{ type = 'ice', value = 10 },
			{ type = 'death', value = 15 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 7401, chance = 2500}, -- mino trophy
			{id = 14010, chance = 2000, uniqueDrop = 1}, -- knight armor
			{id = 14321, chance = 1110, uniqueDrop = 1}, -- royal steel legs
			{id = 5911, chance = 2540}, -- red poc
			{id = 14319, chance = 4200}, -- steel shield
			{id = 14171, chance = 5290}, -- berynit armor
			{id = 14352, chance = 3240, uniqueDrop = 1}, -- iron sword
			{id = 7589, chance = 11070}, -- SMP
			{id = 7588, chance = 21950}, -- SHP
			{id = {12955, 12954}, chance = 300, uniqueDrop = 1}, -- marksman armor/legs
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 9970, chance = 52030}, -- small topaz
			{id = 2147, chance = 52080}, -- small ruby
			{id = 2150, chance = 52120}, -- small amethyst
			{id = 5878, chance = 95180}, -- mino leather
			{id = 2149, chance = 52190}, -- small emerald
			{id = 2152, count = 4, chance = 93990}, -- silver
			{id = 2148, count = {45, 80}, chance = 99890}, -- coin
		}
	},
	{ name = 'Cyclopus', file = 'Cirith/Bosses/Proclay Tasks/Cyclopus.xml', look = {type = 22}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Cyclopus', 'Cyclopus'}, experience = 4400, health = 2200, healthMax = 2200,
		elements = {
			{ type = 'death', value = -15 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 40 },
		},
		loot = {
			{id = 7398, chance = 2500}, -- cyclops trophy
			{id = 14354, chance = 9210}, -- bronze helmet
			{id = 16356, chance = 2000, uniqueDrop = 1}, -- container
			{id = 15780, chance = 46200, subType = 800}, -- zebiasty
			{id = 14349, chance = 25990}, -- iron helmet
			{id = 14172, chance = 1000, uniqueDrop = 1}, -- chest guard
			{id = 7618, chance = 96020}, -- health potion
			{id = 15713, chance = 15990}, -- longsword
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 12963, chance = 72590}, -- lampart shield
			{id = 15784, chance = 61420}, -- copper shield
			{id = 10573, chance = 95050}, -- cyclops toe
			{id = 15776, chance = 27940}, -- short sword
			{id = 2666, chance = 40050}, -- meat
			{id = 2148, count = {45, 89}, chance = 92130}, -- gold
		}
	},
	{ name = 'Voltar The Thief', file = 'Cirith/Bosses/Proclay Tasks/Voltar The Thief.xml', look = {type = 573, head = 21, body = 48, legs = 65, feet = 58}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Voltar The Thief', 'Voltar The Thief'}, experience = 4000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 15753, chance = 6620}, -- brass boots
			{id = {15752, 15751}, chance = 9680}, -- brass legs/armor
			{id = 15750, chance = 11090}, -- brass helmet
			{id = 17267, chance = 1000, uniqueDrop = 1}, -- doll
			{id = 15782, chance = 74590}, -- steel spear
			{id = 3940, chance = 650, uniqueDrop = 1}, -- camouflage backpack
			{id = 17269, chance = 62170}, -- cheesy figurine
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 17268, chance = 88910}, -- earflap
			{id = 2643, chance = 97690}, -- breastplate
			{id = 17271, chance = 99670}, -- bola
			{id = {2235, 8368}, chance = 94520}, -- cheese
			{id = 2152, count = 5, chance = 93990}, -- silver
			{id = 2148, count = {60, 90}, chance = 90100}, -- gold
		}
	},
	{ name = 'Emerald Behemoth', file = 'Cirith/Bosses/Proclay Tasks/Emerald Behemoth.xml', look = {type = 55}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Emerald Behemoth', 'Emerald Behemoth'}, experience = 90000, health = 45000, healthMax = 45000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16206, chance = 390, uniqueDrop = 1}, -- meteorite legs
			{id = 7396, chance = 1050}, -- behemoth trophy
			{id = 16175, chance = 70, uniqueDrop = 1}, -- ruby mace
			{id = 15731, chance = 50, uniqueDrop = 1}, -- axe aggressor
			{id = 2023, chance = 1160}, -- amphora
			{id = 14322, chance = 12141}, -- royal steel helmet
			{id = 2521, chance = 22250}, -- dark shield
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 2125, chance = 62750, subType = 60}, -- crystal necklace
			{id = 14320, chance = 12530, uniqueDrop = 1}, -- steel boots
			{id = 7591, chance = 82750}, -- GHP
			{id = 2553, chance = 3800, uniqueDrop = 1}, -- pick
			{id = 2231, chance = 3810}, -- big bone
			{id = 2174, chance = 24830}, -- strange symbol
			{id = 13953, chance = 520, uniqueDrop = 1}, -- bloody axe
			{id = 5893, chance = 95990}, -- behemoth fang
			{id = 13999, chance = 2750, uniqueDrop = 1}, -- noble armor
			{id = 16162, chance = 7010, uniqueDrop = 1}, -- black steel hammer
			{id = 15733, chance = 3123, uniqueDrop = 1}, -- Devastator
			{id = 5930, chance = 72510}, -- behemoth claw
			{id = 14362, chance = 48200}, -- plate armor
			{id = 14001, chance = 58990}, -- dark armor
			{id = 2472, chance = 500, uniqueDrop = 1}, -- MPA
			{id = 15723, chance = 29040}, -- two handed sword
			{id = 15718, chance = 69750}, -- chopper
			{id = 13888, chance = 10310}, -- vile axe
			{id = 14363, chance = 91750}, -- steel helmet
			{id = 2150, count = 2, chance = 18950}, -- small amethyst
			{id = 13949, chance = 8730, uniqueDrop = 1}, -- golden spear
			{id = 2666, count = 7, chance = 30740}, -- meat
			{id = 2152, count = {6, 10}, chance = 79680}, -- silver
			{id = 2148, count = {61, 97}, chance = 97616}, -- gold
		}
	},
	{ name = 'Lucifer', file = 'Cirith/Bosses/Proclay Tasks/Lucifer.xml', look = {type = 619}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Lucyfer', 'Lucifer'}, experience = 110000, health = 55000, healthMax = 55000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 11441, count = {5, 20}, chance = 10820}, -- assassin bolt
			{id = 16268, chance = 1200, uniqueDrop = 1}, -- thunderstruck backpack
			{id = 17161, chance = 5890}, -- death enchanted in the staff
			{id = 13933, chance = 250, uniqueDrop = 1}, -- demon armor
			{id = 16791, chance = 2140, uniqueDrop = 1}, -- book of fire secrets
			{id = 2153, chance = 65900}, -- violet gem
			{id = 8900, chance = 26590}, -- spellbook
			{id = 13797, chance = 57120}, -- haunted shield
			{id = 2136, chance = 21780}, -- demonbone amulet
			{id = 6300, chance = 38970}, -- death ring
			{id = 2661, chance = 9670}, -- scarf
			{id = 10518, chance = 5000}, -- demon backpack
			{id = 2795, count = 6, chance = 75360}, -- fire mushroom
			{id = 2130, chance = 19600, subType = 800}, -- golden necklace
			{id = 982, count = 5, chance = 23310}, -- dark talon
			{id = {2150, 2147, 9970, 2149}, count = {3, 8}, chance = 72560}, -- small crystals
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 16208, chance = 1800, uniqueDrop = 1}, -- meteorite helmet
			{id = 2123, chance = 38970}, -- ring with diamond
			{id = 7591, count = 8, chance = 88980}, -- GHP
			{id = 8473, count = 8, chance = 49090}, -- UHP
			{id = 2546, count = {10, 80}, chance = 49870}, -- burst arrow
			{id = 17221, chance = 100, uniqueDrop = 1}, -- demonic shield
			{id = 2160, count = 2, chance = 5270}, -- gold coin
			{id = 2214, chance = 48900}, -- ring of healing
			{id = 6500, count = 3, chance = 99700}, -- demonic essence
			{id = 5954, count = 2, chance = 65390}, -- demon horn
			{id = 2152, count = 8, chance = 89610}, -- silver
			{id = 2148, count = {25, 70}, chance = 97170}, -- copper coin
		}
	},
	{ name = 'Azazel', file = 'Cirith/Bosses/Proclay Tasks/Azazel.xml', look = {type = 85}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Azazel', 'Azazel'}, experience = 136400, health = 68200, healthMax = 68200,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13895, chance = 720, uniqueDrop = 1}, -- cursed blade
			{id = 13989, chance = 1500, uniqueDrop = 1}, -- robe of underground
			{id = 6500, count = 3, chance = 94030}, -- demonic essence
			{id = 8903, chance = 9970}, -- spellbook of lost souls
			{id = 2171, chance = 37800}, -- platinum amulet
			{id = 13340, chance = 54680}, -- demon paw
			{id = 16203, chance = 6900}, -- hellfire helmet
			{id = 16174, chance = 1500, uniqueDrop = 1}, -- ruby spear
			{id = 2214, chance = 61870}, -- ring of healing
			{id = 17706, chance = 800, uniqueDrop = 1}, -- plushy darkness cat
			{id = 7591, count = 8, chance = 64820}, -- GHP
			{id = 2472, chance = 2000, uniqueDrop = 1}, -- magic plate armor
			{id = {2150, 2147, 9970, 2149}, count = {3, 8}, chance = 65600}, -- small crystals
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 13932, chance = 270, uniqueDrop = 1}, -- demon legs
			{id = 2156, chance = 72150}, -- red gem
			{id = 13009, chance = 1200, uniqueDrop = 1}, -- Dragon rider helmet
			{id = 2151, count = 5, chance = 43470}, -- talon
			{id = 7393, chance = 5500}, -- demon trophy
			{id = 18072, chance = 9600}, -- burning stick
			{id = 11679, chance = 5150, uniqueDrop = 1}, -- hellfire bow
			{id = 5954, count = 2, chance = 90100}, -- demon horn
			{id = 7590, count = 8, chance = 58590}, -- GMP
			{id = 2152, count = {3, 7}, chance = 79800}, -- silver
			{id = 2148, count = {30, 50}, chance = 99990}, -- gold
		}
	},
	{ name = 'Old Dragon Mother', file = 'Cirith/Bosses/Proclay Tasks/Old Dragon Mother.xml', look = {type = 562}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Old Dragon Mother', 'Old Dragon Mother'}, experience = 64000, health = 32000, healthMax = 32000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16140, chance = 1400, uniqueDrop = 1}, -- golden axe
			{id = 13794, chance = 2400, uniqueDrop = 1}, -- titan helmet
			{id = 7399, chance = 15000}, -- dragon lord trophy
			{id = 13987, chance = 9960, uniqueDrop = 1}, -- DSM
			{id = 13341, chance = 91210}, -- dragon lord claw
			{id = 14319, chance = 42250}, -- tower shield
			{id = 14480, chance = 10260, uniqueDrop = 1}, -- spartan helmet
			{id = 16356, chance = 5000, uniqueDrop = 1}, -- container
			{id = 2392, chance = 46280}, -- fire sword
			{id = {13056, 13071, 13059, 13062}, chance = 20000}, -- silver crafting additives
			{id = 14479, chance = 57390}, -- strange helmet
			{id = 2177, chance = 8650}, -- life crystal
			{id = 7588, count = 12, chance = 98970}, -- SHP
			{id = 5948, chance = 99020}, -- red dragon leather
			{id = 5882, chance = 99940}, -- red dragon scale
			{id = 2033, chance = 13110}, -- golden mug
			{id = 2146, count = 12, chance = 100000}, -- small sapphire
			{id = 2547, count = 97, chance = 86620}, --power bolt
			{id = 13951, chance = 75490, uniqueDrop = 1}, -- hellfire spear
			{id = 1976, chance = 8980}, -- book
			{id = 2796, chance = 12280}, -- green mushroom
			{id = {15744, 15745, 15746, 15747, 15749}, chance = 900, uniqueDrop = 1}, -- dragon set
			{id = 2672, count = 5, chance = 80200}, -- dragon ham
			{id = 2152, count = 20, chance = 41690}, -- silver
			{id = 2148, count = {65, 90}, chance = 95260}, -- coin
		}
	},
	
	{ name = 'Vurtak', file = 'Cirith/Bosses/Vurtak.xml', look = {type = 296},
		description = {'Vurtak', 'Vurtak'}, experience = 32000, health = 16000, healthMax = 16000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = -10 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 14351, chance = 64710, uniqueDrop = 1}, -- iron boots
			{id = 14350, chance = 4980}, -- iron legs
			{id = 2165, chance = 26200}, -- stealth ring
			{id = {14349, 14348}, chance = 8150}, -- iron helmet/armor
			{id = 15776, chance = 91600}, -- killer'blade
			{id = 15742, chance = 18200, uniqueDrop = 1}, -- sapphire ring
			{id = 2160, chance = 10000, uniqueDrop = 1}, -- gold coin
			{id = 2169, chance = 79800}, -- time ring
			{id = 2386, chance = 25900}, -- goblin's chopper
			{id = 17223, chance = 53450}, -- claw
			{id = 13888, chance = 4750, uniqueDrop = 1}, -- lightning axe
			{id = 2667, chance = 82500}, -- fish
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 18075, chance = 12100, uniqueDrop = 1}, -- purple dye
			{id = 14358, chance = 88700}, -- bronze axe
			{id = 2230, chance = 82950}, -- bone
			{id = 14343, chance = 35600, uniqueDrop = 1}, -- alpha shanir
			{id = {14353, 14354, 14355, 14357}, chance = 69890}, -- bronze set
			{id = 2152, count = 5, chance = 36500}, -- silver coin
			{id = 2148, count = 26, chance = 100000}, -- coin
		}
	},
	{ name = 'Vurtak Second', file = 'Cirith/Bosses/Vurtak Second.xml', look = {type = 296}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Vurtak', 'Vurtak'}, experience = 32000, health = 8000, healthMax = 16000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = -10 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 14351, chance = 64710, uniqueDrop = 1}, -- iron boots
			{id = 14350, chance = 4980}, -- iron legs
			{id = 2165, chance = 26200}, -- stealth ring
			{id = {14349, 14348}, chance = 8150}, -- iron helmet/armor
			{id = 15776, chance = 91600}, -- killer'blade
			{id = 15742, chance = 18200, uniqueDrop = 1}, -- sapphire ring
			{id = 2160, chance = 10000, uniqueDrop = 1}, -- gold coin
			{id = 2169, chance = 79800}, -- time ring
			{id = 2386, chance = 25900}, -- goblin's chopper
			{id = 17223, chance = 53450}, -- claw
			{id = 13888, chance = 4750, uniqueDrop = 1}, -- lightning axe
			{id = 2667, chance = 82500}, -- fish
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 18075, chance = 12100, uniqueDrop = 1}, -- purple dye
			{id = 14358, chance = 88700}, -- bronze axe
			{id = 2230, chance = 82950}, -- bone
			{id = 14343, chance = 35600, uniqueDrop = 1}, -- alpha shanir
			{id = {14353, 14354, 14355, 14357}, chance = 69890}, -- bronze set
			{id = 2152, count = 5, chance = 36500}, -- silver coin
			{id = 2148, count = 26, chance = 100000}, -- coin
		}
	},
	{ name = 'Dalamar Argent', file = 'Cirith/Bosses/Dalamar Argent.xml', look = {type = 63}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Dalamar Argent', 'Dalamar Argent'}, experience = 5000, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = 50 },
		},
		loot = {
			{id = 2154, chance = 8050}, -- yellow gem
			{id = 2747, chance = 7870}, -- gave flower
			{id = 15775, chance = 69950}, -- leather boots
			{id = 2177, chance = 6970}, -- life crystal
			{id = 2600, chance = 91010}, -- inkwell
			{id = 14459, chance = 9140}, -- wand of cosmic energy
			{id = 13981, chance = 20000, subType = 100}, -- amulet
			{id = {13906, 13907, 13908, 13909, 13911}, chance = 10590, uniqueDrop = 1}, -- Wradon's set
			{id = 5922, chance = 20070}, -- holy orchid
			{id = 7438, chance = 1770, uniqueDrop = 1}, -- elvish bow
			{id = 2047, chance = 32080}, -- candlestick
			{id = 7589, chance = 73000}, -- SMP
			{id = 7618, chance = 94040}, -- health potion
			{id = {13056, 13071, 13059, 13062}, chance = 500}, -- silver crafting additives
			{id = 2802, chance = 4940}, -- sling herb
			{id = 2544, count = 13, chance = 66050}, -- arrow
			{id = 12958, chance = 87040}, -- lampart boots
			{id = 10551, chance = 70110}, -- elvish talisman
			{id = 2689, chance = 53750}, -- bread
			{id = 2682, chance = 42380}, -- melon
			{id = 1949, chance = 60500}, -- scroll
			{id = 2148, count = {30, 70}, chance = 37060}, -- gold
		}
	},
	{ name = 'Corpse Eater', file = 'Cirith/Bosses/Corpse Eater.xml', look = {type = 311}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Po�eracz Zw�ok', 'Corpse Eater'}, experience = 4000, health = 4000, healthMax = 4000,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -45 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 40 },
		},
		loot = {
			{id = 15714, chance = 27480}, -- club
			{id = 15720, chance = 5000, uniqueDrop = 1}, -- broadsword
			{id = 15737, chance = 2500, uniqueDrop = 1}, -- bone bow
			{id = 15717, chance = 1000, uniqueDrop = 1}, -- glorious axe
			{id = 2145, chance = 51110}, -- small diamond
			{id = 17223, chance = 4900, uniqueDrop = 1}, -- claw
			{id = 15713, chance = 5250}, -- longsword
			{id = 2227, chance = 71880}, -- rotten meat
			{id = 2628, chance = 14500}, -- The Armor of Hardened Leather
			{id = 2168, chance = 92150, uniqueDrop = 1}, -- life ring
			{id = 2230, chance = 94830}, -- bone
			{id = {13056, 13071, 13059, 13062}, chance = 500}, -- silver crafting additives
			{id = 15776, chance = 6830}, -- killer's blade
			{id = 14012, chance = 4500}, -- crimson sword
			{id = 11194, chance = 94900}, -- half-digested piece of meat
			{id = 11404, chance = 1420, uniqueDrop = 1}, -- heavy crossbow
			{id = 3976, count = 10, chance = 98990}, -- worm
			{id = 2148, count = {30, 70}, chance = 97800}, -- gold
		}
	},
	{ name = 'Vincent Ceryni', file = 'Cirith/Bosses/Vincent Ceryni.xml', look = {type = 573, head = 0, body = 78, legs = 114, feet = 0}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Vincent Ceryni', 'Vincent Ceryni'}, experience = 75000, health = 37500, healthMax = 37500,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 30 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2165, chance = 21500}, -- stealth ring
			{id = {13906, 13907, 13908, 13909, 13911}, chance = 60590, uniqueDrop = 1}, -- Wradon's set
			{id = 15753, chance = 72450}, -- brass boots
			{id = 15752, chance = 5620}, -- brass legs
			{id = 18050, chance = 1000, uniqueDrop = 1}, -- green dye
			{id = {15750, 15751}, chance = 8240}, -- brass helmet/armor
			{id = 14358, chance = 27500}, -- bronze axe
			{id = 2160, chance = 500, uniqueDrop = 1}, -- gold coin
			{id = 10718, chance = 4280, uniqueDrop = 1}, -- Forest Whisper Necklace
			{id = 11404, chance = 40930, uniqueDrop = 1}, -- heavy crossbow
			{id = 17223, chance = 19760}, -- claw
			{id = 15720, chance = 39210}, -- broadsword
			{id = 14012, chance = 41250, uniqueDrop = 1}, -- crimson sword
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 13910, chance = 12500, uniqueDrop = 1}, -- wradon's wand
			{id = 17267, chance = 750}, -- doll
			{id = {14353, 14354, 14355, 14357}, chance = 68430}, -- bronze set
			{id = 13948, chance = 21150}, -- hunting spear
			{id = 15776, chance = 22870}, -- killer's blade
			{id = 7435, chance = 890}, -- impaler
			{id = 14343, chance = 67300, uniqueDrop = 1}, -- alpha shanir
			{id = {2235, 8368}, chance = 74520}, -- cheese
			{id = 2152, count = 5, chance = 81500}, -- silver coin
			{id = 2148, count = 11, chance = 95500}, -- copper
			{id = 17268, chance = 100000}, -- earflap
			{id = 17271, chance = 100000}, -- bola
		}
	},
	{ name = 'Analarak', file = 'Cirith/Bosses/Analarak.xml', look = {type = 521}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Analarak', 'Analarak'}, experience = 2000, health = 2000, healthMax = 2000,
		elements = {
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
		},
		loot = {
			{id = 11797, chance = 2500, uniqueDrop = 1}, -- buggy backpack
			{id = 8856, chance = 15000, uniqueDrop = 1}, -- beetlebow
			{id = 15738, chance = 40000}, -- long bow
			{id = 13948, chance = 29000}, -- hunting spear
			{id = 2260, chance = 10000}, -- rune
			{id = 2679, chance = 94870}, -- cherry
			{id = 14771, chance = 81980}, -- cure potion
			{id = 3976, count = 3, chance = 92090}, -- worm
			{id = 2148, count = 20, chance = 94980}, -- copper coin
		}
	},
	{ name = 'Arachnid', file = 'Cirith/Bosses/Arachnid.xml', look = {type = 519}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Arachnid', 'Arachnid'}, experience = 1900, health = 1900, healthMax = 1900,
		elements = {
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -15 },
		},
		loot = {
			{id = {14353, 14354, 14355, 14357}, chance = 35890, uniqueDrop = 1}, -- bronze set
			{id = 14015, chance = 75890, uniqueDrop = 1}, -- short bow
			{id = 2260, chance = 4000}, -- rune
			{id = 2545, count = 100, chance = 89100}, -- poison arrow
			{id = 14771, chance = 41980}, -- cure potion
			{id = 13948, chance = 8990}, -- hunting spear
			{id = 7887, chance = 1200, uniqueDrop = 1}, -- toxic amulet
			{id = 8859, chance = 89650}, -- spider fang
			{id = 11192, chance = 99700}, -- tarantula egg
		}
	},
	{ name = 'Azog', file = 'Cirith/Bosses/Azog.xml', look = {type = 298}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Azog', 'Azog'}, experience = 7000, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 5 },
		},
		loot = {
			{id = 15737, chance = 7200, uniqueDrop = 1}, -- bone bow
			{id = 15789, chance = 79190}, -- copper sword
			{id = 8849, chance = 3290, uniqueDrop = 1}, -- modified crossbow
			{id = 14333, chance = 34890}, -- runed sword
			{id = 15793, chance = 29080}, -- sword
			{id = 2429, chance = 1000, uniqueDrop = 1}, -- Beard cutter
			{id = 17866, chance = 29800}, -- mantreads
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 2152, chance = 91550}, -- silver coin
			{id = {16305, 16306, 16307, 16308}, chance = 16200, uniqueDrop = 1}, -- enhanced leather armor
			{id = 2120, chance = 21150}, -- rope
			{id = 13759, chance = 29990}, -- pelvis bone
			{id = 14015, chance = 15290}, -- short bow
			{id = 2148, count = 71, chance = 66760}, -- coin
			{id = 2230, chance = 91890}, -- bone
			{id = 2050, chance = 92340}, -- torch
		}
	},
	{ name = 'Baru\'khur Al\'s Akr', file = 'Cirith/Bosses/Baru\'khur Al\'s Akr.xml', look = {type = 189}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Baru\'khur Al\'s Akr', 'Baru\'khur Al\'s Akr'}, experience = 8000, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'energy', value = 15 },
			{ type = 'holy', value = 10 },
			{ type = 'earth', value = -10 },
			{ type = 'death', value = -10 },
			{ type = 'physical', value = 15 },
		},
		loot = {
			{id = {14353, 14354, 14355, 14357}, chance = 65190}, -- bronze set
			{id = 14356, chance = 390, uniqueDrop = 1}, -- bronze boots
			{id = 15714, chance = 98790}, -- club
			{id = 15713, chance = 25160}, -- longsword
			{id = 2120, chance = 99990}, -- rope
			{id = 17867, chance = 15000, uniqueDrop = 1}, -- spiky mace
			{id = 2050, chance = 73280}, -- torch
			{id = 13597, chance = 5000, uniqueDrop = 1}, -- ogre ring
			{id = 15772, chance = 96190}, -- leather helmet
			{id = 15773, chance = 81440}, -- leather armor
			{id = 2386, chance = 5900}, -- goblin's chopper
			{id = 15774, chance = 71900}, -- leather legs
			{id = 15775, chance = 66890}, -- leather boots
			{id = 15778, chance = 55900}, -- wooden shield
			{id = 2544, count = 100, chance = 94870}, -- arrow
			{id = 2666, chance = 76110}, -- meat
			{id = 15795, chance = 33000, uniqueDrop = 1}, -- zdobiony klucz
			{id = 10605, chance = 100000}, -- bunch of troll hair
			{id = 2148, count = 20, chance = 38790}, -- coin
		}
	},
	{ name = 'Bloody Skull', file = 'Cirith/Bosses/Bloody Skull.xml', look = {type = 561}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Krwawa Czaszka', 'Bloody Skull'}, experience = 7000, health = 3900, healthMax = 3900,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 80 },
		},
		loot = {
			{id = 11414, chance = 100, uniqueDrop = 1}, -- plushy orc
			{id = 14322, chance = 700, uniqueDrop = 1}, -- royal steel helmet
			{id = 14327, chance = 500, uniqueDrop = 1}, -- plate legs
			{id = 7618, chance = 5580}, -- health potion
			{id = 15723, chance = 1000, uniqueDrop = 1}, -- broadsword
			{id = 5887, chance = 900}, -- piece of royal steel
			{id = 15722, chance = 4190, uniqueDrop = 1}, -- scimitar
			{id = 15782, chance = 1030}, -- steel spear
			{id = 5889, chance = 9580}, -- piece of steel
			{id = 14348, chance = 900, uniqueDrop = 1}, -- iron armor
			{id = 14358, chance = 3500}, -- bronze axe
			{id = 14333, chance = 82190}, -- runed sword
			{id = 13758, chance = 22110}, -- skull belt
			{id = 5892, chance = 2000}, -- steel
			{id = 15713, chance = 8650}, -- longsword
			{id = 7395, chance = 490}, -- orc trophy
			{id = {14353, 14354, 14355, 14357}, chance = 13820}, -- bronze set
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 35190}, -- copper set
			{id = 16932, count = 10, chance = 95740}, -- health potion
			{id = 12963, chance = 60140}, -- lampart shield
			{id = 2209, chance = 84750}, -- accuracy ring
			{id = 13757, chance = 19680}, -- orc leather
			{id = 15776, chance = 17540}, -- killer's blade
			{id = 2410, count = 40, chance = 89000}, -- throwing knife
			{id = 2666, chance = 98020}, -- meat
			{id = 2667, chance = 89000}, -- fish
			{id = 2151, chance = 25000, uniqueDrop = 1}, -- talon
			{id = 2152, chance = 28900}, -- silver coin
			{id = 2148, count = 24, chance = 76890}, -- gold
		}
	},
	{ name = 'Bomber', file = 'Cirith/Bosses/Bomber.xml', look = {type = 233}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Bomber', 'Bomber'}, experience = 5250, health = 70000, healthMax = 70000,
		elements = {
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 40 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13978, chance = 2050, uniqueDrop = 1}, -- bloody crusher
			{id = 14482, chance = 3460, uniqueDrop = 1}, -- ancient helmet
			{id = 6300, chance = 4500}, -- death ring
			{id = 13886, chance = 21510}, -- haunted blade
			{id = 2197, chance = 22300, subType = 10}, -- stone skin amulet
			{id = 2143, count = 2, chance = 36770}, -- white pearl
			{id = 5944, chance = 39010}, -- soul orb
			{id = {13056, 13071, 13059, 13062}, chance = 25000}, -- silver crafting additives
			{id = 2145, count = 4, chance = 41340}, -- small diamond
			{id = 7591, count = 2, chance = 44820}, -- GHP
			{id = 7590, count = 3, chance = 46010}, -- GMP
			{id = 11227, chance = 48530}, -- unholy bone
			{id = 6500, chance = 49720}, -- demonic essence
			{id = 2547, count = 15, chance = 50870}, -- power bolt
			{id = 6558, chance = 5280}, -- concentrated demonic blood
			{id = 2152, count = 2, chance = 87950}, -- silver coin
			{id = 2148, count = 20, chance = 97680}, -- gold
		}
	},
	{ name = 'Callista', file = 'Cirith/Bosses/Callista.xml', look = {type = 78}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Callista', 'Callista'}, experience = 15000, health = 30000, healthMax = 30000,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
		},
		loot = {
			{id = 11404, chance = 2000, uniqueDrop = 1}, -- heavy crossbow
			{id = 2168, chance = 92150}, -- life ring
			{id = 2656, chance = 1000, uniqueDrop = 1}, -- blue robe
			{id = 2124, chance = 900}, -- crystal ring
			{id = 2177, chance = 2500}, -- life crystal
			{id = 2121, chance = 350, uniqueDrop = 1}, -- wedding ring
			{id = 2655, chance = 810}, -- red robe
			{id = 2123, chance = 500, uniqueDrop = 1}, -- ring with diamond
			{id = 2197, chance = 650, subType = 5}, -- ssa
			{id = 8900, chance = 1000, uniqueDrop = 1}, -- spellbook
			{id = {13056, 13071, 13059, 13062}, chance = 1000}, -- silver crafting additives
			{id = 16143, chance = 800, uniqueDrop = 1}, -- golden blade
			{id = 16933, count = 3, chance = 9770}, -- smp
			{id = 2214, chance = 59840}, -- roh
			{id = 2071, chance = 11060}, -- lyre
			{id = 15739, chance = 1050, uniqueDrop = 1}, -- necrotic bow
			{id = 2143, chance = 21190}, -- white pearl
			{id = 2134, chance = 8610}, -- silver brooch
			{id = 2144, chance = 22180}, -- black pearl
			{id = 11331, chance = 44440}, -- petrified scream
			{id = 14765, chance = 26630}, -- coat
			{id = 2170, chance = 19760, subType = 155}, -- silver amulet
			{id = 2148, count = 11, chance = 32180}, -- gold
			{id = 2047, chance = 75860}, -- candlestick
		}
	},
	{ name = 'Caramon Majere', file = 'Cirith/Bosses/Caramon Majere.xml', look = {type = 462, addons = 1}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Caramon Majere', 'Caramon Majere'}, experience = 6500, health = 75000, healthMax = 75000,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 30 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13998, chance = 500, uniqueDrop = 1}, -- master warrior's armor
			{id = 2391, chance = 760, uniqueDrop = 1}, -- war hammer
			{id = 1985, chance = 1000}, -- grey tome
			{id = 2440, chance = 1450, uniqueDrop = 1}, -- daramanian axe
			{id = 2110, chance = 1780}, -- doll
			{id = 9976, chance = 1870}, -- crystal pedestal
			{id = 11395, chance = 1990, uniqueDrop = 1}, -- knight backpack
			{id = 14443, chance = 2000}, -- spike shield
			{id = {13056, 13071, 13059, 13062}, chance = 25000}, -- silver crafting additives
			{id = 13997, chance = 3500, uniqueDrop = 1}, -- warrior's armor
			{id = 2573, chance = 4000}, -- bowl
			{id = 2151, count = 3, chance = 5000}, -- talon
			{id = 13896, chance = 6580}, -- giant sword
			{id = 14779, chance = 15460}, -- knife
			{id = 7591, count = 4, chance = 18980}, -- GHP
			{id = 8473, count = 3, chance = 19090}, -- UHP
			{id = 13589, chance = 10000, uniqueDrop = 1}, -- bastard axe
			{id = 2152, count = 2, chance = 78900}, -- silver
			{id = 2148, count = 9, chance = 97270}, -- gold
		}
	},
	{ name = 'Gaspar Louvain', file = 'Cirith/Bosses/Gaspar Louvain.xml', look = {type = 234}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Gaspar Louvain', 'Gaspar Louvain'}, experience = 4650, health = 130000, healthMax = 130000,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 6300, chance = 2820}, -- death ring
			{id = 14320, chance = 5110, uniqueDrop = 1}, -- royal steel boots
			{id = 5944, chance = 9860}, -- soul orb
			{id = 6500, chance = 5630}, -- demonic rssence
			{id = 2558, chance = 2330}, -- saw
			{id = 7388, chance = 2820}, -- vile axe
			{id = 2470, chance = 450, uniqueDrop = 1}, -- golden legs
			{id = 7412, chance = 730, uniqueDrop = 1}, -- butcher's axe
			{id = 5022, chance = 1410}, -- orichalcum pearl
			{id = {13056, 13071, 13059, 13062}, chance = 25000}, -- silver crafting additives
			{id = 5480, chance = 1410, uniqueDrop = 1}, -- cat's paw
			{id = 6558, chance = 9340}, -- concentrated demonic blood
			{id = 6558, chance = 6210}, -- concentrated demonic blood
			{id = 6558, chance = 3860}, -- concentrated demonic blood
			{id = 2671, chance = 63380}, -- ham
			{id = 2152, count = 2, chance = 19300}, -- silver coin
			{id = 2148, count = 12, chance = 32510}, -- gold
		}
	},
	{ name = 'Gloryblood', file = 'Cirith/Bosses/Gloryblood.xml', look = {type = 312}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Gloryblood', 'Gloryblood'}, experience = 5000, health = 8600, healthMax = 8600,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 50 },
		},
		loot = {
			
		}
	},
	{ name = 'Gorgone', file = 'Cirith/Bosses/Gorgone.xml', look = {type = 330}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Meduza', 'Medusa'}, experience = 4500, health = 95000, healthMax = 95000,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'energy', value = -4 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -6 },
			{ type = 'ice', value = 25 },
		},
		loot = {
			{id = 8857, chance = 2500, uniqueDrop = 1}, -- silkweaver bow
			{id = 16196, chance = 1500, uniqueDrop = 1}, -- druid legs
			{id = 15749, chance = 620, uniqueDrop = 1}, -- dragon executor
			{id = 13940, chance = 6990}, -- terra crossbow
			{id = 15761, chance = 9510}, -- snake blade
			{id = 13979, chance = 13160}, -- crusher
			{id = {13056, 13071, 13059, 13062}, chance = 50000}, -- silver crafting additives
			{id = 2536, chance = 13510}, -- medusa shield
			{id = 15757, chance = 14410}, -- snake legs
			{id = 14010, chance = 15870}, -- knight armor
			{id = 10219, chance = 27820, subType = 20}, -- sacred tree amulet
			{id = 15756, chance = 38860}, -- snake armor
			{id = 13980, chance = 49090, subType = 400}, -- terra amulet
			{id = 8473, count = 5, chance = 69360}, -- UHP
			{id = 11220, chance = 69900}, -- strand of medusa hair
			{id = 7590, count = 5, chance = 70160}, -- GMP
			{id = 2149, count = 10, chance = 73780}, -- small emerald
			{id = 2152, count = 2, chance = 74840}, -- plat coin
			{id = {16165, 16166, 16167, 16168}, chance = 89600}, -- swamplair set
			{id = 2148, count = 11, chance = 97680}, -- gold
		}
	},
	{ name = 'Hammer', file = 'Cirith/Bosses/Hammer.xml', look = {type = 444}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'M�ot', 'Hammer'}, experience = 3750, health = 60000, healthMax = 60000,
		elements = {
			{ type = 'holy', value = 30 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13953, chance = 1000, uniqueDrop = 1}, -- the fallen horse
			{id = 15732, chance = 20050}, -- knight axe
			{id = 15729, chance = 40080}, -- double axe
			{id = 2023, chance = 100}, -- amphora
			{id = 13978, chance = 5380, uniqueDrop = 1}, -- Bloody crusher
			{id = 14320, chance = 5430, uniqueDrop = 1}, -- plate boots
			{id = 2553, chance = 560}, -- pick
			{id = 2231, chance = 65750}, -- big bone
			{id = 2174, chance = 88870}, -- strange symbol
			{id = {13056, 13071, 13059, 13062}, chance = 40000}, -- silver crafting additives
			{id = 15754, chance = 92550, subType = 1000}, -- brass amulet
			{id = 14348, chance = 34140}, -- iron armor
			{id = 14001, chance = 24200}, -- dark armor
			{id = 7591, chance = 45070}, -- GHP
			{id = 15723, chance = 15960}, -- two handed sword
			{id = 2150, count = {5, 12}, chance = 86430}, -- small amethyst
			{id = 15719, chance = 60350}, -- battle hammer
			{id = 13734, chance = 74580}, -- battle stone
			{id = 2666, count = 6, chance = 29820}, -- meat
			{id = 2152, count = {2, 9}, chance = 59680}, -- silver
			{id = 2148, count = {9, 90}, chance = 99570}, -- gold
		}
	},
	{ name = 'Hepaestus', file = 'Cirith/Bosses/Hepaestus.xml', look = {type = 34}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Hepaestus', 'Hepaestus'}, experience = 5000, health = 30000, healthMax = 30000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13951, chance = 400, uniqueDrop = 1}, -- hellfire spear
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 51190}, -- copper set
			{id = 2177, chance = 3250}, -- life crystal
			{id = 13987, chance = 200, uniqueDrop = 1}, -- dragon scale mail
			{id = 15721, chance = 1680, uniqueDrop = 1}, -- ragged sword
			{id = 14350, chance = 2400}, -- iron legs
			{id = 2187, chance = 2290, uniqueDrop = 1}, -- woi
			{id = 14478, chance = 2380}, -- berynit helmet
			{id = 15791, chance = 3600, subType = 500}, -- amber eye
			{id = 13342, chance = 5300, uniqueDrop = 1}, -- dragon claw
			{id = 2145, chance = 9200}, -- small diamond
			{id = {14353, 14354, 14355, 14357}, chance = 5320}, -- bronze set
			{id = 5877, chance = 1970}, -- green dragon leather
			{id = 17133, chance = 350}, -- green dragon wing
			{id = 15717, chance = 1160}, -- glorious axe
			{id = 14447, chance = 160, uniqueDrop = 1}, -- WoI
			{id = 14484, chance = 2160}, -- guard's helmet
			{id = {13056, 13071, 13059, 13062}, chance = 2500}, -- silver crafting additives
			{id = 7588, chance = 1040}, -- SHP
			{id = 14358, chance = 1220}, -- bronze axe
			{id = 2546, count = 9, chance = 10000}, -- burst arrow
			{id = 5920, chance = 2050}, -- green dragon scale
			{id = 2455, chance = 10050}, -- crossbow
			{id = 2672, count = 3, chance = 65050}, -- dragon ham
			{id = 2148, count = {20, 47}, chance = 90160}, -- coin
			{id = 15715, chance = 3200, uniqueDrop = 1}, -- basher
			{id = 14478, chance = 5390, uniqueDrop = 1}, -- berynit helmet
			{id = 14333, chance = 4070}, -- runed sword
		}
	},
	{ name = 'Huma Dragonbane', file = 'Cirith/Bosses/Huma Dragonbane.xml', look = {type = 355}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Huma Dragonbane', 'Huma Dragonbane'}, experience = 38000, health = 35000, healthMax = 35000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 2114, chance = 660}, -- piggy bank
			{id = 13941, chance = 8100, uniqueDrop = 1}, -- royal steel crossbow
			{id = 14319, chance = 9150, uniqueDrop = 1}, -- royal steel shield
			{id = 14322, chance = 34220, uniqueDrop = 1}, -- royal steel helmet
			{id = 2392, chance = 24500, uniqueDrop = 1}, -- fire sword
			{id = 2164, chance = 55510, subType = 20}, -- might ring
			{id = 14323, chance = 10340, uniqueDrop = 1}, -- royal steel armor
			{id = 14321, chance = 9440, uniqueDrop = 1}, -- royal steel legs
			{id = 7591, chance = 61820}, -- GHP
			{id = 15724, chance = 5140, uniqueDrop = 1}, -- berserker
			{id = 13904, chance = 750, uniqueDrop = 1}, -- hero shield
			{id = 2661, chance = 71010}, -- scarf
			{id = 15723, chance = 19880}, -- two handed sword
			{id = 15745, chance = 800, uniqueDrop = 1}, -- dragon legs
			{id = 2071, chance = 71520}, -- lyre
			{id = 5911, chance = 71930}, -- red poc
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 2120, chance = 42060}, -- rope
			{id = 16431, chance = 1000, uniqueDrop = 1}, -- black forest bow
			{id = 2121, chance = 5010}, -- wedding ring
			{id = 13899, chance = 590}, -- bastard sword
			{id = 14765, chance = 78020}, -- cloak
			{id = 2666, chance = 88370}, -- meat
			{id = 7364, count = 49, chance = 91770}, -- sniper arrow
			{id = 2456, chance = 93200}, -- bow
			{id = 2681, chance = 90210}, -- grapes
			{id = 2744, chance = 80600}, -- red rose
			{id = 2544, count = 13, chance = 26710}, -- arrow
			{id = 1949, chance = 45030}, -- scroll
			{id = 2152, count = 2, chance = 69720}, -- silver
			{id = 2148, count = 11, chance = 89720}, -- gold
		}
	},
	{ name = 'Kaz', file = 'Cirith/Bosses/Kaz.xml', look = {type = 418, head = 78, body = 76, legs = 94, feet = 97}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Kaz', 'Kaz'}, experience = 8000, health = 45000, healthMax = 45000,
		elements = {
			{ type = 'physical', value = 1 },
			{ type = 'death', value = 15 },
			{ type = 'energy', value = 3 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 7401, chance = 130}, -- mino trophy
			{id = 14010, chance = 15000, uniqueDrop = 1}, -- knight armor
			{id = {13056, 13071, 13059, 13062}, chance = 15000}, -- silver crafting additives
			{id = 14321, chance = 15510, uniqueDrop = 1}, -- royal steel legs
			{id = 5911, chance = 540}, -- red poc
			{id = 14319, chance = 600}, -- steel shield
			{id = 14171, chance = 790}, -- berynit armor
			{id = 2149, chance = 1040}, -- iron sword
			{id = 9970, chance = 2030}, -- small topaz
			{id = 2147, chance = 2080}, -- small ruby
			{id = 2150, chance = 2120}, -- small amethyst
			{id = 5878, chance = 5180}, -- mino leather
			{id = 2149, chance = 2190}, -- small emerald
			{id = 7588, chance = 6950}, -- SHP
			{id = 7589, chance = 7070}, -- SMP
			{id = 2152, count = 2, chance = 39990}, -- silver
			{id = 2148, count = 20, chance = 99890}, -- coin
		}
	},
	{ name = 'Kharis-O\'Athak', file = 'Cirith/Bosses/Kharis-O\'Athak.xml', look = {type = 463}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Kharis-O\'Athak', 'Kharis-O\'Athak'}, experience = 19000, health = 23600, healthMax = 23600,
		elements = {
			{ type = 'physical', value = 1 },
			{ type = 'energy', value = 3 },
			{ type = 'earth', value = 10 },
			{ type = 'ice', value = 10 },
			{ type = 'death', value = 15 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 7401, chance = 2500}, -- mino trophy
			{id = 14010, chance = 1200, uniqueDrop = 1}, -- knight armor
			{id = 14321, chance = 910, uniqueDrop = 1}, -- royal steel legs
			{id = 14319, chance = 4200}, -- steel shield
			{id = 14171, chance = 3290}, -- berynit armor
			{id = 14352, chance = 4240}, -- iron sword
			{id = 7589, chance = 11070}, -- SMP
			{id = 7588, chance = 21950}, -- SHP
			{id = {13056, 13071, 13059, 13062}, chance = 4000}, -- silver crafting additives
			{id = 14348, chance = 800}, -- iron armor
			{id = 14350, chance = 1310}, -- iron legs
			{id = 14171, chance = 2590}, -- breastplate
			{id = 14333, chance = 6040}, -- runed sword
			{id = 5911, chance = 540}, -- red poc
			{id = 14319, chance = 600}, -- steel shield
			{id = 16933, count = 3, chance = 1070}, -- SMP
			{id = 16932, count = 2, chance = 1950}, -- SHP
			{id = 9970, chance = 2030}, -- small topaz
			{id = 2147, chance = 2080}, -- small ruby
			{id = 2150, chance = 2120}, -- small amethyst
			{id = 5878, chance = 5180}, -- mino leather
			{id = 2149, chance = 2190}, -- small emerald
			{id = 2152, chance = 43990}, -- silver
			{id = 2148, count = 14, chance = 99890}, -- coin
		}
	},
	{ name = 'Mimic Chaeck', file = 'Cirith/Bosses/Mimic Chaeck.xml', look = {type = 523}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Mimik', 'Mimic'}, experience = 45000, health = 8000, healthMax = 8000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 80 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 5 },
		},
		loot = {
			{id = 16435, chance = 500, uniqueDrop = 1}, -- bal gown
			{id = 13890, chance = 1500, uniqueDrop = 1}, -- bow of the miser
			{id = 16269, chance = 2000, uniqueDrop = 1}, -- crystal spike backpack
			{id = 14006, chance = 6500, uniqueDrop = 1}, -- cape of fear
			{id = 16305, chance = 25700}, -- enhanced leather hood
			{id = 12964, chance = 7000}, --lunar crystal 
			{id = {11258, 9837}, chance = 67250}, -- dragon/velvet tapestry
			{id = 14358, chance = 41250}, -- bronze axe
			{id = {2143, 2144}, count = 2, chance = 92580}, -- pearls
			{id = 2145, count = 3, chance = 53140}, -- small diamond
			{id = {2146, 2147, 2149, 2150}, count = 5, chance = 55600}, -- small crystals
			{id = 2152, count = 5, chance = 99400}, -- talon
			{id = 2120, chance = 21150}, -- rope
			{id = {2153, 2154, 2155, 2156, 2158}, chance = 25000}, -- gem
			{id = 5889, count = 2, chance = 91320}, -- piece of steel
			{id = 8309, count = 5, chance = 25890}, -- nail
			{id = 2050, chance = 42340}, -- torch
			{id = 14014, chance = 56350}, -- robe
			{id = 14765, chance = 61900}, -- cape
			{id = 2560, chance = 92340}, -- mirror
			{id = 2152, count = 2, chance = 41690}, -- silver
		},
		loot = {
			{id = 10521, chance = 70, uniqueDrop = 1}, -- jewelled backpack
			{id = 2171, chance = 110}, -- platinum amulet
			{id = 13894, chance = 140, uniqueDrop = 1}, -- frosty rapier
			{id = 14010, chance = 570}, -- knight armor
			{id = 14486, chance = 660}, -- glacier hood
			{id = 7290, chance = 750}, -- shard
			{id = 13299, chance = 13150}, -- glass spear
			{id = {13056, 13071, 13059, 13062}, chance = 25000}, -- silver crafting additives
			{id = 14321, chance = 760, uniqueDrop = 1}, -- royal steel legs
			{id = 2169, chance = 1530}, -- time ring
			{id = 2124, chance = 1580}, -- crystal ring
			{id = 2125, chance = 1660}, -- crystal necklace
			{id = 5879, chance = 1960}, -- GSS
			{id = 15720, chance = 2530}, -- broad sword
			{id = 14362, chance = 2980}, -- plate armor
			{id = 7441, chance = 4930}, -- ice cube
			{id = 14478, chance = 5160}, -- steel helmet
			{id = 7364, count = 6, chance = 5850}, -- sniper arrows
			{id = 7589, chance = 15010}, -- SMP
			{id = 2148, count = 11, chance = 99990}, -- coin
		}
	},
	{ name = 'Nemen\'akh', file = 'Cirith/Bosses/Nemen\'akh.xml', look = {type = 38}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Nemen\'akh', 'Nemen\'akh'}, experience = 5750, health = 39000, healthMax = 39000,
		elements = {
			{ type = 'earth', value = 100 },
			{ type = 'ice', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 8859, chance = 96970}, -- spider fang
			{id = 15752, chance = 98330}, -- brass legs
			{id = 14010, chance = 850, uniqueDrop = 1}, -- knight armor
			{id = 15713, chance = 75250}, -- longsword
			{id = 11804, chance = 1000, uniqueDrop = 1}, -- herbalist bp
			{id = 13924, chance = 1500, uniqueDrop = 1}, -- light crossbow
			{id = 14327, chance = 5140, uniqueDrop = 1}, -- plate legs
			{id = 7588, chance = 93460}, -- SHP
			{id = 14170, chance = 4720}, -- armorour legionnaires
			{id = 14478, chance = 48810}, -- steel helmet
			{id = 15718, chance = 1250}, -- chopper
			{id = 12952, chance = 2810}, -- bronze helmet
			{id = 13986, chance = 3540, uniqueDrop = 1}, -- amber armor
			{id = {13056, 13071, 13059, 13062}, chance = 5000}, -- silver crafting additives
			{id = 2171, chance = 1300}, -- amulet
			{id = 14362, chance = 4000, uniqueDrop = 1}, -- plate armor
			{id = 15609, chance = 1490}, -- massive ornamented bow
			{id = 16932, count = 3, chance = 3460}, -- SHP
			{id = 14355, chance = 6640}, -- bronze legs
			{id = 2545, count = 12, chance = 12160}, -- poison arrows
			{id = 2152, count = {3, 5}, chance = 90650}, -- silver
			{id = 2148, count = {62, 98}, chance = 78690}, -- coin
		}
	},
	{ name = 'Trapped Ogre', file = 'Cirith/Bosses/Ogr.xml', look = {type = 477}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Uwi�ziony Ogr', 'Trapped Ogre'}, experience = 20000, health = 15000, healthMax = 15000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 15 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 15780, chance = 90000, subType = 1000}, -- zebiasty wisior
			{id = {14353, 14354, 14355, 14357}, chance = 55980}, -- bronze set
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 86990}, -- copper set
			{id = {16305, 16306, 16307, 16308}, chance = 25180}, -- enhanced leather armor
			{id = {16244, 16245, 16248}, chance = 25000, uniqueDrop = 1}, -- addons
			{id = 16932, count = 25, chance = 99000}, -- health potion
			{id = 2260, chance = 80000, uniqueDrop = 1}, -- rune
			{id = 15782, chance = 20050}, -- steel spear
			{id = 2667, count = 2, chance = 19800}, -- fish
			{id = 14175, count = 5, chance = 68230}, -- coal
			{id = 10608, count = 3, chance = 46920}, -- lump of dirt
			{id = 2152, count = 2, chance = 43620}, -- silver
			{id = 2148, count = 20, chance = 83890}, -- coin
		}
	},
	{ name = 'Oldymer', file = 'Cirith/Bosses/Oldymer.xml', look = {type = 261}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Oldymer', 'Oldymer'}, experience = 9990, health = 22250, healthMax = 22250,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 17704, chance = 1500}, -- the frosty midnight fire
		}
	},
	{ name = 'Grash-Varrag-Arushat', file = 'Cirith/Bosses/Grash-Varrag-Arushat.xml', look = {type = 6}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Grash-Varrag-Arushat', 'Grash-Varrag-Arushat'}, experience = 3000, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 14006, chance = 46580}, -- cape of fear
			{id = {16305, 16306, 16307, 16308}, chance = 21500}, -- enhanced leather armor
			{id = 16356, chance = 2000}, -- backpack crate
			{id = 1958, chance = 470}, -- grey book
			{id = 2190, chance = 5000, uniqueDrop = 1}, -- thunder rod
			{id = 13948, chance = 11870}, -- hunting spear
			{id = 13757, chance = 4000}, -- orc leather
			{id = 15776, chance = 23000, uniqueDrop = 1}, -- killer's blade
			{id = 13739, chance = 90200}, -- broken shamanic staff
			{id = 2686, count = 2, chance = 100000}, -- corncob
			{id = 2151, chance = 25000}, -- talon
			{id = 16933, count = 4, chance = 27590}, -- light mana potion
			{id = 2148, count = 6, chance = 89880}, -- coin
		}
	},
	{ name = 'Perfect Brain', file = 'Cirith/Bosses/Perfect Brain.xml', look = {type = 256}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Ismail Vilehand', 'Ismail Vilehand'}, experience = 4900, health = 50000, healthMax = 50000,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 80 },
		},
		loot = {
			{id = 15732, chance = 2800}, -- knight axe
			{id = 15757, chance = 3100, uniqueDrop = 1}, -- snake legs
			{id = 13795, chance = 1600, uniqueDrop = 1}, -- bonelord hood
			{id = 2155, chance = 7100}, -- green gem
			{id = 18076, chance = 1000}, -- grey dye
			{id = 8900, chance = 8000}, -- spellbook
			{id = 13896, chance = 13900}, -- giant sword
			{id = 15715, chance = 21700}, -- mace
			{id = {13056, 13071, 13059, 13062}, chance = 15000}, -- silver crafting additives
			{id = 2144, count = 2, chance = 31500}, -- black pearl
			{id = 5898, chance = 33300}, -- beholder eye
			{id = 10579, chance = 45900}, -- piece of dead brain
			{id = 15784, chance = 59500}, -- copper shield
			{id = 7364, count = 4, chance = 88800}, -- sniper arrow
			{id = 15779, chance = 55420}, -- skuller
			{id = 2148, count = 29, chance = 99650}, -- gold
		}
	},
	{ name = 'Raistlin Majere', file = 'Cirith/Bosses/Raistlin Majere.xml', look = {type = 489, head = 0, body = 94, legs = 94, feet = 94, addons = 2}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Raistlin Majere', 'Raistlin Majere'}, experience = 4190, health = 30000, healthMax = 30000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2539, chance = 500, uniqueDrop = 1}, -- phoenix shield
			{id = 9980, chance = 900}, -- piggy bank
			{id = 16202, chance = 1000, uniqueDrop = 1}, -- hellfire armor
			{id = 13939, chance = 1250}, -- hellfire crossbow
			{id = 1985, chance = 1270}, -- grey tome
			{id = 16203, chance = 1500, uniqueDrop = 1}, -- hellfire helmet
			{id = 16174, chance = 510, uniqueDrop = 1}, -- ruby spear
			{id = {13056, 13071, 13059, 13062}, chance = 15000}, -- silver crafting additives
			{id = 8902, chance = 3480}, -- spellbook of mind control
			{id = {12965, 12966, 12967, 12968}, chance = 3500}, -- fiery lunad crystal
			{id = 16143, chance = 4560}, -- golden blade
			{id = 9971, chance = 5000}, -- gold ingot
			{id = 10519, chance = 6250}, -- orange backpack
			{id = 2167, chance = 8880}, -- energy ring
			{id = 14449, chance = 9780}, -- wand of draconia
			{id = 5911, chance = 14500}, -- red poc
			{id = 7590, count = 4, chance = 18980}, -- GMP
			{id = 8472, count = 3, chance = 19090}, -- UMP
			{id = 13588, chance = 10000}, -- fire axe
			{id = 2152, count = 2, chance = 78900}, -- silver
			{id = 2148, count = 19, chance = 97270}, -- gold
		}
	},
	{ name = 'Rattus', file = 'Cirith/Bosses/Rattus.xml', look = {type = 520}, classId = 11, killAmount = 15, charmPoints = 5,
		description = {'Rattus', 'Rattus'}, experience = 1000, health = 400, healthMax = 400,
		elements = {
			{ type = 'energy', value = 50 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2260, chance = 5000}, -- rune
			{id = 2687, count = 2, chance = 7480}, -- cookie
			{id = 2628, chance = 20000, uniqueDrop = 1}, -- The Armor of Hardened Leather
			{id = 15776, chance = 8000, uniqueDrop = 1}, -- killer's blade
			{id = {12958, 12961}, chance = 91420}, -- lampart boots/helmet
			{id = 14779, chance = 8700}, -- knife
			{id = 17224, chance = 5000, uniqueDrop = 1}, -- rat tusk
			{id = 3976, count = 2, chance = 86900}, -- worm
			{id = 8368, count = 2, chance = 23480}, -- cheese
			{id = 2148, count = 3, chance = 47800}, -- coin
		}
	},
	{ name = 'Scaar', file = 'Cirith/Bosses/Scaar.xml', look = {type = 346}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Scaar', 'Scaar'}, experience = 4500, health = 15000, healthMax = 15000,
		loot = {
			{id = 15718, chance = 250}, -- bronze axe
			{id = 15786, chance = 350}, -- miedziany pancerz
			{id = 2110, chance = 470}, -- doll
			{id = 15784, chance = 940}, -- miedziana tarcza
			{id = 15752, chance = 640}, -- brass legs
			{id = 15753, chance = 1030}, -- brass boots
			{id = 15774, chance = 6890}, -- skorzane spodnie
			{id = {13056, 13071, 13059, 13062}, chance = 15000}, -- silver crafting additives
			{id = 14336, chance = 8290}, -- hood
			{id = 2695, count = 2, chance = 9820}, -- egg
			{id = 15783, chance = 9980}, -- rebacz
			{id = 12963, chance = 17050}, -- lampart shield
			{id = 2691, chance = 29800}, -- bread
			{id = 15779, chance = 30630}, -- skuller
			{id = 2148, count = 21, chance = 49230}, -- coin
		}
	},
	{ name = 'Skullcrusher', file = 'Cirith/Bosses/Skullcrusher.xml', look = {type = 414}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Skullcrusher', 'Skullcrusher'}, experience = 50000, health = 21000, healthMax = 21000,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 35 },
			{ type = 'ice', value = 50 },
			{ type = 'energy', value = 15 },
		},
		loot = {
			{id = 982, chance = 2000, uniqueDrop = 1}, -- dark talon
			{id = 14455, chance = 1120}, -- woce
			{id = 6300, chance = 5250}, -- death ring
			{id = 15734, chance = 1000, uniqueDrop = 1}, -- vile axe
			{id = 15740, chance = 9310, uniqueDrop = 1}, -- skull bow
			{id = 2209, chance = 4750}, -- accuracy ring
			{id = 16145, chance = 1500, uniqueDrop = 1}, -- kilt of the fallen
			{id = 14318, chance = 3530, uniqueDrop = 1}, -- royal steel blade
			{id = 2197, chance = 9570, subType = 8}, -- ssa
			{id = 14778, chance = 9690}, -- debowa laska
			{id = 2319, chance = 9780, subType = 150}, -- strange symbol
			{id = 2151, chance = 99970}, -- talon
			{id = 2124, chance = 52470}, -- crystal ring
			{id = 2164, chance = 53960, subType = 25}, -- might ring
			{id = 13759, chance = 94870}, -- pelvis bone
			{id = 2145, count = {3, 5}, chance = 64920}, -- small diamond
			{id = 15779, chance = 69210}, -- skuller
			{id = 2050, chance = 69800}, -- torch
			{id = 2789, count = 3, chance = 69930}, -- brown mushroom
			{id = 16933, count = 4, chance = 79910}, -- SMP
			{id = 16932, count = 4, chance = 79930}, -- SHP
			{id = 2230, chance = 87900}, -- bone
			{id = 2152, count = {5, 10}, chance = 91690}, -- silver
			{id = 2148, count = 27, chance = 99800}, -- coin
		}
	},
	{ name = 'Spitfire', file = 'Cirith/Bosses/Spitfire.xml', look = {type = 515}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Spitfire', 'Spitfire'}, experience = 150000, health = 400000, healthMax = 400000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 65 },
			{ type = 'earth', value = 95 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2539, chance = 5000, uniqueDrop = 1}, -- phoenix shield
			{id = 13897, chance = 2000, uniqueDrop = 1}, -- fire hammer
			{id = 13893, chance = 8000, uniqueDrop = 1}, -- axe of hellstorm
			{id = 16141, chance = 13330, uniqueDrop = 1}, -- golden bow
			{id = 13994, chance = 23410, uniqueDrop = 1}, -- magma armor
			{id = 13939, chance = 53450}, -- hellfire crossbow
			{id = {12965, 12966, 12967, 12968}, chance = 3500}, -- fiery lunad crystal
			{id = 13995, chance = 54000, uniqueDrop = 1}, -- lavos armor
			{id = 13796, chance = 10500}, -- silver shield
			{id = {10519, 10522}, chance = 16250}, -- orange/crown backpack
			{id = 16204, chance = 6780, uniqueDrop = 1}, -- hellfire shield
			{id = {13056, 13071, 13059, 13062}, chance = 100000}, -- silver crafting additives
			{id = 16201, chance = 18000, uniqueDrop = 1}, -- hellfire legs
			{id = 2187, chance = 90900}, -- woi
			{id = 10218, chance = 14500, subType = 10}, -- hellfire lavos amulet
			{id = 14353, chance = 45900, uniqueDrop = 1}, -- bronze armor
			{id = 16200, chance = 97690}, -- hellfire boots
			{id = 13588, chance = 50000, uniqueDrop = 1}, -- fire axe
			{id = 2136, chance = 58800}, -- Demonbone Amulet
			{id = 2147, count = {22, 30}, chance = 61450}, -- small ruby
			{id = 2144, count = {9, 20}, chance = 63450}, -- black pearl
			{id = 10580, chance = 94920}, -- piece of hellfire armor
			{id = 2156, chance = 95490}, -- red gem
			{id = 6500, chance = 94990}, -- demonic essence
			{id = 10552, chance = 99930}, -- fierly heart
			{id = 2152, count = {15, 50}, chance = 78900}, -- silver
			{id = 2148, count = {35, 90}, chance = 100000}, -- gold
		}
	},
	{ name = 'Tezohr', file = 'Cirith/Bosses/Tezohr.xml', look = {type = 29}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Tezohr', 'Tezohr'}, experience = 5000, health = 2200, healthMax = 2200,
		loot = {
			{id = {14353, 14354, 14355, 14357}, chance = 68750}, -- bronze set
			{id = 15713, chance = 33290}, -- longsword
			{id = 15789, chance = 88500}, -- copper sword
			{id = 7401, chance = 660}, -- trophy
			{id = 2532, chance = 10000, uniqueDrop = 1}, -- legs of the minotaur's skin
			{id = 15790, chance = 88790}, -- copper axe
			{id = 15793, chance = 92160}, -- sword
			{id = 14478, chance = 12000, uniqueDrop = 1}, -- berynit helmet
			{id = 14170, chance = 7160, uniqueDrop = 1}, -- legionnaires armor
			{id = 11404, chance = 8700, uniqueDrop = 1}, -- heavy crossbow
			{id = 14358, chance = 53590}, -- bronze axe
			{id = 2050, chance = 61750}, -- torch
			{id = 5878, chance = 100000}, -- mino leather
			{id = 2554, chance = 75290}, -- shovel
			{id = 7618, count = 15, chance = 89570}, -- health potion
			{id = 2152, count = 2, chance = 12650}, -- silver
			{id = 2148, count = 8, chance = 78690}, -- coin
		}
	},
	{ name = 'Thorn', file = 'Cirith/Bosses/Thorn.xml', look = {type = 554, head = 58, body = 78, legs = 114, feet = 9}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Thorn', 'Thorn'}, experience = 10000, health = 10000, healthMax = 10000,
		loot = {
			{id = 13997, chance = 500, uniqueDrop = 1}, -- master warrior's armor
			{id = 13997, chance = 3000, uniqueDrop = 1}, -- warrior's armor
			{id = 14320, chance = 10000, uniqueDrop = 1}, -- royal steel boots
			{id = 14010, chance = 17590}, -- knight armor
			{id = 15788, chance = 24990}, -- miedziane buty
			{id = {13056, 13071, 13059, 13062}, chance = 60000}, -- silver crafting additives
			{id = 11395, chance = 25000, uniqueDrop = 1}, -- knight backpack
			{id = 14348, chance = 58040}, -- iron plate
			{id = 2691, chance = 61530}, -- bread
			{id = 15776, chance = 86000}, -- krotki sword
			{id = 2666, chance = 98900}, -- meat
			{id = 15779, chance = 91540}, -- skuller
			{id = 2152, count = 2, chance = 91650}, -- coin
			{id = 2148, count = 13, chance = 78690}, -- coin
		}
	},
	{ name = 'Uruk', file = 'Cirith/Bosses/Uruk.xml', look = {type = 560}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Uruk', 'Uruk'}, experience = 2500, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'holy', value = 15 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -5 },
		},
		loot = {
			{id = 2260, chance = 3000, uniqueDrop = 1}, -- rune
			{id = 5889, chance = 2500}, -- piece of steel
			{id = 13757, chance = 77470}, -- orc skin
			{id = {13055, 13070, 13058, 13061}, chance = 3000}, -- bronze crafting additives
			{id = {7618, 16932}, count = 5, chance = 89180}, -- health potion
			{id = 2151, chance = 100000, uniqueDrop = 1}, -- talon
			{id = 2667, chance = 80870}, -- fish
			{id = 17866, chance = 2500}, -- mantreads
			{id = {14353, 14354, 14355, 14357}, chance = 3000}, -- bronze set
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 12000}, -- copper set
			{id = 15753, chance = 11550}, -- brass boots
			{id = {15752, 15751}, chance = 14980}, -- brass legs/armor
			{id = 15750, chance = 27590}, -- brass helmet
			{id = 15783, chance = 24290}, -- breaker
			{id = 12963, chance = 90900}, -- lampart shield
			{id = 2410, count = 12, chance = 42670}, -- throwing knife
			{id = 2628, chance = 25000}, -- The Armor of Hardened Leather
			{id = 15776, chance = 5000}, -- killer's blade
			{id = 15722, chance = 4500}, -- scimitar
			{id = 2666, chance = 91760}, -- meat
			{id = 2152, count = 2, chance = 23890}, -- silver
			{id = 2148, count = 50, chance = 93890}, -- coin
		}
	},
	{ name = 'VooDoo Master', file = 'Cirith/Bosses/VooDoo Master.xml', look = {type = 187}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'VooDoo Master', 'VooDoo Master'}, experience = 80000, health = 20000, healthMax = 20000,
		elements = {
			{ type = 'death', value = 45 },
			{ type = 'energy', value = 60 },
			{ type = 'earth', value = 85 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 45 },
		},
		loot = {
			{id = 11387, chance = 900, uniqueDrop = 1}, -- luck amulet
			{id = 3982, chance = 9370, uniqueDrop = 1}, -- green robe
			{id = 15749, chance = 6500, uniqueDrop = 1}, -- dragon executor
			{id = 2155, chance = 31980}, -- green gem
			{id = {12974, 12975, 12976, 12964}, chance = 5000}, -- lunar crystals
			{id = 10602, chance = 95000}, -- grass
			{id = 14172, chance = 16000, uniqueDrop = 1}, -- chest guard
			{id = 16167, chance = 46020, uniqueDrop = 1}, -- swamplair armor
			{id = 16165, chance = 27130, uniqueDrop = 1}, -- swamplair boots
			{id = {13056, 13071, 13059, 13062}, chance = 50000}, -- silver crafting additives
			{id = 13744, chance = 98650}, -- trollroot
			{id = 10605, chance = 99970}, -- bunch of troll hair
			{id = 2149, count = 15, chance = 94830}, -- small emerald
			{id = 7590, count = 3, chance = 98000}, -- gmp
			{id = 7589, count = 3, chance = 92090}, -- smp
			{id = {1998, 3940}, chance = 50000}, -- green backpack
			{id = 2168, chance = 77000}, -- life ring
			{id = {15758, 15757, 15756, 15755, 15761, 15760, 13940}, chance = 86490}, -- snake set
			{id = 2120, chance = 69990}, -- rope
			{id = 2050, chance = 73280}, -- torch
			{id = 14771, chance = 81550}, -- cure potion
			{id = 2152, count = 2, chance = 83990}, -- silver
			{id = 2148, count = 18, chance = 93870}, -- coin
		}
	},
	{ name = 'Zamulosh', file = 'Cirith/Bosses/Zamulosh.xml', look = {type = 30}, classId = 11, killAmount = 15, charmPoints = 30,
		description = {'Zamulosh', 'Zamulosh'}, experience = 12250, health = 120000, healthMax = 120000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 16197, chance = 1000, uniqueDrop = 1}, -- druid cape
			{id = 13953, chance = 1270, uniqueDrop = 1}, -- bloody axe
			{id = 13978, chance = 3160, uniqueDrop = 1}, -- bloody crusher
			{id = 15760, chance = 9990, uniqueDrop = 1}, -- snake bow
			{id = 5886, chance = 10760}, -- spool of yarn
			{id = 13849, chance = 13290, uniqueDrop = 1}, -- leaf helmet
			{id = 15757, chance = 18790}, -- snake legs
			{id = 2171, chance = 20250}, -- platinum amulet
			{id = {13056, 13071, 13059, 13062}, chance = 50000}, -- silver crafting additives
			{id = 13796, chance = 21000}, -- silver shield
			{id = 13848, chance = 23160, uniqueDrop = 1}, -- leaf armor
			{id = 18050, chance = 8000}, -- green dye
			{id = 7384, chance = 5640, uniqueDrop = 1}, -- runed axe
			{id = 14453, chance = 25640}, -- swamplair rod
			{id = 2631, chance = 26580}, -- knight legs
			{id = 17726, chance = 2500, uniqueDrop = 1}, -- plague spear
			{id = 2165, chance = 31010}, -- stealth ring
			{id = 2167, chance = 32280}, -- energy ring
			{id = 16168, chance = 33330, uniqueDrop = 1}, -- swamplair helmet
			{id = 2169, chance = 34180}, -- time ring
			{id = 10219, chance = 38790, subType = 25}, -- sacred tree amulet
			{id = 14010, chance = 41770}, -- knight armor
			{id = 7591, count = 4, chance = 66460}, -- GHP
			{id = 14433, chance = 77850, uniqueDrop = 1}, -- leaden helmet
			{id = 16473, chance = 2400, uniqueDrop = 1}, -- spiky squelcher
			{id = 5879, count = 3, chance = 86080}, -- gss
			{id = 2152, count = 2, chance = 98730}, -- silver
			{id = 2148, count = 2, chance = 100000}, -- coin
		}
	},
	
	-- Demons
	{ name = 'Abomination Creature', level = 120, file = 'Cirith/Demons/abomination creature.xml', look = {type = 372}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Obrzydliwe Stworzenie', 'Abomination Creature'}, experience = 15000, health = 17500, healthMax = 17500,
		boss = 'Last Breath', chance = 150,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'death', value = 70 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 50 },
			{ type = 'wind', value = -5 },
		},
		loot = {
			{id = 15728, chance = 10}, -- skeleton striking face
			{id = 982, count = 2, chance = 310}, -- dark talon
			{id = 11397, chance = 20}, -- diamond mail
			{id = 13997, chance = 10}, -- warrior's armor
			{id = 16160, chance = 15}, -- black steel bow
			{id = 11794, chance = 20}, -- rose armor
			{id = 13992, chance = 30}, -- bronze plate
			{id = 13009, chance = 40}, -- Dragon rider helmet
			{id = 13886, chance = 40}, -- haunted blade
			{id = {13945, 13946}, chance = 350}, -- ashbringer hood
			{id = 14432, chance = 50}, -- cobalt helmet
			{id = 14005, chance = 160}, -- wizard's robe
			{id = 2195, chance = 680}, -- boh
			{id = 14320, chance = 180}, -- royal steel boots
			{id = {2155, 2158}, chance = 140}, -- green/blue gem
			{id = 2536, chance = 470}, -- medusa shield
			{id = 8902, chance = 3110}, -- sb of mind control
			{id = 14448, chance = 5430}, -- necrotic rod
			{id = 14363, chance = 1750}, -- steel helmet
			{id = 11317, chance = 80}, -- dreaded spear
			{id = 15720, chance = 1650}, -- broadsword
			{id = {2150, 2149}, count = 9, chance = 31030}, -- small amethyst/emerald
			{id = 7591, count = 3, chance = 320}, -- GHP
			{id = 7590, count = 3, chance = 360}, -- smp
			{id = 2152, count = 2, chance = 83290}, -- silver
			{id = 2148, count = 67, chance = 93290}, -- gold
		}
	},
	{ name = 'Arch Devil', level = 120, file = 'Cirith/Demons/arch devil.xml', look = {type = 363}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Arcy Diabe�', 'Arch Devil'}, experience = 19600, health = 22500, healthMax = 22500,
		boss = 'Azazel', chance = 150,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 16207, chance = 260}, -- meteorite armor
			{id = 15731, chance = 140}, -- axe aggressor
			{id = 16175, chance = 100}, -- ruby mace
			{id = 13897, chance = 200}, -- fiery hammer
			{id = 18072, chance = 1250}, -- burning stick
			{id = 16203, chance = 600}, -- fiery helmet
			{id = 2136, chance = 1170}, -- demonbone amulet
			{id = 13893, chance = 310}, -- fire axe
			{id = 16204, chance = 420}, -- fiery shield
			{id = 11738, chance = 120}, -- soulcatcher spellbook
			{id = {13995, 13994}, chance = 50}, -- lavos armor
			{id = 6300, chance = 5310}, -- death ring
			{id = 16138, chance = 220}, -- golden armor
			{id = 16202, chance = 450}, -- fiery armor
			{id = 16137, chance = 390}, -- golden legs
			{id = 5888, chance = 1960}, -- piece of hell steel
			{id = 5887, chance = 3010}, -- piece of royal steel
			{id = 12967, chance = 1000}, -- crystal
			{id = 16174, chance = 400}, -- ruby spear
			{id = 982, count = 3, chance = 710}, -- dark talon
			{id = 2153, chance = 9160}, -- violet gem
			{id = 2155, chance = 8240}, -- green gem
			{id = 15729, chance = 3300}, -- war axe
			{id = 14447, chance = 1520}, -- woi
			{id = 14010, chance = 3530}, -- knight armor
			{id = 16143, chance = 2520}, -- golden blade
			{id = 13888, chance = 8820}, -- vile axe
			{id = 2156, chance = 3150}, -- red gem
			{id = 2150, count = 5, chance = 29570}, -- small amethyst
			{id = 2149, count = 5, chance = 39570}, -- small emerald
			{id = 2147, count = 5, chance = 49930}, -- small ruby
			{id = 9970, count = 5, chance = 50920}, -- small topaz
			{id = 8472, count = 3, chance = 1430}, -- UMP
			{id = 8473, count = 3, chance = 2520}, -- UHP
			{id = 2239, chance = 69530}, -- burnt scroll
			{id = 6529, count = 14, chance = 25280}, -- infernal bolt
			{id = 2050, chance = 93150}, -- torch
			{id = 6500, chance = 85110}, -- de
			{id = 2152, count = 5, chance = 100000}, -- plat coin
			{id = 2148, count = {55, 84}, chance = 100000}, -- gold coin
		}
	},
	{ name = 'Askarak Demon', level = 60, file = 'Cirith/Demons/askarak demon.xml', look = {type = 433}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Askarak', 'Askarak'}, experience = 1750, health = 2700, healthMax = 2700,
		boss = 'Shlamer', chance = 50,
		elements = {
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 2125, chance = 20, subType = 100}, -- golden amulet
			{id = 5904, chance = 90}, -- magic sulphur
			{id = 13990, chance = 10}, -- emerald armor
			{id = 8902, chance = 290}, -- spellbook of mind control
			{id = 2155, chance = 530}, -- green gem
			{id = 2114, chance = 600}, -- piggy bank
			{id = 15742, chance = 780}, -- sapphire ring
			{id = 2789, count = 5, chance = 3960}, -- brown mushroom
			{id = 2149, count = 5, chance = 4850}, -- small emerald
			{id = 7589, chance = 420}, -- SMP
			{id = 7588, chance = 590}, -- SHP
			{id = 7850, count = 11, chance = 10360}, -- earth arrow
			{id = 2152, chance = 41900}, -- silver coin
			{id = 2148, count = 25, chance = 86790}, -- gold coin
		}
	},
	{ name = 'Askarak Lord', level = 75, file = 'Cirith/Demons/askarak lord.xml', look = {type = 429}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Lord Askarak', 'Askarak Lord'}, experience = 3000, health = 5600, healthMax = 5600,
		boss = 'Shlamer', chance = 80,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 13004, chance = 15}, -- leaf shield
			{id = 13993, chance = 50}, -- shadow armor
			{id = 14320, chance = 80}, -- royal steel boots
			{id = 5904, chance = 160}, -- magic sulphur
			{id = 16168, chance = 250}, -- swamplair helmet
			{id = 18338, chance = 10}, -- red gem
			{id = 16167, chance = 270}, -- swamplair armor
			{id = {13055, 13070, 13058, 13061}, chance = 480}, -- bronze crafting additives
			{id = 2151, chance = 450}, -- talon
			{id = 2155, chance = 620}, -- green gem
			{id = 15742, chance = 1700}, -- sapphire ring
			{id = 2789, count = 5, chance = 5360}, -- brown mushroom
			{id = 2149, count = 5, chance = 6240}, -- small emerald
			{id = 7589, chance = 340}, -- SMP
			{id = 7588, chance = 720}, -- SHP
			{id = 2152, chance = 41900}, -- silver coin
			{id = 2148, count = 32, chance = 91800}, -- gold coin
		}
	},
	{ name = 'Askarak Prince', level = 100, file = 'Cirith/Demons/askarak prince.xml', look = {type = 434}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Ksi��� Askarak', 'Askarak Prince'}, experience = 4800, health = 8800, healthMax = 8800,
		boss = 'Shlamer', chance = 110,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'energy', value = 65 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 14157, chance = 30}, -- demonic finger
			{id = 14166, chance = 50}, -- sallet
			{id = 13004, chance = 10}, -- leaf shield
			{id = 16197, chance = 190}, -- forest whisper cape
			{id = 16196, chance = 190}, -- forest whisper legs
			{id = 14320, chance = 220}, -- royal steel boots
			{id = 16167, chance = 380}, -- swamplair armor
			{id = 5904, chance = 420}, -- magic sulphur
			{id = 13848, chance = 460}, -- leaf armor
			{id = 13797, chance = 500}, -- haunted shield
			{id = 2151, chance = 700}, -- talon
			{id = 2155, chance = 800}, -- green gem
			{id = 8902, chance = 840}, -- spellbook of mind control
			{id = 2168, chance = 850}, -- life ring
			{id = 2789, count = 5, chance = 4920}, -- brown mushroom
			{id = 2149, count = 5, chance = 12610}, -- small emerald
			{id = 7588, chance = 650}, -- SHP
			{id = 7589, chance = 840}, -- SMP
			{id = 2152, count = 2, chance = 41900}, -- silver coin
			{id = 2148, count = 45, chance = 86790}, -- gold coin
		}
	},
	{ name = 'Shaburak Demon', level = 65, file = 'Cirith/Demons/shaburak demon.xml', look = {type = 431}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Shaburak', 'Shaburak Demon'}, experience = 1500, health = 2700, healthMax = 2700,
		boss = 'Hellfire Warrior', chance = 50,
		elements = {
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 11237, chance = 50}, -- dragon backpack
			{id = 2130, chance = 20, subType = 50}, -- golden amulet
			{id = 5904, chance = 90}, -- magic sulphur
			{id = 2392, chance = 10}, -- fire sword
			{id = 2187, chance = 290}, -- woi
			{id = 2156, chance = 530}, -- red gem
			{id = 2114, chance = 600}, -- piggy bank
			{id = 15742, chance = 780}, -- sapphire ring
			{id = 2789, count = 5, chance = 3960}, -- brown mushroom
			{id = 2147, count = 5, chance = 4850}, -- small ruby
			{id = 7589, chance = 420}, -- SMP
			{id = 7588, chance = 390}, -- SHP
			{id = 2546, count = 11, chance = 10360}, -- burst arrow
			{id = 2152, count = 1, chance = 41900}, -- silver coin
			{id = 2148, count = 25, chance = 86790}, -- gold coin
		}
	},
	{ name = 'Shaburak Lord', level = 75, file = 'Cirith/Demons/shaburak lord.xml', look = {type = 428}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Lord Shaburak', 'Shaburak Lord'}, experience = 3000, health = 5600, healthMax = 5600,
		boss = 'Hellfire Warrior', chance = 80,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 11237, chance = 10}, -- dragon backpack
			{id = 8931, chance = 15}, -- hellfire blade
			{id = 13924, chance = 50}, -- light crossbow
			{id = 18338, chance = 10}, -- red gem
			{id = 2448, chance = 380}, -- shaburak club
			{id = 5904, chance = 160}, -- magic sulphur
			{id = {13055, 13070, 13058, 13061}, chance = 480}, -- bronze crafting additives
			{id = 13951, chance = 120}, -- hellfire spear
			{id = 18072, chance = 270}, -- burning stick
			{id = 2151, chance = 450}, -- talon
			{id = 2156, chance = 620}, -- red gem
			{id = 15742, chance = 1700}, -- sapphire ring
			{id = 2789, count = 5, chance = 5360}, -- brown mushroom
			{id = 2147, count = 5, chance = 6240}, -- small ruby
			{id = 7589, chance = 640}, -- SMP
			{id = 7588, chance = 720}, -- SHP
			{id = 2152, chance = 41900}, -- silver coin
			{id = 2148, count = 32, chance = 91800}, -- gold coin
		}
	},
	{ name = 'Shaburak Prince', level = 100, file = 'Cirith/Demons/shaburak prince.xml', look = {type = 432}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Ksi��� Shaburak', 'Shaburak Prince'}, experience = 4800, health = 8800, healthMax = 8800,
		boss = 'Hellfire Warrior', chance = 110,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'energy', value = 65 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 11237, chance = 20}, -- dragon backpack
			{id = 14157, chance = 30}, -- demonic finger
			{id = 13893, chance = 50}, -- fire axe
			{id = 8931, chance = 10}, -- hellfire blade
			{id = 18072, chance = 190}, -- burning stick
			{id = 11679, chance = 90}, -- hellfire bow
			{id = 2448, chance = 520}, -- shaburak club
			{id = 13939, chance = 380}, -- hellfire xbow
			{id = 5904, chance = 420}, -- magic sulphur
			{id = 16143, chance = 260}, -- golden blade
			{id = 2392, chance = 500}, -- fire sword
			{id = 2151, chance = 700}, -- talon
			{id = 2156, chance = 800}, -- red gem
			{id = 2187, chance = 840}, -- woi
			{id = 13951, chance = 150}, -- fiery spear
			{id = 2789, count = 5, chance = 4920}, -- brown mushroom
			{id = 2147, count = 5, chance = 12610}, -- small ruby
			{id = 7588, chance = 850}, -- SHP
			{id = 7589, chance = 690}, -- SMP
			{id = 2152, count = 2, chance = 41900}, -- silver coin
			{id = 2148, count = 45, chance = 86790}, -- gold coin
		}
	},
	{ name = 'Betrayed Wraith', level = 80, file = 'Cirith/Demons/betrayed wraith.xml', look = {type = 233}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Pot�piony', 'Betrayed Wraith'}, experience = 2900, health = 4200, healthMax = 4200,
		boss = 'Last Breath', chance = 80,
		elements = {
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 90 },
			{ type = 'earth', value = 85 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 13978, chance = 250}, -- bloody crusher
			{id = 14482, chance = 240}, -- ancient helmet
			{id = 6300, chance = 500}, -- death ring
			{id = 13886, chance = 210}, -- haunted blade
			{id = 2197, chance = 1300, subType = 10}, -- stone skin amulet
			{id = 2143, count = 2, chance = 6770}, -- white pearl
			{id = 5944, chance = 1010}, -- soul orb
			{id = 2145, count = 4, chance = 11340}, -- small diamond
			{id = 7591, count = 2, chance = 1820}, -- GHP
			{id = 7590, count = 3, chance = 1010}, -- GMP
			{id = 11227, chance = 19530}, -- unholy bone
			{id = 2537, chance = 235}, -- haunted helmet
			{id = 13307, chance = 430}, -- magma spear
			{id = 15607, chance = 550}, -- skullcrusher bow
			{id = 6500, chance = 1980}, -- demonic essence
			{id = 2547, count = 15, chance = 50870}, -- power bolt
			{id = 6558, chance = 5280}, -- concentrated demonic blood
			{id = 2152, chance = 87950}, -- silver coin
			{id = 2148, count = {38, 72}, chance = 97680}, -- gold
		}
	},
	{ name = 'Black Devil', level = 40, file = 'Cirith/Demons/black devil.xml', look = {type = 458}, classId = 27, killAmount = 500, charmPoints = 15,
		description = {'Mroczny S�uga', 'Black Devil'}, experience = 345, health = 600, healthMax = 600,
		boss = 'Legion', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 45 },
		},
		loot = {
			{id = 14339, chance = 90}, -- steel shield
			{id = 2150, chance = 310}, -- small amethyst
			{id = 14455, chance = 470}, --necrotic rod
			{id = 2568, chance = 1090}, -- cleaver
			{id = 5809, chance = 7000}, -- magical element
			{id = 2050, chance = 1400}, -- torch
			{id = 14455, chance = 1230}, -- the creeper of darkness
			{id = 2050, chance = 1400}, -- torch
			{id = 15715, chance = 1470}, -- weapon
			{id = 5878, chance = 350}, -- hardener leather
			{id = 15789, chance = 2970}, -- copper sword
			{id = 2548, chance = 19840}, -- pitchwork
			{id = 11317, chance = 1}, -- dreaded spear
		}
	},
	{ name = 'Blightwalker', level = 80, file = 'Cirith/Demons/blightwalker.xml', look = {type = 246}, classId = 5, killAmount = 2500, charmPoints = 50,
		description = {'Roznosiciel Zarazy', 'Blightwalker'}, experience = 6850, health = 8900, healthMax = 8900,
		boss = 'Last Breath', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 2694, chance = 59650}, -- bunch of wheat
			{id = 5944, chance = 1070}, -- soul orb   
			{id = 9971, chance = 1160}, -- gold ingot
			{id = 6300, chance = 180}, -- death ring
			{id = 7732, chance = 2010}, -- seeds
			{id = 13307, chance = 620}, -- magma spear
			{id = 2199, chance = 1220, subType = 200}, -- garlic necklacke
			{id = 10604, chance = 9710}, -- bundle of cursed straw
			{id = 9810, chance = 390}, -- rusty armor(rare)
			{id = 7591, chance = 890}, -- GHP
			{id = 14433, chance = 70}, -- leaden helmet
			{id = 15733, chance = 130}, --devastator
			{id = 2550, chance = 3170}, --scythe
			{id = 13797, chance = 500}, -- haunted shield
			{id = 14452, chance = 650}, -- staff of wind
			{id = 13990, chance = 390}, -- emerald armor
			{id = 15757, chance = 440}, -- snake legs
			{id = 2545, chance = 41390, count = 12}, -- poison arrow
			{id = 6500, chance = 650}, -- demonic essence
			{id = 2152, count = 2, chance = 41690}, -- silver
			{id = 2148, count = {24, 64}, chance = 32510}, -- gold
		}
	},
	{ name = 'Chimera', level = 115, file = 'Cirith/Demons/chimera.xml', look = {type = 627}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Chimera', 'Chimera'}, experience = 15000, health = 18000, healthMax = 18000,
		boss = 'Azazel', chance = 150,
		elements = {
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 90 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 8931, chance = 600}, -- hellfire blade
			{id = 2231, chance = 150}, -- big bone
			{id = 11738, chance = 220}, -- soulcatcher spellbook
			{id = 13307, chance = 590}, -- magma spear
			{id = 13994, chance = 220}, -- magma armor
			{id = 2391, chance = 630}, -- war hammer
			{id = 16204, chance = 140}, -- hellfire shield
			{id = {16202, 16200, 16201}, chance = 190}, -- hellfire armor/boots/legs
			{id = 11810, chance = 100}, -- fiery bow
			{id = {13947, 13946}, chance = 420}, -- ashbringer robe
			{id = 9971, chance = 1500}, -- gold ignot
			{id = 17871, chance = 410}, -- darkness blade
			{id = 15740, chance = 710}, -- skeletor bow
			{id = 14479, chance = 1300}, -- strange helmet
			{id = 16137, chance = 190}, -- golden legs
			{id = {13055, 13070, 13058, 13061}, chance = 500}, -- bronze crafting additives
			{id = 2158, chance = 120}, -- blue gem
			{id = 14457, chance = 290}, -- spruce staff of destruction
			{id = 2147, count = 3, chance = 8700}, -- small ruby
			{id = 9970, count = 3, chance = 16000}, -- small topaz
			{id = 14012, chance = 9200}, -- crimson sword
			{id = 2149, count = 3, chance = 15200}, -- small emerald
			{id = 982, chance = 190}, -- dark talon
			{id = 2144, count = 4, chance = 12000}, -- black pearl
			{id = 5925, chance = 9350}, -- hardened bone
			{id = 10552, chance = 9630}, -- fierly heart
			{id = 13710, chance = 10390}, -- blazing bone
			{id = 5944, chance = 2240}, -- Soul Orb
			{id = 6500, chance = 1600}, -- demonic essence
			{id = {7590, 7591}, count = 2, chance = 600}, -- GMP/GHP
			{id = 2152, chance = 71900}, -- silver coin
			{id = 2148, count = 61, chance = 95680}, -- copper coin
		}
	},
	{ name = 'Dark Torturer', level = 80, file = 'Cirith/Demons/dark torturer.xml', look = {type = 234}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Mroczny Kat', 'Dark Torturer'}, experience = 4450, health = 7350, healthMax = 7350,
		boss = 'Harbinger', chance = 180,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16137, chance = 90}, -- golden legs
			{id = 15734, chance = 1030}, -- vile axe
			{id = 15733, chance = 1010}, -- devastator
			{id = 11955, chance = 170}, -- jewelled backpack
			{id = 6300, chance = 1960}, -- death ring
			{id = 5480, chance = 2030}, -- cat's paw
			{id = 2143, count = 2, chance = 2650}, -- pearl
			{id = 5803, chance = 120}, -- arbalest
			{id = 16292, chance = 450}, -- legs of the dawn
			{id = 9971, chance = 1270}, -- gold ingot
			{id = 2558, chance = 5090}, -- saw
			{id = 14320, chance = 210}, -- royal steel boots
			{id = 6500, chance = 990}, -- demonic rssence
			{id = 7591, count = 2, chance = 100}, -- ghp
			{id = 7590, count = 2, chance = 190}, -- gmp
			{id = 5944, chance = 1200}, -- soul orb
			{id = 6558, chance = 5180}, -- concentrated demonic blood
			{id = 6558, chance = 3870}, -- concentrated demonic blood
			{id = 6558, chance = 2550}, -- concentrated demonic blood
			{id = 2152, chance = 56970}, -- silver coin
			{id = 2671, count = 2, chance = 64770}, -- ham
			{id = 2148, count = 80, chance = 92780}, -- gold
		}
	},
	{ name = 'Grimspirit', level = 90, file = 'Cirith/Demons/Grimspirit.xml', look = {type = 377}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Grimspirit', 'Grimspirit'}, experience = 5200, health = 7000, healthMax = 7000,
		boss = 'Last Breath', chance = 100,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 15 },
			{ type = 'energy', value = 40 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 2149, count = 3, chance = 11070}, -- small emerald
			{id = 14456, chance = 690}, -- oak staff of the earth
			{id = 16206, chance = 110}, -- meteorite legs
			{id = 7620, count = 2, chance = 840}, -- mana potion
			{id = 2063, chance = 730}, -- small oil lamp
			{id = 11970, chance = 100}, -- ornamented brooch
			{id = 2656, chance = 1050}, -- blue robe
		    {id = 13000, chance = 110}, -- void cape
            {id = 13002, chance = 140}, -- void boots
			{id = 14450, chance = 290}, -- staff of powerful energy
			{id = 10219, chance = 500, subType = 5}, -- sacred tree amulet
			{id = 2123, chance = 50}, -- Rots
			{id = 10521, chance = 110}, -- moon backpack
			{id = 11694, chance = 1550}, -- fine fabric
			{id = 14504, chance = 690}, -- magical thread
			{id = 2152, count = 3, chance = 42700}, -- silver
			{id = 2148, count = {15, 32}, chance = 92560}, -- gold
		}
	},
	{ name = 'Grimeleech', level = 96, file = 'Cirith/Demons/Grimeleech.xml', look = {type = 374}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Grimeleech', 'Grimeleech'}, experience = 7600, health = 10200, healthMax = 10200,
		boss = 'Last Breath', chance = 120,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 25 },
			{ type = 'death', value = 35 },
			{ type = 'energy', value = 80 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 10576, chance = 5080}, -- mystical hourglass
			{id = 16909, chance = 330}, -- sickle of the wind
			{id = 5904, chance = 1240}, -- magic sulphur
			{id = 10559, chance = 3400}, -- horn
			{id = 19000, chance = 10}, -- doll
			{id = 2472, chance = 100}, -- magic plate armor
			{id = 1982, chance = 110}, -- purple tome
			{id = 2153, chance = 190}, -- violet gem
			{id = 12999, chance = 140}, -- void hood
            {id = 13001, chance = 110}, -- void legs
			{id = 17940, chance = 360}, -- pure mithril
			{id = 3982, chance = 480}, -- green robe
			{id = 2155, chance = 120}, -- green gem
			{id = 14172, chance = 100}, -- chest guard
			{id = 16147, chance = 265}, -- Mask of the fallen
			{id = 5954, chance = 5340}, -- demon horn
			{id = 6300, chance = 840}, -- death ring
			{id = 7591, count = 3, chance = 490}, -- SHP
			{id = 13990, chance = 510}, -- emerald armor
			{id = 13888, chance = 4920}, -- lightning axe
			{id = 16210, chance = 90}, -- meteorite hammer
			{id = 17870, chance = 8320}, -- melted axe
                        {id = 16209, chance = 140}, -- meteorite sword
			{id = 16211, chance = 250}, -- meteorite shield
			{id = 2542, chance = 180}, -- lightning shield
			{id = 7590, count = 3, chance = 1600}, -- SMP
			{id = 13163, chance = 150}, -- steel edge
			{id = 2150, count = 5, chance = 6590}, -- small amethyst
			{id = {7838, 11443}, count = {8, 20}, chance = 5500}, -- flash arrow
			{id = 2136, chance = 150}, -- demonbone amulet
			{id = 10221, chance = 450, subType = 5}, -- shockwave amulet
			{id = 2127, chance = 760}, -- emerald bangle
			{id = 13330, chance = 390}, -- marble spear
			{id = 982, chance = 200}, -- dark talon
			{id = 2152, count = {2, 4}, chance = 59610}, -- silver
			{id = 2148, count = {20, 40}, chance = 97170}, -- gold
		}
	},
	{ name = 'Demon', level = 90, file = 'Cirith/Demons/demon.xml', look = {type = 35}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Demon', 'Demon'}, experience = 10900, health = 13200, healthMax = 13200,
		boss = 'Azazel', chance = 150,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13163, chance = 100}, -- steel edge
			{id = 17940, chance = 360}, -- pure mithril
			{id = 16399, chance = 10}, -- demon doll
			{id = 17871, chance = 180}, -- darkness blade
			{id = 13897, chance = 10}, -- fiery hammer
			{id = 7393, chance = 80}, -- demon trophy
			{id = 2472, chance = 160}, -- magic plate armor
			{id = 13893, chance = 110}, -- fire axe
			{id = 16204, chance = 120}, -- fiery shield
			{id = 16137, chance = 250}, -- golden legs
			{id = 18072, chance = 650}, -- burning stick
			{id = 13894, chance = 690}, -- ice rapier
			{id = 2171, chance = 1270}, -- platinum amulet
			{id = 8902, chance = 340}, -- spellbook of mind control
			{id = 14479, chance = 500}, -- strange helmet
			{id = 1986, chance = 210}, -- red tome
			{id = 8931, chance = 480}, -- hellfire blade
			{id = 16143, chance = 750}, -- golden sickle
			{id = 2164, chance = 1810, subType = 20}, -- might ring
			{id = 2214, chance = 1870}, -- ring of healing
			{id = 2165, chance = 140}, -- stealth ring
			{id = 2392, chance = 1820}, -- fire sword
			{id = 2156, chance = 150}, -- red gem
			{id = 2176, chance = 2830}, -- orb
			{id = 2151, chance = 3470}, -- talon
			{id = 5906, chance = 1000}, -- demon dust
			{id = 13340, chance = 4680}, -- demon paw
			{id = 13951, chance = 1200}, -- fiery spear
			{id = {2150, 2147, 9970, 2149}, count = 5, chance = 7970}, -- small amethyst
			{id = 6500, chance = 4030}, -- demonic essence
			{id = 5954, chance = 5340}, -- demon horn
			{id = 8473, count = 3, chance = 1570}, -- UHP
			{id = 2795, count = 6, chance = 19650}, -- fire mushroom
			{id = 8472, count = 3, chance = 1940}, -- UMP
			{id = 2152, count = 2, chance = 59610}, -- silver
			{id = 2148, count = {32, 76}, chance = 97170}, -- gold
		}
	},
	{ name = 'Demon Guard', level = 1, file = 'Cirith/Demons/demon guard.xml', look = {type = 35},
		description = {'Demoniczny Stra�nik', 'Demon Guard'}, experience = 50, health = 8200, healthMax = 8200,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16143, chance = 70}, -- golden sickle
			{id = 2164, chance = 110, subType = 20}, -- might ring
			{id = 2214, chance = 170}, -- ring of healing
			{id = 2165, chance = 10}, -- stealth ring
			{id = 2392, chance = 80}, -- fire sword
			{id = 2156, chance = 10}, -- red gem
			{id = 2176, chance = 230}, -- orb
			{id = 2151, chance = 340}, -- talon
			{id = 5906, chance = 10}, -- demon dust
			{id = 13340, chance = 480}, -- demon paw
			{id = {2150, 2147, 9970, 2149}, count = 5, chance = 770}, -- small amethyst
			{id = 6500, chance = 430}, -- demonic essence
			{id = 5954, chance = 340}, -- demon horn
			{id = 2148, count = {3, 7}, chance = 97170}, -- gold
		}
	},
	{ name = 'Destroyer', level = 80, file = 'Cirith/Demons/destroyer.xml', look = {type = 236}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Niszczyciel', 'Destroyer'}, experience = 2400, health = 3700, healthMax = 3700,
		boss = 'Harbinger', chance = 130,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 75 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13953, chance = 10}, -- the fallen horse
			{id = 16208, chance = 110}, -- meteorite helmet
			{id = 6300, chance = 140}, -- death ring
			{id = 2125, chance = 520}, -- crystal necklace
			{id = 2178, chance = 540}, -- mind stone
			{id = 16162, chance = 390}, -- black steel hammer
			{id = 16161, chance = 310}, -- black steel sword
			{id = 14320, chance = 180}, -- plate boots
			{id = 7591, chance = 580}, -- GHP
			{id = 14362, chance = 920}, -- plate armor
			{id = 15607, chance = 400}, -- skullcrusher bow
			{id = 2553, chance = 6250}, -- pick
			{id = 11209, chance = 6920}, -- metal spike
			{id = 5944, chance = 1030}, -- soul orb
			{id = 2150, count = 2, chance = 7560}, -- small amethyst
			{id = 14001, chance = 2280}, -- dark armor
			{id = 2546, count = 12, chance = 11350}, -- burst arrow
			{id = 6500, chance = 1830}, -- demonic essence
			{id = 2666, count = 6, chance = 54530}, -- meat
			{id = 2152, chance = 3970}, -- silver coins
			{id = 2148, count = {36, 76}, chance = 99550}, -- gold
		}
	},
	{ name = 'Diabolic Imp', level = 80, file = 'Cirith/Demons/diabolic imp.xml', look = {type = 237}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Demoniczny Imp', 'Diabolic Imp'}, experience = 1400, health = 1950, healthMax = 1950,
		boss = 'Hellfire Warrior', chance = 110,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 6300, chance = 110}, -- death ring
			{id = 16157, chance = 380}, -- black steel armor
			{id = 16158, chance = 140}, -- black steel helmet
			{id = 14455, chance = 770}, -- necrotic rod
			{id = 15719, chance = 1800}, -- battle hammer
			{id = 2150, count = 3, chance = 2320}, -- small amethyst
			{id = 2165, chance = 760}, -- stealth ring
			{id = 15722, chance = 5460}, -- scimitar
			{id = 2625, chance = 10}, -- imp armor
			{id = 13742, chance = 15900}, -- small pitchfork
			{id = 5944, chance = 1210}, -- soul orb
			{id = 6500, chance = 950}, -- demonic essence
			{id = 11679, chance = 100}, -- hellfire bow
			{id = 8900, chance = 1280}, -- spellbook
			{id = 2568, chance = 8990}, -- cleaver
			{id = 6558, chance = 4370}, -- concentrated demonic blood
			{id = 6558, chance = 3990}, -- concentrated demonic blood
			{id = 2548, chance = 49860}, -- pitchfork
			{id = 2152, count = 2, chance = 3330}, -- silver coin
			{id = 2148, count = 47, chance = 99620}, -- gold
		}
	},
	{ name = 'Fire Devil', level = 15, file = 'Cirith/Demons/fire devil.xml', look = {type = 40}, classId = 27, killAmount = 500, charmPoints = 15,
		description = {'Demoniczny S�uga', 'Fire Devil'}, experience = 125, health = 200, healthMax = 200,
		boss = 'Legion', chance = 60,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = {13947, 13946}, chance = 50}, -- ashbringer robe
			{id = 5888, chance = 10}, -- piece of hell steel
			{id = 14339, chance = 90}, -- steel shield
			{id = 16457, chance = 110}, -- fire devil trophy
			{id = 2150, chance = 310}, -- small amethyst
			{id = 14448, chance = 470}, --necrotic rod
			{id = 2568, chance = 1090}, -- cleaver
			{id = 2050, chance = 1400}, -- torch
			{id = 13742, chance = 6500}, -- small pitchfork
			{id = 2050, chance = 1400}, -- torch
			{id = 15715, chance = 1470}, -- weapon
			{id = 15789, chance = 2970}, -- copper sword
			{id = 2548, chance = 19840}, -- pitchwork
		}
	},
	{ name = 'Fury', level = 85, file = 'Cirith/Demons/fury.xml', look = {type = 149, head = 94, body = 77, legs = 96, feet = 0, addons = 3}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Furia', 'Fury'}, experience = 2900, health = 4100, healthMax = 4100,
		boss = 'Lucifer', chance = 60,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 30 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 30 },
		},
		loot = {
			{id = 6300, chance = 70}, -- death ring
			{id = {13947, 13946}, chance = 480}, -- ashbringer robe
			{id = 16137, chance = 100}, -- golden legs
			{id = 2124, chance = 430}, -- crystal ring
			{id = 14320, chance = 360}, -- steel boots
			{id = 15724, chance = 300}, -- berserker
			{id = {13055, 13070, 13058, 13061}, chance = 270}, -- bronze crafting additives
			{id = 5888, chance = 960}, -- piece of hell steel
			{id = 5887, chance = 1150}, -- piece of royal steel
			{id = 2143, count = 4, chance = 5410}, -- white pearl
			{id = 13979, chance = 240}, -- crusher
			{id = 17683, chance = 100}, -- cursed blade
			{id = 5911, chance = 4100}, -- red poc
			{id = 16431, chance = 100}, -- black forest bow
			{id = 2150, count = 3, chance = 8160}, -- small amethyst
			{id = 7591, chance = 580}, -- GHP
			{id = 5944, chance = 1780}, -- soul orb
			{id = 6500, chance = 1790}, -- demonic essence
			{id = 8844, chance = 29460}, -- jalapeno pepper
			{id = 6558, chance = 4130}, -- concentrated demonic blood
			{id = 6558, chance = 3210}, -- concentrated demonic blood
			{id = 6558, chance = 2990}, -- concentrated demonic blood
			{id = 2152, chance = 11690}, -- silver
			{id = 2148, count = 29, chance = 98750}, -- gold
		}
	},
	{ name = 'Hand of Cursed Fate', level = 90, file = 'Cirith/Demons/hand of cursed fate.xml', look = {type = 230}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'R�ka Przekl�tego', 'Hand of Cursed Fate'}, experience = 5400, health = 7500, healthMax = 7500,
		boss = 'Last Breath', chance = 130,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -5 },
			{ type = 'physical', value = 25 },
		},
		loot = {
			{id = 15727, chance = 30}, -- minotaur hammer
			{id = 2195, chance = 330}, -- boh
			{id = 13979, chance = 340}, -- crusher
			{id = 9971, chance = 1210}, -- gold ingot
			{id = 2153, chance = 160}, -- violet gem
			{id = 2171, chance = 260}, -- platinum amulet
			{id = 14323, chance = 390}, -- royal steel armor
			{id = 6300, chance = 1330}, -- death ring
			{id = 2158, chance = 160}, -- blue gem
			{id = 11435, count = 9, chance = 1800}, -- crystalline arrow
			{id = 2167, chance = 2110}, -- energy ring
			{id = 2537, chance = 550}, -- haunted helmet
			{id = 14010, chance = 800}, -- knight armor
			{id = 2187, chance = 920}, -- wand of inferno
			{id = 2127, chance = 5350}, -- emerald bangle
			{id = 2154, chance = 140}, -- yellow gem
			{id = 2200, chance = 1070}, -- protection amulet
			{id = 6500, chance = 3700}, -- demonic essence
			{id = 2146, count = 4, chance = 14160}, -- small sapphire
			{id = 2178, chance = 15730}, -- mind stone
			{id = 7591, count = 2, chance = 680}, -- GHP
			{id = 7590, count = 2, chance = 370}, -- GMP
			{id = 6558, count = 4, chance = 8030}, -- concentrated demonic blood
			{id = 5944, chance = 1620}, -- soul orb
			{id = 2152, count = 2, chance = 4000}, -- silver coin
			{id = 2148, count = 7, chance = 99710}, -- gold
		}
	},
	{ name = 'Hellhound', level = 100, file = 'Cirith/Demons/hellhound.xml', look = {type = 240}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Piekielny Ogar', 'Hellhound'}, experience = 5300, health = 7500, healthMax = 7500,
		boss = 'Lucifer', chance = 120,
		elements = {
			{ type = 'holy', value = -5 },
			{ type = 'physical', value = 10 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13994, chance = 220}, -- magma armor
			{id = 2391, chance = 330}, -- war hammer
			{id = 16203, chance = 250}, -- hellfire helmet
			{id = {13947, 13946}, chance = 380}, -- ashbringer robe
			{id = 16201, chance = 260}, -- hellfire legs
			{id = 11396, chance = 250}, -- holy blade
			{id = 15734, chance = 850}, -- vile axe
			{id = 2231, chance = 960}, -- big bone
			{id = 2155, chance = 120}, -- green gem
			{id = 2656, chance = 410}, -- blue robe
			{id = 9971, chance = 1430}, -- gold ignot
			{id = 14325, chance = 130}, -- steel blade
			{id = 5911, chance = 3270}, -- red poc
			{id = 2154, chance = 110}, -- yellow gem
			{id = 2156, chance = 160}, -- red gem
			{id = 5910, chance = 5240}, -- green poc
			{id = 5914, chance = 6080}, -- yellow poc
			{id = 14012, chance = 3200}, -- crimson sword
			{id = 14447, chance = 790}, -- hellfire rod
			{id = 15717, chance = 1590}, -- glorious axe
			{id = 5925, chance = 9350}, -- hardened bone
			{id = 2144, count = 4, chance = 9380}, -- black pearl
			{id = 10552, chance = 9630}, -- fierly heart
			{id = 2147, count = 3, chance = 10130}, -- small ruby
			{id = 9970, count = 3, chance = 10380}, -- small topaz
			{id = 2149, count = 3, chance = 11070}, -- small emerald
			{id = 13710, chance = 12390}, -- blazing bone
			{id = 8473, chance = 1420}, -- GHP
			{id = 6558, chance = 8290}, -- Concentrated Demonic Blood
			{id = 6558, chance = 6290}, -- Concentrated Demonic Blood
			{id = 10553, chance = 18680}, -- hellhound slobber
			{id = 5944, chance = 1240}, -- Soul Orb
			{id = 6500, chance = 2510}, -- demonic essence
			{id = 7591, chance = 630}, -- SHP
			{id = 7590, count = 3, chance = 1600}, -- GMP
			{id = 2671, count = 14, chance = 30870}, -- Ham
			{id = 2152, count = 2, chance = 57900}, -- silver coin
			{id = 2148, count = 52, chance = 97170}, -- gold
		}
	},
	{ name = 'Hellspawn', level = 70, file = 'Cirith/Demons/hellspawn.xml', look = {type = 322}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Pomiot Piekie�', 'Hellspawn'}, experience = 2050, health = 3500, healthMax = 3500,
		boss = 'Harbinger', chance = 70,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 40 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13706, chance = 1600}, -- beastly claw
			{id = 14175, chance = 4300}, -- coal
			{id = 16456, chance = 90}, -- hellspawn trophy
			{id = 7389, chance = 350}, -- dragon slayer
			{id = {13947, 13946}, chance = 330}, -- ashbringer robe
			{id = 9949, chance = 120}, -- dracoyle statue
			{id = 9969, chance = 180}, -- black skull
			{id = 5888, chance = 560}, -- piece of hell steel
			{id = 13896, chance = 500}, -- giant sword
			{id = 14322, chance = 240}, -- royal steel helmet
			{id = 14321, chance = 250}, -- royal steel legs
			{id = 9810, chance = 3200}, -- rusty armor rare
			{id = 9809, chance = 3200}, -- rusty armor semi rare
			{id = 9970, count = 2, chance = 7120}, -- small topaz
			{id = 2788, count = 2, chance = 7800}, -- red mushroom
			{id = 6500, chance = 960}, -- de
			{id = 8473, chance = 860}, -- UHP
			{id = 14334, chance = 2860}, -- heavy axe
			{id = 14357, chance = 3220}, -- bronze shield
			{id = 7591, chance = 440}, -- GHP
			{id = 11215, chance = 8860}, -- hellspawn tail
			{id = 2152, chance = 61690}, -- silver
			{id = 2148, count = {28, 43}, chance = 82380}, -- coin
		}
	},
	{ name = 'Juggernaut', level = 118, file = 'Cirith/Demons/juggernaut.xml', look = {type = 244}, classId = 27, killAmount = 2500, charmPoints = 50,
		description = {'Juggernaut', 'Juggernaut'}, experience = 15500, health = 20000, healthMax = 20000,
		boss = 'Last Breath', chance = 120,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = -5 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 11418, chance = 5}, -- juggernaut doll
			{id = 11396, chance = 110}, -- holy blade
			{id = 14358, chance = 1110}, -- bronze axe
			{id = {13055, 13070, 13058, 13061}, chance = 850}, -- bronze crafting additives
			{id = 16356, chance = 10}, -- container
			{id = 13992, chance = 200}, -- bronze plate
			{id = {13947, 13944}, chance = 400}, -- ashbringer robe
			{id = 16137, chance = 220}, -- golden legs
			{id = 13899, chance = 200}, -- bastard sword
			{id = 2136, chance = 300}, -- demonbone amulet
			{id = 16211, chance = 260}, -- meteorite shield
			{id = 16138, chance = 170}, -- golden armor
			{id = 8849, chance = 1250}, -- modified crossbow
			{id = 2578, chance = 810}, -- closed trap
			{id = 13009, chance = 100}, -- Dragon rider helmet
			{id = 2153, chance = 320}, -- violet gem
			{id = 2155, chance = 180}, -- green gem
			{id = 15732, chance = 200}, -- knight axe
			{id = 14010, chance = 1310}, -- knight armor
			{id = 13888, chance = 920}, -- lightning axe
			{id = 9971, chance = 1750}, -- gold ingot
			{id = {7632, 7633}, chance = 4000}, -- giant pearl
			{id = 15729, chance = 1250}, -- double axe
			{id = 2156, chance = 180}, -- red gem
			{id = 7365, count = 15, chance = 5540}, -- onyx arrow
			{id = 2150, count = 3, chance = 6910}, -- small amethyst
			{id = 2149, count = 3, chance = 7920}, -- small emerald
			{id = 2147, count = 3, chance = 9090}, -- small ruby
			{id = {2145, 9970}, count = 3, chance = 9850}, -- small diamond/topaz
			{id = 6558, chance = 5010}, -- concentrated demonic blood
			{id = 7591, count = 3, chance = 1140}, -- GHP
			{id = 7590, count = 3, chance = 1220}, -- GMP
			{id = 5944, chance = 1280}, -- soul orb
			{id = 6500, chance = 5470}, -- demonic essence
			{id = 2671, count = 8, chance = 68860}, -- ham
			{id = 2152, count = 2, chance = 45370}, -- silver coin
			{id = 2148, count = {4, 8}, chance = 92450}, -- gold
		}
	},
	{ name = 'Lost Soul', level = 88, file = 'Cirith/Demons/lost soul.xml', look = {type = 232}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Utrapiony', 'Lost Soul'}, experience = 4500, health = 5800, healthMax = 5800,
		boss = 'Last Breath', chance = 100,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'physical', value = 20 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 14482, chance = 350}, -- ancient helmet
			{id = 13796, chance = 480}, -- silver shield
			{id = 13894, chance = 420}, -- frosty rapier
			{id = 18338, chance = 110}, -- red gem
			{id = 13886, chance = 270}, -- haunted blade
			{id = 17161, chance = 280}, -- death enchanted in the staff
			{id = 15729, chance = 480}, -- double axe
			{id = 2133, chance = 1580, subType = 200}, -- ruby necklace
			{id = 2156, chance = 20}, -- red gem
			{id = 6300, chance = 2360}, -- death ring
			{id = 2197, chance = 3050, subType = 8}, -- ssa
			{id = {9809, 9810}, chance = 3720}, -- rusty armor (rare)
			{id = 5806, chance = 5660}, -- silver goblet
			{id = 6500, chance = 2520}, -- demonic essence
			{id = 7591, count = 2, chance = 210}, -- GHP
			{id = 2143, count = 3, chance = 11600}, -- white pearl
			{id = 2144, count = 3, chance = 12070}, -- black pearl
			{id = 7590, count = 2, chance = 260}, -- GMP
			{id = 5944, chance = 1590}, -- soul orb
			{id = 11227, chance = 33120}, -- unholy bone
			{id = 2152, chance = 63840}, -- silver coin
			{id = 2148, count = {42, 84}, chance = 92510}, -- gold
		}
	},
	{ name = 'Nightmare Scion', level = 50, file = 'Cirith/Demons/nightmare scion.xml', look = {type = 321}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Mara', 'Nightmare Scion'}, experience = 950, health = 1400, healthMax = 1400,
		boss = 'Harbinger', chance = 70,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 9941, chance = 110}, -- crystal of focus
			{id = 6574, chance = 240}, -- death ring
			{id = 6300, chance = 250}, -- death ring
			{id = 14352, chance = 270}, -- iron blade
			{id = 14006, chance = 200}, -- cloak of fear
			{id = 15761, chance = 350}, -- snake blade
			{id = 15755, chance = 1180}, -- snake helmet
			{id = 13299, chance = 170}, -- glass spear
			{id = 17704, chance = 20}, -- the frosty midnight fire
			{id = 15607, chance = 250}, -- skullcrusher bow
			{id = 11223, chance = 4890}, --scythe leg
			{id = 11217, chance = 7970}, -- essence of a bad dream
			{id = 2666, count = 4, chance = 51080}, -- ham
			{id = 2152, chance = 1020}, --  silver coin
			{id = 2148, count = 19, chance = 97090}, -- gold
		}
	},
	{ name = 'Nightmare', level = 80, file = 'Cirith/Demons/nightmare.xml', look = {type = 245}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Nocna Mara', 'Nightmare'}, experience = 1950, health = 2700, healthMax = 2700,
		boss = 'Harbinger', chance = 110,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 10 },
			{ type = 'physical', value = 10 },
		},
		loot = {
			{id = 13706, chance = 1600}, -- beastly claw
			{id = 11396, chance = 100}, -- holy blade
			{id = 5669, chance = 110}, --voodoo skull
			{id = 2195, chance = 250}, -- boots of haste
			{id = 15730, chance = 310}, --  war axe
			{id = 17238, chance = 190}, --  morning star
			{id = 7419, chance = 160}, --  dreaded cleaver
			{id = 5888, chance = 420}, -- piece of hell steel
			{id = 2631, chance = 570}, -- knight legs
			{id = 14455, chance = 1500}, -- the creeper of darkness
			{id = 13707, chance = 1510}, -- cuprite
			{id = 14317, chance = 310}, -- enhanced oak shield
			{id = 6300, chance = 1180}, -- death ring
			{id = 17161, chance = 150}, -- death enchanted in the staff
			{id = 2547, count = 4, chance = 9040}, -- power bolt
			{id = 11223, chance = 9940}, --scythe leg
			{id = 6500, chance = 1110}, -- demonic essence
			{id = 11217, chance = 15320}, -- essence of a bad dream
			{id = 6558, count = 2, chance = 4810}, -- concentrated demonic blood
			{id = 5944, chance = 1310}, -- soul orb
			{id = 2671, count = 2, chance = 29430}, -- ham
			{id = 2152, chance = 2570}, --  silver coin
			{id = 2148, count = 24, chance = 96960}, -- gold
		}
	},
	{ name = 'Plaguesmith', level = 92, file = 'Cirith/Demons/plaguesmith.xml', look = {type = 247}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Plugawiciel', 'Plaguesmith'}, experience = 5400, health = 8250, healthMax = 8250,
		boss = 'Harbinger', chance = 130,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13897, chance = 100}, -- fire hammer
			{id = 13953, chance = 200}, -- bloody axe
			{id = {13055, 13070, 13058, 13061}, chance = 490}, -- bronze crafting additives
			{id = 3957, chance = 100}, -- war horn
			{id = 2127, chance = 570}, -- emerald bangle
			{id = 13585, chance = 750}, -- a foul rod
			{id = 13986, chance = 390}, -- amber plate
			{id = 12953, chance = 260}, -- marksman boots
			{id = 17726, chance = 20}, -- plague spear
			{id = 15733, chance = 400}, -- devastator
			{id = 14320, chance = 560}, -- steel boots
			{id = 2134, chance = 2020}, -- silver brooch
			{id = 11396, chance = 130}, -- holy blade
			{id = 5887, chance = 1200}, -- piece of royal steel
			{id = 5889, chance = 2950}, -- piece of steel
			{id = 5888, chance = 2960}, -- piece of hell steel
			{id = 2150, chance = 4760, count = 3}, -- small amethyst
			{id = 2209, chance = 5040}, -- club ring
			{id = 2631, chance = 1360}, -- knight legs
			{id = 9810, chance = 7960}, -- rusty armor
			{id = 7365, count = 4, chance = 7960}, -- onyx arrow
			{id = 6500, chance = 1860}, -- demonic essence
			{id = 15786, chance = 9040}, -- copper armor
			{id = 7591, chance = 900}, -- GHP
			{id = 5944, chance = 1200}, -- soul orb
			{id = 2225, chance = 19940}, -- piece of iron
			{id = 15723, chance = 4360}, -- two handed sword
			{id = 15715, chance = 10060}, -- cranial basher
			{id = 14357, chance = 2140}, -- bronze shield
			{id = 14334, chance = 19850}, -- heavy axe
			{id = 2235, chance = 49840}, -- mouldy cheese
			{id = 2237, chance = 60030}, -- dirty cape
			{id = 2152, count = 2, chance = 11690}, -- silver
			{id = 2148, count = {18, 41}, chance = 99500}, -- gold
		}
	},
	{ name = 'Spectre', level = 75, file = 'Cirith/Demons/spectre.xml', look = {type = 235, head = 20, body = 0, legs = 0, feet = 0}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'Upi�r', 'Spectre'}, experience = 1200, health = 1350, healthMax = 1350,
		boss = 'Harbinger', chance = 100,
		elements = {
			{ type = 'physical', value = 90 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 1 },
		},
		loot = {
			{id = 2136, chance = 120}, -- demonbone amulet
			{id = 15740, chance = 250}, -- skull bow
			{id = 2165, chance = 220}, -- stealth ring
			{id = 6300, chance = 310}, -- death ring
			{id = 15734, chance = 100}, -- vile axe
			{id = 2134, chance = 890}, -- silver brooch
			{id = 7590, chance = 910}, -- GMP
			{id = 14016, chance = 35}, -- holy shield
			{id = 11221, chance = 980}, -- shiny stone
			{id = 6534, chance = 120}, -- flying trident
			{id = 5909, chance = 3750}, -- white piece of cloth
			{id = 5944, chance = 1940}, -- soul orb
			{id = 6500, chance = 1060}, -- demonic essence
			{id = 2071, chance = 9660}, -- lyre
			{id = 14459, chance = 9770}, --woce
			{id = 2152, chance = 41690}, -- silver
			{id = 2148, count = 19, chance = 99700}, -- gold
		}
	},
	{ name = 'The Fallen Servant', level = 60, file = 'Cirith/Demons/the fallen servant.xml', look = {type = 375}, classId = 27, killAmount = 1000, charmPoints = 25,
		description = {'S�uga Upad�ego', 'The Fallen Servant'}, experience = 1700, health = 2500, healthMax = 2500,
		boss = 'Harbinger', chance = 110,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 65 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 40 },
		},
		loot = {
			{id = 13896, chance = 410}, -- giant sword
			{id = 16201, chance = 10}, -- fiery legs
			{id = 13992, chance = 50}, -- bronze plate
			{id = 11435, count = 8, chance = 800}, -- crystalline arrow
			{id = 14010, chance = 90}, -- knight armor
			{id = 13886, chance = 20}, -- haunted blade
			{id = 14170, chance = 590}, -- legionnaires armor
			{id = 14355, chance = 280}, -- bronze legs
			{id = 7588, count = 2, chance = 850}, -- shp
			{id = 2152, chance = 19650}, -- silver coin
			{id = 2148, count = 20, chance = 87390}, -- coin
		}
	},
	{ name = 'Yielothax', level = 90, file = 'Cirith/Demons/yielothax.xml', look = {type = 442}, classId = 34, killAmount = 1000, charmPoints = 25,
		description = {'Yielothax', 'Yielothax'}, experience = 2150, health = 3200, healthMax = 3200,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'death', value = 75 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 20 },
			{ type = 'holy', value = 30 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 19463, chance = 20}, -- scare the death
			{id = 16148, chance = 130}, -- a dirty eye of despair
			{id = 2145, count = 3, chance = 1070}, -- small diamond
			{id = 13592, chance = 250}, -- cursed spear
			{id = 8901, chance = 1920}, -- sb of warding
			{id = 10221, chance = 900, subType = 3}, -- shockwave amulet
			{id = 2158, chance = 90}, -- blue gem
			{id = 2631, chance = 360}, -- knight legs
			{id = 7588, chance = 1250}, -- SHP
			{id = 982, chance = 210}, -- dark talon
			{id = 5892, chance = 1400}, -- steel
			{id = 15609, chance = 940}, -- massive ornamented bow
			{id = 2164, chance = 2540}, -- might ring
			{id = 7589, chance = 1000}, -- SMP
			{id = 2789, count = 2, chance = 14120}, -- brown mushroom
			{id = 9970, count = 2, chance = 7810}, -- small topaz
			{id = 15782, chance = 7430}, -- steel spear
			{id = 5809, chance = 1630}, -- magical element
			{id = 2152, count = 2, chance = 21500}, -- silver coin
			{id = 2148, count = {10, 25}, chance = 95190}, -- coin
		}
	},
	
	-- Djinns
	{ name = 'Afrytt', level = 44, file = 'Cirith/Djinns/afrytt.xml', look = {type = 659, addons = 3}, classId = 14, killAmount = 500, charmPoints = 15,
		description = {'Afrytt', 'Afrytt'}, experience = 1400, health = 2000, healthMax = 2000,
		boss = 'All a Din', chance = 80,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 60 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 2155, chance = 50}, -- green gem
			{id = 13940, chance = 20}, -- swamplair crossbow
			{id = 15755, chance = 280}, -- snake helmet
			{id = 2070, chance = 150}, -- wooden flute
			{id = 16168, chance = 50}, -- swamplair helmet
			{id = 8901, chance = 200}, -- spellbook of warding
			{id = 16148, chance = 60}, -- the fallen rod
			{id = 3982, chance = 20}, -- green robe
			{id = 7732, chance = 1820}, --seeds
			{id = 1860, chance = 3030}, -- green tapestry
			{id = 5910, chance = 2730}, -- green piece of cloth
			{id = 2420, chance = 4860}, -- machete
			{id = 2149, count = 2, chance = 6015}, -- small emerald
			{id = 7589, chance = 950}, -- SMP
			{id = 2673, count = 6, chance = 12000}, -- pear
			{id = 13948, chance = 4570}, -- hunting spear
			{id = 2148, count = {25, 80}, chance = 86150}, -- copper coin
		}
	},
	{ name = 'Green Djinn', level = 35, file = 'Cirith/Djinns/green djinn.xml', look = {type = 659}, classId = 14, killAmount = 500, charmPoints = 15,
		description = {'Zielony D�in', 'Green Djinn'}, experience = 725, health = 1000, healthMax = 1000,
		boss = 'All a Din', chance = 50,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 13979, chance = 190}, -- crusher
			{id = 13988, chance = 200}, -- plate of underwater
			{id = 16305, chance = 220}, -- enhanced leather hood
			{id = 2805, chance = 440}, -- troll green
			{id = 7620, chance = 340}, -- mana potion
			{id = 2063, chance = 730}, -- small oil lamp
			{id = 5910, chance = 1910}, -- green piece of cloth
			{id = 1980, chance = 2410}, -- book
			{id = 2149, count = 4, chance = 2580}, -- small emerald
			{id = 13948, chance = 4570}, -- hunting spear
			{id = 2673, chance = 23790}, -- pear
			{id = 2148, count = {15, 80}, chance = 86150}, -- copper coin
		}
	},
	{ name = 'Marid', level = 44, file = 'Cirith/Djinns/marid.xml', look = {type = 658, addons = 3}, classId = 14, killAmount = 500, charmPoints = 15,
		description = {'Marid', 'Marid'}, experience = 1400, health = 2000, healthMax = 2000,
		boss = 'All a Din', chance = 80,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 60 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 2158, chance = 50}, -- blue gem
			{id = 11680, chance = 90}, -- lightning bow
			{id = 16305, chance = 280}, -- enhanced leather hood
			{id = 2070, chance = 150}, -- wooden flute
			{id = 13299, chance = 160}, -- glass spear
			{id = 16192, chance = 90}, -- frozen helmet
			{id = 8901, chance = 200}, -- spellbook of warding
			{id = 11621, chance = 60}, -- sphere frost
			{id = 14009, chance = 20}, -- sparkling cape
			{id = 7732, chance = 1820}, --seeds
			{id = 1872, chance = 3030}, -- blue tapestry
			{id = 5912, chance = 2730}, -- blue piece of cloth
			{id = 2420, chance = 4860}, -- machete
			{id = 2146, count = 2, chance = 6015}, -- small sapphire
			{id = 7589, chance = 950}, -- SMP
			{id = 2677, count = 25, chance = 12000}, -- blueberry
			{id = 15782, chance = 4570}, -- steel spear
			{id = 2148, count = {25, 80}, chance = 86150}, -- copper coin
		}
	},
	{ name = 'Blue Djinn', level = 35, file = 'Cirith/Djinns/blue djinn.xml', look = {type = 658}, classId = 14, killAmount = 500, charmPoints = 15,
		description = {'Niebieski D�in', 'Blue Djinn'}, experience = 725, health = 1000, healthMax = 1000,
		boss = 'All a Din', chance = 50,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 50 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 13952, chance = 90}, -- crystal spear
			{id = 2656, chance = 200}, -- blue robe
			{id = 16305, chance = 220}, -- enhanced leather hood
			{id = 7620, chance = 340}, -- mana potion
			{id = 2063, chance = 730}, -- small oil lamp
			{id = 5912, chance = 1910}, -- blue piece of cloth
			{id = 1978, chance = 2410}, -- book
			{id = 2146, count = 4, chance = 2580}, -- small sapphire
			{id = 15782, chance = 4570}, -- steel spear
			{id = 2684, chance = 23790}, -- carrot
			{id = 2148, count = {15, 80}, chance = 86150}, -- copper coin
		}
	},
	{ name = 'Efreet', level = 44, file = 'Cirith/Djinns/efreet.xml', look = {type = 660, addons = 3}, classId = 14, killAmount = 500, charmPoints = 15,
		description = {'Ifryt', 'Efreet'}, experience = 1400, health = 2000, healthMax = 2000,
		boss = 'All a Din', chance = 80,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 60 },
			{ type = 'ice', value = -10 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 2156, chance = 50}, -- red gem
			{id = 11810, chance = 10}, -- fiery bow
			{id = 16305, chance = 280}, -- enhanced leather hood
			{id = 11693, chance = 620}, -- mithril ore
			{id = 2070, chance = 150}, -- wooden flute
			{id = 16203, chance = 10}, -- hellfire helmet
			{id = 8901, chance = 200}, -- spellbook of warding
			{id = 13307, chance = 90}, -- magma spear
			{id = 17705, chance = 10}, -- bone of a fire staff
			{id = 13994, chance = 20}, -- magma armor
			{id = 7732, chance = 1820}, --seeds
			{id = 5809, chance = 4070}, -- magical element
			{id = 1869, chance = 3030}, -- red tapestry
			{id = 5911, chance = 2730}, -- red piece of cloth
			{id = 2420, chance = 4860}, -- machete
			{id = 2147, count = 2, chance = 6015}, -- small ruby
			{id = 7589, chance = 950}, -- SMP
			{id = 8840, count = 25, chance = 12000}, -- raspberry
			{id = 15782, chance = 4570}, -- steel spear
			{id = 2148, count = {25, 80}, chance = 86150}, -- copper coin
		}
	},
	{ name = 'Red Djinn', level = 35, file = 'Cirith/Djinns/red djinn.xml', look = {type = 660}, classId = 14, killAmount = 500, charmPoints = 15,
		description = {'Czerwony D�in', 'Red Djinn'}, experience = 725, health = 1000, healthMax = 1000,
		boss = 'All a Din', chance = 50,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = 50 },
			{ type = 'ice', value = -10 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 5809, chance = 2100}, -- magical element
			{id = 13951, chance = 280}, -- hellfire spear
			{id = 2647, chance = 200}, -- red robe
			{id = 16305, chance = 220}, -- enhanced leather hood
			{id = 2744, chance = 440}, -- red rose
			{id = 11693, chance = 600}, -- mithril ore
			{id = 7620, chance = 340}, -- mana potion
			{id = 2063, chance = 730}, -- small oil lamp
			{id = 5911, chance = 1910}, -- red piece of cloth
			{id = 1977, chance = 2410}, -- book
			{id = 2147, count = 4, chance = 2580}, -- small ruby
			{id = 15782, chance = 4570}, -- steel spear
			{id = 2684, chance = 23790}, -- carrot
			{id = 2148, count = {15, 80}, chance = 86150}, -- copper coin
		}
	},
	
	-- Dragons
	{ name = 'Dark Draptor', level = 88, file = 'Cirith/Dragons/Dark Draptor.xml', look = {type = 443}, classId = 10, killAmount = 2500, charmPoints = 50,
		description = {'Draptor Mroku', 'Dark Draptor'}, experience = 5000, health = 8000, healthMax = 8000,
		boss = 'Old Dragon Mother', chance = 60,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 5 },
			{ type = 'death', value = 85 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 14493, chance = 450}, -- forbund
			{id = 2209, chance = 2750}, -- accuracy ring
			{id = 12999, chance = 190}, -- void hood
			{id = 2672, count = 2, chance = 8200}, -- dragon ham
			{id = {7632, 7633}, chance = 1500}, -- giant pearl
			{id = 2134, chance = 910}, -- silver brooch
			{id = 14483, chance = 250}, -- luck hat
			{id = 13592, chance = 620}, -- cursed spear
			{id = 13886, chance = 160}, -- haunted blade
			{id = 14481, chance = 190}, -- lich hat
			{id = 13001, chance = 140}, -- void legs
			{id = 13000, chance = 140}, -- void cape
			{id = 7708, chance = 10}, -- signet
			{id = 14478, chance = 1070}, -- steel helmet
			{id = 5878, chance = 2750}, -- mino leather
			{id = 12954, chance = 280}, -- marksman legs
			{id = 14014, chance = 6350}, -- robe
			{id = 16157, chance = 1120}, -- black steel armor
			{id = 2145, count = 2, chance = 3980}, -- small diamond
			{id = 14457, chance = 350}, -- spruce staff of destruction
			{id = 2154, chance = 500}, -- yellow gem
			{id = 2151, chance = 890}, -- talon
			{id = 13706, count = 2, chance = 2800}, -- beastly claw
			{id = 2152, count = 6, chance = 7230}, -- silver
			{id = 2148, count = {5, 15}, chance = 76490}, -- gold
		}
	},
	{ name = 'Dark Dragon', level = 100, file = 'Cirith/Dragons/Dark Dragon.xml', look = {type = 326}, classId = 10, killAmount = 2500, charmPoints = 50,
		description = {'Smok Cienia', 'Dark Dragon'}, experience = 2600, health = 5030, healthMax = 5030,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 80 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 2195, chance = 4350}, -- boh
			{id = 16155, chance = 75360}, -- black steel boots
			{id = 982, chance = 1000}, -- dark talon
			{id = 2151, count = {2, 6}, chance = 9500}, -- talon
			{id = 2153, chance = 2500}, -- violet gem
			{id = 11317, chance = 500}, -- dreaded spear
			{id = 13797, chance = 1500}, -- haunted shield
			{id = 2168, chance = 76160}, -- life ring
			{id = 14455, chance = 5000}, -- the creeper of darkness
			{id = 16170, chance = 1150}, -- ruby boots
			{id = 15749, chance = 2500}, -- dragon executor
			{id = 14166, chance = 4500}, -- sallet
			{id = 11221, chance = 99810}, -- shiny stone
			{id = 6300, chance = 35900}, -- death ring
			{id = 14001, chance = 25000}, -- dark armor
			{id = 16160, chance = 5000}, -- black steel bow
			{id = 13993, chance = 1000}, -- shadow armor
			{id = 11734, chance = 1500}, -- Ghastly dragon scale mail
			{id = 2150, count = {3, 5}, chance = 40500}, -- small amethyst
			{id = 2672, count = 2, chance = 8340}, -- dragon ham
			{id = 2152, count = {2, 5}, chance = 59880}, -- silver coin
			{id = 2148, count = {30, 60}, chance = 99590}, -- coin
		}
	},
	{ name = 'Dragon Hatchling', level = 18, file = 'Cirith/Dragons/dragon hatchling.xml', look = {type = 271}, classId = 10, killAmount = 250, charmPoints = 5,
		description = {'M�odszy Zielony Smok', 'Green Dragon Hatchling'}, experience = 210, health = 380, healthMax = 380,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13342, chance = 100}, -- dragon claw
			{id = 15791, chance = 490, subType = 500}, -- amber eye
			{id = 14333, chance = 150}, -- runed sword
			{id = 13155, chance = 9390}, -- untreated leather
			{id = 7618, chance = 1200}, -- health potion
			{id = 17133, chance = 200}, -- green dragon wing
			{id = 13706, chance = 2600}, -- beastly claw
			{id = 2672, chance = 72360}, -- dragon ham
			{id = 2546, count = 3, chance = 7850}, -- burst arrow
			{id = 2148, count = {16, 28}, chance = 79910}, -- coin
			{id = 2177, chance = 1650}, -- life crystal
		}
	},
	{ name = 'Dragon Lord Hatchling', level = 28, file = 'Cirith/Dragons/dragon lord hatchling.xml', look = {type = 272}, classId = 10, killAmount = 250, charmPoints = 5,
		description = {'M�odszy Czerwony Smok', 'Red Dragon Hatchling'}, experience = 410, health = 750, healthMax = 750,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13155, chance = 4800}, -- untreated leather
			{id = 13706, count = 2, chance = 1100}, -- beastly claw
			{id = 13995, chance = 100}, -- lava armor
			{id = 13981, chance = 90, subType = 190}, -- magma amulet
			{id = 7620, chance = 600}, -- mana potion
			{id = 13341, chance = 150}, -- dragon lord claw
			{id = 2796, chance = 530}, -- green mushroom
			{id = 17135, chance = 110}, -- red dragon wing
			{id = 2546, count = 7, chance = 10990}, -- burst arrow
			{id = 2148, count = {28, 47}, chance = 75470}, -- gold
			{id = 2672, chance = 79710}, -- dragon ham
			{id = 2177, chance = 2650}, -- life crystal
		}
	},
	{ name = 'Red Dragon', level = 55, file = 'Cirith/Dragons/dragon lord.xml', look = {type = 39}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'Czerwony Smok', 'Red Dragon'}, experience = 1400, health = 2100, healthMax = 2100,
		boss = 'Old Dragon Mother', chance = 50,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2157, chance = 400}, -- gold nugget
			{id = 14493, chance = 370}, -- forbund
			{id = 13987, chance = 90}, -- dsm
			{id = 11420, chance = 10}, -- dragon doll
			{id = 14480, chance = 190}, -- spartan helmet
			{id = 7399, chance = 10}, -- dragon lord trophy
			{id = 13341, chance = 210}, -- dragon lord claw
			{id = 2177, chance = 6500}, -- life crystal
			{id = 7588, chance = 970}, -- SHP
			{id = 15744, chance = 150}, -- dragon plate
			{id = 16291, chance = 370}, -- robe of the dawn
			{id = 16159, chance = 350}, -- axe of black steel
			{id = 16162, chance = 320}, -- hammer of black steel
			{id = 15717, chance = 320}, -- glorious axe
			{id = 5948, chance = 2020}, -- red dragon leather
			{id = 2167, chance = 1000}, -- energy ring
			{id = 5882, chance = 2240}, -- red dragon scale
			{id = 11404, chance = 160}, -- heavy crossbow
			{id = 11679, chance = 110}, -- hellfire bow
			{id = 17135, chance = 350}, -- red dragon wing
			{id = 2146, chance = 5120}, -- small sapphire
			{id = 1976, chance = 8980}, -- book
			{id = 2796, chance = 12280}, -- green mushroom
			{id = 2672, count = 5, chance = 80200}, -- dragon ham
			{id = 2546, count = 12, chance = 13290}, -- burst arrow
			{id = 2152, chance = 42690}, -- silver
			{id = 2148, count = {32, 66}, chance = 95260}, -- coin
		}
	},
	{ name = 'Dragon of Storm', level = 46, file = 'Cirith/Dragons/dragon of storm.xml', look = {type = 291}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'Smok Burzy', 'Dragon of Storm'}, experience = 1350, health = 1825, healthMax = 1825,
		elements = {
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2542, chance = 100}, -- lightning shield
			{id = 15730, chance = 320}, -- war axe
			{id = 10221, chance = 90, subType = 5}, -- shockwave amulet
			{id = 13892, chance = 160}, -- composite hornbow
			{id = 13938, chance = 40}, -- crossbow of spark energy
			{id = 14009, chance = 100}, -- sparkling cape
			{id = 7889, chance = 740}, -- lightning pendant
			{id = 16292, chance = 490}, -- legs of the dawn
			{id = 18073, chance = 180}, -- ethereal staff
			{id = 2145, count = 3, chance = 910}, -- small diamond
			{id = 18071, chance = 135}, -- electric rod
			{id = 14449, chance = 330}, -- wand of draconia
			{id = 18075, chance = 10}, -- purple dye
			{id = 14005, chance = 120}, -- wizard's robe
			{id = 2455, chance = 6170}, -- crossbow
			{id = 2546, count = 10, chance = 7850}, -- burst arrow
			{id = 10581, chance = 7910}, -- wyrm scale
			{id = 7589, chance = 520}, -- SMP
			{id = 7588, chance = 880}, -- SHP
			{id = 2672, count = 3, chance = 34640}, -- dragon ham
			{id = 2152, chance = 41690}, -- silver
			{id = 2148, count = {28, 69}, chance = 57020}, -- coin
		} 
	},
	{ name = 'Green Dragon', level = 40, file = 'Cirith/Dragons/dragon.xml', look = {type = 34}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'Zielony Smok', 'Green Dragon'}, experience = 730, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 51190}, -- copper set
			{id = 2177, chance = 5250}, -- life crystal
			{id = 13987, chance = 50}, -- dragon scale mail
			{id = 15721, chance = 1680}, -- ragged sword
			{id = 14350, chance = 1400}, -- iron legs
			{id = 2187, chance = 790}, -- woi
			{id = 14478, chance = 380}, -- berynit helmet
			{id = 15791, chance = 600, subType = 500}, -- amber eye
			{id = 13342, chance = 300}, -- dragon claw
			{id = 2145, chance = 1200}, -- small diamond
			{id = {14353, 14354, 14355, 14357}, chance = 1320}, -- bronze set
			{id = 5877, chance = 1970}, -- green dragon leather
			{id = 17133, chance = 350}, -- green dragon wing
			{id = 15717, chance = 1160}, -- glorious axe
			{id = 14484, chance = 2160}, -- guard's helmet
			{id = 7588, chance = 840}, -- SHP
			{id = 14358, chance = 1220}, -- bronze axe
			{id = 2546, count = 9, chance = 10000}, -- burst arrow
			{id = 5920, chance = 2050}, -- green dragon scale
			{id = 2455, chance = 10050}, -- crossbow
			{id = 2672, count = 3, chance = 65050}, -- dragon ham
			{id = 2148, count = {20, 47}, chance = 90160}, -- coin
		}
	},
	{ name = 'Frost Dragon Hatchling', level = 30, file = 'Cirith/Dragons/frost dragon hatchling.xml', look = {type = 283}, classId = 10, killAmount = 250, charmPoints = 5,
		description = {'M�odszy Smok Mrozu', 'Frost Dragon Hatchling'}, experience = 1400, health = 2100, healthMax = 2100,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'death', value = 10 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 13155, chance = 4970}, -- untreated leather
			{id = 13706, count = 2, chance = 1190}, -- beastly claw
			{id = 2656, chance = 200}, -- blue robe
			{id = 12980, chance = 120}, -- frozen crystal
			{id = 7290, chance = 220}, -- shard
			{id = 8900, chance = 390}, -- spellbook of englightenment
			{id = 7618, chance = 590}, -- health potion
			{id = 11621, chance = 30}, -- sphere frost
			{id = 18033, chance = 80}, -- teal dye
			{id = 17132, chance = 10}, -- frost dragon wing
			{id = 7441, chance = 1090}, -- ice cube
			{id = 16455, chance = 20}, -- dragon trophy
			{id = 10577, chance = 5000}, -- frosty heart
			{id = 14451, chance = 500}, -- ice cross
			{id = 2664, chance = 79690}, -- dragon ham
			{id = 2148, count = {24, 44}, chance = 86950}, -- gold
		}
	},
	{ name = 'Frost Dragon', level = 60, file = 'Cirith/Dragons/frost dragon.xml', look = {type = 248}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'Smok Mrozu', 'Frost Dragon'}, experience = 3350, health = 5120, healthMax = 5120,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = 10 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 13889, chance = 150}, -- frozen axe
			{id = 13987, chance = 150}, -- DSM
			{id = 18033, chance = 200}, -- teal dye
			{id = 7389, chance = 200}, -- dragon slayer
			{id = 16191, chance = 190}, -- frozen armor
			{id = 16455, chance = 90}, -- dragon trophy
			{id = 16910, chance = 230}, -- a crystal rod of frost
			{id = 11621, chance = 80}, -- sphere frost
			{id = 14480, chance = 210}, -- spartan helmet
			{id = 14319, chance = 190}, -- tower shield
			{id = 13894, chance = 450}, -- ice rapier
			{id = 13299, chance = 450}, -- glass spear
			{id = 17704, chance = 10}, -- the frosty midnight fire
			{id = 14479, chance = 120}, -- strange helmet
			{id = 13937, chance = 150}, -- frozen crossbow
			{id = 2177, chance = 8900}, -- life crystal
			{id = 17132, chance = 350}, -- frost dragon wing
			{id = 7290, chance = 550}, -- shard
			{id = 7588, chance = 770}, -- SHP
			{id = 2033, chance = 2880}, -- golden mug
			{id = 7441, chance = 4040}, -- ice cube
			{id = 2167, chance = 4950}, -- energy ring
			{id = 10577, chance = 5000}, -- frosty heart
			{id = 2146, chance = 5110}, -- small sapphire
			{id = 2547, count = 6, chance = 5820}, -- power bolt
			{id = 1976, chance = 8930}, -- book
			{id = 2796, chance = 12030}, -- green mushroom
			{id = 2664, count = 3, chance = 80220}, -- dragon ham
			{id = 2152, chance = 41690}, -- silver
			{id = 2148, count = {45, 76}, chance = 95100}, -- coin
		}
	},
	{ name = 'Ghastly Dragon', level = 98, file = 'Cirith/Dragons/ghastly dragon.xml', look = {type = 351}, classId = 10, killAmount = 2500, charmPoints = 50,
		description = {'Smok Cienia', 'Ghastly Dragon'}, experience = 5100, health = 7800, healthMax = 7800,
		boss = 'Old Dragon Mother', chance = 80,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 15728, chance = 130}, -- Skeleton striking face
			{id = 11656, chance = 10}, -- ghastly dragon doll
			{id = 14493, chance = 570}, -- forbund
			{id = 16156, chance = 230}, -- black steel legs
			{id = 14009, chance = 120}, -- lightning robe
			{id = 16170, chance = 110}, -- ruby boots
			{id = 8903, chance = 50}, -- spellbook of lost souls
			{id = 16158, chance = 130}, -- black steel helmet
			{id = 16157, chance = 320}, -- black steel armor
			{id = 17871, chance = 150}, -- darkness blade
			{id = 11694, chance = 1390}, -- fine fabric
			{id = 16155, chance = 120}, -- black steel shoes
			{id = 15744, chance = 140}, -- dragon plate
			{id = 5892, chance = 2610}, -- steel
			{id = 9971, chance = 1020}, -- gold ingot
			{id = 16147, chance = 140}, -- Mask of the fallen
			{id = {13945, 13944}, chance = 280}, -- ashbringer hood
			{id = 11734, chance = 190}, -- Ghastly dragon scale mail
			{id = 11221, chance = 810}, -- shiny stone
			{id = 13888, chance = 580}, -- vile axe
			{id = 15757, chance = 490}, -- terra legs
			{id = 11360, chance = 6860}, -- ghastly dragon head
			{id = 6500, chance = 930}, -- demonic essence
			{id = 9810, chance = 1370}, -- rare rusty armor
			{id = 15758, chance = 940}, -- snake boots
			{id = 5944, chance = 1160}, -- soul orb
			{id = 6300, chance = 1940}, -- death ring
			{id = 15718, chance = 5190}, -- chopper
			{id = 17134, chance = 350}, -- ghastly dragon wing
			{id = 11361, chance = 19580}, -- undead heart
			{id = 8473, chance = 530}, -- UHP
			{id = 2152, count = 3, chance = 29880}, -- silver coin
			{id = 8472, chance = 320}, -- UMP
			{id = 14001, chance = 1590}, -- dark armor
			{id = 2149, count = 5, chance = 40500}, -- small emerald
			{id = 14350, chance = 560}, -- iron legs
			{id = 2148, count = {44, 78}, chance = 99590}, -- coin
		}
	},
	{ name = 'Sapphire Dragon', level = 112, file = 'Cirith/Dragons/sapphire dragon.xml', look = {type = 275}, classId = 10, killAmount = 2500, charmPoints = 50,
		description = {'Szafirowy Smok', 'Sapphire Dragon'}, experience = 16000, health = 20000, healthMax = 20000,
		boss = 'Aqualius', chance = 80,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 35 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 10529, chance = 10}, -- sea serpent doll
			{id = 19180, chance = 100}, -- ??? parchment
			{id = 2144, count = 2, chance = 5420}, -- black pearl
			{id = 2143, count = 2, chance = 5550}, -- white pearl
			{id = 17941, chance = 2610}, -- radiating powder
			{id = 2002, chance = 100}, -- blue backpack
			{id = 11955, chance = 120}, -- jewelled backpack
			{id = 13996, chance = 230}, -- crystalline armor
			{id = 10220, chance = 3120, subType = 10}, -- leviathan's amulet
			{id = 16194, chance = 100}, -- frosty mace
			{id = 14343, chance = 2390}, -- alpha shanir
			{id = 14005, chance = 210}, -- wizard's robe
			{id = 11621, chance = 220}, -- sphere frost
			{id = 2165, chance = 410}, -- stealth ring
			{id = {16190, 16191, 16192}, chance = 350}, -- frosty set
			{id = 7590, chance = 920}, -- GMP
			{id = 13937, chance = 700}, -- icey crossbow
			{id = 14504, chance = 800}, -- magical thread
			{id = 2214, chance = 1120}, -- ring of healing
			{id = 2656, chance = 1140}, -- blue robe
			{id = 7589, chance = 400}, -- SMP
			{id = 14012, chance = 880}, -- crimson sword
			{id = 17131, chance = 350}, -- sapphire dragon wing
			{id = 7588, chance = 580}, -- SHP
			{id = 2146, count = 3, chance = 5740}, -- small saphire
			{id = 13950, chance = 1050}, -- frost spear
			{id = 17940, chance = 280}, -- pure mithril
			{id = 14327, chance = 620}, -- steel legs
			{id = 10582, chance = 10120}, -- sea serpent scale
			{id = 2152, count = {3, 5}, chance = 51720}, -- silver coin
			{id = 2672, count = 2, chance = 60470}, -- dragon ham
			{id = 2148, count = {11, 89}, chance = 98250}, -- coin
		}
	},
	{ name = 'Swamply Dragon', level = 50, file = 'Cirith/Dragons/swamply dragon.xml', look = {type = 34}, killAmount = 1000, charmPoints = 25,
		description = {'Bagienny Smok', 'Swamply Dragon'}, experience = 790, health = 1500, healthMax = 1500,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 80 },
		},
		loot = {
			{id = 2152, chance = 41690}, -- silver
			{id = 2148, count = 28, chance = 70000}, -- gold
		}
	},
	{ name = 'Wyvern', level = 42, file = 'Cirith/Dragons/wyvern.xml', look = {type = 678}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'Wiwerna', 'Wyvern'}, experience = 520, health = 795, healthMax = 795,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 70 },
		},
		loot = {
			{id = 13155, chance = 3190}, -- untreated leather
			{id = 13706, chance = 1790}, -- beastly claw
			{id = 14356, chance = 50}, -- bronze boots
			{id = 11436, count = 2, chance = 690}, -- crystalline bolt
			{id = 15761, chance = 30}, -- snake blade
			{id = 2127, chance = 70}, -- emerald bangle
			{id = 5892, chance = 1500}, -- steel
			{id = 10559, chance = 1400}, -- horn
			{id = 8849, chance = 250}, -- modified crossbow
			{id = 7588, chance = 480}, -- SHP
			{id = 2547, chance = 3390}, -- power bolt
			{id = 18338, chance = 80}, -- red gem
			{id = 2146, chance = 5820}, -- small sapphire
			{id = 10560, chance = 12130}, -- wyvern talisman
			{id = 13344, chance = 12890}, -- wyvern head
			{id = 13387, chance = 21890}, -- wyvern thorn
			{id = 2672, count = 3, chance = 60290}, -- dragon ham
			{id = 2148, count = {23, 51}, chance = 100000}, -- gold
		}
	},
	{ name = 'Young Sapphire Dragon', level = 60, file = 'Cirith/Dragons/young sapphire dragon.xml', look = {type = 317}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'M�ody Szafirowy Smok', 'Young Sapphire Dragon'}, experience = 3800, health = 5200, healthMax = 5200,
		boss = 'Aqualius', chance = 40,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 35 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 10529, chance = 10}, -- sea serpent doll
			{id = 19180, chance = 10}, -- ??? parchment
			{id = 2144, count = 2, chance = 3580}, -- black pearl
			{id = 2143, count = 2, chance = 3690}, -- white pearl
			{id = 17941, chance = 1520}, -- radiating powder
			{id = 14343, chance = 10}, -- alpha shanir
			{id = 13996, chance = 80}, -- crystalline armor
			{id = 16194, chance = 5}, -- frosty mace
			{id = 11955, chance = 50}, -- jewelled backpack
			{id = {16190, 16191, 16192}, chance = 160}, -- frosty legs
			{id = 2165, chance = 90}, -- stealth ring
			{id = 13937, chance = 300}, -- icey crossbow
			{id = 11621, chance = 170}, -- sphere frost
			{id = 2002, chance = 100}, -- blue backpack
			{id = 14451, chance = 500}, -- ice cross
			{id = 14005, chance = 50}, -- wizard's robe
			{id = 10220, chance = 200, subType = 4}, -- leviathan's amulet
			{id = 7590, chance = 420}, -- GMP
			{id = 2214, chance = 820}, -- ring of healing
			{id = 14327, chance = 1220}, -- steel legs
			{id = 2656, chance = 940}, -- blue robe
			{id = 14012, chance = 1180}, -- crimson sword
			{id = 17131, chance = 10}, -- sapphire dragon wing
			{id = 7588, chance = 480}, -- SHP
			{id = 2146, count = 2, chance = 8740}, -- small saphire
			{id = 13950, chance = 370}, -- frost spear
			{id = 7589, chance = 400}, -- SMP
			{id = 10582, chance = 10120}, -- sea serpent scale
			{id = 2152, count = 3, chance = 47720}, -- silver coin
			{id = 2672, chance = 60470}, -- dragon ham
			{id = 2148, count = {45, 78}, chance = 98250}, -- copper coin
		}
	},
	{ name = 'Young Swamply Dragon', level = 28, file = 'Cirith/Dragons/young swamply dragon.xml', look = {type = 575}, classId = 10, killAmount = 1000, charmPoints = 25,
		description = {'M�odszy Bagienny Smok', 'Young Swamply Dragon'}, experience = 255, health = 450, healthMax = 450,
		elements = {
			{ type = 'energy', value = 95 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 16363, chance = 10}, -- swamplair backpack
			{id = 16168, chance = 20}, -- swamplair helmet
			{id = 13585, chance = 150}, -- a foul rod
			{id = 13155, chance = 10000}, -- untreated leather
			{id = 15756, chance = 70}, -- snake armor
			{id = 14453, chance = 210}, -- swamplair rod
			{id = 13706, chance = 1490}, -- beastly claw
			{id = 13980, chance = 440, subType = 50}, -- earth absorb amulet
			{id = 16932, chance = 520}, -- health potion
			{id = 15720, chance = 490}, -- broadsword
			{id = 2149, count = 2, chance = 2000}, -- small emerald
			{id = 5877, chance = 2190}, -- green dragon leather
			{id = 2672, chance = 72360}, -- dragon ham
			{id = 2148, count = 41, chance = 79910}, -- coin
		}
	},
	
	-- Drakens
	{ name = 'Draken Spellweaver', level = 86, file = 'Cirith/Drakens/draken spellweaver.xml', look = {type = 340}, classId = 8, killAmount = 2500, charmPoints = 50,
		description = {'Smokon Tkacz Zakl��', 'Draken Spellweaver'}, experience = 3250, health = 5000, healthMax = 5000,
		boss = 'Kiri Norol', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 75 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 60 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 14504, chance = 310}, -- magical thread
			{id = 17941, chance = 1120}, -- radiating powder
			{id = 11309, chance = 10}, -- draken trophy
			{id = 2123, chance = 80}, -- Rots
			{id = 2647, chance = 120}, -- red robe
			{id = 14009, chance = 150}, -- spellweaver robe
			{id = 16156, chance = 140}, -- black steel legs
			{id = 2155, chance = 50}, -- green gem
			{id = 14006, chance = 470}, -- cloak of fear
			{id = 2187, chance = 1580}, -- WoI
			{id = 16155, chance = 390}, -- black steel shoes
			{id = 10028, chance = 3940}, -- draken sulphur
			{id = 7590, chance = 1630}, -- GMP
			{id = 2147, count = 5, chance = 6990}, -- small ruby
			{id = 11308, chance = 20010}, -- weaver's wandtip
			{id = 2152, chance = 25050}, -- silver coin
			{id = 2666, chance = 29920}, -- meat
			{id = 2148, count = 30, chance = 97360}, -- coin
		}
	},
	{ name = 'Draken Warmaster', level = 90, file = 'Cirith/Drakens/draken warmaster.xml', look = {type = 334}, classId = 8, killAmount = 2500, charmPoints = 50,
		description = {'Smokon Mistrz Wojenny', 'Draken Warmaster'}, experience = 2950, health = 4150, healthMax = 4150,
		boss = 'Kiri Norol', chance = 110,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 15 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 14493, chance = 500}, -- forbund
			{id = 5887, chance = 900}, -- royal steel
			{id = 2123, chance = 10}, -- rots
			{id = 16157, chance = 390}, -- black steel armor
			{id = 11299, chance = 340}, -- drakinata
			{id = 16156, chance = 180}, -- black steel legs
			{id = 2147, count = 5, chance = 1580}, -- small ruby
			{id = 14007, chance = 1190}, -- guardian shield
			{id = {13055, 13070, 13058, 13061}, chance = 460}, -- bronze crafting additives
			{id = 16155, chance = 540}, -- black steel shoes
			{id = 8473, chance = 980}, -- UHP
			{id = 7591, count = 2, chance = 530}, -- GHP
			{id = 11316, chance = 7010}, -- warmaster's wristguards
			{id = 14012, chance = 2010}, -- crimson sword
			{id = 11315, chance = 12040}, -- bone shoulderplate
			{id = 2666, chance = 30190}, -- meat
			{id = 2152, chance = 50300}, -- silver coin
			{id = 2148, count = 39, chance = 97360}, -- coin
		}
	},
	{ name = 'Draken Elite', level = 106, file = 'Cirith/Drakens/draken elite.xml', look = {type = 373}, classId = 8, killAmount = 2500, charmPoints = 50,
		description = {'Elitarny Smokon', 'Draken Elite'}, experience = 3900, health = 5550, healthMax = 5550,
		boss = 'Kiri Norol', chance = 150,
		elements = {
			{ type = 'holy', value = 30 },
			{ type = 'death', value = 30 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 30 },
			{ type = 'physical', value = 25 },
		},
		loot = {
			{id = 16480, chance = 10}, -- draken doll
			{id = 16173, chance = 70}, -- ruby helmet
			{id = 13895, chance = 210}, -- cursed blade
			{id = 13989, chance = 150}, -- robe of underground
			{id = 11734, chance = 150}, -- elite draken mail
			{id = 16158, chance = 220}, -- black steel helmet
			{id = 16159, chance = 400}, -- black steel axe
			{id = 16157, chance = 450}, -- black steel armor
			{id = 16205, chance = 160}, -- meteorite boots
			{id = 15734, chance = 640}, -- vile axe
			{id = 16156, chance = 280}, -- black steel legs
			{id = 11732, chance = 330}, -- twiceslicer
			{id = 5904, chance = 840}, -- magic sulphur
			{id = 2145, count = 4, chance = 2460}, -- small diamond
			{id = 10028, chance = 7990}, -- draken sulphur
			{id = 7590, count = 3, chance = 450}, -- GMP
			{id = 8473, count = 3, chance = 340}, -- UHP
			{id = 11733, chance = 24800}, -- broken slicer
			{id = 2666, chance = 29920}, -- meat
			{id = 2152, chance = 50230}, -- silver coin
			{id = 2148, count = 45, chance = 97240}, -- coin
		}
	},
	{ name = 'Draken Abomination', level = 116, file = 'Cirith/Drakens/draken abomination.xml', look = {type = 369}, classId = 8, killAmount = 2500, charmPoints = 50,
		description = {'Smokon Abominacja', 'Draken Abomination'}, experience = 4400, health = 6250, healthMax = 6250,
		boss = 'Kiri Norol', chance = 180,
		elements = {
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 90 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = 15 },
			{ type = 'physical', value = 30 },
		},
		loot = {
			{id = 15728, chance = 100}, -- skeleton striking face
			{id = 13989, chance = 120}, -- robe of underground
			{id = 13925, chance = 5}, -- cursed shield
			{id = 16205, chance = 140}, -- meteorite boots
			{id = 16158, chance = 230}, -- black steel helmet
			{id = 16157, chance = 290}, -- black steel armor
			{id = 17871, chance = 200}, -- darkness blade
			{id = 6534, chance = 510}, -- flying trident
			{id = 16156, chance = 160}, -- black steel legs
			{id = 14455, chance = 700}, -- wand of voodoo
			{id = 14318, chance = 190}, -- royal steel blade
			{id = 14322, chance = 350}, -- royal steel helmet
			{id = 9970, count = 4, chance = 2810}, -- small topaz
			{id = 8472, count = 3, chance = 470}, -- GSP
			{id = 14006, chance = 2310}, -- cloak of fear
			{id = 7590, count = 3, chance = 100}, -- GMP
			{id = 8473, count = 3, chance = 270}, -- UHP
			{id = 2152, chance = 50160}, -- silver coin
			{id = 2666, count = 4, chance = 50210}, -- meat
			{id = 2148, count = 7, chance = 97170}, -- coin
		}
	},
	
	--  Dwarves 
	{ name = 'Dwarf Geomancer', level = 26, file = 'Cirith/Dwarves/dwarf geomancer.xml', look = {type = 66}, classId = 22, killAmount = 1000, charmPoints = 25,
		description = {'Krasnolud Czarnoksi�nik', 'Dwarf Geomancer'}, experience = 280, health = 420, healthMax = 420,
		boss = 'Durin', chance = 90,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -1 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 60 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 16459, chance = 50}, -- dwarf trophy
			{id = 16195, chance = 110}, -- druid boots
			{id = 2146, chance = 1690}, -- small sapphire
			{id = 17870, chance = 320}, -- melted axe
			{id = 13754, chance = 9990}, -- rope belt
			{id = 5880, chance = 5100}, -- iron ore
			{id = 15790, chance = 1050}, -- copper axe
			{id = 11693, chance = 500}, -- mithril ore
			{id = 11694, chance = 790}, -- fine fabric
			{id = 13729, chance = 7940}, -- geomancer robe
			{id = 2673, chance = 2441}, -- pear
			{id = 2787, count = 2, chance = 59020}, -- white mushroom
			{id = 2148, count = 21, chance = 74750}, -- gold
		}
	},
	{ name = 'Dwarf Guard', level = 30, file = 'Cirith/Dwarves/dwarf guard.xml', look = {type = 70}, classId = 22, killAmount = 500, charmPoints = 15,
		description = {'Krasnolud Obro�ca', 'Dwarf Guard'}, experience = 165, health = 245, healthMax = 245,
		boss = 'Durin', chance = 100,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 16459, chance = 40}, -- dwarf trophy
			{id = 2213, chance = 940}, -- dwarven ring
			{id = 2214, chance = 320}, -- ring of endurance
			{id = 2643, chance = 860}, -- breastplate
			{id = 14432, chance = 50}, -- cobalt helmet
			{id = 17656, chance = 330}, -- copper blade
			{id = 8849, chance = 250}, -- modified crossbow
			{id = 17870, chance = 500}, -- melted axe
			{id = 5892, chance = 750}, -- steel
			{id = 2157, chance = 300}, -- gold nugget
			{id = 2150, chance = 1600}, -- small amethyst
			{id = 5887, chance = 330}, -- royal steel
			{id = 15713, chance = 260}, -- longsword
			{id = 2440, chance = 160}, -- daramanian axe
			{id = 14175, chance = 12300}, -- coal
			{id = 7618, chance = 340}, -- health potion
			{id = 5880, chance = 4470}, -- iron ore
			{id = 2148, count = 20, chance = 40190}, -- gold
			{id = 2787, count = 2, chance = 55540}, -- white mushroom
		}
	},
	{ name = 'Dwarf Soldier', level = 24, file = 'Cirith/Dwarves/dwarf soldier.xml', look = {type = 71}, classId = 22, killAmount = 500, charmPoints = 15,
		description = {'Krasnolud �o�nierz', 'Dwarf Soldier'}, experience = 80, health = 135, healthMax = 135,
		boss = 'Durin', chance = 60,
		elements = {
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 16459, chance = 30}, -- dwarf trophy
			{id = 2213, chance = 940}, -- dwarven ring
			{id = 9971, chance = 150}, -- gold ignot
			{id = 11404, chance = 70}, -- heavy crossbow
			{id = 14461, chance = 6290}, -- soldier helmet
			{id = 2157, chance = 220}, -- gold nugget
			{id = 17870, chance = 370}, -- melted axe
			{id = 2455, chance = 1960}, -- crossbow
			{id = 13754, chance = 10000}, -- rope belt
			{id = 13707, chance = 510}, -- cuprite
			{id = 5880, chance = 4890}, -- iron ore
			{id = 2554, chance = 9930}, -- shovel
			{id = 2148, count = 12, chance = 28350}, -- gold
			{id = 7363, count = 5, chance = 5140}, -- piercing bolt
			{id = 11438, count = 7, chance = 37600}, -- bolt
			{id = 2787, count = 2, chance = 39880}, -- white mushroom
		}
	},
	{ name = 'Dwarf', level = 20, file = 'Cirith/Dwarves/dwarf.xml', look = {type = 69, head = 20, body = 30, legs = 40, feet = 50}, classId = 22, killAmount = 500, charmPoints = 15,
		description = {'Krasnolud', 'Dwarf'}, experience = 55, health = 90, healthMax = 90,
		boss = 'Durin', chance = 40,
		elements = {
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 16459, chance = 20}, -- dwarf trophy
			{id = {13055, 13070, 13058, 13061}, chance = 220}, -- bronze crafting additives
			{id = 2213, chance = 940}, -- dwarven ring
			{id = 9971, chance = 100}, -- gold ignot
			{id = 15714, chance = 7800}, -- club
			{id = 17870, chance = 280}, -- melted axe
			{id = 5880, chance = 2860}, -- iron ore
			{id = 13754, chance = 8940}, -- rope belt
			{id = 2157, chance = 10}, -- gold nugget
			{id = 14461, chance = 4000}, -- soldier helmet
			{id = 13707, chance = 900}, -- cuprite
			{id = 14478, chance = 310}, -- berynit helmet
			{id = 2553, chance = 10050}, -- pick
			{id = 2597, chance = 7920}, -- letter
			{id = 15783, chance = 9060}, -- breaker
			{id = 2148, count = 8, chance = 35200}, -- gold
			{id = 2787, chance = 50250}, -- white mushroom
		}
	},
	{ name = 'Lost Husher', level = 58, file = 'Cirith/Dwarves/lost husher.xml', look = {type = 531}, classId = 22, killAmount = 1000, charmPoints = 25,
		description = {'Wygnany Krasnolud', 'Exiled Dwarf'}, experience = 1000, health = 1500, healthMax = 1500,
		boss = 'Throer', chance = 90,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 16459, chance = 90}, -- dwarf trophy
			{id = 17870, chance = 880}, -- melted axe
			{id = 9971, chance = 300}, -- gold ignot
			{id = 2157, chance = 270}, -- gold nugget
			{id = 14319, chance = 190}, -- royal steel shield
			{id = 15757, chance = 280}, -- snake legs
			{id = 14456, chance = 160}, -- oak staff of the earth
			{id = 15758, chance = 220}, -- snake boots
			{id = 15719, chance = 720}, -- battle hammer
			{id = 5880, chance = 2860}, -- iron ore
			{id = 14457, chance = 140}, -- spruce staff of destruction
			{id = 14170, chance = 480}, -- armorour legionnaires
			{id = 11693, chance = 1180}, -- mithril ore
			{id = 13707, chance = 900}, -- cuprite
			{id = 14007, chance = 480}, -- guardian shield
			{id = 2213, chance = 2650}, -- dwarven ring
			{id = 2553, chance = 10050}, -- pick
			{id = 9970, chance = 9720}, -- small topaz
			{id = 7591, count = 2, chance = 520}, -- strong health potion
			{id = 14175, chance = 11830}, -- coal
			{id = 2789, count = 2, chance = 15270}, -- brown mushroom
			{id = 2152, count = 2, chance = 19640}, -- silver
			{id = 2148, count = {16, 48}, chance = 85200}, -- gold
		}
	},
	{ name = 'Lost Thrower', level = 60, file = 'Cirith/Dwarves/lost thrower.xml', look = {type = 533}, classId = 22, killAmount = 1000, charmPoints = 25,
		description = {'Odrzucony Krasnolud', 'Banished Dwarf'}, experience = 1150, health = 1790, healthMax = 1790,
		boss = 'Throer', chance = 100,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 16459, chance = 80}, -- dwarf trophy
			{id = 13707, chance = 900}, -- cuprite
			{id = 2158, chance = 290}, -- blue gem
			{id = 14349, chance = 560}, -- iron helmet
			{id = 13991, chance = 210}, -- mythril armor
			{id = 2213, chance = 2710}, -- dwarven ring
			{id = 12952, chance = 300}, -- bronze helmet
			{id = 14326, chance = 140}, -- steel boots
			{id = 2157, chance = 250}, -- gold nugget
			{id = 15729, chance = 190}, -- double axe
			{id = 17870, chance = 920}, -- melted axe
			{id = 9970, chance = 9720}, -- small topaz
			{id = 11693, chance = 950}, -- mithril ore
			{id = 5880, chance = 4200}, -- iron ore
			{id = 7591, count = 2, chance = 520}, -- strong health potion
			{id = 14175, chance = 12140}, -- coal
			{id = 2789, count = 2, chance = 15050}, -- brown mushroom
			{id = 2152, chance = 20220}, -- silver
			{id = 2148, count = {12, 51}, chance = 92380}, -- gold
		}
	},
	{ name = 'Lost Basher', level = 64, file = 'Cirith/Dwarves/lost basher.xml', look = {type = 532}, classId = 22, killAmount = 1000, charmPoints = 25,
		description = {'Stracony Krasnolud', 'Lost Dwarf'}, experience = 1120, health = 1700, healthMax = 1700,
		boss = 'Throer', chance = 110,
		elements = {
			{ type = 'death', value = 15 },
			{ type = 'energy', value = 5 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 16459, chance = 10}, -- dwarf trophy
			{id = 2156, chance = 250}, -- blue gem
			{id = 16473, chance = 240}, -- spiky squelcher
			{id = 2391, chance = 10}, -- war hammer
			{id = 9971, chance = 315}, -- gold ignot
			{id = 2392, chance = 240}, -- fire sword
			{id = 13896, chance = 160}, -- giant sword
			{id = 14327, chance = 340}, -- plate legs
			{id = 13707, chance = 990}, -- cuprite
			{id = 16165, chance = 530}, -- swamplair boots
			{id = 15732, chance = 270}, -- knight axe
			{id = 13888, chance = 770}, -- lightning axe
			{id = 2213, chance = 2340}, -- dwarven ring
			{id = 14339, chance = 450}, -- iron shield
			{id = 2114, chance = 3940}, -- piggy bank
			{id = 5880, chance = 5000}, -- iron ore
			{id = 7591, count = 2, chance = 590}, -- strong health potion
			{id = 9970, chance = 10170}, -- small topaz
			{id = 2789, count = 2, chance = 14940}, -- brown mushroom
			{id = 14175, chance = 20290}, -- coal
			{id = 2152, chance = 19660}, -- silver
			{id = 2148, count = {10, 45}, chance = 99390}, -- gold
		}
	},
	
	-- Electro-Elementals
	{ name = 'Charged Energy Elemental', level = 70, file = 'Cirith/Electro-Elementals/charged energy elemental.xml', look = {type = 293}, classId = 26, killAmount = 1000, charmPoints = 25,
		description = {'Na�adowany �ywio�ak Energii', 'Charged Energy Elemental'}, experience = 720, health = 1000, healthMax = 1000,
		boss = 'Electro', chance = 100,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -15 },
			{ type = 'fire', value = 65 },
			{ type = 'ice', value = 95 },
		},
		loot = {
			{id = 2001, chance = 20}, -- purple backpack
			{id = 14007, chance = 450}, -- guardian shield
			{id = 8303, chance = 1500}, -- energy soil
			{id = 2150, count = 2, chance = 1450}, -- small amethyst
			{id = 10552, chance = 5710}, -- fierly heart
			{id = 7838, count = 3, chance = 7220}, -- flash arrow
			{id = 10581, chance = 7600}, -- wyrm scale
			{id = 7620, chance = 750}, -- mana potion
			{id = 2152, chance = 14680}, -- silver
			{id = 2148, count = 12, chance = 99980}, -- gold
		}
	},
	{ name = 'Overcharged Energy Elemental', level = 82, file = 'Cirith/Electro-Elementals/overcharged energy elemental.xml', look = {type = 290}, classId = 26, killAmount = 1000, charmPoints = 25,
		description = {'Prze�adowany �ywio�ak Energii', 'Overcharged Energy Elemental'}, experience = 1900, health = 2500, healthMax = 2500,
		boss = 'Electro', chance = 120,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 65 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 2542, chance = 10}, -- lightning shield
			{id = 2001, chance = 20}, -- purple backpack
			{id = 13938, chance = 150}, -- thunderstorm crossbow
			{id = 8902, chance = 250}, -- SB of mind control
			{id = 13888, chance = 310}, -- lightning axe
			{id = 2656, chance = 450}, -- blue robe
			{id = 8303, chance = 2400}, -- energy soil
			{id = 2214, chance = 1220}, -- RoH
			{id = 2150, count = 2, chance = 1450}, -- small amethyst
			{id = 14459, chance = 2000}, -- magic rod
			{id = 7838, count = 3, chance = 7220}, -- flash arrow
			{id = 10581, chance = 11000}, -- wyrm scale
			{id = 2152, chance = 14680}, -- silver
			{id = 7620, chance = 1150}, -- mana potion
			{id = 2148, count = 15, chance = 99980}, -- gold
		}
	},
	{ name = 'Energy Suppressor', level = 80, file = 'Cirith/Electro-Elementals/energy suppressor.xml', look = {type = 293}, classId = 26, killAmount = 2500, charmPoints = 50,
		description = {'T�umik Energii', 'Energy Suppressor'}, experience = 1900, health = 2700, healthMax = 2700,
		boss = 'Electro', chance = 150,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 2542, chance = 10}, -- lightning shield
			{id = 8902, chance = 300}, -- sb mind control
			{id = 14432, chance = 420}, -- cobalt helmet
			{id = 2001, chance = 500}, -- purple backpack
			{id = 8920, chance = 260}, -- wand of starstorm
			{id = 10221, chance = 700, subType = 5}, -- shockwave amulet
			{id = 2656, chance = 770}, -- blue robe
			{id = 13888, chance = 800}, -- lightning axe
			{id = 18338, chance = 90}, -- red gem
			{id = 2158, chance = 90}, -- blue gem
			{id = 7889, chance = 1100}, -- lightning peadant
			{id = 14349, chance = 960}, -- iron helmet
			{id = 15720, chance = 6000}, -- broad sword
			{id = 2150, count = 3, chance = 8500}, -- small amethyst
			{id = 7838, count = 10, chance = 12000}, -- flash arrow
			{id = 2148, count = 13, chance = 98100}, -- gold
		}
	},
	{ name = 'Electric Glimmer', level = 100, file = 'Cirith/Electro-Elementals/electric glimmer.xml', look = {type = 290}, classId = 26, killAmount = 2500, charmPoints = 50,
		description = {'Elektryczny Blask', 'Electric Glimmer'}, experience = 5800, health = 7300, healthMax = 7300,
		boss = 'Electro', chance = 190,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 16207, chance = 10}, -- meteorite armor
			{id = 13888, chance = 220}, -- lightning axe
			{id = 13794, chance = 250}, -- titan helmet
			{id = 14009, chance = 10}, -- sparkling robe
			{id = 14005, chance = 90}, -- wizard's robe
			{id = 2542, chance = 50}, -- lightning shield
			{id = 10221, chance = 700, subType = 10}, -- shockwave amulet
			{id = 16162, chance = 760}, -- hammer of black steel
			{id = 8901, chance = 960}, -- SB of warding
			{id = 8920, chance = 1150}, -- wand of starstorm
			{id = 7889, chance = 1200}, -- lightning pendant
			{id = 2158, chance = 130}, -- blue gem
			{id = 2168, chance = 2300}, -- life ring
			{id = 14317, chance = 3300}, -- enchanced oak shield
			{id = 2189, chance = 4150}, -- seeing eye
			{id = 2150, count = 3, chance = 8500}, -- small amethyst
			{id = 7590, chance = 2000}, -- GMP
			{id = 7838, count = 13, chance = 12000}, -- flash arrow
			{id = 7589, chance = 1000}, -- SMP
			{id = 2152, chance = 33590}, -- silver
			{id = 2148, count = 19, chance = 98100}, -- gold
		}
	},
	{ name = 'Energy Elemental', level = 40, file = 'Cirith/Electro-Elementals/energy elemental.xml', look = {type = 293}, classId = 26, killAmount = 1000, charmPoints = 25,
		description = {'�ywio�ak Energii', 'Energy Elemental'}, experience = 265, health = 500, healthMax = 500,
		boss = 'Electro', chance = 100,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 14007, chance = 150}, -- guardian shield
			{id = 8900, chance = 270}, -- spellbook
			{id = 14459, chance = 630}, -- wand
			{id = 2167, chance = 900}, -- energy ring
			{id = 2170, chance = 1020}, -- silver amulet
			{id = 5809, chance = 5000}, -- magical element
			{id = 2124, chance = 2100}, -- crystal ring
			{id = 14358, chance = 3560}, -- bronze axe
			{id = 2150, count = 2, chance = 5080}, -- small amethyst
			{id = 15720, chance = 6030}, -- broad sword
			{id = 7589, chance = 710}, -- SMP
			{id = 18071, chance = 10}, -- electric rod
			{id = 7838, count = 10, chance = 10140}, -- flash arrow
			{id = 7620, chance = 750}, -- mana potion
			{id = 2148, count = 15, chance = 98100}, -- gold
		}
	},
	{ name = 'Massive Energy Elemental', level = 60, file = 'Cirith/Electro-Elementals/massive energy elemental.xml', look = {type = 290}, classId = 26, killAmount = 1000, charmPoints = 25,
		description = {'Wielki �ywio�ak Energii', 'Energy Elemental'}, experience = 820, health = 1100, healthMax = 1100,
		boss = 'Electro', chance = 120,
		elements = {
			{ type = 'physical', value = 70 },
			{ type = 'holy', value = 25 },
			{ type = 'death', value = 1 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 14009, chance = 100}, -- lightning robe
			{id = 10221, chance = 150}, -- shockwave amulet
			{id = 13888, chance = 300}, -- lightning axe
			{id = 8901, chance = 360}, -- spellbook of warding
			{id = 14007, chance = 500}, -- guardian shield
			{id = 14459, chance = 720}, -- wand
			{id = 7889, chance = 720}, -- lightning pendant
			{id = 18338, chance = 100}, -- red gem
			{id = 2150, count = 3, chance = 3240}, -- small amethyst
			{id = 7590, chance = 1000}, -- gmp
			{id = 7838, count = 15, chance = 6470}, -- flash arrow
			{id = 7589, chance = 630}, -- smp
			{id = 2148, count = 17, chance = 98560}, -- gold
			{id = 18071, chance = 10}, -- electric rod
		}
	},
	
	-- Elves
	{ name = 'Elf Arcanist', level = 41, file = 'Cirith/Elves/elf arcanist.xml', look = {type = 63}, classId = 21, killAmount = 500, charmPoints = 15,
		description = {'Elf Arkanista', 'Elf Arcanist'}, experience = 620, health = 900, healthMax = 900,
		boss = 'Wrath of Nature', chance = 100,
		elements = {
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'fire', value = 50 },
		},
		loot = {
			{id = 5922, chance = 2070}, -- holy orchid
			{id = 2047, chance = 2080}, -- candlestick
			{id = 7589, chance = 300}, -- SMP
			{id = 7618, chance = 240}, -- health potion
			{id = 2154, chance = 10}, -- yellow gem
			{id = 2143, chance = 1990}, -- white pearl
			{id = 2747, chance = 870}, -- gave flower
			{id = 11694, chance = 940}, -- fine fabric
			{id = 2642, chance = 1300}, -- sandals
			{id = 2802, chance = 4940}, -- sling herb
			{id = 5809, chance = 3500}, -- magical element
			{id = {13906, 13907, 13908, 13909, 13911, 13910}, chance = 760}, -- Wradon's set
			{id = 10551, chance = 10110}, -- elvish talisman
			{id = 2689, chance = 13750}, -- bread
			{id = 2177, chance = 9700}, -- life crystal
			{id = 2600, chance = 1010}, -- inkwell
			{id = 2682, chance = 22380}, -- melon
			{id = 1949, chance = 30500}, -- scroll
			{id = 2148, count = 7, chance = 37060}, -- gold
		}
	},
	{ name = 'Elf Scout', level = 38, file = 'Cirith/Elves/elf scout.xml', look = {type = 64}, classId = 21, killAmount = 500, charmPoints = 15,
		description = {'Elf Zwiadowca', 'Elf Scout'}, experience = 355, health = 500, healthMax = 500,
		boss = 'Wrath of Nature', chance = 80,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -1 },
		},
		loot = {
			{id = 15738, chance = 10}, -- long bow
			{id = 2456, chance = 3970}, -- bow
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 3100}, -- lampart set
			{id = 14015, chance = 2810}, -- short bow
			{id = 5921, chance = 1090}, -- heaven blossom
			{id = 2642, chance = 990}, -- sandals
			{id = 2031, chance = 1310}, -- water skin
			{id = 7438, chance = 90}, -- elvish bow
			{id = 11694, chance = 900}, -- fine fabric
			{id = 2143, chance = 990}, -- white pearl
			{id = 14000, chance = 750}, -- light armor
			{id = 10551, chance = 5140}, -- elvish talisman
			{id = 13756, chance = 9980}, -- elven scouting glass
			{id = 2681, chance = 17850}, -- grapes
			{id = 10551, chance = 2090}, -- elvish talisman
			{id = 2544, count = 3, chance = 7050}, -- arrow
			{id = 2545, count = 22, chance = 15250}, -- poison arrow
			{id = 2544, count = 20, chance = 30650}, -- arrow
			{id = 2148, count = 6, chance = 74950}, -- gold
		}
	},
	{ name = 'Elf', level = 30, file = 'Cirith/Elves/elf.xml', look = {type = 62}, classId = 21, killAmount = 500, charmPoints = 15,
		description = {'Elf', 'Elf'}, experience = 160, health = 300, healthMax = 300,
		boss = 'Wrath of Nature', chance = 50,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -1 },
		},
		loot = {
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 2100}, -- lampart set
			{id = 14015, chance = 700}, -- short bow
			{id = 5921, chance = 930}, -- heaven blossom
			{id = 11694, chance = 490}, -- fine fabric
			{id = 2642, chance = 530}, -- sandals
			{id = 15793, chance = 9530}, -- sword
			{id = 14000, chance = 300}, -- light armor
			{id = 7438, chance = 15}, -- elvish bow
			{id = 2143, chance = 1000}, -- white pearl
			{id = 10551, chance = 2090}, -- elvish talisman
			{id = 2544, count = 3, chance = 7050}, -- arrow
			{id = 8839, count = 2, chance = 20290}, -- plum
			{id = 2148, count = 5, chance = 44620}, -- gold
		}
	},

	-- Geo Elementals
	{ name = 'Acid Blob', level = 25, file = 'Cirith/Geo-Elementals/acid blob.xml', look = {type = 314}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Kwasowy Szlam', 'Acid Slime'}, experience = 130, health = 250, healthMax = 250,
		elements = {
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 9967, chance = 18140}, -- Glob of acid
		}
	},
	{ name = 'Aqua Blob', level = 25, file = 'Cirith/Geo-Elementals/aqua blob.xml', look = {type = 316}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Wodny Szlam', 'Aqua Slime'}, experience = 90, health = 150, healthMax = 150,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 65 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 90 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 13587, chance = 9650}, -- Essence of water
		}
	},
	{ name = 'Bog Raider', level = 38, file = 'Cirith/Geo-Elementals/bog raider.xml', look = {type = 299}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Bagienny Korsarz', 'Bog Raider'}, experience = 750, health = 1300, healthMax = 1300,
		boss = 'Shlamer', chance = 90,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 85 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 16167, chance = 100}, -- swamplair armor
			{id = 14173, chance = 180}, -- armor of leaf clan
			{id = 8473, chance = 350}, -- UHP
			{id = 13924, chance = 250}, -- light crossbow
			{id = 14453, chance = 1000}, -- rod
			{id = 13585, chance = 300}, -- a foul rod
			{id = 7591, chance = 960}, -- GHP
			{id = 14327, chance = 200}, -- plate legs
			{id = 8472, chance = 670}, -- GSP
			{id = 10583, chance = 9860}, -- boggy dreads
			{id = 2148, count = 14, chance = 91990}, -- gold
		}
	},
	{ name = 'Carniphila', level = 40, file = 'Cirith/Geo-Elementals/carniphila.xml', look = {type = 120}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Carniphila', 'Carniphila'}, experience = 725, health = 1265, healthMax = 1265,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 35 },
		},
		loot = {
			{id = 8856, chance = 50}, -- beetlebow
			{id = 16165, chance = 60}, -- swamplair boots
			{id = 13848, chance = 20}, -- leaf armor
			{id = 13940, chance = 20}, -- snake crossbow
			{id = 2802, count = 2, chance = 470}, -- sling herb
			{id = 7732, chance = 480}, -- seeds
			{id = 13585, chance = 250}, -- a foul rod
			{id = 16308, chance = 170}, -- enhanced leather boots
			{id = 2804, chance = 890}, -- shadow herb
			{id = 2686, chance = 910}, -- corncob
			{id = 11211, chance = 4160}, -- carniphila seeds
			{id = 13577, chance = 7160}, -- wild herbs
			{id = 2792, chance = 7960}, -- dark mushroom
			{id = 2148, count = 34, chance = 88090}, -- gold
		}
	},
	{ name = 'Defiler', level = 88, file = 'Cirith/Geo-Elementals/defiler.xml', look = {type = 238}, classId = 23, killAmount = 2500, charmPoints = 50,
		description = {'Defiler', 'Defiler'}, experience = 2400, health = 3650, healthMax = 3650,
		boss = 'Shlamer', chance = 120,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
			{ type = 'death', value = 40 },
			{ type = 'physical', value = 15 },
		},
		loot = {
			{id = 16169, chance = 10}, -- swamplair bow
			{id = 16167, chance = 40}, -- swamplair armor
			{id = 2158, chance = 90}, -- blue gem
			{id = 2631, chance = 280}, -- knight legs
			{id = 2155, chance = 90}, -- green gem
			{id = 2154, chance = 90}, -- yellow gem
			{id = 16166, chance = 300}, -- swamplair legs
			{id = 15756, chance = 300}, -- snake armor
			{id = 2156, chance = 80}, -- red gem
			{id = 16140, chance = 10}, -- dragon slayer
			{id = 2145, count = 2, chance = 2510}, -- small diamond
			{id = 6300, chance = 2870}, -- death ring
			{id = 2147, count = 2, chance = 3640}, -- small ruby
			{id = 2151, chance = 4860}, -- talon
			{id = 2149, count = 3, chance = 5480}, -- small emerald
			{id = 7590, count = 2, chance = 760}, -- GMP
			{id = 9968, chance = 12080}, -- glob of tar
			{id = 9967, chance = 12600}, -- glob of acid slime
			{id = 6500, count = 1, chance = 1920}, -- demonic essence
			{id = 5944, chance = 1230}, -- soul orb
			{id = 2152, chance = 66110}, -- silver coin
			{id = 2148, count = 55, chance = 100000}, -- gold
		}
	},
	{ name = 'Earth Destroyer', level = 100, file = 'Cirith/Geo-Elementals/earth destroyer.xml', look = {type = 285}, classId = 23, killAmount = 2500, charmPoints = 50,
		description = {'Niszczyciel Ziemi', 'Earth Destroyer'}, experience = 5800, health = 8800, healthMax = 8800,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 85 },
		},
		loot = {
			{id = 16197, chance = 110}, -- druid cape
			{id = 8857, chance = 310}, -- silkweaver bow
			{id = 16167, chance = 20}, -- swamplair armor
			{id = 15757, chance = 240}, -- snake legs
			{id = 15756, chance = 120}, -- snake armor
			{id = 2536, chance = 260}, -- medusa shield
			{id = 16138, chance = 160}, -- golden armor
			{id = 13796, chance = 80}, -- silver shield
			{id = 2197, chance = 900, subType = 10}, -- ssa
			{id = 7887, chance = 900}, -- terra amulet
			{id = 15760, chance = 120}, -- snake bow
			{id = 16168, chance = 10}, -- swamplair hood
			{id = 2155, chance = 10}, -- green gem
			{id = 2145, count = 6, chance = 1300}, -- small diamond
			{id = 9970, count = 5, chance = 2300}, -- small topaz
			{id = 7591, chance = 300}, -- ghp
			{id = 2200, chance = 4300}, -- prot amulet
			{id = 2168, chance = 5300}, -- life ring
			{id = 2170, chance = 7300, subType = 200}, -- silver amulet
			{id = 7850, count = 12, chance = 8300}, -- earth arrow
			{id = 1294, count = 11, chance = 9000}, -- small stones
			{id = 7588, chance = 1100}, -- shp
			{id = 7589, chance = 1000}, -- SMP
			{id = 2152, chance = 33590}, -- silver
			{id = 2148, count = 19, chance = 98100}, -- gold
		}
	},
	{ name = 'Earth Elemental', level = 40, file = 'Cirith/Geo-Elementals/earth elemental.xml', look = {type = 301}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'�ywio�ak Ziemi', 'Earth Elemental'}, experience = 350, health = 650, healthMax = 650,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 90 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 35 },
		},
		loot = {
			{id = 16168, chance = 10}, -- swamplair helmet
			{id = 14175, chance = 9510}, -- coal
			{id = 13707, chance = 1200}, -- cuprite
			{id = 9970, chance = 530}, -- small topaz
			{id = 13878, chance = 1710}, -- small copper lump
			{id = 13876, chance = 2910}, -- small tin lump
			{id = 13948, chance = 900}, -- hunting spear
			{id = 5880, chance = 4850}, -- iron ore
			{id = 11693, chance = 250}, -- mithril ore
			{id = 2157, chance = 350}, -- gold nugget
			{id = 7589, chance = 1100}, -- smp
			{id = 1294, count = 10, chance = 10010}, -- small stones
			{id = 7850, count = 30, chance = 20160}, -- earth arrow
			{id = 11216, chance = 20230}, -- lump of earth
			{id = 2148, count = 13, chance = 92930}, -- gold
		}
	},
	{ name = 'Ent', level = 72, file = 'Cirith/Geo-Elementals/ent.xml', look = {type = 653}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Ent', 'Ent'}, experience = 2650, health = 5200, healthMax = 5200,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 5 },
			{ type = 'toxic', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 14190, chance = 10}, -- teddy doll
			{id = 16265, chance = 10}, -- backpack
			{id = 13797, chance = 50}, -- haunted shield
			{id = 2168, chance = 160}, -- life ring
			{id = 14455, chance = 500}, -- the creeper of darkness
			{id = 13004, chance = 20}, -- leaf shield
			{id = 8902, chance = 420}, -- sb of mind control
			{id = 11402, chance = 70}, -- massive wooden bow
			{id = 2155, chance = 50}, -- green gem
			{id = 5901, chance = 16530}, -- wood
			{id = 2149, chance = 620}, -- small emerald
			{id = 2213, chance = 670}, -- dwarven ring
			{id = 10219, chance = 760, subType = 5}, -- sacred tree amulet
			{id = 15755, chance = 150}, -- snake helmet
			{id = 7591, chance = 1020}, -- strong health potion
			{id = 2790, chance = 1810}, -- orange mushroom
			{id = 2791, count = 3, chance = 9010}, -- wood mushroom
			{id = 2787, count = 2, chance = 5010}, -- white mushroom
			{id = 7588, chance = 510}, -- health potion
			{id = 2788, chance = 7640}, -- red mushroom
			{id = 11216, chance = 20980}, -- lump of earth
			{id = 1294, count = 7, chance = 22180}, -- small stone
			{id = 2259, chance = 29310}, -- trash
			{id = 2148, count = 20, chance = 34190}, -- coin
		}
	},
	{ name = 'Forest Spirit', level = 40, file = 'Cirith/Geo-Elementals/forest spirit.xml', look = {type = 241}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Le�na Zjawa', 'Forest Spirit'}, experience = 270, health = 480, healthMax = 480,
		elements = {
			{ type = 'physical', value = 65 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13849, chance = 10}, -- leaf helmet
			{id = 15755, chance = 90}, -- snake helmet
			{id = 11694, chance = 1180}, -- fine fabric
			{id = 2149, count = 2, chance = 500}, -- small emerald
			{id = 14450, chance = 10}, -- staff of powerful energy
			{id = 2168, chance = 4920}, -- life ring
			{id = 11693, chance = 300}, -- mithril ore
			{id = 13882, chance = 24960}, -- herb
			{id = 7850, count = 15, chance = 50580}, -- earth arrow
			{id = 2148, count = 9, chance = 87820}, -- gold
		}
	},
	{ name = 'Golem', level = 48, file = 'Cirith/Geo-Elementals/golem.xml', look = {type = 333}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Golem', 'Golem'}, experience = 180, health = 330, healthMax = 330,
		elements = {
			{ type = 'physical', value = 60 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2158, chance = 10}, -- blue gem
			{id = 15717, chance = 80}, -- glorious axe
			{id = 5892, chance = 550}, -- steel
			{id = 13707, chance = 1000}, -- cuprite
			{id = 13985, chance = 10}, -- armor of loyalists
			{id = 11221, chance = 780}, -- shiny stone
			{id = 13878, chance = 900}, -- small copper lump
			{id = 13734, chance = 2980}, -- battle stone
			{id = 5880, chance = 5270}, -- iron ore
			{id = 13876, chance = 3150}, -- small tin lump
			{id = 11216, chance = 11780}, -- lump of earth
			{id = 1294, count = 9, chance = 25670}, -- small stone
			{id = 2148, count = 10, chance = 87820}, -- gold
		}
	},
	{ name = 'Jagged Earth Elemental', level = 60, file = 'Cirith/Geo-Elementals/jagged earth elemental.xml', look = {type = 285}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Szczerbaty �ywio�ak Ziemi', 'Jagged Earth Elemental'}, experience = 1300, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 45 },
			{ type = 'energy', value = 85 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 3982, chance = 10}, -- green robe
			{id = 1998, chance = 20}, -- green backpack
			{id = 13940, chance = 50}, -- snake crossbow
			{id = 16168, chance = 50}, -- swamplair helmet
			{id = 7435, chance = 30}, -- impaler
			{id = 15756, chance = 110}, -- snake armor
			{id = 10219, chance = 500, subType = 5}, -- sacred tree amulet
			{id = 8298, chance = 2400}, -- natural soil
			{id = 2168, chance = 1220}, -- life ring
			{id = 2149, count = 2, chance = 1450}, -- small emerald
			{id = 5880, chance = 1940}, -- iron ore
			{id = 7732, chance = 2420}, -- seeds
			{id = 11216, chance = 5710}, -- lump of earth
			{id = 7850, count = 3, chance = 7220}, -- earth arrow
			{id = 10556, chance = 11000}, -- poisonous slime
			{id = 2152, chance = 14680}, -- silver
			{id = 1294, count = 5, chance = 19350}, -- small stone
			{id = 2245, chance = 22370}, -- twigs
			{id = 2148, count = 13, chance = 99980}, -- gold
		}
	},
	{ name = 'Leaf Golem', level = 50, file = 'Cirith/Geo-Elementals/leaf golem.xml', look = {type = 609}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Le�ny Golem', 'Leaf Golem'}, experience = 750, health = 1400, healthMax = 1400,
		elements = {
			{ type = 'drown', value = 95 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 14175, chance = 5650}, -- coal
			{id = {16290, 16293}, chance = 80}, -- Hood/boots of the dawn
			{id = 7435, chance = 220}, -- impaler
			{id = 14453, chance = 180}, -- swamplair rod
			{id = 2791, chance = 900}, -- wood mushroom
			{id = 2145, count = 2, chance = 1990}, -- small diamond
			{id = 11216, chance = 65200}, -- lump of earth
			{id = 2148, count = {11, 16}, chance = 87820}, -- gold
		}
	},
	{ name = 'Massive Earth Elemental', level = 75, file = 'Cirith/Geo-Elementals/massive earth elemental.xml', look = {type = 285}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Wi�kszy �ywio�ak Ziemi', 'Massive Earth Elemental'}, experience = 750, health = 1330, healthMax = 1330,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 45 },
			{ type = 'energy', value = 90 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 85 },
		},
		loot = {
			{id = 16166, chance = 80}, -- swamplair legs
			{id = 16168, chance = 10}, -- swamplair helmet
			{id = 14352, chance = 60}, -- iron blade
			{id = 14175, chance = 7450}, -- coal
			{id = 13980, chance = 470, subType = 200}, -- terra amulet
			{id = 13877, chance = 1710}, -- copper lump
			{id = 13875, chance = 2910}, -- tin lump
			{id = 2197, chance = 950, subType = 5}, -- ssa
			{id = 15754, chance = 1580, subType = 1000}, -- brass amulet
			{id = 2791, chance = 2700}, -- wood mushroom
			{id = 2145, count = 2, chance = 4730}, -- small diamond
			{id = 9970, count = 2, chance = 5090}, -- small topaz
			{id = 1294, count = 10, chance = 25190}, --  Small Stones
			{id = 11216, chance = 40020}, -- lump of earth
			{id = 2148, count = 13, chance = 93860}, --  gold
		}
	},
	{ name = 'Mercury Blob', level = 25, file = 'Cirith/Geo-Elementals/mercury blob.xml', look = {type = 316}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Rt�ciowy Szlam', 'Mercury Slime'}, experience = 150, health = 280, healthMax = 280,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = 65 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 9966, chance = 18310}, -- Glob of Mercury
		}
	},
	{ name = 'Muddy Earth Elemental', level = 60, file = 'Cirith/Geo-Elementals/muddy earth elemental.xml', look = {type = 301}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Zm�cony �ywio�ak Ziemi', 'Muddy Earth Elemental'}, experience = 520, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 40 },
			{ type = 'energy', value = 85 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 1998, chance = 20}, -- green backpack
			{id = 7435, chance = 140}, -- impaler
			{id = 8298, chance = 1500}, -- natural soil
			{id = 2149, count = 2, chance = 1450}, -- small emerald
			{id = 11216, chance = 5710}, -- lump of earth
			{id = 10219, chance = 250, subType = 5}, -- sacred tree amulet
			{id = 7850, count = 3, chance = 7220}, -- earth arrow
			{id = 10556, chance = 7600}, -- poisonous slime
			{id = 1294, count = 5, chance = 19350}, -- small stone
			{id = 2245, chance = 22370}, -- twigs
			{id = 2152, chance = 14680}, -- silver
			{id = 2148, count = 10, chance = 99980}, -- gold
		}
	},
	{ name = 'Nepethes', level = 100, file = 'Cirith/Geo-Elementals/nepethes.xml', look = {type = 0, auxType = 3885, typeEx = 4149}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Nepethes', 'Nepethes'}, experience = 2900, health = 4150, healthMax = 4150,
		boss = 'Giant Nepethes', chance = 100,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13849, chance = 10}, -- leaf helmet
			{id = 13848, chance = 80}, -- leaf armor
			{id = 13847, chance = 40}, -- leaf legs
			{id = 13800, chance = 4200}, -- nepethes seeds
			{id = 7732, chance = 1050}, -- seeds
			{id = 2155, chance = 190}, -- green gem
			{id = 5803, chance = 50}, -- arbalest
			{id = 14453, chance = 3990}, -- poison rod
			{id = 11225, chance = 5200}, -- nettle blossom
			{id = 7590, chance = 1670}, -- GMP
			{id = 2800, chance = 7100}, -- star herb
			{id = 7850, count = 21, chance = 8990}, -- earth arrow
			{id = 2149, count = 5, chance = 17290}, -- small emerald
			{id = 2803, chance = 18190}, -- powder herb
			{id = 2804, chance = 25800}, -- shadow herb
			{id = 18050, chance = 200}, -- green dye
			{id = 2799, chance = 29880}, -- stone herb
			{id = 2802, count = 2, chance = 87500}, -- sling herb
		}
	},
	{ name = 'Plague Bearer', level = 80, file = 'Cirith/Geo-Elementals/plague bearer.xml', look = {type = 301}, classId = 23, killAmount = 2500, charmPoints = 50,
		description = {'Nosiciel Zarazy', 'Plague Bearer'}, experience = 1800, health = 2400, healthMax = 2400,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 35 },
		},
		loot = {
			{id = 13940, chance = 100}, -- snake crossbow
			{id = 15761, chance = 160}, -- snake blade
			{id = 1998, chance = 150}, -- green backpack
			{id = 8900, chance = 530}, -- sb of enlightenment
			{id = 15758, chance = 370}, -- snake boots
			{id = 14010, chance = 590}, -- knight armor
			{id = 15755, chance = 320}, -- snake helmet
			{id = 14362, chance = 1500}, -- steel armor
			{id = 2149, count = 5, chance = 7430}, -- small emerald
			{id = 7850, count = 9, chance = 8530}, -- earth arrow
			{id = 7589, chance = 890}, -- smp
			{id = 1294, count = 12, chance = 11420}, -- small stones
			{id = 2148, count = 8, chance = 92930}, -- gold
		}
	},
	{ name = 'Slime', level = 1, file = 'Cirith/Geo-Elementals/slime.xml', look = {type = 19}, classId = 23, killAmount = 500, charmPoints = 15,
		description = {'Szlam', 'Slime'}, experience = 100, health = 150, healthMax = 150,
		boss = 'Shlamer', chance = 60,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
	},
	{ name = 'Tar Blob', level = 25, file = 'Cirith/Geo-Elementals/tar blob.xml', look = {type = 315}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'Smolny Szlam', 'Tar Slime'}, experience = 170, health = 320, healthMax = 320,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 9968, chance = 18260}, -- Glob of Tar
		}
	},
	{ name = 'Treebeard', level = 120, file = 'Cirith/Geo-Elementals/treebeard.xml', look = {type = 480}, classId = 23, killAmount = 2500, charmPoints = 50,
		description = {'Drzewiec', 'Treebeard'}, experience = 16200, health = 15200, healthMax = 15200,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 60 },
			{ type = 'earth', value = 90 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 10 },
			{ type = 'wind', value = -10 },
		},
		loot = {
			{id = 16171, chance = 110}, -- ruby legs
			{id = 14356, chance = 20}, -- bronze boots
			{id = 11387, chance = 210}, -- luck amulet
			{id = 2123, chance = 50}, -- rots
			{id = 16208, chance = 120}, -- meteorite helmet
			{id = 13848, chance = 170}, -- leaf armor
			{id = 13991, chance = 120}, -- mythril armor
			{id = 13940, chance = 130}, -- crossbow of wrath nature
			{id = 14457, chance = 300}, -- spruce staff of destruction
			{id = 16167, chance = 310}, -- swamplair armor
			{id = 15734, chance = 420}, -- vile axe
			{id = 13849, chance = 220}, -- leaf helmet
			{id = 17726, chance = 20}, -- plague spear
			{id = 2168, chance = 960}, -- life ring
			{id = 12953, chance = 380}, -- marksman boots
			{id = 12975, chance = 990}, -- Earthy lunar crystal, puncture
			{id = 15756, chance = 790}, -- snake armor
			{id = 13999, chance = 210}, -- noble armor
			{id = 16148, chance = 820}, -- a dirty eye of despair
			{id = 16431, chance = 50}, -- black forest bow
			{id = 10219, chance = 960, subType = 10}, -- sacred tree amulet
			{id = 2791, count = 3, chance = 3010}, -- wood mushroom
			{id = 15758, chance = 2370}, -- snake boots
			{id = 13990, chance = 530}, -- emerald armor
			{id = 12964, chance = 4000}, -- clear lunar crystal
			{id = 15730, chance = 450}, -- war axe
			{id = 2155, chance = 10}, -- green gem
			{id = 2150, count = 4, chance = 6590}, -- small amethyst
			{id = 2144, count = 3, chance = 6780}, -- black pearl
			{id = 15755, chance = 910}, -- snake helmet
			{id = 2214, chance = 9000}, -- roh
			{id = 2149, count = 6, chance = 9220}, -- small emerald
			{id = 8472, count = 2, chance = 1900}, -- UMP
			{id = 8473, count = 2, chance = 1000}, -- UHP
			{id = 2170, chance = 15290, subType = 200}, -- silver amulet
			{id = 11216, chance = 20980}, -- lump of earth
			{id = 1294, count = 31, chance = 22180}, -- small stone
			{id = 2152, count = 2, chance = 34190}, -- silver coin
			{id = 2148, count = 80, chance = 34190}, -- coin
		}
	},
	{ name = 'Young Nepethes', level = 75, file = 'Cirith/Geo-Elementals/young nepethes.xml', look = {type = 0, auxType = 3884, typeEx = 4148}, classId = 23, killAmount = 1000, charmPoints = 25,
		description = {'M�odszy Nepethes', 'Young Nepethes'}, experience = 470, health = 850, healthMax = 850,
		boss = 'Giant Nepethes', chance = 50,
		elements = {
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13849, chance = 10}, -- leaf helmet
			{id = 13847, chance = 10}, -- leaf legs
			{id = 13800, chance = 920}, -- nepethes seeds
			{id = 7732, chance = 10}, -- seeds
			{id = 2155, chance = 10}, -- green gem
			{id = 14453, chance = 390}, -- poison rod
			{id = 11225, chance = 520}, -- nettle blossom
			{id = 7590, chance = 660}, -- GMP
			{id = 7850, count = 21, chance = 2990}, -- earth arrow
			{id = 2149, count = 5, chance = 4290}, -- small emerald
			{id = 2800, chance = 7100}, -- star herb
			{id = 2803, chance = 18190}, -- powder herb
			{id = 2804, chance = 25800}, -- shadow herb
			{id = 2799, chance = 29880}, -- stone herb
			{id = 18050, chance = 80}, -- green dye
			{id = 2802, count = 2, chance = 87500}, -- sling herb
		}
	},
	
	-- Giants
	{ name = 'Behemoth', level = 100, file = 'Cirith/Giants/behemoth.xml', look = {type = 55}, classId = 20, killAmount = 2500, charmPoints = 50,
		description = {'Behemot', 'Behemoth'}, experience = 2950, health = 4000, healthMax = 4000,
		boss = 'Emerald Behemoth', chance = 130,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 30 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 30 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 11694, chance = 1500}, -- fine fabric
			{id = 13164, chance = 190}, -- royal steel blade
			{id = 13953, chance = 10}, -- the fallen horse
			{id = 15732, chance = 620}, -- knight axe
			{id = 15729, chance = 80}, -- double axe
			{id = 2023, chance = 100}, -- amphora
			{id = 7396, chance = 130}, -- behemoth trophy
			{id = 16266, chance = 100}, -- behemoth backpack
			{id = 16356, chance = 50}, -- container
			{id = 13978, chance = 330}, -- Bloody crusher
			{id = 14320, chance = 20}, -- plate boots
			{id = 5930, chance = 440}, -- behemoth claw
			{id = 16905, chance = 800}, -- rope quest
			{id = 2553, chance = 560}, -- pick
			{id = 2231, chance = 750}, -- big bone
			{id = 2174, chance = 870}, -- strange symbol
			{id = 16292, chance = 520}, -- legs of the dawn
			{id = 11435, count = 4, chance = 900}, -- crystalline arrow
			{id = 5887, chance = 1110}, -- piece of royal steel
			{id = 5893, chance = 1090}, -- behemoth fang
			{id = 15754, chance = 2550, subType = 100}, -- brass amulet
			{id = 14348, chance = 1140}, -- iron armor
			{id = 14001, chance = 400}, -- dark armor
			{id = 7591, chance = 570}, -- GHP
			{id = 15723, chance = 1960}, -- two handed sword
			{id = 2150, count = 5, chance = 7430}, -- small amethyst
			{id = 15719, chance = 3350}, -- battle hammer
			{id = 13734, chance = 14580}, -- battle stone
			{id = 2666, count = 6, chance = 29820}, -- meat
			{id = 2152, chance = 56570}, -- silver
			{id = 2148, count = {35, 74}, chance = 99570}, -- gold
		}
	},
	{ name = 'Cliff Strider', level = 120, file = 'Cirith/Giants/cliff strider.xml', look = {type = 495}, classId = 20, killAmount = 2500, charmPoints = 50,
		description = {'Skalniak', 'Cliff Strider'}, experience = 7000, health = 9400, healthMax = 9400,
		elements = {
			{ type = 'physical', value = 55 },
			{ type = 'death', value = 40 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = {16176, 16175}, chance = 200}, -- ruby weapons
			{id = 14582, chance = 230}, -- runed sword
			{id = 14320, chance = 490}, -- steel boots
			{id = 16356, chance = 100}, -- container
			{id = 13991, chance = 650}, -- mythril plate armor
			{id = 14322, chance = 240}, -- royal steel helmet
			{id = 13978, chance = 410}, -- Bloody crusher
			{id = 8850, chance = 250}, -- chain bolter
			{id = 2158, chance = 470}, -- blue gem
			{id = 17940, chance = 1130}, -- pure mithril
			{id = 11333, chance = 500}, --clay lump
			{id = 14321, chance = 570}, -- royal steel legs
			{id = 15730, chance = 490}, -- war axe
			{id = 16910, chance = 500}, -- a crystal rod of frost
			{id = 14325, chance = 1110}, -- steel blade
			{id = 11693, chance = 5310}, -- mithril ore
			{id = 5904, chance = 1310}, -- magic sulphur
			{id = 13888, chance = 1110}, -- lightning axe
			{id = 9942, chance = 2980}, -- crystal balance
			{id = 11436, count = 7, chance = 3390}, -- crystalline bolt
			{id = 2156, chance = 480}, -- red gem
			{id = 2143, count = 3, chance = 9200}, -- white pearl
			{id = 2144, chance = 9730}, -- black pearl
			{id = 11221, chance = 11740}, -- shiny stone
			{id = 5880, chance = 14200}, -- iron ore
			{id = 13707, chance = 15340}, -- Pulverized Ore
			{id = 13706, chance = 1510}, -- Cliff Strider Claw
			{id = 5944, chance = 1210}, -- soul orb
			{id = 13705, count = 2, chance = 7970}, -- vein of ore
			{id = 8473, count = 2, chance = 240}, -- UHP
			{id = 8472, count = 2, chance = 250}, -- UMP
			{id = 2152, count = {2, 4}, chance = 75240}, -- silver
			{id = 2148, count = {59, 81}, chance = 100000}, -- gold
		}
	},
	{ name = 'Cyclops Drone', level = 38, file = 'Cirith/Giants/cyclops drone.xml', look = {type = 644}, classId = 20, killAmount = 1000, charmPoints = 25,
		description = {'Cyklop Robotnik', 'Cyclops Drone'}, experience = 240, health = 325, healthMax = 325,
		boss = 'Cyclopus', chance = 120,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 2213, chance = 430}, -- ring of endurance
			{id = 14175, count = 2, chance = 10000}, -- coal
			{id = 7398, chance = 130}, -- cyclops trophy
			{id = 7588, chance = 400}, -- SHP
			{id = 17656, chance = 500}, -- copper blade
			{id = 5880, chance = 4000}, -- iron ore
			{id = 18061, chance = 4210}, -- cyclops eye
			{id = 5892, chance = 1100}, -- steel
			{id = 14170, chance = 290}, -- leggionaire armor
			{id = 14478, chance = 900}, -- berynit helmet
			{id = 14351, chance = 250}, -- iron boots
			{id = 14349, chance = 345}, -- iron helmet
			{id = 10573, chance = 6920}, -- cyclops toe
			{id = 2666, chance = 50220}, -- meat
			{id = 2148, count = {12, 28}, chance = 81930}, -- gold
		}
	},
	{ name = 'Cyclops Elite', level = 50, file = 'Cirith/Giants/cyclops elite.xml', look = {type = 624}, classId = 20, killAmount = 1000, charmPoints = 25,
		description = {'Kr�lewski Cyklop', 'Royal Cyclops'}, experience = 580, health = 800, healthMax = 800,
		boss = 'Cyclopus', chance = 190,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 11693, chance = 720}, -- mithril ore
			{id = 2214, chance = 260}, -- ring of condition
			{id = 15754, chance = 350, subType = 220}, -- brass necklace
			{id = 7398, chance = 120}, -- cyclops trophy
			{id = 13952, chance = 290}, -- crystal spear
			{id = 5892, chance = 1200}, -- steel
			{id = 14334, chance = 400}, -- heavy axe
			{id = 18061, chance = 3710}, -- cyclops eye
			{id = 5892, chance = 120}, -- huge chunk of cude iron
			{id = 6534, chance = 120}, -- flying trident
			{id = 8849, chance = 750}, -- modified crossbow
			{id = 18338, chance = 10}, -- red gem
			{id = 15716, chance = 720}, -- barbarian axe
			{id = 14317, chance = 1500}, -- enchanced oak shield
			{id = 7588, count = 2, chance = 400}, -- SHP
			{id = {14353, 14355, 14357}, chance = 950}, -- bronze set
			{id = 14354, chance = 900}, -- bronze helmet
			{id = 14175, count = 2, chance = 4800}, -- coal
			{id = 14358, chance = 570}, -- bronze axe
			{id = 2666, chance = 50110}, -- meat
			{id = 10573, chance = 9980}, -- cyclops toe
			{id = 2148, count = {24, 41}, chance = 82840}, -- gold
		}
	},
	{ name = 'Cyclops Smith', level = 46, file = 'Cirith/Giants/cyclops smith.xml', look = {type = 277}, classId = 20, killAmount = 1000, charmPoints = 25,
		description = {'Cyklop Topornik', 'Cyclops Axeman'}, experience = 325, health = 435, healthMax = 435,
		boss = 'Cyclopus', chance = 120,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 2213, chance = 610}, -- ring of endurance
			{id = {14353, 14354, 14355, 14357}, chance = 300}, -- bronze set
			{id = 11404, chance = 110}, -- heavy crossbow
			{id = 18061, chance = 4570}, -- cyclops eye
			{id = 15714, chance = 4980}, -- club
			{id = 17223, chance = 720}, -- claw
			{id = {13055, 13070, 13058, 13061}, chance = 220}, -- bronze crafting additives
			{id = 14317, chance = 140}, -- enhanced oak shield
			{id = 15754, chance = 750, subType = 100}, -- brass necklace
			{id = 7398, chance = 120}, -- cyclops trophy
			{id = 5892, chance = 1200}, -- steel
			{id = 14175, count = 2, chance = 4600}, -- coal
			{id = 7588, chance = 400}, -- SHP
			{id = 2376, chance = 400}, -- runed blade
			{id = 14358, chance = 600}, -- bronze axe
			{id = 10573, chance = 9980}, -- cyclops toe
			{id = 2666, chance = 50110}, -- meat
			{id = 2148, count = {18, 36}, chance = 82840}, -- gold
		}
	},
	{ name = 'Cyclops', level = 36, file = 'Cirith/Giants/cyclops.xml', look = {type = 22}, classId = 20, killAmount = 1000, charmPoints = 25,
		description = {'Cyklop', 'Cyclops'}, experience = 190, health = 260, healthMax = 260,
		boss = 'Cyclopus', chance = 90,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 15754, chance = 450, subType = 100}, -- brass necklace
			{id = 2213, chance = 390}, -- ring of endurance
			{id = 5880, chance = 3270}, -- iron ore
			{id = 14358, chance = 290}, -- bronze axe
			{id = 17656, chance = 310}, -- copper blade
			{id = 18061, chance = 3350}, -- cyclops eye
			{id = 7398, chance = 90}, -- cyclops trophy
			{id = 14175, chance = 9600}, -- coal
			{id = 14317, chance = 600}, -- enchanced oak shield
			{id = 14170, chance = 90}, -- leggionaire armor
			{id = 7618, chance = 20}, -- health potion
			{id = 10573, chance = 5050}, -- cyclops toe
			{id = 2666, chance = 30050}, -- meat
			{id = 2148, count = 20, chance = 82130}, -- gold
		}
	},
	{ name = 'Frost Giant', level = 45, file = 'Cirith/Giants/frost giant.xml', look = {type = 257}, classId = 20, killAmount = 1000, charmPoints = 25,
		description = {'Lodowy Gigant', 'Frost Giant'}, experience = 150, health = 270, healthMax = 270,
		elements = {
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 2656, chance = 210}, -- blue robe
			{id = 7290, chance = 60}, -- shard
			{id = 2209, chance = 120}, -- club ring
			{id = 14488, chance = 180}, -- visor
			{id = 14324, chance = 180}, -- plate shield
			{id = 15723, chance = 160}, -- Two handed sword
			{id = 7618, chance = 890}, -- health potion
			{id = 14451, chance = 600}, -- ice cross
			{id = 15784, chance = 1330}, -- copper shield
			{id = 7441, chance = 2230}, -- ice cube
			{id = 10574, chance = 4850}, -- frost giant pelt
			{id = 16910, chance = 150}, -- a crystal rod of frost
			{id = 2666, count = 2, chance = 4910}, -- meat
			{id = 15776, chance = 8150}, -- short sword
			{id = 2148, count = 13, chance = 82390}, -- gold
		}
	},
	{ name = 'Frost Giantess', level = 45, file = 'Cirith/Giants/frost giantess.xml', look = {type = 265}, classId = 20, killAmount = 1000, charmPoints = 25,
		description = {'Lodowa Gigantka', 'Frost Giantess'}, experience = 150, health = 275, healthMax = 275,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -10 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 2656, chance = 50}, -- blue robe
			{id = 2209, chance = 70}, -- club ring
			{id = 7290, chance = 10}, -- shard
			{id = 14363, chance = 70}, -- plate helmet
			{id = 14324, chance = 110}, -- plate shield
			{id = 7620, chance = 940}, -- mana potion
			{id = 15784, chance = 1480}, -- copper shield
			{id = 7441, chance = 2080}, -- ice cube
			{id = 10574, chance = 4850}, -- frost giant pelt
			{id = 15776, chance = 8020}, -- short sword
			{id = 2194, count = 3, chance = 10390}, -- small stone
			{id = 2671, count = 2, chance = 21030}, -- ham
			{id = 2148, count = 17, chance = 81900}, -- gold
		}
	},
	{ name = 'Gargoyle', level = 38, file = 'Cirith/Giants/gargoyle.xml', look = {type = 95}, classId = 23, killAmount = 500, charmPoints = 15,
		description = {'Gargulec', 'Gargoyle'}, experience = 150, health = 250, healthMax = 250,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'death', value = 1 },
			{ type = 'earth', value = 60 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 11221, chance = 160}, -- shiny stone
			{id = 2209, chance = 230}, -- club ring
			{id = 14001, chance = 50}, -- dark armor
			{id = 14478, chance = 230}, -- steel helmet
			{id = 15784, chance = 990}, -- copper shield
			{id = {16305, 16306, 16307, 16308}, chance = 820}, -- enhanced leather set
			{id = 5887, chance = 100}, -- royal steel
			{id = 5880, chance = 14190}, -- iron ore
			{id = 2129, chance = 1190}, -- wolf tooth chain
			{id = 2680, count = 5, chance = 1630}, -- strawberries
			{id = 15713, chance = 1810}, -- longsword
			{id = 8838, count = 2, chance = 9410}, -- potatoes
			{id = 11189, chance = 12070}, -- stone wing
			{id = 1294, count = 10, chance = 15460}, -- small stone
			{id = 2148, count = 39, chance = 87980}, -- gold coin
		}
	},
	{ name = 'Ogre', level = 70, file = 'Cirith/Giants/ogre.xml', look = {type = 477}, classId = 20, killAmount = 2500, charmPoints = 50,
		description = {'Ogr', 'Ogre'}, experience = 1950, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'physical', value = 45 },
			{ type = 'holy', value = 80 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 13016, chance = 10}, -- forgotten orchid
			{id = 13953, chance = 100}, -- bloody axe
			{id = 13794, chance = 250}, -- titan helmet
			{id = 14450, chance = 210}, -- staff of powerful energy
			{id = 2391, chance = 110}, -- war hammer
			{id = 16211, chance = 90}, -- meteorite shield
			{id = 2571, chance = 80}, -- bowl
			{id = 16356, chance = 70}, -- container
			{id = {13055, 13070, 13058, 13061}, chance = 380}, -- bronze crafting additives
			{id = 16157, chance = 100}, -- black steel armor
			{id = 14320, chance = 250}, -- plate boots
			{id = 6300, chance = 350}, -- death ring
			{id = 2156, chance = 680}, -- red gem
			{id = 14010, chance = 480}, -- knight armor
			{id = 14442, chance = 150, subType = 100}, -- penetration ring
			{id = 14324, chance = 250}, -- steel shield
			{id = 14322, chance = 350}, -- royal steel helmet
			{id = 5887, chance = 810}, -- piece of royal steel
			{id = 2231, chance = 2750}, -- big bone
			{id = 2145, count = 4, chance = 3430}, -- small diamond
			{id = 14348, chance = 2140}, -- iron armor
			{id = 5889, chance = 4810}, -- piece of steel
			{id = 7591, chance = 570}, -- GHP
			{id = 6539, chance = 6150}, -- the ogre protector
			{id = 15715, chance = 7840}, -- cranial basher
			{id = 2666, count = 2, chance = 39820}, -- meat
			{id = 2152, chance = 11690}, -- silver
			{id = 2148, count = {28, 66}, chance = 79570}, -- gold
		}
	},
	{ name = 'Stone Golem', level = 40, file = 'Cirith/Giants/stone golem.xml', look = {type = 67}, classId = 20, killAmount = 500, charmPoints = 15,
		description = {'Kamienny Golem', 'Stone Golem'}, experience = 150, health = 270, healthMax = 270,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 70 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2156, chance = 10}, -- red gem
			{id = 11221, chance = 780}, -- shiny stone
			{id = 2157, chance = 120}, -- gold nugget
			{id = 10548, chance = 1020}, -- ancient stone
			{id = 5880, chance = 4160}, -- iron ore
			{id = {16305, 16306, 16307, 16308}, chance = 710}, -- enhanced leather set
			{id = 13707, count = 2, chance = 1400}, -- cuprite
			{id = 11226, chance = 10330}, -- sulphuorus stone
			{id = 1294, count = 4, chance = 12850}, -- small stone
			{id = 15715, chance = 550}, -- cranial basher
			{id = 11333, chance = 110}, --clay lump
			{id = 5892, chance = 1150}, -- steel
			{id = 14357, chance = 220}, -- bronze shield
		}
	},
	
	--  Goblins 
	{ name = 'Goblin Assassin', level = 26, file = 'Cirith/Goblins/goblin assasin.xml', look = {type = 296}, classId = 19, killAmount = 250, charmPoints = 5,
		description = {'Goblin Asasyn', 'Goblin Assassin'}, experience = 190, health = 330, healthMax = 330,
		boss = 'Demonplague Consumer', chance = 70,
		elements = {
			{ type = 'holy', value = 1 },
			{ type = 'death', value = -1 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 15714, chance = 4820}, -- club
			{id = 2235, chance = 6900}, -- cheese
			{id = 2559, chance = 9630}, -- small axe
			{id = 5880, chance = 2190}, -- iron ore
			{id = 5878, chance = 590}, -- hardener leather
			{id = 15778, chance = 4000}, -- wooden shield
			{id = 1294, count = 3, chance = 10010}, -- small stone
			{id = 2667, chance = 12500}, -- fish
			{id = 15772, chance = 5100}, -- leather helmet
			{id = 15773, chance = 5470}, -- leather armor
			{id = 14452, chance = 450}, -- staff of wind
			{id = 15775, chance = 4150}, -- leather boots
			{id = 2386, chance = 810}, -- goblin's chopper
			{id = 2230, chance = 12950}, -- bone
			{id = 5891, chance = 90}, -- enchanted chicken wing
			{id = 2379, chance = 17420}, -- blade
			{id = 15774, chance = 2900}, -- leather legs
			{id = 2148, count = {6, 14}, chance = 50530}, -- coin
		}
	},
	{ name = 'Goblin Scavenger', level = 26, file = 'Cirith/Goblins/goblin scavenger.xml', look = {type = 297}, classId = 19, killAmount = 250, charmPoints = 5,
		description = {'Goblin Padlino�erca', 'Goblin Scavenger'}, experience = 170, health = 290, healthMax = 290,
		boss = 'Demonplague Consumer', chance = 70,
		elements = {
			{ type = 'holy', value = 1 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 15714, chance = 2110}, -- club
			{id = 2235, chance = 6860}, -- cheese
			{id = 15773, chance = 5720}, -- leather armor
			{id = 15776, chance = 10}, -- killer's blade
			{id = 5878, chance = 560}, -- hardener leather
			{id = 2386, chance = 600}, -- goblin's chopper
			{id = 5880, chance = 1890}, -- iron ore
			{id = 2559, chance = 9810}, -- small axe
			{id = 15772, chance = 5180}, -- leather helmet
			{id = 2230, chance = 12500}, -- bone
			{id = 2667, chance = 13610}, -- fish
			{id = 5891, chance = 110}, -- enchanted chicken wing
			{id = 2379, chance = 18220}, -- blade
			{id = 1294, count = 2, chance = 25510}, -- small stone
			{id = 2148, count = {6, 16}, chance = 50750}, -- coin
		}
	},
	{ name = 'Goblin', level = 26, file = 'Cirith/Goblins/goblin.xml', look = {type = 61}, classId = 19, killAmount = 250, charmPoints = 5,
		description = {'Goblin', 'Goblin'}, experience = 130, health = 225, healthMax = 225,
		boss = 'Demonplague Consumer', chance = 70,
		elements = {
			{ type = 'holy', value = 1 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -12 },
		},
		loot = {
			{id = 2235, chance = 1000}, -- cheese
			{id = 2230, chance = 1110}, -- bone
			{id = 2379, chance = 1800}, -- blade
			{id = 5880, chance = 900}, -- iron ore
			{id = 15772, chance = 4500}, -- leather helmet
			{id = 15773, chance = 2760}, -- leather armor
			{id = 15774, chance = 4590}, -- leather legs
			{id = 15775, chance = 3200}, -- leather boots
			{id = 19219, chance = 980}, -- quiver
			{id = 5891, chance = 10}, -- enchanted chicken wing
			{id = 15778, chance = 4100}, -- wooden shield
			{id = 15714, chance = 6790}, -- club
			{id = 5878, chance = 410}, -- hardener leather
			{id = 14015, chance = 190}, -- short bow
			{id = 2559, chance = 9800}, -- small axe
			{id = 2386, chance = 730}, -- goblin's chopper
			{id = 2667, chance = 12790}, -- fish
			{id = 1294, chance = 15320}, -- small stone
			{id = 2148, count = {4, 12}, chance = 50280}, -- coin
		}
	},
	
	--  Humans 
	{ name = 'Acolyte of the Cult', level = 30, file = 'Cirith/Humans/acolyte of the cult.xml', look = {type = 194, head = 114, body = 121, legs = 121, feet = 57}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'Akolita Kultu', 'Acolyte of the Cult'}, experience = 200, health = 390, healthMax = 390,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 14453, chance = 210}, -- swamplair rod
			{id = 2149, chance = 430}, -- small emerald
			{id = 2168, chance = 490}, -- life ring
			{id = 11694, chance = 1500}, -- fine fabric
			{id = 5809, chance = 1000}, -- magical element
			{id = 13754, chance = 2100}, -- rope belt
			{id = 1976, chance = 880}, -- book
			{id = 5810, chance = 1000}, -- pirate voodoo doll
			{id = 2201, chance = 1040, subType = 100}, -- dragon necklace
			{id = 10555, chance = 7710}, -- cultish robe
			{id = 2148, count = 9, chance = 1390}, -- gold
		}
	},
	{ name = 'Adept of the Cult', level = 40, file = 'Cirith/Humans/adept of the cult.xml', look = {type = 194, head = 114, body = 94, legs = 94, feet = 57}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'Adept Kultu', 'Adept of the Cult'}, experience = 220, health = 430, healthMax = 430,
		elements = {
			{ type = 'holy', value = 30 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -15 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 16909, chance = 10}, -- sickle of the death
			{id = 16143, chance = 110}, -- golden dagger
			{id = 2147, chance = 140}, -- small ruby
			{id = 2655, chance = 190}, -- red robe
			{id = 11694, chance = 590}, -- fine fabric
			{id = 2190, chance = 190}, -- thunder rod
			{id = 2169, chance = 410}, -- time ring
			{id = 5809, chance = 3000}, -- magical element
			{id = 7889, chance = 570}, -- beta shanir
			{id = 13754, chance = 1990}, -- rope belt
			{id = 1976, chance = 930}, -- book
			{id = 2170, chance = 980}, -- silver amulet
			{id = 2379, chance = 1090}, -- dagger
			{id = 5810, chance = 1480}, -- pirate voodoo doll
			{id = 10555, chance = 9920}, -- cultish robe
			{id = 2148, count = 12, chance = 1300}, -- gold
		}
	},
	{ name = 'Enlightened of the Cult', level = 45, file = 'Cirith/Humans/enlightened of the cult.xml', look = {type = 193}, classId = 29, killAmount = 1000, charmPoints = 25,
		description = {'Enlightened of the Cult', 'Enlightened of the Cult'}, experience = 400, health = 700, healthMax = 700,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 17941, chance = 1220}, -- radiating powder
			{id = 11694, chance = 1390}, -- fine fabric
			{id = 11693, chance = 620}, -- mithril ore
			{id = 16909, chance = 10}, -- sickle of the death
			{id = 17329, chance = 30}, -- reef blade
			{id = 2656, chance = 90}, -- blue robe
			{id = 2114, chance = 90}, -- piggy bank
			{id = 2171, chance = 110}, -- platinum amulet
			{id = 5669, chance = 110}, -- voodoo skull
			{id = 11955, chance = 140}, -- jewelled backpack
			{id = 2187, chance = 260}, -- WoI
			{id = 13754, chance = 2170}, -- rope belt
			{id = 5810, chance = 310}, -- pirate voodoo doll
			{id = 16473, chance = 40}, -- spiky squelcher
			{id = 2146, chance = 570}, -- small sapphire
			{id = 2167, chance = 600}, -- energy ring
			{id = 7589, chance = 860}, -- SMP
			{id = 1976, chance = 930}, -- book
			{id = 2200, chance = 390}, -- protection amulet
			{id = 10554, chance = 10350}, -- cultish mask
			{id = 2148, count = {26, 33}, chance = 3670}, -- gold
		}
	},
	{ name = 'Amazon', level = 18, file = 'Cirith/Humans/amazon.xml', look = {type = 137, head = 113, body = 120, legs = 114, feet = 132}, classId = 6, killAmount = 500, charmPoints = 15,
		description = {'Amazonka', 'Amazon'}, experience = 100, health = 150, healthMax = 150,
		boss = 'Nemrod', chance = 50,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'death', value = -5 },
		},
		loot = {
			{id = 2147, chance = 150}, -- small ruby
			{id = 2050, chance = 1040}, -- torch
			{id = 15793, chance = 12890}, -- sword
			{id = 2143, chance = 1310}, -- white pearl
			{id = {16305, 16306, 16307, 16308}, chance = 320}, -- enhanced leather set
			{id = 2410, count = 2, chance = 8080}, --  throwing knife
			{id = 13731, chance = 5180}, -- protective charm
			{id = 13754, chance = 10900}, -- rope belt
			{id = 13755, chance = 2100}, -- safety pin
			{id = 13730, chance = 5910}, -- girlish hair decoration
			{id = 2691, chance = 29570}, -- brown bread
			{id = 2148, count = 20, chance = 40070}, -- gold coins
			{id = 2229, count = 2, chance = 80080}, --  skull
			{id = 2379, chance = 80250}, -- blade
		}
	},
	{ name = 'Assasin', level = 20, file = 'Cirith/Humans/assasin.xml', look = {type = 152, head = 95, body = 95, legs = 95, feet = 95, addons = 3}, classId = 6, killAmount = 500, charmPoints = 15,
		description = {'Asasyn', 'Assasin'}, experience = 135, health = 175, healthMax = 175,
		boss = 'Nemrod', chance = 70,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'death', value = -5 },
		},
		loot = {
			{id = 2145, chance = 230}, -- small diamond
			{id = 14488, chance = 80}, -- visor
			{id = 14170, chance = 170}, -- armor
			{id = 14339, chance = 120}, -- steel shield
			{id = 14357, chance = 1580}, -- bronze shield
			{id = 15784, chance = 1950}, -- copper shield
			{id = 14484, chance = 3200}, -- helmet
			{id = 2379, chance = 3990}, -- blade
			{id = 14779, chance = 9820}, -- knife
			{id = 2050, chance = 30060}, -- torch
			{id = 2050, chance = 30060}, -- torch
			{id = 2148, count = 22, chance = 83160}, -- gold
		}
	},
	{ name = 'Black Knight', level = 65, file = 'Cirith/Humans/black knight.xml', look = {type = 403, head = 97, body = 95, legs = 0, feet = 114}, classId = 6, killAmount = 2500, charmPoints = 50,
		description = {'Czarny Rycerz', 'Black Knight'}, experience = 1800, health = 2470, healthMax = 2470,
		boss = 'Huma Dragonbane', chance = 100,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 35 },
		},
		loot = {
			{id = 15745, chance = 150}, -- dragon legs
			{id = 13899, chance = 160}, -- bastard sword
			{id = 2195, chance = 210}, -- boh
			{id = 2133, chance = 50, subType = 80}, -- ruby necklace
			{id = 14323, chance = 320}, -- royal steel armor
			{id = 14321, chance = 330}, -- royal steel legs
			{id = 2114, chance = 140}, -- piggy bank
			{id = 13944, chance = 310}, -- ashbringer boots
			{id = 16290, chance = 350}, -- Hood of the dawn
			{id = 14001, chance = 520}, -- dark armor
			{id = 14484, chance = 260}, -- Szyszak
			{id = 5885, chance = 800}, -- warrior's sweat
			{id = 14352, chance = 340}, -- iron sword
			{id = 14322, chance = 330}, -- royal steel helmet
			{id = 15723, chance = 470}, -- two handed sword
			{id = 14363, chance = 480}, -- steel helmet
			{id = 5887, chance = 910}, -- piece of royal steel
			{id = 16431, chance = 10}, -- black forest bow
			{id = 15792, chance = 1120}, -- gwiazda tr??mienna
			{id = 14362, chance = 1890}, -- steel armor
			{id = 15713, chance = 12280}, -- longsword
			{id = 15752, chance = 12560}, -- brass legs
			{id = 2120, chance = 14340}, -- rope
			{id = 2691, count = 2, chance = 20360}, -- brown bread
			{id = 15782, chance = 200}, -- steel spear
			{id = 2148, count = {24, 71}, chance = 78860}, -- gold
		}
	},
	{ name = 'Commodore of the Kingdom', level = 110, file = 'Cirith/Humans/commodore of the kingdom.xml', look = {type = 381}, classId = 6, killAmount = 2500, charmPoints = 50,
		description = {'Komandor Kr�lestwa', 'Commodore of the Kingdom'}, experience = 5300, health = 7400, healthMax = 7400,
		boss = 'Goldmoon', chance = 100,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'holy', value = 15 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 35 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 30 },
			{ type = 'earth', value = -5 },
		},
		loot = {
			{id = 10140, chance = 50}, -- Order of Honour
			{id = 13998, chance = 150}, -- Master warrior's armor
			{id = 16291, chance = 210}, -- robe of the dawn
			{id = 2155, chance = 50}, -- green gem
			{id = 17728, chance = 480}, -- sanctified spear
			{id = 13928, chance = 50}, -- shield of invaders
			{id = 2195, chance = 210}, -- boh
			{id = 14480, chance = 320}, -- royal helmet
			{id = 12998, chance = 380}, -- rider of light
			{id = 14009, chance = 220}, -- lightning robe
			{id = {16161, 16159, 16162}, chance = 350}, -- black steel sword/axe/hammer
			{id = 14320, chance = 540}, -- steel boots
			{id = 14322, chance = 300}, -- royal steel helmet
			{id = 16157, chance = 390}, -- black steel armor
			{id = 2171, chance = 210}, -- platinum amulet
			{id = 15732, chance = 440}, -- knight axe
			{id = 16158, chance = 190}, -- black steel helmet
			{id = 14321, chance = 320}, -- royal plate legs
			{id = 14007, chance = 650}, -- guardian shield
			{id = 5887, chance = 1300}, -- piece of royal steel
			{id = 2149, count = 2, chance = 2260}, -- small emerald
			{id = 14327, chance = 820}, -- plate legs
			{id = {8473, 8472}, count = 2, chance = 900}, -- UHP
			{id = 16143, chance = 180}, -- golden knife
			{id = 13944, chance = 490}, -- ashbringer boots
			{id = 2170, chance = 4410, subType = 100}, -- silver amulet
			{id = {7591, 7590}, count = 3, chance = 490}, -- GHP
			{id = 14000, chance = 11000}, -- light armor
			{id = 2666, count = 2, chance = 11310}, -- meat
			{id = 2152, chance = 12140}, -- silver coin
			{id = 2148, count = {42, 68}, chance = 97540}, -- gold
		}
	},
	{ name = 'Dark Apprentice', level = 20, file = 'Cirith/Humans/dark apprentice.xml', look = {type = 133, head = 78, body = 57, legs = 95, feet = 115, addons = 1}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'M�odszy Mag Mroku', 'Dark Apprentice'}, experience = 140, health = 225, healthMax = 225,
		boss = 'Illusionary Hypnotizer-lurker', chance = 5,
		elements = {
			{ type = 'death', value = -5 },
		},
		loot = {
			{id = 16356, chance = 50}, -- container
			{id = 14448, chance = 50}, -- wand of decay
			{id = 14449, chance = 410}, -- wand of dragonbreath
			{id = 7618, chance = 1270}, -- health potion
			{id = 13755, chance = 1970}, -- safety pin
			{id = 7620, chance = 990}, -- mana potion
			{id = 13754, chance = 2870}, -- rope belt
			{id = 13944, chance = 30}, -- ashbringer boots
			{id = 5934, chance = 12060}, -- dead frog
			{id = 2148, count = {9, 18}, chance = 75070}, -- gold
		}
	},
	{ name = 'Dark Magician', level = 30, file = 'Cirith/Humans/dark magician.xml', look = {type = 133, head = 58, body = 95, legs = 51, feet = 131, addons = 2}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'Mag Mroku', 'Dark Magician'}, experience = 185, health = 325, healthMax = 325,
		boss = 'Illusionary Hypnotizer-lurker', chance = 10,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14455, chance = 90}, -- rod
			{id = 7762, chance = 510}, -- small enchanted amethyst
			{id = 7589, chance = 860}, -- SMP
			{id = 5809, chance = 2000}, -- magical element
			{id = 7588, chance = 280}, -- SHP
			{id = 7620, chance = 1180}, -- mana potion
			{id = 13754, chance = 3200}, -- rope belt
			{id = 13755, chance = 2270}, -- safety pin
			{id = 11694, chance = 910}, -- fine fabric
			{id = 13944, chance = 10}, -- ashbringer boots
			{id = 7618, chance = 1950}, -- health potion
			{id = 2148, count = {13, 22}, chance = 75010}, -- gold
		}
	},
	{ name = 'Hero', level = 68, file = 'Cirith/Humans/hero.xml', look = {type = 73}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Wyrzutek', 'Hero'}, experience = 1380, health = 2070, healthMax = 2070,
		boss = 'Huma Dragonbane', chance = 100,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 2114, chance = 60}, -- piggy bank
			{id = 13941, chance = 210}, -- royal steel crossbow
			{id = 15724, chance = 250}, -- berserker
			{id = 16294, chance = 30}, -- Holy staff of the day
			{id = 14319, chance = 290}, -- royal steel shield
			{id = 14322, chance = 420}, -- royal steel helmet
			{id = {16290, 16291}, chance = 30}, -- Hood/robe of the dawn
			{id = 2392, chance = 490}, -- fire sword
			{id = 2121, chance = 20}, -- wedding ring
			{id = 2164, chance = 510, subType = 20}, -- might ring
			{id = 14323, chance = 290}, -- royal steel armor
			{id = 14321, chance = 260}, -- royal steel legs
			{id = 17728, chance = 220}, -- sanctified spear
			{id = 7591, chance = 820}, -- GHP
			{id = 13754, chance = 4150}, -- rope belt
			{id = 5885, chance = 320}, -- warrior's sweat
			{id = 2661, chance = 5010}, -- scarf
			{id = 15723, chance = 680}, -- two handed sword
			{id = 2071, chance = 1520}, -- lyre
			{id = 5911, chance = 1930}, -- red poc
			{id = 2120, chance = 2060}, -- rope
			{id = 14765, chance = 8020}, -- cloak
			{id = 2666, chance = 8370}, -- meat
			{id = 13755, chance = 3000}, -- safety pin
			{id = 7364, count = 4, chance = 11770}, -- sniper arrow
			{id = 2456, chance = 13200}, -- bow
			{id = 2681, chance = 20210}, -- grapes
			{id = 2744, chance = 20600}, -- red rose
			{id = 2544, count = 13, chance = 26710}, -- arrow
			{id = 1949, chance = 45030}, -- scroll
			{id = 2148, count = {32, 59}, chance = 59720}, -- gold
			{id = 2145, chance = 4980}, -- small diamond
		}
	},
	{ name = 'Hunter', level = 26, file = 'Cirith/Humans/hunter.xml', look = {type = 129, head = 95, body = 116, legs = 121, feet = 115}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'�owca', 'Hunter'}, experience = 80, health = 150, healthMax = 150,
		boss = 'Nemrod', chance = 90,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 20 },
		},
		loot = {
			{id = 7400, chance = 50}, -- lion trophy
			{id = 7394, chance = 50}, -- wolf trophy
			{id = 2147, chance = 160}, -- small ruby
			{id = 7397, chance = 490}, -- deer trophy
			{id = 11402, chance = 170}, -- massive wooden bow
			{id = 2376, chance = 350}, -- runed blade
			{id = 19256, chance = 390}, -- leather quiver
			{id = 2201, chance = 3110, subType = 100}, -- dragon necklace
			{id = 2050, chance = 3340}, -- torch
			{id = 2545, count = 4, chance = 4540}, -- poison arrow
			{id = 15785, chance = 4960}, -- copper helmet
			{id = 15786, chance = 5230}, -- copper armor
			{id = 14169, chance = 500}, -- hat
			{id = 2546, count = 3, chance = 5370}, -- burst arrow
			{id = 2456, chance = 5850}, -- bow
			{id = 13754, chance = 4000}, -- rope belt
			{id = 2690, count = 2, chance = 11220}, -- roll
			{id = 2675, count = 2, chance = 20030}, -- orange
			{id = 13755, chance = 2100}, -- safety pin
			{id = 2544, count = 22, chance = 82130}, -- arrow
			{id = 2148, count = {9, 16}, chance = 59720}, -- gold
		}
	},
	{ name = 'Ice Witch', level = 60, file = 'Cirith/Humans/ice witch.xml', look = {type = 149, head = 0, body = 9, legs = 85, feet = 85}, classId = 29, killAmount = 1000, charmPoints = 25,
		description = {'Ice Witch', 'Ice Witch'}, experience = 380, health = 650, healthMax = 650,
		elements = {
			{ type = 'holy', value = 30 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 50 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 7449, chance = 270}, -- crystal sword
			{id = 7441, chance = 9900}, -- ice cube
			{id = 11694, chance = 790}, -- fine fabric
			{id = 2423, chance = 1050}, -- clerical mace
			{id = 17941, chance = 1190}, -- radiating powder
			{id = 13299, chance = 250}, -- glass spear
			{id = 7589, chance = 860}, -- GMP
			{id = 2663, chance = 390}, -- mystic turban
			{id = 7290, chance = 720}, -- shard
			{id = 12964, chance = 800}, -- clear lunar crystal
			{id = 7387, chance = 370}, -- diamond sceptre
			{id = 7892, chance = 290}, -- glacier boots
			{id = 7459, chance = 80}, -- ear muffs
			{id = 2796, chance = 1630}, -- green mushrooms
			{id = 2148, count = {12, 18}, chance = 5280}, -- gold
		}
	},
	{ name = 'Infernalist', level = 75, file = 'Cirith/Humans/infernalist.xml', look = {type = 130, head = 78, body = 76, legs = 94, feet = 97, addons = 2}, classId = 29, killAmount = 2500, charmPoints = 50,
		description = {'Piroman', 'Infernalist'}, experience = 2300, health = 3650, healthMax = 3650,
		boss = 'Illusionary Hypnotizer-lurker', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 65 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 80 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 16174, chance = 100}, -- ruby spear
			{id = 9971, chance = 550}, -- gold ingot
			{id = 15758, chance = 90}, --  snake boots
			{id = 9980, chance = 160}, -- piggy bank
			{id = 1985, chance = 170}, -- grey tome
			{id = 8902, chance = 780}, -- spellbook of mind control
			{id = 9958, chance = 540}, -- royal tapestry
			{id = 5904, chance = 590}, -- magic sulphur
			{id = 12964, chance = 600}, -- clear lunar crystal
			{id = 9969, chance = 750}, -- black skull
			{id = 11679, chance = 30}, -- hellfire bow
			{id = 5911, chance = 1450}, -- red poc
			{id = 2167, chance = 1880}, -- energy ring
			{id = 7760, chance = 440}, -- small enchanted ruby
			{id = 2392, chance = 6270}, -- fire sword
			{id = 8840, count = 5, chance = 8360}, -- raspberry
			{id = 7590, chance = 1980}, -- GMP
			{id = 7591, chance = 990}, -- GHP
			{id = 2148, count = {36, 64}, chance = 97270}, --  gold
		}
	},
	{ name = 'Knight Templar', level = 100, file = 'Cirith/Humans/knight templar.xml', look = {type = 380}, classId = 6, killAmount = 2500, charmPoints = 50,
		description = {'Templariusz', 'Knight Templar'}, experience = 4350, health = 5900, healthMax = 5900,
		boss = 'Goldmoon', chance = 100,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'death', value = -5 },
			{ type = 'holy', value = 35 },
			{ type = 'toxic', value = -5 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 15 },
			{ type = 'ice', value = 35 },
			{ type = 'fire', value = 30 },
		},
		loot = {
			{id = 10137, chance = 50}, -- PURPLE HEART
			{id = 16139, chance = 20}, -- golden helmet
			{id = 2153, chance = 10}, -- violet gem
			{id = 16207, chance = 80}, -- meteorite armor
			{id = 2542, chance = 110}, -- lightning shield
			{id = 2195, chance = 290}, -- boh
			{id = 16291, chance = 200}, -- robe of the dawn
			{id = 13997, chance = 210}, -- Warrior's armor
			{id = 16143, chance = 110}, -- golden blade
			{id = 1985, chance = 40}, -- grey tome
			{id = 15732, chance = 430}, -- knight axe
			{id = 15724, chance = 630}, -- berserker
			{id = 12956, chance = 280}, -- marksman helmet
			{id = 16164, chance = 140}, -- black steel shield
			{id = 16159, chance = 1160}, -- black steel axe
			{id = 14010, chance = 590}, -- knight armor
			{id = 18075, chance = 10}, -- purple dye
			{id = 2171, chance = 110}, -- platinum amulet
			{id = 17728, chance = 610}, -- sanctified spear
			{id = 5887, chance = 1200}, -- piece of royal steel
			{id = {8473, 8472}, chance = 490}, -- UHP
			{id = 2200, chance = 860}, -- protection amulet
			{id = 14350, chance = 320}, -- iron legs
			{id = 2150, count = 2, chance = 2170}, -- small amethyst
			{id = 2145, count = 2, chance = 2320}, -- small diamond
			{id = 14348, chance = 430}, -- iron armor
			{id = {7591, 7590}, count = 2, chance = 490}, -- GHP
			{id = 14170, chance = 510}, -- leggionnaire armor
			{id = 14000, chance = 1510}, -- light armor
			{id = 14317, chance = 340}, -- Enhanced oak shield
			{id = 2666, count = 2, chance = 9760}, -- meat
			{id = 1948, chance = 15320}, -- parchment
			{id = 2152, chance = 32310}, --  silver coin
			{id = 2148, count = {50, 86}, chance = 98480}, -- gold
		}
	},
	{ name = 'Monk', level = 32, file = 'Cirith/Humans/monk.xml', look = {type = 57}, classId = 6, killAmount = 500, charmPoints = 15,
		description = {'Mnich', 'Monk'}, experience = 130, health = 240, healthMax = 240,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
		},
		loot = {
			{id = 14778, chance = 440}, -- staff
			{id = 2642, chance = 500}, -- sandals
			{id = 2044, chance = 880}, -- lamp
			{id = 2015, chance = 890}, -- brown flask
			{id = 13754, chance = 3800}, -- rope belt
			{id = 2177, chance = 1050}, -- life crystal
			{id = 1949, chance = 1930}, -- scroll
			{id = 2193, chance = 2420}, -- ankh
			{id = 10562, chance = 4830}, -- book of prayers
			{id = 2148, count = 2, chance = 15180}, -- gold
			{id = 2689, chance = 19960}, -- bread
		}
	},
	{ name = 'Novice of the Cult', level = 30, file = 'Cirith/Humans/novice of the cult.xml', look = {type = 133, head = 114, body = 95, legs = 114, feet = 114}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'Nowicjusz Kultu', 'Novice of the Cult'}, experience = 170, health = 285, healthMax = 285,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 2145, chance = 230}, -- small diamond
			{id = 2199, chance = 430}, -- garlic necklace
			{id = 13754, chance = 2170}, -- rope belt
			{id = 5810, chance = 450}, -- pirate voodoo doll
			{id = 2213, chance = 450}, -- dwarven ring
			{id = 1976, chance = 640}, -- book
			{id = 10555, chance = 1100}, -- cultish robe
			{id = 2661, chance = 3160}, -- scarf
			{id = 2420, chance = 3320}, -- machete
			{id = 2148, count = 4, chance = 1470}, -- gold
		}
	},
	{ name = 'Outcast Bleed', level = 70, file = 'Cirith/Humans/outcast bleed.xml', look = {type = 391}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony Wygnaniec', 'Vexed Outcast'}, experience = 970, health = 1600, healthMax = 1600,
		boss = 'Nemrod', chance = 110,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 16157, chance = 50}, -- black steel armor
			{id = 16158, chance = 20}, -- black steel helmet
			{id = 5887, chance = 400}, -- piece of royal steel
			{id = 16239, chance = 740}, -- ticket
			{id = 14012, chance = 320}, -- crimson sword
			{id = 2145, count = 2, chance = 830}, -- small diamond
			{id = 14362, chance = 390}, -- steel armor
			{id = 14327, chance = 10}, -- steel legs
			{id = 5892, chance = 1140}, -- steel
			{id = 14351, chance = 350}, -- iron boots
			{id = 13754, chance = 6290}, -- rope belt
			{id = 13985, chance = 170}, -- armor of loyalists
			{id = 14324, chance = 800}, -- steel shield
			{id = 14336, chance = 4430}, -- hood
			{id = 15776, chance = 9780}, -- short sword
			{id = 2050, chance = 30040}, -- torch
			{id = 2148, count = {36, 71}, chance = 83150}, -- coin
		}
	},
	{ name = 'Outcast', level = 25, file = 'Cirith/Humans/outcast.xml', look = {type = 391}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'Wygnaniec', 'Outcast'}, experience = 115, health = 220, healthMax = 220,
		boss = 'Nemrod', chance = 50,
		loot = {
			{id = 15772, chance = 9420}, -- leather helmet
			{id = 15773, chance = 8100}, -- leather armor
			{id = 15774, chance = 8100}, -- leather legs
			{id = 5892, chance = 640}, -- steel
			{id = 16239, chance = 740}, -- ticket
			{id = 13754, chance = 9590}, -- rope belt
			{id = 15775, chance = 8100}, -- leather boots
			{id = 2145, chance = 840}, -- small diamond
			{id = 15778, chance = 8100}, -- wooden shield
			{id = {16305, 16306, 16307, 16308}, chance = 210}, -- enhanced leather set
			{id = 15783, chance = 10500}, -- breaker
			{id = 2050, chance = 30040}, -- torch
			{id = 2148, count = {12, 20}, chance = 83150}, -- coin
		}
	},
	{ name = 'Outlaw Bleed', level = 70, file = 'Cirith/Humans/outlaw bleed.xml', look = {type = 346, addons = 2}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony Banita', 'Outlaw Vexed'}, experience = 770, health = 1000, healthMax = 1000,
		boss = 'Nemrod', chance = 100,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 2110, chance = 70}, -- doll
			{id = 13896, chance = 300}, -- giant sword
			{id = 14363, chance = 210}, -- steel helmet
			{id = 14362, chance = 190}, -- steel armor
			{id = 5892, chance = 510}, -- steel
			{id = 2132, chance = 500, subType = 80}, -- silver necklace
			{id = 13754, chance = 5180}, -- rope belt
			{id = 13985, chance = 180}, -- armor of loyalists
			{id = 2147, count = 2, chance = 2420}, -- small ruby
			{id = 15774, chance = 6890}, -- skorzane spodnie
			{id = 14336, chance = 8290}, -- hood
			{id = 2695, count = 2, chance = 9820}, -- egg
			{id = 2691, chance = 29800}, -- bread
			{id = 2148, count = {29, 56}, chance = 49230}, -- coin
		}
	},
	{ name = 'Outlaw', level = 23, file = 'Cirith/Humans/outlaw.xml', look = {type = 346}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'Banita', 'Outlaw'}, experience = 75, health = 135, healthMax = 135,
		boss = 'Nemrod', chance = 50,
		loot = {
			{id = 15774, chance = 9990}, -- leather legs
			{id = 15783, chance = 3980}, -- breaker
			{id = {16305, 16306, 16307, 16308}, chance = 180}, -- enhanced leather set
			{id = 5892, chance = 390}, -- steel
			{id = 2110, chance = 470}, -- doll
			{id = 13754, chance = 8760}, -- rope belt
			{id = 2695, count = 2, chance = 9820}, -- egg
			{id = 2691, chance = 29800}, -- bread
			{id = 15779, chance = 30630}, -- skuller
			{id = 2148, count = 10, chance = 49230}, -- coin
		}
	},
	{ name = 'Peasant', level = 1, file = 'Cirith/Humans/peasant.xml', look = {type = 509, head = 79, body = 37, legs = 79, feet = 57, addons = 3},
		description = {'Wie�niak', 'Peasant'}, experience = 0, health = 200, healthMax = 200,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'death', value = -10 },
			{ type = 'toxic', value = -15 },
			{ type = 'earth', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2195, chance = 10}, -- boh
			{id = 7618, chance = 7490}, -- health potion
			{id = 14765, chance = 1510}, -- cape
			{id = 2148, count = 12, chance = 98480}, -- gold
		}
	},
	{ name = 'Vexed Buccaneer', level = 41, file = 'Cirith/Humans/vexed buccaneer.xml', look = {type = 97}, classId = 33, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony Grenadier', 'Vexed Buccaneer'}, experience = 975, health = 1400, healthMax = 1400,
		boss = 'Captain Hook', chance = 90,
		elements = {
			{ type = 'fire', value = 25 },
			{ type = 'holy', value = 15 },
			{ type = 'physical', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 14363, chance = 270}, -- steel helmet
			{id = 7588, count = 2, chance = 690}, -- SHP
			{id = 2171, chance = 210}, -- platinum amulet
			{id = 14007, chance = 410}, -- guardian shield
			{id = 11213, chance = 10900}, -- compass
			{id = 2238, chance = 14290}, -- worn leather boots 
			{id = 2131, chance = 10}, -- star amulet
			{id = 13948, chance = 20}, -- hunting spear
			{id = 5706, chance = 200}, -- pirate map
			{id = 5926, chance = 70}, -- pirate backpack
			{id = 5792, chance = 20}, -- dice
			{id = 2154, chance = 90}, -- yellow gem
			{id = 13988, chance = 50}, -- plate of underwater
			{id = 18072, chance = 110}, -- burning stick
			{id = 2187, chance = 200}, -- wand of Inferno
			{id = 2547, count = 8, chance = 14070}, -- power bolt
			{id = 15738, chance = 360}, -- long bow
			{id = 5878, chance = 500}, -- hardener leather
			{id = 2050, chance = 12500}, -- torch
			{id = 5553, chance = 610}, -- cask of rum
			{id = 8841, chance = 300}, -- lemon
			{id = 2144, chance = 1650}, -- black pearl
			{id = 6097, chance = 510}, -- hook
			{id = 6098, chance = 620}, -- eye patch
			{id = 10558, chance = 4000}, -- spookey blue eye
			{id = 2148, count = {20, 40}, chance = 76700}, -- gold
		}
	},
	{ name = 'Pirate Buccaneer', level = 41, file = 'Cirith/Humans/pirate buccaneer.xml', look = {type = 97}, classId = 33, killAmount = 500, charmPoints = 15,
		description = {'Pirat Grenadier', 'Pirate Buccaneer'}, experience = 230, health = 425, healthMax = 425,
		boss = 'Captain Hook', chance = 50,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 10 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 5926, chance = 60}, -- pirate backpack
			{id = 5792, chance = 70}, -- dice
			{id = 5553, chance = 130}, -- rum flask
			{id = 16143, chance = 90}, -- golden blade
			{id = 14363, chance = 150}, -- steel helmet
			{id = 6097, chance = 470}, -- hook
			{id = 5878, chance = 980}, -- hardener leather
			{id = 6098, chance = 480}, -- eye patch
			{id = 6126, chance = 520}, -- peg leg
			{id = 16293, chance = 50}, -- boots of the dawn
			{id = 2143, chance = 2180}, -- white pearl
			{id = 7588, chance = 690}, -- SHP
			{id = 8841, chance = 525}, -- lemon
			{id = 10558, chance = 3290}, -- spookey blue eye
			{id = 2138, chance = 700, subType = 100}, -- sapphire amulet
			{id = 14348, chance = 330}, -- iron armor
			{id = 5706, chance = 110}, -- pirate map
			{id = 6095, chance = 1190}, -- pirate shirt
			{id = 15784, chance = 3990}, -- copper shield
			{id = 2410, count = 5, chance = 8920}, -- throwing knife
			{id = 11213, chance = 9820}, -- compass
			{id = 2238, chance = 9890}, -- worn leather boots 
			{id = 15718, chance = 3900}, -- Chopper
			{id = 2050, chance = 10000}, -- torch
			{id = 2148, count = {18, 32}, chance = 67990}, -- gold
		}
	},
	{ name = 'Vexed Corsair', level = 85, file = 'Cirith/Humans/vexed corsair.xml', look = {type = 98}, classId = 33, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony Korsarz', 'Vexed Corsair'}, experience = 1050, health = 1700, healthMax = 1700,
		boss = 'Captain Hook', chance = 100,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'energy', value = 5 },
			{ type = 'physical', value = 5 },
			{ type = 'earth', value = -5 },
		},
		loot = {
			{id = 8261, chance = 10}, -- jeweller box
			{id = 13991, chance = 1}, -- mythril plate armor
			{id = 2130, chance = 60, subType = 200}, -- golden amulet
			{id = 2427, chance = 40}, -- deepling axe
			{id = 6534, chance = 30}, -- flying trident
			{id = 7588, count = 2, chance = 1150}, -- SHP
			{id = 2631, chance = 510}, -- knight legs
			{id = 14242, chance = 180}, -- heavy trident
			{id = 17683, chance = 30}, -- cursed blade
			{id = 5462, chance = 90}, -- pirate boots
			{id = 5926, chance = 150}, -- pirate backpack
			{id = 2121, chance = 15}, -- brass ring
			{id = 6095, chance = 2000}, -- pirate shirt
			{id = 5553, chance = 990}, -- cask of rum
			{id = 5812, chance = 150}, -- skull candle
			{id = 8841, chance = 290}, -- lemon
			{id = 2114, chance = 350}, -- piggy bank
			{id = 2144, count = 2, chance = 2650}, -- black pearl
			{id = 2143, count = 2, chance = 2890}, -- white pearl
			{id = 2157, chance = 1000}, -- gold nugget
			{id = 6097, chance = 320}, -- hook
			{id = 6098, chance = 500}, -- eye patch
			{id = 6126, chance = 660}, -- peg leg
			{id = 10558, chance = 4210}, -- spookey blue eye
			{id = 11213, chance = 12190}, -- compass
			{id = 2148, count = {18, 50}, chance = 64560}, -- gold
		}
	},
	{ name = 'Pirate Corsair', level = 69, file = 'Cirith/Humans/pirate corsair.xml', look = {type = 98}, classId = 33, killAmount = 500, charmPoints = 15,
		description = {'Pirat Korsarz', 'Pirate Corsair'}, experience = 350, health = 675, healthMax = 675,
		boss = 'Captain Hook', chance = 60,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 14010, chance = 15}, -- knight armor
			{id = 5462, chance = 50}, -- pirate boots
			{id = 5812, chance = 110}, -- skull candle
			{id = 2114, chance = 120}, -- piggy bank
			{id = 5926, chance = 90}, -- pirate backpack
			{id = 14318, chance = 50}, -- royal steel blade
			{id = 16293, chance = 10}, -- boots of the dawn
			{id = 2143, chance = 3480}, -- white pearl
			{id = 5553, chance = 160}, -- rum flask
			{id = 16143, chance = 10}, -- golden blade
			{id = 6098, chance = 470}, -- eye patch
			{id = 6097, chance = 550}, -- hook
			{id = 2157, chance = 930}, -- gold nugget
			{id = 6126, chance = 560}, -- peg leg
			{id = 7588, chance = 790}, -- SHP
			{id = 2521, chance = 170}, -- dark shield
			{id = 8841, chance = 120}, -- lemon
			{id = 6096, chance = 90}, -- pirate hat
			{id = 10558, chance = 4100}, -- spookey blue eye
			{id = 14001, chance = 300}, -- dark armor
			{id = 14358, chance = 900}, -- bronze axe
			{id = 11213, chance = 10570}, -- compass
			{id = 2148, count = {24, 47}, chance = 55350}, -- gold
		}
	},
	{ name = 'Vexed Cutthroat', level = 52, file = 'Cirith/Humans/vexed cutthroat.xml', look = {type = 96}, classId = 33, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony No�ownik', 'Vexed Cutthroat'}, experience = 725, health = 1200, healthMax = 1200,
		boss = 'Captain Hook', chance = 40,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'bleed', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'holy', value = -5 },
		},
		loot = {
			{id = 2195, chance = 90}, -- boh
			{id = 6098, chance = 620}, -- eye patch
			{id = 5918, chance = 820}, -- pirate knee breeches
			{id = 18338, chance = 60}, -- red gem
			{id = 14010, chance = 140}, -- knight armor
			{id = 2446, chance = 90}, -- pharaoh sword
			{id = 5462, chance = 190}, -- pirate boots
			{id = 5792, chance = 90}, -- dice
			{id = 15722, chance = 190}, -- scimitar
			{id = 6131, chance = 330}, -- tortoise shield
			{id = 14325, chance = 690}, -- steel blade
			{id = 5927, chance = 1200}, -- pirate bag
			{id = 6095, chance = 760}, -- pirate shirt
			{id = 5892, chance = 1320}, -- steel
			{id = 2050, chance = 18760}, -- torch
			{id = 5553, chance = 2490}, -- cask of rum
			{id = 8841, chance = 310}, -- lemon
			{id = 2144, chance = 3200}, -- black pearl
			{id = 6126, chance = 990}, -- peg leg
			{id = 11213, chance = 12320}, -- compass
			{id = 10558, chance = 4390}, -- spookey blue eye
			{id = 2148, count = {20, 35}, chance = 81780}, -- gold
		}
	},
	{ name = 'Pirate Cutthroat', level = 52, file = 'Cirith/Humans/pirate cutthroat.xml', look = {type = 96}, classId = 33, killAmount = 500, charmPoints = 15,
		description = {'Pirat No�ownik', 'Pirate Cutthroat'}, experience = 175, health = 325, healthMax = 325,
		boss = 'Captain Hook', chance = 40,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2195, chance = 50}, -- boh
			{id = 5553, chance = 90}, -- rum flask
			{id = 5792, chance = 90}, -- dice
			{id = 14003, chance = 120}, -- light boots
			{id = 13984, chance = 70}, -- lojalist armor
			{id = 6098, chance = 470}, -- eye patch
			{id = 5878, chance = 950}, -- hardener leather
			{id = 2143, chance = 2180}, -- white pearl
			{id = 6126, chance = 500}, -- peg leg
			{id = 8841, chance = 200}, -- lemon
			{id = 6097, chance = 520}, -- hook
			{id = 10558, chance = 2700}, -- spookey blue eye
			{id = 5927, chance = 970}, -- pirate bag
			{id = 5918, chance = 1030}, -- pirate knee breeches
			{id = 5706, chance = 10}, -- pirate map
			{id = 5710, chance = 1910}, -- light shovel
			{id = 14357, chance = 2860}, -- bronze shield
			{id = 14170, chance = 3040}, -- armor
			{id = 11213, chance = 10080}, -- compass
			{id = 2148, count = {16, 26}, chance = 77450}, -- gold
		}
	},
	{ name = 'Vexed Marauder', level = 40, file = 'Cirith/Humans/pirate marauder.xml', look = {type = 93}, classId = 33, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony Maruder', 'Vexed Marauder'}, experience = 570, health = 900, healthMax = 900,
		boss = 'Captain Hook', chance = 30,
		elements = {
			{ type = 'earth', value = 20 },
			{ type = 'energy', value = 15 },
			{ type = 'physical', value = -5 },
			{ type = 'ice', value = -5 },
			{ type = 'fire', value = -5 },
			{ type = 'bleed', value = -10 },
		},
		loot = {
			{id = 16307, chance = 310}, -- enhanced leather legs
			{id = 18338, chance = 60}, -- red gem
			{id = 14362, chance = 260}, -- plate armor
			{id = 5706, chance = 80}, -- pirate map
			{id = 15793, chance = 120}, -- sword
			{id = 15722, chance = 80}, -- scimitar
			{id = 14433, chance = 5}, -- leaden helmet
			{id = 17655, chance = 10}, -- smugglers boots
			{id = 5927, chance = 90}, -- pirate bag
			{id = 12959, chance = 800}, -- lampart legs
			{id = 14319, chance = 520}, -- steel shield
			{id = 15782, chance = 690}, -- steel spear
			{id = 2169, chance = 750}, -- time ring
			{id = 7620, chance = 800}, -- mana potion
			{id = 9970, chance = 2030}, -- small topaz
			{id = 14462, chance = 1200}, -- bandana
			{id = 2050, chance = 12320}, -- torch
			{id = 5553, chance = 500}, -- cask of rum
			{id = 8841, chance = 400}, -- lemon
			{id = 2159, chance = 1200}, -- scarab coin
			{id = 10558, chance = 3200}, -- spookey blue eye
			{id = 2148, count = {10, 25}, chance = 86690}, -- gold
		}
	},
	{ name = 'Pirate Marauder', level = 40, file = 'Cirith/Humans/pirate marauder.xml', look = {type = 93}, classId = 33, killAmount = 500, charmPoints = 15,
		description = {'Pirat Maruder', 'Pirate Marauder'}, experience = 115, health = 210, healthMax = 210,
		boss = 'Captain Hook', chance = 30,
		elements = {
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 5928, chance = 80}, -- goldfish bowl
			{id = 5792, chance = 90}, -- dice
			{id = 5553, chance = 10}, -- rum flask
			{id = 18338, chance = 60}, -- red gem
			{id = 8841, chance = 330}, -- lemon
			{id = 14351, chance = 10}, -- iron boots
			{id = 5927, chance = 450}, -- pirate bag
			{id = 15715, chance = 290}, -- Cranial basher
			{id = 6126, chance = 500}, -- peg leg
			{id = 6097, chance = 510}, -- hook
			{id = 6098, chance = 540}, -- eye patch
			{id = 10558, chance = 2120}, -- spookey blue eye
			{id = 14462, chance = 900}, -- bandana
			{id = 5706, chance = 30}, -- pirate map
			{id = 14357, chance = 1350}, -- bronze shield
			{id = 12960, chance = 3070}, -- lampart armor
			{id = 15782, chance = 50}, -- steel spear
			{id = 11213, chance = 9640}, -- compass
			{id = 2050, chance = 9890}, -- torch
			{id = 2148, count = 21, chance = 77680}, -- gold
		}
	},
	{ name = 'Poacher', level = 20, file = 'Cirith/Humans/poacher.xml', look = {type = 129, head = 95, body = 116, legs = 121, feet = 115, addons = 1}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'K�usownik', 'Poacher'}, experience = 50, health = 90, healthMax = 90,
		boss = 'Nemrod', chance = 30,
		loot = {
			{id = 19256, chance = 150}, -- leather quiver
			{id = 2578, chance = 800}, -- trap
			{id = 2545, count = 3, chance = 2710}, -- poison arrow
			{id = 2050, chance = 4350}, -- torch
			{id = 14169, chance = 450}, -- hat
			{id = 2690, count = 2, chance = 11370}, -- roll
			{id = 2456, chance = 14610}, -- bow
			{id = 19219, chance = 1990}, -- quiver
			{id = 13754, chance = 4500}, -- rope belt
			{id = 13755, chance = 5290}, -- safety pin
			{id = 15774, chance = 26450}, -- leather legs
			{id = 14336, chance = 30990}, -- cape
			{id = 2544, count = 17, chance = 13040}, -- arrow
			{id = 2148, count = 11, chance = 59720}, -- gold
		}
	},
	{ name = 'Savage Female', level = 65, file = 'Cirith/Humans/savage female.xml', look = {type = 158, head = 78, body = 109, legs = 53, feet = 118, addons = 1}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Dzikuska', 'Savage'}, experience = 660, health = 1000, healthMax = 1000,
		boss = 'Hand Faerie', chance = 100,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 11387, chance = 150}, -- luck amulet
			{id = 13990, chance = 110}, -- emerald armor
			{id = 7435, chance = 310}, -- impaler
			{id = 16356, chance = 10}, -- bag
			{id = 16168, chance = 180}, -- swamplair helmet
			{id = 16166, chance = 190}, -- swamplair legs
			{id = 15756, chance = 150}, -- snake armor
			{id = 15758, chance = 500}, -- snake boots
			{id = 13743, chance = 4180}, -- colourful feather
			{id = 2129, chance = 690}, -- wolf tooth chain
			{id = 2168, chance = 900}, -- life ring
			{id = 13980, chance = 1000, subType = 150}, -- terra amulet
			{id = {2149, 2145}, chance = 2800}, -- small emerald
			{id = 14000, chance = 3330}, -- light armor
			{id = 13581, chance = 4190}, -- creature products
			{id = 7588, chance = 700}, -- smp
			{id = 13577, chance = 7590}, -- creature products
			{id = 2152, chance = 53450}, -- silver
			{id = 2148, count = {15, 41}, chance = 99730}, -- gold
		}
	},
	{ name = 'Savage Hunter Male', level = 68, file = 'Cirith/Humans/savage hunter male.xml', look = {type = 154, head = 25, body = 115, legs = 57, feet = 119, addons = 3}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Dziki Oszczepnik', 'Savage Javelin'}, experience = 710, health = 1170, healthMax = 1170,
		boss = 'Hand Faerie', chance = 100,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 80 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 11387, chance = 140}, -- luck amulet
			{id = 13990, chance = 150}, -- emerald armor
			{id = 13847, chance = 120}, -- leaf legs
			{id = 16356, chance = 30}, -- bag
			{id = 13743, chance = 3900}, -- colourful feather
			{id = 1998, chance = 10}, -- green backpack
			{id = {15760, 16169}, chance = 110}, -- snake/swamplair bow
			{id = 14351, chance = 320}, -- iron boots
			{id = 13940, chance = 190}, -- snake crossbow
			{id = 12952, chance = 2140}, -- bronze helmet
			{id = 2129, chance = 490}, -- wolf tooth chain
			{id = 13980, chance = 500, subType = 70}, -- earth amulet
			{id = 14323, chance = 390}, -- royal steel armor
			{id = {2149, 2145}, chance = 1000}, -- small emerald
			{id = 13948, chance = 420}, -- hunting spear
			{id = 13584, chance = 19800}, -- primitive spear
			{id = 14000, chance = 3330}, -- light armor
			{id = 13581, chance = 4190}, -- creature products
			{id = 7588, chance = 700}, -- shp
			{id = 13577, chance = 7590}, -- creature products
			{id = 2152, chance = 63450}, -- silver
			{id = 2148, count = {15, 40}, chance = 99730}, -- gold
		}
	},
	{ name = 'Savage Mage Female', level = 75, file = 'Cirith/Humans/savage mage female.xml', look = {type = 158, head = 102, body = 104, legs = 47, feet = 42, addons = 3}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Dzika Czarodziejka', 'Savage Sorceress'}, experience = 700, health = 1035, healthMax = 1035,
		boss = 'Hand Faerie', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'death', value = 25 },
			{ type = 'energy', value = 80 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 11387, chance = 130}, -- luck amulet
			{id = 3982, chance = 50}, -- green robe
			{id = {13848, 13847}, chance = 20}, -- leaf legs/armor
			{id = 16356, chance = 20}, -- bag
			{id = 1998, chance = 130}, -- green backpack
			{id = {15760, 16169}, chance = 180}, -- snake/swamplair bow
			{id = 14351, chance = 620}, -- iron boots
			{id = 13940, chance = 290}, -- snake crossbow
			{id = 12952, chance = 5140}, -- bronze helmet
			{id = 2129, chance = 490}, -- wolf tooth chain
			{id = 13980, chance = 1500, subType = 70}, -- earth amulet
			{id = 14323, chance = 310}, -- royal steel armor
			{id = 13743, chance = 5260}, -- colourful feather
			{id = 12964, chance = 90}, -- clear lunar crystal
			{id = {2149, 2145}, chance = 1000}, -- small emerald
			{id = 13948, chance = 450}, -- hunting spear
			{id = 14000, chance = 8330}, -- light armor
			{id = 13581, chance = 4190}, -- creature products
			{id = 7588, chance = 700}, -- shp
			{id = 13577, chance = 7590}, -- creature products
			{id = 2152, chance = 53450}, -- silver
			{id = 2148, count = {15, 50}, chance = 99730}, -- gold
		}
	},
	{ name = 'Savage Male', level = 70, file = 'Cirith/Humans/savage male.xml', look = {type = 154, head = 93, body = 111, legs = 68, feet = 3, addons = 1}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Dziki Stra�nik', 'Savage Guard'}, experience = 700, health = 1050, healthMax = 1050,
		boss = 'Hand Faerie', chance = 100,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 5 },
		},
		loot = {
			{id = 13990, chance = 50}, -- emerald armor
			{id = 16356, chance = 15}, -- bag
			{id = 13848, chance = 80}, -- leaf armor
			{id = 16287, chance = 250}, -- blood herb
			{id = 15761, chance = 240}, -- snake blade
			{id = 2129, chance = 690}, -- wolf tooth chain
			{id = 2168, chance = 900}, -- life ring
			{id = {2149, 2145}, chance = 2370}, -- small emerald
			{id = 12960, chance = 4250}, -- lampart armor
			{id = 13743, chance = 5000}, -- colourful feather
			{id = 7588, chance = 540}, -- smp
			{id = 13577, chance = 8210}, -- creature products
			{id = 2152, chance = 70210}, -- silver
			{id = 2148, count = {19, 36}, chance = 99730}, -- gold
		}
	},
	{ name = 'Slinger Bleed', level = 65, file = 'Cirith/Humans/slinger bleed.xml', look = {type = 397, addons = 2}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony Procarz', 'Vexed Slinger'}, experience = 450, health = 830, healthMax = 830,
		boss = 'Nemrod', chance = 80,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 7397, chance = 20}, -- deer trophy
			{id = 16168, chance = 50}, -- swamplair helmet
			{id = 15753, chance = 420}, -- brass boots
			{id = 2209, chance = 500}, -- ring
			{id = 14003, chance = 490}, -- light boots
			{id = 14363, chance = 200}, -- steel helmet
			{id = 2643, chance = 720}, -- breastplate
			{id = 5892, chance = 1090}, -- steel
			{id = 15609, chance = 80}, -- massive ornamented bow
			-- {id = 16240, chance = 3160}, --bag 
			{id = 8840, count = 5, chance = 4940}, -- raspberry
			{id = 2379, chance = 9880}, -- blade
			{id = 15722, chance = 10040}, -- scimitar
			{id = 2691, chance = 10150}, -- bread
			{id = 1294, count = 8, chance = 12480}, -- small stone
			{id = 15774, chance = 14780}, -- skorzane nogawice
			{id = 2148, count = 34, chance = 79690}, -- coin
		}
	},
	{ name = 'Slinger', level = 25, file = 'Cirith/Humans/slinger.xml', look = {type = 397}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'Procarz', 'Slinger'}, experience = 70, health = 130, healthMax = 130,
		boss = 'Nemrod', chance = 30,
		loot = {
			{id = 7397, chance = 20}, -- deer trophy
			{id = 19219, chance = 400}, -- quiver
			{id = 8840, count = 5, chance = 4940}, -- raspberry
			{id = 2379, chance = 9880}, -- blade
			{id = {16305, 16306, 16307, 16308}, chance = 150}, -- enhanced leather set
			{id = 5892, chance = 430}, -- steel
			{id = 15793, chance = 2070}, -- sword
			{id = 13754, chance = 9000}, -- rope belt
			{id = 2691, chance = 10150}, -- bread
			{id = 1294, count = 2, chance = 12480}, -- small stone
			{id = 2148, count = 7, chance = 79690}, -- coin
		}
	},
	{ name = 'Stalker', level = 30, file = 'Cirith/Humans/stalker.xml', look = {type = 128, head = 95, body = 116, legs = 95, feet = 114}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'Stalker', 'Stalker'}, experience = 70, health = 120, healthMax = 120,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 1 },
		},
		loot = {
			{id = 15722, chance = 530}, -- scimitar
			{id = 14333, chance = 1210}, -- zdobiony sword
			{id = 15752, chance = 3500}, -- brass legs
			{id = 15784, chance = 5550}, -- copper shield
			{id = 2148, count = 8, chance = 13040}, -- gold
		}
	},
	{ name = 'Teracian', level = 42, file = 'Cirith/Humans/teracian.xml', look = {type = 130, head = 95, body = 57, legs = 120, feet = 114, addons = 3}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'Teracian', 'Teracian'}, experience = 600, health = 1050, healthMax = 1050,
		boss = 'Illusionary Hypnotizer-lurker', chance = 50,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'death', value = 15 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 2155, chance = 150}, -- green gem
			{id = 16143, chance = 50}, -- golden knife
			{id = 14006, chance = 330}, -- cape of fear
			{id = 12964, chance = 200}, -- clear lunar crystal
			{id = 2070, chance = 230}, -- wooden flute
			{id = 14479, chance = 210}, -- strange helmet
			{id = 14005, chance = 150}, -- wizard's robe
			{id = 7761, chance = 540}, -- small enchanted emerald
			{id = 8902, chance = 110}, -- spellbook
			{id = 14014, chance = 1530}, -- magic coat
			{id = 13754, chance = 3190}, -- rope belt
			{id = 7732, chance = 2350}, -- seeds
			{id = 1860, chance = 2600}, -- green tapestry
			{id = 5910, chance = 3710}, -- green poc
			{id = 2420, chance = 4560}, -- machete
			{id = 2146, chance = 6150}, -- small sapphire
			{id = 7589, chance = 960}, -- SMP
			{id = 13948, chance = 10}, -- spear
			{id = 2677, count = 25, chance = 25930}, -- blueberry
			{id = 2148, count = {31, 79}, chance = 96850}, -- gold
		}
	},
	{ name = 'Valkyrie', level = 19, file = 'Cirith/Humans/valkyrie.xml', look = {type = 139, head = 113, body = 57, legs = 95, feet = 113}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'Walkiria', 'Valkyrie'}, experience = 175, health = 290, healthMax = 290,
		boss = 'Nemrod', chance = 70,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 14487, chance = 1600}, -- chain helmet
			{id = {13055, 13070, 13058, 13061}, chance = 150}, -- bronze crafting additives
			{id = 2145, chance = 140}, -- small diamond
			{id = 7618, chance = 490}, -- health potion
			{id = 2143, chance = 1550}, -- white pearl
			{id = 2229, chance = 760}, -- skull
			{id = 17656, chance = 300}, -- copper blade
			{id = 5892, chance = 310}, -- steel
			{id = 15782, chance = 120}, -- steel spear
			{id = 13755, chance = 2800}, -- safety pin
			{id = {16305, 16306, 16307, 16308}, chance = 440}, -- enhanced leather set
			{id = 13754, chance = 9590}, -- rope belt
			{id = 13731, chance = 3240}, -- protective charm
			{id = 13730, chance = 5910}, -- girlish hair decoration
			{id = 2674, count = 2, chance = 7330}, -- apples
			{id = 2666, chance = 30180}, -- meat
			{id = 2148, count = 12, chance = 32560}, -- gold
		}
	},
	{ name = 'Villain Bleed', level = 68, file = 'Cirith/Humans/villain bleed.xml', look = {type = 397, addons = 3}, classId = 6, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony �otr', 'Vexed Villain'}, experience = 610, health = 1060, healthMax = 1060,
		boss = 'Nemrod', chance = 100,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'holy', value = 25 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 19257, chance = 90}, -- fur quiver
			{id = 2195, chance = 90}, -- boh
			{id = 5896, chance = 190}, -- bear paw
			{id = 2130, chance = 200, subType = 40}, -- golden amulet
			{id = 16239, chance = 240}, -- ticket
			{id = 11435, count = 7, chance = 490}, -- crystalline arrow
			{id = 15738, chance = 290}, -- long bow
			{id = 14003, chance = 390}, -- light boots
			{id = 13754, chance = 7170}, -- rope belt
			{id = 14326, chance = 50}, -- steel boots
			{id = 19256, chance = 880}, -- leather quiver
			{id = 14171, chance = 700}, -- berynit armor
			{id = 13942, chance = 50}, -- repeating crossbow
			{id = 2661, chance = 1640}, -- scarf
			{id = 5892, chance = 1140}, -- steel
			{id = 15609, chance = 400}, -- massive ornamented bow
			{id = 7588, chance = 310}, -- health potion
			{id = 2690, count = 2, chance = 11360}, -- roll
			{id = 2456, chance = 14750}, -- bow
			{id = 7365, count = 5, chance = 18580}, -- arrow
			{id = 14779, chance = 26440}, -- blade
			{id = 14336, chance = 30920}, -- hood
			{id = 2148, count = 29, chance = 56290}, -- coin
		}
	},
	{ name = 'Villain', level = 25, file = 'Cirith/Humans/villain.xml', look = {type = 397, addons = 1}, classId = 6, killAmount = 250, charmPoints = 5,
		description = {'�otr', 'Villain'}, experience = 100, health = 180, healthMax = 180,
		boss = 'Nemrod', chance = 40,
		loot = {
			{id = 19256, chance = 200}, -- leather quiver
			{id = 16239, chance = 240}, -- ticket
			{id = 5896, chance = 590}, -- bear paw
			{id = 5892, chance = 210}, -- steel
			{id = 19219, chance = 2500}, -- quiver
			{id = 2050, chance = 4260}, -- torch
			{id = {16305, 16306, 16307, 16308}, chance = 250}, -- enhanced leather set
			{id = 13754, chance = 7650}, -- rope belt
			{id = 2690, count = 2, chance = 11360}, -- roll
			{id = 2379, chance = 26440}, -- blade
			{id = 15773, chance = 7640}, -- leather armor
			{id = 14779, chance = 13560}, -- knife
			{id = 2129, chance = 660}, -- Wolf Tooth Chain
			{id = 2456, chance = 14750}, -- bow
			{id = 15738, chance = 90}, -- long bow
			{id = 2544, count = 20, chance = 28580}, -- poison arrow
			{id = 2544, count = 20, chance = 18580}, -- arrow
			{id = 2148, count = 11, chance = 56290}, -- coin
		}
	},
	{ name = 'Warlock', level = 100, file = 'Cirith/Humans/warlock.xml', look = {type = 130, head = 19, body = 71, legs = 128, feet = 128, addons = 1}, classId = 29, killAmount = 2500, charmPoints = 50,
		description = {'Czarnoksi�nik', 'Warlock'}, experience = 3000, health = 3500, healthMax = 3500,
		boss = 'Infernal Oozer-orb', chance = 120,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 25 },
			{ type = 'energy', value = 80 },
			{ type = 'death', value = -10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 25 },
		},
		loot = {
			{id = 2123, chance = 50}, -- rots
			{id = 2114, chance = 50}, -- piggy bank
			{id = 16138, chance = 60}, -- golden armor
			{id = 13754, chance = 6700}, -- rope belt
			{id = 1985, chance = 270}, -- grey tome
			{id = 18073, chance = 350}, -- ethereal staff
			{id = 2197, chance = 320, subType = 4}, -- ssa
			{id = 2124, chance = 690}, -- crystal ring
			{id = 14504, chance = 500}, -- magical thread
			{id = 2600, chance = 940}, -- inkwell
			{id = 14009, chance = 150}, -- lightining robe
			{id = 12964, chance = 900}, -- clear lunar crystal
			{id = 2151, chance = 8100}, -- talon
			{id = 14450, chance = 250}, -- staff of powerful energy
			{id = 2146, chance = 11180}, -- small sapphire
			{id = 14005, chance = 150}, -- wizard's robe
			{id = 2047, chance = 1450}, -- candlestick
			{id = 5809, chance = 3190}, -- magical element
			{id = 2178, chance = 2120}, -- mind stone
			{id = 2167, chance = 2120}, -- energy ring
			{id = 2792, chance = 3070}, -- dark mushroom
			{id = 14325, chance = 730}, -- steel blade
			{id = 7590, chance = 1770}, -- GMP
			{id = 7591, chance = 1100}, -- GHP
			{id = 15761, chance = 760}, -- snake blade
			{id = 2689, chance = 9040}, -- bread
			{id = 2679, count = 4, chance = 19680}, -- cherry
			{id = 2148, count = {22, 87}, chance = 29380}, -- gold
		}
	},
	{ name = 'Witch', level = 20, file = 'Cirith/Humans/witch.xml', look = {type = 54}, classId = 29, killAmount = 500, charmPoints = 15,
		description = {'Wied�ma', 'Witch'}, experience = 160, health = 300, healthMax = 300,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = -5 },
		},
		loot = {
			{id = 15776, chance = 10}, -- killer's blade
			{id = 14779, chance = 10100}, -- knife
			{id = 11205, chance = 10}, -- toad
			{id = 10569, chance = 80}, -- witch hat
			{id = 13755, chance = 1760}, -- safety pin
			{id = 13754, chance = 970}, -- rope belt
			{id = {16305, 16306, 16307, 16308}, chance = 450}, -- enhanced leather set
			{id = 16356, chance = 10}, -- container
			{id = 18072, chance = 210}, -- burning stick
			{id = 2143, chance = 6840}, -- white pearl
			{id = 11694, chance = 490}, -- fine fabric
			{id = 5809, chance = 2890}, -- magical element
			{id = 17941, chance = 1090}, -- radiating powder
			{id = 2129, chance = 1060}, -- Wolf Tooth Chain
			{id = 18338, chance = 70}, -- red gem
			{id = 2800, chance = 9080}, -- star herb
			{id = 10568, chance = 10230}, -- witch broom
			{id = 2687, count = 8, chance = 30030}, ---cookie
			{id = 2148, count = 15, chance = 64260}, -- gold
		}
	},
	
	-- Lizards
	{ name = 'Lizard Chosen', level = 80, file = 'Cirith/Lizards/lizard chosen.xml', look = {type = 344}, classId = 9, killAmount = 1000, charmPoints = 25,
		description = {'Jaszczurzy Wybraniec', 'Lizard Chosen'}, experience = 2100, health = 3050, healthMax = 3050,
		boss = 'The Chosen One', chance = 120,
		elements = {
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 16158, chance = 90}, -- black steel helmet
			{id = 16155, chance = 300}, -- black steel shoes
			{id = 16156, chance = 120}, -- black steel legs
			{id = 16157, chance = 120}, -- black steel armor
			{id = 5881, chance = 1000}, -- lizard scale
			{id = 2440, chance = 220}, -- daramanian axe
			{id = 14319, chance = 160}, -- tower shield
			{id = 5876, chance = 2060}, -- lizard leather
			{id = 13151, chance = 990}, -- lizard tail
			{id = 5892, chance = 10}, -- huge chunk of cude iron
			{id = 2145, count = 5, chance = 2510}, -- small diamond
			{id = 11320, chance = 2990}, -- corrupted flag
			{id = 11321, chance = 5910}, -- cursed shoulder spikes
			{id = 7591, count = 2, chance = 640}, -- GHP
			{id = 11319, chance = 9930}, -- spiked iron ball
			{id = 2152, chance = 22950}, -- silver coin
			{id = 2148, count = {35, 70}, chance = 97530}, -- gold
		}
	},
	{ name = 'Lizard Dragon Priest', level = 72, file = 'Cirith/Lizards/lizard dragon priest.xml', look = {type = 339}, classId = 9, killAmount = 1000, charmPoints = 25,
		description = {'Jaszczurzy Smoczy Kap�an', 'Lizard Dragon Priest'}, experience = 1020, health = 1450, healthMax = 1450,
		boss = 'The Chosen One', chance = 100,
		elements = {
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 85 },
		},
		loot = {
			{id = 2154, chance = 10}, -- yellow gem
			{id = 16155, chance = 120}, -- black steel shoes
			{id = 14005, chance = 10}, -- robe
			{id = 14006, chance = 360}, -- cape of fear
			{id = 5809, chance = 7790}, -- magical element
			{id = 2168, chance = 760}, -- life ring
			{id = 11239, chance = 960}, -- bunch of ripe rice
			{id = 14453, chance = 400}, -- terra rod
			{id = 5876, chance = 1020}, -- lizard leather
			{id = 5881, chance = 1090}, -- lizard scale
			{id = 13151, chance = 720}, -- lizard tail
			{id = 14447, chance = 560}, --WoI
			{id = 2150, count = 3, chance = 4870}, -- small amethyst
			{id = 7590, chance = 980}, -- GMP
			{id = 11355, chance = 9960}, -- dragon priest's wandtip
			{id = 7589, chance = 830}, -- SMP
			{id = 2152, chance = 84120}, -- silver
			{id = 2148, count = {28, 60}, chance = 93890}, -- gold
		}
	},
	{ name = 'Lizard High Guard', level = 76, file = 'Cirith/Lizards/lizard high guard.xml', look = {type = 337}, classId = 9, killAmount = 1000, charmPoints = 25,
		description = {'Jaszczurzy Wy�szy Stra�nik', 'Lizard High Guard'}, experience = 1350, health = 1800, healthMax = 1800,
		boss = 'The Chosen One', chance = 100,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 16157, chance = 240}, -- black steel armor
			{id = 16155, chance = 290}, -- black steel shoes
			{id = 16156, chance = 140}, -- black steel legs
			{id = 5881, chance = 970}, -- lizard scale
			{id = 5876, chance = 1000}, -- lizard leather
			{id = 14319, chance = 240}, -- tower shield
			{id = 2149, count = 4, chance = 2510}, -- small emerald
			{id = 13151, chance = 780}, -- lizard tail
			{id = 11326, chance = 3010}, -- high guard's flag
			{id = 11239, chance = 4910}, -- bunch of ripe rice
			{id = 11319, chance = 6960}, -- spiked iron ball
			{id = 7591, chance = 860}, -- GHP
			{id = 11327, chance = 8110}, -- high guard shoulderplates
			{id = 7588, chance = 650}, -- SHP
			{id = 2152, chance = 74950}, -- silver coin
			{id = 2148, count = {17, 34}, chance = 96080}, -- gold
		}
	},
	{ name = 'Lizard Legionnaire', level = 70, file = 'Cirith/Lizards/lizard legionnaire.xml', look = {type = 338}, classId = 9, killAmount = 1000, charmPoints = 25,
		description = {'Jaszczurzy Legionista', 'Lizard Legionnaire'}, experience = 1000, health = 1400, healthMax = 1400,
		boss = 'The Chosen One', chance = 90,
		elements = {
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 11330, chance = 10}, -- lizard trophy
			{id = 16157, chance = 220}, -- black steel armor
			{id = 13949, chance = 90}, -- golden spear
			{id = 16155, chance = 320}, -- black steel shoes
			{id = 15729, chance = 220}, -- double axe
			{id = 13151, chance = 320}, -- lizard tail
			{id = 5876, chance = 970}, -- lizard leather
			{id = 5881, chance = 970}, -- lizard scale
			{id = 2145, count = 2, chance = 980}, -- small diamond
			{id = 13948, chance = 290}, -- hunting spear
			{id = 15761, chance = 220}, -- snake blade
			{id = 11328, chance = 1920}, -- legionnaire flags
			{id = 11239, chance = 1930}, -- bunch of ripe rice
			{id = 7588, chance = 890}, -- SHP
			{id = 11329, chance = 14880}, -- broken halberd
			{id = 2152, chance = 44120}, -- silver
			{id = 2148, count = {15, 40}, chance = 94120}, -- gold
		}
	},
	{ name = 'Lizard Sentinel', level = 28, file = 'Cirith/Lizards/lizard sentinel.xml', look = {type = 114}, classId = 9, killAmount = 500, charmPoints = 15,
		description = {'Jaszczurzy Stra�nik', 'Lizard Sentinel'}, experience = 140, health = 265, healthMax = 265,
		elements = {
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 15784, chance = 250}, -- copper shield
			{id = 2145, chance = 260}, -- small diamond
			{id = 7618, chance = 480}, -- health potion
			{id = 15713, chance = 240}, -- longsword
			{id = 15761, chance = 10}, -- snake blade
			{id = 13152, chance = 440}, -- lizard tail
			{id = 15790, chance = 640}, -- copper axe
			{id = 5881, chance = 940}, -- lizard scale
			{id = 5876, chance = 3210}, -- lizard leather
			{id = 13948, chance = 150}, -- hunting spear
			{id = 14170, chance = 540}, -- armor
			{id = 12960, chance = 12820}, -- lampart armor
			{id = 2148, count = 8, chance = 88460}, -- gold
		}
	},
	{ name = 'Lizard Snakecharmer', level = 32, file = 'Cirith/Lizards/lizard snakecharmer.xml', look = {type = 115}, classId = 9, killAmount = 500, charmPoints = 15,
		description = {'Jaszczurzy Zaklinacz', 'Lizard Snakecharmer'}, experience = 180, health = 325, healthMax = 325,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 15755, chance = 100}, -- snake helmet
			{id = 2154, chance = 50}, -- yellow gem
			{id = 14453, chance = 140}, -- wand
			{id = 14457, chance = 110}, -- spruce staff of destruction
			{id = 2168, chance = 320}, -- life ring
			{id = 2150, chance = 480}, -- small amethyst
			{id = 13706, chance = 2590}, -- beastly claw
			{id = 13155, chance = 10700}, -- untreated leather
			{id = 5809, chance = 3190}, -- magical element
			{id = 5881, chance = 800}, -- lizard scale
			{id = 7620, chance = 800}, -- mana potion
			{id = 17941, chance = 900}, -- radiating powder
			{id = 13152, chance = 800}, -- lizard tail
			{id = 5876, chance = 1280}, -- lizard leather
			{id = 2177, chance = 1340}, -- life crystal
			{id = 13585, chance = 330}, -- a foul rod
			{id = 14765, chance = 8510}, -- cape
			{id = 2818, chance = 28840}, -- dead snake
			{id = 2148, count = 11, chance = 84270}, -- gold
		}
	},
	{ name = 'Lizard Templar', level = 30, file = 'Cirith/Lizards/lizard templar.xml', look = {type = 113}, classId = 9, killAmount = 500, charmPoints = 15,
		description = {'Jaszczurzy Templariusz', 'Lizard Templar'}, experience = 215, health = 410, healthMax = 410,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 15784, chance = 120}, -- copper shield
			{id = 2149, chance = 260}, -- small emerald
			{id = 7618, chance = 730}, -- health potion
			{id = {13055, 13070, 13058, 13061}, chance = 150}, -- bronze crafting additives
			{id = 5876, chance = 920}, -- lizard leather
			{id = 5881, chance = 1010}, -- lizard scale
			{id = 13706, chance = 1490}, -- beastly claw
			{id = 14348, chance = 720}, -- iron armor
			{id = 14334, chance = 1960}, -- heavy axe
			{id = 5878, chance = 410}, -- hardener leather
			{id = 5892, chance = 650}, -- steel
			{id = 14478, chance = 450}, -- berynit helmet
			{id = 13155, chance = 9900}, -- untreated leather
			{id = 13152, chance = 560}, -- lizard tail
			{id = 15793, chance = 4290}, -- sword
			{id = 15776, chance = 960}, -- short sword
			{id = 2148, count = 4, chance = 83750}, -- gold
		}
	},
	{ name = 'Lizard Zaogun', level = 80, file = 'Cirith/Lizards/lizard zaogun.xml', look = {type = 343}, classId = 9, killAmount = 1000, charmPoints = 25,
		description = {'Jaszczurzy Lider', 'Lizard Leader'}, experience = 2200, health = 2955, healthMax = 2955,
		boss = 'The Chosen One', chance = 100,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'death', value = 10 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 16157, chance = 250}, -- black steel armor
			{id = 16156, chance = 120}, -- black steel legs
			{id = 14319, chance = 180}, -- tower shield
			{id = 16155, chance = 350}, -- black steel shoes
			{id = 7588, chance = 120}, -- SHP
			{id = 13151, chance = 1000}, -- lizard tail
			{id = 2149, count = 3, chance = 4740}, -- small emerald
			{id = 7591, count = 3, chance = 740}, -- GHP
			{id = 11324, chance = 8120}, -- zaogun's flag
			{id = 5881, chance = 12370}, -- lizard scale
			{id = 5876, chance = 14650}, -- lizard leather
			{id = 11325, chance = 15190}, -- zaogun shoulderplates
			{id = 2152, chance = 49750}, -- silver coin
			{id = 2148, count = 21, chance = 94190}, -- gold
		}
	},
	{ name = 'Primeval', level = 95, file = 'Cirith/Lizards/primeval.xml', look = {type = 345}, classId = 9, killAmount = 1000, charmPoints = 25,
		description = {'Przedwieczny', 'Primeval'}, experience = 2400, health = 3330, healthMax = 3330,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = 85 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = {13055, 13070, 13058, 13061}, chance = 210}, -- bronze crafting additives
			{id = 15747, chance = 150}, -- dragon axe
			{id = 14175, chance = 21420}, -- coal
			{id = 15721, chance = 150}, -- ragged blade
			{id = 11333, chance = 700}, --clay lump
			{id = 11221, chance = 790}, -- shiny stone
			{id = 14319, chance = 140}, -- royal steel shield
			{id = 5880, chance = 1650}, -- iron ore
			{id = 15734, chance = 100}, -- vile axe
			{id = 11319, chance = 10050}, -- spiked iron ball
			{id = 10548, chance = 20210}, -- ancient stone
			{id = 1294, count = 10, chance = 30220}, -- small stone
			{id = 2152, chance = 96100}, -- silver
			{id = 2148, count = 39, chance = 99940}, -- gold
		}
	},
	
	-- Minotaurs
	{ name = 'Minotaur Archer', level = 20, file = 'Cirith/Minotaurs/minotaur archer.xml', look = {type = 410}, classId = 16, killAmount = 500, charmPoints = 15,
		description = {'Minotaur Kusznik', 'Minotaur Archer'}, experience = 70, health = 100, healthMax = 100,
		boss = 'Wounded Moothan', chance = 60,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 19256, chance = 300}, -- leather quiver
			{id = 11419, chance = 1}, -- plushy minotaur
			{id = 15753, chance = 720}, -- brass boots
			{id = {15752, 15751}, chance = 1310}, -- brass legs/armor
			{id = 15750, chance = 650}, -- brass helmet
			{id = 13942, chance = 10}, -- repeating crossbow
			{id = 13706, chance = 1580}, -- beastly claw
			{id = 5878, chance = 690}, -- hardener leather
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 1090}, -- copper set
			{id = 2455, chance = 2750}, -- crossbow
			{id = 13738, chance = 21780}, -- broken crossbow
			{id = 13745, chance = 8780}, -- naramiennik
			{id = 2666, chance = 17520}, -- meat
			{id = 19219, chance = 1000}, -- quiver
			{id = 13155, chance = 6390}, -- untreated leather
			{id = 7363, count = 5, chance = 3290}, -- piercing bolt
			{id = 11438, count = 7, chance = 14190}, -- bolt
			{id = 2148, count = {9, 16}, chance = 29870}, -- coin
		}
	},
	{ name = 'Minotaur Guard', level = 30, file = 'Cirith/Minotaurs/minotaur guard.xml', look = {type = 29}, classId = 16, killAmount = 500, charmPoints = 15,
		description = {'Minotaur Wojownik', 'Minotaur Guard'}, experience = 110, health = 185, healthMax = 185,
		boss = 'Wounded Moothan', chance = 80,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 11419, chance = 1}, -- plushy minotaur
			{id = 7401, chance = 80}, -- mino trophy
			{id = 16398, chance = 250}, -- crystal teleport
			{id = 7618, chance = 390}, -- health potion
			{id = 14358, chance = 310}, -- bronze axe
			{id = 13706, count = 2, chance = 1370}, -- beastly claw
			{id = {13055, 13070, 13058, 13061}, chance = 220}, -- bronze crafting additives
			{id = 2580, chance = 470}, -- fishing rod
			{id = 5878, chance = 540}, -- hardener leather
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 1990}, -- copper set
			{id = 13760, chance = 9010}, -- naramiennik
			{id = 2148, count = {12, 22}, chance = 59500}, -- coin
		}
	},
	{ name = 'Minotaur Mage', level = 35, file = 'Cirith/Minotaurs/minotaur mage.xml', look = {type = 409}, classId = 16, killAmount = 500, charmPoints = 15,
		description = {'Minotaur Mag', 'Minotaur Mage'}, experience = 90, health = 155, healthMax = 155,
		boss = 'Wounded Moothan', chance = 80,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 35 },
			{ type = 'earth', value = 5 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 11419, chance = 1}, -- plushy minotaur
			{id = {13906, 13907, 13908, 13909, 13911, 13910}, chance = 10}, -- Wradon's set
			{id = 15742, chance = 340}, -- sapphire ring
			{id = 7620, chance = 440}, -- mana potion
			{id = 11694, chance = 610}, -- fine fabric
			{id = 5809, chance = 4690}, -- magical element
			{id = 14457, chance = 50}, -- spruce staff of destruction
			{id = 16398, chance = 250}, -- crystal teleport
			{id = 5878, chance = 450}, -- hardener leather
			{id = 2050, chance = 4970}, -- torch
			{id = 2684, count = 8, chance = 15110}, -- carrot
			{id = 2148, count = {14, 28}, chance = 86480}, -- coin
		}
	},
	{ name = 'Minotaur', level = 16, file = 'Cirith/Minotaurs/minotaur.xml', look = {type = 25}, classId = 16, killAmount = 500, charmPoints = 15,
		description = {'Minotaur', 'Minotaur'}, experience = 70, health = 100, healthMax = 100,
		boss = 'Wounded Moothan', chance = 50,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -15 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -15 },
		},
		loot = {
			{id = 11419, chance = 1}, -- plushy minotaur
			{id = 15753, chance = 350}, -- brass boots
			{id = {15752, 15751}, chance = 530}, -- brass legs/armor
			{id = 15750, chance = 450}, -- brass helmet
			{id = 14333, chance = 310}, -- runed sword
			{id = 13706, chance = 1940}, -- beastly claw
			{id = 7618, chance = 600}, -- health potion
			{id = 15792, chance = 1320}, -- tri-spoke star
			{id = 5878, chance = 430}, -- hardener leather
			{id = 2554, chance = 7580}, -- shovel
			{id = 2050, chance = 3980}, -- torch
			{id = 13760, chance = 10780}, -- naramiennik
			{id = 13155, chance = 4910}, -- untreated leather
			{id = 15793, chance = 2190}, -- sword
			{id = 2148, count = {8, 16}, chance = 49600}, -- coin
		}
	},
	{ name = 'Royal Minotaur Hunter', level = 60, file = 'Cirith/Minotaurs/royal minotaur hunter.xml', look = {type = 415}, classId = 16, killAmount = 1000, charmPoints = 25,
		description = {'Minotaur Kr�lewski Oszczepnik', 'Royal Minotaur Javelin'}, experience = 990, health = 1400, healthMax = 1400,
		boss = 'Mar\'Tal The Leader', chance = 90,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2156, chance = 50}, -- red gem
			{id = 2154, chance = 50}, -- yellow gem
			{id = 2195, chance = 20}, -- boh
			{id = 14010, chance = 40}, -- knight armor
			{id = 7401, chance = 170}, -- mino trophy
			{id = 15741, chance = 90}, -- reflective bow
			{id = 5911, chance = 780}, -- red poc
			{id = 5910, chance = 1010}, -- green poc
			{id = 5912, chance = 1050}, -- blue poc
			{id = 7589, chance = 1270}, -- SMP
			{id = 13949, chance = 90}, -- golden spear
			{id = 7588, count = 2, chance = 850}, -- SHP
			{id = 2150, count = 3, chance = 2450}, -- small amethyst
			{id = 2147, count = 3, chance = 2620}, -- small ruby
			{id = 5878, chance = 1490}, -- mino leather
			{id = 5944, chance = 1040}, -- soul orb
			{id = 13952, chance = 120}, -- crystal spear
			{id = 13948, chance = 800}, -- hunting spear
			{id = 2152, chance = 4990}, -- silver
			{id = 2148, count = {29, 51}, chance = 89460}, -- coin
		}
	},
	{ name = 'Royal Minotaur Mage', level = 70, file = 'Cirith/Minotaurs/royal minotaur mage.xml', look = {type = 416}, classId = 16, killAmount = 1000, charmPoints = 25,
		description = {'Minotaur Kr�lewski Mag', 'Royal Minotaur Mage'}, experience = 730, health = 1100, healthMax = 1100,
		boss = 'Mar\'Tal The Leader', chance = 90,
		elements = {
			{ type = 'death', value = 5 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = 5 },
		},
		loot = {
			{id = 14009, chance = 100}, -- sparkling cape
			{id = 2156, chance = 90}, -- red gem
			{id = 2154, chance = 10}, -- yellow gem
			{id = 2195, chance = 210}, -- boh
			{id = 16398, chance = 150}, -- crystal teleport
			{id = 7401, chance = 140}, -- mino trophy
			{id = 14450, chance = 25}, -- staff of powerful energy
			{id = 14006, chance = 190}, -- cape of fear
			{id = 7589, chance = 1180}, -- SMP
			{id = 5911, chance = 1440}, -- red poc
			{id = 5910, chance = 1440}, -- green poc
			{id = 14455, chance = 330}, -- wand
			{id = 5912, chance = 1610}, -- blue poc
			{id = {2146, 2149, 2147, 9970, 2150, 2145}, chance = 2550}, -- small
			{id = 5944, chance = 1250}, -- soul orb
			{id = 5878, chance = 1420}, -- mino leather
			{id = 2050, chance = 5030}, -- torch
			{id = 15715, chance = 5040}, -- cranial basher
			{id = 14453, chance = 910}, -- wand
			{id = 14336, chance = 7600}, -- hood
			{id = 2152, chance = 3200}, -- silver
			{id = 2148, count = {27, 50}, chance = 89000}, -- coin
		}
	},
	{ name = 'Royal Minotaur Warrior', level = 75, file = 'Cirith/Minotaurs/royal minotaur warrior.xml', look = {type = 444}, classId = 16, killAmount = 2500, charmPoints = 50,
		description = {'Minotaur Kr�lewski Wojownik', 'Royal Minotaur Warrior'}, experience = 2400, health = 3200, healthMax = 3200,
		boss = 'Hammer', chance = 80,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'death', value = 1 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 12955, chance = 100}, -- Marksman armor
			{id = 13931, chance = 80}, -- Helmet of invaders
			{id = 2154, chance = 70}, -- yellow gem
			{id = 2156, chance = 80}, -- red gem
			{id = 2195, chance = 210}, -- boh
			{id = 14352, chance = 250}, -- iron blade
			{id = 9971, chance = 320}, -- gold ingot
			{id = 14318, chance = 10}, -- royal steel blade
			{id = 7401, chance = 220}, -- mino trophy
			{id = 15727, chance = 10}, -- minotaur hammer
			{id = 14443, chance = 30}, -- spiky shield
			{id = {13055, 13070, 13058, 13061}, chance = 240}, -- bronze crafting additives
			{id = 2171, chance = 990}, -- amulet
			{id = 15717, chance = 220}, -- glorious axe
			{id = 7590, chance = 1330}, -- GMP
			{id = 7591, chance = 1380}, -- GHP
			{id = 5911, chance = 2330}, -- red poc
			{id = 2147, count = 2, chance = 2550}, -- small ruby
			{id = 2145, count = 2, chance = 2660}, -- small diamond
			{id = 5878, chance = 1430}, -- mino leather
			{id = 2050, chance = 5030}, -- torch
			{id = 2666, chance = 6660}, -- meat
			{id = 2152, chance = 11830}, -- silver
			{id = 2148, count = {41, 78}, chance = 100000}, -- coin
		}
	},
	{ name = 'Royal Minotaur', level = 60, file = 'Cirith/Minotaurs/royal minotaur.xml', look = {type = 463}, classId = 16, killAmount = 1000, charmPoints = 25,
		description = {'Minotaur Kr�lewski', 'Royal Minotaur'}, experience = 790, health = 1200, healthMax = 1200,
		boss = 'Mar\'Tal The Leader', chance = 90,
		elements = {
			{ type = 'physical', value = 1 },
			{ type = 'death', value = 15 },
			{ type = 'energy', value = 3 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 7401, chance = 130}, -- mino trophy
			{id = 14010, chance = 320}, -- knight armor
			{id = 14321, chance = 110}, -- royal steel legs
			{id = 5911, chance = 540}, -- red poc
			{id = 14319, chance = 800}, -- steel shield
			{id = 14171, chance = 790}, -- berynit armor
			{id = 2149, chance = 540}, -- iron sword
			{id = 7589, chance = 170}, -- SMP
			{id = 7588, chance = 150}, -- SHP
			{id = 2376, chance = 650}, -- runed blade
			{id = 14432, chance = 150}, -- cobalt helmet
			{id = 9970, chance = 2030}, -- small topaz
			{id = 14352, chance = 500}, -- iron blade
			{id = 2147, chance = 2080}, -- small ruby
			{id = 2150, chance = 2120}, -- small amethyst
			{id = 5878, chance = 1180}, -- mino leather
			{id = 2149, chance = 2190}, -- small emerald
			{id = 2152, chance = 3990}, -- silver
			{id = 2148, count = {18, 47}, chance = 89890}, -- coin
		}
	},
	
	-- Mutateds
	{ name = 'Infected Prisoner', level = 32, file = 'Cirith/Mutated/infected prisoner.xml', look = {type = 323}, classId = 12, killAmount = 500, charmPoints = 15,
		description = {'Zara�ony Wi�zie�', 'Infected Prisoner'}, experience = 205, health = 340, healthMax = 340,
		boss = 'X\'men\'us', chance = 70,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 15776, chance = 300}, -- killer's blade
			{id = 2801, chance = 510}, -- fern
			{id = 14455, chance = 50}, -- the creeper of darkness
			{id = 5878, chance = 520}, -- hardener leather
			{id = 7910, chance = 600}, -- peanut
			{id = 2234, chance = 990}, -- moldy cheese
			{id = 13754, count = 2, chance = 8760}, -- rope belt
			{id = 2565, chance = 1130}, -- spoon
			{id = 14016, chance = 45}, -- holy shield
			{id = 13706, count = 4, chance = 1900}, -- beastly claw
			{id = 11404, chance = 80}, -- heavy crossbow
			{id = 5889, chance = 1200}, --piece of steel
			{id = 2553, chance = 1500}, -- pick
			{id = 2554, chance = 5190}, -- shovel
			{id = 17223, chance = 10}, -- claw
			{id = 3976, count = 2, chance = 5780}, --worms
			{id = 8368, chance = 8110}, -- cheese
			{id = 2226, chance = 10620}, -- fishbone
			{id = 11219, chance = 20160}, -- mutated flesh
			{id = 2148, count = {12, 21}, chance = 74980}, -- gold
		}
	},
	--  { name = 'Mutated Bat', level = 1, file = 'Cirith/Mutated/mutated bat.xml',}, ,
	{ name = 'Mutated Tiger', level = 50, file = 'Cirith/Mutated/mutated tiger.xml', look = {type = 318}, classId = 12, killAmount = 1000, charmPoints = 25,
		description = {'Zmutowany Tygrys', 'Mutated Tiger'}, experience = 1000, health = 1350, healthMax = 1350,
		boss = 'X\'men\'us', chance = 90,
		elements = {
			{ type = 'death', value = 65 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = -15 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 17223, chance = 220}, -- claw
			{id = 2168, chance = 5000}, -- life ring
			{id = 6534, chance = 40}, -- flying trident
			{id = 2226, chance = 10620}, -- fishbone
			{id = 11404, chance = 130}, -- heavy crossbow
			{id = 11222, chance = 11140}, -- sabretooth
			{id = 7588, count = 2, chance = 600}, -- health potion
			{id = 11204, chance = 20750}, -- striped fur
			{id = 2671, count = 2, chance = 28600}, -- ham
			{id = 2666, count = 3, chance = 35780}, -- meat
			{id = 2148, count = 23, chance = 12750}, -- gold
		}
	},
	
	-- Necromancers
	{ name = 'Fallen Island Warlock', level = 120, file = 'Cirith/Necromancers/fallen island warlock.xml', look = {type = 130, head = 78, body = 76, legs = 109, feet = 32, addons = 2}, classId = 29, killAmount = 2500, charmPoints = 50,
		description = {'Upad�y Czarnoksi�nik', 'Fallen Island Warlock'}, experience = 9500, health = 10100, healthMax = 10100,
		boss = 'Ner\'zhul', chance = 120,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 95 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 13895, chance = 210}, -- curseed blade
			{id = 2123, chance = 30}, -- rots
			{id = 15728, chance = 160}, -- Skeleton striking face
			{id = 8902, chance = 390}, -- sb of mind control
			{id = 16207, chance = 210}, -- meteorite armor
			{id = 16138, chance = 140}, -- golden armor
			{id = 2197, chance = 480, subType = 10}, -- ssa
			{id = 13989, chance = 10}, -- darkness coat
			{id = 13992, chance = 20}, -- bronze armor
			{id = {13945, 13947}, chance = 400}, -- ashbringer hood/robe
			{id = 16143, chance = 310}, -- golden blade
			{id = 2195, chance = 370}, -- boh
			{id = 16155, chance = 700}, -- black steel boots
			{id = 982, chance = 10}, -- dark talon
			{id = 16156, chance = 320}, -- black steel legs
			{id = 16146, chance = 20}, -- robe of the fallen
			{id = 13896, chance = 1690}, -- giant sword
			{id = 2178, chance = 1970}, -- mind stone
			{id = 2656, chance = 410}, -- blue robe
			{id = 2792, chance = 2610}, -- dark mushroom
			{id = 14001, chance = 350}, -- dark armor
			{id = 2153, chance = 44}, -- violet gem
			{id = 2177, chance = 6870}, -- life crystal
			{id = 12964, chance = 5400}, -- clear lunar crystal
			{id = 18338, chance = 200}, -- red gem
			{id = 14448, chance = 1164}, -- wand of voodoo
			{id = 14449, chance = 2980}, -- wand of dragonbreath
			{id = 8900, chance = 1000}, -- sb of enlightnment
			{id = 9813, chance = 2750}, -- rusty legs
			{id = 13886, chance = 590}, -- haunted blade
			{id = 8472, count = 3, chance = 790}, -- GMP
			{id = 8473, count = 3, chance = 800}, -- GHP
			{id = 2149, count = 8, chance = 9760}, -- small emerald
			{id = 14006, chance = 870}, -- cape of fear
			{id = 2150, count = 5, chance = 11860}, -- small amethyst
			{id = 2678, count = 2, chance = 12190}, -- cocount
			{id = 2152, count = 2, chance = 63150}, -- silver coin
			{id = 2148, count = 53, chance = 97900}, -- coin
		}
	},
	{ name = 'Necromancer', level = 50, file = 'Cirith/Necromancers/necromancer.xml', look = {type = 9}, classId = 29, killAmount = 1000, charmPoints = 25,
		description = {'Nekromanta', 'Necromancer'}, experience = 380, health = 580, healthMax = 580,
		boss = 'Forbidden Hunger Mage', chance = 80,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 14006, chance = 220}, -- cape of fear
			{id = 13989, chance = 110}, -- robe of the underworld
			{id = 2123, chance = 120}, -- diamond ring
			{id = 7589, chance = 320}, -- SMP
			{id = 11694, chance = 1230}, -- fine fabric
			{id = {13906, 13907, 13908, 13909, 13911}, chance = 290}, -- Wradon's set
			{id = 15738, chance = 660}, -- long bow
			{id = 5925, count = 2, chance = 6120}, -- hardened bone
			{id = 5809, chance = 2990}, -- magical element
			{id = 17161, chance = 160}, -- death enchanted in the staff
			{id = 15739, chance = 125}, -- necrotic bow
			{id = 17223, chance = 120}, -- claw
			{id = 2796, chance = 1460}, -- green mushroom
			{id = 11231, chance = 10120}, -- book of necromantic rituals
			{id = 2148, count = 11, chance = 30140}, -- gold
		}
	},
	{ name = 'Priestess', level = 52, file = 'Cirith/Necromancers/priestess.xml', look = {type = 58}, classId = 29, killAmount = 1000, charmPoints = 25,
		description = {'Zbuntowana', 'Priestess'}, experience = 220, health = 390, healthMax = 390,
		boss = 'Forbidden Hunger Mage', chance = 80,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 1 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 40 },
		},
		loot = {
			{id = 2114, chance = 70}, -- piggy bank
			{id = 2521, chance = 190}, -- dark shield
			{id = 2125, chance = 660}, -- crystal necklace
			{id = 2151, chance = 740}, -- talon
			{id = 11694, chance = 900}, -- fine fabric
			{id = 7620, chance = 900}, -- mana potion
			{id = 5809, chance = 1000}, -- magical element
			{id = 1976, chance = 940}, -- book
			{id = 12964, chance = 540}, -- clear lunar crystal
			{id = 2192, chance = 1170}, -- crystal ball
			{id = 2070, chance = 1450}, -- wooden flute
			{id = 14358, chance = 770}, -- bronze axe
			{id = 10555, chance = 1860}, -- cultish robe
			{id = 2791, chance = 3230}, -- wood mushroom
			{id = 14016, chance = 65}, -- holy shield
			{id = 10561, chance = 4980}, -- black hood
			{id = 2803, chance = 6160}, -- powder herb
			{id = 2674, count = 2, chance = 7560}, -- apple
			{id = 11214, chance = 9980}, -- dark rosary
			{id = 2760, chance = 11840}, -- goat grass
			{id = 2802, chance = 13360}, -- sling herb
			{id = 2148, count = 5, chance = 59720}, -- gold
		}
	},
	{ name = 'Trustee of Darkness', level = 66, file = 'Cirith/Necromancers/trustee of darkness.xml', look = {type = 492, addons = 2}, classId = 29, killAmount = 1000, charmPoints = 25,
		description = {'Powiernik Mroku', 'Trustee of Darkness'}, experience = 1250, health = 1800, healthMax = 1800,
		boss = 'Forbidden Hunger Mage', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = -5 },
		},
		loot = {
			{id = 18076, chance = 50}, -- grey dye
			{id = 14006, chance = 720}, -- cape of fear
			{id = 2153, chance = 20}, -- violet gem
			{id = 2154, chance = 70}, -- yellow gem
			{id = 16356, chance = 70}, -- container
			{id = 13592, chance = 590}, -- cursed spear
			{id = 2796, chance = 1460}, -- green mushroom
			{id = 18338, chance = 10}, -- red gem
			{id = 13989, chance = 190}, -- robe of the underworld
			{id = 15737, chance = 80}, -- bone bow
			{id = 2147, count = 3, chance = 2890}, -- small ruby
			{id = 17683, chance = 50}, -- cursed blade
			{id = 2150, count = 3, chance = 3250}, -- small amethyst
			{id = 2144, count = 3, chance = 5370}, -- black pearl
			{id = 11231, chance = 10120}, -- book of necromantic rituals
			{id = 7589, chance = 1510}, -- SMP
			{id = 2152, chance = 21250}, -- silver
			{id = 2148, count = 22, chance = 69890}, -- gold
		}
	},
	
	--  Orcs 
	{ name = 'Orc Berserker', level = 41, file = 'Cirith/Orcs/orc berserker.xml', look = {type = 8}, classId = 18, killAmount = 1000, charmPoints = 25,
		description = {'Ork Berserker', 'Orc Berserker'}, experience = 145, health = 210, healthMax = 210,
		boss = 'Nobbu', chance = 70,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = 2628, chance = 450}, -- The Armor of Hardened Leather
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 1590}, -- copper set
			{id = 2044, chance = 820}, -- lamp
			{id = 13757, chance = 4030}, -- orc leather
			{id = 5889, chance = 8900}, -- piece of steel
			{id = 2671, chance = 10040}, -- ham
			{id = 15730, chance = 10}, -- war axe
			{id = 2148, count = {14, 28}, chance = 72970}, -- coin
		}
	},
	{ name = 'Orc Leader', level = 45, file = 'Cirith/Orcs/orc leader.xml', look = {type = 59}, classId = 18, killAmount = 1000, charmPoints = 25,
		description = {'Ork Lider', 'Orc Leader'}, experience = 320, health = 450, healthMax = 450,
		boss = 'Nobbu', chance = 80,
		elements = {
			{ type = 'holy', value = 15 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = 14322, chance = 110}, -- royal steel helmet
			{id = 14327, chance = 30}, -- plate legs
			{id = 7618, chance = 580}, -- health potion
			{id = 15723, chance = 670}, -- broadsword
			{id = 14348, chance = 870}, -- iron armor
			{id = 5887, chance = 190}, -- piece of royal steel
			{id = 13758, chance = 2110}, -- skull belt
			{id = 15722, chance = 4190}, -- scimitar
			{id = 5892, chance = 1120}, -- steel
			{id = 15752, chance = 21470}, -- brass legs
			{id = 15782, chance = 1030}, -- steel spear
			{id = 5889, chance = 3580}, -- piece of steel
			{id = 15713, chance = 2990}, -- longsword
			{id = 14358, chance = 1500}, -- bronze axe
			{id = 2789, chance = 9820}, -- mushroom
			{id = 2410, count = 4, chance = 9980}, -- throwing knife
			{id = 12963, chance = 60140}, -- lampart shield
			{id = 13757, chance = 19680}, -- orc leather
			{id = 2148, count = {22, 46}, chance = 74010}, -- coin
			{id = 2152, count = 1, chance = 28900}, -- silver coin
			{id = 2667, chance = 30050}, -- fish
		}
	},
	{ name = 'Orc Rider', level = 30, file = 'Cirith/Orcs/orc rider.xml', look = {type = 4}, classId = 18, killAmount = 500, charmPoints = 15,
		description = {'Ork Je�dziec', 'Orc Rider'}, experience = 120, health = 180, healthMax = 180,
		boss = 'Nobbu', chance = 60,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = {15784, 15785, 15786, 15787, 15788}, chance = 1620}, -- copper set
			{id = 2050, chance = 990}, -- torch
			{id = 11229, chance = 9540}, -- wolf fur
			{id = 13754, chance = 3950}, -- rope belt
			{id = 5892, chance = 300}, -- steel
			{id = 13757, chance = 9850}, -- orc leather
			{id = 5880, chance = 1090}, -- iron ore
			{id = 5889, chance = 2590}, -- piece of steel
			{id = 13155, chance = 3120}, -- untreated leather
			{id = 2666, count = 3, chance = 24020}, -- meat
			{id = 2148, count = {12, 20}, chance = 47260}, -- coin
		}
	},
	{ name = 'Orc Shaman', level = 39, file = 'Cirith/Orcs/orc shaman.xml', look = {type = 6}, classId = 18, killAmount = 500, charmPoints = 15,
		description = {'Ork Szaman', 'Orc Shaman'}, experience = 310, health = 405, healthMax = 405,
		boss = 'Nobbu', chance = 60,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = 14006, chance = 150}, -- cape of fear
			{id = 14343, chance = 90}, -- alpha shanir
			{id = 16356, chance = 10}, -- bag
			{id = 1958, chance = 470}, -- grey book
			{id = 5809, chance = 2000}, -- magical element
			{id = 5889, chance = 3190}, -- piece of steel
			{id = 13757, chance = 4000}, -- orc leather
			{id = 13739, chance = 10200}, -- broken shamanic staff
			{id = 2686, count = 2, chance = 10980}, -- corncob
			{id = 2148, count = {6, 12}, chance = 89880}, -- coin
		}
	},
	{ name = 'Orc Spearman', level = 15, file = 'Cirith/Orcs/orc spearman.xml', look = {type = 50}, classId = 18, killAmount = 500, charmPoints = 15,
		description = {'Ork W��cznik', 'Orc Spearman'}, experience = 59, health = 105, healthMax = 105,
		boss = 'Nobbu', chance = 50,
		elements = {
			{ type = 'holy', value = 15 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 15782, chance = 50}, -- steel spear
			{id = 11414, chance = 1}, -- plushy orc
			{id = 14337, chance = 5880}, -- orc shield
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 2020}, -- lampart set
			{id = 13757, chance = 5860}, -- orc skin
			{id = 13754, chance = 4190}, -- rope belt
			{id = 19219, chance = 390}, -- quiver
			{id = 5880, chance = 720}, -- iron ore
			{id = 3976, chance = 9760}, -- worm
			{id = 14175, chance = 9650}, -- coal
			{id = 5889, chance = 1000}, -- piece of steel
			{id = 13155, chance = 3210}, -- untreated leather
			{id = 12962, chance = 970}, -- lampart spear
			{id = 2420, chance = 1890}, -- machete
			{id = 2666, chance = 10870}, -- meat
			{id = 2148, count = 6, chance = 65480}, -- coin
		}
	},
	{ name = 'Orc Warlord', level = 50, file = 'Cirith/Orcs/orc warlord.xml', look = {type = 2}, classId = 18, killAmount = 1000, charmPoints = 25,
		description = {'Ork Dygnitarz Wojenny', 'Orc Warlord'}, experience = 610, health = 960, healthMax = 960,
		boss = 'Nobbu', chance = 100,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 80 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = 5892, chance = 2010}, -- steel
			{id = 16205, chance = 10}, -- meteorite boots
			{id = 14166, chance = 20}, -- sallet
			{id = 7395, chance = 70}, -- orc trophy
			{id = 2440, chance = 190}, -- daramanian axe
			{id = 14004, chance = 70}, -- trapper boots
			{id = 5887, chance = 510}, -- royal steel
			{id = 7618, chance = 440}, -- health potion
			{id = 14362, chance = 230}, -- plate armor
			{id = 14349, chance = 430}, -- iron helmet
			{id = 5889, chance = 5990}, -- piece of steel
			{id = 15723, chance = 90}, -- two handed sword
			{id = 15754, chance = 2050, subType = 200}, -- brass necklace
			{id = 15722, chance = 1440}, -- scimitar
			{id = 14350, chance = 520}, -- iron legs
			{id = 13758, chance = 4900}, -- skull belt
			{id = 12962, chance = 970}, -- lampart spear
			{id = 14348, chance = 980}, -- iron armor
			{id = 15716, chance = 480}, -- knight axe
			{id = 2667, count = 2, chance = 10790}, -- fish
			{id = 2148, count = {31, 62}, chance = 54650}, -- coin
			{id = 13757, chance = 20370}, -- orc leather
			{id = 13727, chance = 25370}, -- broken helmet
		}
	},
	{ name = 'Orc Warrior', level = 18, file = 'Cirith/Orcs/orc warrior.xml', look = {type = 7}, classId = 18, killAmount = 500, charmPoints = 15,
		description = {'Ork Wojownik', 'Orc Warrior'}, experience = 65, health = 125, healthMax = 125,
		boss = 'Nobbu', chance = 60,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = 14337, chance = 8120}, -- orc shield
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 2410}, -- lampart set
			{id = 2379, chance = 120}, -- blade
			{id = 5892, chance = 310}, -- steel
			{id = 15776, chance = 10}, -- killer's blade
			{id = 5889, chance = 1690}, -- piece of steel
			{id = 13758, chance = 980}, -- skull belt
			{id = 13757, chance = 4030}, -- orc leather
			{id = 13754, chance = 4000}, -- rope belt
			{id = 14358, chance = 150}, -- bronze axe
			{id = 5880, chance = 990}, -- iron ore
			{id = 13155, chance = 2780}, -- untreated leather
			{id = 13727, chance = 9990}, -- broken helmet
			{id = 2666, chance = 15020}, -- meat
			{id = 2148, count = 8, chance = 65140}, -- gold
		}
	},
	{ name = 'Orc', level = 10, file = 'Cirith/Orcs/orc.xml', look = {type = 5}, classId = 18, killAmount = 500, charmPoints = 15,
		description = {'Ork', 'Orc'}, experience = 40, health = 70, healthMax = 70,
		boss = 'Nobbu', chance = 50,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 11414, chance = 1}, -- plushy orc
			{id = 14337, chance = 4200}, -- orc shield
			{id = 15778, chance = 6500}, -- wooden shield
			{id = 5880, chance = 500}, -- iron ore
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 1800}, -- lampart set
			{id = 13754, chance = 3190}, -- rope belt
			{id = 15783, chance = 1910}, -- breaker
			{id = 13757, chance = 7470}, -- orc skin
			{id = 5889, chance = 990}, -- piece of steel
			{id = 14175, chance = 3970}, -- coal
			{id = 13155, chance = 2870}, -- untreated leather
			{id = 2666, chance = 10870}, -- meat
			{id = 2148, count = 4, chance = 77690}, -- coin
		}
	},
	
	-- Pyro-Elementals
	{ name = 'Phoenix', level = 38, file = 'Cirith/Pyro-Elementals/Phoenix.xml', look = {type = 671}, classId = 24, killAmount = 2500, charmPoints = 50,
		description = {'Feniks', 'Phoenix'}, experience = 11300, health = 15900, healthMax = 15900,
		boss = 'Hellfire Warrior', chance = 150,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 2647, chance = 920}, -- red robe
			{id = 12955, chance = 250}, -- Marksman armor
			{id = 16207, chance = 170}, -- meteorite armor
			{id = 16204, chance = 480}, -- hellfire shield
			{id = 16173, chance = 150}, -- ruby helmet
			{id = 16171, chance = 130}, -- ruby legs
			{id = 13796, chance = 5290}, -- silver shield
			{id = 13307, chance = 1990}, -- magma spear
			{id = 2392, chance = 2900}, -- fire sword
			{id = 2365, chance = 50}, -- backpack of holding
			{id = 14442, chance = 1250, subType = 20}, -- penetration ring
			{id = 14033, chance = 900}, -- broken ring of ending
			{id = 8850, chance = 180}, -- chain bolter
			{id = 13939, chance = 390}, -- hellfire crossbow
			{id = 5944, chance = 7860}, -- Soul Orb
			{id = 2539, chance = 80}, -- phoenix shield
			{id = 16138, chance = 490}, -- golden armor
			{id = 2127, chance = 3100}, -- emerald bangle
			{id = 13165, chance = 260}, -- golden edge
			{id = 16140, chance = 240}, -- golden axe
			{id = 2187, chance = 900}, -- wand of Inferno
			{id = 5809, chance = 7890}, -- magical element
			{id = 8921, chance = 320}, -- Red dragon wand
			{id = 16315, chance = 10}, -- plushy phoenix
			{id = 11706, chance = 120}, -- phoenix egg
			{id = 13282, chance = 4000}, -- phoenix feather
			{id = 10552, chance = 9930}, -- fierly heart
			{id = 11436, count = {10, 20}, chance = 12430}, -- crystalline bolt
			{id = 2147, count = 5, chance = 10700}, -- small ruby
			{id = 2151, count = 2, chance = 7400}, -- talon
			{id = 2239, chance = 49790}, -- Burnt Scroll
			{id = 2160, chance = 2100}, -- gold coin
			{id = 2152, count = {2, 9}, chance = 91020}, -- silver coin
			{id = 2148, count = {20, 50}, chance = 94760}, -- gold
		}
	},
	{ name = 'Blazing Fire Elemental', level = 60, file = 'Cirith/Pyro-Elementals/blazing fire elemental.xml', look = {type = 49}, classId = 24, killAmount = 1000, charmPoints = 25,
		description = {'Ra��cy �ywio�ak Ognia', 'Blazing Fire Elemental'}, experience = 650, health = 1000, healthMax = 1000,
		boss = 'Hellfire Warrior', chance = 80,
		elements = {
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 40 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
		},
		loot = {
			{id = 2000, chance = 50}, -- red backpack
			{id = 2656, chance = 10}, -- blue robe
			{id = 8299, chance = 1500}, -- glimmering soil
			{id = 2147, count = 2, chance = 1450}, -- small ruby
			{id = 10552, chance = 7600}, -- fierly heart
			{id = 7840, count = 3, chance = 7220}, -- flaming arrow
			{id = 2152, chance = 4680}, -- silver
			{id = 2239, chance = 24140}, -- burnt scroll
			{id = 2148, count = 22, chance = 99980}, -- gold
		}
	},
	{ name = 'Blistering Fire Elemental', level = 60, file = 'Cirith/Pyro-Elementals/blistering fire elemental.xml', look = {type = 242}, classId = 24, killAmount = 1000, charmPoints = 25,
		description = {'Parz�cy �ywio�ak Ognia', 'Blistering Fire Elemental'}, experience = 1400, health = 2500, healthMax = 2500,
		boss = 'Hellfire Warrior', chance = 100,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 40 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
		},
		loot = {
			{id = 2539, chance = 10}, -- phoenix shield
			{id = 2000, chance = 20}, -- red backpack
			{id = 13939, chance = 150}, -- fiery crossbow
			{id = 8902, chance = 250}, -- SB of mind control
			{id = 2392, chance = 30}, -- fire sword
			{id = 2656, chance = 50}, -- blue robe
			{id = 8299, chance = 2400}, -- glimmering soil
			{id = 2147, count = 2, chance = 1450}, -- small ruby
			{id = 14449, chance = 2000}, -- wand of draconia
			{id = 10552, chance = 11000}, -- fierly heart
			{id = 7840, count = 3, chance = 7220}, -- flaming arrow
			{id = 2152, chance = 14680}, -- silver
			{id = 2239, chance = 24140}, -- burnt scroll
			{id = 2148, count = 15, chance = 99980}, -- gold
		}
	},
	{ name = 'Fire Elemental', level = 38, file = 'Cirith/Pyro-Elementals/fire elemental.xml', look = {type = 49}, classId = 24, killAmount = 1000, charmPoints = 25,
		description = {'�ywio�ak Ognia', 'Fire Elemental'}, experience = 170, health = 280, healthMax = 280,
		boss = 'Hellfire Warrior', chance = 80,
		elements = {
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13588, chance = 10}, -- fire axe
			{id = 13981, chance = 200, subType = 120}, -- magma amulet
			{id = 14447, chance = 190}, -- woi
			{id = 13951, chance = 50}, -- spear
			{id = 5809, chance = 1890}, -- magical element
			{id = 2147, chance = 1900}, -- small ruby
			{id = 2148, count = 9, chance = 34760}, -- gold
		}
	},
	{ name = 'Flicker of Death', level = 80, file = 'Cirith/Pyro-Elementals/flicker of death.xml', look = {type = 49}, classId = 24, killAmount = 2500, charmPoints = 50,
		description = {'Migotanie �mierci', 'Flicker of Death'}, experience = 1150, health = 2100, healthMax = 2100,
		boss = 'Hellfire Warrior', chance = 130,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'death', value = 40 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 50 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13893, chance = 10}, -- fire axe
			{id = 2156, chance = 40}, -- red gem
			{id = 13995, chance = 90}, -- lava armor
			{id = 8900, chance = 430}, -- sb enlight~
			{id = 8902, chance = 470}, -- sb of mind control
			{id = 12967, chance = 670}, -- fiery lunar crystal (implosion)
			{id = 10218, chance = 840, subType = 10}, -- hellfire lavos amulet
			{id = 2656, chance = 150}, -- blue robe
			{id = 9810, chance = 1210}, -- rusty armor
			{id = 14449, chance = 1300}, -- wand of draconia
			{id = 2655, chance = 130}, -- red robe
			{id = 2147, count = 3, chance = 3210}, -- small ruby
			{id = 7840, count = 12, chance = 12310}, -- flaming arrow
			{id = 2238, chance = 21310}, -- worn boots
			{id = 2239, chance = 24140}, -- burnt scroll
			{id = 2152, chance = 14680}, -- silver
			{id = 2148, count = 10, chance = 99980}, -- gold
		}
	},
	{ name = 'Hellfire Fighter', level = 100, file = 'Cirith/Pyro-Elementals/hellfire fighter.xml', look = {type = 243}, classId = 24, killAmount = 2500, charmPoints = 50,
		description = {'Piekielny Wojownik', 'Hellfire Fighter'}, experience = 2500, health = 3800, healthMax = 3800,
		boss = 'Hellfire Warrior', chance = 140,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2136, chance = 260}, -- Demonbone Amulet
			{id = 13588, chance = 110}, -- Fire Axe
			{id = 13995, chance = 210}, -- lavos armor
			{id = 14175, chance = 24540}, -- coal
			{id = 13939, chance = 190}, -- hellfire crossbow
			{id = 13951, chance = 510}, -- hellfire spear
			{id = 2152, chance = 1020}, -- silver coin
			{id = 2145, chance = 1370}, -- Small Diamond
			{id = 8931, chance = 210}, -- hellfire blade
			{id = 2127, chance = 2100}, -- emerald bangle
			{id = 2392, chance = 630}, -- Fire Sword
			{id = 10580, chance = 4920}, -- piece of hellfire armor
			{id = 2187, chance = 900}, -- wand of Inferno
			{id = 11679, chance = 120}, -- hellfire bow
			{id = 10552, chance = 9930}, -- fierly heart
			{id = 5944, chance = 1860}, -- Soul Orb
			{id = 6500, chance = 1990}, -- demonic essence
			{id = 2239, chance = 49790}, -- Burnt Scroll
			{id = 2148, count = 35, chance = 92510}, -- Gold Coin
		}
	},
	{ name = 'Massive Fire Elemental', level = 75, file = 'Cirith/Pyro-Elementals/massive fire elemental.xml', look = {type = 242}, classId = 24, killAmount = 1000, charmPoints = 25,
		description = {'Wi�kszy �ywio�ak Ognia', 'Massive Fire Elemental'}, experience = 650, health = 1200, healthMax = 1200,
		boss = 'Hellfire Warrior', chance = 120,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'death', value = 20 },
			{ type = 'energy', value = 30 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -15 },
		},
		loot = {
			{id = 13995, chance = 120}, -- lava armor
			{id = 2392, chance = 330}, -- fire sword
			{id = 13893, chance = 80}, -- fire axe
			{id = 13981, chance = 1210, subType = 100}, -- magma amulet
			{id = 14447, chance = 320}, -- woi
			{id = 8931, chance = 90}, -- hellfire blade
			{id = 13951, chance = 110}, -- spear
			{id = 2147, count = 2, chance = 6130}, -- small ruby
			{id = 2152, chance = 14680}, -- silver
			{id = 2148, count = 19, chance = 99980}, -- gold
		}
	},
	{ name = 'Spark of the Phoenix', level = 100, file = 'Cirith/Pyro-Elementals/spark of the phoenix.xml', look = {type = 242}, classId = 24, killAmount = 2500, charmPoints = 50,
		description = {'Iskra Feniksa', 'Spark of the Phoenix'}, experience = 2100, health = 3900, healthMax = 3900,
		boss = 'Hellfire Warrior', chance = 140,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 35 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 2539, chance = 10}, -- phoenix shield
			{id = 16207, chance = 20}, -- meteorite armor
			{id = 16156, chance = 50}, -- black steel legs
			{id = 13994, chance = 10}, -- magma armor
			{id = 16200, chance = 30}, -- hellfire boots
			{id = 13893, chance = 10}, -- fire axe
			{id = 8902, chance = 10}, -- SB of mind control
			{id = 2656, chance = 130}, -- blue robe
			{id = 13896, chance = 1120}, -- giant sword
			{id = 13939, chance = 140}, -- fiery crossbow
			{id = 9810, chance = 2130}, -- rusty armor
			{id = 2392, chance = 170}, -- fire sword
			{id = 2147, count = 2, chance = 6130}, -- small ruby
			{id = 14449, chance = 650}, -- wand of draconia
			{id = 2152, chance = 14680}, -- silver
			{id = 7840, count = 16, chance = 21420}, -- flaming arrow
			{id = 2148, count = 40, chance = 85100}, -- gold
		}
	},
	
	-- Serpents
	{ name = 'Cobra', level = 6, file = 'Cirith/Serpents/cobra.xml', look = {type = 81}, classId = 7, killAmount = 250, charmPoints = 5,
		description = {'Kobra', 'Cobra'}, experience = 30, health = 65, healthMax = 65,
		boss = 'Snaker', chance = 90,
		elements = {
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 10550, chance = 4830}, -- cobra tongue
		}
	},
	{ name = 'Hydra', level = 55, file = 'Cirith/Serpents/hydra.xml', look = {type = 633, addons = 7}, classId = 7, killAmount = 1000, charmPoints = 25,
		description = {'Hydra', 'Hydra'}, experience = 1600, health = 2350, healthMax = 2350,
		boss = 'Steno', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'ice', value = 40 },
		},
		loot = {
			{id = 15757, chance = 210}, -- snake legs
			{id = 2195, chance = 310}, -- boots of haste
			{id = 15756, chance = 180}, -- snake armor
			{id = 14480, chance = 160}, -- spartan helmet
			{id = 13585, chance = 400}, -- a foul rod
			{id = 11694, chance = 1550}, -- fine fabric
			{id = 5878, chance = 2430}, -- hardener leather
			{id = 8903, chance = 210}, -- spellbook
			{id = 15755, chance = 320}, -- snake helmet
			{id = 7589, chance = 460}, -- SMP
			{id = 2177, chance = 1540}, -- life crystal
			{id = 15758, chance = 300}, -- snake boots
			{id = 14322, chance = 350}, -- royal steel helmet
			{id = 2197, chance = 880, subType = 10}, -- stone skin amulet
			{id = 4850, chance = 950}, -- hydra egg
			{id = 14010, chance = 610}, -- knight armor
			{id = 2214, chance = 1170}, -- RoH
			{id = 8842, chance = 4740}, -- cucumber
			{id = 2146, chance = 5050}, -- small sapphire
			{id = 11193, chance = 10080}, -- hydra head
			{id = 2152, count = {1, 2}, chance = 26570}, -- silver coin
			{id = 2671, count = 4, chance = 60250}, -- ham
			{id = 2148, count = {54, 86}, chance = 87710}, -- coin
		}
	},
	{ name = 'Medusa', level = 70, file = 'Cirith/Serpents/medusa.xml', look = {type = 330}, classId = 7, killAmount = 2500, charmPoints = 50,
		description = {'Gorgona', 'Gorgone'}, experience = 3350, health = 4500, healthMax = 4500,
		boss = 'Steno', chance = 100,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = {16144, 12953, 15746}, chance = 100}, -- underworld boots/marksman boots/dragon boots
			{id = 13940, chance = 230}, -- terra crossbow
			{id = 15757, chance = 260}, -- terra legs
			{id = 2144, chance = 4490}, -- black pearl
			{id = 2536, chance = 260}, -- medusa shield
			{id = 10219, chance = 820, subType = 5}, -- sacred tree amulet
			{id = 15756, chance = 360}, -- snake armor
			{id = 13979, chance = 400}, -- Crusher
			{id = 16909, chance = 10}, -- sickle of the wind
			{id = 17726, chance = 140}, -- plague spear
			{id = 5809, chance = 4510}, -- magical element
			{id = 14010, chance = 570}, -- knight armor
			{id = 2149, count = 4, chance = 3780}, -- small emerald
			{id = 13980, chance = 4090, subType = 90}, -- terra amulet
			{id = 8473, count = 2, chance = 360}, -- UHP
			{id = 11220, chance = 9900}, -- strand of medusa hair
			{id = 7590, count = 2, chance = 1160}, -- GMP
			{id = 2152, count = {2, 3}, chance = 34840}, -- plat coin
			{id = 2148, count = {41, 62}, chance = 97680}, -- gold
		}
	},
	{ name = 'Sandstone Cobra', level = 38, file = 'Cirith/Serpents/sandstone cobra.xml', look = {type = 486}, classId = 7, killAmount = 1000, charmPoints = 25,
		description = {'Pustynna Kobra', 'Sandstone Cobra'}, experience = 1100, health = 1950, healthMax = 1950,
		boss = 'Snaker', chance = 100,
		elements = {
			{ type = 'death', value = 20 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = -5 },
			{ type = 'wind', value = 100 },
		},
		loot = {
			{id = 8857, chance = 10}, -- silweaver bow
			{id = 2154, chance = 50}, -- yellow gem
			{id = 3982, chance = 50}, -- green robe
			{id = 2656, chance = 10}, -- blue robe
			{id = 15756, chance = 60}, -- snake armor
			{id = 15719, chance = 290}, -- battle hammer
			{id = 15732, chance = 320}, -- knight axe
			{id = 13896, chance = 280}, -- giant sword
			{id = 18338, chance = 10}, -- red gem
			{id = 13990, chance = 160}, -- emerald armor
			{id = 8902, chance = 2210}, -- sb of mind control
			{id = 14348, chance = 660}, -- plate armor
			{id = 14571, chance = 7690}, -- cobra tail
			{id = 14560, chance = 8790}, -- cobra head
			{id = 2152, chance = 17600}, -- plat coin
			{id = 2159, count = 3, chance = 18610}, -- scarab coin
		}
	},
	{ name = 'Serpent Spawn', level = 62, file = 'Cirith/Serpents/serpent spawn.xml', look = {type = 220}, classId = 7, killAmount = 2500, charmPoints = 50,
		description = {'W�owy Tw�r', 'Serpent Spawn'}, experience = 2150, health = 3000, healthMax = 3000,
		boss = 'Steno', chance = 100,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 16197, chance = 100}, -- druid armor
			{id = 8902, chance = 190}, -- sb mind control
			{id = 14480, chance = 150}, -- spartan helmet
			{id = 13980, chance = 190, subType = 120}, -- terra amulet
			{id = 14172, chance = 150}, -- chest guard
			{id = 14482, chance = 150}, -- antic helmet
			{id = 2167, chance = 2000}, -- energy ring
			{id = 4842, chance = 590}, -- old parchment
			{id = 14488, chance = 160}, -- visor
			{id = 15761, chance = 120}, -- snake blade
			{id = 2177, chance = 1800}, -- life crystal
			{id = 14319, chance = 390}, -- steel shield
			{id = 8849, chance = 850}, -- modified crossbow
			{id = 14453, chance = 930}, -- wand
			{id = 11224, chance = 970}, -- winged tail
			{id = 7590, chance = 990}, -- GMP
			{id = 14318, chance = 160}, -- royal steel blade
			{id = 2033, chance = 2970}, -- golden mug
			{id = 2547, chance = 6130}, -- power bolt
			{id = 2146, chance = 11970}, -- small sapphire
			{id = 10610, chance = 14800}, -- snake skin
			{id = 2796, chance = 18380}, -- green mushroom
			{id = 2152, count = 2, chance = 41690}, -- silver
			{id = 2148, count = {41, 87}, chance = 97200}, -- gold
		}
	},
	{ name = 'Snake', file = 'Cirith/Serpents/snake.xml', look = {type = 28}, classId = 7, killAmount = 250, charmPoints = 5,
		description = {'W��', 'Snake'}, experience = 10, health = 15, healthMax = 15,
		boss = 'Snaker', chance = 90,
		elements = {
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -10 },
		}},
	
	--  Traps 
	{ name = 'Mimic Crate', file = 'Cirith/Traps/mimic.xml', look = {type = 523}, classId = 27, killAmount = 500, charmPoints = 15,
		description = {'Mimik', 'Mimic'}, experience = 3100, health = 5000, healthMax = 5000,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'holy', value = 80 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = 45 },
			{ type = 'earth', value = 45 },
			{ type = 'fire', value = 45 },
			{ type = 'ice', value = 45 },
			{ type = 'wind', value = 5 },
		},
		loot = {
			{id = 16435, chance = 50}, -- bal gown
			{id = 13890, chance = 500}, -- bow of the miser
			{id = 16269, chance = 1000}, -- crystal spike backpack
			{id = 14006, chance = 2500}, -- cape of fear
			{id = {13055, 13070, 13058, 13061}, chance = 5000}, -- bronze crafting additives
			{id = 16305, chance = 5700}, -- enhanced leather hood
			{id = 12964, chance = 7000}, -- lunar crystal
			{id = {11258, 9837}, chance = 7250}, -- dragon/velvet tapestry
			{id = 14358, chance = 11250}, -- bronze axe
			{id = {2143, 2144}, count = 2, chance = 12580}, -- pearls
			{id = 2145, count = 3, chance = 13140}, -- small diamond
			{id = {2146, 2147, 2149, 2150}, count = 5, chance = 15600}, -- small crystals
			{id = 2151, count = 5, chance = 19400}, -- talon
			{id = 2120, chance = 21150}, -- rope
			{id = {2153, 2154, 2155, 2156, 2158}, chance = 25000}, -- gem
			{id = 5889, count = 2, chance = 31320}, -- piece of steel
			{id = 8309, count = 5, chance = 25890}, -- nail
			{id = 2050, chance = 42340}, -- torch
			{id = 14014, chance = 56350}, -- robe
			{id = 14765, chance = 61900}, -- cape
			{id = 2560, chance = 92340}, -- mirror
			{id = 2152, count = 2, chance = 41690}, -- silver
			{id = 2148, count = 50, chance = 96760}, -- coin
		}
	},
	{ name = 'Lavahole', file = 'Cirith/Traps/lavahole.xml', look = {type = 0, typeEx = 389, auxType = 391},
		description = {'Krater', 'Lavahole'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Oriental Pillar', file = 'Cirith/Traps/pillar.xml', look = {type = 0, typeEx = 1551, auxType = 2190},
		description = {'Filar', 'Pillar'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	{ name = 'Mushroom', file = 'Cirith/Traps/mushroom.xml', look = {type = 0, typeEx = 10778, auxType = 9862},
		description = {'Grzyb', 'Mushroom'}, experience = 0, health = 100, healthMax = 100,
		elements = {
			{ type = 'physical', value = 100 },
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
			{ type = 'wind', value = 100 },
		} },
	
	-- Trolls
	{ name = 'Swamp Troll', level = 12, file = 'Cirith/Trolls/swamp troll.xml', look = {type = 186}, classId = 17, killAmount = 250, charmPoints = 5,
		description = {'Bagienny Troll', 'Swamp Troll'}, experience = 25, health = 55, healthMax = 55,
		boss = 'Ellort', chance = 40,
		elements = {
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -10 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = -10 },
		},
		loot = {
			{id = 15785, chance = 80}, -- miedziany helm
			{id = 15782, chance = 110}, -- stalowy oszczep
			{id = 15778, chance = 3180}, -- drewniana tarcza
			{id = 12963, chance = 140}, -- lampart shield
			{id = 15773, chance = 5140}, -- skorzany pancerz
			{id = 15780, chance = 230, subType = 980}, -- zebiasty wisior
			{id = 2050, chance = 11690}, -- torch
			{id = 2666, chance = 9600}, -- meat
			{id = 2120, chance = 6870}, -- rope
			{id = 12962, chance = 7400}, -- lampart spear
			{id = 10605, chance = 1980}, -- bunch of troll hair
			{id = 14771, chance = 780}, -- cure potion
			{id = 15791, chance = 100, subType = 28}, -- bursztynowe oko
			{id = 2148, count = 8, chance = 32500}, -- coin
		}
	},
	{ name = 'Troll Bleed', level = 24, file = 'Cirith/Trolls/troll bleed.xml', look = {type = 184}, classId = 17, killAmount = 500, charmPoints = 15,
		description = {'Krwawy Troll', 'Bloody Troll'}, experience = 65, health = 150, healthMax = 150,
		boss = 'Ellort', chance = 70,
		elements = {
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 14357, chance = 520}, -- bronze shield
			{id = 14351, chance = 290}, -- iron boots
			{id = 15786, chance = 1200}, -- copper armor
			{id = 13706, chance = 1150}, -- beastly claw
			{id = 13707, chance = 700}, -- cuprite
			{id = 14461, chance = 1900}, -- soldier helmet
			{id = 14448, chance = 900}, -- necrotic rod
			{id = 15787, chance = 2310}, -- copper legs
			{id = 15788, chance = 7400}, -- copper boots
			{id = 10605, chance = 5140}, -- bunch of troll hair
			{id = 15714, chance = 2480}, -- club
			{id = 14015, chance = 480}, -- short bow
			{id = 2628, chance = 180}, -- The Armor of Hardened Leather
			{id = 15738, chance = 340}, -- bow
			{id = 13948, chance = 1860}, -- hunting spear
			{id = 13155, chance = 4900}, -- untreated leather
			{id = 2544, count = 12, chance = 19760}, -- arrow
			{id = 2666, chance = 39890}, -- meat
			{id = 2148, count = {5, 18}, chance = 28340}, -- coin
		}
	},
	{ name = 'Troll Champion Bleed', level = 28, file = 'Cirith/Trolls/troll champion bleed.xml', look = {type = 185}, classId = 17, killAmount = 500, charmPoints = 15,
		description = {'Krwawy Troll Czempion', 'Bloody Troll Champion'}, experience = 110, health = 230, healthMax = 230,
		boss = 'Ellort', chance = 90,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 14357, chance = 2980}, -- bronze shield
			{id = 15785, chance = 5470}, -- copper helmet
			{id = 13707, chance = 900}, -- cuprite
			{id = 13948, chance = 2200}, -- hunting spear
			{id = 15786, chance = 7050}, -- copper armor
			{id = 14015, chance = 540}, -- short bow
			{id = 14358, chance = 920}, -- bronze axe
			{id = 13706, chance = 1700}, -- beastly claw
			{id = 15713, chance = 850}, -- longsword
			{id = 15787, chance = 590}, -- copper legs
			{id = 15788, chance = 810}, -- copper boots
			{id = 2376, chance = 150}, -- runed blade
			{id = 14461, chance = 1750}, -- soldier helmet
			{id = 13155, chance = 5420}, -- untreated leather
			{id = 7618, chance = 430}, -- health potion
			{id = 10605, chance = 470}, -- bunch of troll hair
			{id = 2628, chance = 210}, -- The Armor of Hardened Leather
			{id = 2147, chance = 280}, -- small ruby
			{id = 2666, chance = 55570}, -- meat
			{id = 2148, count = {6, 20}, chance = 40220}, -- coin
		}
	},
	{ name = 'Troll Champion Fire', level = 80, file = 'Cirith/Trolls/troll champion fire.xml', look = {type = 190}, classId = 17, killAmount = 2500, charmPoints = 50,
		description = {'Ognisty Troll Czempion', 'Fire Troll Champion'}, experience = 1580, health = 2400, healthMax = 2400,
		boss = 'Ellort', chance = 150,
		elements = {
			{ type = 'death', value = -5 },
			{ type = 'energy', value = 15 },
			{ type = 'fire', value = 80 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 16204, chance = 50}, -- fiery shield
			{id = 16156, chance = 70}, -- black steel legs
			{id = 2539, chance = 90}, -- phoenix shield
			{id = 2392, chance = 560}, -- fire sword
			{id = 12967, chance = 340}, -- red crystal (implosion)
			{id = 2195, chance = 190}, -- boh
			{id = 13951, chance = 450}, -- fiery spear
			{id = 14324, chance = 810}, -- steel shield
			{id = 2214, chance = 3180}, -- roh
			{id = 2152, chance = 15610}, -- silver
			{id = 11679, chance = 60}, -- hellfire bow
			{id = 6529, count = 3, chance = 980}, -- infernal bolt
			{id = 10552, chance = 10130}, -- fiery heart
			{id = 10605, chance = 15330}, -- bunch of troll hair
			{id = {7591, 7590}, chance = 800}, -- GMP/GHP
			{id = 5944, chance = 1380}, -- soul orb
			{id = 2671, count = 2, chance = 29380}, -- meat
			{id = 2148, count = {56, 79}, chance = 96940}, -- coin
		}
	},
	{ name = 'Troll Champion Ice', level = 45, file = 'Cirith/Trolls/troll champion ice.xml', look = {type = 88}, classId = 17, killAmount = 1000, charmPoints = 25,
		description = {'Lodowy Troll Czempion', 'Icy Troll Champion'}, experience = 980, health = 1350, healthMax = 1350,
		boss = 'Ellort', chance = 120,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'fire', value = 40 },
			{ type = 'ice', value = 80 },
		},
		loot = {
			{id = 2165, chance = 140}, -- stealth ring
			{id = 2656, chance = 420}, -- blue robe
			{id = 14482, chance = 150}, -- Ancient helmet
			{id = 7618, chance = 420}, -- hp
			{id = 17704, chance = 50}, -- the frosty midnight fire
			{id = 13891, chance = 420}, -- frosty bow
			{id = 14487, chance = 490}, -- chain helmet
			{id = 15720, chance = 780}, -- broad sword
			{id = 2200, chance = 280}, -- amulet
			{id = 2167, chance = 800}, -- energy ring
			{id = 13299, chance = 440}, -- glass spear
			{id = 16910, chance = 120}, -- a crystal rod of frost
			{id = 14348, chance = 1270}, -- iron armor
			{id = 14350, chance = 350}, -- iron legs
			{id = 14353, chance = 6400}, -- bronze armor
			{id = 14355, chance = 6980}, -- bronze legs
			{id = 10564, chance = 5280}, -- frozen troll ear
			{id = 14170, chance = 1550}, -- leggionaire armor
			{id = 15789, chance = 10870}, -- copper sword
			{id = 2148, count = {13, 30}, chance = 94830}, -- coin
		}
	},
	{ name = 'Troll Champion Leaf', level = 60, file = 'Cirith/Trolls/troll champion leaf.xml', look = {type = 187}, classId = 17, killAmount = 1000, charmPoints = 25,
		description = {'Le�ny Troll Czempion', 'Leaf Troll Champion'}, experience = 1470, health = 1800, healthMax = 1800,
		boss = 'Ellort', chance = 140,
		elements = {
			{ type = 'death', value = 50 },
			{ type = 'energy', value = 50 },
			{ type = 'earth', value = 80 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 16167, chance = 110}, -- swamplair armor
			{id = 10219, chance = 320, subType = 5}, -- sacred tree amulet
			{id = 15757, chance = 220}, -- snake legs
			{id = 3982, chance = 10}, -- green robe
			{id = 2165, chance = 110}, -- stealth ring
			{id = 14005, chance = 110}, -- wizard's robe
			{id = 14324, chance = 500}, -- steel shield
			{id = 13988, chance = 180}, -- plate of underwater
			{id = 13980, chance = 890, subType = 80}, -- terra amulet
			{id = 7590, chance = 910}, -- gmp
			{id = 13985, chance = 410}, -- armor of loyalists
			{id = 13744, chance = 8070}, -- trollroot
			{id = 16169, chance = 190}, -- swamplair bow
			{id = 2214, chance = 1120}, -- roh
			{id = 7589, chance = 1000}, -- smp
			{id = 15717, chance = 360}, -- glorious axe
			{id = 7588, chance = 560}, -- shp
			{id = 2149, count = 3, chance = 5740}, -- small emerald
			{id = 14350, chance = 200}, -- iron legs
			{id = 10605, chance = 10140}, -- bunch of troll hair
			{id = 2152, chance = 25650}, -- silver
			{id = 8845, count = 2, chance = 60470}, -- beetroot
			{id = 2148, count = {28, 58}, chance = 98260}, -- coin
		}
	},
	{ name = 'Troll Champion Shadow', level = 10, file = 'Cirith/Trolls/troll champion shadow.xml', look = {type = 189}, classId = 17, killAmount = 250, charmPoints = 5,
		description = {'Cienisty Troll Czempion', 'Shadow Troll Champion'}, experience = 45, health = 75, healthMax = 75,
		boss = 'Baru\'khur Al\'s Akr', chance = 40,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 15772, chance = 6250}, -- leather helmet
			{id = 15773, chance = 6000}, -- leather armor
			{id = 15774, chance = 3590}, -- leather legs
			{id = 12962, chance = 2800}, -- lampart spear
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 1650}, -- lampart set
			{id = 15775, chance = 5970}, -- leather boots
			{id = 13706, chance = 1990}, -- beastly claw
			{id = 5880, chance = 400}, -- iron ore
			{id = 13754, chance = 5160}, -- rope belt
			{id = 15778, chance = 5260}, -- wooden shield
			{id = 2544, count = 14, chance = 15320}, -- arrow
			{id = 14333, chance = 160}, -- zdobiony sword
			{id = 7618, chance = 1390}, -- health potion
			{id = 13155, chance = 4560}, -- untreated leather
			{id = 2545, count = 8, chance = 8590}, -- arrow
			{id = 10605, chance = 6250}, -- bunch of troll hair
			{id = 2666, chance = 10870}, -- meat
			{id = 11425, count = 20, chance = 29070}, -- arrow
			{id = 2148, count = 10, chance = 41980}, -- coin
		}
	},
	{ name = 'Troll Fire', level = 60, file = 'Cirith/Trolls/troll fire.xml', look = {type = 191}, classId = 17, killAmount = 2500, charmPoints = 50,
		description = {'Ognisty Troll', 'Fire Troll'}, experience = 875, health = 1300, healthMax = 1300,
		boss = 'Ellort', chance = 140,
		elements = {
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 20 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 75 },
			{ type = 'ice', value = -15 },
		},
		loot = {
			{id = 13995, chance = 210}, -- lavos armor
			{id = 13939, chance = 130}, -- fire crossbow
			{id = 2392, chance = 310}, -- fire sword
			{id = 2647, chance = 120}, -- red robe
			{id = 13951, chance = 350}, -- fiery spear
			{id = 15750, chance = 1800}, -- brass helmet
			{id = 13981, chance = 950, subType = 40}, -- magma amulet
			{id = 14447, chance = 430}, -- woi
			{id = 15778, chance = 4370}, -- wooden shield
			{id = 2642, chance = 1420}, -- sandals
			{id = 15775, chance = 2590}, -- leather boots
			{id = 10605, chance = 3250}, -- bunch of troll hair
			{id = 15718, chance = 2760}, -- chopper
			{id = 15772, chance = 9980}, -- leather helmet
			{id = 14348, chance = 200}, -- plate armor
			{id = 2050, chance = 7690}, -- torch
			{id = 14336, chance = 11460}, -- hood
			{id = 2379, chance = 2390}, -- blade
			{id = 2666, chance = 15180}, -- meat
			{id = 2152, chance = 20500}, -- silver
			{id = 2148, count = {24, 36}, chance = 59800}, -- coin
		}
	},
	{ name = 'Troll Ice', level = 25, file = 'Cirith/Trolls/troll ice.xml', look = {type = 86}, classId = 17, killAmount = 1000, charmPoints = 25,
		description = {'Lodowy Troll', 'Icy Troll'}, experience = 370, health = 650, healthMax = 650,
		boss = 'Ellort', chance = 100,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = 20 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'fire', value = 60 },
			{ type = 'ice', value = 75 },
		},
		loot = {
			{id = 2656, chance = 210}, -- blue robe
			{id = 13979, chance = 100}, -- crusher
			{id = 13707, chance = 900}, -- cuprite
			{id = 14451, chance = 390}, -- ice cross
			{id = 13950, chance = 350}, -- frozen spear
			{id = 13155, chance = 9990}, -- untreated leather
			{id = 2195, chance = 100}, -- boh
			{id = 2171, chance = 340}, -- platinum amulet
			{id = 13891, chance = 270}, -- frosty bow
			{id = 2167, chance = 490}, -- energy ring
			{id = 2200, chance = 260}, -- amulet
			{id = 2124, chance = 990}, -- crystal ring
			{id = 7589, chance = 670}, -- SMP
			{id = 2804, chance = 4810}, -- shadow herb
			{id = 2148, count = {12, 20}, chance = 89920}, -- coin
		}
	},
	{ name = 'Troll Leaf', level = 40, file = 'Cirith/Trolls/troll leaf.xml', look = {type = 186}, classId = 17, killAmount = 1000, charmPoints = 25,
		description = {'Le�ny Troll', 'Leaf Troll'}, experience = 550, health = 850, healthMax = 850,
		boss = 'Ellort', chance = 120,
		elements = {
			{ type = 'death', value = 45 },
			{ type = 'energy', value = 40 },
			{ type = 'earth', value = 75 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 11387, chance = 210}, -- luck amulet
			{id = 5878, chance = 1430}, -- hardener leather
			{id = 16167, chance = 10}, -- swamplair armor
			{id = 2155, chance = 80}, -- green gem
			{id = 16165, chance = 210}, -- swamplair boots
			{id = 16356, chance = 200}, -- container
			{id = 13155, chance = 8480}, -- untreated leather
			{id = 3982, chance = 290}, -- green robe
			{id = 2168, chance = 770}, -- life ring
			{id = 16169, chance = 180}, -- swamplair bow
			{id = 15758, chance = 460}, -- snake boots
			{id = 14778, chance = 200}, -- wooden rod
			{id = 8845, chance = 1020}, -- beetroot
			{id = 13744, chance = 6290}, -- trollroot
			{id = 2152, chance = 7990}, -- silver
			{id = 2149, count = 2, chance = 1830}, -- small emerald
			{id = 10602, chance = 5000}, -- grass
			{id = 7590, chance = 800}, -- gmp
			{id = 10605, chance = 9970}, -- bunch of troll hair
			{id = 7589, chance = 590}, -- smp
			{id = 2148, count = {18, 32}, chance = 63870}, -- coin
		}
	},
	{ name = 'Troll Shadow', level = 5, file = 'Cirith/Trolls/troll shadow.xml', look = {type = 188}, classId = 17, killAmount = 250, charmPoints = 5,
		description = {'Cienisty Troll', 'Shadow Troll'}, experience = 32, health = 50, healthMax = 50,
		boss = 'Ellort', chance = 40,
		elements = {
			{ type = 'holy', value = 10 },
			{ type = 'death', value = -10 },
			{ type = 'energy', value = 15 },
			{ type = 'earth', value = -10 },
		},
		loot = {
			{id = 15772, chance = 5180}, -- leather helmet
			{id = 15773, chance = 4000}, -- leather armor
			{id = 15774, chance = 3590}, -- leather legs
			{id = {12961, 12960, 12959, 12958, 12963}, chance = 990}, -- lampart set
			{id = 15775, chance = 2050}, -- leather boots
			{id = 15778, chance = 4190}, -- wooden shield
			{id = 13706, chance = 1930}, -- beastly claw
			{id = 5880, chance = 210}, -- iron ore
			{id = 13754, chance = 4100}, -- rope belt
			{id = 12962, chance = 3970}, -- lampart spear
			{id = 2642, chance = 220}, -- sandals
			{id = 15714, chance = 2750}, -- club
			{id = 2379, chance = 12390}, -- blade
			{id = 10605, chance = 3250}, -- bunch of troll hair
			{id = 2120, chance = 5150}, -- rope
			{id = 2666, chance = 15180}, -- meat
			{id = 13155, chance = 5000}, -- untreated leather
			{id = 2050, chance = 7690}, -- torch
			{id = 11425, count = 5, chance = 29860}, -- arrow
			{id = 2545, count = 8, chance = 4190}, -- arrow
			{id = 2148, count = 8, chance = 32500}, -- coin
		}
	},
	
	-- Undead
	{ name = 'Banshee', level = 58, file = 'Cirith/Undeads/banshee.xml', look = {type = 78}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Banshee', 'Banshee'}, experience = 750, health = 1000, healthMax = 1000,
		boss = 'Legion', chance = 110,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = 10 },
		},
		loot = {
			{id = 2124, chance = 80}, -- crystal ring
			{id = 2177, chance = 1180}, -- life crystal
			{id = 2121, chance = 110}, -- wedding ring
			{id = 2655, chance = 150}, -- red robe
			{id = 18073, chance = 110}, -- ethereal staff
			{id = 15756, chance = 120}, -- snake armor
			{id = 8900, chance = 260}, -- spellbook
			{id = 2656, chance = 90}, -- blue robe
			{id = 8903, chance = 300}, -- spellbook of lost souls
			{id = 7589, chance = 770}, -- smp
			{id = 2214, chance = 840}, -- roh
			{id = 2197, chance = 890, subType = 5}, -- ssa
			{id = 2071, chance = 1060}, -- lyre
			{id = 2143, chance = 7190}, -- white pearl
			{id = 16143, chance = 110}, -- golden blade
			{id = {13945, 13947}, chance = 150}, -- ashbringer hood/robe
			{id = 2440, chance = 220}, -- daramanian axe
			{id = 2134, chance = 1610}, -- silver brooch
			{id = 2144, chance = 5180}, -- black pearl
			{id = 13733, chance = 6500}, -- hair of banshee
			{id = 11331, chance = 4440}, -- petrified scream
			{id = 14765, chance = 6630}, -- coat
			{id = 2170, chance = 9760, subType = 155}, -- silver amulet
			{id = 2148, count = 10, chance = 32180}, -- gold
			{id = 2047, chance = 75860}, -- candlestick
		}
	},
	{ name = 'Bonebeast', level = 25, file = 'Cirith/Undeads/bonebeast.xml', look = {type = 101}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Kosciana Bestia', 'Bonebeast'}, experience = 280, health = 515, healthMax = 515,
		boss = 'Azog', chance = 80,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 11155, chance = 120}, -- bonebeast trophy
			{id = 7618, chance = 530}, -- health potion
			{id = 5925, count = 3, chance = 7480}, -- hardened bone
			{id = 2796, chance = 1400}, -- green mushroom
			{id = 15778, chance = 2080}, -- wooden shield
			{id = 15714, chance = 4980}, -- club
			{id = 14362, chance = 280}, -- plate armor
			{id = 10559, chance = 2400}, -- horn
			{id = 11188, chance = 9800}, -- bony tail
			{id = 2229, chance = 19610}, -- skull
			{id = 2148, count = {18, 26}, chance = 41000}, -- gold
			{id = 2230, chance = 49320}, -- bone
		}
	},
	{ name = 'Crypt Shambler', level = 24, file = 'Cirith/Undeads/crypt shambler.xml', look = {type = 311}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Umarly', 'Crypt Shambler'}, experience = 185, health = 330, healthMax = 330,
		boss = 'Azog', chance = 100,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 5 },
			{ type = 'energy', value = 25 },
		},
		loot = {
			{id = {14353, 14354, 14355, 14357}, chance = 500}, -- bronze set
			{id = 11194, chance = 4900}, -- half-digested piece of meat
			{id = 2230, chance = 4830}, -- bone
			{id = 14455, chance = 130}, -- the creeper of darkness
			{id = 14484, chance = 630}, -- guard's helmet
			{id = 2168, chance = 230}, -- life ring
			{id = 15720, chance = 120}, -- broadsword
			{id = 5925, chance = 5000}, -- hardened bone
			{id = 2227, chance = 1880}, -- rotten meat
			{id = 2145, chance = 510}, -- small diamond
			{id = 15714, chance = 12040}, -- club
			{id = 3976, count = 10, chance = 8990}, -- worm
			{id = 2148, count = {21, 46}, chance = 57800}, -- gold
		}
	},
	{ name = 'Demon Skeleton Warrior', level = 38, file = 'Cirith/Undeads/demon skeleton warrior.xml', look = {type = 387}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Splugawiony Szkielet', 'Defiled Skeleton'}, experience = 870, health = 1200, healthMax = 1200,
		boss = 'Azog', chance = 120,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 6526, chance = 20}, -- skeleton decoration
			{id = 2178, chance = 510}, -- mind stone
			{id = 2194, chance = 580}, -- mysterious fetish
			{id = {14353, 14354, 14355, 14357}, chance = 520}, -- bronze set
			{id = 15720, chance = 380}, -- broadsword
			{id = 2147, chance = 1500}, -- small ruby
			{id = 2144, chance = 3100}, -- black pearl
			{id = 2050, chance = 4930}, -- torch
			{id = 2157, chance = 720}, -- gold nugget
			{id = 5925, count = 2, chance = 13000}, -- hardened bone
			{id = 15607, chance = 40}, -- skullcrusher bow
			{id = 7620, chance = 780}, -- mana potion
			{id = 13592, chance = 300}, -- cursed spear
			{id = 7588, count = 2, chance = 800}, -- SHP
			{id = 10563, chance = 12260}, -- demonic skeletal hand
			{id = 2152, chance = 13330}, -- silver coin
			{id = 2148, count = {18, 38}, chance = 86150}, -- gold
		}
	},
	{ name = 'Demon Skeleton', level = 34, file = 'Cirith/Undeads/demon skeleton.xml', look = {type = 37}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Demoniczny Szkielet', 'Demon Skeleton'}, experience = 240, health = 400, healthMax = 400,
		boss = 'Azog', chance = 100,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'toxic', value = 100 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 15 },
		},
		loot = {
			{id = 6526, chance = 15}, -- skeleton decoration
			{id = 14012, chance = 290}, -- crimson sword
			{id = 2144, chance = 3000}, -- black pearl
			{id = 2178, chance = 510}, -- mind stone
			{id = 2194, chance = 580}, -- mysterious fetish
			{id = 2157, chance = 530}, -- gold nugget
			{id = 5925, count = 2, chance = 9060}, -- hardened bone
			{id = 2147, chance = 1500}, -- small ruby
			{id = 15720, chance = 590}, -- broadsword
			{id = 2050, chance = 4930}, -- torch
			{id = 7620, chance = 580}, -- mana potion
			{id = 7618, count = 2, chance = 2730}, -- heath potion
			{id = 10563, chance = 12260}, -- demonic skeletal hand
			{id = 2148, count = {18, 44}, chance = 97510}, -- gold
		}
	},
	{ name = 'Frightening Slime', level = 39, file = 'Cirith/Undeads/frightening slime.xml', look = {type = 459}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Przera�aj�cy Szlam', 'Frightening Slime'}, experience = 175, health = 350, healthMax = 350,
		boss = 'Legion', chance = 80,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 14012, chance = 90}, -- crimson sword
			{id = 15718, chance = 320}, -- chopper
			{id = 15720, chance = 140}, -- broadsword
			{id = 5925, count = 2, chance = 3000}, -- hardened bone
			{id = 13948, chance = 750}, -- hunting spear
			{id = 14455, chance = 250}, -- the creeper of darkness
			{id = 2168, chance = 760}, -- life ring
			{id = 2127, chance = 940}, -- emerald bangle
			{id = 13948, chance = 350}, -- steel spear
			{id = {2230, 2231}, chance = 75860}, -- bone
			{id = 7620, count = 2, chance = 670}, -- mana potion
			{id = 2148, count = {8, 20}, chance = 41890}, -- copper
		}
	},
	{ name = 'Ghost', level = 19, file = 'Cirith/Undeads/ghost.xml', look = {type = 48}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Duch', 'Ghost'}, experience = 80, health = 150, healthMax = 150,
		boss = 'Legion', chance = 30,
		elements = {
			{ type = 'physical', value = 85 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 10 },
		},
		loot = {
			{id = 13299, chance = 50}, -- glass spear
			{id = 5909, chance = 1900}, -- white poc
			{id = 15776, chance = 120}, -- killer's blade
			{id = 1962, chance = 1330}, -- orange book
			{id = 5809, chance = 2990}, -- magical element
			{id = 11694, chance = 350}, -- fine fabric
			{id = 12998, chance = 30}, -- rider of light
			{id = 10606, chance = 1840}, -- ghostly tissue
			{id = 2804, chance = 14490}, -- shadow herb
		}
	},
	{ name = 'Ghoul', level = 20, file = 'Cirith/Undeads/ghoul.xml', look = {type = 18}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Ghul', 'Ghoul'}, experience = 170, health = 320, healthMax = 320,
		boss = 'Legion', chance = 40,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 5 },
			{ type = 'wind', value = 15 },
		},
		loot = {
			{id = 15784, chance = 590}, -- copper shield
			{id = 15785, chance = 600}, -- copper helmet
			{id = 15713, chance = 210}, -- longsword
			{id = 15786, chance = 450}, -- copper armor
			{id = 5809, chance = 720}, -- magical element
			{id = 5925, chance = 9990}, -- hardned bone
			{id = 2168, chance = 170}, -- life ring
			{id = 15787, chance = 620}, -- copper legs
			{id = 15788, chance = 590}, -- copper boots
			{id = 15789, chance = 990}, -- copper sword
			{id = 13746, chance = 6490}, -- pile of grave earth
			{id = 15783, chance = 6890}, -- rebacz
			{id = 16249, chance = 800}, -- bucket
			{id = 2050, chance = 4960}, -- torch
			{id = 5913, chance = 990}, -- brown piece of cloth
			{id = 2229, chance = 280}, -- skull
			{id = 14771, chance = 1980}, -- cure potion
			{id = 5889, chance = 3090}, -- piece of steel
			{id = 2050, chance = 4970}, -- torch
			{id = 13706, chance = 1870}, -- beastly claw
			{id = 3976, count = 2, chance = 9710}, -- worm
			{id = 11202, chance = 14760}, -- rotten piece of cloth
			{id = 2148, count = {13, 31}, chance = 68780}, -- gold
		}
	},
	{ name = 'Grim Reaper', level = 86, file = 'Cirith/Undeads/grim reaper.xml', look = {type = 625, addons = 1}, classId = 5, killAmount = 2500, charmPoints = 50,
		description = {'Ponury �niwiarz', 'Grim Reaper'}, experience = 3500, health = 4250, healthMax = 4250,
		boss = 'Last Breath', chance = 100,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 80 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 40 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 65 },
		},
		loot = {
			{id = 15728, chance = 100}, -- Skeleton striking face
			{id = 13993, chance = 220}, -- shadow armor
			{id = {13945, 13946, 13944}, chance = 340}, -- ashbringer hood
			{id = 6300, chance = 240}, -- death ring
			{id = 16190, chance = 110}, -- frozen legs
			{id = 16162, chance = 420}, -- black steel hammer
			{id = 14455, chance = 450}, -- underworld rod
			{id = 11694, chance = 1690}, -- fine fabric
			{id = 2143, count = 4, chance = 7480}, -- white pearl
			{id = 17161, chance = 450}, -- death enchanted in the staff
			{id = 2521, chance = 130}, -- dark shield
			{id = 2162, chance = 4970}, -- magic lightwand
			{id = 2152, chance = 65080}, -- silver coin
			{id = 10576, chance = 5080}, -- mystical hourglass
			{id = 16909, chance = 310}, -- sickle of the wind
			{id = 17683, chance = 110}, -- cursed blade
			{id = 7590, chance = 700}, -- GMP
			{id = 2537, chance = 150}, -- haunted helmet
			{id = 8473, chance = 520}, -- UHP
			{id = 15739, chance = 160}, -- necrotic bow
			{id = 6500, chance = 1680}, -- demonic essence
			{id = 6558, chance = 5060}, -- concentrated demonic blood
			{id = 2148, count = {40, 75}, chance = 98830}, -- gold coin
		}
	},
	{ name = 'Lich', level = 70, file = 'Cirith/Undeads/lich.xml', look = {type = 99}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Licz', 'Lich'}, experience = 1350, health = 1880, healthMax = 1880,
		boss = 'Legion', chance = 100,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 70 },
			{ type = 'earth', value = 100 },
		},
		loot = {
			{id = 14455, chance = 15}, -- staff
			{id = 2154, chance = 70}, -- yellow gem
			{id = 14009, chance = 50}, -- robe
			{id = {13945, 13947}, chance = 100}, -- ashbringer hood/robe
			{id = 15758, chance = 130}, -- snake boots
			{id = 2535, chance = 70}, -- castle shield
			{id = 2178, chance = 300}, -- mind stone
			{id = 2171, chance = 370}, -- platinium amulet
			{id = 13886, chance = 10}, -- haunted blade
			{id = 14481, chance = 10}, -- lich hat
			{id = 14478, chance = 270}, -- steel helmet
			{id = 2214, chance = 1530}, -- roh
			{id = 2149, count = 3, chance = 2170}, -- small emerald
			{id = 14357, chance = 2400}, -- bronze shield
			{id = 18338, chance = 10}, -- red gem
			{id = 9970, count = 3, chance = 2670}, -- small topaz
			{id = 2143, chance = 9530}, -- white pearl
			{id = 2144, count = 3, chance = 5370}, -- black pearl
			{id = 7589, chance = 540}, -- smp
			{id = 2152, chance = 36210}, -- silver
			{id = 2148, count = {26, 44}, chance = 99730}, -- gold
		}
	},
	{ name = 'Mummy', level = 25, file = 'Cirith/Undeads/mummy.xml', look = {type = 65}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Mumia', 'Mummy'}, experience = 150, health = 240, healthMax = 240,
		boss = 'Azog', chance = 80,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 15 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 15780, chance = 950, subType = 1000}, -- tooth necklace
			{id = 11694, chance = 490}, -- fine fabric
			{id = 15754, chance = 2410, subType = 100}, -- brass necklace
			{id = 15713, chance = 10}, -- longsword
			{id = 2379, chance = 470}, -- blade
			{id = 5914, chance = 860}, -- yellow poc
			{id = 2144, chance = 1020}, -- black pearl
			{id = 2143, chance = 1000}, -- white pearl
			{id = 2134, chance = 3860}, -- silver brooch
			{id = 2162, chance = 5800}, -- magic lightwand
			{id = 10565, chance = 9950}, -- gauze bandage
			{id = 3976, count = 3, chance = 19360}, -- worm
			{id = 2148, count = {15, 28}, chance = 38690}, -- gold
		}
	},
	{ name = 'Nightstalker', level = 34, file = 'Cirith/Undeads/nightstalker.xml', look = {type = 320}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Mara', 'Nightstalker'}, experience = 400, health = 700, healthMax = 700,
		boss = 'Legion', chance = 60,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -5 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 5809, chance = 1880}, -- magical element
			{id = 16162, chance = 210}, -- hammer of black steel
			{id = 2195, chance = 160}, -- boh
			{id = 9942, chance = 130}, -- crystal of balance
			{id = 2171, chance = 140}, -- platinum amulet
			{id = 15720, chance = 110}, -- broadsword
			{id = 14000, chance = 520}, -- light armor
			{id = 2537, chance = 150}, -- haunted helmet
			{id = 2200, chance = 460}, -- prostection amulet
			{id = 2124, chance = 990}, -- crystal ring
			{id = 7589, chance = 570}, -- smp
			{id = 2804, chance = 4820}, -- shadow herb
			{id = 2148, count = 12, chance = 89920}, -- gold
		}
	},
	{ name = 'Skeleton Warrior', level = 13, file = 'Cirith/Undeads/skeleton warrior.xml', look = {type = 298}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Szkielet Wojownik', 'Skeleton Warrior'}, experience = 55, health = 105, healthMax = 105,
		boss = 'Azog', chance = 80,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 6526, chance = 10}, -- skeleton decoration
			{id = 15750, chance = 200}, -- brass helmet
			{id = 15751, chance = 250}, -- brass armor
			{id = 15752, chance = 190}, -- brass legs
			{id = 15753, chance = 120}, -- brass boots
			{id = 5809, chance = 270}, -- magical element
			{id = 15790, chance = 490}, -- copper axe
			{id = 2379, chance = 7390}, -- blade
			{id = 15779, chance = 8210}, -- skuller
			{id = 5925, chance = 15140}, -- hardned bone
			{id = 2230, chance = 31890}, -- bone
			{id = 2544, chance = 2890, count = 6}, -- arrow
			{id = 2456, chance = 690}, -- bow
			{id = 2148, count = 17, chance = 6760}, -- coin
		}
	},
	{ name = 'Skeleton', level = 8, file = 'Cirith/Undeads/skeleton.xml', look = {type = 33}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Szkielet', 'Skeleton'}, experience = 35, health = 50, healthMax = 50,
		boss = 'Azog', chance = 50,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'fire', value = 5 },
		},
		loot = {
			{id = 6526, chance = 50}, -- skeleton decoration
			{id = 14487, chance = 210}, -- chain helmet
			{id = 15793, chance = 1070}, -- sword
			{id = 15779, chance = 9210}, -- skuller
			{id = 2050, chance = 9800}, -- torch
			{id = 5809, chance = 90}, -- magical element
			-- {id = 16242, chance = 1790}, --backpack 
			{id = 5925, chance = 6590}, -- hardned bone
			{id = 13759, chance = 4870}, -- pelvis bone
			{id = 2230, chance = 27900}, -- bone
			{id = 2148, count = 7, chance = 4870}, -- coin
		}
	},
	{ name = 'Skeletor', level = 35, file = 'Cirith/Undeads/skeletor.xml', look = {type = 414}, classId = 5, killAmount = 500, charmPoints = 15,
		description = {'Ko�ciej', 'Skeletor'}, experience = 850, health = 1500, healthMax = 1500,
		boss = 'Azog', chance = 120,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 40 },
		},
		loot = {
			{id = 982, chance = 90}, -- dark talon
			{id = 2151, chance = 970}, -- talon
			{id = 13759, chance = 4870}, -- pelvis bone
			{id = 15737, chance = 30}, -- bone bow
			{id = 17161, chance = 80}, -- death enchanted in the staff
			{id = 2145, count = 5, chance = 4920}, -- small diamond
			{id = 15779, chance = 9210}, -- skuller
			{id = 2050, chance = 9800}, -- torch
			{id = 2789, count = 3, chance = 9930}, -- brown mushroom
			{id = 7589, chance = 910}, -- SMP
			{id = 7588, chance = 930}, -- SHP
			{id = 13942, chance = 50}, -- repeating crossbow
			{id = 15607, chance = 65}, -- skullcrusher bow
			{id = 2230, chance = 27900}, -- bone
			{id = 2152, chance = 11690}, -- silver
			{id = 2148, count = {20, 46}, chance = 99800}, -- coin
		}
	},
	{ name = 'Souleater', level = 50, file = 'Cirith/Undeads/souleater.xml', look = {type = 376}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Duszyciel', 'Souleater'}, experience = 950, health = 1900, healthMax = 1900,
		boss = 'Legion', chance = 100,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 11729, chance = 20}, -- souleater trophy
			{id = 11695, chance = 190}, -- lizard essence
			{id = 4863, chance = 10}, -- spirit container
			{id = 6300, chance = 310}, -- death ring
			{id = 14459, chance = 290}, -- magic rod
			{id = 14448, chance = 270}, -- necrotic rod
			{id = 2792, chance = 1980}, -- mushroom
			{id = 7590, chance = 520}, -- GMP
			{id = 8473, chance = 310}, -- UHP
			{id = 2152, chance = 69630}, -- silver coin
			{id = 2148, count = {34, 62}, chance = 88040}, -- coin
		}
	},
	{ name = 'Undead Cavebear', level = 45, file = 'Cirith/Undeads/undead cavebear.xml', look = {type = 385}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Undead Cavebear', 'Undead Cavebear'}, experience = 2250, health = 100, healthMax = 100,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 2168, chance = 100000}, -- broad sword
		}
	},
	{ name = 'Undead Dragon', level = 108, file = 'Cirith/Undeads/undead dragon.xml', look = {type = 231}, classId = 10, killAmount = 2500, charmPoints = 50,
		description = {'Nieumar�y Smok', 'Undead Dragon'}, experience = 7800, health = 9550, healthMax = 9550,
		boss = 'Last Breath', chance = 100,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 11409, chance = 10}, -- undead dragon doll
			{id = 2158, chance = 90}, -- blue gem
			{id = 13997, chance = 220}, -- warriors armor
			{id = 13993, chance = 250}, -- shadow armor
			{id = {13945, 13946}, chance = 510}, -- ashbringer hood
			{id = 16138, chance = 120}, -- golden armor
			{id = 14009, chance = 230}, -- sparkling robe
			{id = 15724, chance = 930}, -- berserker
			{id = {15747, 9971}, chance = 310}, -- dragon axe/gold ingot
			{id = 9810, chance = 1130}, -- rusty armor
			{id = 6300, chance = 1320}, -- death ring
			{id = 15744, chance = 300}, -- dragon plate
			{id = 6534, chance = 680}, -- flying trident
			{id = 14480, chance = 130}, -- spartan helmet
			{id = 2177, chance = 11940}, -- life crystal
			{id = 17130, chance = 350}, -- undead dragon wing
			{id = 14012, chance = 470}, -- crimson sword
			{id = 2033, chance = 4920}, -- golden mug
			{id = 14010, chance = 460}, -- knight armor
			{id = 6500, chance = 1010}, -- demonic essence
			{id = 2547, count = 15, chance = 14070}, -- power bolt
			{id = 5925, chance = 15320}, -- hardened bone
			{id = 7591, count = 3, chance = 1350}, -- GHP
			{id = 7590, count = 3, chance = 1420}, -- GMP
			{id = 2144, count = 2, chance = 23840}, -- black pearl
			{id = 2146, count = 2, chance = 25780}, -- small sapphire
			{id = 11227, chance = 32090}, -- unholy bone
			{id = 2152, count = 2, chance = 21070}, -- silver coin
			{id = 2148, count = 39, chance = 95140}, -- gold
		}
	},
	{ name = 'Vampire Bride', level = 60, file = 'Cirith/Undeads/vampire bride.xml', look = {type = 312}, classId = 31, killAmount = 1000, charmPoints = 25,
		description = {'Wampirzyca', 'Vampire Bride'}, experience = 900, health = 1200, healthMax = 1200,
		boss = 'Alucard', chance = 120,
		elements = {
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 10 },
			{ type = 'earth', value = 20 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 20 },
		},
		loot = {
			{id = 5669, chance = 150}, -- mysterious voodoo skull
			{id = 7733, chance = 200}, -- flowers
			{id = 2195, chance = 15}, -- boh
			{id = 15771, chance = 20}, -- the magic edge of life
			{id = 9837, chance = 940}, -- velvet tapestry
			{id = 5905, chance = 650}, -- vampire dust
			{id = 14006, chance = 90}, -- cloak of fear
			{id = 9809, chance = 1000}, -- semi rare rusty armor
			{id = 2127, chance = 1020}, -- emerald bangle
			{id = 2145, count = 2, chance = 1040}, -- small diamond
			{id = 7588, chance = 400}, -- SHP
			{id = 13736, chance = 4980}, -- blood preservation
			{id = 14448, chance = 430}, -- necrotic rod
			{id = 2152, chance = 9960}, -- silver coin
			{id = 10601, chance = 10050}, -- vampire teeth
			{id = 7590, chance = 1090}, -- SMP
			{id = 2148, count = {20, 36}, chance = 93500}, -- gold
		}
	},
	{ name = 'Vampire Count', level = 68, file = 'Cirith/Undeads/vampire count.xml', look = {type = 513, head = 0, body = 94, legs = 0, feet = 0}, classId = 31, killAmount = 1000, charmPoints = 25,
		description = {'Wampirzy Hrabia', 'Vampire Count'}, experience = 1860, health = 2400, healthMax = 2400,
		boss = 'Alucard', chance = 180,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 30 },
		},
		loot = {
			{id = 5913, chance = 1500}, -- brown poc
			{id = 8903, chance = 10}, -- spellbook of lost soul
			{id = 2534, chance = 80}, -- vampire shield
			{id = 13992, chance = 10}, -- bronze plate
			{id = 14004, chance = 150}, -- trapper boots
			{id = 15771, chance = 12}, -- the magic edge of life
			{id = 2144, chance = 5300}, -- black pearl
			{id = 2440, chance = 300}, -- daramanian axe
			{id = 2214, chance = 1550}, -- roh
			{id = 14455, chance = 150}, -- the creeper of darkness
			{id = 2229, chance = 2340}, -- skull
			{id = 13619, chance = 130}, -- vampire's cape chain
			{id = 7588, chance = 400}, -- shp
			{id = 5905, chance = 650}, -- vampire dust
			{id = 13736, chance = 4980}, -- blood
			{id = 2152, count = 2, chance = 9960}, -- silver
			{id = 10601, chance = 10500}, -- vampire teeth
			{id = 7589, chance = 690}, -- smp
			{id = 2152, chance = 21890}, -- silver
			{id = 2148, count = 16, chance = 92130}, -- gold
		}
	},
	{ name = 'Vampire Lord', level = 100, file = 'Cirith/Undeads/vampire lord.xml', look = {type = 513, head = 0, body = 79, legs = 0, feet = 0}, classId = 31, killAmount = 1000, charmPoints = 25,
		description = {'Wampirzy Lord', 'Vampire Lord'}, experience = 3200, health = 4200, healthMax = 4200,
		boss = 'Alucard', chance = 400,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 30 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = 50 },
		},
		loot = {
			{id = 5911, chance = 50}, -- red poc
			{id = 15771, chance = 120}, -- the magic edge of life
			{id = 13978, chance = 150}, -- bloody crusher
			{id = 2534, chance = 230}, -- vampire shield
			{id = 12956, chance = 110}, -- marksman helmet
			{id = 5905, chance = 1650}, -- vampire dust
			{id = 6300, chance = 500}, -- death ring
			{id = 16161, chance = 110}, -- black steel blade
			{id = 13736, chance = 3140}, -- blood
			{id = 2144, count = 2, chance = 6770}, -- black pearl
			{id = 10601, chance = 7560}, -- vampire teeth
			{id = 5944, chance = 1010}, -- soul orb
			{id = 18076, chance = 200}, -- grey dye
			{id = 13619, chance = 770}, -- vampire's cape chain
			{id = 2145, count = 4, chance = 11340}, -- small diamond
			{id = 7591, count = 2, chance = 1820}, -- GHP
			{id = 7590, count = 2, chance = 1010}, -- GMP
			{id = 6500, chance = 1720}, -- de
			{id = 2152, count = 2, chance = 9960}, -- silver
			{id = 7364, count = 2, chance = 50870}, -- sniper arrow
			{id = 2148, count = {20, 39}, chance = 83010}, -- gold
		}
	},
	{ name = 'Vampire Viscount', level = 62, file = 'Cirith/Undeads/vampire viscount.xml', look = {type = 513, head = 0, body = 85, legs = 0, feet = 0}, classId = 31, killAmount = 1000, charmPoints = 25,
		description = {'Wampirzy Vicehrabia', 'Vampire Viscount'}, experience = 900, health = 1200, healthMax = 1200,
		boss = 'Alucard', chance = 120,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 40 },
			{ type = 'fire', value = 15 },
			{ type = 'ice', value = 30 },
		},
		loot = {
			{id = 5912, chance = 50}, -- blue poc
			{id = 2156, chance = 60}, -- red gem
			{id = 14010, chance = 80}, -- knight armor
			{id = 16158, chance = 10}, -- black steel helmet
			{id = 2534, chance = 50}, -- vampire shield
			{id = 14006, chance = 140}, -- cloak of fear
			{id = 15771, chance = 80}, -- the magic edge of life
			{id = 5905, chance = 650}, -- vampire dust
			{id = 2229, chance = 1020}, -- skull
			{id = 2144, chance = 4650}, -- black pearl
			{id = 13736, chance = 3140}, -- blood
			{id = 2147, count = 2, chance = 3240}, -- small ruby
			{id = 13619, chance = 130}, -- vampire's cape chain
			{id = 7588, chance = 680}, -- shp
			{id = 10601, chance = 7560}, -- vampire teeth
			{id = 7589, chance = 640}, -- smp
			{id = 2148, count = {11, 41}, chance = 83010}, -- gold
		}
	},
	{ name = 'Vampire', level = 42, file = 'Cirith/Undeads/vampire.xml', look = {type = 68}, classId = 31, killAmount = 1000, charmPoints = 25,
		description = {'Wampir', 'Vampire'}, experience = 400, health = 475, healthMax = 475,
		boss = 'Alucard', chance = 100,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 5809, chance = 1890}, -- magical element
			{id = 13706, count = 2, chance = 1780}, -- beastly claw
			{id = 15782, chance = 200}, -- steel spear
			{id = 2172, chance = 150}, -- bronze amulet
			{id = 2229, chance = 1020}, -- skull
			{id = 14007, chance = 10}, -- guardian shield
			{id = 2521, chance = 720}, -- dark shield
			{id = 14333, chance = 1990}, -- runed sword
			{id = 7588, chance = 540}, -- shp
			{id = 13979, chance = 10}, -- Crusher
			{id = 2144, chance = 2470}, -- black pearl
			{id = 2747, chance = 1980}, -- grave flower
			{id = 13736, chance = 5120}, -- blood
			{id = 5905, chance = 650}, -- vampire dust
			{id = 17656, chance = 1000}, -- copper blade
			{id = 10601, chance = 7480}, -- vampire teeth
			{id = 2148, count = {12, 31}, chance = 90040}, -- gold
		}
	},
	{ name = 'Vampire Bat', level = 35, file = 'Cirith/Undeads/vampire bat.xml', look = {type = 122}, classId = 31, killAmount = 1000, charmPoints = 25,
		description = {'Nietoperzy Wampir', 'Vampire Bat'}, experience = 200, health = 475, healthMax = 475,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = -10 },
		},
		loot = {
			{id = 5809, chance = 320}, -- magical element
			{id = 13706, chance = 980}, -- beastly claw
			{id = 15782, chance = 440}, -- steel spear
			{id = 2229, chance = 200}, -- skull
			{id = 13979, chance = 20}, -- Crusher
			{id = 2172, chance = 50}, -- bronze amulet
			{id = 14007, chance = 20}, -- guardian shield
			{id = 2521, chance = 140}, -- dark shield
			{id = 14333, chance = 400}, -- runed sword
			{id = 7588, chance = 310}, -- shp
			{id = 5905, chance = 125}, -- vampire dust
			{id = 2144, chance = 330}, -- black pearl
			{id = 2747, chance = 390}, -- grave flower
			{id = 13736, chance = 1020}, -- blood
			{id = 17656, chance = 200}, -- copper blade
			{id = 10601, chance = 1490}, -- vampire teeth
			{id = 2148, count = 2, chance = 18040}, -- gold
		}
	},
	{ name = 'Wisp', level = 90, file = 'Cirith/Undeads/wisp.xml', look = {type = 294}, classId = 5, killAmount = 250, charmPoints = 5,
		description = {'Ognik', 'Wisp'}, experience = 0, health = 460, healthMax = 460,
		elements = {
			{ type = 'physical', value = 35 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 30 },
			{ type = 'earth', value = 90 },
		},
		loot = {
			{id = 10521, chance = 10}, -- moon backpack
		}
	},
	{ name = 'Zombie', level = 32, file = 'Cirith/Undeads/zombie.xml', look = {type = 100}, classId = 5, killAmount = 1000, charmPoints = 25,
		description = {'Zombi', 'Zombie'}, experience = 280, health = 500, healthMax = 500,
		boss = 'Corpse Eater', chance = 100,
		elements = {
			{ type = 'lifedrain', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 5 },
			{ type = 'earth', value = 15 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 18076, chance = 10}, -- grey dye
			{id = 15784, chance = 1290}, -- copper shield
			{id = 5878, chance = 450}, -- hardener leather
			{id = 11694, chance = 600}, -- fine fabric
			{id = 15785, chance = 1000}, -- copper helmet
			{id = 15786, chance = 1250}, -- copper armor
			{id = 14334, chance = 800}, -- heavy axe
			{id = 15787, chance = 1620}, -- copper legs
			{id = 13155, chance = 6390}, -- untreated leather
			{id = 2168, chance = 320}, -- life ring
			{id = 15788, chance = 1190}, -- copper boots
			{id = 15783, chance = 7690}, -- Breaker
			{id = 7620, chance = 710}, -- mana potion
			{id = 10575, chance = 15170}, -- half eaten brain
			{id = 2148, count = {12, 34}, chance = 81950}, -- gold
		}
	},
		
		-- Pharaohs
		{ name = 'Merihathor', file = 'Cirith/Undeads/Pharaohs/Merihathor.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Merihathor', 'Merihathor'}, experience = 50000, health = 100000, healthMax = 100000,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 25 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Neferkare', file = 'Cirith/Undeads/Pharaohs/Neferkare.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Neferkare', 'Neferkare'}, experience = 30000, health = 25000, healthMax = 25000,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 100 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Achthoes III', file = 'Cirith/Undeads/Pharaohs/Achthoes III.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Achthoes III', 'Achthoes III'}, experience = 40000, health = 55000, healthMax = 55000,
		elements = {
			{ type = 'physical', value = 90 },
			{ type = 'bleed', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 75 },
			{ type = 'earth', value = 75 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Mentuhotep IV', file = 'Cirith/Undeads/Pharaohs/Mentuhotep IV.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Mentuhotep IV', 'Mentuhotep IV'}, experience = 42000, health = 60000, healthMax = 60000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = 100 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = 5 },
			{ type = 'ice', value = 15 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Amenemhat VI', file = 'Cirith/Undeads/Pharaohs/Amenemhat VI.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Amenemhat VI', 'Amenemhat VI'}, experience = 35000, health = 32000, healthMax = 32000,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 100 },
			{ type = 'earth', value = 95 },
			{ type = 'fire', value = 20 },
			{ type = 'ice', value = -5 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Sekhaenre', file = 'Cirith/Undeads/Pharaohs/Sekhaenre.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Sekhaenre', 'Sekhaenre'}, experience = 32000, health = 30000, healthMax = 30000,
		elements = {
			{ type = 'physical', value = 25 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 90 },
			{ type = 'earth', value = 100 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 55 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Montuemsaf', file = 'Cirith/Undeads/Pharaohs/Montuemsaf.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Montuemsaf', 'Montuemsaf'}, experience = 31000, health = 27500, healthMax = 27500,
		elements = {
			{ type = 'physical', value = 10 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 25 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = -5 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Wesermontu', file = 'Cirith/Undeads/Pharaohs/Wesermontu.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Wesermontu', 'Wesermontu'}, experience = 38000, health = 50000, healthMax = 50000,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'drown', value = 100 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = 5 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'fire', value = 70 },
			{ type = 'ice', value = 15 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
		{ name = 'Khuiiker', file = 'Cirith/Undeads/Pharaohs/Khuiiker.xml', look = {type = 84}, classId = 11, killAmount = 15, charmPoints = 10,
		description = {'Khuiiker', 'Khuiiker'}, experience = 36000, health = 35000, healthMax = 35000,
		elements = {
			{ type = 'physical', value = 50 },
			{ type = 'bleed', value = 100 },
			{ type = 'holy', value = -15 },
			{ type = 'death', value = 100 },
			{ type = 'energy', value = 60 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 45 },
		},
			loot = {
				{id = 2152, count = 2, chance = 50000}, -- silver coin
				{id = 2148, count = 5, chance = 100000}, -- gold
			}
		},
	
	-- Underwaters
	{ name = 'Crab', level = 3, file = 'Cirith/Underwaters/crab.xml', look = {type = 112}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Krab', 'Crab'}, experience = 25, health = 55, healthMax = 55,
		elements = {
			{ type = 'drown', value = 100 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 1 },
		},
		loot = {
			{id = 2667, chance = 19760}, -- fish
			{id = 11183, chance = 19690}, -- crab pincers
			{id = 2148, count = 3, chance = 80030}, -- gold
		}
	},
	{ name = 'Deepling Guard', level = 110, file = 'Cirith/Underwaters/deepling guard.xml', look = {type = 525}, classId = 3, killAmount = 2500, charmPoints = 50,
		description = {'Obro�ca Dranei', 'Deepling Guard'}, experience = 4350, health = 7000, healthMax = 7000,
		boss = 'Aqualius', chance = 50,
		elements = {
			{ type = 'death', value = 10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 19177, chance = 100}, -- ??? parchment
			{id = 15748, chance = 250}, -- deepling crossbow
			{id = 16189, chance = 130}, -- frosty boots
			{id = 16473, chance = 120}, -- spiky squelcher
			{id = 16270, chance = 310}, -- deepling backpack
			{id = 2427, chance = 190}, -- deepling axe
			{id = 17329, chance = 200}, -- reef blade
			{id = 5895, chance = 890}, -- fish fin
			{id = 16783, chance = 890}, -- deepling claw
			{id = 14242, chance = 510}, -- heavy trident
			{id = 2146, count = 3, chance = 2810}, -- small sapphire
			{id = 13950, chance = 650}, -- shiver spear
			{id = 16784, chance = 8890}, -- deepling filet
			{id = 16775, chance = 11990}, -- deepling guard belt buckle
			{id = 16776, chance = 14980}, -- deepling breaktime snack
			{id = 7591, count = 3, chance = 1000}, -- smp
			{id = 7590, count = 3, chance = 1030}, -- shp
			{id = 2152, count = 2, chance = 41690}, -- silver
			{id = 2148, count = 35, chance = 90160}, -- coin
		}
	},
	{ name = 'Deepling Spellsinger', level = 106, file = 'Cirith/Underwaters/deepling spellsinger.xml', look = {type = 526}, classId = 3, killAmount = 2500, charmPoints = 50,
		description = {'Mag Dranei', 'Deepling Spellsinger'}, experience = 2150, health = 3200, healthMax = 3200,
		boss = 'Aqualius', chance = 50,
		elements = {
			{ type = 'death', value = 50 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 19178, chance = 100}, -- ??? parchment
			{id = 11800, chance = 110}, -- frozen heart backpack
			{id = 15748, chance = 130}, -- deepling crossbow
			{id = {12979, 12980, 12981}, chance = 300}, -- lunar crystal
			{id = 5895, chance = 510}, -- fish fin
			{id = 2138, chance = 790, subType = 90}, -- sapphire amulet
			{id = 8900, chance = 400}, -- sb of enlightenment
			{id = 17329, chance = 300}, -- reef blade
			{id = 2168, chance = 2480}, -- life ring
			{id = 2146, chance = 2940}, -- small sapphire
			{id = 2667, chance = 3540}, -- fish
			{id = 16774, chance = 10760}, -- key
			{id = 16784, chance = 14980}, -- deepling filet
			{id = 16773, chance = 15190}, -- spellsinger seal
			{id = 2152, count = 1, chance = 40160}, -- silver
			{id = 2148, count = 48, chance = 90160}, -- coin
		}
	},
	{ name = 'Deepling Warrior', level = 108, file = 'Cirith/Underwaters/deepling warrior.xml', look = {type = 524}, classId = 3, killAmount = 2500, charmPoints = 50,
		description = {'Wojownik Dranei', 'Deepling Warrior'}, experience = 3500, health = 5200, healthMax = 5200,
		boss = 'Aqualius', chance = 50,
		elements = {
			{ type = 'death', value = 10 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = -10 },
			{ type = 'fire', value = 100 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 19179, chance = 100}, -- ??? parchment
			{id = 16189, chance = 110}, -- frosty boots
			{id = 2002, chance = 50}, -- blue backpack
			{id = 13889, chance = 120}, -- frozen axe
			{id = 16191, chance = 120}, -- frosty armor
			{id = 14486, chance = 130}, -- glacier hood
			{id = 5895, chance = 890}, -- fish fin
			{id = 14242, chance = 180}, -- heavy trident
			{id = 2168, chance = 3020}, -- life ring
			{id = 17329, chance = 110}, -- reef blade
			{id = 2149, chance = 3090}, -- small emerald
			{id = 7590, chance = 990}, -- shp
			{id = 7590, chance = 1050}, -- smp
			{id = 16777, chance = 10010}, -- deepling warts
			{id = 16778, chance = 14950}, -- deeptags
			{id = 16784, chance = 15250}, -- deepling filet
			{id = 2152, chance = 41690}, -- silver
			{id = 2148, count = 49, chance = 90160}, -- coin
		}
	},
	{ name = 'Giant Crab', level = 25, file = 'Cirith/Underwaters/giant crab.xml', look = {type = 200}, classId = 4, killAmount = 500, charmPoints = 15,
		description = {'Wielki Krab', 'Giant Crab'}, experience = 175, health = 290, healthMax = 290,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'drown', value = 100 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 17329, chance = 10}, -- reef blade
			{id = 2143, chance = 490}, -- white pearl
			{id = 14478, chance = 200}, -- berynit helmet
			{id = 2143, chance = 4480}, -- white pearl
			{id = 15751, chance = 5270}, -- brass armor
			{id = 10549, chance = 7000}, -- bloody pincers
			{id = 13752, chance = 6240}, -- bloody pincers
			{id = 2667, chance = 13430}, -- fish
			{id = 2148, count = 11, chance = 86030}, -- gold
		}
	},
	{ name = 'Massive Water Elemental', file = 'Cirith/Underwaters/massive water elemental.xml', look = {type = 11}, classId = 25, killAmount = 1000, charmPoints = 25,
		description = {'Wielki �ywio�ak Lodu', 'Massive Water Elemental'}, experience = 1350, health = 2450, healthMax = 2450,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 100 },
		},
	},
	{ name = 'Roaring Water Elemental', level = 60, file = 'Cirith/Underwaters/roaring water elemental.xml', look = {type = 11}, classId = 25, killAmount = 1000, charmPoints = 25,
		description = {'Rozdra�niony �ywio�ak Lodu', 'Roaring Ice Elemental'}, experience = 1300, health = 2500, healthMax = 2500,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 50 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = -15 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 16191, chance = 10}, -- frosty armor
			{id = 2002, chance = 20}, -- blue backpack
			{id = 13937, chance = 70}, -- icey crossbow
			{id = 14486, chance = 20}, -- glacier hood
			{id = 13894, chance = 10}, -- frosty rapier
			{id = 2656, chance = 150}, -- blue robe
			{id = 8302, chance = 2400}, -- iced soil
			{id = 10220, chance = 500, subType = 5}, -- leviathan amulet
			{id = 2168, chance = 1220}, -- life ring
			{id = 2146, count = 2, chance = 1450}, -- small sapphire
			{id = 7632, chance = 1940}, -- giant pearl
			{id = 7441, chance = 5710}, -- ice cube
			{id = 7839, count = 3, chance = 7220}, -- shiver arrow
			{id = 10577, chance = 11000}, -- frost heart
			{id = 2152, chance = 14680}, -- silver
			{id = 2148, count = 5, chance = 99980}, -- gold
		}
	},
	{ name = 'Slick Water Elemental', level = 60, file = 'Cirith/Underwaters/slick water elemental.xml', look = {type = 286}, classId = 25, killAmount = 1000, charmPoints = 25,
		description = {'Wzmocniony �ywio�ak Lodu', 'Slick Ice Elemental'}, experience = 660, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 40 },
			{ type = 'death', value = 50 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 65 },
			{ type = 'fire', value = 25 },
			{ type = 'ice', value = 100 },
		},
		loot = {
			{id = 2002, chance = 20}, -- blue backpack
			{id = 13894, chance = 20}, -- frosty rapier
			{id = 8302, chance = 1500}, -- iced soil
			{id = 13950, chance = 350}, -- small stone
			{id = 2146, count = 2, chance = 1450}, -- small sapphire
			{id = 7441, chance = 5710}, -- ice cube
			{id = 10220, chance = 250, subType = 5}, -- leviathan amulet
			{id = 7839, count = 3, chance = 7220}, -- shiver arrow
			{id = 10577, chance = 7600}, -- frost heart
			{id = 2152, chance = 14680}, -- silver
			{id = 2148, count = 9, chance = 99980}, -- gold
		}
	},
	{ name = 'Water Elemental', level = 65, file = 'Cirith/Underwaters/water elemental.xml', look = {type = 286}, classId = 25, killAmount = 1000, charmPoints = 25,
		description = {'�ywio�ak Lodu', 'Ice Elemental'}, experience = 1800, health = 3000, healthMax = 3000,
		elements = {
			{ type = 'physical', value = 30 },
			{ type = 'holy', value = 40 },
			{ type = 'death', value = 50 },
			{ type = 'fire', value = 25 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 65 },
			{ type = 'ice', value = 100 },
			{ type = 'drown', value = 100 },
		},
		loot = {
			{id = 2002, chance = 20}, -- blue backpack
			{id = 13894, chance = 50}, -- frosty rapier
			{id = 13950, chance = 350}, -- small stone
			{id = 2146, count = 2, chance = 1450}, -- small sapphire
			{id = 7441, chance = 5710}, -- ice cube
			{id = 7839, count = 3, chance = 7220}, -- shiver arrow
			{id = 10577, chance = 7600}, -- frost heart
			{id = 2152, count = 1, chance = 41690}, -- silver
			{id = 2148, count = 12, chance = 99980}, -- gold
		}
	},
	{ name = 'Abyssal Spirit', level = 65, file = 'Cirith/Underwaters/Abyssal Spirit.xml', look = {type = 635}, classId = 25, killAmount = 2500, charmPoints = 50,
		description = {'Duch Otch�ani', 'Abyssal Spirit'}, experience = 3875, health = 6250, healthMax = 6250,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'bleed', value =  50},
			{ type = 'holy', value =  45},
			{ type = 'death', value =  -10},
			{ type = 'energy', value =  -5},
			{ type = 'earth', value =  -5},
			{ type = 'fire', value =  20},
			{ type = 'ice', value =  80},
		},
		loot = {
			{id = 16481, chance = 80}, -- luck ring
			{id = 16164, chance = 400}, -- black steel shield
			{id = 13164, chance = 410}, -- royal steel edge
			{id = 11621, chance = 210}, -- sphere of frost
			{id = 17692, chance = 30}, -- arydian armor
			{id = 2391, chance = 90}, -- war hammer
			{id = 2656, chance = 780}, -- blue robe
			{id = 5912, chance = 4420}, -- blue piece of cloth
			{id = 15752, chance = 12250}, -- brass legs
			{id = 2146, chance = 13920}, -- small sapphire
			{id = 7632, chance = 2150}, -- giant pearl
			{id = 13952, chance = 790}, -- crystal spear
			{id = 7590, chance = 8560}, -- strong mana potion
			{id = 7159, chance = 3990}, -- green perch
			{id = 14504, chance = 900}, -- magical thread
			{id = 13891, chance = 440}, -- frosty bow
			{id = 7839, count = 7, chance = 12400}, -- shiver arrow
			{id = 13894, chance = 320}, -- frosty rapier
			{id = 11436, count = 5, chance = 7530}, -- crystalline bolt
			{id = 13587, chance = 29650}, -- Essence of water
			{id = 2152, count = 4, chance = 59120}, -- silver coin
			{id = 2148, count = {10, 25}, chance = 83250}, -- copper coin
		}
	},
	
	--  Yuan-ti 
	{ name = 'Warrior Yuan-ti', level = 115, file = 'Cirith/Yuan-ti/warrior yuan-ti.xml', look = {type = 663}, classId = 30, killAmount = 2500, charmPoints = 50,
		description = {'Wojownik Yuan-ti', 'Warrior Yuan-ti'}, experience = 8850, health = 12500, healthMax = 12500,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 13998, chance = 100}, -- master warrior's armor
			{id = 2121, chance = 50}, -- brass ring
			{id = 16356, chance = 10}, -- bag
			{id = 13597, chance = 120}, -- ogre ring
			{id = 11299, chance = 210}, -- drakinata
			{id = 15726, chance = 150}, -- berserker blade
			{id = 13163, chance = 90}, -- steel edge
			{id = 2165, chance = 290}, -- stealth ring
			{id = 13949, chance = 200}, -- golden spear
			{id = 6534, chance = 580}, -- flying trident
			{id = 5892, chance = 2720}, -- steel
			{id = 16158, chance = 410}, -- black steel helmet
			{id = 13797, chance = 690}, -- haunted shield
			{id = 5944, count = 2, chance = 1500}, -- soul orb
			{id = 5878, chance = 3950}, -- hardener leather
			{id = 14010, chance = 1100}, -- knight armor
			{id = 2153, chance = 2520}, -- violet gem
			{id = 15755, chance = 3190}, -- snake helmet
			{id = 15722, chance = 5230}, -- scimitar
			{id = 13948, chance = 6420}, -- hunting spear
			{id = 2177, count = 2, chance = 8650}, -- life crystal
			{id = 2149, count = 3, chance = 8000}, -- small emerald
			{id = 10610, chance = 14800}, -- snake skin
			{id = 8473, count = 3, chance = 1000}, -- UHP
			{id = 2152, count = 9, chance = 41690}, -- silver
			{id = 2148, count = {20, 60}, chance = 99980}, -- gold
		}
	},
	{ name = 'Mage Yuan-ti', level = 115, file = 'Cirith/Yuan-ti/mage yuan-ti.xml', look = {type = 370}, classId = 30, killAmount = 2500, charmPoints = 50,
		description = {'Mag Yuan-ti', 'Mage Yuan-ti'}, experience = 7600, health = 10000, healthMax = 10000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 30 },
			{ type = 'death', value = 5 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 75 },
			{ type = 'fire', value = -5 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 17679, chance = 60}, -- king's helmet
			{id = 16195, chance = 70}, -- druid boots
			{id = 18074, chance = 1}, -- plushy storm bird
			{id = 8918, chance = 510}, -- spellbook of black magic
			{id = 16197, chance = 180}, -- druid cape
			{id = 14460, chance = 250}, -- death wolf staff
			{id = 15771, chance = 190}, -- the magic edge of life
			{id = 17940, chance = 250}, -- pure mithril
			{id = 5809, count = 2, chance = 9500}, -- magical element
			{id = 16269, chance = 110}, -- crystal spike backpack
			{id = 13947, chance = 350}, -- ashbringer robe
			{id = 2123, chance = 380}, -- diamond ring
			{id = 2155, chance = 390}, -- green gem
			{id = 2143, count = 2, chance = 7490}, -- white pearl
			{id = 2164, chance = 900, subType = 100}, -- might ring
			{id = 11368, chance = 980}, --  beetle necklace
			{id = 2172, chance = 850}, -- bronze amulet
			{id = 14172, chance = 60}, -- chest guard
			{id = 16165, chance = 530}, -- swamplair boots
			{id = 13980, chance = 200, subType = 2120}, -- terra amulet
			{id = 2167, chance = 6000}, -- energy ring
			{id = 8902, chance = 2190}, -- sb mind control
			{id = 5878, chance = 3950}, -- hardener leather
			{id = 9810, chance = 3130}, -- rusty armor
			{id = 14456, chance = 2150}, -- oak staff of the earth
			{id = 15742, chance = 8200}, -- sapphire ring
			{id = {2153, 2154, 2155, 2156, 2158}, chance = 9000}, -- gem
			{id = 10610, chance = 16290}, -- snake skin
			{id = 8472, count = {2, 4}, chance = 1240}, -- UMP
			{id = 2152, count = 9, chance = 38230}, -- silver
			{id = 2148, count = {20, 60}, chance = 99180}, -- gold
		}
	},
	{ name = 'Shooter Yuan-ti', level = 115, file = 'Cirith/Yuan-ti/shooter yuan-ti.xml', look = {type = 664}, classId = 30, killAmount = 2500, charmPoints = 50,
		description = {'Strzelec Yuan-ti', 'Shooter Yuan-ti'}, experience = 8300, health = 11300, healthMax = 11300,
		elements = {
			{ type = 'physical', value = 15 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 45 },
			{ type = 'death', value = -5 },
			{ type = 'energy', value = -10 },
			{ type = 'earth', value = 25 },
			{ type = 'fire', value = -10 },
			{ type = 'ice', value = 10 },
		},
		loot = {
			{id = 5803, chance = 110}, -- arbalest
			{id = 8853, chance = 220}, -- siege crossbow
			{id = 13992, chance = 250}, -- bronze plate
			{id = {11802, 11798}, chance = 60}, -- backpack
			{id = 2155, chance = 5000}, -- green gem
			{id = 16143, chance = 750}, -- golden knife
			{id = 7697, chance = 10}, -- signet
			{id = 11694, count = 2, chance = 1420}, -- fine fabric
			{id = 2195, chance = 200}, -- boots of haste
			{id = 12956, chance = 170}, -- marksman helmet
			{id = 13940, chance = 500}, -- crossbow of wrath nature
			{id = 5878, count = 2, chance = 3950}, -- hardener leather
			{id = 14482, chance = 350}, -- antic helmet
			{id = 2168, chance = 1960}, -- life ring
			{id = 13754, count = 2, chance = 3190}, -- rope belt
			{id = 6529, count = {10, 20}, chance = 5280}, -- infernal bolt
			{id = 16138, chance = 120}, -- golden armor
			{id = 4842, chance = 5590}, -- old parchment
			{id = 2127, chance = 3350}, -- emerald bangle
			{id = 2144, count = 3, chance = 6770}, -- black pearl
			{id = 2643, chance = 8670}, -- breastplate
			{id = 10610, chance = 19800}, -- snake skin
			{id = 7588, count = {2, 4}, chance = 1000}, -- SHP
			{id = 7589, count = {2, 4}, chance = 840}, -- SMP
			{id = 13980, chance = 6190, subType = 120}, -- terra amulet
			{id = 2152, count = 9, chance = 38230}, -- silver
			{id = 2455, chance = 61000}, -- crossbow
			{id = 2148, count = {20, 60}, chance = 99180}, -- gold
		}
	},
	{ name = 'Defender Yuan-ti', level = 115, file = 'Cirith/Yuan-ti/defender yuan-ti.xml', look = {type = 665}, classId = 30, killAmount = 2500, charmPoints = 50,
		description = {'Obro�ca Yuan-ti', 'Defender Yuan-ti'}, experience = 13600, health = 15000, healthMax = 15000,
		elements = {
			{ type = 'physical', value = 40 },
			{ type = 'drown', value = 100 },
			{ type = 'holy', value = 30 },
			{ type = 'energy', value = -5 },
			{ type = 'earth', value = 20 },
			{ type = 'ice', value = 15 },
		},
		loot = {
			{id = 2629, chance = 40}, -- lord armor
			{id = 15728, chance = 210}, -- skeleton striking face
			{id = 16173, chance = 50}, -- ruby helmet
			{id = 16207, chance = 120}, -- meteorite armor
			{id = 13794, chance = 140}, -- titan helmet
			{id = 14433, chance = 180}, -- leaden helmet
			{id = 11801, chance = 90}, -- warlord backpack
			{id = 14493, chance = 1250}, -- forbund
			{id = 13299, chance = 1250}, -- glass spear
			{id = 16164, chance = 340}, -- black steel shield
			{id = 14480, chance = 10}, -- spartan helmet
			{id = 5878, count = 2, chance = 3950}, -- hardener leather
			{id = 16473, chance = 240}, -- spiky squelcher
			{id = 16804, chance = 660}, -- book
			{id = 2536, chance = 500}, -- medusa shield
			{id = 14010, chance = 990}, -- knight armor
			{id = 17238, chance = 1250}, --  morning star
			{id = 11326, chance = 5010}, -- high guard's flag
			{id = 12963, chance = 9750}, -- lampart shield
			{id = 10610, chance = 15500}, -- snake skin
			{id = 8472, count = {2, 4}, chance = 1240}, -- UMP
			{id = 2152, count = 9, chance = 48230}, -- silver
			{id = 2146, chance = 54920}, -- small sapphire
			{id = 2148, count = {20, 60}, chance = 99180}, -- gold
		}
	},
	
	-- Ratatosk
	{ name = 'Ratatosk Warrior', level = 28, file = 'Cirith/Ratatosk/Ratatosk Warrior.xml', look = {type = 259}, classId = 35, killAmount = 1000, charmPoints = 25,
		description = {'Wojownik Ratatosk', 'Ratatosk Warrior'}, experience = 950, health = 1730, healthMax = 1730,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'fire', value = -5 },
			{ type = 'holy', value = 5 },
			{ type = 'physical', value = 10 },
			{ type = 'bleed', value = 20 },
			{ type = 'earth', value = 10 },
			{ type = 'ice', value = 55 },
		},
		loot = {
			{id = 16189, chance = 150}, -- frosty boots
			{id = 11203, chance = 700}, -- silky fur
			{id = 2646, chance = 790}, -- platinium shield
			{id = 13894, chance = 790}, -- frosty rapier
			{id = 5880, chance = 1590}, -- iron ore
			{id = 18069, chance = 60}, -- assassin blade
			{id = 13299, chance = 790}, -- glass spear
			{id = 16192, chance = 200}, -- frozen helmet
			{id = 2670, count = 3, chance = 8650}, -- shrimp
			{id = 7618, chance = 7320}, -- health potion
			{id = 2169, chance = 500}, -- time ring
			{id = 17238, chance = 300}, --  morning star
			{id = 2152, chance = 9990}, -- silver coin
			{id = 2148, count = {8, 21}, chance = 92140}, -- copper coin
		}
	},
	{ name = 'Ratatosk Shaman', level = 28, file = 'Cirith/Ratatosk/Ratatosk Shaman.xml', look = {type = 249}, classId = 35, killAmount = 1000, charmPoints = 25,
		description = {'Szaman Ratatosk', 'Ratatosk Shaman'}, experience = 850, health = 1525, healthMax = 1525,
		elements = {
			{ type = 'physical', value = -10 },
			{ type = 'fire', value = -10 },
			{ type = 'energy', value = -10 },
			{ type = 'death', value = -5 },
			{ type = 'holy', value = 5 },
			
			{ type = 'earth', value = 5 },
			{ type = 'ice', value = 30 },
		},
		loot = {
			{id = 17704, chance = 730}, -- northern pike
			{id = 2669, chance = 80}, -- the frosty midnight fire
			{id = 2146, count = 3, chance = 7680}, -- small sapphire
			{id = 2177, chance = 1990}, -- life crystal
			{id = 2157, chance = 990}, -- gold nugget
			{id = 19464, chance = 10}, -- Frostbite amulet
			{id = 13064, chance = 90}, -- strange fish
			{id = 14451, chance = 500}, -- ice cross
			{id = 2670, count = 4, chance = 9240}, -- shrimp
			{id = 11203, chance = 920}, -- silky fur
			{id = 14443, chance = 280}, -- spike shield
			{id = 15733, chance = 200}, -- devastator
			{id = 2152, chance = 12140}, -- silver coin
			{id = 2148, count = {9, 12}, chance = 99180}, -- copper coin
		}
	},
	{ name = 'Ratatosk Fisher', level = 28, file = 'Cirith/Ratatosk/Ratatosk Fisher.xml', look = {type = 260}, classId = 35, killAmount = 1000, charmPoints = 25,
		description = {'W�dkarz Ratatosk', 'Ratatosk Fisher'}, experience = 785, health = 1425, healthMax = 1425,
		elements = {
			{ type = 'energy', value = -10 },
			{ type = 'fire', value = -10 },
			{ type = 'physical', value = -5 },
			{ type = 'bleed', value = 15 },
			{ type = 'earth', value = 25 },
			{ type = 'ice', value = 45 },
		},
		loot = {
			{id = 17701, chance = 220}, -- shark
			{id = 13952, chance = 510}, -- crystal spear
			{id = 14005, chance = 70}, -- wizard's robe
			{id = 7839, count = 7, chance = 12500}, -- shiver arrow
			{id = 17698, chance = 180}, -- koi carp
			{id = 14432, chance = 240}, -- cobalt helmet
			{id = 13982, chance = 1190, subType = 400}, -- glacier amulet
			{id = {7632, 7633}, chance = 490}, --giant pearl
			{id = 11203, chance = 620}, -- silky fur
			{id = 15718, chance = 4250}, -- chopper
			{id = 13584, chance = 12510}, -- primitive spear
			{id = 2670, count = 3, chance = 6130}, -- shrimp
			{id = 5925, chance = 2350}, -- hardened bone
			{id = 2152, chance = 10900}, -- silver coin
			{id = 2148, count = {5, 15}, chance = 99180}, -- copper coin
		}
	},
	
	-- Kynohead
	{ name = 'Kynohead Warrior', level = 28, file = 'Cirith/Kynoheads/Kynohead Warrior.xml', look = {type = 666}, classId = 32, killAmount = 1000, charmPoints = 25,
		description = {'Psiog�owy Wojownik', 'Kynohead Warrior'}, experience = 680, health = 1350, healthMax = 1350,
		elements = {
			{ type = 'physical', value = 5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 10 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 13706, count = 2, chance = 1090}, -- beastly claw
			{id = 2159, count = 3, chance = 110}, -- scarab coin
			{id = 2218, chance = 50}, -- paw amulet
			{id = 14363, chance = 390}, -- steel helmet
			{id = 14348, chance = 900}, -- iron plate
			{id = 18069, chance = 10}, -- assassin blade
			{id = 15723, chance = 10}, -- two handed sword
			{id = 14352, chance = 150}, -- iron blade
			{id = 14325, chance = 320}, -- steel blade
			{id = 5897, chance = 990}, -- wolf paw
			{id = 2666, count = 2, chance = 17680}, -- meat
			{id = 2148, count = 30, chance = 99180}, -- gold
		}
	},
	{ name = 'Kynohead Hunter', level = 30, file = 'Cirith/Kynoheads/Kynohead Hunter.xml', look = {type = 667}, classId = 32, killAmount = 1000, charmPoints = 25,
		description = {'Psiog�owy �owca', 'Kynohead Hunter'}, experience = 650, health = 1250, healthMax = 1250,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'holy', value = -10 },
			{ type = 'death', value = 5 },
			{ type = 'earth', value = -5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -10 },
		},
		loot = {
			{id = 13754, count = 2, chance = 3700}, -- rope belt
			{id = 2159, count = 3, chance = 110}, -- scarab coin
			{id = 7620, count = 2, chance = 880}, -- light mana potion
			{id = 13983, chance = 190, subType = 150}, -- lightning amulet
			{id = 14003, chance = 310}, -- light boots
			{id = 14363, chance = 280}, -- steel helmet
			{id = 7365, count = 12, chance = 2790}, -- onyx arrow
			{id = 7364, count = {2, 15}, chance = 6520}, -- sniper arrow
			{id = 2456, chance = 3240}, -- bow
			{id = 5897, chance = 720}, -- wolf paw
			{id = 2671, chance = 21590}, -- ham
			{id = 2148, count = {5, 18}, chance = 99180}, -- gold
		}
	},
	{ name = 'Kynohead Shaman', level = 25, file = 'Cirith/Kynoheads/Kynohead Shaman.xml', look = {type = 662}, classId = 32, killAmount = 1000, charmPoints = 25,
		description = {'Psiog�owy Czarownik', 'Kynohead Shaman'}, experience = 725, health = 1000, healthMax = 1000,
		elements = {
			{ type = 'physical', value = -5 },
			{ type = 'bleed', value = -5 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 35 },
			{ type = 'earth', value = 10 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 5809, count = 2, chance = 1060}, -- magical element
			{id = 2159, chance = 1500}, -- scarab coin
			{id = 2177, chance = 2650}, -- life crystal
			{id = 13597, chance = 70}, -- ogre ring
			{id = 2319, chance = 500}, -- iron shield
			{id = 14339, chance = 610}, -- iron shield
			{id = 14006, chance = 300}, -- cape of fear
			{id = 13299, chance = 100}, -- glass spear
			{id = 14014, chance = 4890}, --robe
			{id = 14446, chance = 80}, --sapphire rod
			{id = 14455, chance = 120}, --necrotic rod
			{id = 2193, chance = 2420}, -- ankh
			{id = 2682, count = 3, chance = 990}, -- watermelon
			{id = 2148, count = {8, 31}, chance = 99180}, -- gold
		}
	},
	{ name = 'Kynohead Warden', level = 32, file = 'Cirith/Kynoheads/Kynohead Warden.xml', look = {type = 668}, classId = 32, killAmount = 1000, charmPoints = 25,
		description = {'Psiog�owy Stra�nik', 'Kynohead Warden'}, experience = 800, health = 1750, healthMax = 1750,
		elements = {
			{ type = 'physical', value = 20 },
			{ type = 'bleed', value = 10 },
			{ type = 'holy', value = -5 },
			{ type = 'death', value = 20 },
			{ type = 'earth', value = 5 },
			{ type = 'fire', value = 10 },
			{ type = 'ice', value = -5 },
		},
		loot = {
			{id = 5887, chance = 800}, -- piece of royal steel
			{id = 9970, count = 4, chance = 780}, -- small topaz
			{id = 2159, count = 2, chance = 1150}, -- scarab coin
			{id = 7618, count = 3, chance = 780}, -- health potion
			{id = 15780, chance = 5300, subType = 50}, -- tooth necklace
			{id = 13796, chance = 190}, -- silver shield
			{id = 6534, chance = 180}, -- flying trident
			{id = 14320, chance = 110}, -- steel boots
			{id = 14350, chance = 200}, -- iron legs
			{id = 14362, chance = 830}, -- steel armor
			{id = 15716, chance = 720}, -- barbarian axe
			{id = 2446, chance = 40}, -- pharaoh sword
			{id = 15717, chance = 960}, -- glorious axe
			{id = 13888, chance = 500}, -- lightning axe
			{id = 13748, chance = 2150}, -- noble turban
			{id = 2687, count = {2, 12}, chance = 1000}, -- cookie
			{id = 2687, count = {2, 12}, chance = 1000}, -- cookie
			{id = 2152, chance = 2100}, -- silver coin
			{id = 2148, count = {10, 40}, chance = 99180}, -- gold
		}
	},
}
