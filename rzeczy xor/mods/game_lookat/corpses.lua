corpses =
{
	{ id = 3058, clientId = 4240,
		attributes = {
					decayto = 3059,
					duration = 7200,
					decayeffect = 143,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 3059, clientId = 4241,
		attributes = {
					decayto = 3060,
					duration = 7200,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 3060, clientId = 4242,
		attributes = {
					decayto = 0
					}
	},
	
	{ id = 3065, clientId = 4247,
		attributes = {
					decayto = 3066,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 3066, clientId = 4248,
		attributes = {
					decayto = 3060,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 6080, clientId = 4315,
		attributes = {
					decayto = 3132,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 3132, clientId = 4315,
		attributes = {
					decayto = 6082,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 6082, clientId = 6082,
		attributes = {
					decayto = 0
					}
	},
	{ id = 2824, clientId = 4005,
		attributes = {
					decayto = 2825,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					weight = 18000,
					}
	},
	{ id = 2825, clientId = 4006,
		attributes = {
					decayto = 2964,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					weight = 14000,
					}
	},
	{ id = 2964, clientId = 4145,
		attributes = {
					decayto = 0,
					weight = 12000,
					}
	},
	
	{ id = 14328, clientId = 13406,
		attributes = {
					decayto = 14329,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "ogr",
					}
	},
	{ id = 14329, clientId = 13407,
		attributes = {
					decayto = 14330,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "ogr",
					}
	},
	{ id = 14330, clientId = 13408,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14303, clientId = 13381,
		attributes = {
					decayto = 14304,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					}
	},
	{ id = 14304, clientId = 13382,
		attributes = {
					decayto = 2964,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					}
	},
	{ id = 14314, clientId = 13392,
		attributes = {
					decayto = 14315,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14315, clientId = 13393,
		attributes = {
					decayto = 14316,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14316, clientId = 13394,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14311, clientId = 13389,
		attributes = {
					decayto = 14312,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14312, clientId = 13390,
		attributes = {
					decayto = 14313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14313, clientId = 13391,
		attributes = {
					decayto = 0
					}
	},
	{ id = 14308, clientId = 13386,
		attributes = {
					decayto = 14309,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14309, clientId = 13387,
		attributes = {
					decayto = 14310,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14310, clientId = 13388,
		attributes = {
					decayto = 0
					}
	},
	{ id = 14305, clientId = 13383,
		attributes = {
					decayto = 14306,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14306, clientId = 13384,
		attributes = {
					decayto = 14307,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal minotaur",
					}
	},
	{ id = 14307, clientId = 13385,
		attributes = {
					decayto = 0
					}
	},
	{ id = 2806, clientId = 3987,
		attributes = {
					decayto = 2810,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					weight = 60000,
					}
	},
	{ id = 2810, clientId = 4252,
		attributes = {
					decayto = 2811,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					weight = 50000,
					}
	},
	{ id = j, clientId = 864,
		attributes = {
					decayto = 0,
					weight = 30000,
					}
	},
	{ id = 2848, clientId = 4029,
		attributes = {
					decayto = 2885,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					weight = 2000,
					}
	},
	{ id = 2885, clientId = 4066,
		attributes = {
					decayto = 0,
					weight = 800,
					}
	},
	{ id = 2807, clientId = 3988,
		attributes = {
					decayto = 2822,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					weight = 2000,
					}
	},
	{ id = 2822, clientId = 4003,
		attributes = {
					decayto = 2823,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					weight = 1000,
					}
	},
	{ id = 2823, clientId = 4004,
		attributes = {
					decayto = 0,
					weight = 800,
					}
	},
	{ id = 2808, clientId = 3989,
		attributes = {
					decayto = 3117,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 3117, clientId = 4299,
		attributes = {
					decayto = 3118,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 3118, clientId = 4300,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2813, clientId = 3994,
		attributes = {
					decayto = 2814,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					weight = 6300,
					}
	},
	{ id = 2814, clientId = 3995,
		attributes = {
					decayto = 2815,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					weight = 4400,
					}
	},
	{ id = 2815, clientId = 3996,
		attributes = {
					decayto = 0,
					weight = 3000,
					}
	},
	{ id = 2817, clientId = 3998,
		attributes = {
					decayto = 2818,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "snake",
					weight = 1200,
					}
	},
	{ id = 2818, clientId = 3999,
		attributes = {
					decayto = 2819,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "snake",
					weight = 840,
					}
	},
	{ id = 2819, clientId = 4000,
		attributes = {
					decayto = 0,
					weight = 620,
					}
	},
	{ id = 3080, clientId = 4262,
		attributes = {
					decayto = 2821,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 20000,
					}
	},
	{ id = 2821, clientId = 4002,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 16000,
					}
	},
	{ id = 2966, clientId = 4147,
		attributes = {
					decayto = 0,
					weight = 10000,
					}
	},
	{ id = 3086, clientId = 4268,
		attributes = {
					decayto = 3087,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 11950,
					}
	},
	{ id = 3087, clientId = 4269,
		attributes = {
					decayto = 3088,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 11500,
					}
	},
	{ id = 3088, clientId = 4270,
		attributes = {
					decayto = 0,
					weight = 8000,
					}
	},
	{ id = 2826, clientId = 4007,
		attributes = {
					decayto = 2827,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 26000,
					}
	},
	{ id = 2827, clientId = 4008,
		attributes = {
					decayto = 2829,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 20000,
					}
	},
	{ id = 2828, clientId = 4009,
		attributes = {
					decayto = 2829,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 8000,
					}
	},
	{ id = 2829, clientId = 4010,
		attributes = {
					decayto = 0,
					weight = 4000,
					}
	},
	{ id = 2835, clientId = 4016,
		attributes = {
					decayto = 2836,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deer",
					weight = 6800,
					}
	},
	{ id = 2836, clientId = 4017,
		attributes = {
					decayto = 2837,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deer",
					weight = 6800,
					}
	},
	{ id = 2837, clientId = 4018,
		attributes = {
					decayto = 0,
					weight = 5000,
					}
	},
	{ id = 2843, clientId = 4024,
		attributes = {
					decayto = 2975,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 20000,
					}
	},
	{ id = 2975, clientId = 4156,
		attributes = {
					decayto = 2976,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 10000,
					}
	},
	{ id = 2976, clientId = 4157,
		attributes = {
					decayto = 0,
					weight = 10000,
					}
	},
	{ id = 2928, clientId = 4109,
		attributes = {
					decayto = 2929,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 20000,
					}
	},
	{ id = 2929, clientId = 4110,
		attributes = {
					decayto = 2997,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 10000,
					}
	},
	{ id = 2997, clientId = 4111,
		attributes = {
					decayto = 0,
					weight = 10000,
					}
	},
	{ id = 3104, clientId = 4286,
		attributes = {
					decayto = 3105,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 3105, clientId = 4287,
		attributes = {
					decayto = 3106,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 3106, clientId = 4288,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2849, clientId = 4030,
		attributes = {
					decayto = 2850,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 2850, clientId = 4031,
		attributes = {
					decayto = 2851,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 2851, clientId = 4032,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3113, clientId = 4295,
		attributes = {
					decayto = 3114,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghoul",
					weight = 60000,
					}
	},
	{ id = 3114, clientId = 4296,
		attributes = {
					decayto = 3115,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghoul",
					weight = 50000,
					}
	},
	{ id = 3115, clientId = 4297,
		attributes = {
					decayto = 0,
					weight = 40000,
					}
	},
	{ id = 2857, clientId = 4038,
		attributes = {
					decayto = 2858,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 2858, clientId = 4039,
		attributes = {
					decayto = 2859,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 2859, clientId = 4040,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2860, clientId = 4041,
		attributes = {
					decayto = 2861,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2861, clientId = 4042,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2862, clientId = 4043,
		attributes = {
					decayto = 2863,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2863, clientId = 4044,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2864, clientId = 4045,
		attributes = {
					decayto = 2865,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2865, clientId = 4046,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2881, clientId = 4062,
		attributes = {
					decayto = 2882,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 2882, clientId = 4063,
		attributes = {
					decayto = 2883,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 2883, clientId = 4064,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2886, clientId = 4067,
		attributes = {
					decayto = 2887,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "devil",
					weight = 100000,
					}
	},
	{ id = 2887, clientId = 4068,
		attributes = {
					decayto = 2888,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "devil",
					weight = 100000,
					}
	},
	{ id = 2888, clientId = 4069,
		attributes = {
					decayto = 0,
					weight = 100000,
					}
	},
	{ id = 14372, clientId = 13450,
		attributes = {
					decayto = 14373,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "devil",
					weight = 100000,
					}
	},
	{ id = 14373, clientId = 13451,
		attributes = {
					decayto = 14374,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "devil",
					weight = 100000,
					}
	},
	{ id = 14374, clientId = 13452,
		attributes = {
					decayto = 0,
					weight = 100000,
					}
	},
	{ id = 2889, clientId = 4070,
		attributes = {
					decayto = 2890,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lion",
					}
	},
	{ id = 2890, clientId = 4071,
		attributes = {
					decayto = 2891,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lion",
					}
	},
	{ id = 2891, clientId = 4072,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2893, clientId = 4074,
		attributes = {
					decayto = 2894,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 2894, clientId = 4075,
		attributes = {
					decayto = 2895,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 2895, clientId = 4076,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2897, clientId = 4078,
		attributes = {
					decayto = 2898,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scorpion",
					weight = 1000,
					}
	},
	{ id = 2898, clientId = 4079,
		attributes = {
					decayto = 2965,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scorpion",
					weight = 1000,
					}
	},
	{ id = 2965, clientId = 4146,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 2899, clientId = 4080,
		attributes = {
					decayto = 2900,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "wasp",
					weight = 1000,
					}
	},
	{ id = 2900, clientId = 4081,
		attributes = {
					decayto = 2901,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "wasp",
					weight = 1000,
					}
	},
	{ id = 2901, clientId = 4082,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 18508, clientId = 17586,
		attributes = {
					decayto = 18509,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "wasp",
					weight = 1000,
					}
	},
	{ id = 18509, clientId = 17587,
		attributes = {
					decayto = 18510,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "wasp",
					weight = 1000,
					}
	},
	{ id = 18510, clientId = 17588,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 2902, clientId = 4083,
		attributes = {
					decayto = 2903,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bug",
					weight = 1000,
					}
	},
	{ id = 2903, clientId = 4084,
		attributes = {
					decayto = 2904,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bug",
					weight = 1000,
					}
	},
	{ id = 2904, clientId = 4085,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 2905, clientId = 4086,
		attributes = {
					decayto = 2906,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "sheep",
					weight = 10000,
					}
	},
	{ id = 2906, clientId = 4087,
		attributes = {
					decayto = 2907,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "sheep",
					weight = 10000,
					}
	},
	{ id = 2914, clientId = 4095,
		attributes = {
					decayto = 2915,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "sheep",
					weight = 10000,
					}
	},
	{ id = 2915, clientId = 4096,
		attributes = {
					decayto = 2907,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "sheep",
					weight = 10000,
					}
	},
	{ id = 2907, clientId = 4088,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 2908, clientId = 4089,
		attributes = {
					decayto = 2909,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					}
	},
	{ id = 2909, clientId = 4090,
		attributes = {
					decayto = 2910,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					}
	},
	{ id = 2910, clientId = 4091,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 19560, clientId = 18638,
		attributes = {
					decayto = 19561,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					}
	},
	{ id = 19561, clientId = 18639,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3108, clientId = 4290,
		attributes = {
					decayto = 2977,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 1000,
					}
	},
	{ id = 2977, clientId = 4158,
		attributes = {
					decayto = 2978,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 1000,
					}
	},
	{ id = 2978, clientId = 4159,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 2916, clientId = 4097,
		attributes = {
					decayto = 2917,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "demon",
					}
	},
	{ id = 2917, clientId = 4098,
		attributes = {
					decayto = 2918,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "demon",
					}
	},
	{ id = 2918, clientId = 4099,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2920, clientId = 4101,
		attributes = {
					decayto = 2921,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 90000,
					}
	},
	{ id = 2921, clientId = 4102,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 90000,
					}
	},
	{ id = 2809, clientId = 3990,
		attributes = {
					decayto = 2922,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 60000,
					}
	},
	{ id = 2922, clientId = 4103,
		attributes = {
					decayto = 2923,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 60000,
					}
	},
	{ id = 2923, clientId = 4104,
		attributes = {
					decayto = 0,
					weight = 60000,
					}
	},
	{ id = 13145, clientId = 12223,
		attributes = {
					decayto = 13146,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 60000,
					}
	},
	{ id = 13146, clientId = 12224,
		attributes = {
					decayto = 13147,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 60000,
					}
	},
	{ id = 13147, clientId = 12225,
		attributes = {
					decayto = 0,
					weight = 60000,
					}
	},
	{ id = 2924, clientId = 4105,
		attributes = {
					decayto = 2925,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 8000,
					}
	},
	{ id = 2925, clientId = 4106,
		attributes = {
					decayto = 2926,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					weight = 8000,
					}
	},
	{ id = 2926, clientId = 4107,
		attributes = {
					decayto = 0,
					weight = 8000,
					}
	},
	{ id = 2931, clientId = 4112,
		attributes = {
					decayto = 2932,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "behemoth",
					}
	},
	{ id = 2932, clientId = 4113,
		attributes = {
					decayto = 2933,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "behemoth",
					}
	},
	{ id = 2933, clientId = 4114,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2935, clientId = 4116,
		attributes = {
					decayto = 2936,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "pig",
					weight = 8000,
					}
	},
	{ id = 2936, clientId = 4117,
		attributes = {
					decayto = 2937,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "pig",
					weight = 7000,
					}
	},
	{ id = 2937, clientId = 4118,
		attributes = {
					decayto = 0,
					weight = 6000,
					}
	},
	{ id = 2938, clientId = 4119,
		attributes = {
					decayto = 2939,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2939, clientId = 4120,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2940, clientId = 4121,
		attributes = {
					decayto = 2941,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "goblin",
					weight = 4000,
					}
	},
	{ id = 2941, clientId = 4122,
		attributes = {
					decayto = 2942,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "goblin",
					weight = 4000,
					}
	},
	{ id = 2942, clientId = 4123,
		attributes = {
					decayto = 0,
					weight = 4000,
					}
	},
	{ id = 2945, clientId = 4126,
		attributes = {
					decayto = 2946,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					weight = 100000,
					}
	},
	{ id = 2946, clientId = 4127,
		attributes = {
					decayto = 2947,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					weight = 100000,
					}
	},
	{ id = 2947, clientId = 4128,
		attributes = {
					decayto = 0,
					weight = 60000,
					}
	},
	{ id = 2949, clientId = 144,
		attributes = {
					decayto = 2950,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "mummy",
					weight = 20000,
					}
	},
	{ id = 2950, clientId = 4131,
		attributes = {
					decayto = 2951,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "mummy",
					weight = 20000,
					}
	},
	{ id = 2951, clientId = 4132,
		attributes = {
					decayto = 0,
					weight = 15000,
					}
	},
	{ id = 2952, clientId = 4133,
		attributes = {
					decayto = 2953,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "stone golem",
					}
	},
	{ id = 2953, clientId = 4134,
		attributes = {
					decayto = 2954,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "stone golem",
					}
	},
	{ id = 2954, clientId = 4135,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2956, clientId = 4137,
		attributes = {
					decayto = 2957,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					weight = 100000,
					}
	},
	{ id = 2957, clientId = 4138,
		attributes = {
					decayto = 2958,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					weight = 100000,
					}
	},
	{ id = 2958, clientId = 4139,
		attributes = {
					decayto = 0,
					weight = 100000,
					}
	},
	{ id = 2960, clientId = 4141,
		attributes = {
					decayto = 2961,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2961, clientId = 4142,
		attributes = {
					decayto = 2962,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2962, clientId = 4143,
		attributes = {
					decayto = 0,
					weight = 60000,
					}
	},
	{ id = 2967, clientId = 4148,
		attributes = {
					decayto = 2968,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2968, clientId = 4149,
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					weight = 120000,
					}
	},
	{ id = 2969, clientId = 4150,
		attributes = {
					decayto = 2970,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					}
	},
	{ id = 2970, clientId = 4151,
		attributes = {
					decayto = 2971,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					}
	},
	{ id = 2971, clientId = 4152,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2972, clientId = 4153,
		attributes = {
					decayto = 2973,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 2973, clientId = 4154,
		attributes = {
					decayto = 2974,
					corpsetype = "blood",
					bloodtype = "orc",
					bloodtype = "orc",
					}
	},
	{ id = 2974, clientId = 4155,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2979, clientId = 4160,
		attributes = {
					decayto = 2980,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					weight = 100000,
					}
	},
	{ id = 2980, clientId = 4161,
		attributes = {
					decayto = 2947,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					weight = 100000,
					}
	},
	{ id = 2981, clientId = 4162,
		attributes = {
					decayto = 2982,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					weight = 100000,
					}
	},
	{ id = 2982, clientId = 4163,
		attributes = {
					decayto = 2947,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					weight = 100000,
					}
	},
	{ id = 2983, clientId = 4164,
		attributes = {
					decayto = 2984,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2984, clientId = 4165,
		attributes = {
					decayto = 2962,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2985, clientId = 4166,
		attributes = {
					decayto = 2986,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2986, clientId = 4167,
		attributes = {
					decayto = 2962,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2987, clientId = 4168,
		attributes = {
					decayto = 2988,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 2988, clientId = 4169,
		attributes = {
					decayto = 2962,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dwarf",
					weight = 60000,
					}
	},
	{ id = 3001, clientId = 4182,
		attributes = {
					decayto = 3002,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 3002, clientId = 4183,
		attributes = {
					decayto = 2991,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 2989, clientId = 4170,
		attributes = {
					decayto = 2990,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 2990, clientId = 4171,
		attributes = {
					decayto = 2991,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 2991, clientId = 4172,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3119, clientId = 4301,
		attributes = {
					decayto = 3120,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rabbit",
					weight = 1000,
					}
	},
	{ id = 3120, clientId = 4302,
		attributes = {
					decayto = 3121,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rabbit",
					weight = 1000,
					}
	},
	{ id = 3121, clientId = 4303,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 2998, clientId = 4179,
		attributes = {
					decayto = 2999,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "banshee",
					weight = 8000,
					}
	},
	{ id = 2999, clientId = 4180,
		attributes = {
					decayto = 0,
					weight = 8000,
					}
	},
	{ id = 3004, clientId = 4185,
		attributes = {
					decayto = 3005,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scarab",
					}
	},
	{ id = 3005, clientId = 4186,
		attributes = {
					decayto = 3006,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scarab",
					}
	},
	{ id = 3006, clientId = 4187,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3007, clientId = 4188,
		attributes = {
					decayto = 3008,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "cobra",
					weight = 6000,
					}
	},
	{ id = 3008, clientId = 4189,
		attributes = {
					decayto = 3009,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "cobra",
					weight = 6000,
					}
	},
	{ id = 3009, clientId = 4190,
		attributes = {
					decayto = 0,
					weight = 6000,
					}
	},
	{ id = 3010, clientId = 4191,
		attributes = {
					decayto = 3011,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "larva",
					weight = 1000,
					}
	},
	{ id = 3011, clientId = 4192,
		attributes = {
					decayto = 3012,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "larva",
					weight = 1000,
					}
	},
	{ id = 3012, clientId = 4193,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 3013, clientId = 4194,
		attributes = {
					decayto = 3014,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scarab",
					weight = 2000,
					}
	},
	{ id = 3014, clientId = 4195,
		attributes = {
					decayto = 3015,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scarab",
					weight = 2000,
					}
	},
	{ id = 3015, clientId = 4196,
		attributes = {
					decayto = 0,
					weight = 2000,
					}
	},
	{ id = 3016, clientId = 4197,
		attributes = {
					decayto = 3017,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "pharaoh",
					}
	},
	{ id = 3017, clientId = 4198,
		attributes = {
					decayto = 3018,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "pharaoh",
					}
	},
	{ id = 3018, clientId = 4199,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3034, clientId = 4215,
		attributes = {
					decayto = 3035,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "pharaoh",
					}
	},
	{ id = 3035, clientId = 4216,
		attributes = {
					decayto = 3036,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "pharaoh",
					}
	},
	{ id = 3036, clientId = 4217,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3019, clientId = 4200,
		attributes = {
					decayto = 3020,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hyaena",
					weight = 8000,
					}
	},
	{ id = 3020, clientId = 4201,
		attributes = {
					decayto = 3021,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hyaena",
					weight = 8000,
					}
	},
	{ id = 3021, clientId = 4202,
		attributes = {
					decayto = 0,
					weight = 8000,
					}
	},
	{ id = 3022, clientId = 4203,
		attributes = {
					decayto = 3023,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "gargoyle",
					}
	},
	{ id = 3023, clientId = 4204,
		attributes = {
					decayto = 3024,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "gargoyle",
					}
	},
	{ id = 3024, clientId = 4205,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3025, clientId = 4206,
		attributes = {
					decayto = 3026,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "lich",
					}
	},
	{ id = 3026, clientId = 4207,
		attributes = {
					decayto = 3027,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "lich",
					}
	},
	{ id = 3027, clientId = 4208,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3028, clientId = 4209,
		attributes = {
					decayto = 3029,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "zombie",
					weight = 100000,
					}
	},
	{ id = 3029, clientId = 4210,
		attributes = {
					decayto = 3030,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "zombie",
					weight = 100000,
					}
	},
	{ id = 3030, clientId = 4211,
		attributes = {
					decayto = 0,
					weight = 100000,
					}
	},
	{ id = 3031, clientId = 4212,
		attributes = {
					decayto = 3032,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "bonebeast",
					}
	},
	{ id = 3032, clientId = 4213,
		attributes = {
					decayto = 3033,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "bonebeast",
					}
	},
	{ id = 3033, clientId = 4214,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3046, clientId = 4227,
		attributes = {
					decayto = 3047,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					weight = 1000,
					}
	},
	{ id = 3047, clientId = 4228,
		attributes = {
					decayto = 3048,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					weight = 1000,
					}
	},
	{ id = 3048, clientId = 4229,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 3049, clientId = 4230,
		attributes = {
					decayto = 3050,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					weight = 1000,
					}
	},
	{ id = 3050, clientId = 4231,
		attributes = {
					decayto = 3051,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					weight = 1000,
					}
	},
	{ id = 3051, clientId = 4232,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 3052, clientId = 4233,
		attributes = {
					decayto = 3053,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					}
	},
	{ id = 3053, clientId = 4234,
		attributes = {
					decayto = 3054,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "bonelord",
					}
	},
	{ id = 3054, clientId = 4235,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3055, clientId = 4236,
		attributes = {
					decayto = 3056,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yeti",
					}
	},
	{ id = 3056, clientId = 4237,
		attributes = {
					decayto = 3057,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yeti",
					}
	},
	{ id = 3057, clientId = 4238,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 3073, clientId = 4255,
		attributes = {
					decayto = 2813,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					weight = 6800,
					}
	},
	{ id = 3074, clientId = 4256,
		attributes = {
					decayto = 3075,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					weight = 6800,
					}
	},
	{ id = 3075, clientId = 4257,
		attributes = {
					decayto = 0,
					weight = 6000,
					}
	},
	{ id = 3077, clientId = 4259,
		attributes = {
					decayto = 3078,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "snake",
					weight = 1200,
					}
	},
	{ id = 3078, clientId = 4260,
		attributes = {
					decayto = 3079,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "snake",
					weight = 1000,
					}
	},
	{ id = 3079, clientId = 4261,
		attributes = {
					decayto = 0,
					weight = 800,
					}
	},
	{ id = 3095, clientId = 4277,
		attributes = {
					decayto = 3096,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deer",
					weight = 25000,
					}
	},
	{ id = 3096, clientId = 4278,
		attributes = {
					decayto = 3097,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deer",
					weight = 25000,
					}
	},
	{ id = 3097, clientId = 4279,
		attributes = {
					decayto = 0,
					weight = 21000,
					}
	},
	{ id = 3109, clientId = 4291,
		attributes = {
					decayto = 3110,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 3110, clientId = 4292,
		attributes = {
					decayto = 3111,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 3111, clientId = 4293,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4253, clientId = 4318,
		attributes = {
					decayto = 4254,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crab",
					weight = 500,
					}
	},
	{ id = 4254, clientId = 4319,
		attributes = {
					decayto = 4255,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crab",
					weight = 500,
					}
	},
	{ id = 4255, clientId = 4320,
		attributes = {
					decayto = 0,
					weight = 300,
					}
	},
	{ id = 4256, clientId = 4321,
		attributes = {
					decayto = 4257,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 4257, clientId = 4322,
		attributes = {
					decayto = 4258,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 4258, clientId = 4323,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4259, clientId = 4324,
		attributes = {
					decayto = 4260,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 4260, clientId = 4325,
		attributes = {
					decayto = 4261,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 4261, clientId = 4326,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4262, clientId = 4327,
		attributes = {
					decayto = 4263,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 4263, clientId = 4328,
		attributes = {
					decayto = 4264,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 4264, clientId = 4329,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4265, clientId = 4330,
		attributes = {
					decayto = 4266,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chicken",
					weight = 500,
					}
	},
	{ id = 4266, clientId = 4331,
		attributes = {
					decayto = 4267,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chicken",
					weight = 500,
					}
	},
	{ id = 4267, clientId = 4332,
		attributes = {
					decayto = 0,
					weight = 300,
					}
	},
	{ id = 4268, clientId = 4333,
		attributes = {
					decayto = 4269,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "monkey",
					}
	},
	{ id = 4269, clientId = 4334,
		attributes = {
					decayto = 4270,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "monkey",
					}
	},
	{ id = 4270, clientId = 4335,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4271, clientId = 4336,
		attributes = {
					decayto = 4272,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "monkey",
					}
	},
	{ id = 4272, clientId = 4337,
		attributes = {
					decayto = 4273,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "monkey",
					}
	},
	{ id = 4273, clientId = 4338,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4274, clientId = 4339,
		attributes = {
					decayto = 4275,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "monkey",
					}
	},
	{ id = 4275, clientId = 4340,
		attributes = {
					decayto = 4276,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "monkey",
					}
	},
	{ id = 4276, clientId = 4341,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4277, clientId = 4342,
		attributes = {
					decayto = 4278,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crocodile",
					}
	},
	{ id = 4278, clientId = 4343,
		attributes = {
					decayto = 4279,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crocodile",
					}
	},
	{ id = 4279, clientId = 4344,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4280, clientId = 4345,
		attributes = {
					decayto = 4281,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "carniphila",
					}
	},
	{ id = 4281, clientId = 4346,
		attributes = {
					decayto = 4282,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "carniphila",
					}
	},
	{ id = 4282, clientId = 4347,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4283, clientId = 4348,
		attributes = {
					decayto = 4284,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 4284, clientId = 4349,
		attributes = {
					decayto = 4285,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 4285, clientId = 4350,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4286, clientId = 4351,
		attributes = {
					decayto = 4287,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "panda",
					}
	},
	{ id = 4287, clientId = 4352,
		attributes = {
					decayto = 4288,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "panda",
					}
	},
	{ id = 4288, clientId = 4353,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4289, clientId = 4354,
		attributes = {
					decayto = 4290,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "centipede",
					}
	},
	{ id = 4290, clientId = 4355,
		attributes = {
					decayto = 4291,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "centipede",
					}
	},
	{ id = 4291, clientId = 4356,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4292, clientId = 4357,
		attributes = {
					decayto = 4293,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "tiger",
					}
	},
	{ id = 4293, clientId = 4358,
		attributes = {
					decayto = 4294,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "tiger",
					}
	},
	{ id = 4294, clientId = 4359,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4295, clientId = 4360,
		attributes = {
					decayto = 4296,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elephant",
					}
	},
	{ id = 4296, clientId = 4361,
		attributes = {
					decayto = 4297,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elephant",
					}
	},
	{ id = 4297, clientId = 4362,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4298, clientId = 4363,
		attributes = {
					decayto = 4299,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bat",
					weight = 500,
					}
	},
	{ id = 4299, clientId = 4364,
		attributes = {
					decayto = 4300,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bat",
					weight = 400,
					}
	},
	{ id = 4300, clientId = 4365,
		attributes = {
					decayto = 0,
					weight = 300,
					}
	},
	{ id = 4301, clientId = 4366,
		attributes = {
					decayto = 4302,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "flamingoo",
					}
	},
	{ id = 4302, clientId = 4367,
		attributes = {
					decayto = 4303,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "flamingoo",
					}
	},
	{ id = 4303, clientId = 4368,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4304, clientId = 4369,
		attributes = {
					decayto = 4305,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dworc",
					}
	},
	{ id = 4305, clientId = 4370,
		attributes = {
					decayto = 4306,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dworc",
					}
	},
	{ id = 4306, clientId = 4371,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4307, clientId = 4372,
		attributes = {
					decayto = 4308,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dworc",
					}
	},
	{ id = 4308, clientId = 4373,
		attributes = {
					decayto = 4309,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dworc",
					}
	},
	{ id = 4309, clientId = 4374,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4310, clientId = 4375,
		attributes = {
					decayto = 4311,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dworc",
					}
	},
	{ id = 4311, clientId = 4376,
		attributes = {
					decayto = 4312,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dworc",
					}
	},
	{ id = 4312, clientId = 4377,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4992, clientId = 4991,
		attributes = {
					decayto = 4313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "butterfly",
					weight = 10,
					}
	},
	{ id = 4993, clientId = 4992,
		attributes = {
					decayto = 4313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "butterfly",
					weight = 10,
					}
	},
	{ id = 4994, clientId = 4993,
		attributes = {
					decayto = 4313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "butterfly",
					weight = 10,
					}
	},
	{ id = 5014, clientId = 5013,
		attributes = {
					decayto = 4313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "butterfly",
					weight = 10,
					}
	},
	{ id = 4313, clientId = 4378,
		attributes = {
					decayto = 0,
					weight = 10,
					}
	},
	{ id = 4314, clientId = 4379,
		attributes = {
					decayto = 4315,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "parrot",
					weight = 2000,
					}
	},
	{ id = 4315, clientId = 4380,
		attributes = {
					decayto = 4316,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "parrot",
					weight = 1500,
					}
	},
	{ id = 4316, clientId = 4381,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 4317, clientId = 4382,
		attributes = {
					decayto = 4318,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bird",
					}
	},
	{ id = 4318, clientId = 4383,
		attributes = {
					decayto = 4319,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bird",
					}
	},
	{ id = 4319, clientId = 4384,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4320, clientId = 4385,
		attributes = {
					decayto = 4321,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "tarantula",
					}
	},
	{ id = 4321, clientId = 4386,
		attributes = {
					decayto = 4322,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "tarantula",
					}
	},
	{ id = 4322, clientId = 4387,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4323, clientId = 4388,
		attributes = {
					decayto = 4324,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "serpent",
					}
	},
	{ id = 4324, clientId = 4389,
		attributes = {
					decayto = 4325,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "serpent",
					}
	},
	{ id = 4325, clientId = 4390,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 4326, clientId = 4391,
		attributes = {
					decayto = 4327,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "nettle",
					}
	},
	{ id = 4327, clientId = 4392,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5522, clientId = 5521,
		attributes = {
					decayto = 5537,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5523, clientId = 5522,
		attributes = {
					decayto = 5533,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5524, clientId = 5523,
		attributes = {
					decayto = 5531,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5525, clientId = 5524,
		attributes = {
					decayto = 5529,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5526, clientId = 5525,
		attributes = {
					decayto = 5535,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5527, clientId = 5526,
		attributes = {
					decayto = 5528,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "dust",
					}
	},
	{ id = 5528, clientId = 5527,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5529, clientId = 5528,
		attributes = {
					decayto = 5530,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5530, clientId = 5529,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5531, clientId = 5530,
		attributes = {
					decayto = 5532,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5532, clientId = 5531,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5533, clientId = 5532,
		attributes = {
					decayto = 5534,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5534, clientId = 5533,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5535, clientId = 5534,
		attributes = {
					decayto = 5536,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5536, clientId = 5535,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5537, clientId = 5536,
		attributes = {
					decayto = 5538,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "quara",
					}
	},
	{ id = 5538, clientId = 5537,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5540, clientId = 5539,
		attributes = {
					decayto = 5541,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					}
	},
	{ id = 5541, clientId = 5540,
		attributes = {
					decayto = 5542,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					}
	},
	{ id = 5542, clientId = 5541,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5565, clientId = 5564,
		attributes = {
					decayto = 0,
					weight = 40000,
					}
	},
	{ id = 5566, clientId = 5565,
		attributes = {
					decayto = 5567,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 20000,
					}
	},
	{ id = 5567, clientId = 5566,
		attributes = {
					decayto = 5568,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeleton",
					weight = 20000,
					}
	},
	{ id = 5568, clientId = 5567,
		attributes = {
					decayto = 0,
					weight = 20000,
					}
	},
	{ id = 5625, clientId = 5624,
		attributes = {
					decayto = 5626,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "tortoise",
					weight = 15000,
					}
	},
	{ id = 5626, clientId = 5625,
		attributes = {
					decayto = 5627,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "tortoise",
					weight = 15000,
					}
	},
	{ id = 5627, clientId = 5626,
		attributes = {
					decayto = 0,
					weight = 15000,
					}
	},
	{ id = 5666, clientId = 5665,
		attributes = {
					decayto = 5667,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mammoth",
					}
	},
	{ id = 5667, clientId = 5666,
		attributes = {
					decayto = 5668,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mammoth",
					}
	},
	{ id = 5668, clientId = 5667,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5688, clientId = 5688,
		attributes = {
					decayto = 5689,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crab",
					}
	},
	{ id = 5689, clientId = 5689,
		attributes = {
					decayto = 5690,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crab",
					}
	},
	{ id = 5690, clientId = 5690,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5727, clientId = 5727,
		attributes = {
					decayto = 5728,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "seagull",
					weight = 1000,
					}
	},
	{ id = 5728, clientId = 5728,
		attributes = {
					decayto = 5729,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "seagull",
					weight = 800,
					}
	},
	{ id = 5729, clientId = 5729,
		attributes = {
					decayto = 0,
					weight = 500,
					}
	},
	{ id = 5765, clientId = 5765,
		attributes = {
					decayto = 5766,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "toad",
					}
	},
	{ id = 5766, clientId = 5766,
		attributes = {
					decayto = 5767,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "toad",
					}
	},
	{ id = 5767, clientId = 5767,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5931, clientId = 5931,
		attributes = {
					decayto = 5932,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "ferumbras",
					}
	},
	{ id = 5932, clientId = 5932,
		attributes = {
					decayto = 5933,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "ferumbras",
					}
	},
	{ id = 5933, clientId = 5933,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 5934, clientId = 5934,
		attributes = {
					decayto = 5935,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "frog",
					weight = 1000,
					}
	},
	{ id = 5935, clientId = 5935,
		attributes = {
					decayto = 5936,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "frog",
					weight = 800,
					}
	},
	{ id = 5936, clientId = 5936,
		attributes = {
					decayto = 0,
					weight = 500,
					}
	},
	{ id = 6307, clientId = 6306,
		attributes = {
					decayto = 6308,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "undead dragon",
					}
	},
	{ id = 6308, clientId = 6307,
		attributes = {
					decayto = 6309,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "undead dragon",
					}
	},
	{ id = 6309, clientId = 6308,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6310, clientId = 6309,
		attributes = {
					decayto = 6319,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "lost soul",
					}
	},
	{ id = 6319, clientId = 6318,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6313, clientId = 6312,
		attributes = {
					decayto = 6314,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "lost soul",
					}
	},
	{ id = 6314, clientId = 6313,
		attributes = {
					decayto = 6315,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "lost soul",
					}
	},
	{ id = 6315, clientId = 6314,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6317, clientId = 6316,
		attributes = {
					decayto = 6318,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "lost soul",
					}
	},
	{ id = 6318, clientId = 6317,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6321, clientId = 6320,
		attributes = {
					decayto = 6322,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "destroyer",
					}
	},
	{ id = 6322, clientId = 6321,
		attributes = {
					decayto = 6323,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "destroyer",
					}
	},
	{ id = 6323, clientId = 6322,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6325, clientId = 6324,
		attributes = {
					decayto = 6326,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "hellfire",
					}
	},
	{ id = 6326, clientId = 6325,
		attributes = {
					decayto = 6327,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "hellfire",
					}
	},
	{ id = 6327, clientId = 6326,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6328, clientId = 6327,
		attributes = {
					decayto = 6329,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "torturer",
					}
	},
	{ id = 6329, clientId = 6328,
		attributes = {
					decayto = 6330,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "torturer",
					}
	},
	{ id = 6330, clientId = 6329,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6333, clientId = 6332,
		attributes = {
					decayto = 6334,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hellhound",
					}
	},
	{ id = 6334, clientId = 6333,
		attributes = {
					decayto = 6335,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hellhound",
					}
	},
	{ id = 6335, clientId = 6334,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6337, clientId = 6336,
		attributes = {
					decayto = 6338,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "juggernaut",
					}
	},
	{ id = 6338, clientId = 6337,
		attributes = {
					decayto = 6339,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "juggernaut",
					}
	},
	{ id = 6339, clientId = 6338,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6341, clientId = 6340,
		attributes = {
					decayto = 6342,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightmare",
					}
	},
	{ id = 6342, clientId = 6341,
		attributes = {
					decayto = 6343,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightmare",
					}
	},
	{ id = 6343, clientId = 6342,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6345, clientId = 6344,
		attributes = {
					decayto = 6346,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 10000,
					}
	},
	{ id = 6346, clientId = 6345,
		attributes = {
					decayto = 6347,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 8000,
					}
	},
	{ id = 6347, clientId = 6346,
		attributes = {
					decayto = 0,
					weight = 6000,
					}
	},
	{ id = 6349, clientId = 6348,
		attributes = {
					decayto = 6350,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 8000,
					}
	},
	{ id = 6350, clientId = 6349,
		attributes = {
					decayto = 6351,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 6000,
					}
	},
	{ id = 6351, clientId = 6350,
		attributes = {
					decayto = 0,
					weight = 4000,
					}
	},
	{ id = 6355, clientId = 6354,
		attributes = {
					decayto = 0,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "ghost",
					weight = 10000,
					}
	},
	{ id = 6365, clientId = 6364,
		attributes = {
					decayto = 6366,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "imp",
					}
	},
	{ id = 6366, clientId = 6365,
		attributes = {
					decayto = 6367,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "imp",
					}
	},
	{ id = 6367, clientId = 6366,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6520, clientId = 6519,
		attributes = {
					decayto = 6521,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "plaguesmith",
					}
	},
	{ id = 6521, clientId = 6520,
		attributes = {
					decayto = 6522,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "plaguesmith",
					}
	},
	{ id = 6522, clientId = 6521,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 6552, clientId = 6552,
		attributes = {
					corpsetype = "venom",
					decayto = 0,
					}
	},
	{ id = 7092, clientId = 7092,
		attributes = {
					decayto = 7093,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7093, clientId = 7093,
		attributes = {
					decayto = 7094,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7094, clientId = 7094,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7256, clientId = 7256,
		attributes = {
					decayto = 7257,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "braindeath",
					}
	},
	{ id = 7257, clientId = 7257,
		attributes = {
					decayto = 7258,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "braindeath",
					}
	},
	{ id = 7258, clientId = 7258,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7283, clientId = 7283,
		attributes = {
					decayto = 7284,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "golem",
					}
	},
	{ id = 7284, clientId = 7284,
		attributes = {
					decayto = 7285,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "golem",
					}
	},
	{ id = 7285, clientId = 7285,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7321, clientId = 7321,
		attributes = {
					decayto = 7322,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chakoya",
					}
	},
	{ id = 7322, clientId = 7322,
		attributes = {
					decayto = 7323,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chakoya",
					}
	},
	{ id = 7323, clientId = 7323,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7325, clientId = 7325,
		attributes = {
					decayto = 7326,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chakoya",
					}
	},
	{ id = 7326, clientId = 7326,
		attributes = {
					decayto = 7323,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chakoya",
					}
	},
	{ id = 7328, clientId = 7328,
		attributes = {
					decayto = 7329,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chakoya",
					}
	},
	{ id = 7329, clientId = 7329,
		attributes = {
					decayto = 7323,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chakoya",
					}
	},
	{ id = 7331, clientId = 7331,
		attributes = {
					decayto = 7332,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "giant",
					}
	},
	{ id = 7332, clientId = 7332,
		attributes = {
					decayto = 7333,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "giant",
					}
	},
	{ id = 7333, clientId = 7333,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7335, clientId = 7335,
		attributes = {
					decayto = 7336,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "penguin",
					}
	},
	{ id = 7336, clientId = 7336,
		attributes = {
					decayto = 7337,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "penguin",
					}
	},
	{ id = 7337, clientId = 7337,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7339, clientId = 7339,
		attributes = {
					decayto = 7340,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rabbit",
					}
	},
	{ id = 7340, clientId = 7340,
		attributes = {
					decayto = 7341,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rabbit",
					}
	},
	{ id = 7341, clientId = 7341,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7345, clientId = 7345,
		attributes = {
					decayto = 7346,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "crystal spider",
					}
	},
	{ id = 7346, clientId = 7346,
		attributes = {
					decayto = 7347,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "crystal spider",
					}
	},
	{ id = 7347, clientId = 7347,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7624, clientId = 272,
		attributes = {
					decayto = 7625,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7625, clientId = 273,
		attributes = {
					decayto = 7627,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7627, clientId = 275,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7629, clientId = 277,
		attributes = {
					decayto = 7630,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "squirrel",
					}
	},
	{ id = 7630, clientId = 278,
		attributes = {
					decayto = 7631,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "squirrel",
					}
	},
	{ id = 7631, clientId = 279,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7638, clientId = 287,
		attributes = {
					decayto = 7639,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cat",
					}
	},
	{ id = 7639, clientId = 288,
		attributes = {
					decayto = 7640,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cat",
					}
	},
	{ id = 7640, clientId = 289,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7741, clientId = 657,
		attributes = {
					decayto = 7742,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 7742, clientId = 658,
		attributes = {
					decayto = 7743,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 7743, clientId = 659,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7927, clientId = 862,
		attributes = {
					decayto = 7928,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 7928, clientId = 863,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 7929, clientId = 4254,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 7970, clientId = 910,
		attributes = {
					decayto = 7971,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7971, clientId = 911,
		attributes = {
					decayto = 4344,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 4344, clientId = 912,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9879, clientId = 8965,
		attributes = {
					decayto = 9880,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "serpent",
					weight = 60000,
					}
	},
	{ id = 9880, clientId = 8966,
		attributes = {
					decayto = 9881,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "serpent",
					weight = 60000,
					}
	},
	{ id = 9881, clientId = 8967,
		attributes = {
					decayto = 0,
					weight = 60000,
					}
	},
	{ id = 8308, clientId = 951,
		attributes = {
					decayto = 8311,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "serpent",
					}
	},
	{ id = 8311, clientId = 955,
		attributes = {
					decayto = 8312,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "serpent",
					}
	},
	{ id = 8312, clientId = 956,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 8593, clientId = 7763,
		attributes = {
					decayto = 0,
					weight = 60000,
					}
	},
	{ id = 8934, clientId = 8106,
		attributes = {
					decayto = 8935,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "earth elemental",
					weight = 40000,
					}
	},
	{ id = 8935, clientId = 8107,
		attributes = {
					decayto = 8936,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "earth elemental",
					weight = 30000,
					}
	},
	{ id = 8936, clientId = 8108,
		attributes = {
					decayto = 0,
					weight = 20000,
					}
	},
	{ id = 8938, clientId = 8110,
		attributes = {
					decayto = 8939,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					weight = 50000,
					}
	},
	{ id = 8939, clientId = 8111,
		attributes = {
					decayto = 8940,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					weight = 30000,
					}
	},
	{ id = 8940, clientId = 8112,
		attributes = {
					decayto = 0,
					weight = 15000,
					}
	},
	{ id = 8942, clientId = 8114,
		attributes = {
					decayto = 8943,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					weight = 35000,
					}
	},
	{ id = 8943, clientId = 8115,
		attributes = {
					decayto = 8944,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					weight = 35000,
					weight = 20000,
					}
	},
	{ id = 8944, clientId = 8116,
		attributes = {
					decayto = 0,
					weight = 15000,
					}
	},
	{ id = 8946, clientId = 8118,
		attributes = {
					decayto = 0,
					weight = 1000,
					}
	},
	{ id = 8948, clientId = 8120,
		attributes = {
					decayto = 8949,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					weight = 50000,
					}
	},
	{ id = 8949, clientId = 8121,
		attributes = {
					decayto = 8950,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					weight = 40000,
					}
	},
	{ id = 8950, clientId = 8122,
		attributes = {
					decayto = 0,
					weight = 30000,
					}
	},
	{ id = 8952, clientId = 8124,
		attributes = {
					decayto = 8953,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "raider",
					weight = 35000,
					}
	},
	{ id = 8953, clientId = 8125,
		attributes = {
					decayto = 8954,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "raider",
					weight = 25000,
					}
	},
	{ id = 8954, clientId = 8126,
		attributes = {
					decayto = 0,
					weight = 15000,
					}
	},
	{ id = 8956, clientId = 8128,
		attributes = {
					decayto = 8957,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "grim reaper",
					weight = 40000,
					}
	},
	{ id = 8957, clientId = 8129,
		attributes = {
					decayto = 8958,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "grim reaper",
					weight = 30000,
					}
	},
	{ id = 8958, clientId = 8130,
		attributes = {
					decayto = 0,
					weight = 20000,
					}
	},
	{ id = 8961, clientId = 8133,
		attributes = {
					corpsetype = "fire",
					decayto = 8962,
					}
	},
	{ id = 8962, clientId = 8134,
		attributes = {
					corpsetype = "fire",
					decayto = 8963,
					}
	},
	{ id = 8963, clientId = 8135,
		attributes = {
					corpsetype = "fire",
					decayto = 0,
					}
	},
	{ id = 8967, clientId = 8139,
		attributes = {
					corpsetype = "energy",
					decayto = 0,
					}
	},
	{ id = 8968, clientId = 8140,
		attributes = {
					corpsetype = "water",
					decayto = 0,
					}
	},
	{ id = 8969, clientId = 8141,
		attributes = {
					corpsetype = "fire",
					decayto = 0,
					}
	},
	{ id = 9654, clientId = 8738,
		attributes = {
					decayto = 9658,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 9658, clientId = 8742,
		attributes = {
					decayto = 9659,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 9659, clientId = 8743,
		attributes = {
					decayto = 0,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 9824, clientId = 8910,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9830, clientId = 8916,
		attributes = {
					decayto = 9831,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutated",
					}
	},
	{ id = 9831, clientId = 8917,
		attributes = {
					decayto = 9832,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutated",
					}
	},
	{ id = 9832, clientId = 8918,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9868, clientId = 8954,
		attributes = {
					decayto = 9869,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "haunted",
					}
	},
	{ id = 9869, clientId = 8955,
		attributes = {
					decayto = 9870,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "haunted",
					}
	},
	{ id = 9870, clientId = 8956,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9872, clientId = 8958,
		attributes = {
					decayto = 9873,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutated",
					}
	},
	{ id = 9873, clientId = 8959,
		attributes = {
					decayto = 9874,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutated",
					}
	},
	{ id = 9874, clientId = 8960,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9876, clientId = 8962,
		attributes = {
					decayto = 9877,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "crypt shambler",
					}
	},
	{ id = 9877, clientId = 8963,
		attributes = {
					decayto = 9878,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "crypt shambler",
					}
	},
	{ id = 9878, clientId = 8964,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9914, clientId = 9000,
		attributes = {
					corpsetype = "blood",
					decayto = 0,
					}
	},
	{ id = 9916, clientId = 9002,
		attributes = {
					decayto = 9917,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightstalker",
					}
	},
	{ id = 9917, clientId = 9003,
		attributes = {
					decayto = 9918,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightstalker",
					}
	},
	{ id = 9918, clientId = 9004,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9920, clientId = 9006,
		attributes = {
					decayto = 9921,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightstalker",
					}
	},
	{ id = 9921, clientId = 9007,
		attributes = {
					decayto = 9922,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightstalker",
					}
	},
	{ id = 9922, clientId = 9008,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9924, clientId = 9010,
		attributes = {
					decayto = 9925,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hellspawn",
					}
	},
	{ id = 9925, clientId = 9011,
		attributes = {
					decayto = 9926,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hellspawn",
					}
	},
	{ id = 9926, clientId = 9012,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9935, clientId = 9021,
		attributes = {
					decayto = 9936,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "gozzler",
					}
	},
	{ id = 9936, clientId = 9022,
		attributes = {
					decayto = 9937,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "gozzler",
					}
	},
	{ id = 9937, clientId = 9023,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9963, clientId = 9050,
		attributes = {
					corpsetype = "toxic",
					decayto = 0,
					}
	},
	{ id = 9964, clientId = 9051,
		attributes = {
					corpsetype = "toxic",
					decayto = 0,
					}
	},
	{ id = 9965, clientId = 9052,
		attributes = {
					corpsetype = "toxic",
					decayto = 0,
					}
	},
	{ id = 13586, clientId = 12664,
		attributes = {
					corpsetype = "toxic",
					decayto = 0,
					}
	},
	{ id = 10007, clientId = 9094,
		attributes = {
					decayto = 10008,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutated",
					}
	},
	{ id = 10008, clientId = 9095,
		attributes = {
					decayto = 10009,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutated",
					}
	},
	{ id = 10009, clientId = 9096,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 10525, clientId = 9608,
		attributes = {
					decayto = 10526,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "medusa",
					}
	},
	{ id = 10526, clientId = 9609,
		attributes = {
					decayto = 10527,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "medusa",
					}
	},
	{ id = 10527, clientId = 9610,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11263, clientId = 10352,
		attributes = {
					decayto = 11264,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11264, clientId = 10353,
		attributes = {
					decayto = 11265,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11265, clientId = 10354,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11267, clientId = 10356,
		attributes = {
					decayto = 11268,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11268, clientId = 10357,
		attributes = {
					decayto = 11269,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11269, clientId = 10358,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11271, clientId = 10360,
		attributes = {
					decayto = 11272,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11272, clientId = 10361,
		attributes = {
					decayto = 11273,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11273, clientId = 10362,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11275, clientId = 10364,
		attributes = {
					decayto = 11276,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11276, clientId = 10365,
		attributes = {
					decayto = 11277,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11277, clientId = 10366,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11279, clientId = 10368,
		attributes = {
					decayto = 11280,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11280, clientId = 10369,
		attributes = {
					decayto = 11281,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "lizard",
					}
	},
	{ id = 11281, clientId = 10370,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11311, clientId = 10400,
		attributes = {
					decayto = 11312,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11312, clientId = 10401,
		attributes = {
					decayto = 11313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11313, clientId = 10402,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11342, clientId = 10431,
		attributes = {
					decayto = 11343,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "terramite",
					}
	},
	{ id = 11343, clientId = 10432,
		attributes = {
					decayto = 11344,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "terramite",
					}
	},
	{ id = 11344, clientId = 10433,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11357, clientId = 10446,
		attributes = {
					decayto = 11358,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "undead dragon",
					}
	},
	{ id = 11358, clientId = 10447,
		attributes = {
					decayto = 11359,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "undead dragon",
					}
	},
	{ id = 11359, clientId = 10448,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11369, clientId = 10458,
		attributes = {
					decayto = 11371,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "beetle",
					}
	},
	{ id = 11371, clientId = 10460,
		attributes = {
					decayto = 11372,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "beetle",
					}
	},
	{ id = 11372, clientId = 10461,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11351, clientId = 10440,
		attributes = {
					decayto = 11352,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "sandcrawler",
					}
	},
	{ id = 11352, clientId = 10441,
		attributes = {
					decayto = 11353,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "sandcrawler",
					}
	},
	{ id = 11353, clientId = 10442,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11241, clientId = 10330,
		attributes = {
					decayto = 11242,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "gnarlhound",
					}
	},
	{ id = 11242, clientId = 10331,
		attributes = {
					decayto = 11243,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "gnarlhound",
					}
	},
	{ id = 11243, clientId = 10332,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11245, clientId = 10334,
		attributes = {
					decayto = 11246,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 11246, clientId = 10335,
		attributes = {
					decayto = 11243,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 11104, clientId = 10191,
		attributes = {
					decayto = 11105,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11105, clientId = 10192,
		attributes = {
					decayto = 11106,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11106, clientId = 10193,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11305, clientId = 10394,
		attributes = {
					decayto = 11306,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "widow",
					}
	},
	{ id = 11306, clientId = 10395,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 9781, clientId = 8867,
		attributes = {
					decayto = 9782,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutant turtle",
					}
	},
	{ id = 9782, clientId = 8868,
		attributes = {
					decayto = 9783,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "mutant turtle",
					}
	},
	{ id = 9783, clientId = 8869,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11696, clientId = 10774,
		attributes = {
					decayto = 11697,
					corpsetype = "toxic",
					fluidsource = "wine",
					bloodtype = "abomination creature",
					}
	},
	{ id = 11697, clientId = 10775,
		attributes = {
					decayto = 11698,
					corpsetype = "toxic",
					fluidsource = "wine",
					bloodtype = "abomination creature",
					}
	},
	{ id = 11698, clientId = 10776,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11708, clientId = 10786,
		attributes = {
					decayto = 11709,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11709, clientId = 10787,
		attributes = {
					decayto = 11710,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11710, clientId = 10788,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11718, clientId = 10796,
		attributes = {
					decayto = 11719,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crocodile",
					}
	},
	{ id = 11719, clientId = 10797,
		attributes = {
					decayto = 11720,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crocodile",
					}
	},
	{ id = 11720, clientId = 10798,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11714, clientId = 10792,
		attributes = {
					decayto = 11715,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "brimstone bug",
					}
	},
	{ id = 11715, clientId = 10793,
		attributes = {
					decayto = 11716,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "brimstone bug",
					}
	},
	{ id = 11716, clientId = 10794,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11722, clientId = 10800,
		attributes = {
					decayto = 11723,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11723, clientId = 10801,
		attributes = {
					decayto = 11724,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 11724, clientId = 10802,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11661, clientId = 10739,
		attributes = {
					decayto = 11660,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "dustrunner",
					}
	},
	{ id = 11660, clientId = 10738,
		attributes = {
					decayto = 11659,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "dustrunner",
					}
	},
	{ id = 11659, clientId = 10737,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11781, clientId = 10859,
		attributes = {
					decayto = 11782,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 11782, clientId = 10860,
		attributes = {
					decayto = 11783,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 11783, clientId = 10861,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 12254, clientId = 11332,
		attributes = {
					decayto = 12255,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "panther",
					}
	},
	{ id = 12255, clientId = 11333,
		attributes = {
					decayto = 12256,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "panther",
					}
	},
	{ id = 12256, clientId = 11334,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 12258, clientId = 11336,
		attributes = {
					decayto = 12259,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crustacean",
					}
	},
	{ id = 12259, clientId = 11337,
		attributes = {
					decayto = 12260,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "crustacean",
					}
	},
	{ id = 12260, clientId = 11338,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 12262, clientId = 11340,
		attributes = {
					decayto = 12263,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boar",
					weight = 60000,
					}
	},
	{ id = 12263, clientId = 11341,
		attributes = {
					decayto = 12264,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boar",
					weight = 55000,
					}
	},
	{ id = 12264, clientId = 11342,
		attributes = {
					decayto = 0,
					weight = 50000,
					}
	},
	{ id = 12266, clientId = 11344,
		attributes = {
					decayto = 12267,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "stampor",
					}
	},
	{ id = 12267, clientId = 11345,
		attributes = {
					decayto = 12268,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "stampor",
					}
	},
	{ id = 12268, clientId = 11346,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 12270, clientId = 11348,
		attributes = {
					decayto = 12271,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "cavebear",
					}
	},
	{ id = 12271, clientId = 11349,
		attributes = {
					decayto = 12272,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "cavebear",
					}
	},
	{ id = 12272, clientId = 11350,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13375, clientId = 12453,
		attributes = {
					decayto = 13376,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 13376, clientId = 12454,
		attributes = {
					decayto = 13380,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 13378, clientId = 12456,
		attributes = {
					decayto = 13379,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 13379, clientId = 12457,
		attributes = {
					decayto = 13380,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "orc",
					}
	},
	{ id = 13380, clientId = 12458,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13781, clientId = 12859,
		attributes = {
					decayto = 13782,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "strider",
					}
	},
	{ id = 13782, clientId = 12860,
		attributes = {
					decayto = 13783,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "strider",
					}
	},
	{ id = 13783, clientId = 12861,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14047, clientId = 13125,
		attributes = {
					decayto = 14048,
					corpsetype = "toxic",
					fluidsource = "wine",
					bloodtype = "yielothax",
					}
	},
	{ id = 14048, clientId = 13126,
		attributes = {
					decayto = 14049,
					corpsetype = "toxic",
					fluidsource = "wine",
					bloodtype = "yielothax",
					}
	},
	{ id = 14049, clientId = 13127,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14051, clientId = 13129,
		attributes = {
					decayto = 14052,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deer",
					}
	},
	{ id = 14052, clientId = 13130,
		attributes = {
					decayto = 14053,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deer",
					}
	},
	{ id = 14053, clientId = 13131,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14055, clientId = 13133,
		attributes = {
					decayto = 14056,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "camel",
					}
	},
	{ id = 14056, clientId = 13134,
		attributes = {
					decayto = 14057,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "camel",
					}
	},
	{ id = 14057, clientId = 13135,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14067, clientId = 13145,
		attributes = {
					decayto = 14068,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scorpion",
					}
	},
	{ id = 14068, clientId = 13146,
		attributes = {
					decayto = 14069,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "scorpion",
					}
	},
	{ id = 14069, clientId = 13147,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14426, clientId = 13504,
		attributes = {
					decayto = 14428,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 14428, clientId = 13506,
		attributes = {
					decayto = 14430,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 14430, clientId = 13508,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13630, clientId = 12708,
		attributes = {
					decayto = 13631,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13631, clientId = 12709,
		attributes = {
					decayto = 13632,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13632, clientId = 12710,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13633, clientId = 12711,
		attributes = {
					decayto = 13634,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13634, clientId = 12712,
		attributes = {
					decayto = 13635,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13635, clientId = 12713,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13636, clientId = 12714,
		attributes = {
					decayto = 13637,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13637, clientId = 12715,
		attributes = {
					decayto = 13638,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13638, clientId = 12716,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13639, clientId = 12717,
		attributes = {
					decayto = 13640,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13640, clientId = 12718,
		attributes = {
					decayto = 13641,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13641, clientId = 12719,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13642, clientId = 12720,
		attributes = {
					decayto = 13643,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13643, clientId = 12721,
		attributes = {
					decayto = 13644,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13644, clientId = 12722,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13645, clientId = 12723,
		attributes = {
					decayto = 13646,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13646, clientId = 12724,
		attributes = {
					decayto = 13647,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13647, clientId = 12725,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13648, clientId = 12726,
		attributes = {
					decayto = 13649,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13649, clientId = 12727,
		attributes = {
					decayto = 13650,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13650, clientId = 12728,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13651, clientId = 12729,
		attributes = {
					decayto = 13652,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13652, clientId = 12730,
		attributes = {
					decayto = 13653,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13653, clientId = 12731,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13621, clientId = 12699,
		attributes = {
					decayto = 13622,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13622, clientId = 12700,
		attributes = {
					decayto = 13623,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13623, clientId = 12701,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13654, clientId = 12732,
		attributes = {
					decayto = 13655,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13655, clientId = 12733,
		attributes = {
					decayto = 13656,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13656, clientId = 12734,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13657, clientId = 12735,
		attributes = {
					decayto = 13658,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13658, clientId = 12736,
		attributes = {
					decayto = 13659,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13659, clientId = 12737,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13660, clientId = 12738,
		attributes = {
					decayto = 13661,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13661, clientId = 12739,
		attributes = {
					decayto = 13662,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13662, clientId = 12740,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13663, clientId = 12741,
		attributes = {
					decayto = 13664,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13664, clientId = 12742,
		attributes = {
					decayto = 13665,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13665, clientId = 12743,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13666, clientId = 12744,
		attributes = {
					decayto = 13667,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13667, clientId = 12745,
		attributes = {
					decayto = 13668,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13668, clientId = 12746,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13669, clientId = 12747,
		attributes = {
					decayto = 13670,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13670, clientId = 12748,
		attributes = {
					decayto = 13671,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13671, clientId = 12749,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13672, clientId = 12750,
		attributes = {
					decayto = 13673,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13673, clientId = 12751,
		attributes = {
					decayto = 13674,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13674, clientId = 12752,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13675, clientId = 12753,
		attributes = {
					decayto = 13676,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13676, clientId = 12754,
		attributes = {
					decayto = 13677,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13677, clientId = 12755,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13678, clientId = 12756,
		attributes = {
					decayto = 13679,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13679, clientId = 12757,
		attributes = {
					decayto = 13680,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13680, clientId = 12758,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13681, clientId = 12759,
		attributes = {
					decayto = 13682,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13682, clientId = 12760,
		attributes = {
					decayto = 13683,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13683, clientId = 12761,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13684, clientId = 12762,
		attributes = {
					decayto = 13685,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13685, clientId = 12763,
		attributes = {
					decayto = 13686,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13686, clientId = 12764,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13687, clientId = 12765,
		attributes = {
					decayto = 13688,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13688, clientId = 12766,
		attributes = {
					decayto = 13689,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13689, clientId = 12767,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13690, clientId = 12768,
		attributes = {
					decayto = 13691,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13691, clientId = 12769,
		attributes = {
					decayto = 13692,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13692, clientId = 12770,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13693, clientId = 12771,
		attributes = {
					decayto = 13694,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13694, clientId = 12772,
		attributes = {
					decayto = 13695,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 13695, clientId = 12773,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13609, clientId = 12687,
		attributes = {
					decayto = 13610,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 13610, clientId = 12688,
		attributes = {
					decayto = 13613,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 13611, clientId = 12689,
		attributes = {
					decayto = 13612,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 13612, clientId = 12690,
		attributes = {
					decayto = 13613,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "vampire",
					}
	},
	{ id = 13613, clientId = 12691,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11727, clientId = 10805,
		attributes = {
					decayto = 11728,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "souleater",
					}
	},
	{ id = 11728, clientId = 10806,
		attributes = {
					decayto = 0,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "souleater",
					}
	},
	{ id = 13806, clientId = 12884,
		attributes = {
					decayto = 13807,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeletor",
					}
	},
	{ id = 13807, clientId = 12885,
		attributes = {
					decayto = 13808,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "skeletor",
					}
	},
	{ id = 13808, clientId = 12886,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11293, clientId = 10382,
		attributes = {
					decayto = 0,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "eternals",
					}
	},
	{ id = 16257, clientId = 15335,
		attributes = {
					decayto = 16258,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16258, clientId = 15336,
		attributes = {
					decayto = 16259,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16259, clientId = 15337,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11821, clientId = 10899,
		attributes = {
					decayto = 11822,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11822, clientId = 10900,
		attributes = {
					decayto = 11823,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11823, clientId = 10901,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11824, clientId = 10902,
		attributes = {
					decayto = 11825,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "templar",
					}
	},
	{ id = 11825, clientId = 10903,
		attributes = {
					decayto = 16253,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "templar",
					}
	},
	{ id = 16253, clientId = 15331,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16254, clientId = 15332,
		attributes = {
					decayto = 16255,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "templar",
					}
	},
	{ id = 16255, clientId = 15333,
		attributes = {
					decayto = 16256,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "templar",
					}
	},
	{ id = 16256, clientId = 15334,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16273, clientId = 15351,
		attributes = {
					decayto = 16274,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "templar",
					}
	},
	{ id = 16274, clientId = 15352,
		attributes = {
					decayto = 16275,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "templar",
					}
	},
	{ id = 16275, clientId = 15353,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16360, clientId = 15438,
		attributes = {
					decayto = 16359,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "spitfire",
					}
	},
	{ id = 16359, clientId = 15437,
		attributes = {
					decayto = 16358,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "spitfire",
					}
	},
	{ id = 16358, clientId = 15436,
		attributes = {
					decayto = 0,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "spitfire",
					}
	},
	{ id = 15633, clientId = 14711,
		attributes = {
					decayto = 15635,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 15635, clientId = 14713,
		attributes = {
					decayto = 15636,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 15636, clientId = 14714,
		attributes = {
					decayto = 0,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16447, clientId = 15525,
		attributes = {
					decayto = 16448,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					}
	},
	{ id = 16448, clientId = 15526,
		attributes = {
					decayto = 2815,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rat",
					}
	},
	{ id = 16445, clientId = 15523,
		attributes = {
					decayto = 16446,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "spider",
					}
	},
	{ id = 16446, clientId = 15524,
		attributes = {
					decayto = 0,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "spider",
					}
	},
	{ id = 16449, clientId = 15527,
		attributes = {
					decayto = 16450,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bug",
					}
	},
	{ id = 16450, clientId = 15528,
		attributes = {
					decayto = 16451,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bug",
					}
	},
	{ id = 16451, clientId = 15529,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16452, clientId = 15530,
		attributes = {
					decayto = 16453,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bug",
					}
	},
	{ id = 16453, clientId = 15531,
		attributes = {
					decayto = 16454,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bug",
					}
	},
	{ id = 16454, clientId = 15532,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 10499, clientId = 9582,
		attributes = {
					decayto = 10500,
					corpsetype = "water",
					fluidsource = "water",
					bloodtype = "water",
					}
	},
	{ id = 10500, clientId = 9583,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16761, clientId = 15839, 
		attributes = {
					decayto = 16762,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16762, clientId = 15840, 
		attributes = {
					decayto = 16763,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16763, clientId = 15841, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16764, clientId = 15842, 
		attributes = {
					decayto = 16765,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16765, clientId = 15843, 
		attributes = {
					decayto = 16766,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16766, clientId = 15844, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 15620, clientId = 14698, 
		attributes = {
					decayto = 15621,
					corpsetype = "blood",
					fluidsource = "blood",
					}
	},
	{ id = 15621, clientId = 14699, 
		attributes = {
					decayto = 15622,
					corpsetype = "blood",
					fluidsource = "blood",
					}
	},
	{ id = 15622, clientId = 14700, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16767, clientId = 15845, 
		attributes = {
					decayto = 16768,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16768, clientId = 15846, 
		attributes = {
					decayto = 16769,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16769, clientId = 15847, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16770, clientId = 15848, 
		attributes = {
					decayto = 16771,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16771, clientId = 15849, 
		attributes = {
					decayto = 16772,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 16772, clientId = 15850, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 15628, clientId = 14706, 
		attributes = {
					decayto = 15629,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 15629, clientId = 14707, 
		attributes = {
					decayto = 15630,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "deepling",
					}
	},
	{ id = 15630, clientId = 14708, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13017, clientId = 12095, 
		attributes = {
					decayto = 13018,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "basilisk",
					}
	},
	{ id = 13018, clientId = 12096, 
		attributes = {
					decayto = 13019,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "basilisk",
					}
	},
	{ id = 13019, clientId = 12097, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13021, clientId = 12099, 
		attributes = {
					decayto = 13022,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13022, clientId = 12100, 
		attributes = {
					decayto = 13023,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13023, clientId = 12101, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16949, clientId = 16027, 
		attributes = {
					decayto = 16950,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 16950, clientId = 16028, 
		attributes = {
					decayto = 16951,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 16951, clientId = 16029, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16952, clientId = 16030, 
		attributes = {
					decayto = 16953,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 16953, clientId = 16031, 
		attributes = {
					decayto = 16954,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 16954, clientId = 16032, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16955, clientId = 16033, 
		attributes = {
					decayto = 16956,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 16956, clientId = 16034, 
		attributes = {
					decayto = 16957,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 16957, clientId = 16035, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16958, clientId = 16036, 
		attributes = {
					decayto = 16959,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 16959, clientId = 16037, 
		attributes = {
					decayto = 16960,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 16960, clientId = 16038, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16961, clientId = 16039, 
		attributes = {
					decayto = 16962,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16962, clientId = 16040, 
		attributes = {
					decayto = 16963,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16963, clientId = 16041, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16964, clientId = 16042, 
		attributes = {
					decayto = 16965,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16965, clientId = 16043, 
		attributes = {
					decayto = 16966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16966, clientId = 16044, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16967, clientId = 16045, 
		attributes = {
					decayto = 16968,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16968, clientId = 16046, 
		attributes = {
					decayto = 16969,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16969, clientId = 16047, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16970, clientId = 16048, 
		attributes = {
					decayto = 16971,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16971, clientId = 16049, 
		attributes = {
					decayto = 16972,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16972, clientId = 16050, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16973, clientId = 16051, 
		attributes = {
					decayto = 16974,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 16974, clientId = 16052, 
		attributes = {
					decayto = 16975,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 16975, clientId = 16053, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16976, clientId = 16054, 
		attributes = {
					decayto = 16977,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16977, clientId = 16055, 
		attributes = {
					decayto = 16978,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16978, clientId = 16056, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16979, clientId = 16057, 
		attributes = {
					decayto = 16980,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16980, clientId = 16058, 
		attributes = {
					decayto = 16981,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16981, clientId = 16059, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16982, clientId = 16060, 
		attributes = {
					decayto = 16983,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16983, clientId = 16061, 
		attributes = {
					decayto = 16984,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16984, clientId = 16062, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16985, clientId = 16063, 
		attributes = {
					decayto = 16986,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 16986, clientId = 16064, 
		attributes = {
					decayto = 16987,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 16987, clientId = 16065, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 19216, clientId = 18294, 
		attributes = {
					decayto = 19217,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 19217, clientId = 18295, 
		attributes = {
					decayto = 19218,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "royal dwarf",
					}
	},
	{ id = 19218, clientId = 18296, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16988, clientId = 16066, 
		attributes = {
					decayto = 16989,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16989, clientId = 16067, 
		attributes = {
					decayto = 16990,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16990, clientId = 16068, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16991, clientId = 16069, 
		attributes = {
					decayto = 16992,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16992, clientId = 16070, 
		attributes = {
					decayto = 16993,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16993, clientId = 16071, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 16994, clientId = 16072, 
		attributes = {
					decayto = 16995,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16995, clientId = 16073, 
		attributes = {
					decayto = 16996,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 16996, clientId = 16074, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17011, clientId = 16089, 
		attributes = {
					decayto = 17012,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 17012, clientId = 16090, 
		attributes = {
					decayto = 17013,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 17013, clientId = 16091, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17014, clientId = 16092, 
		attributes = {
					decayto = 17015,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 17015, clientId = 16093, 
		attributes = {
					decayto = 17016,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 17016, clientId = 16094, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13810, clientId = 12888, 
		attributes = {
					decayto = 13811,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13811, clientId = 12889, 
		attributes = {
					decayto = 13821,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13813, clientId = 12891, 
		attributes = {
					decayto = 13814,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13814, clientId = 12892, 
		attributes = {
					decayto = 14038,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13816, clientId = 12894, 
		attributes = {
					decayto = 13817,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13817, clientId = 12895, 
		attributes = {
					decayto = 13821,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13819, clientId = 12897, 
		attributes = {
					decayto = 13820,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13820, clientId = 12898, 
		attributes = {
					decayto = 14038,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 13821, clientId = 12899, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14038, clientId = 13116, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17234, clientId = 16312, 
		attributes = {
					decayto = 17235,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17235, clientId = 16313, 
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17230, clientId = 16308, 
		attributes = {
					decayto = 17231,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17231, clientId = 16309, 
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17232, clientId = 16310, 
		attributes = {
					decayto = 17233,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17233, clientId = 16311, 
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17236, clientId = 16314, 
		attributes = {
					decayto = 17237,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 17237, clientId = 16315, 
		attributes = {
					decayto = 2966,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "minotaur",
					}
	},
	{ id = 7621, clientId = 269,
		attributes = {
					decayto = 7622,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7622, clientId = 270,
		attributes = {
					decayto = 17242,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 7626, clientId = 274, 
		attributes = {
					decayto = 17242,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17242, clientId = 16320, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17243, clientId = 16321, 
		attributes = {
					decayto = 17244,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17244, clientId = 16322, 
		attributes = {
					decayto = 17245,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17245, clientId = 16323, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17239, clientId = 16317, 
		attributes = {
					decayto = 17240,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17240, clientId = 16318, 
		attributes = {
					decayto = 17241,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17241, clientId = 16319, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17248, clientId = 16326, 
		attributes = {
					decayto = 17249,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "swarmer",
					}
	},
	{ id = 17249, clientId = 16327, 
		attributes = {
					decayto = 17250,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "swarmer",
					}
	},
	{ id = 17250, clientId = 16328, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17251, clientId = 16329, 
		attributes = {
					decayto = 17252,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17252, clientId = 16330, 
		attributes = {
					decayto = 17253,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17253, clientId = 16331, 
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17254, clientId = 16332, 
		attributes = {
					decayto = 17255,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17255, clientId = 16333, 
		attributes = {
					decayto = 17256,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17256, clientId = 16334,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17261, clientId = 16339,
		attributes = {
					decayto = 17262,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17262, clientId = 16340,
		attributes = {
					decayto = 17263,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17263, clientId = 16341,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17264, clientId = 16342,
		attributes = {
					decayto = 17265,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17265, clientId = 16343,
		attributes = {
					decayto = 17266,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17266, clientId = 16344,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17274, clientId = 16352,
		attributes = {
					decayto = 17275,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17275, clientId = 16353,
		attributes = {
					decayto = 17276,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17276, clientId = 16354,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17277, clientId = 16355,
		attributes = {
					decayto = 17278,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17278, clientId = 16356,
		attributes = {
					decayto = 17279,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17279, clientId = 16357,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17280, clientId = 16358,
		attributes = {
					decayto = 17281,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17281, clientId = 16359,
		attributes = {
					decayto = 17282,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 17282, clientId = 16360,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17283, clientId = 16361,
		attributes = {
					decayto = 17284,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17284, clientId = 16362,
		attributes = {
					decayto = 17242,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17242, clientId = 16320,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17285, clientId = 16363,
		attributes = {
					decayto = 17286,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17286, clientId = 16364,
		attributes = {
					decayto = 17242,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17242, clientId = 16320,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17287, clientId = 16365,
		attributes = {
					decayto = 17288,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17288, clientId = 16366,
		attributes = {
					decayto = 17242,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17242, clientId = 16320,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17302, clientId = 16380,
		attributes = {
					decayto = 17303,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17303, clientId = 16381,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17304, clientId = 16382,
		attributes = {
					decayto = 17305,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17305, clientId = 16383,
		attributes = {
					decayto = 18272,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 18272, clientId = 17350,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17306, clientId = 16384,
		attributes = {
					decayto = 17307,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17307, clientId = 16385,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17308, clientId = 16386,
		attributes = {
					decayto = 17309,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17309, clientId = 16387,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17310, clientId = 16388,
		attributes = {
					decayto = 17311,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17311, clientId = 16389,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17312, clientId = 16390,
		attributes = {
					decayto = 17313,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 17313, clientId = 16391,
		attributes = {
					decayto = 18271,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 18271, clientId = 17349,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17314, clientId = 16392,
		attributes = {
					decayto = 17315,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17315, clientId = 16393,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17316, clientId = 16394,
		attributes = {
					decayto = 17317,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17317, clientId = 16395,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "dragon",
					}
	},
	{ id = 17880, clientId = 16958,
		attributes = {
					decayto = 17881,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					}
	},
	{ id = 17881, clientId = 16959,
		attributes = {
					decayto = 17882,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "rotworm",
					}
	},
	{ id = 17882, clientId = 16960,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17894, clientId = 16972,
		attributes = {
					decayto = 17895,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 17895, clientId = 16973,
		attributes = {
					decayto = 17896,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "spider",
					}
	},
	{ id = 17896, clientId = 16974,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17856, clientId = 16934,
		attributes = {
					decayto = 17857,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 17857, clientId = 16935,
		attributes = {
					decayto = 17858,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 17858, clientId = 16936,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17982, clientId = 17060,
		attributes = {
					decayto = 17983,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 17983, clientId = 17061,
		attributes = {
					decayto = 17984,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 17985, clientId = 17063,
		attributes = {
					decayto = 17986,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 17986, clientId = 17064,
		attributes = {
					decayto = 17984,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 17987, clientId = 17065,
		attributes = {
					decayto = 17988,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 17988, clientId = 17066,
		attributes = {
					decayto = 17984,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "hydra",
					}
	},
	{ id = 17984, clientId = 17062,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18055, clientId = 17133,
		attributes = {
					decayto = 18056,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 18056, clientId = 17134,
		attributes = {
					decayto = 18057,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 18057, clientId = 17135,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18058, clientId = 17136,
		attributes = {
					decayto = 18059,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 18059, clientId = 17137,
		attributes = {
					decayto = 18060,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "cyclops",
					}
	},
	{ id = 18060, clientId = 17138,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18034, clientId = 17112,
		attributes = {
					decayto = 18035,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "frozen nightmare",
					}
	},
	{ id = 18035, clientId = 17113,
		attributes = {
					decayto = 18036,
					corpsetype = "undead",
					fluidsource = "milk",
					bloodtype = "frozen nightmare",
					}
	},
	{ id = 18036, clientId = 17114,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18065, clientId = 17143,
		attributes = {
					decayto = 18066,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "unhurried acarid",
					}
	},
	{ id = 18066, clientId = 17144,
		attributes = {
					decayto = 18067,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "unhurried acarid",
					}
	},
	{ id = 18067, clientId = 17145,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18109, clientId = 17187,
		attributes = {
					decayto = 18110,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "necromancer",
					}
	},
	{ id = 18110, clientId = 17188,
		attributes = {
					decayto = 18111,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "necromancer",
					}
	},
	{ id = 18111, clientId = 17189,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18115, clientId = 17193,
		attributes = {
					decayto = 18116,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "druid",
					}
	},
	{ id = 18116, clientId = 17194,
		attributes = {
					decayto = 18117,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "druid",
					}
	},
	{ id = 18117, clientId = 17195,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18118, clientId = 17196,
		attributes = {
					decayto = 18119,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 18119, clientId = 17197,
		attributes = {
					decayto = 18120,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 18120, clientId = 17198,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18121, clientId = 17199,
		attributes = {
					decayto = 18122,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 18122, clientId = 17200,
		attributes = {
					decayto = 18123,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 18123, clientId = 17201,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11623, clientId = 10701,
		attributes = {
					decayto = 11624,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11624, clientId = 10702,
		attributes = {
					decayto = 11625,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11625, clientId = 10703,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18037, clientId = 17115,
		attributes = {
					decayto = 18038,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boss",
					}
	},
	{ id = 18038, clientId = 17116,
		attributes = {
					decayto = 18039,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boss",
					}
	},
	{ id = 18039, clientId = 17117,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11636, clientId = 10714,
		attributes = {
					decayto = 11637,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11637, clientId = 10715,
		attributes = {
					decayto = 11638,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11638, clientId = 10716,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11639, clientId = 10717,
		attributes = {
					decayto = 11640,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11640, clientId = 10718,
		attributes = {
					decayto = 11641,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11641, clientId = 10719,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 11642, clientId = 10720,
		attributes = {
					decayto = 11643,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11643, clientId = 10721,
		attributes = {
					decayto = 11644,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "human",
					}
	},
	{ id = 11644, clientId = 10722,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 2839, clientId = 4020,
		attributes = {
					decayto = 2840,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					}
	},
	{ id = 2840, clientId = 4021,
		attributes = {
					decayto = 2829,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wolf",
					}
	},
	{ id = 3099, clientId = 4281,
		attributes = {
					decayto = 3100,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 3100, clientId = 4282,
		attributes = {
					decayto = 3111,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "bear",
					}
	},
	{ id = 3101, clientId = 4283,
		attributes = {
					decayto = 3102,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boar",
					}
	},
	{ id = 3102, clientId = 4284,
		attributes = {
					decayto = 2870,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boar",
					}
	},
	{ id = 2870, clientId = 4051,
		attributes = {
					decayto = 3111,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "boar",
					}
	},
	{ id = 13336, clientId = 12414,
		attributes = {
					decayto = 13337,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "leaf golem",
					}
	},
	{ id = 13337, clientId = 12415,
		attributes = {
					decayto = 13338,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "leaf golem",
					}
	},
	{ id = 13338, clientId = 12416,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18255, clientId = 17333,
		attributes = {
					decayto = 18256,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "shaburak",
					}
	},
	{ id = 18256, clientId = 17334,
		attributes = {
					decayto = 18257,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "shaburak",
					}
	},
	{ id = 18257, clientId = 17335,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18264, clientId = 17342,
		attributes = {
					decayto = 18265,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "shaburak",
					}
	},
	{ id = 18265, clientId = 17343,
		attributes = {
					decayto = 18266,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "shaburak",
					}
	},
	{ id = 18266, clientId = 17344,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14039, clientId = 13117,
		attributes = {
					decayto = 14042,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "shaburak",
					}
	},
	{ id = 14042, clientId = 13120,
		attributes = {
					decayto = 14043,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "shaburak",
					}
	},
	{ id = 14043, clientId = 13121,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14041, clientId = 13119,
		attributes = {
					decayto = 14044,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "askarak",
					}
	},
	{ id = 14044, clientId = 13122,
		attributes = {
					decayto = 14045,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "askarak",
					}
	},
	{ id = 14045, clientId = 13123,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18258, clientId = 17336,
		attributes = {
					decayto = 18259,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "askarak",
					}
	},
	{ id = 18259, clientId = 17337,
		attributes = {
					decayto = 18260,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "askarak",
					}
	},
	{ id = 18260, clientId = 17338,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18261, clientId = 17339,
		attributes = {
					decayto = 18262,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "askarak",
					}
	},
	{ id = 18262, clientId = 17340,
		attributes = {
					decayto = 18263,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "askarak",
					}
	},
	{ id = 18263, clientId = 17341,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13052, clientId = 12130,
		attributes = {
					decayto = 13053,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chimera",
					}
	},
	{ id = 13053, clientId = 12131,
		attributes = {
					decayto = 13054,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "chimera",
					}
	},
	{ id = 13054, clientId = 12132,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18268, clientId = 17346,
		attributes = {
					decayto = 18269,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "demon",
					}
	},
	{ id = 18269, clientId = 17347,
		attributes = {
					decayto = 2918,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "demon",
					}
	},
	{ id = 17008, clientId = 16086,
		attributes = {
					decayto = 17009,
					}
	},
	{ id = 17009, clientId = 16087,
		attributes = {
					decayto = 17010,
					}
	},
	{ id = 17010, clientId = 16088,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18402, clientId = 17480,
		attributes = {
					decayto = 18403,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 18403, clientId = 17481,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 18404, clientId = 17482,
		attributes = {
					decayto = 18405,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 18405, clientId = 17483,
		attributes = {
					decayto = 7929,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "troll",
					}
	},
	{ id = 18434, clientId = 17512,
		attributes = {
					decayto = 18435,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18435, clientId = 17513,
		attributes = {
					decayto = 18438,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18436, clientId = 17514,
		attributes = {
					decayto = 18437,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18437, clientId = 17515,
		attributes = {
					decayto = 18438,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18446, clientId = 17524,
		attributes = {
					decayto = 18447,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18447, clientId = 17525,
		attributes = {
					decayto = 18438,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18438, clientId = 17516,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18439, clientId = 17517,
		attributes = {
					decayto = 18440,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18440, clientId = 17518,
		attributes = {
					decayto = 18443,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18441, clientId = 17519,
		attributes = {
					decayto = 18442,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18442, clientId = 17520,
		attributes = {
					decayto = 18443,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18444, clientId = 17522,
		attributes = {
					decayto = 18445,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18445, clientId = 17523,
		attributes = {
					decayto = 18443,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 18443, clientId = 17521,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 19543, clientId = 18621,
		attributes = {
					decayto = 19544,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 19544, clientId = 18622,
		attributes = {
					decayto = 18443,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "djinn",
					}
	},
	{ id = 13196, clientId = 12274,
		attributes = {
					decayto = 13197,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13197, clientId = 12275,
		attributes = {
					decayto = 13198,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13198, clientId = 12276,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13199, clientId = 12277,
		attributes = {
					decayto = 13200,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13200, clientId = 12278,
		attributes = {
					decayto = 13201,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13201, clientId = 12279,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13202, clientId = 12280,
		attributes = {
					decayto = 13203,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13203, clientId = 12281,
		attributes = {
					decayto = 13204,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13204, clientId = 12282,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13205, clientId = 12283,
		attributes = {
					decayto = 13206,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13206, clientId = 12284,
		attributes = {
					decayto = 13207,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "yuan-ti",
					}
	},
	{ id = 13207, clientId = 12285,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14562, clientId = 13640,
		attributes = {
					decayto = 14563,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "sandstone cobra",
					}
	},
	{ id = 14563, clientId = 13641,
		attributes = {
					decayto = 14564,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "sandstone cobra",
					}
	},
	{ id = 14564, clientId = 13642,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18490, clientId = 17568,
		attributes = {
					decayto = 18491,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "phoenix",
					}
	},
	{ id = 18491, clientId = 17569,
		attributes = {
					decayto = 18492,
					corpsetype = "fire",
					fluidsource = "lava",
					bloodtype = "phoenix",
					}
	},
	{ id = 18492, clientId = 17570,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18499, clientId = 17577,
		attributes = {
					decayto = 18500,
					corpsetype = "water",
					fluidsource = "water",
					bloodtype = "frost giant",
					}
	},
	{ id = 18500, clientId = 17578,
		attributes = {
					decayto = 18501,
					corpsetype = "water",
					fluidsource = "water",
					bloodtype = "frost giant",
					}
	},
	{ id = 18501, clientId = 17579,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 14555, clientId = 13633,
		attributes = {
					decayto = 14556,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					}
	},
	{ id = 14556, clientId = 13634,
		attributes = {
					decayto = 14557,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "elf",
					}
	},
	{ id = 14557, clientId = 13635,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17841, clientId = 16919,
		attributes = {
					decayto = 17842,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightmare",
					}
	},
	{ id = 17842, clientId = 16920,
		attributes = {
					decayto = 17843,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "nightmare",
					}
	},
	{ id = 17843, clientId = 16921,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13208, clientId = 12286,
		attributes = {
					decayto = 13209,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13209, clientId = 12287,
		attributes = {
					decayto = 13210,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13210, clientId = 12288,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13211, clientId = 12289,
		attributes = {
					decayto = 13212,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13212, clientId = 12290,
		attributes = {
					decayto = 13213,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13213, clientId = 12291,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13214, clientId = 12292,
		attributes = {
					decayto = 13212,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13215, clientId = 12293,
		attributes = {
					decayto = 13213,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13216, clientId = 12294,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 13219, clientId = 12297,
		attributes = {
					decayto = 13220,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13220, clientId = 12298,
		attributes = {
					decayto = 13221,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "kynohead",
					}
	},
	{ id = 13221, clientId = 12299,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 18564, clientId = 17642,
		attributes = {
					decayto = 18565,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wyvern",
					}
	},
	{ id = 18565, clientId = 17643,
		attributes = {
					decayto = 18566,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "wyvern",
					}
	},
	{ id = 18566, clientId = 17644,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 12286, clientId = 11364,
		attributes = {
					decayto = 12287,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 12287, clientId = 11365,
		attributes = {
					decayto = 12285,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "draken",
					}
	},
	{ id = 12285, clientId = 11363,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17740, clientId = 16818,
		attributes = {
					decayto = 17741,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "dragonfly",
					}
	},
	{ id = 17741, clientId = 16819,
		attributes = {
					decayto = 17742,
					corpsetype = "venom",
					fluidsource = "slime",
					bloodtype = "dragonfly",
					}
	},
	{ id = 17742, clientId = 16820,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 17756, clientId = 16834,
		attributes = {
					decayto = 17757,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "demon",
					}
	},
	{ id = 17757, clientId = 16835,
		attributes = {
					decayto = 17758,
					corpsetype = "blood",
					fluidsource = "blood",
					bloodtype = "demon",
					}
	},
	{ id = 17758, clientId = 16836,
		attributes = {
					decayto = 0,
					}
	},
	{ id = 19460, clientId = 18538,
		attributes = {
					decayto = 0,
					corpsetype = "water",
					}
	},
}
