m_LookAtFunctions.execute = function(item, widget, it, attributeList, count, subType, serverId, clientId)
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		language = LANGUAGE_ENGLISH
	else
		language = LANGUAGE_POLISH
	end
	
	if not it.attributes then
		it.attributes = {}
	end
	
	local text = it.name and it.name[language]
	local var = attributeList[ITEM_NAME]
	if var then
		local value = {var.value, attributeList[ITEM_DESCRIPTION].value}
		text = value[language]
	end
	
	local classId = ITEMCLASS_NORMAL
	if it.attributes.shopitem then
		classId = ITEMCLASS_SHOP
	elseif it.attributes.questitem then
		classId = ITEMCLASS_QUEST
	else
		local var = attributeList[ITEM_CLASS_ITEM]
		if var then
			classId = var.value
		end
	end
	
	local isLegendary = classId == ITEMCLASS_LEGENDARY and it.attributes.legendary
	if isLegendary and it.attributes.legendary.name and #it.attributes.legendary.name[1] > 0 then
		text = it.attributes.legendary.name[language] or it.attributes.legendary.name[1]
		
		if it.attributes.legendary.inscription then
			text = text:format(g_game.getLocalPlayer():getName())
		end
	end
	
	if classId == 0 then
		classId = ITEMCLASS_NORMAL
	end
	
	local tmpWidget = widget:getChildById('cooldown')
	if tmpWidget then
		tmpWidget:setBackgroundColor(ITEM_NAME_COLOR[classId])
		tmpWidget.color = ITEM_NAME_COLOR[classId]
	end
	
	if subType and subType > 0 then
		text = text .. ' (' .. tr(FLUID_NAME[count]) .. ')'
	end
	
	local tmpWidget = widget:getChildById('name')
	tmpWidget:setText(text)
	tmpWidget:setColor(ITEM_NAME_COLOR[classId])
	
	local tmpWidget = widget:getChildById('shader')
	if tmpWidget then
		if classId ~= ITEMCLASS_IGNORE then
			tmpWidget:setImageSource('/ui/mask' .. classId)
		else
			tmpWidget:hide()
		end
	end
	
	local tmpWidget = widget:getChildById('prefix')
	if tmpWidget then
		if it.attributes.runeword then
			tmpWidget:setText(tr('Rune word: ') .. it.attributes.runeword)
		elseif it.attributes.exceptionalword then
			local text = m_LookAtFunctions.getClassName(classId, language)
			if text then
				tmpWidget:setText(text .. (language == LANGUAGE_POLISH and it.attributes.exceptionalword or ''))
			end
		end
		
		tmpWidget:setColor(ITEM_NAME_COLOR[classId])
	end
	
	local spell = false
	if it.attributes.spell or (isLegendary and it.attributes.legendary.spell) then
		if isLegendary and it.attributes.legendary.spell then
			spell = it.attributes.legendary.spell
		else
			spell = it.attributes.spell
		end
	end
	
	local var = attributeList[ITEM_SPELL_ATTRIBUTES]
	if var then
		if not spell or #spell == 0 then
			spell = {}
		end
		
		local k = var.value:explode('|')
		for i = 1, #k do
			local v = k[i]:explode(',')
			local instantSpell = {}
			
			instantSpell.name = v[1]
			for j = 2, #v - 1, 2 do
				local id = tonumber(v[j])
				if id == m_LookAtFunctions.SPELL_ATTRIBUTE_COOLDOWN then
					instantSpell.changeCooldown = -(tonumber(v[j + 1]) * 1000)
				elseif id == m_LookAtFunctions.SPELL_ATTRIBUTE_MANA then
					instantSpell.changeMana = -tonumber(v[j + 1])
				elseif id == m_LookAtFunctions.SPELL_ATTRIBUTE_DAMAGE then
					instantSpell.changeDamage = tonumber(v[j + 1])
				end
			end
			
			table.insert(spell, instantSpell)
		end
	end
	
	local text = false
	if isLegendary then
		text = it.attributes.legendary.description
		
		if it.attributes.legendary.inscription then
			text[1] = text[1]:format(g_game.getLocalPlayer():getName())
		end
	end
	
	if item and (text or spell) then
		item:getChildById('animatedSquare'):show()
		item.onHoverChange = function(self, hovered)
			if hovered then
				m_LookAtFunctions.createHoverWindow(self, text and #text > 0 and it.attributes.legendary.description[language], spell)
			else
				m_LookAtFunctions.destroyHover()
			end
		end
	end
	
	local tmpWidget = widget:getChildById('attributesPanel')
	local height = widget:getHeight() + 20
	local text = it.attributes.description
	local var = attributeList[ITEM_TEXT]
	if var then
		text = {var.value, var.value}
	end
	
	local descriptionBox = widget:getChildById('descriptionBox')
	if descriptionBox then
		if text then
			descriptionBox:setText(text[language])
			descriptionBox:show()
			height = height + descriptionBox:getHeight()
			tmpWidget:setOn(true)
		else
			descriptionBox:setHeight(0)
		end
	end
	
	height = height - 8
	
	local attributes = {}
	local damageFactor = m_LookAtFunctions.getDamageFactor(classId, it.attributes.runeword)
	
	if it.attributes.maxdamage then
		if it.attributes.mindamage then
			table.insert(attributes, {m_LookAtFunctions.getAttributeValue(it.attributes.mindamage, damageFactor) .. ' - ' .. m_LookAtFunctions.getAttributeValue(it.attributes.maxdamage, damageFactor), ITEM_MIN_DAMAGE})
		else
			table.insert(attributes, {'+' .. m_LookAtFunctions.getAttributeValue(it.attributes.maxdamage, damageFactor), ITEM_DISTANCE_DAMAGE})
		end
	end
	
	if it.attributes.defense then
		local value = ''
		local var = it.attributes.defense
		if var then
			value = m_LookAtFunctions.getAttributeValue(var, damageFactor)
		end
		
		if isLegendary and it.attributes.legendary.defense then
			if var then
				value = value .. ' + ' .. it.attributes.legendary.defense
			else
				value = it.attributes.legendary.defense
			end
		end
		
		table.insert(attributes, {value, ITEM_DEFENSE})
	end
	
	if it.attributes.range then
		local var = it.attributes.range
		if isLegendary and it.attributes.legendary.range then
			var = it.attributes.legendary.range
		end
		
		if m_LookAtFunctions.getWeaponType(it.attributes) == m_LookAtFunctions.WEAPON_WAND then
			if classId == ITEMCLASS_PERFECT then
				var = var + 1
			elseif classId == ITEMCLASS_LEGENDARY then
				var = var + 2
			elseif classId == ITEMCLASS_UNIQUE then
				var = var + 3
			end
		elseif classId == ITEMCLASS_UNIQUE then
			var = var + 2
		end
		
		table.insert(attributes, {var, ITEM_SHOOT_RANGE})
	end
	
	if it.attributes.spentmana then
		local var = it.attributes.spentmana
		if isLegendary and it.attributes.legendary.spentmana then
			var = it.attributes.legendary.spentmana
		end
		
		table.insert(attributes, {var, ITEM_SPENT_MANA})
	end
	
	if it.attributes.armor or (isLegendary and it.attributes.legendary.armor) then
		local value = ''
		local var = it.attributes.armor
		if var then
			value = m_LookAtFunctions.getAttributeValue(var, damageFactor)
		end
		
		if isLegendary and it.attributes.legendary.armor then
			if var then
				value = value .. ' + ' .. it.attributes.legendary.armor
			else
				value = it.attributes.legendary.armor
			end
		end
		
		table.insert(attributes, {value, ITEM_ARMOR})
	end
	
	if it.attributes.charges then
		local value = attributeList[ITEM_CHARGES]
		if value then
			value = value.value
		else
			value = m_LookAtFunctions.getAttributeValue(it.attributes.charges, damageFactor)
		end
		
		table.insert(attributes, {value, ITEM_CHARGES})
	end
	
	if it.attributes.weapontype then
		table.insert(attributes, {false, ITEM_WEAPON_TYPE, m_LookAtFunctions.getWeaponType(it.attributes)})
	end
	
	if it.attributes.attackspeed or (isLegendary and it.attributes.legendary.attackspeed) then
		if it.attributes.weapontype and it.attributes.weapontype ~= 'shield' and it.attributes.weapontype ~= 'quiver' then
			local var = it.attributes.attackspeed / 50
			local value = m_LookAtFunctions.getAttributeValue(var, damageFactor / 2) * 50
			
			var = it.attributes.attackspeed
			value = var - (value - var)
			
			if isLegendary and it.attributes.legendary.attackspeed then
				value = value - it.attributes.legendary.attackspeed
			end
			
			table.insert(attributes, {value / 1000 .. ' ' .. tr('seconds'), ITEM_ATTACK_SPEED})
		else
			local var = it.attributes.attackspeed
			if isLegendary and it.attributes.legendary.attackspeed then
				var = it.attributes.legendary.attackspeed
			end
			
			var = m_LookAtFunctions.getAttributeValue(var, damageFactor)
			table.insert(attributes, {(var > 0 and '+' or '') .. var / 1000 .. ' ' .. tr('seconds'), ITEM_ATTACK_SPEED})
		end
	end
	
	if it.attributes.duration or it.attributes.showduration then
		local seconds = attributeList[ITEM_DURATION]
		if seconds then
			seconds = seconds.value
		elseif it.attributes.duration then
			if it.attributes.bufftype then
				seconds = it.attributes.duration
			else
				seconds = m_LookAtFunctions.getAttributeValue(it.attributes.duration, damageFactor)
			end
		end
		
		if not seconds and it.attributes.transformequipto then
			if it.attributes.bufftype then
				seconds = m_LookAtFunctions.getItemDescription(it.attributes.transformequipto).attributes.duration
			else
				seconds = m_LookAtFunctions.getAttributeValue(m_LookAtFunctions.getItemDescription(it.attributes.transformequipto).attributes.duration, damageFactor)
			end
		end
		
		if seconds then
			local minutes, hours = 0, 0
			while(seconds >= 3600) do
				hours = hours + 1
				seconds = seconds - 3600
			end
			
			while(seconds >= 60) do
				minutes = minutes + 1
				seconds = seconds - 60
			end
			
			local text = ''
			if hours > 0 then
				text = hours .. ' ' .. (hours > 1 and tr('hours') or tr('hour'))
			end
			
			if minutes > 0 then
				text = text .. (hours > 0 and ', ' or '') .. minutes .. ' ' .. (minutes > 1 and tr('minutes') or tr('minute'))
			end
			
			if seconds > 0 then
				if hours > 0 or minutes > 0 then
					text = text .. ' ' .. tr('and') .. ' '
				elseif minutes > 0 then
					text = text .. ', '
				end
				
				text = text .. seconds .. ' ' .. (seconds > 1 and tr('seconds') or tr('second'))
			end
			
			table.insert(attributes, {text, ITEM_DURATION})
		end
	end
	
	if it.attributes.containersize then
		local size = 0
		if classId == ITEMCLASS_LEGENDARY then
			size = 4
		elseif classId == ITEMCLASS_UNIQUE then
			size = 8
		end
		
		table.insert(attributes, {it.attributes.containersize + size, ITEM_CONTAINER})
	end
	
	if it.attributes.hitchance or (isLegendary and it.attributes.legendary.hitchance) then
		local var = it.attributes.hitchance or 0
		if isLegendary and it.attributes.legendary.hitchance then
			var = var + it.attributes.legendary.hitchance
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 10 .. '%', ITEM_HIT_CHANCE})
	end
	
	if it.attributes.dodgemelee or (isLegendary and it.attributes.legendary.dodgemelee) then
		local var = it.attributes.dodgemelee or 0
		if isLegendary and it.attributes.legendary.dodgemelee then
			var = var + it.attributes.legendary.dodgemelee
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) / 10 .. '%', ITEM_DODGE_MELEE})
	end
	
	if it.attributes.dodgerange or (isLegendary and it.attributes.legendary.dodgerange) then
		local var = it.attributes.dodgerange or 0
		if isLegendary and it.attributes.legendary.dodgerange then
			var = var + it.attributes.legendary.dodgerange
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) / 10 .. '%', ITEM_DODGE_RANGE})
	end
	
	if it.attributes.stolenlife or (isLegendary and it.attributes.legendary.stolenlife) then
		local var = it.attributes.stolenlife or 0
		if isLegendary and it.attributes.legendary.stolenlife then
			var = var + it.attributes.legendary.stolenlife
		end
		
		table.insert(attributes, {m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 10 .. '%', ITEM_COUNT_STOLEN_LIFE})
	end
	
	if it.attributes.stolenmana or (isLegendary and it.attributes.legendary.stolenmana) then
		local var = it.attributes.stolenmana or 0
		if isLegendary and it.attributes.legendary.stolenmana then
			var = var + it.attributes.legendary.stolenmana
		end
		
		table.insert(attributes, {m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 10 .. '%', ITEM_COUNT_STOLEN_MANA})
	end
	
	if it.attributes.stat or (isLegendary and it.attributes.legendary.stat) then
		local list = {}
		if it.attributes.stat then
			for i = 1, #it.attributes.stat do
				list[m_LookAtFunctions.getStatId(it.attributes.stat[i].type)] = it.attributes.stat[i].value
			end
		end
		
		if isLegendary and it.attributes.legendary.stat then
			for i = 1, #it.attributes.legendary.stat do
				local id = m_LookAtFunctions.getStatId(it.attributes.legendary.stat[i].type)
				list[id] = (list[id] or 0) + it.attributes.legendary.stat[i].value
			end
		end
		
		for k, v in pairs(list) do
			table.insert(attributes, {(v > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(v, damageFactor), k == 1 and ITEM_MAXHEALTH or ITEM_MAXMANA, k})
		end
	end
	
	if it.attributes.regenhealth or (isLegendary and it.attributes.legendary.regenhealth) then
		local var = it.attributes.regenhealth or 0
		if isLegendary and it.attributes.legendary.regenhealth then
			var = var + it.attributes.legendary.regenhealth
		end
		
		table.insert(attributes, {m_LookAtFunctions.getAttributeValue(var, damageFactor), ITEM_REGEN_HEALTH})
	end
	
	if it.attributes.regenmana or (isLegendary and it.attributes.legendary.regenmana) then
		local var = it.attributes.regenmana or 0
		if isLegendary and it.attributes.legendary.regenmana then
			var = var + it.attributes.legendary.regenmana
		end
		
		table.insert(attributes, {m_LookAtFunctions.getAttributeValue(var, damageFactor), ITEM_REGEN_MANA})
	end
	
	if it.attributes.potion then
		for i = 1, #it.attributes.potion do
			table.insert(attributes, {it.attributes.potion[i].value, ITEM_POTIONS, m_LookAtFunctions.getStatId(it.attributes.potion[i].type)})
		end
	end
	
	if it.attributes.criticalhitchance or (isLegendary and it.attributes.legendary.criticalhitchance) then
		local var = it.attributes.criticalhitchance or 0
		if isLegendary and it.attributes.legendary.criticalhitchance then
			var = var + it.attributes.legendary.criticalhitchance
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 10 .. '%', ITEM_CRITICAL})
	end
	
	if it.attributes.criticalhitmultiplier or (isLegendary and it.attributes.legendary.criticalhitmultiplier) then
		local var = it.attributes.criticalhitmultiplier or 0
		if isLegendary and it.attributes.legendary.criticalhitmultiplier then
			var = var + it.attributes.legendary.criticalhitmultiplier
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) / 10 .. '%', ITEM_CRITICAL_HIT_MULTIPLIER})
	end
	
	if it.attributes.chanceblockcritical or (isLegendary and it.attributes.legendary.chanceblockcritical) then
		local var = it.attributes.chanceblockcritical or 0
		if isLegendary and it.attributes.legendary.chanceblockcritical then
			var = var + it.attributes.legendary.chanceblockcritical
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) .. '%', ITEM_BLOCK_CRITICAL})
	end
	
	if it.attributes.reducecriticalhit or (isLegendary and it.attributes.legendary.reducecriticalhit) then
		local var = it.attributes.reducecriticalhit or 0
		if isLegendary and it.attributes.legendary.reducecriticalhit then
			var = var + it.attributes.legendary.reducecriticalhit
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) .. '%', ITEM_REDUCE_CRITICAL_HIT})
	end
	
	if it.attributes.magicattack or (isLegendary and it.attributes.legendary.magicattack) then
		local var = it.attributes.magicattack or 0
		if isLegendary and it.attributes.legendary.magicattack then
			var = var + it.attributes.legendary.magicattack
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) .. '%', ITEM_MAGIC_ATTACK})
	end
	
	if it.attributes.familiar or (isLegendary and it.attributes.legendary.familiar) then
		local var = it.attributes.familiar or 0
		if isLegendary and it.attributes.legendary.familiar then
			var = var + it.attributes.legendary.familiar
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) .. '%', ITEM_FAMILIAR})
	end
	
	if it.attributes.speed or (isLegendary and it.attributes.legendary.speed) then
		local var = it.attributes.speed or 0
		if isLegendary and it.attributes.legendary.speed then
			var = var + it.attributes.legendary.speed
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor), ITEM_SPEED})
	end
	
	if it.attributes.doublehit or (isLegendary and it.attributes.legendary.doublehit) then
		local var = it.attributes.doublehit or 0
		if isLegendary and it.attributes.legendary.doublehit then
			var = var + it.attributes.legendary.doublehit
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 10 .. '%', ITEM_DOUBLE_HIT})
	end
	
	if it.attributes.absolutlyignorearmor or (isLegendary and it.attributes.legendary.absolutlyignorearmor) then
		local var = it.attributes.absolutlyignorearmor or 0
		if isLegendary and it.attributes.legendary.absolutlyignorearmor then
			var = var + it.attributes.legendary.absolutlyignorearmor
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) .. '%', ITEM_FULL_IGNORE_ARMOR})
	end
	
	if it.attributes.luck or (isLegendary and it.attributes.legendary.luck) then
		local var = it.attributes.luck or 0
		if isLegendary and it.attributes.legendary.luck then
			var = var + it.attributes.legendary.luck
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 100 .. '%', ITEM_LUCK})
	end
	
	if it.attributes.recoveryammunition or (isLegendary and it.attributes.legendary.recoveryammunition) then
		local var = it.attributes.recoveryammunition or 0
		if isLegendary and it.attributes.legendary.recoveryammunition then
			var = var + it.attributes.legendary.recoveryammunition
		end
		
		table.insert(attributes, {m_LookAtFunctions.getAttributeValue(var, damageFactor) .. '%', ITEM_RECOVERY_AMMUNITION})
	end
	
	if it.attributes.deathhit or (isLegendary and it.attributes.legendary.deathhit) then
		local var = it.attributes.deathhit or 0
		if isLegendary and it.attributes.legendary.deathhit then
			var = var + it.attributes.legendary.deathhit
		end
		
		table.insert(attributes, {(var > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(var, damageFactor) / 100 .. '%', ITEM_DEATH_HIT})
	end
	
	if it.attributes.assignedspell or (isLegendary and it.attributes.legendary.assignedspell) then
		local var = it.attributes.assignedspell or 0
		if isLegendary and it.attributes.legendary.assignedspell then
			var = it.attributes.legendary.assignedspell
		end
		
		table.insert(attributes, {{var.chance, var.name}, ITEM_SPELL_CHANCE})
	end
	
	if it.attributes.level then
		table.insert(attributes, {it.attributes.level, ITEM_LEVEL})
	end
	
	if it.attributes.vocation then
		table.insert(attributes, {m_LookAtFunctions.getVocationName(it.attributes.vocation), ITEM_VOCATION})
	end
	
	if it.attributes.bonusloot or (isLegendary and it.attributes.legendary.bonusloot) then
		local list = {}
		if it.attributes.bonusloot then
			local tmpString = it.attributes.bonusloot:explode(';')
			for i = 1, #tmpString / 2 do
				list[tmpString[i * 2 - 1]] = tmpString[i * 2]
			end
		end
		
		if isLegendary and it.attributes.legendary.bonusloot then
			local tmpString = it.attributes.legendary.bonusloot:explode(';')
			for i = 1, #tmpString / 2 do
				local id = tmpString[i * 2 - 1]
				if not list[id] then
					list[id] = tmpString[i * 2]
				else
					list[id] = list[id] + tmpString[i * 2]
				end
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {{v, m_LookAtFunctions.getItemNameById(tonumber(k), language)}, ITEM_LOOTLIST})
			end
		end
	end
	
	if it.attributes.invisibility then
		table.insert(attributes, {false, ITEM_INVISIBILITY})
	end
	
	if it.attributes.manashield then
		table.insert(attributes, {false, ITEM_MANASHIELD})
	end
	
	if it.attributes.preventdeath then
		table.insert(attributes, {false, ITEM_PREVENT_DEATH})
	end
	
	-- Combat Types
	if it.attributes.absorb or (isLegendary and it.attributes.legendary.absorb) then
		local list = {}
		if it.attributes.absorb then
			for i = 1, #it.attributes.absorb do
				local id = m_LookAtFunctions.getCombatId(it.attributes.absorb[i].type)
				if id ~= 0 then
					list[id] = it.attributes.absorb[i].value
				end
			end
		end
		
		if isLegendary and it.attributes.legendary.absorb then
			for i = 1, #it.attributes.legendary.absorb do
				local id = m_LookAtFunctions.getCombatId(it.attributes.legendary.absorb[i].type)
				if id ~= 0 then
					list[id] = (list[id] or 0) + it.attributes.legendary.absorb[i].value
				end
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {(v > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(v * 10, damageFactor) / 10 .. '%', ITEM_ABSORB, k})
			end
		end
	end
	
	if it.attributes.increment or (isLegendary and it.attributes.legendary.increment) then
		local list = {}
		if it.attributes.increment then
			for i = 1, #it.attributes.increment do
				list[m_LookAtFunctions.getCombatId(it.attributes.increment[i].type)] = it.attributes.increment[i].value
			end
		end
		
		if isLegendary and it.attributes.legendary.increment then
			for i = 1, #it.attributes.legendary.increment do
				local id = m_LookAtFunctions.getCombatId(it.attributes.legendary.increment[i].type)
				list[id] = (list[id] or 0) + it.attributes.legendary.increment[i].value
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {(v > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(v * 10, damageFactor) / 10 .. '%', ITEM_INCREMENT, k})
			end
		end
	end
	
	if it.attributes.bonusdamage or (isLegendary and it.attributes.legendary.bonusdamage) then
		local list = {}
		if it.attributes.bonusdamage then
			for i = 1, #it.attributes.bonusdamage do
				list[m_LookAtFunctions.getCombatId(it.attributes.bonusdamage[i].type)] = it.attributes.bonusdamage[i].value
			end
		end
		
		if isLegendary and it.attributes.legendary.bonusdamage then
			for i = 1, #it.attributes.legendary.bonusdamage do
				local id = m_LookAtFunctions.getCombatId(it.attributes.legendary.bonusdamage[i].type)
				list[id] = (list[id] or 0) + it.attributes.legendary.bonusdamage[i].value
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {(v > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(v * 10, damageFactor) / 10 .. '%', ITEM_BONUS_DAMAGE, k})
			end
		end
	end
	
	if it.attributes.reflect or (isLegendary and it.attributes.legendary.reflect) then
		local list = {}
		if it.attributes.reflect then
			for i = 1, #it.attributes.reflect do
				list[m_LookAtFunctions.getCombatId(it.attributes.reflect[i].type)] = {it.attributes.reflect[i].chance, it.attributes.reflect[i].value}
			end
		end
		
		if isLegendary and it.attributes.legendary.reflect then
			for i = 1, #it.attributes.legendary.reflect do
				local id = m_LookAtFunctions.getCombatId(it.attributes.legendary.reflect[i].type)
				if not list[id] then
					list[id] = {it.attributes.legendary.reflect[i].chance, it.attributes.legendary.reflect[i].value}
				else
					list[id][1] = list[id][1] + it.attributes.legendary.reflect[i].chance
					list[id][2] = list[id][2] + it.attributes.legendary.reflect[i].value
				end
			end
		end
		
		for k, v in pairs(list) do
			if v[1] ~= 0 and v[2] ~= 0 then
				local value = {
					v[1],
					m_LookAtFunctions.getAttributeValue(v[2] * 10, damageFactor) / 10
				}
				table.insert(attributes, {value, ITEM_REFLECT_CHANCE, k})
			end
		end
	end
	
	if it.attributes.conditions or (isLegendary and it.attributes.legendary.conditions) then
		local list = {}
		local level = 0.75 * g_game.getLocalPlayer():getLevel()
		if it.attributes.conditions then
			local v = it.attributes.conditions
			list[m_LookAtFunctions.getSuppressId(v.type)] = {v.chance, v.damage + level, v.ticks / 1000, v.ticks * v.count / 1000}
		end
		
		if isLegendary and it.attributes.legendary.conditions then
			local v = it.attributes.legendary.conditions
			list[m_LookAtFunctions.getSuppressId(v.type)] = {v.chance, v.damage + level, v.ticks / 1000, v.ticks * v.count / 1000}
		end
		
		for k, v in pairs(list) do
			if v[1] ~= 0 then
				v[1] = math.min(100, m_LookAtFunctions.getAttributeValue(v[1] * 10, damageFactor) / 10)
				table.insert(attributes, {v, ITEM_CONDITION, k})
			end
		end
	end
	
	if it.attributes.conditionticks or (isLegendary and it.attributes.legendary.conditionticks) then
		local list = {}
		if it.attributes.conditionticks then
			for i = 1, #it.attributes.conditionticks do
				list[m_LookAtFunctions.getCombatId(it.attributes.conditionticks[i].type)] = it.attributes.conditionticks[i].value
			end
		end
		
		if isLegendary and it.attributes.legendary.conditionticks then
			for i = 1, #it.attributes.legendary.conditionticks do
				local id = m_LookAtFunctions.getCombatId(it.attributes.legendary.conditionticks[i].type)
				list[id] = (list[id] or 0) + it.attributes.legendary.conditionticks[i].value
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {(v < 0 and '+' or '') .. -m_LookAtFunctions.getAttributeValue(v * 10, damageFactor) / 10 .. '%', ITEM_CONDITION_TICKS, k})
			end
		end
	end
	
	if it.attributes.suppress or (isLegendary and it.attributes.legendary.suppress) then
		local list = {}
		if it.attributes.suppress then
			for i = 1, #it.attributes.suppress do
				table.insert(list, it.attributes.suppress[i].type)
			end
		end
		
		if isLegendary and it.attributes.legendary.suppress then
			for i = 1, #it.attributes.legendary.suppress do
				table.insert(list, it.attributes.legendary.suppress[i].type)
			end
		end
		
		for _, v in pairs(list) do
			table.insert(attributes, {false, ITEM_SUPPRESS, m_LookAtFunctions.getSuppressId(v)})
		end
	end
	
	if it.attributes.damage or (isLegendary and it.attributes.legendary.damage) then
		local list = {}
		if it.attributes.damage then
			for i = 1, #it.attributes.damage do
				list[m_LookAtFunctions.getMonsterClassId(it.attributes.damage[i].type)] = it.attributes.damage[i].value
			end
		end
		
		if isLegendary and it.attributes.legendary.damage then
			for i = 1, #it.attributes.legendary.damage do
				local id = m_LookAtFunctions.getMonsterClassId(it.attributes.legendary.damage[i].type)
				if id ~= 0 then
					list[id] = (list[id] or 0) + it.attributes.legendary.damage[i].value
				end
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {{(v > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(v, damageFactor)}, ITEM_MONSTER_DAMAGE, k})
			end
		end
	end
	
	if it.attributes.absorb or (isLegendary and it.attributes.legendary.absorb) then
		local list = {}
		if it.attributes.absorb then
			for i = 1, #it.attributes.absorb do
				local id = m_LookAtFunctions.getMonsterClassId(it.attributes.absorb[i].type)
				if id ~= 0 then
					list[id] = it.attributes.absorb[i].value
				end
			end
		end
		
		if isLegendary and it.attributes.legendary.absorb then
			for i = 1, #it.attributes.legendary.absorb do
				local id = m_LookAtFunctions.getMonsterClassId(it.attributes.legendary.absorb[i].type)
				if id ~= 0 then
					list[id] = (list[id] or 0) + it.attributes.legendary.absorb[i].value
				end
			end
		end
		
		for k, v in pairs(list) do
			if v ~= 0 then
				table.insert(attributes, {{(v > 0 and '+' or '') .. m_LookAtFunctions.getAttributeValue(v, damageFactor)}, ITEM_MONSTER_DEFENSE, k})
			end
		end
	end
	
	local var = attributeList[ITEM_SKILL]
	if var then
		for i = 1, #var do
			local v = var[i]
			if v.value > 0xFFFF then
				v.value = 0xFFFF - v.value
			end
			
			table.insert(attributes, {(v.value > 0 and '+' or '') .. v.value, ITEM_SKILL, v.type + 1})
		end
		
		if it.attributes.slots then
			local var = attributeList[ITEM_SLOTS]
			if var then
				if var.value > 0xFFFF then
					var.value = 0xFFFF - var.value
				end
				
				table.insert(attributes, {var.value, ITEM_SLOTS})
			end
		end
	else
		local tmpAmount = 0
		if it.attributes.skill or (isLegendary and it.attributes.legendary.skill) then
			local list = {}
			if it.attributes.skill then
				for i = 1, #it.attributes.skill do
					local id = m_LookAtFunctions.getSkillId(it.attributes.skill[i].type)
					if id ~= SKILL_NONE then
						list[id] = it.attributes.skill[i].value
					end
				end
			end
			
			if isLegendary and it.attributes.legendary.skill then
				for i = 1, #it.attributes.legendary.skill do
					local id = m_LookAtFunctions.getSkillId(it.attributes.legendary.skill[i].type)
					if id ~= SKILL_NONE then
						list[id] = (list[id] or 0) + it.attributes.legendary.skill[i].value
					end
				end
			end
			
			for k, v in pairs(list) do
				if v ~= 0 then
					local count = m_LookAtFunctions.getAttributeValue(v, damageFactor)
					tmpAmount = tmpAmount + count
					table.insert(attributes, {(v > 0 and '+' or '') .. count, ITEM_SKILL, k + 1})
				end
			end
		end
		
		if it.attributes.slots then
			table.insert(attributes, {m_LookAtFunctions.getAttributeValue(it.attributes.slots * it.attributes.slotsefficiency, damageFactor) + tmpAmount, ITEM_SLOTS})
		end
	end
	
	if it.attributes.bufftype then
		table.insert(attributes, {{tr(it.attributes.bufftype), attributeList[ITEM_BUFF_NAME].value, it.attributes.buffstrength}, ITEM_BUFF_TYPE})
	end
	
	if it.attributes.breakchance then
		table.insert(attributes, {it.attributes.breakchance / 100 .. '%', ITEM_BREAK_CHANCE})
	end
	
	if it.attributes.food then
		table.insert(attributes, {it.attributes.food .. ' ' .. tr('seconds'), ITEM_FOOD})
	end
	
	local crafting = modules.game_rewards.m_CraftingFunctions.isCraftingItem(clientId)
	if crafting then
		table.insert(attributes, {false, ITEM_CRAFTABLE})
	end
	
	if it.attributes.craftingmaterial then
		if not crafting then
			table.insert(attributes, {false, ITEM_CRAFTABLE})
		end
		
		crafting = modules.game_rewards.m_CraftingFunctions.getCraftingList(clientId)
	end
	
	local var = attributeList[ITEM_HOUSE_OWNER]
	if var then
		table.insert(attributes, {var.value ~= '' and var.value or tr('None'), ITEM_HOUSE_OWNER})
		table.insert(attributes, {attributeList[ITEM_HOUSE_NAME].value, ITEM_HOUSE_NAME})
	end
	
	local var = attributeList[ITEM_WEIGHT]
	if var then
		table.insert(attributes, {var.value, ITEM_WEIGHT})
	elseif it.attributes.weight then
		if it.attributes.charges then
			count = 1
		end
		
		if isLegendary and it.attributes.legendary.weight then
			table.insert(attributes, {it.attributes.legendary.weight * count, ITEM_WEIGHT})
		else
			table.insert(attributes, {it.attributes.weight * count, ITEM_WEIGHT})
		end
	end
	
	if it.attributes.runeword then
		local var = attributeList[ITEM_RUNE_REROLLS]
		if var then
			table.insert(attributes, {var.value, ITEM_RUNE_REROLLS})
		end
		
		local var = attributeList[ITEM_RUNE_QUALITY]
		if var then
			table.insert(attributes, {var.value, ITEM_RUNE_QUALITY})
		end
	end
	
	if serverId and clientId and g_game.getLocalPlayer():getGroupId() > 1 then
		table.insert(attributes, {serverId, ITEM_ID})
		table.insert(attributes, {clientId, ITEM_CLIENT_ID})
	end
	
	local begin = 1
	local tmpHeight = 0
	local lastAttribute = 0
	for i = 1, #attributes do
		local attributeId = attributes[i][2]
		if not it.attributes.runeword then
			if (begin == 1 and isInArray({ITEM_MONSTER_DAMAGE, ITEM_MONSTER_DEFENSE, ITEM_ABSORB, ITEM_CONDITION_TICKS, ITEM_SUPPRESS, ITEM_INCREMENT, ITEM_BONUS_DAMAGE, ITEM_REFLECT_CHANCE, ITEM_CONDITION}, attributeId)) or
				((begin == 1 or begin == 2) and attributeId == ITEM_SKILL and lastAttribute ~= ITEM_SKILL) then
				begin = begin + 1
				tmpHeight = m_LookAtFunctions.createSeparator(tmpHeight, tmpWidget)
			end
		end
		
		tmpHeight = tmpHeight + m_LookAtFunctions.createLabel(attributes[i][1], tmpWidget, attributeId, attributes[i][3], it.attributes.slotspriority, crafting)
		lastAttribute = attributeId
		
		if not it.attributes.runeword then
			if (begin == 2 and not isInArray({ITEM_SKILL, ITEM_MONSTER_DAMAGE, ITEM_MONSTER_DEFENSE, ITEM_ABSORB, ITEM_CONDITION_TICKS, ITEM_SUPPRESS, ITEM_INCREMENT, ITEM_BONUS_DAMAGE, ITEM_REFLECT_CHANCE, ITEM_CONDITION}, attributeId)) or
				((begin == 2 or begin == 3) and attributeId == ITEM_SLOTS) then
				begin = begin + 1
				tmpHeight = m_LookAtFunctions.createSeparator(tmpHeight, tmpWidget)
			end
		end
	end
	
	tmpWidget:setHeight(tmpHeight)
	
	if attributeList[ITEM_INSPECT] then
		widget:setHeight(height + tmpHeight - 36)
		modules.game_encyclopedia.m_ItemlistFunctions.addItemDescription(widget)
	elseif attributeList[ITEM_CRAFTING] then
		widget:setHeight(height + tmpHeight)
		return widget
	else
		widget:setHeight(height + tmpHeight - 28)
	end
end