npc = {
	-- Test Server
	{ name = 'Test Server', script = 'TestServer.lua', direction = 'south',
			look = {type = 541, head = 97, body = 21, legs = 58, feet = 115, addons = 1}
			},
	{ name = 'Dungeoner', script = 'Dungeoner.lua', direction = 'west',
			look = {type = 541, head = 114, body = 58, legs = 38, feet = 96, addons = 2}
			},
	
	-- Event
	{ name = 'Silva', script = 'Cirith/quests/Silva.lua', walkinterval = 9000, speed = 160,
			look = {type = 655, head = 78, body = 114, legs = 116, feet = 78, addons = 3}
			},
	{ name = 'Sato Okada', script = 'Cirith/quests/Sato Okada.lua', walkinterval = 9000, speed = 160,
			look = {type = 684, head = 1, body = 56, legs = 0, feet = 54, addons = 3}
			},
	{ name = 'Kurokawa', script = 'Cirith/quests/Kurokawa.lua', walkinterval = 10000, speed = 150,
			look = {type = 541, head = 39, body = 114, legs = 21, feet = 13, addons = 3}
			},
	
	-- Mararoko
	{ name = 'Gal Tamanah', protocolName = 'Gal\'Tamanah', protocolNameEnglish = 'Gal\'Tamanah', script = 'Mararoko/Gal Tamanah.lua', speed = 100, talkradius = 3, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 683, head = 78, body = 114, legs = 116, feet = 78, addons = 3}
			},
	{ name = 'Ercument', script = 'Mararoko/Ercument.lua', walkinterval = 12000, speed = 140,
			look = {type = 701, head = 78, body = 114, legs = 116, feet = 78}
			},
	{ name = 'Herkheba', script = 'Mararoko/Herkheba.lua', walkinterval = 12000, speed = 140,
			look = {type = 701, head = 78, body = 114, legs = 116, feet = 78}
			},
	{ name = 'Shaahir', script = 'Mararoko/Shaahir.lua', walkinterval = 9000, speed = 160,
			look = {type = 693, head = 78, body = 17, legs = 20, feet = 58, addons = 3}
			},
	{ name = 'Saaliha', script = 'Mararoko/Saaliha.lua', direction = 'south',
			look = {type = 140, head = 40, body = 58, legs = 117, feet = 78, addons = 3}
			},
	{ name = 'Hikma', script = 'Mararoko/Hikma.lua', direction = 'north',
			look = {type = 512, head = 78, body = 26, legs = 95, feet = 63, addons = 2}
			},
	{ name = 'Asad', script = 'Mararoko/Asad.lua', direction = 'north',
			look = {type = 689, head = 0, body = 41, legs = 37, feet = 0, addons = 1}
			},
	{ name = 'Nawfal', script = 'Mararoko/Nawfal.lua', direction = 'west',
			look = {type = 490}
			},
	{ name = 'Aisha', script = 'Mararoko/Aisha.lua', walkinterval = 8000, speed = 140,
			look = {type = 150, head = 6, body = 8, legs = 38, feet = 30, addons = 3}
			},
	{ name = 'Jumail', script = 'Mararoko/Jumail.lua', walkinterval = 12000, speed = 100,
			look = {type = 413, head = 9, body = 121, legs = 0, feet = 77, addons = 1}
			},
	{ name = 'Uqbah', script = 'Mararoko/Uqbah.lua', walkinterval = 8000, speed = 190,
			look = {type = 682, head = 78, body = 37, legs = 1, feet = 95, addons = 1}
			},
	{ name = 'Furqaan', script = 'Mararoko/Furqaan.lua', walkinterval = 5000, speed = 180,
			look = {type = 146, head = 97, body = 37, legs = 95, feet = 11, addons = 2}
			},
	{ name = 'Nahtair', script = 'Mararoko/Nahtair.lua', direction = 'south',
			look = {type = 683, head = 37, body = 23, legs = 61, feet = 47, addons = 1}
			},
	{ name = 'Zubair', script = 'Mararoko/Zubair.lua', walkinterval = 10000, speed = 110,
			look = {type = 681, head = 30, body = 74, legs = 76, feet = 97, addons = 1}
			},
	{ name = 'Zarqa', script = 'Mararoko/Zarqa.lua', direction = 'south',
			look = {type = 511, head = 25, body = 113, legs = 113, feet = 22, addons = 3}
			},
	{ name = 'Khaira', script = 'Mararoko/Khaira.lua', direction = 'south',
			look = {type = 567, head = 76, body = 17, legs = 67, feet = 28, addons = 2}
			},
	{ name = 'Aadila', script = 'Mararoko/Aadila.lua', direction = 'north',
			look = {type = 597, head = 96, body = 18, legs = 93, feet = 1}
			},
	{ name = 'Rabi', script = 'Mararoko/Rabi.lua', walkinterval = 8000, speed = 180,
			look = {type = 407, head = 116, body = 95, legs = 68, feet = 32, addons = 1}
			},
	{ name = 'Laiqa', script = 'Mararoko/Laiqa.lua', walkinterval = 15000, speed = 110,
			look = {type = 150, head = 42, body = 75, legs = 116, feet = 38, addons = 3}
			},
	{ name = 'Muzna', script = 'Mararoko/Muzna.lua', direction = 'south',
			look = {type = 150, head = 4, body = 93, legs = 23, feet = 53, addons = 3}
			},
	{ name = 'Mahyar', script = 'Mararoko/Mahyar.lua', direction = 'south',
			look = {type = 683, head = 122, body = 49, legs = 12, feet = 104, addons = 1}
			},
	{ name = 'Kobad', script = 'Mararoko/Kobad.lua', direction = 'south',
			look = {type = 683, head = 49, body = 95, legs = 42, feet = 12, addons = 1}
			},
	{ name = 'Azartash', script = 'Mararoko/Azartash.lua', direction = 'east',
			look = {type = 465, head = 78, body = 1, legs = 39, feet = 38, addons = 2}
			},
	{ name = 'Cumhur', script = 'Mararoko/Cumhur.lua', direction = 'south',
			look = {type = 453}
			},
	
	{ name = 'Izzat', script = 'emptyTalk.lua', direction = 'west',
			look = {type = 509, head = 119, body = 20, legs = 118, feet = 121, addons = 2}
			},
	
	{ name = 'Sadiqe', script = 'multiTalk.lua', direction = 'west',
			look = {type = 512, addons = 2}
			},
	{ name = 'Bairam', script = 'default.lua', direction = 'east',
			look = {type = 681, head = 1, body = 21, legs = 18, feet = 54, addons = 1}
			},
	{ name = 'Parisa', script = 'default.lua', direction = 'north',
			look = {type = 361, head = 96, body = 10, legs = 64, feet = 47}
			},
	{ name = 'Vahid', script = 'default.lua', direction = 'north',
			look = {type = 397}
			},
	{ name = 'Shiva', script = 'default.lua', direction = 'north',
			look = {type = 140, head = 36, body = 51, legs = 114, feet = 126, addons = 3}
			},
	{ name = 'Yekta', script = 'default.lua', walkinterval = 8000, speed = 180,
			look = {type = 329, head = 94, body = 7, legs = 94, feet = 95}
			},
	{ name = 'Javad', script = 'default.lua', direction = 'south',
			look = {type = 328, head = 95, body = 68, legs = 117, feet = 76}
			},
	{ name = 'Reddy', script = 'default.lua', direction = 'east',
			look = {type = 683, head = 89, body = 18, legs = 67, feet = 69, addons = 1}
			},
	{ name = 'Gray', script = 'default.lua', direction = 'west',
			look = {type = 683, head = 114, body = 40, legs = 42, feet = 16, addons = 1}
			},
	-- Templates
	{ name = 'Laxmon', script = 'multiTalk.lua', direction = 'west',
			look = {type = 465, head = 114, body = 41, legs = 28, feet = 10}
			},
	{ name = 'Adonia', script = 'multiTalk.lua', direction = 'east',
			look = {type = 157, head = 14, body = 16, legs = 36, feet = 96, addons = 2}
			},
	{ name = 'Aererd', script = 'multiTalk.lua', direction = 'west',
			look = {type = 381}
			},
	{ name = 'Ehenu', script = 'multiTalk.lua', direction = 'south',
			look = {type = 568, head = 3, body = 132, legs = 3, feet = 5, addons = 3}
			},
	{ name = 'Fedro', script = 'multiTalk.lua', direction = 'east',
			look = {type = 407, head = 59, body = 40, legs = 118, feet = 29, addons = 1}
			},
	{ name = 'Keneis', script = 'multiTalk.lua', direction = 'south',
			look = {type = 510, head = 97, body = 76, legs = 80, feet = 30, addons = 2}
			},
	{ name = 'Kristo', script = 'multiTalk.lua', direction = 'east',
			look = {type = 468, head = 60, body = 39, legs = 33, feet = 30}
			},
	{ name = 'Martyrius', script = 'multiTalk.lua', direction = 'south',
			look = {type = 465, head = 21, body = 57, legs = 60, feet = 14, addons = 2}
			},
			
	{ name = 'Jellena', script = 'template.lua', direction = 'north',
			look = {type = 569, head = 23, body = 60, legs = 4, feet = 58}
			},
	{ name = 'Havoy', script = 'template.lua', walkinterval = 4000, speed = 140,
			look = {type = 569, head = 8, body = 107, legs = 23, feet = 12, addons = 1}
			},
	{ name = 'Levhan', script = 'template.lua', walkinterval = 5000, speed = 130,
			look = {type = 413, head = 95, body = 107, legs = 23, feet = 67, addons = 1}
			},
	{ name = 'Arius', script = 'template.lua', walkinterval = 8000, speed = 110,
			look = {type = 509, head = 34, body = 34, legs = 78, feet = 113, addons = 3}
			},
	{ name = 'Adriana', script = 'template.lua', walkinterval = 11000, speed = 110,
			look = {type = 361, head = 117, body = 3, legs = 43, feet = 130, addons = 2}
			},
	{ name = 'Azmelqart', script = 'template.lua', direction = 'west',
			look = {type = 132, head = 115, body = 130, legs = 1, feet = 16, addons = 1}
			},
	{ name = 'Ahag', script = 'template.lua', direction = 'east',
			look = {type = 391}
			},
	{ name = 'Bauzis', script = 'template.lua', direction = 'south',
			look = {type = 473}
			},
	{ name = 'Brahim', script = 'template.lua', direction = 'south',
			look = {type = 397, addons = 2}
			},
	{ name = 'Borek', script = 'template.lua',
			look = {type = 0, auxType = 15575, typeex = 16497}
			},
	{ name = 'Auren', script = 'template.lua', walkinterval = 7000, speed = 100,
			look = {type = 453, addons = 3}
			},
	{ name = 'Faliana', script = 'template.lua', walkinterval = 10000, speed = 80,
			look = {type = 361, head = 114, body = 3, legs = 59, feet = 93}
			},
	{ name = 'Godhon', script = 'template.lua', walkinterval = 10000, speed = 100,
			look = {type = 354, head = 33, body = 105, legs = 42, feet = 43}
			},
	{ name = 'Gricio', script = 'template.lua',
			look = {type = 0, auxType = 13409, typeex = 17321}
			},
	{ name = 'Hosseyni', script = 'template.lua', direction = 'south',
			look = {type = 346, addons = 1}
			},
	{ name = 'Makaio', script = 'template.lua', walkinterval = 6000, speed = 110,
			look = {type = 509, head = 18, body = 3, legs = 121, feet = 118, addons = 2}
			},
	{ name = 'Margatnep', script = 'template.lua', direction = 'north',
			look = {type = 570, head = 41, body = 36, legs = 50, feet = 48, addons = 1}
			},
	{ name = 'Nubolir', script = 'template.lua', walkinterval = 4000, speed = 130,
			look = {type = 570, head = 57, body = 78, legs = 118, feet = 114, addons = 2}
			},
	{ name = 'Orlando', script = 'template.lua', walkinterval = 8000, speed = 80,
			look = {type = 328, head = 115, body = 27, legs = 115, feet = 72}
			},
	{ name = 'Raimunda', script = 'template.lua', walkinterval = 4000, speed = 110,
			look = {type = 360, head = 102, body = 9, legs = 41, feet = 36}
			},
	{ name = 'Rauno', script = 'template.lua', direction = 'south',
			look = {type = 420}
			},
	{ name = 'Sabina', script = 'template.lua', direction = 'north',
			look = {type = 511, head = 97, body = 92, legs = 4, feet = 31, addons = 3}
			},
	{ name = 'Viktor', script = 'template.lua', direction = 'west',
			look = {type = 419, addons = 2}
			},
	{ name = 'Yakhia', script = 'template.lua', walkinterval = 20000, speed = 80,
			look = {type = 151, head = 24, body = 76, legs = 125, feet = 9, addons = 2}
			},
			
	{ name = 'Graden', script = 'default.lua', direction = 'north',
			look = {type = 403, head = 78, body = 49, legs = 98, feet = 12, addons = 2}
			},
	{ name = 'Forenthen', script = 'default.lua', direction = 'south',
			look = {type = 403, head = 98, body = 52, legs = 31, feet = 22, addons = 1}
			},
	{ name = 'Karlon', script = 'default.lua', direction = 'south',
			look = {type = 493, addons = 3}
			},
	{ name = 'Metrenis', script = 'default.lua', direction = 'south',
			look = {type = 465, head = 78, body = 38, legs = 7, feet = 90, addons = 3}
			},
	{ name = 'Volunteer Rhgart', script = 'default.lua', walkinterval = 6000, speed = 160,
			look = {type = 570, head = 78, body = 32, legs = 4, feet = 77}
			},
	{ name = 'Miros', script = 'default.lua', walkinterval = 5000, speed = 160,
			look = {type = 509, head = 78, body = 17, legs = 27, feet = 77}
			},
	{ name = 'Bernard', script = 'default.lua', walkinterval = 8000, speed = 120,
			look = {type = 509, head = 1, body = 78, legs = 51, feet = 5}
			},
	{ name = 'Mensen', script = 'default.lua', direction = 'north',
			look = {type = 569, head = 114, body = 11, legs = 61, feet = 104, addons = 2}
			},
	{ name = 'CrocodileEggs', script = 'default.lua', hidename = true, hidehealth = true,
			look = {typeex = 4875}
			},
	{ name = 'Crystal', script = 'default.lua', hidename = true, hidehealth = true,
			look = {typeex = 15160}
			},
	{ name = 'Anne', script = 'default.lua', direction = 'east',
			look = {type = 361, head = 94, body = 2, legs = 9, feet = 128, addons = 3}
			},
	{ name = 'Blake', script = 'default.lua', direction = 'north',
			look = {type = 453}
			},
	{ name = 'Edward', script = 'default.lua', direction = 'east',
			look = {type = 476, head = 28, body = 2, legs = 66, feet = 95}
			},
	{ name = 'Francesca', script = 'default.lua', direction = 'south',
			look = {type = 331}
			},
	{ name = 'Guerin', script = 'default.lua', direction = 'north',
			look = {type = 493}
			},
	{ name = 'Half Living Kentaru 2', protocolName = 'Kentaru', protocolNameEnglish = 'Kentaru', script = 'default.lua', hidehealth = true, hidename = true,
			look = {typeex = 1767}
			},
	{ name = 'Mountain', script = 'default.lua', hidehealth = true, hidename = true,
			look = {typeex = 4471}
			},
	{ name = 'Necromancer Candle', script = 'default.lua', hidehealth = true, hidename = true,
			look = {typeex = 11913}
			},
	{ name = 'Olympus', script = 'default.lua', canwalkanywhere = true,
			look = {type = 365}
			},
	{ name = 'Ragins', script = 'default.lua', direction = 'south',
			look = {type = 476, head = 32, body = 56, legs = 60, feet = 42}
			},
	{ name = 'Rubyscire', script = 'default.lua', speed = 200, direction = 'east',
			look = {type = 382, addons = 2}
			},
	{ name = 'Shemen', script = 'default.lua', direction = 'north',
			look = {type = 567, head = 78, body = 17, legs = 115, feet = 19, addons = 2}
			},
	{ name = 'Wakron', script = 'default.lua', direction = 'east',
			look = {type = 473}
			},
	{ name = 'Sir Wessax', script = 'default.lua', direction = 'south',
			look = {type = 332}
			},
	{ name = 'Statue', script = 'default.lua', hidehealth = true, hidename = true,
			look = {typeex = 13954}
			},
	{ name = 'Tanni', script = 'default.lua', direction = 'west',
			look = {type = 360, head = 24, body = 57, legs = 112, feet = 36, addons = 1}
			},
	{ name = 'Water', script = 'default.lua', hidehealth = true, hidename = true,
			look = {typeex = 103}
			},
	{ name = 'Zdenka', script = 'default.lua', direction = 'north',
			look = {type = 141, head = 59, body = 7, legs = 98, feet = 58}
			},
	{ name = 'Zefiro', script = 'default.lua', direction = 'east',
			look = {type = 465, head = 0, body = 26, legs = 60, feet = 12, addons = 3}
			},
	{ name = 'Jacquard Free', protocolName = 'Jacquard', protocolNameEnglish = 'Jacquard', script = 'default.lua', walkinterval = 2000, speed = 100,
			look = {type = 454}
			},
	{ name = 'Jacquard NPC', protocolName = 'Jacquard', protocolNameEnglish = 'Jacquard', script = 'default.lua', walkinterval = 1000, speed = 200,
			look = {type = 454}
			},
	{ name = 'Lava', script = 'default.lua', direction = 'south',
			look = {type = 512, head = 97, body = 56, legs = 123, feet = 77, addons = 2}
			},
	{ name = 'Light Brown Horse', protocolName = 'Ko�', protocolNameEnglish = 'Horse', script = 'default.lua', direction = 'south',
			look = {type = 450}
			},
	{ name = 'Light Brown Horse South', protocolName = 'Ko�', protocolNameEnglish = 'Horse', script = 'default.lua', direction = 'south',
			look = {type = 450}
			},
	{ name = 'Horse', protocolName = 'Ko�', script = 'default.lua', direction = 'north',
			look = {type = 452}
			},
	{ name = 'Jankus', script = 'default.lua', direction = 'east',
			look = {type = 382, addons = 3}
			},
	{ name = 'Polat', script = 'default.lua', direction = 'south',
			look = {type = 382}
			},
	{ name = 'Harkan', script = 'default.lua', direction = 'west',
			look = {type = 382, addons = 2}
			},
	{ name = 'Avalax', script = 'default.lua', direction = 'west',
			look = {type = 382, addons = 3}
			},
	{ name = 'Markens', script = 'default.lua', direction = 'south',
			look = {type = 471, addons = 1}
			},
	{ name = 'Yarthan', script = 'default.lua', direction = 'north',
			look = {type = 472, addons = 3}
			},
	{ name = 'Gerden', script = 'default.lua', direction = 'east',
			look = {type = 472, addons = 2}
			},
	{ name = 'Brundar', script = 'default.lua', walkinterval = 8000, speed = 140,
			look = {type = 194, head = 39, body = 37, legs = 77, feet = 0}
			},
	{ name = 'Leonar', script = 'default.lua', walkinterval = 3000, speed = 200,
			look = {type = 194, head = 42, body = 124, legs = 117, feet = 10}
			},
	{ name = 'Khazalth', script = 'default.lua', direction = 'west',
			look = {type = 413, head = 58, body = 40, legs = 20, feet = 0}
			},
	{ name = 'Sorbaga', script = 'default.lua', walkinterval = 5000, speed = 180,
			look = {type = 264, head = 78, body = 40, legs = 20, feet = 0}
			},
	{ name = 'Groburr', script = 'default.lua', walkinterval = 2000, speed = 200,
			look = {type = 478, head = 114, body = 47, legs = 35, feet = 115, addons = 3}
			},
	{ name = 'Tarnfaeln', script = 'default.lua', direction = 'south',
			look = {type = 568, head = 78, body = 28, legs = 42, feet = 11, addons = 3}
			},
	{ name = 'Nargass', script = 'default.lua', walkinterval = 6000, speed = 100,
			look = {type = 541, head = 26, body = 45, legs = 39, feet = 115, addons = 3}
			},
	{ name = 'Brundar War', protocolName = 'Brundar', protocolNameEnglish = 'Brundar', script = 'default.lua', direction = 'east',
			look = {type = 194, head = 39, body = 37, legs = 77, feet = 0}
			},
	{ name = 'Leonar War', protocolName = 'Leonar', protocolNameEnglish = 'Leonar', script = 'default.lua', direction = 'east',
			look = {type = 194, head = 42, body = 124, legs = 117, feet = 10}
			},
	{ name = 'Khazalth War', protocolName = 'Khazalth', protocolNameEnglish = 'Khazalth', script = 'default.lua', direction = 'east',
			look = {type = 413, head = 58, body = 40, legs = 20, feet = 0}
			},
	{ name = 'Sorbaga War', protocolName = 'Sorbaga', protocolNameEnglish = 'Sorbaga', script = 'default.lua', direction = 'east',
			look = {type = 264, head = 78, body = 40, legs = 20, feet = 0}
			},
	{ name = 'Groburr War', protocolName = 'Groburr', protocolNameEnglish = 'Groburr', script = 'default.lua', direction = 'west',
			look = {type = 478, head = 114, body = 47, legs = 35, feet = 115, addons = 3}
			},
	{ name = 'Nargass War', protocolName = 'Nargass', protocolNameEnglish = 'Nargass', script = 'default.lua', direction = 'west',
			look = {type = 541, head = 26, body = 45, legs = 39, feet = 115, addons = 3}
			},
	{ name = 'Asthrog War', protocolName = 'Asthrog', protocolNameEnglish = 'Asthrog', script = 'default.lua', direction = 'west',
			look = {type = 567, head = 38, body = 58, legs = 57, feet = 115}
			},
	{ name = 'Valhida War', protocolName = 'Valhida', protocolNameEnglish = 'Valhida', script = 'default.lua', direction = 'west',
			look = {type = 569, head = 96, body = 97, legs = 40, feet = 20}
			},
	{ name = 'Grymasza War', protocolName = 'Grymasza', protocolNameEnglish = 'Grymasza', script = 'default.lua', direction = 'west',
			look = {type = 597, head = 114, body = 22, legs = 21, feet = 20}
			},
			
			
			
			
	
	{ name = 'Henri', script = 'emptyTalk.lua', walkinterval = 5000, speed = 120,
			look = {type = 251, head = 0, body = 12, legs = 31, feet = 0, addons = 2}
			},
	{ name = 'Jarkko', script = 'emptyTalk.lua', direction = 'north',
			look = {type = 251, head = 78, body = 26, legs = 45, feet = 3, addons = 3}
			},
	{ name = 'Unto', script = 'emptyTalk.lua', walkinterval = 7500, speed = 200,
			look = {type = 251, head = 40, body = 20, legs = 39, feet = 0, addons = 2}
			},
	{ name = 'Vihtori', script = 'emptyTalk.lua', walkinterval = 3000, speed = 140,
			look = {type = 251, head = 38, body = 31, legs = 50, feet = 0, addons = 1}
			},
	{ name = 'Juha', script = 'emptyTalk.lua', walkinterval = 3000, speed = 140,
			look = {type = 251, head = 4, body = 30, legs = 51, feet = 0, addons = 3}
			},
	{ name = 'Tomi', script = 'emptyTalk.lua', walkinterval = 10000, speed = 170,
			look = {type = 251, head = 39, body = 22, legs = 41, feet = 19, addons = 0}
			},
	{ name = 'Visa', script = 'emptyTalk.lua', walkinterval = 7000, speed = 150,
			look = {type = 251, head = 41, body = 19, legs = 0, feet = 0, addons = 1}
			},
	{ name = 'Petteri', script = 'emptyTalk.lua', walkinterval = 9000, speed = 120,
			look = {type = 251, head = 57, body = 0, legs = 0, feet = 0, addons = 2}
			},
	{ name = 'Turo', script = 'emptyTalk.lua', walkinterval = 6000, speed = 170,
			look = {type = 251, head = 43, body = 49, legs = 23, feet = 19, addons = 0}
			},
	{ name = 'Tenho', script = 'emptyTalk.lua', walkinterval = 9500, speed = 190,
			look = {type = 251, head = 58, body = 8, legs = 47, feet = 2, addons = 2}
			},
	{ name = 'Pia', script = 'emptyTalk.lua', direction = 'east',
			look = {type = 252, head = 5, body = 24, legs = 5, feet = 1, addons = 2}
			},
	{ name = 'Anu', script = 'emptyTalk.lua', walkinterval = 4000, speed = 200,
			look = {type = 252, head = 0, body = 1, legs = 2, feet = 3, addons = 3}
			},
	{ name = 'Sonja', script = 'emptyTalk.lua', walkinterval = 12000, speed = 150,
			look = {type = 252, head = 114, body = 19, legs = 38, feet = 57, addons = 1}
			},
	{ name = 'Rauha', script = 'emptyTalk.lua', walkinterval = 5500, speed = 130,
			look = {type = 252, head = 77, body = 27, legs = 28, feet = 19, addons = 2}
			},
	{ name = 'Arja', script = 'emptyTalk.lua', walkinterval = 7500, speed = 190,
			look = {type = 252, head = 114, body = 20, legs = 39, feet = 0, addons = 0}
			},
	{ name = 'Juva', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 252, head = 40, body = 42, legs = 43, feet = 39, addons = 1}
			},
	{ name = 'Sara', script = 'emptyTalk.lua', walkinterval = 8000, speed = 210,
			look = {type = 252, head = 56, body = 33, legs = 42, feet = 38, addons = 1}
			},	
	{ name = 'Arton', script = 'emptyTalk.lua', direction = 'north',
			look = {type = 359}
			},
	{ name = 'Bertiers', script = 'emptyTalk.lua', direction = 'east',
			look = {type = 478, head = 23, body = 53, legs = 20, feet = 110, addons = 2}
			},
	{ name = 'Calixtus', script = 'emptyTalk.lua', direction = 'east', lightColor = 206, intensity = 14,
			look = {type = 493, addons = 3}
			},
	{ name = 'Cermes', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 403, head = 50, body = 0, legs = 24, feet = 39}
			},
	{ name = 'Dominic', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 569, head = 38, body = 106, legs = 95, feet = 0, addons = 2}
			},
	{ name = 'Difmos', script = 'emptyTalk.lua', walkinterval = 5000, speed = 120,
			look = {type = 403, head = 39, body = 41, legs = 33, feet = 112, addons = 2}
			},
	{ name = 'Drahn', script = 'emptyTalk.lua', walkinterval = 4000, speed = 90,
			look = {type = 403, head = 60, body = 28, legs = 79, feet = 12, addons = 3}
			},
	{ name = 'Drunkan', script = 'emptyTalk.lua', walkinterval = 8500, speed = 100,
			look = {type = 403, head = 1, body = 60, legs = 57, feet = 13, addons = 3}
			},
	{ name = 'Emil', script = 'emptyTalk.lua', direction = 'west',
			look = {type = 471, addons = 3}
			},
	{ name = 'Eordrin', script = 'emptyTalk.lua', walkinterval = 5000, speed = 110,
			look = {type = 475}
			},
	{ name = 'Ethar', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 403, head = 75, body = 14, legs = 50, feet = 95, addons = 2}
			},
	{ name = 'Ganir', script = 'emptyTalk.lua', walkinterval = 7500, speed = 100,
			look = {type = 403, head = 127, body = 53, legs = 11, feet = 5}
			},
	{ name = 'Herbet', script = 'emptyTalk.lua', direction = 'west',
			look = {type = 346, addons = 1}
			},
	{ name = 'Hyrum', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 569, head = 13, body = 67, legs = 47, feet = 68}
			},
	{ name = 'Ilbak', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 273, head = 73, body = 116, legs = 56, feet = 33, addons = 2}
			},
	{ name = 'Irtyx', script = 'emptyTalk.lua', direction = 'east',
			look = {type = 403, head = 108, body = 8, legs = 65, feet = 20}
			},
	{ name = 'Jebel', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 570, head = 114, body = 124, legs = 1, feet = 106, addons = 1}
			},
	{ name = 'Leontinus', script = 'emptyTalk.lua', walkinterval = 10000, speed = 100,
			look = {type = 456}
			},
	{ name = 'Maekus', script = 'emptyTalk.lua', walkinterval = 3000, speed = 90,
			look = {type = 403, head = 99, body = 21, legs = 33, feet = 49, addons = 2}
			},
	{ name = 'Mara', script = 'emptyTalk.lua', walkinterval = 7000, speed = 100,
			look = {type = 360, head = 86, body = 57, legs = 68, feet = 30, addons = 1}
			},
	{ name = 'Okha', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 491, addons = 3}
			},
	{ name = 'Othman', script = 'emptyTalk.lua', direction = 'north',
			look = {type = 273, head = 12, body = 22, legs = 31, feet = 52, addons = 1}
			},
	{ name = 'Owen', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 552, head = 26, body = 32, legs = 4, feet = 10}
			},
	{ name = 'Premysl', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 490, addons = 3}
			},
	{ name = 'Prizio', script = 'emptyTalk.lua', direction = 'east',
			look = {type = 570, head = 78, body = 11, legs = 33, feet = 130, addons = 1}
			},
	{ name = 'Rutar', script = 'emptyTalk.lua', direction = 'north',
			look = {type = 403, head = 26, body = 57, legs = 40, feet = 16, addons = 1}
			},
	{ name = 'Stanislav', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 407, head = 19, body = 4, legs = 57, feet = 27, addons = 1}
			},
	{ name = 'Thorge', script = 'emptyTalk.lua', direction = 'north',
			look = {type = 453, addons = 2}
			},
	{ name = 'Ubus', script = 'emptyTalk.lua', walkinterval = 15000, speed = 80,
			look = {type = 403, head = 64, body = 58, legs = 13, feet = 38, addons = 1}
			},
	{ name = 'Ulo', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 568, head = 21, body = 40, legs = 9, feet = 58}
			},
	{ name = 'Usebio', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 569, head = 0, body = 7, legs = 21, feet = 18}
			},
	{ name = 'Vojtech', script = 'emptyTalk.lua', direction = 'east',
			look = {type = 417, head = 131, body = 29, legs = 61, feet = 57}
			},
	{ name = 'Zelbe', script = 'emptyTalk.lua', direction = 'south',
			look = {type = 403, head = 30, body = 42, legs = 95, feet = 38}
			},
	
	-- Chaeck
	{ name = 'Apollo', script = 'Chaeck/Traders.lua', walkinterval = 5000, speed = 70,
			look = {type = 354, head = 40, body = 95, legs = 68, feet = 0, addons = 2}
			},
	{ name = 'Ageit', script = 'Chaeck/Traders.lua', direction = 'west',
			look = {type = 476, head = 76, body = 38, legs = 57, feet = 58, addons = 3}
			},
	{ name = 'Alastor', script = 'Chaeck/Alastor.lua', walkinterval = 6500, speed = 110,
			look = {type = 509, head = 76, body = 98, legs = 61, feet = 78, addons = 2}
			},
	{ name = 'Dervil', script = 'Chaeck/Traders.lua', talkradius = 2,
			look = {type = 0, auxType = 13409, typeex = 14331}
			},
	{ name = 'Filia', script = 'Chaeck/Traders.lua', walkinterval = 2100, speed = 90,
			look = {type = 360, head = 79, body = 23, legs = 116, feet = 97, addons = 1}
			},
	{ name = 'Filip', script = 'Chaeck/Traders.lua', walkinterval = 4800, speed = 90,
			look = {type = 509, head = 78, body = 70, legs = 29, feet = 79, addons = 2}
			},
	{ name = 'Gretos', script = 'Chaeck/Traders.lua', walkinterval = 4000, speed = 110,
			look = {type = 473, addons = 1}
			},
	{ name = 'Hajduk', script = 'Chaeck/Traders.lua', walkinterval = 2000, speed = 90,
			look = {type = 354, head = 78, body = 114, legs = 116, feet = 78, addons = 2}
			},
	{ name = 'Hanward', script = 'Chaeck/Traders.lua', walkinterval = 3000, speed = 80,
			look = {type = 478, head = 3, body = 43, legs = 19, feet = 97}
			},
	{ name = 'Hondvor', script = 'Chaeck/Traders.lua', walkinterval = 10100, speed = 40,
			look = {type = 493, addons = 2}
			},
	{ name = 'Jeb', script = 'Chaeck/Traders.lua', walkinterval = 5100, speed = 60,
			look = {type = 478, head = 79, body = 58, legs = 47, feet = 117}
			},
	{ name = 'Karol', script = 'Chaeck/Traders.lua', walkinterval = 2900, speed = 110,
			look = {type = 465, head = 58, body = 78, legs = 98, feet = 115}
			},
	{ name = 'Marta', script = 'Chaeck/Traders.lua', walkinterval = 2000, speed = 80,
			look = {type = 361, head = 77, body = 89, legs = 90, feet = 58, addons = 3}
			},
	{ name = 'Napster', script = 'Chaeck/Traders.lua', walkinterval = 6200, speed = 50,
			look = {type = 354, head = 76, body = 81, legs = 80, feet = 97, addons = 2}
			},
	{ name = 'Niko', script = 'Chaeck/Traders.lua', walkinterval = 2300, speed = 80, talkradius = 2,
			look = {type = 354, head = 40, body = 46, legs = 67, feet = 75, addons = 1}
			},
	{ name = 'Rick', script = 'Chaeck/Traders.lua', walkinterval = 5100, speed = 60,
			look = {type = 509, head = 79, body = 39, legs = 44, feet = 117, addons = 2}
			},
	{ name = 'Rupert', script = 'Chaeck/Traders.lua', walkinterval = 6900, speed = 110,
			look = {type = 419, addons = 2}
			},
	{ name = 'Slyntem', script = 'Chaeck/Traders.lua', walkinterval = 2000, speed = 90,
			look = {type = 509, head = 101, body = 98, legs = 43, feet = 132, addons = 1}
			},
	{ name = 'Sofia', script = 'Chaeck/Traders.lua', walkinterval = 3170, speed = 80,
			look = {type = 361, head = 79, body = 41, legs = 53, feet = 1, addons = 3}
			},
	{ name = 'Timothy', script = 'Chaeck/Traders.lua', walkinterval = 3190, speed = 100,
			look = {type = 465, head = 78, body = 60, legs = 67, feet = 96}
			},
			
	{ name = 'Tom', script = 'Chaeck/Tom.lua', walkinterval = 2000, speed = 160,
			look = {type = 465, head = 4, body = 100, legs = 33, feet = 98, addons = 3}
			},
	{ name = 'Uncross de Harr', script = 'Chaeck/Uncross de Harr.lua', walkinterval = 2000, speed = 70,
			look = {type = 397, addons = 2}
			},
	{ name = 'Tonil', script = 'Chaeck/Tonil.lua', direction = 'west',
			look = {type = 476, head = 58, body = 60, legs = 10, feet = 43, addons = 3}
			},
	{ name = 'Tral', script = 'Chaeck/Tral.lua', walkinterval = 6900, speed = 110,
			look = {type = 476, head = 59, body = 38, legs = 49, feet = 77}
			},
	{ name = 'Traug', script = 'Chaeck/Traug.lua', walkinterval = 2000, speed = 70,
			look = {type = 354, head = 76, body = 113, legs = 68, feet = 115, addons = 2}
			},
	{ name = 'Amber', script = 'Chaeck/Peasants.lua', walkinterval = 1900, speed = 180,
			look = {type = 360, head = 8, body = 19, legs = 23, feet = 1, addons = 1}
			},
	{ name = 'Asgard', script = 'Chaeck/Peasants.lua', walkinterval = 3500, speed = 110,
			look = {type = 509, head = 8, body = 66, legs = 6, feet = 76, addons = 2}
			},
	{ name = 'Coloven', script = 'Chaeck/Peasants.lua', walkinterval = 1500, speed = 200,
			look = {type = 360, head = 43, body = 57, legs = 36, feet = 76}
			},
	{ name = 'Hyacynth', script = 'Chaeck/Peasants.lua', walkinterval = 2300,
			look = {type = 509, head = 79, body = 37, legs = 79, feet = 57}
			},
	{ name = 'Yabu', script = 'Chaeck/Peasants.lua', walkinterval = 2200,
			look = {type = 509, head = 79, body = 37, legs = 79, feet = 57, addons = 3}
			},
	{ name = 'Murhus', script = 'Chaeck/Peasants.lua', walkinterval = 1700,
			look = {type = 509, head = 79, body = 37, legs = 79, feet = 57, addons = 1}
			},
	{ name = 'Taja', script = 'Chaeck/Peasants.lua', walkinterval = 2000, speed = 70,
			look = {type = 360, head = 82, body = 65, legs = 6, feet = 76, addons = 2}
			},
			
	{ name = 'Synthia', script = 'Chaeck/Synthia.lua', walkinterval = 2000, speed = 70,
			look = {type = 361, head = 78, body = 114, legs = 91, feet = 91, addons = 3}
			},
	{ name = 'Soya', script = 'Chaeck/Soya.lua', walkinterval = 10000, speed = 80,
			look = {type = 140, head = 78, body = 69, legs = 58, feet = 114, addons = 3}
			},
	{ name = 'Sulur', script = 'Chaeck/Sulur.lua', walkinterval = 2700, speed = 60, talkradius = 3,
			look = {type = 354, head = 1, body = 44, legs = 43, feet = 2}
			},
	{ name = 'Selius', script = 'Chaeck/Selius.lua', walkinterval = 2000, speed = 90,
			look = {type = 493, addons = 2}
			},
	{ name = 'Sewer', script = 'Chaeck/Sewer.lua', speed = 210, talkradius = 3, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 476, head = 40, body = 46, legs = 67, feet = 75, addons = 3}
			},
	{ name = 'Sigurd', script = 'Chaeck/Sigurd.lua', direction = 'east',
			look = {type = 419, addons = 3}
			},
	{ name = 'Sam', script = 'Chaeck/Sam.lua', walkinterval = 5000, speed = 100, talkradius = 2,
			look = {type = 465, head = 39, body = 32, legs = 40, feet = 2}
			},
	{ name = 'Sanyo', script = 'Chaeck/Fisherman.lua', direction = 'east',
			look = {type = 474}
			},
	{ name = 'Salim', script = 'Chaeck/Salim.lua', walkinterval = 15000, speed = 170,
			look = {type = 397, addons = 2}
			},
	{ name = 'Redgard', script = 'Chaeck/Redgard.lua', walkinterval = 2100,
			look = {type = 421}
			},
	{ name = 'Ralker back', protocolName = 'Ralker', protocolNameEnglish = 'Ralker', script = 'Chaeck/RalkerBack.lua', walkinterval = 5000, speed = 70,
			look = {type = 354, head = 78, body = 67, legs = 155, feet = 42, addons = 3}
			},
	{ name = 'Ralker', script = 'Chaeck/Ralker.lua', walkinterval = 5000, speed = 70,
			look = {type = 354, head = 78, body = 67, legs = 155, feet = 42, addons = 3}
			},
	{ name = 'Ocid', script = 'Chaeck/Ocid.lua', walkinterval = 3000,
			look = {type = 354, head = 63, body = 44, legs = 11, feet = 76, addons = 1}
			},
	{ name = 'Mentril', script = 'Chaeck/Mentril.lua', walkinterval = 13000, speed = 90, talkradius = 3, travel = true,
			look = {type = 465, head = 78, body = 79, legs = 115, feet = 96}
			},
	{ name = 'Miriam', script = 'Chaeck/Smocza Grypa.lua', direction = 'east',
			look = {type = 157, head = 53, body = 111, legs = 0, feet = 132, addons = 2}
			},
	{ name = 'Maria', script = 'Chaeck/Maria.lua', walkinterval = 2000, speed = 100,
			look = {type = 148, head = 121, body = 42, legs = 11, feet = 67, addons = 2}
			},
	{ name = 'Livia', script = 'Chaeck/Livia.lua', walkinterval = 2000, speed = 70,
			look = {type = 361, head = 63, body = 44, legs = 11, feet = 76}
			},
	{ name = 'Karmin', script = 'Chaeck/Karmin.lua', speed = 190, talkradius = 3, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 476, head = 78, body = 114, legs = 116, feet = 78, addons = 3}
			},
	{ name = 'Kate', script = 'Chaeck/Kate.lua', walkinterval = 2150,
			look = {type = 138, head = 78, body = 111, legs = 2, feet = 42}
			},
	{ name = 'Albert', script = 'Chaeck/Albert.lua', walkinterval = 3500, speed = 180,
			look = {type = 456, addons = 3}
			},
	{ name = 'Ari', script = 'Chaeck/Outlaw.lua', walkinterval = 9000, speed = 150,
			look = {type = 397}
			},
	{ name = 'Cerber', script = 'Chaeck/Outlaw.lua', walkinterval = 5000, speed = 160,
			look = {type = 391}
			},
	{ name = 'Aron', script = 'Chaeck/Aron.lua', walkinterval = 21000, speed = 90, travel = true,
			look = {type = 478, head = 19, body = 79, legs = 25, feet = 40}
			},
	{ name = 'Athushi', script = 'Chaeck/Fisherman.lua', walkinterval = 4000, speed = 100,
			look = {type = 474, addons = 2}
			},
	{ name = 'Beol', script = 'Chaeck/Beol.lua', speed = 220, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 476, head = 58, body = 60, legs = 10, feet = 43, addons = 3}
			},
	{ name = 'Bryer', script = 'Chaeck/Bryer.lua', direction = 'south',
			look = {type = 509, head = 29, body = 45, legs = 29, feet = 0, addons = 2}
			},
	{ name = 'Challin', script = 'Chaeck/Challin.lua', walkinterval = 2000, speed = 90,
			look = {type = 419, addons = 2}
			},
	{ name = 'Crocodile Gize', script = 'Chaeck/Crocodile Gize.lua', direction = 'east',
			look = {type = 119}
			},
	{ name = 'Derva', script = 'Chaeck/Derva.lua', walkinterval = 4000, speed = 100,
			look = {type = 360, head = 43, body = 58, legs = 12, feet = 99, addons = 3}
			},
	{ name = 'Gandalf', script = 'Chaeck/Gandalf.lua', walkinterval = 2000, speed = 90,
			look = {type = 475}
			},
	{ name = 'Hardouin', script = 'Chaeck/Hardouin.lua', walkinterval = 3000, speed = 200,
			look = {type = 493}
			},
	{ name = 'Henry', script = 'Chaeck/Henry.lua', walkinterval = 3000, speed = 120,
			look = {type = 468, head = 78, body = 14, legs = 29, feet = 98}
			},
			
	-- Cirith
	-- others
	{ name = 'Ragnar', script = 'Cirith/others/Ragnar.lua', walkinterval = 4500, speed = 110, idleImmunity = true,
			look = {type = 422}
			},
	{ name = 'Fand', script = 'Cirith/others/Fand.lua', walkinterval = 4500, speed = 110,
			look = {type = 413, addons = 3}
			},
	{ name = 'Dihsar', script = 'Cirith/others/Dihsar.lua', walkinterval = 4500, speed = 110, idleImmunity = true,
			look = {type = 146, head = 78, body = 31, legs = 32, feet = 11, addons = 3}
			},
	{ name = 'Izavel', script = 'Cirith/others/Izavel.lua', direction = 'west',
			look = {type = 140, head = 41, body = 11, legs = 131, feet = 13, addons = 1}
			},
	{ name = 'Yaze', script = 'Cirith/others/Yaze.lua', direction = 'east', travel = true,
			look = {type = 570, head = 78, body = 9, legs = 42, feet = 47}
			},
	{ name = 'Harnald', script = 'Cirith/others/Harnald.lua', direction = 'north', travel = true,
			look = {type = 509, addons = 2}
			},
	{ name = 'Anja', script = 'Cirith/others/Anja.lua', direction = 'east', travel = true,
			look = {type = 567, head = 3, body = 13, legs = 21, feet = 25}
			},
	{ name = 'Red Baron', script = 'Cirith/others/Red Baron.lua', direction = 'east', travel = true,
			look = {type = 590, head = 59, body = 75, legs = 94, feet = 21}
			},
	{ name = 'Jorten', script = 'Cirith/others/Jorten.lua', direction = 'south',
			look = {type = 453, addons = 2}
			},
	{ name = 'Phassya', script = 'Cirith/others/Phassya.lua', walkinterval = 4500, speed = 110,
			look = {type = 567, head = 11, body = 32, legs = 48, feet = 16}
			},
	{ name = 'Zeghyrdan', script = 'Cirith/others/Zeghyrdan.lua', direction = 'east',
			look = {type = 549, head = 78, body = 22, legs = 30, feet = 12}
			},
	{ name = 'Abithar', script = 'Cirith/others/Abithar.lua', direction = 'west',
			look = {type = 476, head = 77, body = 99, legs = 120, feet = 98}
			},
	{ name = 'Aesop', script = 'Cirith/others/Aesop.lua', direction = 'west',
			look = {type = 328, head = 113, body = 42, legs = 39, feet = 26}
			},
	{ name = 'Ariane', script = 'Cirith/others/Ariane.lua', walkinterval = 4500, speed = 110,
			look = {type = 360, head = 21, body = 36, legs = 21, feet = 60, addons = 3}
			},
	{ name = 'Arnald', script = 'Cirith/others/Arnald.lua', direction = 'south',
			look = {type = 403, head = 78, body = 1, legs = 59, feet = 58, addons = 3}
			},
	{ name = 'Arvis', script = 'Cirith/others/Arvis.lua', walkinterval = 3000, speed = 100,
			look = {type = 159, head = 78, body = 97, legs = 96, feet = 114}
			},
	{ name = 'Brigida', script = 'Cirith/others/Brigida.lua', direction = 'north',
			look = {type = 157, head = 43, body = 77, legs = 78, feet = 33, addons = 2}
			},
	{ name = 'Chares', script = 'Cirith/others/Chares.lua', direction = 'west',
			look = {type = 510, head = 56, body = 47, legs = 95, feet = 113, addons = 2}
			},
	{ name = 'Cyprianus', script = 'Cirith/others/Cyprianus.lua', direction = 'south',
			look = {type = 470, addons = 1}
			},
	{ name = 'Belona', script = 'Cirith/others/Belona.lua', direction = 'south',
			look = {type = 354, head = 116, body = 99, legs = 59, feet = 114, addons = 3}
			},
	{ name = 'Dethron', script = 'Cirith/others/Dethron.lua', walkinterval = 10000, speed = 130,
			look = {type = 476, head = 96, body = 30, legs = 75, feet = 76}
			},
	{ name = 'Erramun', script = 'Cirith/others/Erramun.lua', direction = 'south',
			look = {type = 403, head = 78, body = 1, legs = 37, feet = 39, addons = 3}
			},
	{ name = 'Flaen', script = 'Cirith/others/Flaen.lua', walkinterval = 4000, speed = 100,
			look = {type = 397, addons = 2}
			},
	{ name = 'Francois', script = 'Cirith/others/Francois.lua', walkinterval = 2000, speed = 130,
			look = {type = 509, head = 16, body = 18, legs = 64, feet = 66, addons = 3}
			},
	{ name = 'Gaiane', script = 'Cirith/others/Gaiane.lua', walkinterval = 5000, speed = 100,
			look = {type = 511, head = 27, body = 27, legs = 57, feet = 33, addons = 3}
			},
	{ name = 'Gersenda', script = 'Cirith/others/Gersenda.lua', direction = 'east',
			look = {type = 511, head = 49, body = 31, legs = 115, feet = 77, addons = 1}
			},
	{ name = 'Haszkel', script = 'Cirith/others/Haszkel.lua', walkinterval = 2000, speed = 70,
			look = {type = 360, head = 25, body = 90, legs = 16, feet = 79, addons = 3}
			},
	{ name = 'Hermana', script = 'Cirith/others/Hermana.lua', walkinterval = 10000, speed = 90,
			look = {type = 360, head = 22, body = 13, legs = 93, feet = 112}
			},
	{ name = 'Hyllus', script = 'Cirith/others/Hyllus.lua', direction = 'east',
			look = {type = 453, addons = 3}
			},
	{ name = 'Utu', script = 'Cirith/others/Utu.lua', direction = 'east',
			look = {type = 453, addons = 1}
			},
	{ name = 'Veer', script = 'Cirith/others/Veer.lua', walkinterval = 1700, speed = 110,
			look = {type = 473, addons = 1}
			},
	{ name = 'Ziggy', script = 'Cirith/others/Ziggy.lua', direction = 'east',
			look = {type = 346, addons = 2}
			},
	{ name = 'Ignatius', script = 'Cirith/others/Ignatius.lua', direction = 'south',
			look = {type = 403, head = 78, body = 42, legs = 22, feet = 24, addons = 1}
			},
	{ name = 'Iolanthe', script = 'Cirith/others/Iolanthe.lua', direction = 'south',
			look = {type = 140, head = 29, body = 38, legs = 17, feet = 48, addons = 3}
			},
	{ name = 'Isumson', script = 'Cirith/others/Isumson.lua', walkinterval = 8000, speed = 80,
			look = {type = 407, head = 95, body = 10, legs = 115, feet = 3, addons = 1}
			},
	{ name = 'Jaremar', script = 'Cirith/others/Jaremar.lua', walkinterval = 2300,
			look = {type = 509, head = 15, body = 76, legs = 84, feet = 90, addons = 3}
			},
	{ name = 'Juan', script = 'Cirith/others/Juan.lua', walkinterval = 1700, speed = 110,
			look = {type = 328, head = 12, body = 62, legs = 31, feet = 98}
			},
	{ name = 'Kabathar', script = 'Cirith/others/Kabathar.lua', walkinterval = 2000, speed = 100, travel = true,
			look = {type = 509, head = 78, body = 79, legs = 33, feet = 114, addons = 2}
			},
	{ name = 'Kerena', script = 'Cirith/others/Kerena.lua', walkinterval = 2000, speed = 100,
			look = {type = 160, head = 78, body = 90, legs = 54, feet = 115}
			},
	{ name = 'Kethor', script = 'Cirith/others/Kethor.lua', walkinterval = 8000, speed = 80,
			look = {type = 407, head = 13, body = 23, legs = 39, feet = 59, addons = 1}
			},
	{ name = 'Laelius', script = 'Cirith/others/Laelius.lua', walkinterval = 11000, speed = 130,
			look = {type = 146, head = 2, body = 93, legs = 12, feet = 59, addons = 2}
			},
	{ name = 'Livian', script = 'Cirith/others/Livian.lua', walkinterval = 10000, speed = 70,
			look = {type = 360, head = 46, body = 117, legs = 114, feet = 10, addons = 1}
			},
	{ name = 'Marian', script = 'Cirith/others/Marian.lua', direction = 'south',
			look = {type = 403, head = 78, body = 43, legs = 0, feet = 0}
			},
	{ name = 'Motte', script = 'Cirith/others/Motte.lua', direction = 'south',
			look = {type = 403, head = 95, body = 62, legs = 79, feet = 24}
			},
	{ name = 'Nicodemus', script = 'Cirith/others/Nicodemus.lua', direction = 'west',
			look = {type = 468, head = 6, body = 28, legs = 48, feet = 51}
			},
	{ name = 'Okenos', script = 'Cirith/others/Okenos.lua', walkinterval = 8000, speed = 80,
			look = {type = 354, head = 85, body = 68, legs = 11, feet = 120, addons = 3}
			},
	{ name = 'Petr', script = 'Cirith/others/Petr.lua', walkinterval = 8000, speed = 100, idleImmunity = true, travel = true,
			look = {type = 151, head = 37, body = 20, legs = 7, feet = 51, addons = 3}
			},
	{ name = 'Petr Orilion', protocolName = 'Petr', protocolNameEnglish = 'Petr', script = 'Cirith/others/Petr Orilion.lua', direction = 'south', travel = true,
			look = {type = 151, head = 37, body = 20, legs = 7, feet = 51, addons = 3}
			},
	{ name = 'Quilse', script = 'Cirith/others/Quilse.lua', direction = 'east', lightColor = 206, intensity = 14,
			look = {type = 471, addons = 3}
			},
	{ name = 'Rahel', script = 'Cirith/others/Rahel.lua', walkinterval = 5000, speed = 100,
			look = {type = 512, head = 77, body = 34, legs = 41, feet = 114, addons = 2}
			},
	{ name = 'Redever', script = 'Cirith/others/Redever.lua', walkinterval = 8000, speed = 80,
			look = {type = 407, head = 77, body = 52, legs = 57, feet = 53, addons = 1}
			},
	{ name = 'Redrich', script = 'Cirith/others/Redrich.lua', walkinterval = 8000, speed = 80,
			look = {type = 407, head = 115, body = 47, legs = 30, feet = 26, addons = 1}
			},
	{ name = 'Rhoben', script = 'Cirith/others/Rhoben.lua', walkinterval = 2000, speed = 120,
			look = {type = 476, head = 78, body = 61, legs = 102, feet = 115}
			},
	{ name = 'Salomon', script = 'Cirith/others/Salomon.lua', walkinterval = 7800, speed = 110,
			look = {type = 413, head = 132, body = 89, legs = 79, feet = 35}
			},
	{ name = 'Saral', script = 'Cirith/others/Saral.lua', direction = 'east', lightColor = 206, intensity = 14,
			look = {type = 472, addons = 3}
			},
	{ name = 'Shaira', script = 'Cirith/others/Shaira.lua', walkinterval = 2000, speed = 70,
			look = {type = 138, head = 76, body = 11, legs = 45, feet = 64, addons = 3}
			},
	{ name = 'Sharp Blade', script = 'Cirith/others/Sharp Blade.lua', direction = 'east', travel = true,
			look = {type = 151, head = 78, body = 4, legs = 42, feet = 95, addons = 2}
			},
	{ name = 'Silvius', script = 'Cirith/others/Silvius.lua', walkinterval = 3000, speed = 110,
			look = {type = 354, head = 64, body = 87, legs = 80, feet = 100, addons = 3}
			},
	{ name = 'Theudofrid', script = 'Cirith/others/Theudofrid.lua', direction = 'south',
			look = {type = 354, head = 60, body = 108, legs = 76, feet = 10, addons = 3}
			},
	
	-- misc
	{ name = 'Falimar', script = 'Cirith/Falimar.lua', direction = 'west',
			look = {type = 456, addons = 3}
			},
	{ name = 'Adarth', script = 'Cirith/Adarth.lua', walkinterval = 4000, speed = 100,
			look = {type = 354, head = 21, body = 44, legs = 113, feet = 26, addons = 2}
			},
	{ name = 'Chalcil', script = 'Cirith/Chalcil.lua', walkinterval = 7500, speed = 110,
			look = {type = 567, head = 132, body = 19, legs = 57, feet = 39, addons = 1}
			},
	{ name = 'Aldape', script = 'Cirith/Aldape.lua', walkinterval = 12000, speed = 90,
			look = {type = 478, head = 19, body = 100, legs = 29, feet = 61, addons = 2}
			},
	{ name = 'Chantalle', script = 'Cirith/Chantalle.lua', walkinterval = 4500, speed = 120, canwalkanywhere = true, idleImmunity = true,
			look = {type = 361, head = 77, body = 93, legs = 79, feet = 77, addons = 3}
			},
	{ name = 'Damord', script = 'Cirith/Damord.lua', direction = 'south',
			look = {type = 491, addons = 2}
			},
	{ name = 'Devo', script = 'Cirith/Devo.lua', walkinterval = 8000, speed = 60,
			look = {type = 492, head = 95, body = 95, legs = 95, feet = 95}
			},
	{ name = 'Ductor', script = 'Cirith/Ductor.lua', walkinterval = 12000, speed = 90,
			look = {type = 468, head = 100, body = 76, legs = 41, feet = 33}
			},
	{ name = 'Elenor', script = 'Cirith/Elenor.lua', walkinterval = 5100, speed = 60,
			look = {type = 470, addons = 3}
			},
	{ name = 'Evraal', script = 'Cirith/Evraal.lua', walkinterval = 25000, speed = 210,
			look = {type = 465, addons = 3}
			},
	{ name = 'Faramond', script = 'Cirith/Faramond.lua', walkinterval = 8000, speed = 100,
			look = {type = 490}
			},
	{ name = 'Galnar', script = 'Cirith/Galnar.lua', direction = 'east',
			look = {type = 491, addons = 3}
			},
	{ name = 'Garandriel', script = 'Cirith/Garandriel.lua', walkinterval = 10000, speed = 80,
			look = {type = 397, addons = 2}
			},
	{ name = 'Gerwazy', script = 'Cirith/Gerwazy.lua', walkinterval = 13000, speed = 90,
			look = {type = 465, head = 78, body = 79, legs = 115, feet = 96}
			},
	{ name = 'Guerran', script = 'Cirith/Guerran.lua', direction = 'east',
			look = {type = 354, head = 0, body = 10, legs = 13, feet = 9, addons = 3}
			},
	{ name = 'Half Living Kentaru', protocolName = 'Kentaru', protocolNameEnglish = 'Kentaru', script = 'Cirith/Half Living Kentaru.lua',
			look = {typeex = 1766}
			},
	{ name = 'Half Living Walking Kentaru', protocolName = 'Kentaru', protocolNameEnglish = 'Kentaru', script = 'Cirith/Half Living Walking Kentaru.lua', speed = 300,
			look = {type = 397}
			},
	{ name = 'Harold', script = 'Cirith/Harold.lua', walkinterval = 5000, speed = 120,
			look = {type = 509, head = 98, body = 40, legs = 48, feet = 117, addons = 3}
			},
	{ name = 'Harry', script = 'Cirith/Harry.lua', direction = 'west',
			look = {type = 478, head = 39, body = 117, legs = 132, feet = 118}
			},
	{ name = 'Hayn', script = 'Cirith/Hayn.lua', direction = 'south', lightColor = 206, intensity = 14,
			look = {type = 471, addons = 1}
			},
	{ name = 'Hera', script = 'Cirith/Hera.lua', walkinterval = 6000, speed = 90,
			look = {type = 512, addons = 3}
			},
	{ name = 'Jail Guardian', protocolName = 'Stra�nik', protocolNameEnglish = 'Guardian', script = 'Cirith/Guardian.lua', speed = 165,
			look = {type = 493, addons = 3}
			},
	{ name = 'Jail Guardians', protocolName = 'Stra�nik', protocolNameEnglish = 'Guardian', script = 'Cirith/Guardians.lua', direction = 'east',
			look = {type = 493, addons = 3}
			},
	{ name = 'Julian', script = 'Cirith/Julian.lua', walkinterval = 4000, speed = 90,
			look = {type = 490, addons = 2}
			},
	{ name = 'Kael', script = 'Cirith/Kael.lua', walkinterval = 7000, speed = 100,
			look = {type = 509, addons = 2}
			},
	{ name = 'Karim', script = 'Cirith/Karim.lua', opacity = 45,
			look = {type = 489, addons = 3}
			},
	{ name = 'Kentaru NPC', protocolName = 'Kentaru', protocolNameEnglish = 'Kentaru', script = 'Cirith/Kentaru.lua',
			look = {type = 397}
			},
	{ name = 'Klein', script = 'Cirith/Klein.lua', walkinterval = 6000, speed = 120,
			look = {type = 510, head = 20, body = 49, legs = 60, feet = 5, addons = 3}
			},
	{ name = 'Konan', script = 'Cirith/Konan.lua', walkinterval = 8700, speed = 90,
			look = {type = 510, head = 3, body = 45, legs = 111, feet = 76, addons = 2}
			},
	{ name = 'Lara', script = 'Cirith/Lara.lua', walkinterval = 2000, speed = 70,
			look = {type = 157, head = 104, body = 100, legs = 4, feet = 6, addons = 2}
			},
	{ name = 'Lavue', script = 'Cirith/Lavue.lua', direction = 'east', lightColor = 206, intensity = 14,
			look = {type = 472, addons = 3}
			},
	{ name = 'Liakopulos', script = 'Cirith/Liakopulos.lua', walkinterval = 5100, speed = 60,
			look = {type = 470, addons = 2}
			},
	{ name = 'Liliet', script = 'Cirith/Liliet.lua', walkinterval = 1900,
			look = {type = 360, head = 8, body = 19, legs = 23, feet = 1, addons = 1}
			},
	{ name = 'Louis', script = 'Louis.lua', walkinterval = 2000,
			look = {type = 130, head = 19, body = 114, legs = 113, feet = 94, addons = 2}
			},
	{ name = 'Lovi', script = 'Cirith/Lovi.lua', walkinterval = 3500, speed = 120,
			look = {type = 453, addons = 3}
			},
	{ name = 'Madabolt', script = 'Cirith/Madabolt.lua', walkinterval = 5000, speed = 100,
			look = {type = 419, addons = 3}
			},
	{ name = 'Malia', script = 'Cirith/Malia.lua', walkinterval = 7500, speed = 110,
			look = {type = 567, head = 10, body = 56, legs = 31, feet = 20, addons = 1}
			},
	{ name = 'Maron', script = 'Cirith/Maron.lua', walkinterval = 5100, speed = 60,
			look = {type = 470, addons = 1}
			},
	{ name = 'Mefisto', script = 'Cirith/Mefisto.lua', walkinterval = 5100, speed = 60,
			look = {type = 492, addons = 3}
			},
	{ name = 'Meibcil', script = 'meibcil.lua', walkinterval = 2000,
			look = {type = 408, head = 97, body = 40, legs = 22, feet = 78, addons = 1}
			},
	{ name = 'Meibham', script = 'meibham.lua', walkinterval = 2000,
			look = {type = 408, head = 115, body = 11, legs = 0, feet = 49, addons = 2}
			},
	{ name = 'Pauline', script = 'Cirith/Pauline.lua', walkinterval = 10000, speed = 70,
			look = {type = 360, head = 82, body = 32, legs = 16, feet = 8, addons = 2}
			},
	{ name = 'Onald', script = 'Cirith/Onald.lua', walkinterval = 10000, speed = 70, travel = true,
			look = {type = 407, head = 13, body = 23, legs = 39, feet = 59}
			},
	{ name = 'Manten', script = 'Cirith/Manten.lua', walkinterval = 6000, speed = 90, travel = true,
			look = {type = 490}
			},
	{ name = 'Oozaru NPC', script = 'OozaruNpc.lua', walkinterval = 2200,
			look = {type = 509, head = 79, body = 37, legs = 79, feet = 57, addons = 3}
			},
	{ name = 'Philon', script = 'Cirith/Philon.lua', walkinterval = 4500, speed = 100,
			look = {type = 407, head = 41, body = 5, legs = 58, feet = 115}
			},
	{ name = 'Polifem NPC', protocolName = 'Polifem', protocolNameEnglish = 'Polifem', script = 'Cirith/Polifem.lua', walkinterval = 1700, talkradius = 6,
			look = {type = 22}
			},
	{ name = 'Rafael', script = 'Cirith/Rafael.lua', walkinterval = 7000, speed = 100,
			look = {type = 419, addons = 3}
			},
	{ name = 'Reina', script = 'Cirith/Reina.lua', walkinterval = 1900,
			look = {type = 360, head = 9, body = 21, legs = 21, feet = 4, addons = 1}
			},
	{ name = 'Renna', script = 'Cirith/Renna.lua', walkinterval = 10000, speed = 90,
			look = {type = 511, head = 25, body = 35, legs = 45, feet = 55, addons = 1}
			},
	{ name = 'Rhenna', script = 'Rhenna.lua', walkinterval = 2000,
			look = {type = 138, head = 78, body = 48, legs = 21, feet = 49, addons = 3}
			},
	{ name = 'Risinger', script = 'Cirith/Risinger.lua', walkinterval = 12000, speed = 90,
			look = {type = 407, head = 25, body = 97, legs = 155, feet = 4, addons = 1}
			},
	{ name = 'Roland', script = 'Cirith/Roland.lua', walkinterval = 12000, speed = 90,
			look = {type = 478, head = 0, body = 104, legs = 42, feet = 38}
			},
	{ name = 'Saturas', script = 'Cirith/Saturas.lua', walkinterval = 5000, speed = 60,
			look = {type = 470}
			},
	{ name = 'Sedhor', script = 'Cirith/Sedhor.lua', walkinterval = 2000, speed = 100,
			look = {type = 465, head = 59, body = 68, legs = 55, feet = 114}
			},
	{ name = 'Sedron', script = 'Cirith/Sedron.lua', walkinterval = 5100, speed = 60,
			look = {type = 492, addons = 2}
			},
	{ name = 'Dusek', script = 'Cirith/Dusek.lua', direction = 'south',
			look = {type = 380}
			},
	{ name = 'Semmir', script = 'Cirith/Semmir.lua', direction = 'west',
			look = {type = 468, head = 40, body = 95, legs = 68, feet = 0}
			},
	{ name = 'Slain Kentaru', protocolName = 'Kentaru', script = 'Cirith/Slain Kentaru.lua',
			look = {typeex = 3058}
			},
	{ name = 'Sollya', script = 'Cirith/Sollya.lua', walkinterval = 4500, speed = 100,
			look = {type = 361, addons = 1}
			},
	{ name = 'Stailim', script = 'Cirith/Stailim.lua', walkinterval = 7500, speed = 110,
			look = {type = 470, addons = 2}
			},
	{ name = 'Sygina', script = 'Cirith/Sygina.lua', direction = 'east',
			look = {type = 511, head = 25, body = 30, legs = 35, feet = 40, addons = 1}
			},
	{ name = 'Syslaven', script = 'Cirith/Syslaven.lua', walkinterval = 10000, speed = 70, travel = true,
			look = {type = 397, addons = 2}
			},
	{ name = 'Test', script = 'test.lua',
			look = {type = 125, head = 82, body = 65, legs = 6, feet = 76, addons = 2}
			},
	{ name = 'Thaomir', script = 'Cirith/Thaomir.lua', walkinterval = 3500, speed = 80,
			look = {type = 510, head = 0, body = 65, legs = 22, feet = 100, addons = 2}
			},
	{ name = 'Tiglight', script = 'Cirith/Semmir.lua', direction = 'east',
			look = {type = 354}
			},
	{ name = 'Tobias', script = 'Cirith/Tobias.lua', walkinterval = 6000, speed = 90,
			look = {type = 133, head = 78, body = 108, legs = 96, feet = 29, addons = 3}
			},
	{ name = 'Tran', script = 'Cirith/Tran.lua', walkinterval = 1900,
			look = {type = 273, head = 97, body = 89, legs = 92, feet = 80, addons = 2}
			},
	{ name = 'Uras', script = 'Cirith/Uras.lua', walkinterval = 12000, speed = 90,
			look = {type = 478, head = 35, body = 96, legs = 15, feet = 105, addons = 3}
			},
	{ name = 'Vedget', script = 'Cirith/Vedget.lua', walkinterval = 21000, speed = 90,
			look = {type = 478, head = 21, body = 79, legs = 24, feet = 67}
			},
	{ name = 'Vill', script = 'Cirith/Semmir.lua', walkinterval = 5000, speed = 100,
			look = {type = 354, head = 25, body = 50, legs = 75, feet = 100, addons = 3}
			},
	{ name = 'Virtelion', script = 'Cirith/Virtelion.lua', walkinterval = 7500, speed = 110,
			look = {type = 570, head = 78, body = 24, legs = 57, feet = 1, addons = 1}
			},
	{ name = 'Vonten', script = 'Cirith/Vonten.lua', walkinterval = 5100, speed = 60,
			look = {type = 453, addons = 3}
			},
	{ name = 'Hieronim', script = 'Cirith/Hieronim.lua', walkinterval = 12000, speed = 90,
			look = {type = 490}
			},
	{ name = 'Kastinan', script = 'Cirith/Kastinan.lua', speed = 220, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 543, head = 114, body = 41, legs = 58, feet = 115}
			},
	{ name = 'Larenton', script = 'Cirith/Larenton.lua', direction = 'north',
			look = {type = 585}
			},
			
	-- crowd
	{ name = 'Bawlor', script = 'Cirith/crowd/Bawlor.lua', direction = 'south',
			look = {type = 346, addons = 2}
			},
	{ name = 'Andre', script = 'Cirith/crowd/Andre.lua', direction = 'east',
			look = {type = 472, addons = 3}
			},
	{ name = 'Axerk', script = 'Cirith/crowd/Axerk.lua', direction = 'south',
			look = {type = 381}
			},
	{ name = 'Baya', script = 'Cirith/crowd/Baya.lua', walkinterval = 8000, speed = 90,
			look = {type = 270, head = 48, body = 95, legs = 104, feet = 114, addons = 1}
			},
	{ name = 'Concio', script = 'Cirith/crowd/Concio.lua', direction = 'south',
			look = {type = 568, head = 36, body = 3, legs = 12, feet = 39, addons = 3}
			},
	{ name = 'Cuthbert', script = 'Cirith/crowd/Cuthbert.lua', direction = 'south',
			look = {type = 354, head = 19, body = 63, legs = 11, feet = 25, addons = 3}
			},
	{ name = 'Danel', script = 'Cirith/crowd/Danel.lua', direction = 'south',
			look = {type = 346, addons = 3}
			},
	{ name = 'Dena', script = 'Cirith/crowd/Dena.lua', walkinterval = 8000, speed = 90,
			look = {type = 360, addons = 2}
			},
	{ name = 'Drazen', script = 'Cirith/crowd/Drazen.lua', direction = 'east',
			look = {type = 151, head = 35, body = 25, legs = 30, feet = 33, addons = 2}
			},
	{ name = 'Ember', script = 'Cirith/crowd/Ember.lua', direction = 'south',
			look = {type = 157, head = 37, body = 18, legs = 28, feet = 78}
			},
	{ name = 'Florian', script = 'Cirith/crowd/Florian.lua', direction = 'east',
			look = {type = 490, addons = 2}
			},
	{ name = 'Galileo', script = 'Cirith/crowd/Galileo.lua', direction = 'south',
			look = {type = 491, addons = 2}
			},
	{ name = 'Garnart', script = 'Cirith/crowd/Garnart.lua', walkinterval = 9500, speed = 90,
			look = {type = 359}
			},
	{ name = 'Genovefa', script = 'Cirith/crowd/Genovefa.lua', walkinterval = 3000, speed = 100,
			look = {type = 360, head = 10, body = 11, legs = 57, feet = 35}
			},
	{ name = 'Hasdrubal', script = 'Cirith/crowd/Hasdrubal.lua', direction = 'south',
			look = {type = 419, addons = 2}
			},
	{ name = 'Hoysus', script = 'Cirith/crowd/Hoysus.lua', walkinterval = 10000, speed = 70,
			look = {type = 403, head = 114, body = 27, legs = 118, feet = 30}
			},
	{ name = 'Jana', script = 'Cirith/crowd/Jana.lua', walkinterval = 8000, speed = 80,
			look = {type = 141, head = 116, body = 59, legs = 4, feet = 114}
			},
	{ name = 'Kahina', script = 'Cirith/crowd/Kahina.lua', walkinterval = 11000, speed = 60,
			look = {type = 270, head = 111, body = 29, legs = 113, feet = 34, addons = 2}
			},
	{ name = 'Khoz', script = 'Cirith/crowd/Khoz.lua', walkinterval = 8000, speed = 80,
			look = {type = 354, head = 5, body = 106, legs = 65, feet = 44, addons = 3}
			},
	{ name = 'Klement', script = 'Cirith/crowd/Klement.lua', direction = 'west',
			look = {type = 474}
			},
	{ name = 'Lodmon', script = 'Cirith/crowd/Lodmon.lua', walkinterval = 11000, speed = 60,
			look = {type = 413, head = 59, body = 50, legs = 22, feet = 119}
			},
	{ name = 'Mia', script = 'Cirith/crowd/Mia.lua', direction = 'west',
			look = {type = 360, head = 74, body = 15, legs = 72, feet = 91, addons = 1}
			},
	{ name = 'Mollie', script = 'Cirith/crowd/Mollie.lua', direction = 'south',
			look = {type = 511, head = 32, body = 120, legs = 96, feet = 30, addons = 1}
			},
	{ name = 'Naffain', script = 'Cirith/crowd/Naffain.lua', walkinterval = 5000, speed = 100,
			look = {type = 555, head = 97, body = 11, legs = 23, feet = 3}
			},
	{ name = 'Probo', script = 'Cirith/crowd/Probo.lua', direction = 'south',
			look = {type = 472, addons = 1}
			},
	{ name = 'Rumir', script = 'Cirith/crowd/Rumir.lua', walkinterval = 7000, speed = 100,
			look = {type = 403, head = 78, body = 59, legs = 49, feet = 24}
			},
	{ name = 'Sabbatius', script = 'Cirith/crowd/Sabbatius.lua', walkinterval = 5000, speed = 90,
			look = {type = 456, addons = 3}
			},
	{ name = 'Svela', script = 'Cirith/crowd/Svela.lua', walkinterval = 10000, speed = 100,
			look = {type = 422}
			},
	{ name = 'Terpandro', script = 'Cirith/crowd/Terpandro.lua', direction = 'east',
			look = {type = 472, addons = 1}
			},
	{ name = 'Timmu', script = 'Cirith/crowd/Timmu.lua', direction = 'east',
			look = {type = 570, head = 0, body = 27, legs = 49, feet = 39}
			},
	{ name = 'Tina', script = 'Cirith/crowd/Tina.lua', walkinterval = 5000, speed = 90,
			look = {type = 511, addons = 1}
			},
	{ name = 'Toby', script = 'Cirith/crowd/Toby.lua', walkinterval = 6000, speed = 70,
			look = {type = 475}
			},
	{ name = 'Uray', script = 'Cirith/crowd/Uray.lua', walkinterval = 4000, speed = 90,
			look = {type = 543, head = 116, body = 38, legs = 23, feet = 18}
			},
	{ name = 'Valens', script = 'Cirith/crowd/Valens.lua', direction = 'south',
			look = {type = 491, addons = 1}
			},
	{ name = 'Valeriana', script = 'Cirith/crowd/Valeriana.lua', direction = 'south',
			look = {type = 567, head = 15, body = 39, legs = 24, feet = 60, addons = 2}
			},
	{ name = 'Wade', script = 'Cirith/crowd/Wade.lua', direction = 'south',
			look = {type = 354, head = 99, body = 102, legs = 116, feet = 78, addons = 2}
			},
	{ name = 'Warten', script = 'Cirith/crowd/Warten.lua', speed = 200, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 403, head = 58, body = 118, legs = 17, feet = 58, addons = 2}
			},
	{ name = 'Wrad', script = 'Cirith/crowd/Wrad.lua', direction = 'south',
			look = {type = 380}
			},
			
	-- new home
	{ name = 'Alp', script = 'Cirith/NewHome/Alp.lua', walkinterval = 4000, speed = 180,
			look = {type = 251, head = 115, body = 49, legs = 28, feet = 22, addons = 3}
			},
	{ name = 'Osmo', script = 'Cirith/NewHome/Osmo.lua', speed = 0, canwalkanywhere = true, idleImmunity = true,
			look = {type = 251, head = 95, body = 7, legs = 26, feet = 10, addons = 3}
			},
	{ name = 'Osmo Guard', protocolName = 'Osmo', protocolNameEnglish = 'Osmo', script = 'Cirith/NewHome/Osmo.lua', speed = 0,
			look = {type = 251, head = 95, body = 7, legs = 26, feet = 10, addons = 3}
			},
	{ name = 'Roberth', script = 'Cirith/NewHome/Roberth.lua', direction = 'west',
			look = {type = 382, addons = 2}
			},
	{ name = 'Assith', script = 'Cirith/NewHome/Assith.lua', walkinterval = 5000, speed = 190,
			look = {type = 552, head = 78, body = 0, legs = 57, feet = 24}
			},
	{ name = 'Bartolomeus', script = 'Cirith/NewHome/Bartolomeus.lua', walkinterval = 5000, speed = 120,
			look = {type = 490, addons = 2}
			},
	{ name = 'Beliba', script = 'Cirith/NewHome/Beliba.lua', direction = 'west',
			look = {type = 382, addons = 3}
			},
	{ name = 'Dijkstra', script = 'Cirith/NewHome/Dijkstra.lua', walkinterval = 6000, speed = 200,
			look = {type = 476, head = 13, body = 19, legs = 22, feet = 99}
			},
	{ name = 'Fisher', script = 'Cirith/NewHome/Fisher.lua', talkradius = 4,
			look = {type = 509, head = 25, body = 35, legs = 41, feet = 44, addons = 2}
			},
	{ name = 'Hyphving', script = 'Cirith/NewHome/Hyphving.lua', direction = 'north',
			look = {type = 346}
			},
	{ name = 'Luka', script = 'Cirith/NewHome/Luka.lua', direction = 'south', talkradius = 4, lightColor = 206, intensity = 14,
			look = {type = 472, addons = 3}
			},
	{ name = 'Pawahtuun NPC', protocolName = 'Pawahtuun', protocolNameEnglish = 'Pawahtuun', script = 'Cirith/NewHome/Pawahtuun.lua', opacity = 45, talkradius = 6,
			look = {type = 193}
			},
	{ name = 'Vernon', script = 'Cirith/NewHome/Vernon.lua', direction = 'south',
			look = {type = 490, addons = 2}
			},
	{ name = 'Zephyros', script = 'Cirith/NewHome/Zephyros.lua', direction = 'south',
			look = {type = 510, addons = 2}
			},
	
	-- quests
	{ name = 'Cyrian', script = 'Cirith/quests/Cyrian.lua', direction = 'west',
			look = {type = 491, addons = 2}
			},
	{ name = 'Ibrahim', script = 'Cirith/quests/Ibrahim.lua', direction = 'east',
			look = {type = 584}
			},
	{ name = 'Unvensen', script = 'Cirith/quests/Unvensen.lua', direction = 'north',
			look = {type = 462}
			},
	{ name = 'Bianka', script = 'Cirith/quests/Bianka.lua', walkinterval = 9000, speed = 100,
			look = {type = 360, head = 101, body = 43, legs = 115, feet = 61, addons = 3}
			},
	{ name = 'Clerebold', script = 'Cirith/quests/Clerebold.lua', walkinterval = 3500, speed = 60,
			look = {type = 354, head = 60, body = 67, legs = 97, feet = 76, addons = 2}
			},
	{ name = 'Daggerspell', script = 'Cirith/quests/Daggerspell.lua', walkinterval = 7000, speed = 100,
			look = {type = 462, addons = 1}
			},
	{ name = 'Dantven', script = 'Cirith/quests/Dantven.lua', walkinterval = 10000, speed = 100,
			look = {type = 598, head = 72, body = 66, legs = 7, feet = 26, addons = 1}
			},
	{ name = 'Den', script = 'Cirith/quests/Den.lua', walkinterval = 7000, speed = 100,
			look = {type = 552, head = 60, body = 116, legs = 22, feet = 59}
			},
	{ name = 'Edvard', script = 'Cirith/quests/Edvard.lua', walkinterval = 2000, speed = 90,
			look = {type = 568, head = 114, body = 97, legs = 116, feet = 119, addons = 1}
			},
	{ name = 'Grunfeld', script = 'Cirith/quests/Grunfeld.lua', direction = 'west',
			look = {type = 470, addons = 1}
			},
	{ name = 'Hawkyr', script = 'Cirith/quests/Hawkyr.lua', walkinterval = 7000, speed = 100,
			look = {type = 517}
			},
	{ name = 'Jenny', script = 'Cirith/quests/Jenny.lua', walkinterval = 12000, speed = 70,
			look = {type = 360, head = 37, body = 13, legs = 43, feet = 26, addons = 3}
			},
	{ name = 'Keltus', script = 'Cirith/quests/Keltus.lua',
			look = {type = 0, auxType = 15575, typeex = 16497}
			},
	{ name = 'Kyjun', script = 'Cirith/quests/Kyjun.lua', direction = 'east',
			look = {type = 403, head = 10, body = 76, legs = 124, feet = 28}
			},
	{ name = 'Lawrence Puppet', protocolName = 'Pupil Lawrenca', script = 'Cirith/quests/Lawrence Puppet.lua', speed = 300, idleImmunity = true,
			look = {type = 406}
			},
	{ name = 'Lawrence', script = 'Cirith/quests/Lawrence.lua', speed = 300, canwalkanywhere = true, idleImmunity = true, lightColor = 206, intensity = 14,
			look = {type = 405}
			},
	{ name = 'Lorkan Hak', script = 'Cirith/quests/Lorkan Hak.lua', speed = 190, talkradius = 4, idleImmunity = true,
			look = {type = 474, addons = 3}
			},
	{ name = 'Pavish', script = 'Cirith/quests/Pavish.lua', walkinterval = 2000, speed = 90,
			look = {type = 419, addons = 3}
			},
	{ name = 'Proclay', script = 'Cirith/quests/Proclay.lua', walkinterval = 5500, speed = 100,
			look = {type = 599, head = 116, body = 11, legs = 116, feet = 85, addons = 2}
			},
	{ name = 'Qadir', script = 'Cirith/quests/Qadir.lua', direction = 'west',
			look = {type = 464}
			},
	{ name = 'Sajer', script = 'Cirith/quests/Sajer.lua',
			look = {type = 0, auxType = 15575, typeex = 16497}
			},
	{ name = 'Sebris', script = 'Cirith/quests/Sebris.lua', direction = 'south',
			look = {type = 403, head = 61, body = 48, legs = 23, feet = 116}
			},
	{ name = 'Tarnaelt', script = 'Cirith/quests/Tarnaelt.lua', direction = 'south',
			look = {type = 476, head = 95, body = 101, legs = 62, feet = 99}
			},
	{ name = 'Thelber', script = 'Cirith/quests/Thelber.lua', walkinterval = 2000, speed = 80,
			look = {type = 354, head = 25, body = 31, legs = 37, feet = 45, addons = 3}
			},
	{ name = 'Treebeard NPC', protocolName = 'Praojciec', protocolNameEnglish = 'Forefather', script = 'Cirith/quests/Treebeard.lua', talkradius = 6,
			look = {type = 480}
			},
	{ name = 'Verena', script = 'Cirith/quests/Verena.lua', direction = 'west',
			look = {type = 141, head = 38, body = 105, legs = 30, feet = 76}
			},
	{ name = 'Puchamed', script = 'Cirith/quests/Puchamed.lua', walkinterval = 4000, speed = 160,
			look = {type = 541, head = 97, body = 21, legs = 58, feet = 115, addons = 1}
			},
	{ name = 'Speaker', protocolName = 'M�wca', script = 'Cirith/quests/Speaker.lua', direction = 'south',
			look = {type = 599, head = 1, body = 2, legs = 3, feet = 4, addons = 2}
			},
	{ name = 'Erthil', script = 'Cirith/quests/Erthil.lua', direction = 'north',
			look = {type = 541, head = 114, body = 58, legs = 38, feet = 96, addons = 2}
			},
	{ name = 'Erthil War', protocolName = 'Erthil', protocolNameEnglish = 'Erthil', script = 'Cirith/quests/Erthil.lua', direction = 'east',
			look = {type = 541, head = 114, body = 58, legs = 38, feet = 96, addons = 2}
			},
	{ name = 'Ghrobar', script = 'Cirith/quests/Ghrobar.lua', direction = 'south',
			look = {type = 478, head = 78, body = 59, legs = 61, feet = 115}
			},
	{ name = 'Ghrobar War', protocolName = 'Ghrobar', protocolNameEnglish = 'Ghrobar', script = 'Cirith/quests/Ghrobar.lua', direction = 'west',
			look = {type = 478, head = 78, body = 59, legs = 61, feet = 115}
			},
	{ name = 'Vectur', script = 'Cirith/quests/Vectur.lua', direction = 'south',
			look = {type = 541, head = 78, body = 49, legs = 106, feet = 95, addons = 1}
			},
	{ name = 'Indow', script = 'Cirith/quests/Indow.lua', walkinterval = 4000, speed = 140,
			look = {type = 597, head = 0, body = 41, legs = 15, feet = 95}
			},
	{ name = 'Lillian', script = 'Cirith/quests/Lillian.lua', direction = 'north',
			look = {type = 279, head = 78, body = 114, legs = 94, feet = 115}
			},
	{ name = 'Golfas', script = 'Cirith/quests/Golfas.lua', walkinterval = 10000, speed = 100,
			look = {type = 597, head = 0, body = 31, legs = 12, feet = 115, addons = 3}
			},
	{ name = 'Maxigashi', script = 'Cirith/quests/Maxigashi.lua', direction = 'south',
			look = {type = 569, head = 0, body = 48, legs = 26, feet = 115}
			},
	{ name = 'Cold Hearted', script = 'Cirith/quests/Cold Hearted.lua', direction = 'south',
			look = {type = 151, head = 78, body = 43, legs = 85, feet = 95, addons = 3}
			},
	{ name = 'Vestonator', script = 'Cirith/quests/Vestonator.lua', walkinterval = 8000, speed = 120,
			look = {type = 289, head = 78, body = 35, legs = 1, feet = 19, addons = 3}
			},
	{ name = 'Nynthus', script = 'Cirith/quests/Nynthus.lua', walkinterval = 5000, speed = 120,
			look = {type = 465, head = 8, body = 48, legs = 19, feet = 65, addons = 3}
			},
	{ name = 'Ravender', script = 'Cirith/quests/Ravender.lua', walkinterval = 8000, speed = 110,
			look = {type = 590, head = 78, body = 94, legs = 0, feet = 56, addons = 1}
			},
	{ name = 'Alammred', script = 'Cirith/quests/Alammred.lua', walkinterval = 8000, speed = 110,
			look = {type = 615}
			},
	
	
	
	
	{ name = 'Santa', protocolName = 'Miko�aj', script = 'Cirith/Santa.lua', walkinterval = 8000, speed = 110,
			look = {type = 661}
			},
	{ name = 'Santa\'s Helper', protocolName = 'Pomocnik Miko�aja', script = 'Cirith/SantaHelper.lua', walkinterval = 8000, speed = 110,
			look = {type = 153, head = 97, body = 116, legs = 116, feet = 21, addons = 3}
			},
}