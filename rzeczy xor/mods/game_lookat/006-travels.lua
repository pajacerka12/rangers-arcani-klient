TRAVEL_WINDOW = {
	['Mentril'] = {
		{position = {x = 1254, y = 1353, z = 7}, price = 40, name = 'Fishermen Village'},
		{position = {x = 1300, y = 1284, z = 7}, price = 30, name = 'Mayor Henry'},
		{position = {x = 1372, y = 1341, z = 7}, price = 50, name = 'Albert the Priest'},
	},
	['Aron'] = {
		{position = {x = 1251, y = 1297, z = 7}, price = 50, name = 'West Bridge'},
		{position = {x = 1254, y = 1353, z = 7}, price = 90, name = 'Fishermen Village'},
		{position = {x = 1300, y = 1284, z = 7}, price = 80, name = 'Mayor Henry'},
		{position = {x = 1276, y = 1309, z = 7}, price = 50, name = 'Selius the Guard'},
	},
	['Yaze'] = {
		{position = {x = 1313, y = 801, z = 7}, price = 250, name = 'Northern Village'},
	},
	['Harnald'] = {
		{position = {x = 1342, y = 855, z = 7}, price = 250, name = 'Cirith Temple'},
	},
	['Anja'] = {
		{position = {x = 1399, y = 1119, z = 7}, price = 200, name = 'Southern Port'},
	},
	['Red Baron'] = {
		{position = {x = 1373, y = 957, z = 7}, price = 200, name = 'Cirith Prison'},
	},
	['Kabathar'] = {
		{position = {x = 1415, y = 884, z = 7}, price = 45, name = 'Eastern Village'},
		{position = {x = 1314, y = 988, z = 7}, price = 50, name = 'Southern Village'},
	},
	['Petr'] = {
		{position = {x = 1562, y = 703, z = 7}, price = 300, name = 'Orilion'},
	},
	['Petr Orilion'] = {
		{position = {x = 1406, y = 1121, z = 6}, price = 300, name = 'Southern Port'},
	},
	['Sharp Blade'] = {
		{position = {x = 1678, y = 1235, z = 7}, price = 500, name = 'Tortoise Island'},
	},
	['Onald'] = {
		{position = {x = 1403, y = 1054, z = 7}, price = 100, name = 'Southern Port'},
		{position = {x = 1284, y = 874, z = 7}, price = 50, name = 'Cirith, Bailey'},
		{position = {x = 1417, y = 882, z = 7}, price = 75, name = 'Eastern Village'},
		{position = {x = 1185, y = 1297, z = 7}, price = 300, name = 'Tavern at the Crossroads'},
	},
	['Manten'] = {
		{position = {x = 1314, y = 988, z = 7}, price = 300, name = 'Southern Village'},
	},
	['Syslaven'] = {
		{position = {x = 1315, y = 988, z = 7}, price = 100, name = 'Southern Village'},
	},
}
