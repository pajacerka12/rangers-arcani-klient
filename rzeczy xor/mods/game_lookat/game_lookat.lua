m_LookAtList = {}
m_LookAtFunctions = {}

m_LookAtFunctions.GameServerItemAttributes = 152

m_LookAtFunctions.healthMultiplier = 2
m_LookAtFunctions.experienceMultiplier = 2

m_LookAtFunctions.SPELL_ATTRIBUTE_COOLDOWN = 0
m_LookAtFunctions.SPELL_ATTRIBUTE_MANA = 1
m_LookAtFunctions.SPELL_ATTRIBUTE_DAMAGE = 2

m_LookAtFunctions.WEAPON_NONE = 0
m_LookAtFunctions.WEAPON_TWOHAND = 1
m_LookAtFunctions.WEAPON_ONEHAND = 2
m_LookAtFunctions.WEAPON_SHIELD = 3
m_LookAtFunctions.WEAPON_WAND = 4
m_LookAtFunctions.WEAPON_AMMO = 5
m_LookAtFunctions.WEAPON_QUIVER = 6

m_LookAtFunctions.itemListByServerId = {}
m_LookAtFunctions.itemListByClientId = {}
m_LookAtFunctions.itemListByType = {}
m_LookAtFunctions.monsterListByClass = {}
m_LookAtFunctions.config = {
	maximumItems = 3,
	maxIconsInLine = 8,
	iconSize = 19,
	
	className = {
		[ITEMCLASS_DESOLATE] = {'Zdewastowan', 'Desolate'},
		[ITEMCLASS_DAMAGED] = {'Uszkodzon', 'Damaged'},
		[ITEMCLASS_NORMAL] = {'Normaln', 'Normal'},
		[ITEMCLASS_PERFECT] = {'Doskona�', 'Perfect'},
		[ITEMCLASS_LEGENDARY] = {'Legendarn', 'Legendary'},
		[ITEMCLASS_UNIQUE] = {'Unikaln', 'Unique'},
	},
	
	vocationName = {
		[0] = tr("mages, druids, wizards, hunters, grenadiers and trackers"),
		[1] = tr("hunters, grenadiers, trackers, mercenaries, paladins and barbarians"),
		[2] = tr("mages, druids, wizards, mercenaries, paladins and barbarians"),
		[3] = tr("mages, druids and wizards"),
		[4] = tr("druids and wizards"),
		[5] = tr("druids"),
		[6] = tr("wizards"),
		[7] = tr("hunters, grenadiers and trackers"),
		[8] = tr("trackers and grenadiers"),
		[9] = tr("trackers"),
		[10] = tr("grenadiers"),
		[11] = tr("mercenaries, paladins and barbarians"),
		[12] = tr("paladins and barbarians"),
		[13] = tr("paladins"),
		[14] = tr("barbarians")
	},
	
	skillNames = {
		['strength'] = SKILL_STRENGTH,
		['dexterity'] = SKILL_DEXTERITY,
		['condition'] = SKILL_CONDITION,
		['wisdom'] = SKILL_WISDOM,
		['vigor'] = SKILL_VIGOR
	},
	
	itemDescription = {
		[ITEM_MIN_DAMAGE] = {icon = 1, description = tr('Damage')},
		[ITEM_DISTANCE_DAMAGE] = {icon = 4, description = 'Additional damage'},
		[ITEM_ATTACK_SPEED] = {icon = 20, description = tr('Attack speed'), multiplier = 0.001},
		[ITEM_SHOOT_RANGE] = {icon = 22, description = tr('Range')},
		[ITEM_SPENT_MANA] = {icon = 55, description = tr(' mana spent per shoot'), opposite = true},
		
		[ITEM_ARMOR] = {icon = 2, description = tr('Armor')},
		[ITEM_DEFENSE] = {icon = 3, description = tr('Defense')},
		[ITEM_CHARGES] = {icon = 10, description = tr('Charges')},
		[ITEM_CONTAINER] = {icon = 15, description = tr('Container')},
		
		[ITEM_WEAPON_TYPE] = {icon = {4, 1, 3, 28, 11, 11}, description = {tr('Two handed weapon'), tr('One handed weapon'), tr('Shield'), tr('Wand'), tr('Ammunition'), tr('Quiver')}},
		
		[ITEM_DODGE_MELEE] = {icon = 51, description = tr('Dodge melee attacks')},
		[ITEM_DODGE_RANGE] = {icon = 52, description = tr('Dodge range attacks')},
		[ITEM_RECOVERY_AMMUNITION] = {icon = 11, description = tr(' chance recovery ammunition'), opposite = true},
		[ITEM_HIT_CHANCE] = {icon = 47, description = tr('Hit chance')},
		[ITEM_COUNT_STOLEN_LIFE] = {icon = 13, description = tr('Stealing life')},
		[ITEM_MAXHEALTH] = {icon = 39, description = tr('Maximum health')},
		[ITEM_REGEN_HEALTH] = {icon = 39, description = tr(' health regeneration per turn'), opposite = true},
		
		[ITEM_COUNT_STOLEN_MANA] = {icon = 45, description = tr('Stealing mana')},
		[ITEM_MAXMANA] = {icon = 40, description = tr('Maximum mana')},
		[ITEM_REGEN_MANA] = {icon = 40, description = tr(' mana regeneration per turn'), opposite = true},
		
		[ITEM_STAT] = {icon = {39, 40}, editorDescription = {'health', 'mana'}, description = {tr('Maximum health'), tr('Maximum mana')}},
		[ITEM_POTIONS] = {icon = {39, 40}, editorDescription = {'health', 'mana'}, description = {tr('Health regeneration'), tr('Mana regeneration')}},
		
		[ITEM_CRITICAL] = {icon = 12, description = tr('Critical hit')},
		[ITEM_CRITICAL_HIT_MULTIPLIER] = {icon = 12, description = tr('Critical hit multiplier')},
		[ITEM_BLOCK_CRITICAL] = {icon = 6, description = tr('Block critical hit')},
		[ITEM_REDUCE_CRITICAL_HIT] = {icon = 6, description = tr('Reduce damage of critical hit')},
		
		[ITEM_MAGIC_ATTACK] = {icon = 17, description = tr('Magic increase')},
		[ITEM_FAMILIAR] = {icon = 4, description = tr('Familiar damage')},
		[ITEM_SPEED] = {icon = 24, description = tr('Speed')},
		[ITEM_DOUBLE_HIT] = {icon = 9, description = tr('Double hit'), toolTip = tr('Working only with melee attacks.')},
		[ITEM_FULL_IGNORE_ARMOR] = {icon = 14, description = tr('Chance to penetrate resistances')},
		[ITEM_LUCK] = {icon = 41, description = tr('Luck')},
		[ITEM_DEATH_HIT] = {icon = 35, description = 'Death hit', toolTip = tr('It only works with living beings.\nAn attribute works only with a critical hit.\nResistance to death reduces the damage received by action of artifact.\nEffect do not works at bosses.')},
		
		[ITEM_SKILL] = {icon = {19, 51, 27, 17, 49}, description = {tr('Strength'), tr('Dexterity'), tr('Condition'), tr('Wisdom'), tr('Vigor')}},
		[ITEM_SLOTS] = {icon = 52, description = tr('Maximum summary of skills')},
		
		[ITEM_ABSORB] = {icon = {30, 16, 16, 16, 27, 23, 37, 36, 35, 48, 31, 34, 32, 33, 16}, divide = 10,
			description = {tr('Physical absorb'), tr('Undefined absorb'), tr('Lifedrain absorb'), tr('Manadrain absorb'),
						'', tr('Drown absorb'), tr('Bleed absorb'), tr('Holy absorb'), tr('Death absorb'), tr('Toxic absorb'), 
						tr('Energy absorb'), tr('Earth absorb'), tr('Fire absorb'), tr('Ice absorb'), tr('Wind absorb')}},
								
		[ITEM_CONDITION_TICKS] = {icon = {30, 16, 16, 16, 27, 23, 37, 36, 35, 48, 31, 34, 32, 33, 16}, divide = 10,
			description = {'', '', '', '', '', '',
						tr('Duration of bleeding'), tr('Duration of dazzling'), tr('Duration of the curse'),
						'', tr('Duration of paralysis'), tr('Duration of poisoning'),
						tr('Duration of arson'), tr('Duration of freezing'), ''}},
		
		[ITEM_SUPPRESS] = {icon = {34, 32, 31, 0, 0, 49, 0, 0, 0, 0, 51, 50, 0, 0, 56, 23, 0, 0, 33, 36, 35, 0, 0, 0, 37, 16, 48, 0, 0, 0, 0, 51, 52, 53, 38, 36},
			description = {tr('Poison immunity'), tr('Burning immunity'), tr('Electrical immunity'), '', '', tr('Paralyze immunity'), '', '', '', '', tr('Reverse directions immunity'), tr('Drunkenness immunity'), '', '', tr('Soul nova immunity'), tr('Drown immunity'), '', '', tr('Freezing immunity'), tr('Dazzled immunity'),
						tr('Cursed immunity'), '', '', '', tr('Bleed immunity'), tr('Wind immunity'), tr('Toxic immunity'), '', '', '', '', tr('Push immunity'), tr('Stun immunity'), tr('Fear immunity'), tr('Silent immunity'), tr('Blind immunity')}},
		
		[ITEM_INCREMENT] = {icon = {30, 16, 16, 16, 27, 23, 37, 36, 35, 48, 31, 34, 32, 33, 16}, 
			description = {tr('Increased physical damage'), tr('Increased undefined damage'), tr('Increased lifedrain damage'),
						tr('Increased manadrain damage'), tr('Increased healing'), tr('Increased drown damage'),
						tr('Increased bleed damage'), tr('Increased holy damage'), tr('Increased death damage'),
						tr('Increased toxic damage'), tr('Increased energy damage'), tr('Increased earth damage'),
						tr('Increased fire damage'), tr('Increased ice damage'), tr('Increased wind damage')}},
		
		[ITEM_BONUS_DAMAGE] = {icon = {30, 16, 16, 16, 27, 23, 37, 36, 35, 48, 31, 34, 32, 33, 16}, divide = 10,
			description = {tr('Additional physical damage'), tr('Additional undefined damage'), tr('Additional lifedrain damage'),
						tr('Additional manadrain damage'), '', tr('Additional drown damage'),
						tr('Additional bleed damage'), tr('Additional holy damage'), tr('Additional death damage'),
						tr('Additional toxic damage'), tr('Additional energy damage'), tr('Additional earth damage'),
						tr('Additional fire damage'), tr('Additional ice damage'), tr('Additional wind damage')}},
		
		[ITEM_REFLECT_CHANCE] = {icon = {30, 16, 16, 16, 27, 23, 37, 36, 35, 48, 31, 34, 32, 33, 16}, divide = 10,
			firstDescription = '%s%% chance to reflect %s%%', toolTip = tr('Reflects damage from the opposite element,\nfire damage is turned into ice damage, for example.'),
			description = {tr(' physical damage'), tr(' undefined damage'), tr(' lifedrain damage'),
						tr(' manadrain damage'), '', tr(' drown damage'),
						tr(' bleed damage'), tr(' holy damage'), tr(' death damage'),
						tr(' toxic damage'), tr(' energy damage'), tr(' earth damage'),
						tr(' fire damage'), tr(' ice damage'), tr(' wind damage')}},
		
		[ITEM_CONDITION] = {icon = {34, 32, 31, 30, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 23, 16, 16, 33, 36, 35, 16, 16, 16, 37, 53, 48}, firstDescription = 'for %d damage per %s seconds. This effect lasts for %s seconds',
			description = {tr('chance to poison the target'), tr('chance to set the target ablaze'), tr('chance to electrified'), 
						tr('chance to tear the target'), '', '', '', '', '', '', '', '', '', '', '', tr('chance to induce drowning effect on the target'), '', '', 
						tr('chance to frostbite the target'), tr('chance to dazzle the target'), tr('chance to apply curse on target'), '', '', '', 
						tr('chance to induce bleeding'), tr('chance to cause wind damage'), tr('chance to toxify the target')}},
		
		[ITEM_MONSTER_DAMAGE] = {icon = 12, firstDescription = '%s%% damage against ',
			description = {'', '', tr('underwaters'), tr('animals'), tr('undeads'), tr('humans'), tr('serpents'),
						tr('drakens'), tr('lizards'), tr('dragons'), tr('bosses'), tr('mutateds'), tr('apes'),
						tr('djinns'), tr('bonelords'), tr('minotaurs'), tr('trolls'), tr('orcs'), tr('goblins'),
						tr('giants'), tr('elves'), tr('dwarfs'), tr('geo-elementals'), tr('pyro-elementals'),
						tr('cryo-elementals'), tr('electro-elementals'), tr('demons'), tr('arachnids'),
						tr('necromancers'), 'yuan-ti', tr('vampires'), tr('kynoheads'), tr('pirates'), tr('perverteds')}},
		
		[ITEM_MONSTER_DEFENSE] = {icon = 5, firstDescription = '%s%% of defense against ',
			description = {'', '', tr('underwaters'), tr('animals'), tr('undeads'), tr('humans'), tr('serpents'),
						tr('drakens'), tr('lizards'), tr('dragons'), tr('bosses'), tr('mutateds'), tr('apes'),
						tr('djinns'), tr('bonelords'), tr('minotaurs'), tr('trolls'), tr('orcs'), tr('goblins'),
						tr('giants'), tr('elves'), tr('dwarfs'), tr('geo-elementals'), tr('pyro-elementals'),
						tr('cryo-elementals'), tr('electro-elementals'), tr('demons'), tr('arachnids'),
						tr('necromancers'), 'yuan-ti', tr('vampires'), tr('kynoheads'), tr('pirates'), tr('perverteds')}},
		
		[ITEM_RUNE_REROLLS] = {icon = 59, description = tr('Rune type rerolls left')},
		[ITEM_RUNE_QUALITY] = {icon = 58, description = tr('Rune quality rerolls left')},
		
		[ITEM_FOOD] = {icon = 7, description = tr('Food regeneration')},
		[ITEM_BREAK_CHANCE] = {icon = 14, description = tr('Break chance')},
		[ITEM_LEVEL] = {icon = 16, description = tr('Experience level required')},
		[ITEM_VOCATION] = {icon = 46, description = tr('Only be wielded properly by')},
		[ITEM_BUFF_TYPE] = {icon = 16, description = 'Increase %s against %s by %d%%'},
		
		[ITEM_SPELL_CHANCE] = {icon = 16, description = '%d%% chance to cast %s'},
		[ITEM_LOOTLIST] = {icon = 7, description = '%d%% more chance to drop %s'},
		
		[ITEM_INVISIBILITY] = {icon = 36, description = tr('Invisibility')},
		[ITEM_MANASHIELD] = {icon = 26, description = tr('Magic Shield')},
		[ITEM_PREVENT_DEATH] = {icon = 59, description = tr('Allows for a one-time respawn at the place of death')},
		[ITEM_CRAFTABLE] = {icon = 54, description = tr('Craftable and Smeltable')},
		
		[ITEM_DURATION] = {icon = 38, description = tr('It has')},
		[ITEM_WEIGHT] = {icon = 8, description = tr('Weight'), multiplier = 0.001},
		[ITEM_HOUSE_OWNER] = {icon = 52, description = tr('Owner')},
		[ITEM_HOUSE_NAME] = {icon = 17},
		
		[ITEM_POS] = {icon = 16, description = 'Pos'},
		[ITEM_ID] = {icon = 16, description = 'ID'},
		[ITEM_CLIENT_ID] = {icon = 16, description = 'Client ID'},
	},
}

m_LookAtFunctions.getIconByCombatType = function(id)
	return m_LookAtFunctions.config.itemDescription[ITEM_ABSORB].icon[id]
end

m_LookAtFunctions.getAttributeDetails = function(id)
	return m_LookAtFunctions.config.itemDescription[id]
end

m_LookAtFunctions.hasAttribute = function(clientId, id, isLegendary)
	local it = m_LookAtFunctions.getItemByClientId(clientId)
	if not it then
		return true
	end
	
	if not it.attributes.legendary then
		it.attributes.legendary = {}
	end
	
	if id == ITEM_DISTANCE_DAMAGE then
		return it.attributes.maxdamage and not it.attributes.mindamage
	elseif id == ITEM_ARMOR then
		return it.attributes.armor or (isLegendary and it.attributes.legendary.armor)
	elseif id == ITEM_HIT_CHANCE then
		return it.attributes.hitchance or (isLegendary and it.attributes.legendary.hitchance)
	elseif id == ITEM_COUNT_STOLEN_LIFE then
		return it.attributes.stolenlife or (isLegendary and it.attributes.legendary.stolenlife)
	elseif id == ITEM_REGEN_HEALTH then
		return it.attributes.regenhealth or (isLegendary and it.attributes.legendary.regenhealth)
	elseif id == ITEM_COUNT_STOLEN_MANA then
		return it.attributes.stolenmana or (isLegendary and it.attributes.legendary.stolenmana)
	elseif id == ITEM_REGEN_MANA then
		return it.attributes.regenmana or (isLegendary and it.attributes.legendary.regenmana)
	elseif id == ITEM_CRITICAL then
		return it.attributes.criticalhitchance or (isLegendary and it.attributes.legendary.criticalhitchance)
	elseif id == ITEM_CRITICAL_HIT_MULTIPLIER then
		return it.attributes.criticalhitmultiplier or (isLegendary and it.attributes.legendary.criticalhitmultiplier)
	elseif id == ITEM_BLOCK_CRITICAL then
		return it.attributes.chanceblockcritical or (isLegendary and it.attributes.legendary.chanceblockcritical)
	elseif id == ITEM_REDUCE_CRITICAL_HIT then
		return it.attributes.reducecriticalhit or (isLegendary and it.attributes.legendary.reducecriticalhit)
	elseif id == ITEM_MAGIC_ATTACK then
		return it.attributes.magicattack or (isLegendary and it.attributes.legendary.magicattack)
	elseif id == ITEM_FAMILIAR then
		return it.attributes.familiar or (isLegendary and it.attributes.legendary.familiar)
	elseif id == ITEM_SPEED then
		return it.attributes.speed or (isLegendary and it.attributes.legendary.speed)
	elseif id == ITEM_DOUBLE_HIT then
		return it.attributes.doublehit or (isLegendary and it.attributes.legendary.doublehit)
	elseif id == ITEM_FULL_IGNORE_ARMOR then
		return it.attributes.absolutlyignorearmor or (isLegendary and it.attributes.legendary.absolutlyignorearmor)
	elseif id == ITEM_LUCK then
		return it.attributes.luck or (isLegendary and it.attributes.legendary.luck)
	elseif id == ITEM_DEATH_HIT then
		return it.attributes.deathhit or (isLegendary and it.attributes.legendary.deathhit)
	elseif id == ITEM_SPELL_CHANCE then
		return it.attributes.assignedspell or (isLegendary and it.attributes.legendary.assignedspell)
	elseif id == ITEM_LOOTLIST then
		return it.attributes.bonusloot or (isLegendary and it.attributes.legendary.bonusloot)
	elseif id == ITEM_INVISIBILITY then
		return it.attributes.invisibility
	elseif id == ITEM_MANASHIELD then
		return it.attributes.manashield
	elseif id == ITEM_CRAFTABLE then
		return modules.game_rewards.m_CraftingFunctions.isCraftingItem(clientId)
	elseif id == ITEM_DURATION then
		return it.attributes.duration or it.attributes.showduration
	elseif id == ITEM_BREAK_CHANCE then
		return it.attributes.breakchance
	elseif id == ITEM_SUPPRESS then
		return it.attributes.suppress or (isLegendary and it.attributes.legendary.suppress)
	elseif id == ITEM_INCREMENT then
		return it.attributes.increment or (isLegendary and it.attributes.legendary.increment)
	elseif id == ITEM_BONUS_DAMAGE then
		return it.attributes.bonusdamage or (isLegendary and it.attributes.legendary.bonusdamage)
	elseif id == ITEM_REFLECT_CHANCE then
		return it.attributes.reflect or (isLegendary and it.attributes.legendary.reflect)
	elseif id == ITEM_CONDITION then
		return it.attributes.conditions or (isLegendary and it.attributes.legendary.conditions)
	elseif id == ITEM_MONSTER_DAMAGE then
		return it.attributes.damage or (isLegendary and it.attributes.legendary.damage)
	elseif id == ITEM_MONSTER_DEFENSE then
		if it.attributes.absorb or (isLegendary and it.attributes.legendary.absorb) then
			if it.attributes.absorb then
				for i = 1, #it.attributes.absorb do
					local id = m_LookAtFunctions.getMonsterClassId(it.attributes.absorb[i].type)
					if id ~= 0 then
						return true
					end
				end
			end
			
			if isLegendary and it.attributes.legendary.absorb then
				for i = 1, #it.attributes.legendary.absorb do
					local id = m_LookAtFunctions.getMonsterClassId(it.attributes.legendary.absorb[i].type)
					if id ~= 0 then
						return true
					end
				end
			end
			
			return false
		end
	elseif id == ITEM_ABSORB then
		if it.attributes.absorb or (isLegendary and it.attributes.legendary.absorb) then
			if it.attributes.absorb then
				for i = 1, #it.attributes.absorb do
					local id = m_LookAtFunctions.getCombatId(it.attributes.absorb[i].type)
					if id ~= 0 then
						return true
					end
				end
			end
			
			if isLegendary and it.attributes.legendary.absorb then
				for i = 1, #it.attributes.legendary.absorb do
					local id = m_LookAtFunctions.getCombatId(it.attributes.legendary.absorb[i].type)
					if id ~= 0 then
						return true
					end
				end
			end
		end
		
		return false
	elseif id == ITEM_MAXHEALTH then
		if it.attributes.stat or (isLegendary and it.attributes.legendary.stat) then
			if it.attributes.stat then
				for i = 1, #it.attributes.stat do
					if it.attributes.stat[i].type == 'health' then
						return true
					end
				end
			end
			
			if isLegendary and it.attributes.legendary.stat then
				for i = 1, #it.attributes.legendary.stat do
					if it.attributes.legendary.stat[i].type == 'health' then
						return true
					end
				end
			end
		end
		
		return false
	elseif id == ITEM_MAXMANA then
		if it.attributes.stat or (isLegendary and it.attributes.legendary.stat) then
			if it.attributes.stat then
				for i = 1, #it.attributes.stat do
					if it.attributes.stat[i].type == 'mana' then
						return true
					end
				end
			end
			
			if isLegendary and it.attributes.legendary.stat then
				for i = 1, #it.attributes.legendary.stat do
					if it.attributes.legendary.stat[i].type == 'mana' then
						return true
					end
				end
			end
		end
		
		return false
	else
		return print('Not found id ' .. id)
	end
	
	return false
end

m_LookAtFunctions.getImageClip = function(id)
	if not id then
		return '0 0 ' .. m_LookAtFunctions.config.iconSize .. ' ' .. m_LookAtFunctions.config.iconSize
	end
	
	return (((id - 1) % m_LookAtFunctions.config.maxIconsInLine) * m_LookAtFunctions.config.iconSize) .. ' ' .. ((math.ceil(id / m_LookAtFunctions.config.maxIconsInLine) - 1)*m_LookAtFunctions.config.iconSize) .. ' ' .. m_LookAtFunctions.config.iconSize .. ' ' .. m_LookAtFunctions.config.iconSize
end

m_LookAtFunctions.setIconImageType = function(widget, iconType)
	widget:setImageClip(m_LookAtFunctions.getImageClip(iconType))
end

m_LookAtFunctions.getFreeSocked = function()
	local currentTime = os.time()
	local id = false
	local widget = false
	
	for i = 1, m_LookAtFunctions.config.maximumItems do
		local tmpWidget = m_LookAtList.window[i]
		if not tmpWidget then
			return i
		elseif not tmpWidget:getChildById('cooldown'):isOn() and tmpWidget.time and tmpWidget.time <= currentTime then
			currentTime = tmpWidget.time
			id = i
		end
	end
	
	if id then
		m_LookAtFunctions.destroyWindow(id)
	end
	
	return id
end

m_LookAtFunctions.getClassName = function(id, language)
	local var = m_LookAtFunctions.config.className[id]
	return var and var[language]
end

m_LookAtFunctions.getStatId = function(id)
	if id == 'health' then
		return 1
	elseif id == 'mana' then
		return 2
	end
	
	return 0
end

m_LookAtFunctions.getVocationName = function(list)
	local tmpList = {}
	for i = 1, #list do
		tmpList[list[i]] = true
	end
	
	local id = -1
	if tmpList[1] and tmpList[2] and tmpList[3] and tmpList[4] and tmpList[5] and tmpList[6] then
		id = 0
	elseif tmpList[4] and tmpList[5] and tmpList[6] and tmpList[7] and tmpList[8] and tmpList[9] then
		id = 1
	elseif tmpList[1] and tmpList[2] and tmpList[3] and tmpList[7] and tmpList[8] and tmpList[9] then
		id = 2
	elseif tmpList[1] and tmpList[2] and tmpList[3] then
		id = 3
	elseif tmpList[2] and tmpList[3] then
		id = 4
	elseif tmpList[2] then
		id = 5
	elseif tmpList[3] then
		id = 6
	elseif tmpList[4] and tmpList[5] and tmpList[6] then
		id = 7
	elseif tmpList[5] and tmpList[6] then
		id = 8
	elseif tmpList[5] then
		id = 9
	elseif tmpList[6] then
		id = 10
	elseif tmpList[7] and tmpList[8] and tmpList[9] then
		id = 11
	elseif tmpList[8] and tmpList[9] then
		id = 12
	elseif tmpList[8] then
		id = 13
	elseif tmpList[9] then
		id = 14
	end
	
	return m_LookAtFunctions.config.vocationName[id]
end

m_LookAtFunctions.getSkillId = function(id)
	if id == "strength" then
		return SKILL_STRENGTH
	elseif id == "dexterity" then
		return SKILL_DEXTERITY
	elseif id == "condition" then
		return SKILL_CONDITION
	elseif id == "wisdom" then
		return SKILL_WISDOM
	elseif id == "vigor" then
		return SKILL_VIGOR
	end
	
	return SKILL_NONE
end

m_LookAtFunctions.getSuppressId = function(id)
	if id == "earth" then
		return CONDITION_POISON
	elseif id == "fire" then
		return CONDITION_FIRE
	elseif id == "energy" then
		return CONDITION_ENERGY
	elseif id == "physical" then
		return CONDITION_PHYSICAL
	elseif id == "paralyze" then
		return CONDITION_PARALYZE
	elseif id == "drunk" then
		return CONDITION_DRUNK
	elseif id == "drown" then
		return CONDITION_DROWN
	elseif id == "ice" then
		return CONDITION_FREEZING
	elseif id == "holy" then
		return CONDITION_DAZZLED
	elseif id == "death" then
		return CONDITION_CURSED
	elseif id == "bleed" then
		return CONDITION_BLEED
	elseif id == "wind" then
		return CONDITION_WIND
	elseif id == "toxic" then
		return CONDITION_TOXIC
	elseif id == "stun" then
		return CONDITION_STUN
	elseif id == "fear" then
		return CONDITION_FEAR
	elseif id == "soul" then
		return CONDITION_SOUL_NOVA
	elseif id == "directions" then
		return CONDITION_DIRECTIONS
	elseif id == "silence" then
		return CONDITION_SILENCE
	elseif id == "blind" then
		return CONDITION_BLIND
	end
	
	return 0
end

m_LookAtFunctions.getMonsterClassId = function(id)
	if id == "underwater" then
		return MONSTER_UNDERWATER
	elseif id == "animal" then
		return MONSTER_ANIMAL
	elseif id == "undead" then
		return MONSTER_UNDEAD
	elseif id == "vampire" then
		return MONSTER_VAMPIRE
	elseif id == "human" then
		return MONSTER_HUMAN
	elseif id == "serpent" then
		return MONSTER_SERPENT
	elseif id == "draken" then
		return MONSTER_DRAKEN
	elseif id == "lizard" then
		return MONSTER_LIZARD
	elseif id == "dragon" then
		return MONSTER_DRAGON
	elseif id == "boss" then
		return MONSTER_BOSS
	elseif id == "mutated" then
		return MONSTER_MUTATED
	elseif id == "ape" then
		return MONSTER_APE
	elseif id == "djinn" then
		return MONSTER_DJINN
	elseif id == "bonelord" then
		return MONSTER_BONELORD
	elseif id == "minotaur" then
		return MONSTER_MINOTAUR
	elseif id == "troll" then
		return MONSTER_TROLL
	elseif id == "orc" then
		return MONSTER_ORC
	elseif id == "goblin" then
		return MONSTER_GOBLIN
	elseif id == "giant" then
		return MONSTER_GIANT
	elseif id == "elf" then
		return MONSTER_ELF
	elseif id == "dwarf" then
		return MONSTER_DWARF
	elseif id == "geo-elemental" then
		return MONSTER_GEO_ELEMENTAL
	elseif id == "pyro-elemental" then
		return MONSTER_PYRO_ELEMENTAL
	elseif id == "cryo-elemental" then
		return MONSTER_CRYO_ELEMENTAL
	elseif id == "electro-elemental" then
		return MONSTER_ELECTRO_ELEMENTAL
	elseif id == "demon" then
		return MONSTER_DEMON
	elseif id == "arachnid" then
		return MONSTER_ARACHNID
	elseif id == "wizard" then
		return MONSTER_WIZARD
	elseif id == "yuan-ti" then
		return MONSTER_YUAN_TI
	elseif id == "vampire" then
		return MONSTER_VAMPIRE
	elseif id == "kynohead" then
		return MONSTER_KYNOHEAD
	elseif id == "pirate" then
		return MONSTER_PIRATE
	elseif id == "perverted" then
		return MONSTER_PERVERTED
	elseif id == "ratatosk" then
		return MONSTER_RATATOSK
	end
	
	return 0
end

m_LookAtFunctions.getCombatId = function(id)
	if id == "physical" then
		return COMBAT_PHYSICALDAMAGE
	elseif id == "lifedrain" then
		return COMBAT_LIFEDRAIN
	elseif id == "manadrain" then
		return COMBAT_MANADRAIN
	elseif id == "healing" then
		return COMBAT_HEALING
	elseif id == "drown" then
		return COMBAT_DROWNDAMAGE
	elseif id == "bleed" then
		return COMBAT_BLEEDDAMAGE
	elseif id == "holy" then
		return COMBAT_HOLYDAMAGE
	elseif id == "death" then
		return COMBAT_DEATHDAMAGE
	elseif id == "toxic" then
		return COMBAT_TOXICDAMAGE
	elseif id == "energy" then
		return COMBAT_ENERGYDAMAGE
	elseif id == "earth" then
		return COMBAT_EARTHDAMAGE
	elseif id == "fire" then
		return COMBAT_FIREDAMAGE
	elseif id == "ice" then
		return COMBAT_ICEDAMAGE
	elseif id == "wind" then
		return COMBAT_WINDDAMAGE
	end
	
	return 0
end

m_LookAtFunctions.getWeaponType = function(it)
	local id = it.weapontype
	if id == "twohand" then
		return m_LookAtFunctions.WEAPON_TWOHAND
	elseif id == "onehand" then
		return m_LookAtFunctions.WEAPON_ONEHAND
	elseif id == "shield" then
		return m_LookAtFunctions.WEAPON_SHIELD
	elseif id == "wand" then
		return m_LookAtFunctions.WEAPON_WAND
	elseif id == "ammunition" then
		return m_LookAtFunctions.WEAPON_AMMO
	elseif id == "quiver" then
		return m_LookAtFunctions.WEAPON_QUIVER
	end
	
	return m_LookAtFunctions.WEAPON_NONE
end

m_LookAtFunctions.inspectItem = function(lookThing, classId, inspect, virtual, self)
	if lookThing:isItem() and (virtual or inspect) then
		if virtual then
			local clientId = lookThing:getId()
			local it = m_LookAtFunctions.getItemByClientId(clientId)
			if it then
				local list = {
					[ITEM_CLASS_ITEM] = {value = type(classId) == 'number' and classId or getClassIdByName(classId)},
					[ITEM_COUNT] = {value = (self and self.subType) or lookThing:getCount()},
				}
				
				if lookThing:isFluidContainer() and self and self.fluidSource then
					list[ITEM_SUBTYPE] = {value = self.fluidSource}
				end
				
				m_LookAtFunctions.open(clientId, m_LookAtFunctions.getItemByClientId(clientId).id, list)
			end
		else
			g_game.inspectNpcTrade(lookThing, classId or '', inspect)
		end
	else
		g_game.look(lookThing)
	end
end

m_LookAtFunctions.createLabel = function(value, panel, id, attributeType, priority, crafting)
	if tonumber(value) == 0 or value == '0%' then
		return 0, 0
	end
	
	local var = m_LookAtFunctions.config.itemDescription[id]
	if var then
		local widget = g_ui.createWidget('AttributeLabel', panel)
		local tmpWidget = widget:getChildById('icon')
		if var.toolTip then
			tmpWidget:setTooltip(var.toolTip)
		end
		
		if type(var.icon) == 'table' then
			m_LookAtFunctions.setIconImageType(tmpWidget, var.icon[attributeType or 1])
		else
			m_LookAtFunctions.setIconImageType(tmpWidget, var.icon)
		end
		
		local description = ''
		if type(value) == 'table' then
			if id == ITEM_CONDITION then
				description = value[1] .. '% ' .. var.description[attributeType or 1] .. ' ' .. tr(var.firstDescription, value[2], value[3], value[4])
			elseif var.firstDescription then
				description = tr(var.firstDescription, unpack(value)) .. var.description[attributeType or 1]
			else
				description = tr(var.description, unpack(value))
			end
		elseif type(var.description) == 'table' then
			if value then
				if var.opposite then
					description = value .. var.description[attributeType or 1]
				else
					description = var.description[attributeType or 1] .. ': ' .. value
				end
			else
				description = var.description[attributeType or 1]
			end
		elseif value then
			if var.opposite then
				if type(value) == 'string' then
					description = value .. var.description
				else
					description = value * (var.multiplier or 1) .. var.description
				end
			else
				if type(value) == 'string' then
					if var.description then
						description = var.description .. (isInArray({ITEM_VOCATION, ITEM_DURATION}, id) and ' ' or ': ') .. value
					else
						description = value
					end
				else
					description = var.description .. ': ' .. value * (var.multiplier or 1)
				end
			end
		else
			description = var.description
		end
		
		if id == ITEM_SLOTS and priority then
			widget:setPhantom(false)
			widget.priority = priority
			widget.onHoverChange = m_LookAtFunctions.onHoverSlotsChange
		elseif id == ITEM_CRAFTABLE and crafting then
			widget:setPhantom(false)
			widget.crafting = crafting
			widget.onHoverChange = m_LookAtFunctions.onHoverCraftingChange
		end
		
		widget:setText(description)
		return widget:getHeight() + 5, widget:getWidth()
	end
	
	return 0, 0
end

m_LookAtFunctions.onHoverCraftingChange = function(self, hovered)
	if not hovered then
		if m_LookAtList.slotsHover then
			m_LookAtList.slotsHover:destroy()
			m_LookAtList.slotsHover = nil
		end
		
		return true
	end
	
	m_LookAtList.slotsHover = g_ui.createWidget('CraftingHoverPanel', self:getRootParent())
	
	local count = #self.crafting
	local panel = m_LookAtList.slotsHover:getChildById('list')
	for i = 1, count do
		local widget = g_ui.createWidget('CraftingItemHover', panel)
		widget:setItemId(self.crafting[i].id)
		widget:setItemCount(self.crafting[i].amount)
	end
	
	panel:setWidth(math.min(8, count) * 34)
	m_LookAtList.slotsHover:setHeight(32 + (math.ceil(count / 8) * 34))
	
	if count > 8 then
		m_LookAtList.slotsHover:getChildById('label'):setText(tr('Items using this item to craft'))
	end
	
	local pos = self:getPosition()
	pos.x = pos.x + 64
	pos.y = pos.y + self:getHeight() - 8
	
	if pos.x + m_LookAtList.slotsHover:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_LookAtList.slotsHover:getWidth()
	end
	
	if pos.y + m_LookAtList.slotsHover:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_LookAtList.slotsHover:getHeight()
	end
	
	m_LookAtList.slotsHover:setPosition(pos)
end

m_LookAtFunctions.onHoverSlotsChange = function(self, hovered)
	if not hovered then
		if m_LookAtList.slotsHover then
			m_LookAtList.slotsHover:destroy()
			m_LookAtList.slotsHover = nil
		end
		
		return true
	end
	
	m_LookAtList.slotsHover = g_ui.createWidget('SlotsHoverPanel', self:getRootParent())
	
	local tmpHeight = 44
	local panel = m_LookAtList.slotsHover:getChildById('list')
	local explode = self.priority:explode(';')
	local var = m_LookAtFunctions.config.itemDescription[ITEM_SKILL]
	for i = 1, #explode do
		local id = m_LookAtFunctions.config.skillNames[explode[i]] + 1
		local widget = g_ui.createWidget('AttributeLabel', panel)
		
		m_LookAtFunctions.setIconImageType(widget:getChildById('icon'), var.icon[id])
		widget:setText(var.description[id])
		tmpHeight = tmpHeight + widget:getHeight() + 5
	end
	
	m_LookAtList.slotsHover:setHeight(tmpHeight)
	panel:setHeight(tmpHeight)
	
	local pos = self:getPosition()
	pos.x = pos.x + 64
	pos.y = pos.y + self:getHeight() - 8
	
	if pos.x + m_LookAtList.slotsHover:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_LookAtList.slotsHover:getWidth()
	end
	
	if pos.y + m_LookAtList.slotsHover:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_LookAtList.slotsHover:getHeight()
	end
	
	m_LookAtList.slotsHover:setPosition(pos)
end

m_LookAtFunctions.getAttributeValue = function(attribute, factor)
	if factor == 0 then
		return attribute
	end
	
	if attribute < 0 then
		return math.ceil(attribute - ((attribute * factor) / 100))
	end
	
	return math.ceil(attribute * ((100 + factor) / 100))
end

m_LookAtFunctions.getDamageFactor = function(id, rune)
	if id == ITEMCLASS_DESOLATE then
		return rune and -25 or -25
	elseif id == ITEMCLASS_DAMAGED then
		return rune and -10 or -10
	elseif id == ITEMCLASS_PERFECT then
		return rune and 25 or 15
	elseif id == ITEMCLASS_LEGENDARY then
		return rune and 50 or 15
	elseif id == ITEMCLASS_UNIQUE then
		return rune and 100 or 25
	end
	
	return 0
end

m_LookAtFunctions.createSeparator = function(tmpHeight, tmpWidget)
	return tmpHeight + g_ui.createWidget('AtlasNoAnchorSeparator', tmpWidget):getHeight()
end

m_LookAtFunctions.getSpellIcon = function(widget, name)
	local var = SpelllistSettings[name:lower()]
	if var then
		widget:setImageClip(modules.game_character.m_SpellsFunctions.getImageClip(var.iconId))
	end
end

m_LookAtFunctions.createHoverWindow = function(self, description, spell)
	if not m_LookAtList.hover then
		m_LookAtList.hover = g_ui.displayUI('look_panel')
	end
	
	local pos = self:getPosition()
	pos.x = pos.x + 22
	pos.y = pos.y + 22
	
	m_LookAtList.hover:setPosition(pos)
	
	local height = 0
	local width = 240
	if spell then
		local widget = m_LookAtList.hover:getChildById('spell')
		height = #spell * 12
		
		for i = 1, #spell do
			local tmpWidget = g_ui.createWidget('SpellLabelHover', widget)
			local icon = tmpWidget:getChildById('icon')
			local label = tmpWidget:getChildById('label')
			
			local v = spell[i]
			m_LookAtFunctions.getSpellIcon(icon, v.name)
			
			local text = v.name .. '\n'
			local begin = false
			if v.changeMana then
				text = text .. (v.changeMana > 0 and '+' or '') .. v.changeMana .. ' ' .. tr('required mana to cast spell')
				begin = true
			end
			
			if v.changeCooldown then
				text = text .. (begin and '\n' or '') .. (v.changeCooldown > 0 and '+' or '') .. v.changeCooldown / 1000 .. 's ' .. tr('to cooldown')
				begin = true
			end
			
			if v.changeDamage then
				text = text .. (begin and '\n' or '') .. (v.changeDamage > 0 and '+' or '') .. v.changeDamage .. '% ' .. tr('to damage')
			end
			
			label:setText(text)
			height = height + label:getHeight()
			tmpWidget:setHeight(label:getHeight())
		end
		
		widget:setHeight(height)
	end
	
	if description then
		local widget = m_LookAtList.hover:getChildById('label')
		widget:setText(description)
		height = height + widget:getHeight() + 24
		width = widget:getWidth()
	end
	
	m_LookAtList.hover:setHeight(height)
	m_LookAtList.hover:setWidth(width + 16)
end

m_LookAtFunctions.getItemNameById = function(serverId, language)
	local it = m_LookAtFunctions.getItemDescription(serverId)
	return it and it.name[language] or ''
end

m_LookAtFunctions.getItemNameByClientId = function(clientId, language)
	local it = m_LookAtFunctions.getItemByClientId(clientId)
	return it and it.name[language] or ''
end

m_LookAtFunctions.getItemByClientId = function(clientId)
	return m_LookAtFunctions.itemListByClientId[clientId]
end

m_LookAtFunctions.getItemDescription = function(serverId)
	return m_LookAtFunctions.itemListByServerId[serverId]
end

m_LookAtFunctions.open = function(clientId, serverId, list)
	if not m_LookAtList.window then
		m_LookAtList.window = {}
	end
	
	local socked = 0
	if not list[ITEM_INSPECT] then
		socked = m_LookAtFunctions.getFreeSocked()
	end
	
	if not socked then
		return true
	end
	
	local it = m_LookAtFunctions.getItemDescription(serverId)
	if not it then
		return true
	end
	
	local widget = g_ui.createWidget('UILookAtWindow', modules.game_interface.getRootPanel())
	local defaultPos = {x = 50, y = 50}
	if socked > 1 then
		local tmpWidget = m_LookAtList.window[socked - 1]
		if tmpWidget then
			defaultPos = {x = tmpWidget:getX() + tmpWidget:getWidth() + 32, y = tmpWidget:getY()}
		end
	end
	
	widget:setPosition(defaultPos)
	widget:setId('protected' .. 100 + socked)
	widget:setup()
	widget:open()
	widget.id = socked
	widget.time = os.time()
	widget.cooldown = m_LookAtFunctions.duration or 100
	
	local item = widget:getChildById('protectedItem')
	item:setItemId(clientId)
	
	local var = list[ITEM_CRAFT_ID]
	if var then
		local scroll = widget:getChildById('scroll')
		scroll:show()
		scroll:setItemId(var.value)
	end
	
	local count = list[ITEM_COUNT] and list[ITEM_COUNT].value or 1
	local subType = list[ITEM_SUBTYPE]
	if subType then
		item:setItemCount(subType.value)
		
		if item:getItem():isFluidContainer() then
			if subType.value == 0 then
				subType = 0
			else
				subType = count
			end
		else
			subType = subType.value
		end
	else
		item:setItemCount(count)
	end
	
	if list[ITEM_INSPECT] then
		widget:getChildById('cooldown'):hide()
		widget:getChildById('closeButton'):hide()
		item:setOn(true)
	else
		local tmpWidget = widget:getChildById('closeButton')
		tmpWidget.onMouseRelease = m_LookAtFunctions.clickCooldownButton
		tmpWidget.onHoverChange = m_LookAtFunctions.onHoverChange
		m_LookAtFunctions.sendAttributeCooldown(widget)
	end
	
	m_LookAtFunctions.execute(item, widget, it, list, math.max(1, count), subType, serverId, clientId)
	m_LookAtList.window[socked] = widget
end

m_LookAtFunctions.onHoverChange = function(self, hovered)
	if not hovered or not modules.client_options.getOption('displayHints') then
		if m_LookAtList.hover then
			m_LookAtList.hover:destroy()
			m_LookAtList.hover = nil
		end
		
		return true
	end
	
	local rootWidget = modules.game_interface.getRootPanel()
	m_LookAtList.hover = g_ui.createWidget('LootAtHoverPanel', rootWidget)
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 8
	pos.y = pos.y + self:getHeight() - 8
	
	if pos.x + m_LookAtList.hover:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_LookAtList.hover:getWidth()
	end
	
	if pos.y + m_LookAtList.hover:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_LookAtList.hover:getHeight()
	end
	
	m_LookAtList.hover:setPosition(pos)
end

m_LookAtFunctions.parseItemAttributes = function(protocol, msg)
	local moveItem = msg:getU8() == 1
	local clientId = msg:getU16()
	local serverId = msg:getU16()
	
	local list = {}
	local size = msg:getU8()
	for i = 1, size do
		local id = msg:getU16()
		local value
		if msg:getU8() == 1 then
			value = msg:getString()
		else
			value = msg:getU32()
		end
		
		local type = msg:getU8()
		if id == ITEM_SKILL then
			if not list[id] then
				list[id] = {}
			end
			
			table.insert(list[id], {value = value, type = type})
		else
			list[id] = {value = value, type = type}
		end
	end
	
	if moveItem then
		modules.game_lookitemmove.m_LookItemMoveFunction.open(m_LookAtFunctions.getItemDescription(serverId), list)
	else
		m_LookAtFunctions.open(clientId, serverId, list)
	end
end

m_LookAtFunctions.sendAttributeCooldown = function(self)
	local widget = self:getChildById('cooldown')
	if self:isOn() then
		widget.count = (widget.count or 1) + 1
		widget:setBackgroundColor(widget.color .. (widget.count % 2 == 0 and 'AA' or ''))
		self.event = scheduleEvent(function()
			m_LookAtFunctions.sendAttributeCooldown(self)
		end, 500)
	else
		widget:setPercent(100 - ((self.cooldown / (m_LookAtFunctions.duration or 100)) * 100))
		
		if self.cooldown > 0 then
			self.event = scheduleEvent(function()
				self.cooldown = self.cooldown - 1
				m_LookAtFunctions.sendAttributeCooldown(self)
			end, 100)
		else
			m_LookAtFunctions.destroyWindow(self.id)
		end
	end
end

m_LookAtFunctions.clickCooldownButton = function(self, mousePosition, mouseButton)
	if mouseButton == MouseRightButton then
		local widget = self:getParent()
		if widget.event then
			removeEvent(widget.event)
		end
		
		widget:setOn(not widget:isOn())
		if not widget:isOn() then
			local tmpWidget = widget:getChildById('cooldown')
			tmpWidget:setBackgroundColor(tmpWidget.color)
		end
		
		m_LookAtFunctions.sendAttributeCooldown(widget)
	else
		m_LookAtFunctions.destroyWindow(self:getParent().id)
	end
end

m_LookAtFunctions.destroy = function()
	if m_LookAtList.window then
		for i = m_LookAtFunctions.config.maximumItems, 1, -1 do
			m_LookAtFunctions.destroyWindow(i)
		end
	end
	
	m_LookAtFunctions.destroyHover()
	m_LookAtList.window = {}
end

m_LookAtFunctions.destroyWindow = function(id)
	m_LookAtFunctions.destroyHover()
	
	if not m_LookAtList.window then
		return true
	end
	
	local widget = m_LookAtList.window[id]
	if widget then
		if widget.event then
			removeEvent(widget.event)
		end
		
		widget:destroy()
		m_LookAtList.window[id] = nil
	end
end

m_LookAtFunctions.destroyHover = function()
	if m_LookAtList.hover then
		m_LookAtList.hover:destroy()
		m_LookAtList.hover = nil
	end
	
	if m_LookAtList.slotsHover then
		m_LookAtList.slotsHover:destroy()
		m_LookAtList.slotsHover = nil
	end
end

m_LookAtFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_LookAtFunctions.GameServerItemAttributes, m_LookAtFunctions.parseItemAttributes)
end

m_LookAtFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_LookAtFunctions.GameServerItemAttributes, m_LookAtFunctions.parseItemAttributes)
end

m_LookAtFunctions.setDuration = function(value)
	m_LookAtFunctions.duration = (tonumber(value) * 10) or 100
end

m_LookAtFunctions.getItemList = function()
	return m_LookAtFunctions.itemListByType
end

m_LookAtFunctions.getMonstersList = function(classId)
	return m_LookAtFunctions.monsterListByClass[classId]
end

m_LookAtFunctions.getMonstersNameList = function()
	if m_LookAtFunctions.monsterListByName then
		return m_LookAtFunctions.monsterListByName
	end
	
	m_LookAtFunctions.monsterListByName = {}
	for i = 1, #monsters do
		if monsters[i].classId and monsters[i].description then
			table.insert(m_LookAtFunctions.monsterListByName, {name = monsters[i].name, description = monsters[i].description})
		end
	end
	
	return m_LookAtFunctions.monsterListByName
end

m_LookAtFunctions.getMonsterByName = function(name)
	for i = 1, #monsters do
		if monsters[i].classId and monsters[i].name:lower() == name:lower() then
			return monsters[i]
		end
	end
	
	return false
end

m_LookAtFunctions.getMonsterByDescription = function(name)
	for i = 1, #monsters do
		if monsters[i].classId and monsters[i].description and isInArray(monsters[i].description, name) then
			return monsters[i].name
		end
	end
	
	return false
end

m_LookAtFunctions.getNPCTradeListByName = function(name)
	return TRADE_WINDOW[name]
end

m_LookAtFunctions.getNPCTravelListByName = function(name)
	return TRAVEL_WINDOW[name]
end

m_LookAtFunctions.getNPCByName = function(name)
	for i = 1, #npc do
		if npc[i].name == name then
			return npc[i]
		end
	end
	
	return false
end

m_LookAtFunctions.getNPCList = function(itemId)
	local list = {}
	for k, v in pairs(TRADE_WINDOW) do
		for i = 1, #v do
			if v[i].id == itemId then
				local outfit
				for j = 1, #npc do
					if npc[j].name == k then
						outfit = npc[j].look
						break
					end
				end
				
				if outfit then
					table.insert(list, {name = k, sell = v[i].sell, buy = v[i].buy, look = outfit})
				end
				
				break
			end
		end
	end
	
	return list
end

m_LookAtFunctions.getMonsterList = function(itemId)
	local list = {}
	for i = 1, #monsters do
		local v = monsters[i]
		if v.loot then
			for j = 1, #v.loot do
				local var = v.loot[j]
				if type(var.id) == 'table' then
					local begin = false
					for k = 1, #var.id do
						if var.id[k] == itemId then
							table.insert(list, v)
							begin = true
							break
						end
					end
					
					if begin then
						break
					end
				else
					if var.id == itemId then
						table.insert(list, v)
						break
					end
				end
			end
		end
	end
	
	return list
end

m_LookAtFunctions.load = function(list, isCorpse)
	for i = 1, #list do
		local v = list[i]
		v.itemType = g_game.getItemType(v)
		
		if isCorpse then
			v.attributes.isCorpse = true
			v.attributes.duration = 180
			v.attributes.containersize = 36
		end
		
		if v.clientId then
			m_LookAtFunctions.itemListByClientId[v.clientId] = v
		end
		
		if v.id then
			m_LookAtFunctions.itemListByServerId[v.id] = v
		end
		
		if v.itemType ~= ITEMTYPE_NONE then
			if v.itemType ~= ITEMTYPE_PREMIUM or v.attributes.weight then
				if not m_LookAtFunctions.itemListByType[v.itemType] then
					m_LookAtFunctions.itemListByType[v.itemType] = {}
				end
				
				v.list = m_LookAtFunctions.getMonsterList(v.id)
				v.npcList = m_LookAtFunctions.getNPCList(v.id)
				table.insert(m_LookAtFunctions.itemListByType[v.itemType], v)
			end
		end
	end
end

function onLoad()
	connect(g_game, {
		onGameEnd = onGameEnd,
		onSendCreatureDescription = onSendCreatureDescription
	})
	
	g_ui.importStyle('look_item')
	g_ui.importStyle('look_player')
	g_ui.importStyle('look_monster')
	g_ui.importStyle('game_lookat')
	m_LookAtFunctions.registerProtocol()
	
	m_LookAtFunctions.load(corpses, true)
	m_LookAtFunctions.load(items, false)
	
	for i = 1, #monsters do
		local v = monsters[i]
		if v.classId then
			if not m_LookAtFunctions.monsterListByClass[v.classId] then
				m_LookAtFunctions.monsterListByClass[v.classId] = {}
			end
			
			v.health = math.ceil(v.health * m_LookAtFunctions.healthMultiplier)
			v.healthMax = math.ceil(v.healthMax * m_LookAtFunctions.healthMultiplier)
			v.experience = math.ceil(v.experience * m_LookAtFunctions.experienceMultiplier)
			
			if not v.killAmount then
				v.killAmount = {50, 500, 1000}
			else
				if v.killAmount == 15 then
					v.killAmount = {5, 10, 15}
				elseif v.killAmount == 25 then
					v.killAmount = {5, 10, 25}
				elseif v.killAmount == 250 then
					v.killAmount = {10, 100, 250}
				elseif v.killAmount == 500 then
					v.killAmount = {25, 250, 500}
				elseif v.killAmount == 1000 then
					v.killAmount = {50, 500, 1000}
				elseif v.killAmount == 2500 then
					v.killAmount = {100, 1000, 2500}
				end
			end
			
			table.insert(m_LookAtFunctions.monsterListByClass[v.classId], v)
		end
	end
	
	items = {}
	corpses = {}
end

function onUnLoad()
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onSendCreatureDescription = onSendCreatureDescription
	})

	m_LookAtFunctions.destroy()
	m_LookAtFunctions.unregisterProtocol()
	
	m_LookAtFunctions.itemListByServerId = {}
	m_LookAtFunctions.itemListByClientId = {}
	m_LookAtFunctions.itemListByType = {}
	m_LookAtFunctions.monsterListByClass = {}
end

function onGameEnd()
	m_LookAtFunctions.destroy()
end

function onSendCreatureDescription(description, virtualOutfitCreature)
	if not m_LookAtList.window then
		m_LookAtList.window = {}
	end
	
	local socked = m_LookAtFunctions.getFreeSocked()
	if not socked then
		return true
	end
	
	local list = {}
	local strVector = description:explode(';')
	for i = 1, #strVector do
		local tmpStrVector = strVector[i]:explode('_')
		list[tmpStrVector[1]] = tmpStrVector[2]
	end
	
	local widget = nil
	local progress = {0, 0}
	if list['type'] == 'player' then
		widget = g_ui.createWidget('UILookAtPlayerWindow', modules.game_interface.getRootPanel())
		
		local value = math.ceil((list['experience'] - list['experiencePrev']) / (list['experienceLevel'] - list['experiencePrev']) * 10000) / 100
		progress = {value, g_game.getMilharNumber(list['experience']) .. ' / ' .. g_game.getMilharNumber(list['experienceLevel']) .. ' (' .. value .. '%)'}
	else
		widget = g_ui.createWidget('UILookAtMonsterWindow', modules.game_interface.getRootPanel())
		
		local value = math.ceil(list['health'] / list['maxHealth'] * 10000) / 100
		progress = {value, g_game.getMilharNumber(list['health']) .. ' / ' .. g_game.getMilharNumber(list['maxHealth']) .. ' (' .. value .. '%)'}
	end
	
	local progressBar = widget:getChildById('progressBar')
	progressBar:setPercent(progress[1])
	progressBar:setText(progress[2])
	
	local defaultPos = {x = 50, y = 50}
	if socked > 1 then
		local tmpWidget = m_LookAtList.window[socked - 1]
		if tmpWidget then
			defaultPos = {x = tmpWidget:getX() + tmpWidget:getWidth() + 32, y = tmpWidget:getY()}
		end
	end
	
	widget:setPosition(defaultPos)
	widget:setId('protected' .. 100 + socked)
	widget:setup()
	widget.id = socked
	widget.time = os.time()
	widget.cooldown = m_LookAtFunctions.duration or 100
	widget:open()
	widget:getChildById('creature'):setCreature(virtualOutfitCreature)
	m_LookAtFunctions.sendAttributeCooldown(widget)
	m_LookAtList.window[socked] = widget
	
	for k, v in pairs(list) do
		local tmpWidget = widget:getChildById(k)
		if tmpWidget then
			tmpWidget:getChildById('label'):setText(v)
		end
	end
end