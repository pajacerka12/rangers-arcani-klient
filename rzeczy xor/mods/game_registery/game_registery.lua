m_RegisteryFunctions = {}
m_RegisteryList = {}

m_RegisteryFunctions.loginSize = {3, 64}
m_RegisteryFunctions.passwordSize = {3, 64}
m_RegisteryFunctions.nameSize = {4, 20}

m_RegisteryFunctions.password = ''
m_RegisteryFunctions.account = ''
m_RegisteryList.vocation = 0

m_RegisteryFunctions.language = LANGUAGE_ENGLISH

m_RegisteryFunctions.color = {'#C60000', '#FFFFFF'}

function onLoad()
	if not g_game.isOnline() then
		CharacterList.destroyList()
	else
		CharacterList.hide()
	end
	
	m_RegisteryFunctions.loadLanguage()
end

function onUnload()
	if not g_game.isOnline() then
		CharacterList.destroyList()
	else
		CharacterList.hide()
	end
	
	m_RegisteryFunctions.destroy()
end

m_RegisteryFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_RegisteryFunctions.language = LANGUAGE_ENGLISH
	else
		m_RegisteryFunctions.language = LANGUAGE_POLISH
	end
end

m_RegisteryFunctions.destroy = function()
	if m_RegisteryList.window then
		m_RegisteryList.window:destroy()
		m_RegisteryList.window = nil
	end
	
	m_RegisteryList = {}
end

m_RegisteryFunctions.executePasswords = function(password, account)
	m_RegisteryFunctions.password = password
	m_RegisteryFunctions.account = account
end

m_RegisteryFunctions.clear = function()
	m_RegisteryFunctions.password = ''
	m_RegisteryFunctions.account = ''
	m_RegisteryList.vocation = 0
end

m_RegisteryFunctions.back = function(ignore)
	m_RegisteryFunctions.destroy()
	EnterGame.destroyLoadBox()
	if not ignore then
		EnterGame.show()
		return
	end
	
	-- m_RegisteryFunctions.clear()
	modules.client_background.setTip(tr(onLoginTip))
	modules.client_background.showLogo()
	CharacterList.destroyList()
end

m_RegisteryFunctions.openRegistery = function()
	if m_RegisteryList.window and m_RegisteryList.window.character then
		m_RegisteryFunctions.destroy()
	end

	if not m_RegisteryList.window then
		modules.client_entergame.EnterGame.hide()
		m_RegisteryList.window = g_ui.displayUI('game_registery')
		m_RegisteryList.window:getChildById('button'):setOn(m_RegisteryFunctions.language == LANGUAGE_ENGLISH)
		
		m_RegisteryList.currentSelect = m_RegisteryList.window:getChildById('serverList'):getChildById('fr_server')
		m_RegisteryList.currentSelect:setOn(true)
		
		-- m_RegisteryFunctions.clear()
		modules.client_background.setTip(tr(onRegisterTip))
		modules.client_background.showLogo()
		CharacterList.destroyList()
	end
end

m_RegisteryFunctions.checkText = function(self, var)
	local text = self:getText():len()
	if text < var[1] or text > var[2] then
		self:setColor(m_RegisteryFunctions.color[1])
	else
		self:setColor(m_RegisteryFunctions.color[2])
	end
end

m_RegisteryFunctions.onAccountNameChange = function(self)
	m_RegisteryFunctions.checkText(self, m_RegisteryFunctions.loginSize)
	if m_RegisteryList.window:getChildById('accountNameTextEdit'):getText() == '' then
		m_RegisteryList.window:getChildById('accountNameLabel'):show()
	else
		m_RegisteryList.window:getChildById('accountNameLabel'):hide()
	end
end

m_RegisteryFunctions.onPasswordsChange = function(self)
	m_RegisteryFunctions.checkText(self, m_RegisteryFunctions.passwordSize)
	if m_RegisteryList.window:getChildById('accountPasswordTextEdit'):getText() == '' then
		m_RegisteryList.window:getChildById('accountPasswordLabel'):show()
	else
		m_RegisteryList.window:getChildById('accountPasswordLabel'):hide()
	end
end

m_RegisteryFunctions.onMailChange = function(self)
	self:setColor(m_RegisteryFunctions.color[2])
	if m_RegisteryList.window:getChildById('accountMailTextEdit'):getText() == '' then
		m_RegisteryList.window:getChildById('accountMailLabel'):show()
	else
		m_RegisteryList.window:getChildById('accountMailLabel'):hide()
	end
end

m_RegisteryFunctions.onCharacterNameChange = function(self)
	if not m_RegisteryList.window then
		return false
	end

	m_RegisteryFunctions.checkText(self, m_RegisteryFunctions.nameSize)
	if m_RegisteryList.window:getChildById('nameTextEdit'):getText() == '' then
		m_RegisteryList.window:getChildById('nameLabel'):show()
	else
		m_RegisteryList.window:getChildById('nameLabel'):hide()
	end
end

m_RegisteryFunctions.onPasswordChange = function(self)
	m_RegisteryFunctions.checkText(self, m_RegisteryFunctions.passwordSize)
end

m_RegisteryFunctions.newPassword = function()
	m_RegisteryFunctions.back(false)
end

m_RegisteryFunctions.checkPassword = function()
	m_RegisteryFunctions.password = m_RegisteryList.window:getChildById('accountPasswordTextEdit'):getText()
	m_RegisteryFunctions.account = m_RegisteryList.window:getChildById('accountNameTextEdit'):getText()
end

m_RegisteryFunctions.onChangePassword = function()
	if m_RegisteryList.window then
		m_RegisteryFunctions.checkPassword()
		m_RegisteryFunctions.destroy()
	end
	
	m_RegisteryList.window = g_ui.displayUI('game_changepassword')
	if m_RegisteryFunctions.language == LANGUAGE_ENGLISH then
		for i = 1, 3 do
			m_RegisteryList.window:getChildById('button' .. i):setOn(true)
		end
	end
end

m_RegisteryFunctions.onCreateAccount = function(character)
	if m_RegisteryList.window then
		m_RegisteryFunctions.checkPassword()
		m_RegisteryFunctions.destroy()
	end
	
	EnterGame.destroyLoadBox()
	m_RegisteryList.window = g_ui.displayUI('game_createaccount')
	m_RegisteryList.window.character = character
	modules.client_background.setTip(tr(onCreateCharacterTip))
	modules.client_background.hideLogo()
	
	if m_RegisteryFunctions.language == LANGUAGE_ENGLISH then
		m_RegisteryList.window:getChildById('create'):setOn(true)
		for i = 1, 3 do
			m_RegisteryList.window:getChildById('vocation' .. i):setOn(true)
		end
	end
	
	if character then
		m_RegisteryList.window:getChildById('label'):setChecked(true)
		
		local widget = m_RegisteryList.window:getChildById('back')
		widget:setOn(m_RegisteryFunctions.language == LANGUAGE_ENGLISH)
		widget:show()
	end
end

m_RegisteryFunctions.onErrorExistAccount = function()
	EnterGame.destroyLoadBox()
	
	m_RegisteryList.window:getChildById('accountNameTextEdit'):setColor(m_RegisteryFunctions.color[2])
	m_RegisteryList.window:getChildById('onError'):setText(tr(onAccountExist))
end

m_RegisteryFunctions.onCharacterError = function(description)
	EnterGame.destroyLoadBox()
	modules.client_background.setTip(description)
end

m_RegisteryFunctions.cancel = function()
	m_RegisteryFunctions.destroy()
	CharacterList.show()
end

m_RegisteryFunctions.select = function(self, id)
	for i = 1, 3 do
		m_RegisteryList.window:getChildById('vocation' .. i):setChecked(false)
	end
	
	self:setChecked(true)
	m_RegisteryList.vocation = tonumber(id:sub(id:len(), id:len()))
end

m_RegisteryFunctions.onRemoveCharacter = function(name)
	if m_RegisteryList.window then
		m_RegisteryList.window:destroy()
		m_RegisteryList.window = nil
	end
	
	m_RegisteryList.window = g_ui.displayUI('game_removecharacter')
	
	local widget = {
		m_RegisteryList.window:getChildById('buttonYes'),
		m_RegisteryList.window:getChildById('buttonNo')
	}
	widget[1]:setOn(m_RegisteryFunctions.language == LANGUAGE_ENGLISH)
	widget[2]:setOn(m_RegisteryFunctions.language == LANGUAGE_ENGLISH)
	
	m_RegisteryList.window:getChildById('label'):setText(tr('Are you sure to remove the character named %s? The action will be permanent.', name))
	connect(widget[1], { onClick = function() m_RegisteryFunctions.removeCharacter(name) end })
	connect(widget[2], { onClick = m_RegisteryFunctions.backToCharacterList })
	connect(m_RegisteryList.window, { onEnter = onOk })
	connect(m_RegisteryList.window, { onEscape = onOk })
end

m_RegisteryFunctions.removeCharacter = function(name)
	local G = {}
	G.account = m_RegisteryFunctions.account
	G.password = m_RegisteryFunctions.password
	G.name = name
	
	EnterGame.doLogin(G, ACCOUNT_DELETE)
end

m_RegisteryFunctions.backToCharacterList = function()
	m_RegisteryFunctions.destroy()
	CharacterList.show()
	
	modules.client_background.setTip(tr(onCharacterListTip[math.random(#onCharacterListTip)]))
end

m_RegisteryFunctions.checkIP = function(G)
	if not G then
		G = {}
	end
	
	if m_RegisteryList.currentSelect then
		if m_RegisteryList.currentSelect:getId() == 'cn_server' then
			G.currentId = 2
		elseif m_RegisteryList.currentSelect:getId() == 'fr_server' then
			G.currentId = 1
		else
			return false
		end
	end
	
	return G
end

m_RegisteryFunctions.doChangePassword = function()
	local G = m_RegisteryFunctions.checkIP(G)
	if not G then
		return false
	end
	
	local oldPassword = m_RegisteryList.window:getChildById('oldPasswordTextEdit')
	local newPassword = m_RegisteryList.window:getChildById('newPasswordTextEdit')
	local confirmPassword = m_RegisteryList.window:getChildById('confirmPasswordTextEdit')
	
	local text = oldPassword:getText()
	if text == '' or text ~= m_RegisteryFunctions.password then
		oldPassword:setColor(m_RegisteryFunctions.color[1])
		return true
	end
	
	local text = confirmPassword:getText()
	if text == '' or text ~= newPassword:getText() then
		confirmPassword:setColor(m_RegisteryFunctions.color[1])
		return true
	end
	
	local size = text:len()
	if size < m_RegisteryFunctions.passwordSize[1] or size > m_RegisteryFunctions.passwordSize[2] then
		newPassword:setColor(m_RegisteryFunctions.color[1])
		confirmPassword:setColor(m_RegisteryFunctions.color[1])
		return false
	end
	
	G.account = m_RegisteryFunctions.account
	G.password = m_RegisteryFunctions.password
	G.newPassword = text
	
	EnterGame.doLogin(G, ACCOUNT_PASSWORD)
end

m_RegisteryFunctions.create = function()
	if m_RegisteryList.vocation == 0 or m_RegisteryFunctions.account == '' or m_RegisteryFunctions.password == '' then
		return false
	end
	
	local G = m_RegisteryFunctions.checkIP(G)
	if not G then
		return false
	end
	
	local widget = m_RegisteryList.window:getChildById('nameTextEdit')
	local text = widget:getText()
	if text == '' then
		return false
	end
	
	local size = text:len()
	if size < m_RegisteryFunctions.nameSize[1] or size > m_RegisteryFunctions.nameSize[2] then
		widget:setColor(m_RegisteryFunctions.color[1])
		return false
	end
	
	G.name = text
	G.account = m_RegisteryFunctions.account
	G.password = m_RegisteryFunctions.password
	G.vocation = m_RegisteryList.vocation
	
	EnterGame.doLogin(G, ACCOUNT_CHARACTER)
end

m_RegisteryFunctions.register = function()
	m_RegisteryList.window:getChildById('onError'):clearText()
	
	local G = m_RegisteryFunctions.checkIP(G)
	if not G then
		return false
	end
	
	local account = m_RegisteryList.window:getChildById('accountNameTextEdit')
	local size = account:getText():len()
	if size < m_RegisteryFunctions.loginSize[1] or size > m_RegisteryFunctions.loginSize[2] then
		account:setColor(m_RegisteryFunctions.color[1])
		return false
	end
	
	local password = m_RegisteryList.window:getChildById('accountPasswordTextEdit')
	local size = password:getText():len()
	if size < m_RegisteryFunctions.passwordSize[1] or size > m_RegisteryFunctions.passwordSize[2] then
		password:setColor(m_RegisteryFunctions.color[1])
		return false
	end
	
	local mail = m_RegisteryList.window:getChildById('accountMailTextEdit')
	local text = mail:getText()
	if m_RegisteryFunctions.checkValidEmail(text) then
		mail:setColor(m_RegisteryFunctions.color[1])
		return false
	end
	
	G.account = account:getText()
	G.password = password:getText()
	G.mail = 
	
	EnterGame.doLogin(G, ACCOUNT_CREATE)
end

m_RegisteryFunctions.checkValidEmail = function(text)
	if text == '' then
		return true
	end
	
	-- The email address must start with a letter (no numbers or symbols).
	if not text:sub(1, 1):match("%a") then
		return true
	end
	
	-- There must be an @ somewhere in the string that is located before the dot.
	local text = text:explode('@')
	if #text ~= 2 then
		return true
	end
	
	-- There must be text after the @ symbol but before the dot.
	local text = text[2]:explode('%.')
	if #text ~= 2 then
		return true
	end
	
	if text[1]:len() < 2 then
		return true
	end
	
	-- There must be a dot and text after the dot.
	if not text[2]:match("%a") then
		return true
	end
	
	return false
end

m_RegisteryFunctions.check = function(self)
	if m_RegisteryList.currentSelect then
		m_RegisteryList.currentSelect:setOn(false)
	end
	
	m_RegisteryList.currentSelect = self
	m_RegisteryList.currentSelect:setOn(true)
end