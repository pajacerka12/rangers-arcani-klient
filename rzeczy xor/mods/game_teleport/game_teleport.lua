m_TeleportFunctions = {}
m_TeleportList = {}

m_TeleportFunctions.crystalId = 15476
m_TeleportFunctions.amount = 18

m_TeleportFunctions.destroy = function()
	if m_TeleportList.window then
		m_TeleportList.window:destroy()
		m_TeleportList = {}
	end
end

m_TeleportFunctions.select = function(self)
	if m_TeleportList.lastSelected == self or self:isOn() then
		return true
	end
	
	if m_TeleportList.lastSelected then
		m_TeleportList.lastSelected:getChildById('blink'):hide()
	end
	
	m_TeleportList.lastSelected = self
	self:getChildById('blink'):show()
end

m_TeleportFunctions.teleport = function()
	if not m_TeleportList.lastSelected then
		return true
	end
	
	g_game.answerModalDialog(m_TeleportList.id, m_TeleportList.lastSelected.id)
end

function onTeleportTiles(id, choices)
	if #choices == 0 then
		m_TeleportFunctions.destroy()
		return true
	end

	if m_TeleportList.window then
		return false
	end
	
	m_TeleportList.window = g_ui.displayUI('game_teleport')
	m_TeleportList.panel = m_TeleportList.window:getChildById('panel')
	m_TeleportList.button = m_TeleportList.window:getChildById('button')
	m_TeleportList.item = m_TeleportList.button:getChildById('item')
	m_TeleportList.id = id
	
	m_TeleportList.button.onText = function(self, coords, text, iconCoords)
		for _, v in pairs(iconCoords) do
			local id, icon = unpack(v)
			if icon.x ~= 0 and icon.y ~= 0 then
				local pos = self:getPosition()
				local offset = {x = icon.x - pos.x, y = icon.y - pos.y - 3}
				
				m_TeleportList.item:setMarginLeft(offset.x)
				m_TeleportList.item:setItemId(m_TeleportFunctions.crystalId)
			end
		end
	end
	
	local list = {}
	for i = 1, #choices do
		list[choices[i][1]] = choices[i][2]
	end
	
	for i = 1, m_TeleportFunctions.amount do
		local widget = g_ui.createWidget('TeleportOptionWidget', m_TeleportList.panel)
		local image = widget:getChildById('image')
		local label = widget:getChildById('label')
		local v = list[i]
		if not v then
			image:hide()
			widget:setOn(true)
		else
			widget.id = i
			image:setImageSource('images/' .. i)
			label:setText(v)
			
			if not m_TeleportList.lastSelected then
				m_TeleportFunctions.select(widget)
			end
		end
	end
end

function onGameEnd()
	m_TeleportFunctions.destroy()
end

function onGameStart()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_TeleportFunctions.language = LANGUAGE_ENGLISH
	else
		m_TeleportFunctions.language = LANGUAGE_POLISH
	end
end

function onLoad()
	connect(g_game, {
		onTeleportTiles = onTeleportTiles,
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	if g_game.isOnline() then
		onGameStart()
	end
end

function onUnload()
	disconnect(g_game, {
		onTeleportTiles = onTeleportTiles,
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	m_TeleportFunctions.destroy()
end