m_MarketFunctions = {}
m_MarketList = {}

m_MarketFunctions.language = LANGUAGE_ENGLISH

m_MarketFunctions.GameServerManageMarket = 92

m_MarketFunctions.ClientManageMarket = 137

m_MarketFunctions.PAGE_BUY_OFFERS = 1
m_MarketFunctions.PAGE_SELL_OFFERS = 2
m_MarketFunctions.PAGE_YOUR_BUY_OFFERS = 3
m_MarketFunctions.PAGE_YOUR_SELL_OFFERS = 4
m_MarketFunctions.PAGE_HISTORY = 5

m_MarketFunctions.UPDATE_NONE = 0
m_MarketFunctions.UPDATE_BUY_OFFERS = 1
m_MarketFunctions.UPDATE_SELL_OFFERS = 2
m_MarketFunctions.UPDATE_PLAYER_BUY_OFFERS = 3
m_MarketFunctions.UPDATE_PLAYER_SELL_OFFERS = 4
m_MarketFunctions.UPDATE_HISTORY = 5
m_MarketFunctions.UPDATE_SEARCH = 6
m_MarketFunctions.UPDATE_BALANCE = 7

m_MarketFunctions.SORT_BY_NONE = 0
m_MarketFunctions.SORT_BY_NAME_LOWER = 1
m_MarketFunctions.SORT_BY_NAME_HIGHER = 2
m_MarketFunctions.SORT_BY_OWNER_LOWER = 3
m_MarketFunctions.SORT_BY_OWNER_HIGHER = 4
m_MarketFunctions.SORT_BY_COUNT_LOWER = 5
m_MarketFunctions.SORT_BY_COUNT_HIGHER = 6
m_MarketFunctions.SORT_BY_PRICE_LOWER = 7
m_MarketFunctions.SORT_BY_PRICE_HIGHER = 8
m_MarketFunctions.SORT_BY_TIME_LOWER = 9
m_MarketFunctions.SORT_BY_TIME_HIGHER = 10
m_MarketFunctions.SORT_BY_QUALITY_LOWER = 11
m_MarketFunctions.SORT_BY_QUALITY_HIGHER = 12

m_MarketFunctions.sortList = {
	{tr('Ascending by quality'), m_MarketFunctions.SORT_BY_QUALITY_HIGHER},
	{tr('Descending by quality'), m_MarketFunctions.SORT_BY_QUALITY_LOWER},
	{tr('Ascending by name'), m_MarketFunctions.SORT_BY_NAME_HIGHER},
	{tr('Descending by name'), m_MarketFunctions.SORT_BY_NAME_LOWER},
	{tr('Ascending by owner'), m_MarketFunctions.SORT_BY_OWNER_HIGHER},
	{tr('Descending by owner'), m_MarketFunctions.SORT_BY_OWNER_LOWER},
	{tr('Ascending by time left'), m_MarketFunctions.SORT_BY_TIME_HIGHER},
	{tr('Descending by time left'), m_MarketFunctions.SORT_BY_TIME_LOWER},
	{tr('Ascending by count'), m_MarketFunctions.SORT_BY_COUNT_HIGHER},
	{tr('Descending by count'), m_MarketFunctions.SORT_BY_COUNT_LOWER},
	{tr('Ascending by price'), m_MarketFunctions.SORT_BY_PRICE_HIGHER},
	{tr('Descending by price'), m_MarketFunctions.SORT_BY_PRICE_LOWER}
}

m_MarketFunctions.qualityList = {
	{tr('Desolate'), ITEMCLASS_DESOLATE},
	{tr('Damaged'), ITEMCLASS_DAMAGED},
	{tr('Normal'), ITEMCLASS_NORMAL},
	{tr('Perfect'), ITEMCLASS_PERFECT},
	{tr('Legendary'), ITEMCLASS_LEGENDARY},
	{tr('Unique'), ITEMCLASS_UNIQUE}
}

m_MarketFunctions.disablesQuality = {ITEMTYPE_AMMUNITION, ITEMTYPE_POTION, ITEMTYPE_FOOD, ITEMTYPE_VALUABLE, ITEMTYPE_CREATURE_PRODUCT, ITEMTYPE_DOLL, ITEMTYPE_TROPHY, ITEMTYPE_CONTAINERS, ITEMTYPE_PREMIUM}

m_MarketFunctions.sellCategories = {'One handed weapons', 'Two handed weapons', 'Bows', 'Crossbows', 'Spears', 'Ammunition', 'Quivers', 'Wands', 'Helmets', 'Armors', 'Legs', 'Boots', 'Shields', 'Necklaces', 'Rings', 'Containers', 'Runes', 'Potions', 'Food', 'Valuables', 'Creature Products', 'Crafting Materials', 'Dolls', 'Trophies', 'Premium Items'}
m_MarketFunctions.categories = {'One handed weapons', 'Two handed weapons', 'Bows', 'Crossbows', 'Spears', 'Ammunition', 'Quivers', 'Wands', 'Helmets', 'Armors', 'Legs', 'Boots', 'Shields', 'Necklaces', 'Rings', 'Containers', 'Runes', 'Potions', 'Food', 'Valuables', 'Creature Products', 'Crafting Materials', 'Dolls', 'Trophies', 'Premium Items', 'Scrolls', 'All'}
m_MarketFunctions.categoryList = {
	['One handed weapons'] = ITEMTYPE_ONEHANDED_MELEE,
	['Two handed weapons'] = ITEMTYPE_TWOHANDED_MELEE,
	['Bows'] = ITEMTYPE_BOW,
	['Crossbows'] = ITEMTYPE_CROSSBOW,
	['Spears'] = ITEMTYPE_SPEAR,
	['Ammunition'] = ITEMTYPE_AMMUNITION,
	['Quivers'] = ITEMTYPE_QUIVER,
	['Wands'] = ITEMTYPE_WAND,
	['Helmets'] = ITEMTYPE_HELMET,
	['Armors'] = ITEMTYPE_ARMOR,
	['Legs'] = ITEMTYPE_LEGS,
	['Boots'] = ITEMTYPE_FEET,
	['Shields'] = ITEMTYPE_SHIELD,
	['Necklaces'] = ITEMTYPE_NECKLACE,
	['Rings'] = ITEMTYPE_RING,
	['Containers'] = ITEMTYPE_BACKPACK,
	['Runes'] = ITEMTYPE_RUNE,
	['Potions'] = ITEMTYPE_POTION,
	['Food'] = ITEMTYPE_FOOD,
	['Valuables'] = ITEMTYPE_VALUABLE,
	['Creature Products'] = ITEMTYPE_CREATURE_PRODUCT,
	['Crafting Materials'] = ITEMTYPE_CRAFTING_MATERIAL,
	['Dolls'] = ITEMTYPE_DOLL,
	['Trophies'] = ITEMTYPE_TROPHY,
	['Premium Items'] = ITEMTYPE_PREMIUM,
	['All'] = ITEMTYPE_NONE
}

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	if g_game.isOnline() then
		m_MarketFunctions.registerProtocol()
	end
end

function onUnload()
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	onGameEnd()
end

function onGameEnd()
	m_MarketFunctions.destroy()
	m_MarketFunctions.unregisterProtocol()
end

function onGameStart()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_MarketFunctions.language = LANGUAGE_ENGLISH
	else
		m_MarketFunctions.language = LANGUAGE_POLISH
	end
	
	m_MarketFunctions.registerProtocol()
end

function onTextChange(self)
	if self:getText() ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
end

m_MarketFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_MarketFunctions.ClientManageMarket, m_MarketFunctions.parseManageMarket)
end

m_MarketFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_MarketFunctions.ClientManageMarket, m_MarketFunctions.parseManageMarket)
end

m_MarketFunctions.updateBalance = function(balance)
	m_MarketFunctions.balance = balance
	if m_MarketList.balance then
		m_MarketList.balance:setText(g_game.getMilharNumber(m_MarketFunctions.balance))
	end
end

m_MarketFunctions.create = function()
	if m_MarketList.window then
		m_MarketFunctions.destroy()
	else
		modules.game_character.m_CharacterFunctions.destroy()
		modules.game_encyclopedia.m_EncyclopediaFunctions.destroy()
		modules.game_rewards.m_RewardsFunctions.destroy()
		modules.game_questlog.m_QuestLogFunctions.destroy()
		modules.game_store.m_StoreFunctions.onCloseStore()
		
		m_MarketList.window = g_ui.displayUI('game_market')
		m_MarketList.balance = m_MarketList.window:getChildById('balance')
		if m_MarketFunctions.balance then
			m_MarketList.balance:setText(g_game.getMilharNumber(m_MarketFunctions.balance))
		end
		
		m_MarketList.buttons = {}
		for i = 1, 5 do
			m_MarketList.buttons[i] = m_MarketList.window:getChildById(i)
		end
		
		m_MarketFunctions.select(m_MarketList.buttons[4], m_MarketFunctions.PAGE_YOUR_SELL_OFFERS)
		modules.game_bottompanel.m_MainFunctions.open(WINDOW_MARKET)
	end
end

m_MarketFunctions.destroy = function()
	if m_MarketList.window then
		m_MarketFunctions.destroyMouseGrabber()
		m_MarketFunctions.destroySubWindow()
		m_MarketFunctions.removeEvent()
		m_MarketFunctions.removeUpdateEvent()
		m_MarketFunctions.removeBalanceEvent()
		m_MarketList.window:destroy()
		m_MarketList = {}
		
		modules.game_bottompanel.m_MainFunctions.close(WINDOW_MARKET)
		m_MarketFunctions.protocol = nil
	end
end

m_MarketFunctions.destroyMouseGrabber = function()
	if m_MarketList.mouseGrabber then
		m_MarketList.mouseGrabber:destroy()
		m_MarketList.mouseGrabber = nil
	end
end

m_MarketFunctions.destroySubWindow = function()
	if m_MarketList.subWindow then
		m_MarketList.subWindow:destroy()
		m_MarketList.subWindow = nil
	end
	
	m_MarketList.currentSelected = nil
end

m_MarketFunctions.removeEvent = function()
	if m_MarketList.event then
		removeEvent(m_MarketList.event)
		m_MarketList.event = nil
	end
end

m_MarketFunctions.removeUpdateEvent = function()
	if m_MarketList.updateEvent then
		removeEvent(m_MarketList.updateEvent)
		m_MarketList.updateEvent = nil
	end
end

m_MarketFunctions.removeBalanceEvent = function()
	if m_MarketList.balanceEvent then
		removeEvent(m_MarketList.balanceEvent)
		m_MarketList.balanceEvent = nil
	end
end

m_MarketFunctions.select = function(self, id)
	if m_MarketList.current then
		m_MarketList.current:setOn(false)
	end
	
	m_MarketFunctions.setPage(self, tonumber(id))
	m_MarketList.current = self
	self:setOn(true)
end

m_MarketFunctions.addEvent = function(page)
	if not m_MarketList.window or not m_MarketList.subWindow then
		m_MarketFunctions.removeEvent()
		return false
	end
	
	local list = m_MarketList.list
	if list:getChildCount() == 0 then
		m_MarketFunctions.removeEvent()
		return false
	end
	
	for _, pid in pairs(list:getChildren()) do
		local delay = pid:getChildById('delay')
		if delay then
			local timer = delay:getText():explode(':')
			local hours, minutes, seconds = tonumber(timer[1]), tonumber(timer[2]), tonumber(timer[3])
			
			seconds = seconds - 1
			if seconds < 0 then
				seconds = 59
				minutes = minutes - 1
			end
			
			if minutes < 0 then
				minutes = 59
				hours = hours - 1
			end
			
			if seconds <= 0 and minutes <= 0 and hours <= 0 then
				pid:destroy()
			else
				if hours < 10 then
					hours = '0' .. hours
				end
				
				if minutes < 10 then
					minutes = '0' .. minutes
				end
				
				if seconds < 10 then
					seconds = '0' .. seconds
				end
				
				delay:setText(hours .. ':' .. minutes .. ':' .. seconds)
			end
		end
	end
end

m_MarketFunctions.getTime = function(delay)
	local hours = math.floor(delay / 3600)
	local minutes = math.floor((delay % 3600) / 60)
	local seconds = delay % (3600 / 60)
	
	if hours < 10 then
		hours = '0' .. hours
	end
	
	if minutes < 10 then
		minutes = '0' .. minutes
	end
	
	if seconds < 10 then
		seconds = '0' .. seconds
	end
	
	return hours .. ':' .. minutes .. ':' .. seconds
end

m_MarketFunctions.addHistoryItem = function(var, separator)
	local localPlayer = g_game.getLocalPlayer()
	if not localPlayer then
		return false
	end
	
	local message = ''
	local isOwner = localPlayer:getName() == var.owner
	if separator < 0 then
		if isOwner then
			message = tr('You sold %s%s for %d%s to %s.', var.count > 1 and var.count .. ' ' or '', var.name, var.price, var.count > 1 and tr(' per unit') or '', var.customer)
		else
			message = tr('You bought %s%s for %d%s by %s.', var.count > 1 and var.count .. ' ' or '', var.name, var.price, var.count > 1 and tr(' per unit') or '', var.owner)
		end
	else
		if isOwner then
			message = tr('You bought %s%s for %d%s from %s.', var.count > 1 and var.count .. ' ' or '', var.name, var.price, var.count > 1 and tr(' per unit') or '', var.customer)
		else
			message = tr('You sold %s%s for %d%s to %s.', var.count > 1 and var.count .. ' ' or '', var.name, var.price, var.count > 1 and tr(' per unit') or '', var.owner)
		end
	end
	
	local widget = g_ui.createWidget('OfferInformation', m_MarketList.list)
	widget:setText(message)
	
	if separator == 0 then
		local widget = g_ui.createWidget('OfferInformation', m_MarketList.list)
		widget:setText('-')
	end
end

m_MarketFunctions.addItem = function(var)
	local widget = nil
	if m_MarketList.pageId == m_MarketFunctions.PAGE_SELL_OFFERS or m_MarketList.pageId == m_MarketFunctions.PAGE_BUY_OFFERS then
		widget = g_ui.createWidget('SellOfferWidget', m_MarketList.list)
		widget.count = var.count
		widget.price = var.price
		widget:getChildById('owner'):setText(var.owner)
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_YOUR_BUY_OFFERS then
		widget = g_ui.createWidget('YourBuyOfferWidget', m_MarketList.list)
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_YOUR_SELL_OFFERS then
		widget = g_ui.createWidget('YourSellOfferWidget', m_MarketList.list)
	end
	
	widget:setId(var.id)
	if var.description ~= '' then
		widget:setTooltip(var.description)
	end
	
	local item = widget:getChildById('item')
	local name = widget:getChildById('name')
	local delay = widget:getChildById('delay')
	local count = widget:getChildById('count')
	local price = widget:getChildById('price')
	
	item:setItemId(var.spriteId)
	item:getItem():setClassId(var.quality)
	item:getItem():setMarketId(var.id)
	item:setItemCount(math.min(100, var.count))
	updateItemQuality(item, var.quality)
	if var.pageCategory > 2 then
		item:getItem():setMarketPage(var.pageCategory - 2)
	else
		item:getItem():setMarketPage(var.pageCategory)
	end
	
	name:setText(var.name)
	count:setText(g_game.getMilharNumber(var.count))
	delay:setText(m_MarketFunctions.getTime(var.delay))
	price:setText(g_game.getMilharNumber(var.price))
	
	item.onMouseRelease = function(self, mousePosition, mouseButton)
		if m_MarketList.nextRelease then
			m_MarketList.nextRelease = false
			return false
		end
		
		if (g_mouse.isPressed(MouseLeftButton) and mouseButton == MouseRightButton) or (g_mouse.isPressed(MouseRightButton) and mouseButton == MouseLeftButton) then
			m_MarketList.nextRelease = true
			m_MarketFunctions.lookAtItem(var.id)
			return true
		elseif mouseButton == MouseRightButton then
			local menu = g_ui.createWidget('PopupMenu')
			menu:setGameMenu(true)
			menu:addOption(tr('Look'), function() return m_MarketFunctions.lookAtItem(var.id) end)
			menu:display(mousePosition)
			return true
		end
		
		return false
	end
end

m_MarketFunctions.updateItemList = function()
	m_MarketFunctions.removeSelect()
	m_MarketList.list:destroyChildren()
	m_MarketFunctions.removeUpdateEvent()
	m_MarketFunctions.removeBalanceEvent()
	m_MarketList.updateEvent = scheduleEvent(function() m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_SEARCH) end, 150)
end

m_MarketFunctions.onCategoryOptionChange = function(self, text, data)
	local text = m_MarketList.categoriesList[text] or text
	local id = m_MarketFunctions.categoryList[text] or ITEMTYPE_NONE
	if m_MarketList.categoryId == id then
		return false
	end
	
	m_MarketFunctions.updatePages(1)
	m_MarketList.categoryId = id
	m_MarketList.currentPage = 1
	m_MarketFunctions.updateItemList()
end

m_MarketFunctions.sortByName = function(k, v)
	return k.name[m_MarketFunctions.language] < v.name[m_MarketFunctions.language]
end

m_MarketFunctions.onQualityOptionChange = function(self, text, data)
	for _, v in pairs(m_MarketFunctions.qualityList) do
		if v[1] == text then
			updateItemQuality(m_MarketList.subWindow:getChildById('item'), v[2])
			m_MarketList.qualityId = v[2]
			break
		end
	end
end

m_MarketFunctions.onItemsOptionChange = function(self, text, data)
	for k, v in pairs(m_MarketFunctions.categoryList) do
		if tr(k) == text then
			local list = modules.game_lookat.m_LookAtFunctions.getItemList()[v]
			if not list then
				return true
			end
			
			m_MarketList.currentItem = nil
			m_MarketList.subWindow:getChildById('item'):setItemId(0)
			m_MarketList.subWindow:getChildById('sendOfferButton'):setEnabled(false)
			m_MarketList.subWindow:getChildById('cancelButton'):setEnabled(m_MarketList.currentSelected)
			m_MarketList.subWindow:getChildById('count'):setEnabled(false)
			
			local qualityComboBox = m_MarketList.subWindow:getChildById('qualityComboBox')
			if isInArray(m_MarketFunctions.disablesQuality, v) then
				qualityComboBox:setCurrentOption(tr('Normal'))
				qualityComboBox:setEnabled(false)
				m_MarketFunctions.onQualityOptionChange(nil, tr('Normal'))
			else
				local option = tr('Unique')
				if v == ITEMTYPE_CRAFTING_MATERIAL then
					qualityComboBox:removeOption(option)
				elseif not qualityComboBox:getOption(option) then
					qualityComboBox:addOption(option)
				end
				
				qualityComboBox:setEnabled(true)
			end
			
			m_MarketList.page:destroyChildren()
			table.sort(list, m_MarketFunctions.sortByName)
			
			for i = 1, #list do
				local it = list[i]
				local widget = g_ui.createWidget('ItemMarkerCategory', m_MarketList.page)
				widget:setText(it.name[m_MarketFunctions.language])
				widget.onMouseRelease = m_MarketFunctions.selectCategoryItem
				widget.category = v
				widget.id = it.clientId
				
				local item = widget:getChildById('item')
				item:setItemId(it.clientId)
				item:getItem():setCategoryId(it.itemType)
			end
			
			break
		end
	end
end

m_MarketFunctions.selectCategoryItem = function(self, mousePosition, mouseButton)
	if m_MarketList.currentItem then
		m_MarketList.currentItem:getChildById('blink'):hide()
	end
	
	m_MarketList.currentItem = self
	self:getChildById('blink'):show()
	
	m_MarketList.subWindow:getChildById('sendOfferButton'):setEnabled(true)
	m_MarketList.subWindow:getChildById('cancelButton'):setEnabled(m_MarketList.currentSelected)
	m_MarketList.subWindow:getChildById('count'):setEnabled(true)
	m_MarketList.subWindow:getChildById('item'):setItemId(self.id)
end

m_MarketFunctions.onSortOptionChange = function(self, text, data)
	for _, v in pairs(m_MarketFunctions.sortList) do
		if text == v[1] then
			if m_MarketList.sortType == v[2] then
				return true
			end
			
			m_MarketList.sortType = v[2]
			m_MarketList.currentPage = 1
			m_MarketFunctions.updateItemList()
			break
		end
	end
end

m_MarketFunctions.search = function(self)
	onTextChange(self)
	
	m_MarketList.search = self:getText()
	m_MarketList.currentPage = 1
	m_MarketFunctions.updateItemList()
end

m_MarketFunctions.updatePages = function(pages)
	if m_MarketList.pageId ~= m_MarketFunctions.PAGE_SELL_OFFERS and m_MarketList.pageId ~= m_MarketFunctions.PAGE_BUY_OFFERS then
		return true
	end
	
	if pages then
		m_MarketList.pages = pages
	end
	
	m_MarketList.page:setText(m_MarketList.currentPage .. '/' .. m_MarketList.pages)
	if m_MarketList.pages > 1 then
		m_MarketList.nextButton:setEnabled(m_MarketList.currentPage < m_MarketList.pages)
		m_MarketList.prevButton:setEnabled(m_MarketList.currentPage > 1)
	else
		m_MarketList.nextButton:setEnabled(false)
		m_MarketList.prevButton:setEnabled(false)
	end
end

m_MarketFunctions.switch = function(v)
	m_MarketList.current = nil
	
	if v then
		m_MarketFunctions.setPage(m_MarketList.buttons[4], m_MarketFunctions.PAGE_YOUR_SELL_OFFERS)
	else
		m_MarketFunctions.setPage(m_MarketList.buttons[4], m_MarketFunctions.PAGE_YOUR_BUY_OFFERS)
	end
	
	m_MarketList.current = m_MarketList.buttons[4]
end

m_MarketFunctions.setPage = function(self, page)
	if m_MarketList.current == self then
		return true
	end
	
	m_MarketList.currentItem = nil
	m_MarketList.nextButton = nil
	m_MarketList.prevButton = nil
	m_MarketList.count = nil
	m_MarketList.page = nil
	m_MarketList.qualityId = nil
	
	m_MarketList.currentPage = 1
	m_MarketList.search = ''
	m_MarketList.pageId = page
	m_MarketList.categoryId = ITEMTYPE_NONE
	m_MarketList.sortType = SORT_BY_TIME_LOWER
	m_MarketFunctions.destroySubWindow()
	m_MarketFunctions.removeBalanceEvent()
	if page == m_MarketFunctions.PAGE_BUY_OFFERS or page == m_MarketFunctions.PAGE_SELL_OFFERS then
		if page == m_MarketFunctions.PAGE_BUY_OFFERS then
			m_MarketList.subWindow = g_ui.createWidget('MarketBuyOffers', m_MarketList.window)
			m_MarketList.pageCategory = m_MarketFunctions.PAGE_BUY_OFFERS
			m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_BUY_OFFERS)
			
			m_MarketFunctions.destroyMouseGrabber()
			
			m_MarketList.mouseGrabber = g_ui.createWidget('UIWidget')
			m_MarketList.mouseGrabber:hide()
			m_MarketList.mouseGrabber:setFocusable(false)
			m_MarketList.mouseGrabber.onMouseRelease = m_MarketFunctions.onMouseRelease
		else
			m_MarketList.subWindow = g_ui.createWidget('MarketSellOffers', m_MarketList.window)
			m_MarketList.pageCategory = m_MarketFunctions.PAGE_SELL_OFFERS
			m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_SELL_OFFERS)
		end
		
		m_MarketList.list = m_MarketList.subWindow:getChildById('list')
		m_MarketList.count = m_MarketList.subWindow:getChildById('count')
		m_MarketList.page = m_MarketList.subWindow:getChildById('page')
		m_MarketList.nextButton = m_MarketList.subWindow:getChildById('nextButton')
		m_MarketList.prevButton = m_MarketList.subWindow:getChildById('prevButton')
		m_MarketList.categoriesList = {}
		m_MarketList.sortList = {}
		
		local list = {}
		for _, v in pairs(m_MarketFunctions.categories) do
			m_MarketList.categoriesList[tr(v)] = v
			table.insert(list, tr(v))
		end
		
		table.sort(list)
		
		local categoryComboBox = m_MarketList.subWindow:getChildById('categoryComboBox')
		for _, v in pairs(list) do
			categoryComboBox:addOption(v)
			if v == tr('All') then
				categoryComboBox:setCurrentOption(v)
			end
		end
		
		connect(categoryComboBox, { onOptionChange = m_MarketFunctions.onCategoryOptionChange })
		
		local sortComboBox = m_MarketList.subWindow:getChildById('sortComboBox')
		for _, v in pairs(m_MarketFunctions.sortList) do
			sortComboBox:addOption(v[1])
			if v[1] == tr('Descending by time left') then
				sortComboBox:setCurrentOption(v[1])
			end
		end
		
		connect(sortComboBox, { onOptionChange = m_MarketFunctions.onSortOptionChange })
	elseif page == m_MarketFunctions.PAGE_YOUR_SELL_OFFERS then
		m_MarketFunctions.destroyMouseGrabber()
		
		m_MarketList.subWindow = g_ui.createWidget('MarketYourBuyOffers', m_MarketList.window)
		m_MarketList.list = m_MarketList.subWindow:getChildById('list')
		m_MarketList.count = m_MarketList.subWindow:getChildById('count')
		m_MarketList.mouseGrabber = g_ui.createWidget('UIWidget')
		m_MarketList.mouseGrabber:hide()
		m_MarketList.mouseGrabber:setFocusable(false)
		m_MarketList.mouseGrabber.onMouseRelease = m_MarketFunctions.onMouseRelease
		
		m_MarketList.pageCategory = m_MarketFunctions.PAGE_SELL_OFFERS
		m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_PLAYER_SELL_OFFERS)
	elseif page == m_MarketFunctions.PAGE_YOUR_BUY_OFFERS then
		m_MarketFunctions.destroyMouseGrabber()
		
		m_MarketList.subWindow = g_ui.createWidget('MarketYourSellOffers', m_MarketList.window)
		m_MarketList.list = m_MarketList.subWindow:getChildById('list')
		m_MarketList.count = m_MarketList.subWindow:getChildById('count')
		m_MarketList.page = m_MarketList.subWindow:getChildById('items')
		m_MarketList.categoriesList = {}
		
		local list = {}
		for _, v in pairs(m_MarketFunctions.sellCategories) do
			m_MarketList.categoriesList[tr(v)] = v
			table.insert(list, tr(v))
		end
		
		table.sort(list)
		
		local categoryComboBox = m_MarketList.subWindow:getChildById('categoryComboBox')
		for _, v in pairs(list) do
			categoryComboBox:addOption(v)
		end
		
		connect(categoryComboBox, { onOptionChange = m_MarketFunctions.onItemsOptionChange })
		m_MarketFunctions.onItemsOptionChange(nil, list[1])
		
		local qualityComboBox = m_MarketList.subWindow:getChildById('qualityComboBox')
		for _, v in pairs(m_MarketFunctions.qualityList) do
			qualityComboBox:addOption(v[1])
			if v[1] == tr('Normal') then
				qualityComboBox:setCurrentOption(v[1])
			end
		end
		
		connect(qualityComboBox, { onOptionChange = m_MarketFunctions.onQualityOptionChange })
		m_MarketFunctions.onQualityOptionChange(nil, tr('Normal'))
		
		m_MarketList.pageCategory = m_MarketFunctions.PAGE_BUY_OFFERS
		m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_PLAYER_BUY_OFFERS)
	elseif page == m_MarketFunctions.PAGE_HISTORY then
		m_MarketFunctions.removeEvent()
		
		m_MarketList.subWindow = g_ui.createWidget('MarketHistory', m_MarketList.window)
		m_MarketList.list = m_MarketList.subWindow:getChildById('list')
		
		m_MarketList.pageCategory = m_MarketFunctions.UPDATE_HISTORY
		m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_HISTORY)
	end
end

m_MarketFunctions.show = function()
	if not m_MarketList.window then
		return false
	end
	
	m_MarketList.window:show()
	m_MarketList.window:focus()
end

m_MarketFunctions.onMouseRelease = function(self, mousePosition, mouseButton)
	if mouseButton == MouseLeftButton then
		local clickedWidget = modules.game_interface.getRootPanel():recursiveGetChildByPos(mousePosition, false)
		if clickedWidget then
			if clickedWidget:getClassName() == 'UIGameMap' then
				local tile = clickedWidget:getTile(mousePosition)
				if tile then
					local thing = tile:getTopMoveThing()
					if thing and thing:isItem() then
						m_MarketFunctions.selectItem(thing, false, true)
					end
				end
			elseif clickedWidget:getClassName() == 'UIItem' and not clickedWidget:isVirtual() then
				m_MarketFunctions.selectItem(clickedWidget:getItem(), false, true)
			end
		end
	end
	
	m_MarketFunctions.show()
	g_mouse.popCursor('target')
	self:ungrabMouse()
	return true
end

m_MarketFunctions.cancelItem = function()
	if not m_MarketList.currentSelected then
		return false
	end
	
	m_MarketList.subWindow:getChildById('cancelButton'):setEnabled(false)
	m_MarketFunctions.removeOffer(tonumber(m_MarketList.currentSelected:getId()))
end

m_MarketFunctions.selectItem = function(self, clear, grab)
	if not grab and m_MarketList.currentSelected then
		m_MarketList.currentSelected:getChildById('blink'):hide()
		m_MarketList.currentSelected = nil
	end
	
	m_MarketFunctions.removeBalanceEvent()
	if m_MarketList.pageId == m_MarketFunctions.PAGE_BUY_OFFERS then
		m_MarketList.subWindow:getChildById('selectButton'):setEnabled(true)
		if self and grab then
			local item = m_MarketList.currentSelected:getChildById('item')
			local fromId = item:getItem():getId()
			local toId = self:getId()
			
			local fromQuality = item:getItem():getClassId()
			local toQuality = self:getClassId()
			if toQuality == ITEMCLASS_IGNORE then
				toQuality = ITEMCLASS_NONE
			end
			
			if fromId == toId and fromQuality == toQuality then
				local widget = m_MarketList.subWindow:getChildById('item')
				widget:setItemId(fromId)
				widget:setItemCount(math.min(100, self:getCount()))
				widget:getItem():setClassId(toQuality)
				updateItemQuality(widget, toQuality)
				
				m_MarketList.count:setMaximum(self:getCount())
				m_MarketList.count:setValue(self:getCount())
				m_MarketList.count:setEnabled(m_MarketList.count:getMaximum() > 1)
				m_MarketList.subWindow:getChildById('sellButton'):setEnabled(true)
				m_MarketList.currentItem = self
			end
		else
			m_MarketFunctions.resetItem()
		end
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_SELL_OFFERS then
		m_MarketList.subWindow:getChildById('buyButton'):setEnabled(clear)
		if clear then
			m_MarketList.currentItem = self
		else
			m_MarketList.currentItem = nil
		end
		
		m_MarketList.count:setMaximum(self.count)
		m_MarketList.count:setValue(self.count)
		m_MarketList.count:setEnabled(m_MarketList.count:getMaximum() > 1)
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_YOUR_SELL_OFFERS then
		m_MarketList.subWindow:getChildById('cancelButton'):setEnabled(self and clear)
		m_MarketList.subWindow:getChildById('sendOfferButton'):setEnabled(not clear)
		
		m_MarketList.subWindow:getChildById('price'):clearText()
		m_MarketList.subWindow:getChildById('description'):clearText()
		
		local item = m_MarketList.subWindow:getChildById('item')
		if clear then
			item:setItemId(0)
			m_MarketList.count:setMaximum(1)
			m_MarketList.count:setValue(1)
			updateItemQuality(item, ITEMCLASS_NONE)
			m_MarketList.currentItem = nil
		elseif self then
			item:setItemId(self:getId())
			m_MarketList.count:setMaximum(self:getCount())
			m_MarketList.count:setValue(self:getCount())
			updateItemQuality(item, self:getClassId())
			m_MarketList.currentItem = self
		end
		
		m_MarketList.count:setEnabled(m_MarketList.count:getMaximum() > 1)
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_YOUR_BUY_OFFERS then
		m_MarketList.subWindow:getChildById('cancelButton'):setEnabled(self and clear)
	end
	
	if clear and self then
		m_MarketList.currentSelected = self
		self:getChildById('blink'):show()
	end
end

m_MarketFunctions.grabItem = function()
	if g_ui.isMouseGrabbed() then 
		return false
	end
	
	if m_MarketList.currentSelected and m_MarketList.pageId ~= m_MarketFunctions.PAGE_BUY_OFFERS then
		m_MarketList.currentSelected:getChildById('blink'):hide()
		m_MarketList.currentSelected = nil
	end
	
	m_MarketList.mouseGrabber:grabMouse()
	g_mouse.pushCursor('target')
	m_MarketList.window:hide()
end

m_MarketFunctions.flashBalance = function()
	if m_MarketList.balance then
		m_MarketList.balance.count = 0
		m_MarketFunctions.removeBalanceEvent()
		m_MarketList.balance:setOn(true)
		m_MarketList.balanceEvent = cycleEvent(function()
			if not m_MarketList.balance then
				m_MarketFunctions.removeBalanceEvent()
			else
				m_MarketList.balance.count = (m_MarketList.balance.count or 0) + 1
				if m_MarketList.balance.count < 10 then
					m_MarketList.balance:setOn(not m_MarketList.balance:isOn())
				end
			end
		end, 500)
	end
end

m_MarketFunctions.buyItem = function()
	if not m_MarketList.currentItem then
		return false
	end
	
	local count = m_MarketList.count:getValue()
	if m_MarketList.currentItem.price * count > (m_MarketFunctions.balance or 0) then
		m_MarketFunctions.flashBalance()
		return false
	end
	
	m_MarketFunctions.buyOffer(tonumber(m_MarketList.currentItem:getId()), count)
	m_MarketFunctions.removeSelect()
end

m_MarketFunctions.removeSelect = function()
	if m_MarketList.currentItem then
		m_MarketList.currentItem:getChildById('blink'):hide()
		m_MarketList.currentItem = nil
	end
	
	m_MarketList.currentSelected = nil
	m_MarketList.count:setValue(1)
	m_MarketList.count:setEnabled(false)
	
	if m_MarketList.pageId == m_MarketFunctions.PAGE_SELL_OFFERS then
		m_MarketList.subWindow:getChildById('buyButton'):setEnabled(false)
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_YOUR_BUY_OFFERS then
		m_MarketList.subWindow:getChildById('cancelButton'):setEnabled(m_MarketList.currentSelected)
		m_MarketList.subWindow:getChildById('sendOfferButton'):setEnabled(false)
	elseif m_MarketList.pageId == m_MarketFunctions.PAGE_BUY_OFFERS then
		m_MarketList.subWindow:getChildById('sellButton'):setEnabled(false)
		m_MarketList.subWindow:getChildById('selectButton'):setEnabled(false)
	end
end

m_MarketFunctions.prevPage = function()
	if m_MarketList.currentPage <= 1 then
		m_MarketList.currentPage = 1
	else
		m_MarketList.currentPage = m_MarketList.currentPage - 1
	end
	
	m_MarketFunctions.updatePages()
	m_MarketFunctions.updateItemList()
end

m_MarketFunctions.nextPage = function()
	if m_MarketList.currentPage >= m_MarketList.pages then
		m_MarketList.currentPage = m_MarketList.pages
	else
		m_MarketList.currentPage = m_MarketList.currentPage + 1
	end
	
	m_MarketFunctions.updatePages()
	m_MarketFunctions.updateItemList()
end

m_MarketFunctions.updateCorners = function(value)
	if not m_MarketList.subWindow then
		return true
	end
	
	for _, pid in pairs(m_MarketList.list:getChildren()) do
		local widget = pid:getChildById('item')
		local corner = widget:getChildById('corner')
		if value and widget:getItem():getClassId() ~= ITEMCLASS_NONE then
			corner:show()
		else
			corner:hide()
		end
	end
end

m_MarketFunctions.updateFrames = function(value)
	if not m_MarketList.subWindow then
		return true
	end
	
	for _, pid in pairs(m_MarketList.list:getChildren()) do
		local widget = pid:getChildById('item')
		if value then
			widget:setImageClip(getFrameClipByClassId(widget:getItem():getClassId()))
		else
			widget:setImageClip(getFrameClipByClassId(ITEMCLASS_NONE))
		end
	end
end

m_MarketFunctions.getOfferById = function(id, mode)
	if not m_MarketList.subWindow then
		return false
	end
	
	if mode and (m_MarketList.pageId == m_MarketFunctions.PAGE_SELL_OFFERS or m_MarketList.pageId == m_MarketFunctions.PAGE_YOUR_SELL_OFFERS) then
		return false
	end
	
	return m_MarketList.list:getChildById(id)
end

-- protocol
m_MarketFunctions.checkProtocol = function()
	if not m_MarketFunctions.protocol then
		m_MarketFunctions.protocol = g_game.getProtocolGame()
	end
	
	return OutputMessage.create()
end

m_MarketFunctions.sendRequest = function()
	if not m_MarketList.currentItem or not m_MarketList.qualityId then
		return false
	end
	
	local price = tonumber(m_MarketList.subWindow:getChildById('price'):getText())
	if not price or price < 1 then
		return false
	end
	
	local count = m_MarketList.count:getValue()
	if price * count > (m_MarketFunctions.balance or 0) then
		m_MarketFunctions.flashBalance()
		return false
	end
	
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(1)
	msg:addU16(m_MarketList.currentItem.id)
	msg:addU16(count)
	msg:addU64(price)
	msg:addU8(m_MarketList.qualityId)
	msg:addString(m_MarketList.subWindow:getChildById('description'):getText())
	m_MarketFunctions.protocol:send(msg)
end

m_MarketFunctions.send = function()
	if not m_MarketList.currentItem then
		return false
	end
	
	local price = tonumber(m_MarketList.subWindow:getChildById('price'):getText())
	if not price or price < 1 then
		return false
	end
	
	local position = m_MarketList.currentItem:getPosition()
	local id = m_MarketList.currentItem:getId()
	local stackpos = m_MarketList.currentItem:getStackPos()
	
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(2)
	m_MarketFunctions.protocol:addPosition(msg, position)
	msg:addU16(id)
	msg:addU8(stackpos)
	msg:addU16(m_MarketList.subWindow:getChildById('count'):getValue())
	msg:addU64(price)
	msg:addString(m_MarketList.subWindow:getChildById('description'):getText())
	m_MarketFunctions.protocol:send(msg)
	
	m_MarketFunctions.selectItem(false, true)
end

m_MarketFunctions.updateMarket = function(id)
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(3)
	msg:addU8(id)
	msg:addU16(m_MarketList.currentPage)
	msg:addU8(m_MarketList.categoryId)
	msg:addU8(m_MarketList.sortType)
	msg:addString(m_MarketList.search:lower())
	msg:addU8(m_MarketList.pageCategory)
	m_MarketFunctions.protocol:send(msg)
end

m_MarketFunctions.removeOffer = function(id)
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(4)
	msg:addU32(id)
	msg:addU8(m_MarketList.pageCategory)
	m_MarketFunctions.protocol:send(msg)
end

m_MarketFunctions.buyOffer = function(id, count)
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(5)
	msg:addU32(id)
	msg:addU16(count)
	msg:addU8(m_MarketList.pageCategory)
	m_MarketFunctions.protocol:send(msg)
end

m_MarketFunctions.lookAtItem = function(id)
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(6)
	msg:addU32(id)
	msg:addU8(m_MarketList.pageCategory)
	m_MarketFunctions.protocol:send(msg)
end

m_MarketFunctions.resetItem = function()
	local widget = m_MarketList.subWindow:getChildById('item')
	widget:setItemId(0)
	updateItemQuality(widget, ITEMCLASS_NONE)
	
	m_MarketList.count:setMaximum(1)
	m_MarketList.count:setValue(1)
	m_MarketList.count:setEnabled(false)
	m_MarketList.subWindow:getChildById('sellButton'):setEnabled(false)
	m_MarketList.currentItem = nil
end

m_MarketFunctions.sellItem = function()
	if not m_MarketList.currentItem or not m_MarketList.subWindow then
		return false
	end
	
	local position = m_MarketList.currentItem:getPosition()
	local id = tonumber(m_MarketList.currentItem:getId())
	local stackpos = m_MarketList.currentItem:getStackPos()
	
	local msg = m_MarketFunctions.checkProtocol()
	msg:addU8(m_MarketFunctions.GameServerManageMarket)
	msg:addU8(7)
	msg:addU32(tonumber(m_MarketList.currentSelected:getId()))
	m_MarketFunctions.protocol:addPosition(msg, position)
	msg:addU16(id)
	msg:addU8(stackpos)
	msg:addU16(m_MarketList.count:getValue())
	m_MarketFunctions.protocol:send(msg)
	m_MarketFunctions.resetItem()
end

m_MarketFunctions.parseManageMarket = function(protocol, msg)
	local id = msg:getU8()
	if id == 1 then -- market balance
		m_MarketFunctions.updateBalance(msg:getU64())
	elseif id == 2 then -- remove offer
		local offerId = msg:getU16()
		local mode = msg:getU8() == 1
		local widget = m_MarketFunctions.getOfferById(offerId, mode)
		if widget then
			if m_MarketList.currentSelected == widget then
				m_MarketList.currentSelected = nil
			end
			
			widget:destroy()
			
			if m_MarketList.pageId == m_MarketFunctions.PAGE_SELL_OFFERS or m_MarketList.pageId == m_MarketFunctions.PAGE_BUY_OFFERS then
				m_MarketList.list:destroyChildren()
				m_MarketFunctions.removeUpdateEvent()
				m_MarketList.updateEvent = scheduleEvent(function() m_MarketFunctions.updateMarket(m_MarketFunctions.UPDATE_SEARCH) end, 150)
			end
		end
	elseif id == 3 then -- update offer
		local offerId = msg:getU32()
		local count = msg:getU16()
		local mode = msg:getU8() == 1
		local widget = m_MarketFunctions.getOfferById(offerId, mode)
		if widget then
			widget:getChildById('item'):setItemCount(math.min(100, count))
			widget:getChildById('count'):setText(count)
		end
	elseif id == 4 then -- update item list
		local size = msg:getU8()
		if size == 0 then
			return true
		end
		
		local var = msg:getU8()
		if var == m_MarketFunctions.UPDATE_HISTORY then
			local separator = msg:getU16()
			for i = 1, size do
				local offerId = msg:getU32()
				local count = msg:getU16()
				local price = msg:getU64()
				local name = msg:getString()
				local owner = msg:getString()
				local customer = msg:getString()
				
				separator = separator - 1
				m_MarketFunctions.addHistoryItem({ id = offerId, name = name, count = count, price = price, owner = owner, customer = customer }, separator)
			end
		else
			if not m_MarketFunctions.protocol then
				m_MarketFunctions.protocol = g_game.getProtocolGame()
			end
			
			local localPlayer = g_game.getLocalPlayer()
			local pages = msg:getU16()
			for i = 1, size do
				local offerId = msg:getU32()
				local spriteId = msg:getU16()
				local count = msg:getU16()
				local quality = msg:getU8()
				local name = msg:getString()
				local delay = msg:getU32()
				local price = msg:getU64()
				local description = msg:getString()
				local owner = msg:getString()
				local customer = msg:getString()
				local pageCategory = msg:getU8()
				if m_MarketList.subWindow and (m_MarketList.pageCategory == pageCategory or m_MarketList.pageCategory + 2 == pageCategory) then
					m_MarketFunctions.addItem({ id = offerId, spriteId = spriteId, quality = quality, name = name, delay = delay, count = count, price = price, description = description, owner = owner, customer = customer, pageCategory = pageCategory })
				end
			end
			
			if m_MarketList.subWindow then
				m_MarketFunctions.removeEvent()
				m_MarketFunctions.updatePages(pages)
				m_MarketList.event = cycleEvent(m_MarketFunctions.addEvent, 1000)
			end
		end
	end
end