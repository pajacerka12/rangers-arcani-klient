m_BossHealthBarFunctions = {}
m_BossHealthBarList = {}

m_BossHealthBarFunctions.width = 289

m_BossHealthBarFunctions.config = {
	[1] = {clip = '2487 590 106 100', size = {width = 106, height = 100}, color = '2644 1023 289 17'},
	[2] = {clip = '2609 600 82 82', size = {width = 82, height = 82}, color = '2644 1050 289 17'},
	[3] = {clip = '2714 603 75 77', size = {width = 75, height = 77}, color = '2644 1081 289 17'},
	[4] = {clip = '2806 604 84 83', size = {width = 84, height = 83}, color = '2644 1109 289 17'},
	[5] = {clip = '2897 600 83 87', size = {width = 83, height = 87}, color = '2644 1141 289 17'},
	[6] = {clip = '2505 477 70 95', size = {width = 70, height = 95}, color = '2644 1171 289 17'},
	[7] = {clip = '2613 489 71 75', size = {width = 71, height = 75}, color = '2644 1201 289 17'},
	[8] = {clip = '2712 483 70 83', size = {width = 70, height = 83}, color = '2644 1231 289 17'},
	[9] = {clip = '2808 489 70 76', size = {width = 70, height = 76}, color = '2644 1260 289 17'},
	[10] = {clip = '2902 487 78 77', size = {width = 78, height = 77}, color = '2644 1294 289 17'},
	[11] = {clip = '2366 482 99 93', size = {width = 99, height = 93}, color = '2648 1384 289 17'},
	[12] = {clip = '2251 477 104 102', size = {width = 104, height = 102}, color = '2648 1469 289 17'},
	[13] = {clip = '2143 478 101 99', size = {width = 101, height = 99}, color = '2648 1441 289 17'},
	[14] = {clip = '2044 480 81 97', size = {width = 81, height = 97}, color = '2648 1413 289 17'},
	[15] = {clip = '1944 469 85 116', size = {width = 85, height = 116}, color = '2647 1326 289 17'},
	[16] = {clip = '1934 598 103 101', size = {width = 103, height = 101}, color = '2648 1355 289 17'},
}

function onRemoveBossHealthBar()
	m_BossHealthBarFunctions.destroy()
end

function onAddBossHealthBar(name, type, health, healthMax, color)
	m_BossHealthBarFunctions.onAddBossHealthBar(name, type, health, healthMax, color)
end

function onGameEnd()
	m_BossHealthBarFunctions.destroy()
end

function onLoad()
	connect(g_game, {
					onGameEnd = onGameEnd,
					onRemoveBossHealthBar = onRemoveBossHealthBar,
					onAddBossHealthBar = onAddBossHealthBar
					})
end

function onUnload()
	disconnect(g_game, {
					onGameEnd = onGameEnd,
					onRemoveBossHealthBar = onRemoveBossHealthBar,
					onAddBossHealthBar = onAddBossHealthBar
					})
	
	m_BossHealthBarFunctions.destroy()
end

m_BossHealthBarFunctions.destroy = function()
	if m_BossHealthBarList.window then
		m_BossHealthBarList.window:destroy()
		m_BossHealthBarList = {}
		
		modules.game_textmessage.updatePosition(false)
	end
end

m_BossHealthBarFunctions.updateHealth = function(v)
	if m_BossHealthBarList.healthStatus[1] == 0 then
		m_BossHealthBarFunctions.destroy()
		return true
	end
	
	local value = math.ceil(m_BossHealthBarFunctions.width * (m_BossHealthBarList.healthStatus[1] / m_BossHealthBarList.healthStatus[2]))
	m_BossHealthBarList.health:setWidth(value)
	
	local value = math.ceil((m_BossHealthBarList.healthStatus[1] / m_BossHealthBarList.healthStatus[2]) * 100)
	m_BossHealthBarList.label:setText(value .. '%')
end

m_BossHealthBarFunctions.onAddBossHealthBar = function(name, type, health, healthMax, color)
	if m_BossHealthBarList.health and m_BossHealthBarList.healthStatus[1] == health then
		return false
	end
	
	if not m_BossHealthBarList.window then
		m_BossHealthBarList.window = g_ui.loadUI('game_bosshealth', modules.game_interface.getRootPanel())
		m_BossHealthBarList.health = m_BossHealthBarList.window:getChildById('health')
		m_BossHealthBarList.label = m_BossHealthBarList.window:getChildById('label')
		m_BossHealthBarList.name = m_BossHealthBarList.window:getChildById('name')
		m_BossHealthBarList.avatar = m_BossHealthBarList.window:getChildById('avatar')
		
		modules.game_textmessage.updatePosition(true)
	end
	
	local v = m_BossHealthBarFunctions.config[type]
	if m_BossHealthBarList.type ~= type then
		if not v then
			v = m_BossHealthBarFunctions.config[1]
		end
		
		m_BossHealthBarList.avatar:setSize(v.size)
		m_BossHealthBarList.avatar:setImageClip(v.clip)
		m_BossHealthBarList.health:setImageClip(v.color)
		m_BossHealthBarList.type = type
	end
	
	m_BossHealthBarList.healthStatus = {health, healthMax}
	m_BossHealthBarFunctions.updateHealth(v)
	
	if m_BossHealthBarList.name and m_BossHealthBarList.name:getText() ~= name then
		m_BossHealthBarList.name:setText(name)
	end
end

m_BossHealthBarFunctions.getColor = function(color)
	if color == 0 then
		return '00'
	end

    local B, K, OUT, I, D = 16, '0123456789ABCDEF', '', 0
    while color > 0 do
        I = I + 1
        color, D = math.floor(color / B),math.mod(color, B) + 1
        OUT = string.sub(K, D, D) .. OUT
    end
	
	if OUT:len() == 1 then
		return '0' .. OUT
	end
	
    return tostring(OUT)
end