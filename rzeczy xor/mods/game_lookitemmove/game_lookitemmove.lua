m_LookItemMoveList = {}
m_LookItemMoveFunction = {}

m_LookItemMoveFunction.attributes = {
	{'hand', 18},
	{'damage', 4},
	{'armor', 2},
	{'defense', 3},
	{'runeWord', 7},
	{'strength', 19},
	{'dexterity', 9},
	{'condition', 27},
	{'wisdom', 17},
	{'vigor', 49}
}

m_LookItemMoveFunction.send = function(position, item, classType, marketId, marketPage)
	g_game.sendLookItemMove(item, classType, marketId, marketPage)
end

m_LookItemMoveFunction.cancelEvent = function()
	m_LookItemMoveList.itemId = false
	m_LookItemMoveList.position = false
	
	if m_LookItemMoveFunction.event then
		m_LookItemMoveFunction.event:cancel()
		m_LookItemMoveFunction.event = nil
	end
end

m_LookItemMoveFunction.createWindow = function(self, position, item, classType, marketId, marketPage, virtual)
	if not m_LookItemMoveFunction.visible then
		return true
	end
	
	m_LookItemMoveFunction.cancelEvent()
	m_LookItemMoveList.itemId = item:getId()
	m_LookItemMoveList.position = position
	
	if virtual then
		m_LookItemMoveFunction.event = scheduleEvent(function()
			local list = {}
			list[ITEM_CLASS_ITEM] = {value = getClassIdByName(classType)}
			if item:isStackable() then
				list[ITEM_COUNT] = {value = item:getCount()}
			elseif item:isFluidContainer() then
				list[ITEM_COUNT] = {value = self.subType or item:getCount()}
			end
			
			m_LookItemMoveFunction.open(modules.game_lookat.m_LookAtFunctions.getItemByClientId(item:getId()), list)
		end, 200)
	else
		m_LookItemMoveFunction.event = scheduleEvent(function()
			m_LookItemMoveFunction.send(position, item, classType, marketId, marketPage)
		end, 200)
	end
end

m_LookItemMoveFunction.destroyWindowByItem = function(itemId)
	if itemId and m_LookItemMoveList.itemId == itemId then
		m_LookItemMoveFunction.destroyWindow()
	end
end

m_LookItemMoveFunction.destroyWindow = function()
	m_LookItemMoveFunction.cancelEvent()
	
	if m_LookItemMoveList.window then
		m_LookItemMoveList.list:destroyChildren()
		
		m_LookItemMoveList.window:destroy()
		m_LookItemMoveList = {}
	end
end

m_LookItemMoveFunction.setWindowVisible = function(enable)
	m_LookItemMoveFunction.visible = enable
end

m_LookItemMoveFunction.addLabel = function(name, height, width, description, color, update)
	local widget = g_ui.createWidget(name)
	if update then
		widget:setParent(m_LookItemMoveList.list)
	end
	
	if description then
		widget:setText(description)
		
		width = math.max(widget:getWidth() + 16, width)
	end
	
	if color then
		widget:setColor(color)
	end
	
	if not update then
		widget:setParent(m_LookItemMoveList.list)
	end
	
	return height + widget:getHeight(), width
end

m_LookItemMoveFunction.open = function(it, list)
	if not it or not m_LookItemMoveList.position then
		return true
	end
	
	if not it.attributes then
		return true
	end
	
	if not m_LookItemMoveList.window then
		m_LookItemMoveList.window = g_ui.displayUI('game_lookitemmove')
		m_LookItemMoveList.list = m_LookItemMoveList.window:getChildById('list')
	else
		m_LookItemMoveList.list:destroyChildren()
	end
	
	m_LookItemMoveList.window:show()
	
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		language = LANGUAGE_ENGLISH
	else
		language = LANGUAGE_POLISH
	end
	
	local classId = ITEMCLASS_NORMAL
	if it.attributes.shopitem then
		classId = ITEMCLASS_SHOP
	elseif it.attributes.questitem then
		classId = ITEMCLASS_QUEST
	else
		local var = list[ITEM_CLASS_ITEM]
		if var then
			classId = var.value
		end
	end
	
	local isLegendary = classId == ITEMCLASS_LEGENDARY and it.attributes.legendary
	local text = it.name[language]
	local var = list[ITEM_NAME]
	if var then
		local value = {var.value, list[ITEM_DESCRIPTION].value}
		text = value[language]
	else
		if isLegendary and it.attributes.legendary.name and #it.attributes.legendary.name[1] > 0 then
			text = it.attributes.legendary.name[language] or it.attributes.legendary.name[1]
			
			if it.attributes.legendary.inscription then
				text = text:format(g_game.getLocalPlayer():getName())
			end
		end
	end
	
	local height = 16
	local width = 0
	if it.attributes.runeword then
		text = tr('Rune word: ') .. it.attributes.runeword .. '\n' .. text
	elseif it.attributes.exceptionalword then
		local className = modules.game_lookat.m_LookAtFunctions.getClassName(classId, language)
		if className then
			text = className .. (language == LANGUAGE_POLISH and it.attributes.exceptionalword or '') .. ' ' .. text
		end
	end
	
	local var = list[ITEM_COUNT]
	if var then
		if it.attributes.container then
			text = text .. ' (' .. tr(FLUID_NAME[var.value]) .. ')'
		else
			text = text .. (var.value > 1 and ' (' .. var.value .. ')' or '')
		end
	end
	
	height, width = m_LookItemMoveFunction.addLabel('LookItemName', height, width, text, ITEM_NAME_COLOR[classId == ITEMCLASS_NONE and ITEMCLASS_DAMAGED or classId])
	
	local text = it.attributes.description
	local separator = false
	if text then
		height, width = m_LookItemMoveFunction.addLabel('LookItemAttribute', height, width, text[language], '#6FE300', true)
		separator = true
	end
	
	local attributes = {}
	local damageFactor = modules.game_lookat.m_LookAtFunctions.getDamageFactor(classId, it.attributes.runeword)
	if it.attributes.armor or (isLegendary and it.attributes.legendary.armor) then
		local value = ''
		local var = it.attributes.armor
		if var then
			value = modules.game_lookat.m_LookAtFunctions.getAttributeValue(var, damageFactor)
		end
		
		if isLegendary and it.attributes.legendary.armor then
			if var then
				value = value .. ' + ' .. it.attributes.legendary.armor
			else
				value = it.attributes.legendary.armor
			end
		end
		
		table.insert(attributes, {value, ITEM_ARMOR})
	end
	
	if it.attributes.defense then
		local value = ''
		local var = it.attributes.defense
		if var then
			value = modules.game_lookat.m_LookAtFunctions.getAttributeValue(var, damageFactor)
		end
		
		if isLegendary and it.attributes.legendary.defense then
			if var then
				value = value .. ' + ' .. it.attributes.legendary.defense
			else
				value = it.attributes.legendary.defense
			end
		end
		
		table.insert(attributes, {value, ITEM_DEFENSE})
	end
	
	if it.attributes.weapontype then
		table.insert(attributes, {false, ITEM_WEAPON_TYPE, modules.game_lookat.m_LookAtFunctions.getWeaponType(it.attributes)})
	end
	
	if it.attributes.maxdamage then
		if it.attributes.mindamage then
			table.insert(attributes, {modules.game_lookat.m_LookAtFunctions.getAttributeValue(it.attributes.mindamage, damageFactor) .. ' - ' .. modules.game_lookat.m_LookAtFunctions.getAttributeValue(it.attributes.maxdamage, damageFactor), ITEM_MIN_DAMAGE})
		else
			table.insert(attributes, {'+' .. modules.game_lookat.m_LookAtFunctions.getAttributeValue(it.attributes.maxdamage, damageFactor), ITEM_DISTANCE_DAMAGE})
		end
	end
	
	if it.attributes.stolenlife or (isLegendary and it.attributes.legendary.stolenlife) then
		local var = it.attributes.stolenlife or 0
		if isLegendary and it.attributes.legendary.stolenlife then
			var = var + it.attributes.legendary.stolenlife
		end
		
		table.insert(attributes, {modules.game_lookat.m_LookAtFunctions.getAttributeValue(var * 10, damageFactor) / 10 .. '%', ITEM_COUNT_STOLEN_LIFE})
	end
	
	local var = list[ITEM_SKILL]
	if var then
		for i = 1, #var do
			local v = var[i]
			if v.value > 0xFFFF then
				v.value = 0xFFFF - v.value
			end
			
			table.insert(attributes, {(v.value > 0 and '+' or '') .. v.value, ITEM_SKILL, v.type + 1})
		end
		
		if it.attributes.slots then
			local var = list[ITEM_SLOTS]
			if var then
				if var.value > 0xFFFF then
					var.value = 0xFFFF - var.value
				end
				
				table.insert(attributes, {var.value, ITEM_SLOTS})
			end
		end
	else
		local amount = 0
		if it.attributes.skill or (isLegendary and it.attributes.legendary.skill) then
			local list = {}
			if it.attributes.skill then
				for i = 1, #it.attributes.skill do
					local id = modules.game_lookat.m_LookAtFunctions.getSkillId(it.attributes.skill[i].type)
					if id ~= SKILL_NONE then
						list[id] = it.attributes.skill[i].value
					end
				end
			end
			
			if isLegendary and it.attributes.legendary.skill then
				for i = 1, #it.attributes.legendary.skill do
					local id = modules.game_lookat.m_LookAtFunctions.getSkillId(it.attributes.legendary.skill[i].type)
					if id ~= SKILL_NONE then
						list[id] = (list[id] or 0) + it.attributes.legendary.skill[i].value
					end
				end
			end
			
			for k, v in pairs(list) do
				if v ~= 0 then
					local count = modules.game_lookat.m_LookAtFunctions.getAttributeValue(v, damageFactor)
					amount = amount + count
					table.insert(attributes, {(v > 0 and '+' or '') .. count, ITEM_SKILL, k + 1})
				end
			end
		end
		
		if it.attributes.slots then
			table.insert(attributes, {modules.game_lookat.m_LookAtFunctions.getAttributeValue(it.attributes.slots * it.attributes.slotsefficiency, damageFactor) + amount, ITEM_SLOTS})
		end
	end
	
	if it.attributes.containersize then
		local size = 0
		if classId == ITEMCLASS_LEGENDARY then
			size = 4
		elseif classId == ITEMCLASS_UNIQUE then
			size = 8
		end
		
		table.insert(attributes, {it.attributes.containersize + size, ITEM_CONTAINER})
	end
	
	local begin = true
	if separator and #attributes > 0 then
		height = m_LookItemMoveFunction.addLabel('AtlasNoAnchorSeparator', height, width)
		begin = false
	end
	
	for i = 1, #attributes do
		if begin and attributes[i][2] == ITEM_SKILL then
			height = m_LookItemMoveFunction.addLabel('AtlasNoAnchorSeparator', height, width)
			begin = false
		end
		
		local tmpHeight, tmpWidth = modules.game_lookat.m_LookAtFunctions.createLabel(attributes[i][1], m_LookItemMoveList.list, attributes[i][2], attributes[i][3])
		height = height + tmpHeight
		width = math.max(width, tmpWidth)
	end
	
	m_LookItemMoveList.window:setWidth(width)
	if text then
		height = height + 4
	end
	
	m_LookItemMoveList.window:setHeight(height)
	
	local pos = m_LookItemMoveList.window:getPosition()
	pos.x = m_LookItemMoveList.position.x + pos.x + 27
	pos.y = m_LookItemMoveList.position.y + pos.y + 27
	
	local rootWidget = modules.game_interface.getRootPanel()
	if pos.x + m_LookItemMoveList.window:getWidth() >= rootWidget:getWidth() then
		pos.x = pos.x - m_LookItemMoveList.window:getWidth()
	end
	
	if pos.y + m_LookItemMoveList.window:getHeight() >= rootWidget:getHeight() then
		pos.y = pos.y - m_LookItemMoveList.window:getHeight()
	end
	
	m_LookItemMoveList.window:setPosition(pos)
end

function onGameEnd()
	m_LookItemMoveFunction.destroyWindow()
end

function onLoad()
	connect(g_game, {
		onGameEnd = onGameEnd
	})
end

function onUnLoad()
	disconnect(g_game, {
		onGameEnd = onGameEnd
	})

	m_LookItemMoveFunction.destroyWindow()
end
