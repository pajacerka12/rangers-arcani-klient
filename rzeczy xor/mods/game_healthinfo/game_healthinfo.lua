m_HealthInfoFunctions = {}
m_HealthInfoList = {}

m_HealthInfoFunctions.icons = {
	{path = '/images/leaf/states/earth'},
	{path = '/images/leaf/states/fire'},
	{path = '/images/leaf/states/energy'},
	{path = '/images/leaf/states/drunk'},
	{path = '/images/leaf/states/barrer'},
	{path = '/images/leaf/states/paralyze'},
	{path = '/images/leaf/states/haste'},
	{path = '/images/leaf/states/fight'},
	{path = '/images/leaf/states/drown'},
	{path = '/images/leaf/states/ice'},
	{path = '/images/leaf/states/holy'},
	{path = '/images/leaf/states/death'},
	{path = '/images/leaf/states/protection'},
	{path = '/images/leaf/states/protection'},
	{path = '/images/leaf/states/protection'},
	{path = '/images/leaf/states/bleed'},
	{path = '/images/leaf/states/skull'},
	{path = '/images/leaf/states/potion'}
}

m_HealthInfoFunctions.TYPE_POSITIVE = 1
m_HealthInfoFunctions.TYPE_NEGATIVE = 2

m_HealthInfoFunctions.checkCooldown = function()
	if not m_HealthInfoList.list or #m_HealthInfoList.list == 0 then
		if m_HealthInfoList.event then
			m_HealthInfoList.event:cancel()
			m_HealthInfoList.event = nil
		end
		
		m_HealthInfoList.list = {}
		return true
	end
	
	for i = #m_HealthInfoList.list, 1, -1 do
		local id, panel = unpack(m_HealthInfoList.list[i])
		local content, icon = m_HealthInfoFunctions.getIcon(id, m_HealthInfoFunctions.TYPE_POSITIVE, panel)
		
		if not icon then
			content, icon = m_HealthInfoFunctions.getIcon(id, m_HealthInfoFunctions.TYPE_NEGATIVE, panel)
		end
		
		if icon and content then
			if icon.cooldown and icon.cooldown > 0 then
				local percent = 100 - (icon.cooldown / icon.toCooldown * 100)
				
				local progressBar = icon:getChildById('progress')
				progressBar:setPercent(percent)
				
				local minutes = 0
				local seconds = (icon.cooldown / 10)
				while (seconds >= 60) do
					minutes = minutes + 1
					seconds = seconds - 60
				end
				
				local description = ''
				if minutes > 0 then
					description = minutes .. ' minute' .. (minutes > 1 and 's.' or '.')
				elseif seconds >= 0 then
					description = seconds .. ' second' .. (seconds > 1 and 's.' or '.')
				end
				
				icon.timeLeft = description
				icon.cooldown = icon.cooldown - 1
				
				if m_HealthInfoList.descriptionWindow and m_HealthInfoList.descriptionWindow.id == id then
					local list = m_HealthInfoList.descriptionWindow:getChildById('list')
					if list then
						list:getChildByIndex(#list:getChildren()):setText(description)
					end
				end
			else
				if m_HealthInfoList.descriptionWindow and m_HealthInfoList.descriptionWindow.id == id then
					m_HealthInfoList.descriptionWindow:destroy()
					m_HealthInfoList.descriptionWindow = nil
				end
				
				if id == 'passive_infight' and modules.game_sounds then
					modules.game_sounds.play(SOUNDS_GAME)
				elseif id == 'icon_reverse_walking' then
					modules.game_interface.setReverseMoving(false)
				elseif id == 'icon_block_diagonals' then
					modules.game_interface.setBlockingDiagonal(false)
				end
				
				icon:destroy()
				table.remove(m_HealthInfoList.list, i)
			end
		else
			if m_HealthInfoList.descriptionWindow and m_HealthInfoList.descriptionWindow.id == id then
				m_HealthInfoList.descriptionWindow:destroy()
				m_HealthInfoList.descriptionWindow = nil
			end
			
			table.remove(m_HealthInfoList.list, i)
		end
	end
end

m_HealthInfoFunctions.createLabel = function(parent, iconId, description)
	local widget = g_ui.createWidget('SpellAttributeLabel', parent)
	local icon = widget:getChildById('icon')
	
	modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, iconId)
	widget:setText(description)
	widget:setParent(parent)
	return widget
end

m_HealthInfoFunctions.loadIcon = function(now, id, description, iconType, delay, panel)
	local icon = g_ui.createWidget('InfoPlayerConditionIcon')
	icon:setId(id)
	
	if now < 0xFF then
		local particle = icon:getChildById('particle')
		if delay > 0 then
			particle:setMargin(2)
		end
		
		modules.game_spellbar.m_SpellBarFunctions.setIconImageType(icon:getChildById('icon'), now)
		modules.game_spellbar.m_SpellBarFunctions.setIconImageType(particle, now)
	else
		icon:getChildById('particle'):setImageSource(m_HealthInfoFunctions.icons[now - 0xFF].path)
		icon:getChildById('icon'):setImageSource(m_HealthInfoFunctions.icons[now - 0xFF].path)
	end
	
	if id == 'passive_infight' and modules.game_sounds then
		modules.game_sounds.play(SOUNDS_BATTLE)
	elseif id == 'icon_reverse_walking' then
		modules.game_interface.setReverseMoving(true)
	elseif id == 'icon_block_diagonals' then
		modules.game_interface.setBlockingDiagonal(true)
	end
	
	scheduleEvent(function()
		local content, icon = m_HealthInfoFunctions.getIcon(id, iconType, panel)
		if icon then
			icon:getChildById('particle'):hide()
		end
	end, 800)
	
	icon.description = description
	icon.onHoverChange = function(self, hovered)
		if not hovered then
			if m_HealthInfoList.descriptionWindow then
				m_HealthInfoList.descriptionWindow:destroy()
				m_HealthInfoList.descriptionWindow = nil
			end
			
			return false
		end
		
		local rootWidget = modules.game_interface.getRootPanel()
		if not m_HealthInfoList.descriptionWindow then
			m_HealthInfoList.descriptionWindow = g_ui.createWidget('SpellInfoHoverPanel', rootWidget)
		end
		
		local list = m_HealthInfoList.descriptionWindow:getChildById('list')
		if list then
			local height = 16
			
			local widget = m_HealthInfoFunctions.createLabel(list, 17, self.description)
			height = height + widget:getHeight()
			
			if icon.timeLeft then
				widget = m_HealthInfoFunctions.createLabel(list, 38, icon.timeLeft)
				widget:setMarginTop(6)
				height = height + widget:getHeight() + 6
			end
			
			m_HealthInfoList.descriptionWindow.id = self:getId()
			m_HealthInfoList.descriptionWindow:setHeight(height)
		end
		
		local pos = self:getPosition()
		pos.x = pos.x + self:getWidth() - 8
		pos.y = pos.y + self:getHeight() - 8
		
		if pos.x + m_HealthInfoList.descriptionWindow:getWidth() >= rootWidget:getWidth() then
			pos.x = pos.x - m_HealthInfoList.descriptionWindow:getWidth()
		end
		
		if pos.y + m_HealthInfoList.descriptionWindow:getHeight() >= rootWidget:getHeight() then
			pos.y = pos.y - m_HealthInfoList.descriptionWindow:getHeight()
		end
		
		m_HealthInfoList.descriptionWindow:setPosition(pos)
	end
	
	if delay > 0 then
		local progress = icon:getChildById('progress')
		progress:setPercent(0)
		progress:setOn(iconType == modules.game_healthinfo.m_HealthInfoFunctions.TYPE_NEGATIVE)
		
		icon.cooldown = delay * 10
		icon.toCooldown = delay * 10
		
		local begin = true
		for i = 1, #m_HealthInfoList.list do
			local tmpId, tmpPanel = unpack(m_HealthInfoList.list[i])
			if id == tmpId then
				begin = false
				break
			end
		end
		
		if begin then
			table.insert(m_HealthInfoList.list, {id, panel})
		end
		
		if not m_HealthInfoList.event then
			m_HealthInfoList.event = cycleEvent(m_HealthInfoFunctions.checkCooldown, 100)
		end
	else
		icon:getChildById('icon'):setOn(true)
	end
	
	return icon, panel
end

m_HealthInfoFunctions.updateIcon = function(id, panel)
	for i = 1, #m_HealthInfoList.list do
		local tmpId, tmpPanel = unpack(m_HealthInfoList.list[i])
		if id == tmpId then
			m_HealthInfoList.list[i] = {tmpId, panel}
			break
		end
	end
end

m_HealthInfoFunctions.toggleIcon = function(now, id, description, iconType, delay, panel)
	local content, icon = m_HealthInfoFunctions.getIcon(id, iconType, panel)
	if icon then
		icon:destroy()
	elseif content then
		icon, panel = m_HealthInfoFunctions.loadIcon(now, id, description, iconType, delay, panel)
		icon:setParent(content)
	end
	
	m_HealthInfoFunctions.updateSize(content, panel[4])
	return panel, icon
end

m_HealthInfoFunctions.getIcon = function(id, iconType, panel)
	if not panel[1] then
		return true
	end
	
	local widget = panel[2]:getChildById(id)
	if widget then
		return panel[2], widget
	end
	
	local widget = panel[3]:getChildById(id)
	if widget then
		return panel[3], widget
	end
	
	if iconType == m_HealthInfoFunctions.TYPE_POSITIVE then
		return panel[2]
	elseif iconType == m_HealthInfoFunctions.TYPE_NEGATIVE then
		return panel[3]
	end
	
	return false, false
end

m_HealthInfoFunctions.showArcs = function(value)
	if not value then
		if m_HealthInfoList.arcs then
			m_HealthInfoList.arcs:destroy()
			m_HealthInfoList.arcs = nil
		end
		
		return false
	end
	
	if not m_HealthInfoList.arcs then
		m_HealthInfoList.arcs = g_ui.createWidget('HealthManaArcs', modules.game_interface.getMapPanel())
	end
	
	if g_game.isOnline() then
		local localPlayer = g_game.getLocalPlayer()
		onHealthChange(localPlayer, localPlayer:getHealth(), localPlayer:getMaxHealth())
		onManaChange(localPlayer, localPlayer:getMana(), localPlayer:getMaxMana())
	end
end

m_HealthInfoFunctions.updateSize = function(parent, horizontal)
	if horizontal then
		local count = math.ceil(parent:getChildCount() / 12)
		parent:setHeight(count * 18)
		
		if parent == m_HealthInfoList.goodConditionPanel then
			m_HealthInfoList.name:setMarginTop(count * 18)
		end
		
		local count = math.ceil(m_HealthInfoList.badConditionPanel:getChildCount() / 12) + math.ceil(m_HealthInfoList.goodConditionPanel:getChildCount() / 12)
		m_HealthInfoList.window:setHeight(116 + ((count - 1) * 18))
	else
		local count = math.ceil(parent:getChildCount() / 12)
		parent:setWidth(count * 18)
	end
end

m_HealthInfoFunctions.addIcon = function(now, id, description, removeIcon, iconType, delay, panel)
	local content, icon = m_HealthInfoFunctions.getIcon(id, iconType, panel)
	if icon then
		if removeIcon then
			if m_HealthInfoList.descriptionWindow and m_HealthInfoList.descriptionWindow.id == id then
				m_HealthInfoList.descriptionWindow:destroy()
				m_HealthInfoList.descriptionWindow = nil
			end
			
			icon:destroy()
			
			if id == 'passive_infight' and modules.game_sounds then
				modules.game_sounds.play(SOUNDS_GAME)
			elseif id == 'icon_reverse_walking' then
				modules.game_interface.setReverseMoving(false)
			elseif id == 'icon_block_diagonals' then
				modules.game_interface.setBlockingDiagonal(false)
			end
		else
			icon.description = description
			icon.cooldown = delay * 10
			icon.toCooldown = delay * 10
		end
		
		m_HealthInfoFunctions.updateSize(content, panel[4])
		return true
	end
	
	if removeIcon then
		return true
	end
	
	panel, icon = m_HealthInfoFunctions.toggleIcon(now, id, description, iconType, delay, panel)
	return icon
end

function onLoad()
	g_ui.importStyle('game_healthinfo')
	
	local rootPanel = modules.game_interface.getRootPanel()
	m_HealthInfoList.window = g_ui.createWidget('GameInfoPlayerWindow', rootPanel)
	m_HealthInfoList.window:setPosition({x = 182, y = 48})
	m_HealthInfoList.window:setup()
	
	m_HealthInfoList.name = m_HealthInfoList.window:getChildById('name')
	m_HealthInfoList.badConditionPanel = m_HealthInfoList.window:getChildById('badConditionPanel')
	m_HealthInfoList.goodConditionPanel = m_HealthInfoList.window:getChildById('goodConditionPanel')
	
	m_HealthInfoList.health = m_HealthInfoList.window:getChildById('health')
	m_HealthInfoList.healthBar = m_HealthInfoList.health:getChildById('value')
	m_HealthInfoList.healthLabel = m_HealthInfoList.health:getChildById('label')
	m_HealthInfoList.healthSize = m_HealthInfoList.health:getSize()
	
	m_HealthInfoList.mana = m_HealthInfoList.window:getChildById('mana')
	m_HealthInfoList.manaBar = m_HealthInfoList.mana:getChildById('value')
	m_HealthInfoList.manaLabel = m_HealthInfoList.mana:getChildById('label')
	m_HealthInfoList.manaSize = m_HealthInfoList.mana:getSize()
	
	m_HealthInfoList.barrier = m_HealthInfoList.window:getChildById('barrier')
	m_HealthInfoList.barrierBar = m_HealthInfoList.barrier:getChildById('value')
	m_HealthInfoList.barrierLabel = m_HealthInfoList.barrier:getChildById('label')
	m_HealthInfoList.barrierSize = m_HealthInfoList.barrier:getSize()
	
	m_HealthInfoList.experience = m_HealthInfoList.window:getChildById('experience')
	m_HealthInfoList.experienceBar = m_HealthInfoList.experience:getChildById('value')
	m_HealthInfoList.experienceLabel = m_HealthInfoList.experience:getChildById('label')
	m_HealthInfoList.experienceSize = m_HealthInfoList.experience:getSize()
	
	m_HealthInfoList.list = {}
	
	g_keyboard.bindKeyDown('Ctrl+Y', toggle)
	
	for k, v in pairs(m_HealthInfoFunctions.icons) do
		g_textures.preload(v.path)
	end
	
	if g_game.isOnline() then
		local localPlayer = g_game.getLocalPlayer()
		onManaChange(localPlayer, localPlayer:getMana(), localPlayer:getMaxMana())
		onHealthChange(localPlayer, localPlayer:getHealth(), localPlayer:getMaxHealth())
		onBarrerChange(localPlayer, localPlayer:getBarrer(), localPlayer:getMaxBarrer())
		onVocationChange(localPlayer, localPlayer:getVocation())
		onLevelChange(localPlayer, localPlayer:getLevel(), localPlayer:getLevelPercent())
		onGameStart()
	end
	
	if modules.client_options.getOption('drawHealthManaArcs') then
		m_HealthInfoFunctions.showArcs()
	end
	
	connect(LocalPlayer, {
		onManaChange = onManaChange,
		onHealthChange = onHealthChange,
		onBarrerChange = onBarrerChange,
		onStatesChange = onStatesChange,
		onVocationChange = onVocationChange,
		onLevelChange = onLevelChange,
		onUpdateUnknownHealth = onUpdateUnknownHealth,
		onUpdateUnknownMana = onUpdateUnknownMana
	})
	
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
end

function onUnload()
	if m_HealthInfoList.arcs then
		m_HealthInfoList.arcs:destroy()
	end
	
	onGameEnd()
	if m_HealthInfoList.window then
		m_HealthInfoList.badConditionPanel:destroyChildren()
		m_HealthInfoList.goodConditionPanel:destroyChildren()
	
		m_HealthInfoList.window:destroy()
		m_HealthInfoList = {}
	end
	
	g_keyboard.unbindKeyDown('Ctrl+Y')
	
	disconnect(LocalPlayer, {
		onManaChange = onManaChange,
		onHealthChange = onHealthChange,
		onBarrerChange = onBarrerChange,
		onStatesChange = onStatesChange,
		onVocationChange = onVocationChange,
		onLevelChange = onLevelChange,
		onUpdateUnknownHealth = onUpdateUnknownHealth,
		onUpdateUnknownMana = onUpdateUnknownMana
	})

	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
end

function onGameStart()
	m_HealthInfoList.button = modules.client_topmenu.addTopButton('InfoPlayerButton', 'infoPlayerButton', tr('Player information') .. ' (Ctrl+Y)', toggle)
	m_HealthInfoList.button:setOn(m_HealthInfoList.window:isVisible())
	
	local localPlayer = g_game.getLocalPlayer()
	m_HealthInfoList.name:setText(localPlayer:getName())
	m_HealthInfoList.experienceLabel.onHoverChange = modules.game_spellbar.m_SpellBarFunctions.onHoverChange
	
	if m_HealthInfoList.goodConditionPanel then
		m_HealthInfoList.goodConditionPanel:destroyChildren()
		m_HealthInfoList.badConditionPanel:destroyChildren()
	end
	
	m_HealthInfoFunctions.unknownMana = false
	m_HealthInfoFunctions.unknownHealth = false
end

function onGameEnd()
	if m_HealthInfoList.event then
		m_HealthInfoList.event:cancel()
		m_HealthInfoList.event = nil
		m_HealthInfoList.list = {}
	end
	
	if m_HealthInfoList.button then
		m_HealthInfoList.button:destroy()
		m_HealthInfoList.button = nil
	end
	
	if m_HealthInfoList.descriptionWindow then
		m_HealthInfoList.descriptionWindow:destroy()
		m_HealthInfoList.descriptionWindow = nil
	end
end

function onVocationChange(localPlayer, vocationId)
	local clip
	if vocationId == 1 then
		clip = '57 57 19 19'
	elseif vocationId == 2 then
		clip = '19 76 19 19'
	elseif vocationId == 3 then
		clip = '113 57 19 19'
	elseif vocationId == 4 then
		clip = '38 19 19 19'
	elseif vocationId == 5 then
		clip = '0 19 19 19'
	elseif vocationId == 6 then
		clip = '76 38 19 19'
	elseif vocationId == 7 then
		clip = '19 0 19 19'
	elseif vocationId == 8 then
		clip = '94 0 19 19'
	elseif vocationId == 9 then
		clip = '57 0 19 19'
	end
	
	m_HealthInfoList.name:setIconClip(clip)
end

function onUpdateUnknownMana(localPlayer, enabled)
	m_HealthInfoFunctions.unknownMana = enabled
	if enabled then
		onManaChange(localPlayer, m_HealthInfoFunctions.mana[1], m_HealthInfoFunctions.mana[2])
	else
		onManaChange(localPlayer, m_HealthInfoFunctions.mana[1], m_HealthInfoFunctions.mana[2])
	end
end

function onManaChange(localPlayer, mana, maxMana, oldMana, oldMaxMana)
	if maxMana == 0 then
		return true
	end
	
	mana = math.min(mana, maxMana)
	m_HealthInfoFunctions.mana = {mana, maxMana}
	
	local value = mana / maxMana
	local width = 1
	if not m_HealthInfoFunctions.unknownMana then
		width = math.ceil(m_HealthInfoList.manaSize.width * math.min(1, value))
	end
	
	m_HealthInfoList.manaBar:setWidth(width)
	
	local width = 1
	if not m_HealthInfoFunctions.unknownMana then
		width = math.ceil(162 * math.min(1, value))
	end
	
	m_HealthInfoList.manaBar:setImageClip('2221 1825 ' .. width .. ' 18')
	
	if m_HealthInfoFunctions.unknownMana then
		m_HealthInfoList.manaLabel:setText('? / ?')
	else
		m_HealthInfoList.manaLabel:setText(mana .. ' / ' .. maxMana)
	end
	
	if m_HealthInfoList.arcs then
		local height = 1
		if not m_HealthInfoFunctions.unknownMana then
			height = math.max(1, math.ceil(m_HealthInfoList.arcs:getHeight() * math.min(1, (mana / maxMana))))
		end
		
		local widget = m_HealthInfoList.arcs:getChildById('fullManaBar')
		widget:setImageClip('0 ' .. (m_HealthInfoList.arcs:getHeight() - height) .. ' 63 ' .. height)
		widget:setHeight(height)
	end
	
	modules.game_spellbar.onManaChange(localPlayer, mana, maxMana)
end

function onUpdateUnknownHealth(localPlayer, enabled)
	m_HealthInfoFunctions.unknownHealth = enabled
	if enabled then
		onHealthChange(localPlayer, m_HealthInfoFunctions.health[1], m_HealthInfoFunctions.health[2])
	else
		onHealthChange(localPlayer, m_HealthInfoFunctions.health[1], m_HealthInfoFunctions.health[2])
	end
end

function onHealthChange(localPlayer, health, maxHealth, oldHealth, oldMaxHealth)
	if maxHealth == 0 then
		return true
	end
	
	health = math.min(health, maxHealth)
	m_HealthInfoFunctions.health = {health, maxHealth}
	
	local value = health / maxHealth
	local width = 1
	if not m_HealthInfoFunctions.unknownHealth then
		width = math.ceil(m_HealthInfoList.healthSize.width * math.min(1, value))
	end
	
	m_HealthInfoList.healthBar:setWidth(width)
	
	local width = 1
	if not m_HealthInfoFunctions.unknownHealth then
		width = math.ceil(162 * math.min(1, value))
	end
	
	m_HealthInfoList.healthBar:setImageClip('2221 1880 ' .. width .. ' 18')
	
	if m_HealthInfoFunctions.unknownHealth then
		m_HealthInfoList.healthLabel:setText('? / ?')
	else
		m_HealthInfoList.healthLabel:setText(health .. ' / ' .. maxHealth)
	end
	
	if m_HealthInfoList.arcs then
		local widget = m_HealthInfoList.arcs:getChildById('fullHealthBar')
		
		local healthPercent = 1
		local height = 1
		if not m_HealthInfoFunctions.unknownHealth then
			healthPercent = math.ceil(health / maxHealth * 100)
			height = math.max(1, math.ceil(m_HealthInfoList.arcs:getHeight() * math.min(1, (health / maxHealth))))
		end
		
		if healthPercent > 92 then
			widget:setImageColor("#00BC00")
		elseif healthPercent > 60 then
			widget:setImageColor("#50A150")
		elseif healthPercent > 30 then
			widget:setImageColor("#A1A100")
		elseif healthPercent > 8 then
			widget:setImageColor("#BF0A0A")
		elseif healthPercent > 3 then
			widget:setImageColor("#910F0F")
		elseif healthPercent > 0 then
			widget:setImageColor("#850C0C")
		else
			widget:setImageColor("#00BC00")
		end
		
		widget:setImageClip('0 ' .. (m_HealthInfoList.arcs:getHeight() - height) .. ' 63 ' .. height)
		widget:setHeight(height)
	end
end

function onLevelChange(localPlayer, level, percent, oldLevel)
	local value = percent / 100
	local width = math.ceil(m_HealthInfoList.experienceSize.width * math.min(1, value))
	m_HealthInfoList.experienceBar:setWidth(width)
	
	local width = math.ceil(162 * math.min(1, value))
	m_HealthInfoList.experienceBar:setImageClip('2221 2016 ' .. width .. ' 14')
	m_HealthInfoList.experienceLabel:setText(level .. ' ( ' .. percent .. '% )')
	
	if level and oldLevel and oldLevel ~= -1 then
		if level > oldLevel then
			modules.client_destroy.m_DestroyFunctions.showMessage(BLINK_LEVEL_UP, 'AdvancedPanel', oldLevel, level)
		elseif level < oldLevel then
			modules.client_destroy.m_DestroyFunctions.showMessage(BLINK_LEVEL_DOWN, 'AdvancedPanel', oldLevel, level)
		end
	end
end

function onBarrerChange(localPlayer, barrier, maxBarrier)
	maxBarrier = math.max(1, maxBarrier)
	barrier = math.min(barrier, maxBarrier)
	m_HealthInfoFunctions.barrier = {barrier, maxBarrier}
	
	local value = barrier / maxBarrier
	local width = 1
	if not m_HealthInfoFunctions.unknownHealth then
		width = math.ceil(m_HealthInfoList.barrierSize.width * math.min(1, value))
	end
	
	m_HealthInfoList.barrierBar:setWidth(width)
	
	local width = 1
	if not m_HealthInfoFunctions.unknownHealth then
		width = math.ceil(162 * math.min(1, value))
	end
	
	m_HealthInfoList.barrierBar:setImageClip('2221 1931 ' .. width .. ' 14')
	
	if m_HealthInfoFunctions.unknownHealth then
		m_HealthInfoList.barrierLabel:setText('? / ?')
	else
		m_HealthInfoList.barrierLabel:setText(barrier .. ' / ' .. maxBarrier)
	end
end

function onStatesChange(localPlayer, icon, old, id, description, removeIcon, iconType, delay)
	if id == '' then
		return false
	end
	
	modules.game_spellbar.m_SpellBarFunctions.onStatesChange(localPlayer, icon, old, id .. 'Spellbar', description, removeIcon, iconType, delay)
	m_HealthInfoFunctions.addIcon(icon, id, description, removeIcon, iconType, delay, {m_HealthInfoList.window, m_HealthInfoList.goodConditionPanel, m_HealthInfoList.badConditionPanel, true})
end

function toggle()
	if m_HealthInfoList.button:isOn() then
		m_HealthInfoList.window:close()
		m_HealthInfoList.button:setOn(false)
	else
		m_HealthInfoList.window:open()
		m_HealthInfoList.button:setOn(true)
	end
end

-- function setArcsOpacity(value)
	-- if m_HealthInfoList.arcs then
		-- m_HealthInfoList.arcs:setOpacity((100 - value) / 100)
	-- end
-- end
