m_QuestLogFunctions = {}
m_QuestLogList = {}

m_QuestLogFunctions.MAIN_PAGE = 0
m_QuestLogFunctions.LIST_PAGE = 1

m_QuestLogFunctions.GameServerQuestList = 240
m_QuestLogFunctions.GameServerQuestDetails = 241

function parseQuestDetails(protocol, msg)
	local name = msg:getString()
	local description = msg:getString()
	
	local list = {}
	local tmpAmount = msg:getU8()
	for j = 1, tmpAmount do
		local isVisible = msg:getU8() ~= 0
		local isDone = msg:getU8() ~= 0
		local tmpName = msg:getString()
		
		table.insert(list, {isVisible, isDone, tmpName})
	end
	
	if not m_QuestLogList.questDetail then
		m_QuestLogList.questDetail = {}
	end
	
	for i = 1, #m_QuestLogList.questDetail do
		if m_QuestLogList.questDetail[i][1] == name then
			m_QuestLogList.questDetail[i] = {name, description, list}
			m_QuestLogFunctions.updateDescription()
			return true
		end
	end
	
	table.insert(m_QuestLogList.questDetail, {name, description, list})
	m_QuestLogFunctions.updateDescription()
end

function parseQuestList(protocol, msg)
	local list = {}
	local amount = msg:getU16()
	if amount > 0 then
		for i = 1, amount do
			local id = msg:getU16()
			local name = msg:getString()
			local completed = msg:getU8() ~= 0
			
			local tmpList = {}
			local tmpAmount = msg:getU8()
			if tmpAmount > 0 then
				for j = 1, tmpAmount do
					local tmpName = msg:getString()
					local tmpCompleted = msg:getU8() ~= 0
					local tmpMain = msg:getU8() ~= 0
					
					table.insert(tmpList, {tmpName, tmpCompleted, tmpMain})
				end
			end
			
			table.insert(list, {id, name, completed, tmpList})
		end
	end
	
	m_QuestLogList.questList = list
	m_QuestLogFunctions.create()
end

function onUpdateQuestLog(id, name)
	local player = g_game.getLocalPlayer():getId() - 0x10000000
	if not m_QuestLogFunctions.blink[player] then
		m_QuestLogFunctions.blink[player] = {}
	end
	
	if not m_QuestLogFunctions.find(m_QuestLogFunctions.blink[player], name) then
		table.insert(m_QuestLogFunctions.blink[player], {name = name, id = id})
	end
	
	if m_QuestLogList.window and not m_QuestLogList.event then
		m_QuestLogList.event = cycleEvent(m_QuestLogFunctions.blinkQuest, 500)
	end
	
	if not m_QuestLogList.window then
		modules.game_bottompanel.m_MainFunctions.blink(WINDOW_QUESTLOG)
	end
	
	modules.client_destroy.m_DestroyFunctions.showMessage(BLINK_QUEST_LOG, 'QuestLogPanel', id, name)
end

function toggle()
	if m_QuestLogList.window then
		m_QuestLogFunctions.destroy()
	else
		g_game.requestQuestLog()
	end
end

function onGameEnd()
	m_QuestLogFunctions.destroy()
end

function onLoad()
	m_QuestLogFunctions.blink = {}
	g_ui.importStyle('quest_widget')
	ProtocolGame.registerOpcode(m_QuestLogFunctions.GameServerQuestList, parseQuestList)
	ProtocolGame.registerOpcode(m_QuestLogFunctions.GameServerQuestDetails, parseQuestDetails)
	
	connect(g_game, {
		onUpdateQuestLog = onUpdateQuestLog,
		onGameEnd = onGameEnd
	})
end

function onUnload()
	m_QuestLogFunctions.destroy()
	ProtocolGame.unregisterOpcode(m_QuestLogFunctions.GameServerQuestList, parseQuestList)
	ProtocolGame.unregisterOpcode(m_QuestLogFunctions.GameServerQuestDetails, parseQuestDetails)
	
	disconnect(g_game, {
		onUpdateQuestLog = onUpdateQuestLog,
		onGameEnd = onGameEnd
	})
end

m_QuestLogFunctions.find = function(list, name, id)
	for i = 1, #list do
		if (name and list[i].name == name) or (id and list[i].id == id) then
			return i
		end
	end
	
	return false
end

m_QuestLogFunctions.selectById = function()
	if not m_QuestLogFunctions.questOrder then
		return true
	end
	
	for _, v in pairs(m_QuestLogList.list:getChildren()) do
		if tonumber(v:getId()) == m_QuestLogFunctions.questOrder[1] then
			for _, pid in pairs(v:getChildren()) do
				if pid:getText() == m_QuestLogFunctions.questOrder[2] then
					if not pid:getParent():isOn() then
						m_QuestLogFunctions.expand(pid)
					end
					
					m_QuestLogFunctions.select(pid)
					return true
				end
			end
		end
	end
end

m_QuestLogFunctions.onMouseRelease = function(self)
	m_QuestLogFunctions.questOrder = {self.questId, self.questName}
	if not m_QuestLogList.window then
		g_game.requestQuestLog()
	else
		m_QuestLogFunctions.selectById()
	end
end

m_QuestLogFunctions.create = function()
	if m_QuestLogList.window then
		m_QuestLogFunctions.destroy()
	end
	
	modules.game_character.m_CharacterFunctions.destroy()
	modules.game_encyclopedia.m_EncyclopediaFunctions.destroy()
	modules.game_rewards.m_RewardsFunctions.destroy()
	modules.game_store.m_StoreFunctions.onCloseStore()
	modules.game_market.m_MarketFunctions.destroy()

	m_QuestLogList.window = g_ui.displayUI('game_questlog')
	m_QuestLogList.progressList = m_QuestLogList.window:getChildById('progressList')
	m_QuestLogList.descriptionBox = m_QuestLogList.window:getChildById('descriptionBox')
	m_QuestLogList.missionName = m_QuestLogList.window:getChildById('missionName')
	m_QuestLogList.list = m_QuestLogList.window:getChildById('list')
	
	modules.game_bottompanel.m_MainFunctions.open(WINDOW_QUESTLOG)
	
	m_QuestLogList.questDetail = {}
	
	local player = g_game.getLocalPlayer():getId() - 0x10000000
	if m_QuestLogFunctions.blink[player] then
		m_QuestLogList.event = cycleEvent(m_QuestLogFunctions.blinkQuest, 500)
	end
	
	if m_QuestLogList.questList then
		for i = 1, #m_QuestLogList.questList do
			m_QuestLogFunctions.addQuest(m_QuestLogList.questList[i])
		end
	end
	
	m_QuestLogFunctions.selectById()
end

m_QuestLogFunctions.destroy = function()
	if m_QuestLogList.window then
		m_QuestLogList.current = nil
		m_QuestLogList.list:destroyChildren()
		
		m_QuestLogFunctions.stop()
		
		m_QuestLogList.window:destroy()
		m_QuestLogList.window = nil
		
		m_QuestLogList = {}
		
		m_QuestLogFunctions.questOrder = nil
		modules.game_bottompanel.m_MainFunctions.close(WINDOW_QUESTLOG)
	end
end

m_QuestLogFunctions.stop = function()
	if m_QuestLogList.event then
		removeEvent(m_QuestLogList.event)
		m_QuestLogList.event = nil
	end
end

m_QuestLogFunctions.expand = function(self)
	local parent = self:getParent()
	if parent:isOn() then
		parent:setHeight(40)
	else
		parent:setHeight(#parent:getChildren() * 40)
	end
	
	if m_QuestLogList.current then
		m_QuestLogList.current:getChildById('mask'):hide()
		m_QuestLogFunctions.clearDescription()
	end
	
	parent:setOn(not parent:isOn())
end

m_QuestLogFunctions.select = function(self)
	if m_QuestLogList.current then
		m_QuestLogList.current:getChildById('mask'):hide()
	end

	m_QuestLogList.current = self
	self:getChildById('mask'):show()
	
	local name = self:getText()
	local player = g_game.getLocalPlayer():getId() - 0x10000000
	if m_QuestLogFunctions.blink[player] and m_QuestLogFunctions.find(m_QuestLogFunctions.blink[player], name, self.questId) then
		g_game.requestQuestLine(self.questId, name)
	elseif m_QuestLogFunctions.find(m_QuestLogList.questDetail, name) then
		m_QuestLogFunctions.updateDescription()
	else
		g_game.requestQuestLine(self.questId, name)
	end
end

m_QuestLogFunctions.addLabel = function(parent, name, id)
	local widget = g_ui.createWidget(id, parent)
	widget:setText(name)
	return widget
end

m_QuestLogFunctions.clearDescription = function()
	m_QuestLogList.missionName:clearText()
	m_QuestLogList.descriptionBox:clearText()
	m_QuestLogList.progressList:destroyChildren()
end

m_QuestLogFunctions.updateDescription = function()
	m_QuestLogFunctions.clearDescription()
	
	if m_QuestLogList.current then
		local player = g_game.getLocalPlayer():getId() - 0x10000000
		if m_QuestLogFunctions.blink[player] then
			local tmpWidget = m_QuestLogList.current:getParent():getChildByIndex(1)
			local id = tmpWidget.questId
			local i = m_QuestLogFunctions.find(m_QuestLogFunctions.blink[player], m_QuestLogList.current:getText(), id)
			if i then
				m_QuestLogList.current:setChecked(false)
				
				table.remove(m_QuestLogFunctions.blink[player], i)
				if not m_QuestLogFunctions.find(m_QuestLogFunctions.blink[player], false, id) then
					tmpWidget:setChecked(false)
				end
			end
		end
		
		if #m_QuestLogList.questDetail > 0 then
			local name = m_QuestLogList.current:getText()
			for i = 1, #m_QuestLogList.questDetail do
				if name == m_QuestLogList.questDetail[i][1] then
					m_QuestLogList.missionName:setText(m_QuestLogList.questDetail[i][1])
					m_QuestLogList.descriptionBox:setText(m_QuestLogList.questDetail[i][2])
					
					local list = m_QuestLogList.questDetail[i][3]
					for j = #list, 1, -1 do
						if list[j][1] then
							local widget = g_ui.createWidget('ProgressMissionWidget', m_QuestLogList.progressList)
							widget:setText(list[j][3])
							widget:getChildById('checkBox'):setOn(list[j][2])
						end
					end
					
					break
				end
			end
		end
	end
end

m_QuestLogFunctions.addQuest = function(quest)
	local id, name, completed, questList = unpack(quest)
	local parent = g_ui.createWidget('QuestLogContainer', m_QuestLogList.list)
	parent:setId(id)
	
	local widget = m_QuestLogFunctions.addLabel(parent, name, 'MissionLogMission')
	widget.onMouseRelease = m_QuestLogFunctions.expand
	widget:setId(id)
	
	for i = 1, #questList do
		local tmpName, tmpCompleted, tmpMain = unpack(questList[i])
		local tmpWidget = m_QuestLogFunctions.addLabel(parent, tmpName, 'QuestLogMission')
		
		tmpWidget.questId = id
		tmpWidget:setOn(tmpMain)
		tmpWidget:getChildById('checkBox'):setOn(tmpCompleted)
		tmpWidget.onMouseRelease = m_QuestLogFunctions.select
	end
end

m_QuestLogFunctions.blinkQuest = function()
	local player = g_game.getLocalPlayer():getId() - 0x10000000
	local list = m_QuestLogFunctions.blink[player]
	if not list then
		m_QuestLogFunctions.stop()
		return false
	end
	
	for _, v in pairs(m_QuestLogList.list:getChildren()) do
		for _, pid in pairs(v:getChildren()) do
			if m_QuestLogFunctions.find(list, pid:getText(), tonumber(pid:getId())) then
				pid:setChecked(not pid:isChecked())
			end
		end
	end
end
