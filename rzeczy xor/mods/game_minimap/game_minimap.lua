m_MinimapFunctions = {}
m_MinimapList = {}

m_MinimapFunctions.language = LANGUAGE_ENGLISH
m_MinimapFunctions.otmm = true
m_MinimapFunctions.config = {
	-- Cirith
	{ name = {'Cirith', 'Cirith'}, description = {'stolica', 'capital'},
	positions = {
		{{x = 1315, y = 862, z = 7}},
		{{x = 1325, y = 862, z = 7}},
		{{x = 1383, y = 862, z = 7}},
		{{x = 1367, y = 935, z = 7}},
		{{x = 1347, y = 894, z = 7}},
		{{x = 1387, y = 889, z = 7}},
		{{x = 1293, y = 864, z = 7}, {x = 1293, y = 866, z = 7}},
		{{x = 1368, y = 845, z = 7}, {x = 1369, y = 845, z = 7}},
		{{x = 1350, y = 860, z = 7}, {x = 1353, y = 860, z = 7}},
		{{x = 1390, y = 888, z = 7}, {x = 1390, y = 890, z = 7}},
		{{x = 1366, y = 936, z = 7}, {x = 1368, y = 936, z = 7}},
		{{x = 1318, y = 932, z = 7}, {x = 1320, y = 932, z = 7}},
		{{x = 1292, y = 904, z = 7}, {x = 1292, y = 905, z = 7}},
		{{x = 1314, y = 901, z = 8}, {x = 1314, y = 902, z = 8}},
		{{x = 1331, y = 919, z = 7}, {x = 1334, y = 919, z = 7}},
		{{x = 1341, y = 913, z = 7}, {x = 1341, y = 915, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, �wi�tynia', 'capital, temple'},
	positions = {
		{{x = 1351, y = 847, z = 7}},
		{{x = 1351, y = 859, z = 7}, {x = 1352, y = 859, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, brama po�udniowa', 'capital, southern gate'},
	positions = {
		{{x = 1318, y = 937, z = 7}, {x = 1321, y = 937, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, brama wschodnia', 'capital, eastern gate'},
	positions = {
		{{x = 1400, y = 888, z = 7}, {x = 1400, y = 891, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, plac szkoleniowy', 'capital, training ground'},
	positions = {
		{{x = 1366, y = 938, z = 7}, {x = 1368, y = 938, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, cmentarz', 'capital, cemetery'},
	positions = {
		{{x = 1353, y = 831, z = 7}},
		{{x = 1367, y = 842, z = 7}, {x = 1371, y = 842, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, przedzamcze', 'capital, bailey'},
	positions = {
		{{x = 1292, y = 864, z = 7}, {x = 1292, y = 866, z = 7}},
		{{x = 1286, y = 846, z = 7}, {x = 1288, y = 846, z = 7}},
		{{x = 1277, y = 879, z = 7}, {x = 1279, y = 879, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, zamek kr�lewski', 'capital, royal castle'},
	positions = {
		{{x = 1286, y = 843, z = 7}, {x = 1287, y = 843, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, biblioteka miejska', 'capital, public library'},
	positions = {
		{{x = 1340, y = 914, z = 7}, {x = 1340, y = 915, z = 7}},
		{{x = 1332, y = 918, z = 7}, {x = 1333, y = 918, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, opuszczona katedra', 'capital, abandoned cathedral'},
	positions = {
		{{x = 1348, y = 831, z = 7}},
	}},
	{ name = {'Cirith', 'Cirith'}, description = {'stolica, slumsy', 'capital, slums'},
	positions = {
		{{x = 1403, y = 894, z = 7}, {x = 1428, y = 922, z = 7}},
	}},
	
	-- Cirith Kingdom
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'przedpola Cirith', 'fields of Cirith'},
	positions = {
		{{x = 1506, y = 874, z = 7}},
		{{x = 1456, y = 1060, z = 7}},
		{{x = 1152, y = 887, z = 7}},
		{{x = 1206, y = 868, z = 7}},
		{{x = 1218, y = 851, z = 7}},
		{{x = 1476, y = 783, z = 7}, {x = 1476, y = 808, z = 7}},
		{{x = 1402, y = 893, z = 7}, {x = 1429, y = 893, z = 7}},
		{{x = 1401, y = 923, z = 7}, {x = 1428, y = 923, z = 7}},
		{{x = 1429, y = 893, z = 7}, {x = 1429, y = 923, z = 7}},
		{{x = 1465, y = 971, z = 7}, {x = 1465, y = 975, z = 7}},
		{{x = 1466, y = 975, z = 7}, {x = 1473, y = 975, z = 7}},
		{{x = 1384, y = 1064, z = 7}, {x = 1384, y = 1085, z = 7}},
		{{x = 1402, y = 1047, z = 7}, {x = 1406, y = 1047, z = 7}},
		{{x = 1414, y = 1046, z = 7}, {x = 1439, y = 1046, z = 7}},
		{{x = 1273, y = 970, z = 7}, {x = 1273, y = 993, z = 7}},
		{{x = 1292, y = 1001, z = 7}, {x = 1294, y = 1001, z = 7}},
		{{x = 1301, y = 995, z = 7}, {x = 1311, y = 995, z = 7}},
		{{x = 1337, y = 992, z = 7}, {x = 1359, y = 992, z = 7}},
		{{x = 1279, y = 1030, z = 7}, {x = 1311, y = 1030, z = 7}},
		{{x = 1311, y = 1031, z = 7}, {x = 1311, y = 1041, z = 7}},
		{{x = 1220, y = 1126, z = 7}, {x = 1220, y = 1177, z = 7}},
		{{x = 1221, y = 1176, z = 7}, {x = 1258, y = 1176, z = 7}},
		{{x = 1258, y = 1177, z = 7}, {x = 1258, y = 1191, z = 7}},
		{{x = 1162, y = 927, z = 7}, {x = 1162, y = 928, z = 7}},
		{{x = 1241, y = 1193, z = 7}, {x = 1246, y = 1193, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'ziemie zatracenia', 'lands of perdition'},
	positions = {
		{{x = 1184, y = 1182, z = 7}, {x = 1184, y = 1183, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'ziemie niczyje', 'nomans lands'},
	positions = {
		{{x = 1241, y = 1199, z = 7}, {x = 1247, y = 1199, z = 7}},
		{{x = 1197, y = 1182, z = 7}, {x = 1197, y = 1183, z = 7}},
		{{x = 1236, y = 1296, z = 7}, {x = 1236, y = 1298, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'wzg�rza �wiat�a', 'hills of light'},
	positions = {
		{{x = 1220, y = 851, z = 6}},
		{{x = 1206, y = 870, z = 6}},
		{{x = 1150, y = 887, z = 6}},
		{{x = 1164, y = 927, z = 6}, {x = 1164, y = 928, z = 6}},
		{{x = 1278, y = 884, z = 7}, {x = 1279, y = 884, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'ruiny zakonu nekromant�w', 'ruins of necromancers order'},
	positions = {
		{{x = 1295, y = 1118, z = 7}, {x = 1295, y = 1135, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'las Sunray', 'Sunray forest'},
	positions = {
		{{x = 1278, y = 1031, z = 7}, {x = 1310, y = 1031, z = 7}},
		{{x = 1310, y = 1032, z = 7}, {x = 1310, y = 1040, z = 7}},
		{{x = 1221, y = 1127, z = 7}, {x = 1221, y = 1176, z = 7}},
		{{x = 1222, y = 1175, z = 7}, {x = 1259, y = 1175, z = 7}},
		{{x = 1259, y = 1176, z = 7}, {x = 1259, y = 1191, z = 7}},
		{{x = 1296, y = 1118, z = 7}, {x = 1296, y = 1135, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'port po�udniowy', 'southern port'},
	positions = {
		{{x = 1385, y = 1065, z = 7}, {x = 1385, y = 1086, z = 7}},
		{{x = 1401, y = 1048, z = 7}, {x = 1408, y = 1048, z = 7}},
		{{x = 1415, y = 1047, z = 7}, {x = 1438, y = 1047, z = 7}},
		{{x = 1405, y = 1116, z = 6}, {x = 1409, y = 1127, z = 6}},
		
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'wie� po�udniowa', 'southern village'},
	positions = {
		{{x = 1318, y = 941, z = 7}, {x = 1322, y = 940, z = 7}},
		{{x = 1274, y = 969, z = 7}, {x = 1274, y = 994, z = 7}},
		{{x = 1292, y = 1000, z = 7}, {x = 1294, y = 1000, z = 7}},
		{{x = 1300, y = 994, z = 7}, {x = 1310, y = 994, z = 7}},
		{{x = 1337, y = 991, z = 7}, {x = 1359, y = 991, z = 7}},
		{{x = 1313, y = 987, z = 7}, {x = 1315, y = 989, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'g�ra tytan�w', 'titans mountain'},
	positions = {
		{{x = 1506, y = 872, z = 6}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'wzg�rze gigant�w', 'hill of giants'},
	positions = {
		{{x = 1456, y = 1062, z = 6}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'twierdza Aernihad', 'Aernihad fortress'},
	positions = {
		{{x = 1466, y = 971, z = 7}, {x = 1473, y = 974, z = 7}},
	}},
	{ name = {'Kr�lestwo Cirith', 'Cirith Kingdom'}, description = {'las Krimo', 'Krimo forest'},
	positions = {
		{{x = 1477, y = 784, z = 7}, {x = 1477, y = 807, z = 7}},
	}},
	
	-- Chaeck
	{ name = {'Prowincja Chaeck', 'Chaeck province'}, description = {'przedpola Chaeck', 'fields of Chaeck'},
	positions = {
		{{x = 1348, y = 1300, z = 7}},
		{{x = 1288, y = 1370, z = 7}},
		{{x = 1380, y = 1365, z = 7}, {x = 1383, y = 1365, z = 7}},
		{{x = 1353, y = 1300, z = 7}, {x = 1354, y = 1300, z = 7}},
		{{x = 1369, y = 1339, z = 7}, {x = 1369, y = 1342, z = 7}},
		{{x = 1276, y = 1304, z = 7}, {x = 1277, y = 1304, z = 7}},
		{{x = 1299, y = 1286, z = 7}, {x = 1302, y = 1286, z = 7}},
	}},
	{ name = {'Prowincja Chaeck', 'Chaeck province'}, description = {'g�ra cienia', 'mountain of shadow'},
	positions = {
		{{x = 1348, y = 1298, z = 6}},
		{{x = 1352, y = 1299, z = 7}, {x = 1353, y = 1299, z = 7}},
	}},
	{ name = {'Prowincja Chaeck', 'Chaeck province'}, description = {'Chaeck', 'Chaeck'},
	positions = {
		{{x = 1369, y = 1396, z = 6}, {x = 1371, y = 1398, z = 6}},
		{{x = 1380, y = 1366, z = 7}, {x = 1383, y = 1366, z = 7}},
	}},
	{ name = {'Prowincja Chaeck', 'Chaeck province'}, description = {'�wi�tynia Alberta', 'Albert\'s temple'},
	positions = {
		{{x = 1368, y = 1340, z = 7}, {x = 1368, y = 1341, z = 7}},
	}},
	{ name = {'Prowincja Chaeck', 'Chaeck province'}, description = {'posterunek kapitana stra�y', 'post of militia captain'},
	positions = {
		{{x = 1276, y = 1305, z = 7}, {x = 1277, y = 1305, z = 7}},
	}},
	{ name = {'Prowincja Chaeck', 'Chaeck province'}, description = {'siedziba burmistrza', 'seat of the mayor'},
	positions = {
		{{x = 1300, y = 1285, z = 7}, {x = 1301, y = 1285, z = 7}},
	}},
	
}

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		setTime = executeTime
	})

	connect(LocalPlayer, {
		onPositionChange = updatePosition
	})
	
	m_MinimapList.window = g_ui.loadUI('game_minimap', modules.game_interface.getLeftPanel())
	m_MinimapList.panel = m_MinimapList.window:getChildById('consolePanel')
	m_MinimapList.widget = m_MinimapList.panel:getChildById('minimap')
	m_MinimapList.time = m_MinimapList.panel:getChildById('timeLabel')
	m_MinimapList.name = m_MinimapList.panel:getChildById('name')
	m_MinimapList.description = m_MinimapList.panel:getChildById('description')
	
	m_MinimapList.size = g_settings.getSize('MinimapSize', {width = 170, height = 255})
	m_MinimapList.window:setSize(m_MinimapList.size)
	m_MinimapList.window:setup()
	
	if g_game.isOnline() then
		onGameStart()
	else
		m_MinimapFunctions.loadLanguage()
	end
	
	m_MinimapFunctions.loadMap()
	g_keyboard.bindKeyDown('Ctrl+M', toggle)
end

function onUnload()
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		setTime = executeTime
	})

	disconnect(LocalPlayer, {
		onPositionChange = updatePosition
	})
	
	if m_MinimapList.window then
		m_MinimapFunctions.saveMap()
		
		m_MinimapList.window:destroy()
		m_MinimapList = {}
	end
	
	g_keyboard.unbindKeyDown('Ctrl+M')
end

function onGameStart()
	-- local player = g_game.getLocalPlayer()
	-- if player then
		-- updatePosition(player, player:getPosition())
	-- end
	
	m_MinimapFunctions.loadLanguage()
	m_MinimapList.button = modules.client_topmenu.addTopButton('BlankButton', 'minimapButton', tr('Minimap') .. ' (Ctrl+M)', toggle)
	m_MinimapList.button:setOn(m_MinimapList.window:isVisible())
end

function onGameEnd()
	if m_MinimapList.window then
		m_MinimapFunctions.saveMap()
	end
	
	m_MinimapList.selected = nil
	if m_MinimapList.button then
		m_MinimapList.button:destroy()
		m_MinimapList.button = nil
	end
end

function executeTime(hour, minute)
	if minute == 60 then
		hour = hour + 1
		minute = 0
	end
	
	if hour == 24 then
		hour = 0
	end
	
	m_MinimapList.time:setText((hour < 10 and '0' or '') .. hour .. ':' .. (minute < 10 and '0' or '') .. minute)
end

function toggle()
	if m_MinimapList.button:isOn() then
		m_MinimapList.window:close()
		m_MinimapList.button:setOn(false)
	else
		m_MinimapList.window:open()
		m_MinimapList.button:setOn(true)
	end
end

function updatePosition(localPlayer, position)
	if not m_MinimapList.widget:isDragging() then
		m_MinimapList.widget:setCameraPosition(position)
		m_MinimapList.widget:setCrossPosition(position)
	end
	
	for i = 1, #m_MinimapFunctions.config do
		for j = 1, #m_MinimapFunctions.config[i].positions do
			local fromPos = m_MinimapFunctions.config[i].positions[j][1]
			local toPos = m_MinimapFunctions.config[i].positions[j][2] or fromPos
			if m_MinimapFunctions.isInRange(position, fromPos, toPos) then
				m_MinimapFunctions.updateDescription(m_MinimapFunctions.config[i].name[m_MinimapFunctions.language], m_MinimapFunctions.config[i].description[m_MinimapFunctions.language])
				return true
			end
		end
	end
end

m_MinimapFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_MinimapFunctions.language = LANGUAGE_ENGLISH
	else
		m_MinimapFunctions.language = LANGUAGE_POLISH
	end
end

m_MinimapFunctions.isInRange = function(position, fromPosition, toPosition)
    return position.x >= fromPosition.x and position.y >= fromPosition.y and position.z >= fromPosition.z and position.x <= toPosition.x and position.y <= toPosition.y and position.z <= toPosition.z
end

m_MinimapFunctions.destroyMaximizeMinimap = function()
	if m_MinimapList.oldSize then
		m_MinimapFunctions.toggleFullMap()
	end
end

m_MinimapFunctions.toggleFullMap = function()
	if not m_MinimapList.oldSize then
		m_MinimapList.oldSize = m_MinimapList.window:getSize()
		m_MinimapList.window:setSize({width = 800, height = 600})
	else
		m_MinimapList.window:setSize(m_MinimapList.oldSize)
		m_MinimapList.oldSize = nil
	end
	
	updateCameraPosition()
end

m_MinimapFunctions.loadMap = function()
	if not m_MinimapList.widget then
		m_MinimapList.widget = infoPlayerWindow:getChildById('minimap')
	end
	
	if m_MinimapFunctions.otmm then
		local minimapFile = '/minimap.otmm'
		if g_resources.fileExists(minimapFile) then
			g_minimap.loadOtmm(minimapFile)
		end
	else
		local minimapFile = '/minimap.otcm'
		if g_resources.fileExists(minimapFile) then
			g_map.loadOtcm(minimapFile)
		end
	end
	
	m_MinimapList.widget:load()
end

m_MinimapFunctions.saveMap = function()
	if m_MinimapList.oldSize then
		g_settings.set('MinimapSize', m_MinimapList.oldSize)
	else
		g_settings.set('MinimapSize', m_MinimapList.size)
	end
	
	if m_MinimapFunctions.otmm then
		local minimapFile = '/minimap.otmm'
		g_minimap.saveOtmm(minimapFile)
	else
		local minimapFile = '/minimap.otcm'
		g_map.saveOtcm(minimapFile)
	end
	
	if m_MinimapList.widget then
		m_MinimapList.widget:save()
	end
end

m_MinimapFunctions.zoomIn = function()
	if m_MinimapList.widget then
		m_MinimapList.widget:zoomIn()
	end
end

m_MinimapFunctions.zoomOut = function()
	if m_MinimapList.widget then
		m_MinimapList.widget:zoomOut()
	end
end

m_MinimapFunctions.floorDown = function()
	if m_MinimapList.widget then
		m_MinimapList.widget:floorDown(1)
	end
end

m_MinimapFunctions.floorUp = function()
	if m_MinimapList.widget then
		m_MinimapList.widget:floorUp(1)
	end
end

m_MinimapFunctions.onGeometryChange = function(self)
	local height = self:getHeight()
	if height <= 200 then
		self:setHeight(200)
	end
	
	local width = self:getWidth()
	if width <= 170 then
		self:setWidth(170)
	end
	
	m_MinimapList.size = self:getSize()
end

m_MinimapFunctions.destroy = function()
	if m_MinimapList.animate then
		if m_MinimapList.event then
			removeEvent(m_MinimapList.event)
			m_MinimapList.event = nil
		end
		
		m_MinimapList.animate:destroy()
		m_MinimapList.animate = nil
	end
end

m_MinimapFunctions.updateDescription = function(name, description)
	if m_MinimapList.name:getText() == name and m_MinimapList.description:getText() == description then
		return true
	end
	
	m_MinimapList.name:setText(name)
	m_MinimapList.description:setText(description)
	
	m_MinimapFunctions.destroy()
	m_MinimapList.animate = g_ui.createWidget('AnimatedMessage', modules.game_interface.getRootPanel())
	m_MinimapList.animate:getChildById('name'):setText(name)
	m_MinimapList.animate:getChildById('description'):setText(description)
	
	g_effects.fadeIn(m_MinimapList.animate, 500)
	m_MinimapList.event = scheduleEvent(function()
		g_effects.fadeOut(m_MinimapList.animate, 500)
		m_MinimapList.event = scheduleEvent(function() m_MinimapFunctions.destroy() end, 500)
	end, 4000)
end

m_MinimapFunctions.onTextChange = function(self)
	if self:getText() ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
end