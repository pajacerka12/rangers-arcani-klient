InventorySlotStyles = {
	[InventorySlotHead] = "HeadSlot",
	[InventorySlotNeck] = "NeckSlot",
	[InventorySlotBack] = "BackSlot",
	[InventorySlotBody] = "BodySlot",
	[InventorySlotRight] = "RightSlot",
	[InventorySlotLeft] = "LeftSlot",
	[InventorySlotLeg] = "LegSlot",
	[InventorySlotFeet] = "FeetSlot",
	[InventorySlotFinger] = "FingerSlot",
	[InventorySlotAmmo] = "AmmoSlot",
	[InventoryLightSlot] = "LightSlot",
	[InventorySecondRing] = "SecondSlot",
}

local ITEMCLASS_NONE = 0
local ITEMCLASS_DESOLATE = 1
local ITEMCLASS_DAMAGED = 2
local ITEMCLASS_NORMAL = 3
local ITEMCLASS_PERFECT = 4
local ITEMCLASS_LEGENDARY = 5
local ITEMCLASS_UNIQUE = 6
local ITEMCLASS_QUEST = 7
local ITEMCLASS_IGNORE = 8
local ITEMCLASS_SHOP = 9

SetIconType = {
	[1] = {"set", "Brak element�w zestawowych"},
	[2] = {"leatheri", "Posiadasz podstawowy zestaw sk�rzany"},
	[3] = {"leatherii", "Posiadasz rozszerzony zestaw sk�rzany"},
	[4] = {"copperi", "Posiadasz podstawowy zestaw miedziany"},
	[5] = {"copperii", "Posiadasz rozszerzony zestaw miedziany"},
}

m_inventoryWindow = nil
m_inventoryPanel = nil
m_inventoryButton = nil

m_capacityButton = nil
m_chaseButton = nil
m_stayButton = nil
m_outfitButton = nil
m_guildButton = nil
m_skillsButton = nil
m_safeButton = nil
m_lockBox = nil
m_lockBoxEnabled = true

function onLoad()
	m_inventoryWindow = g_ui.loadUI('game_inventory', modules.game_interface.getRightPanel())
	m_inventoryWindow:setup()
	m_inventoryPanel = m_inventoryWindow:getChildById('contentsPanel')
	
	m_capacityButton = m_inventoryWindow:getChildById('capacity')
	m_chaseButton = m_inventoryWindow:getChildById('chase')
	m_stayButton = m_inventoryWindow:getChildById('stay')
	m_outfitButton = m_inventoryWindow:getChildById('outfit')
	m_guildButton = m_inventoryWindow:getChildById('guild')
	m_skillsButton = m_inventoryWindow:getChildById('skills')
	m_lockBox = m_inventoryWindow:getChildById('lockBox')
	m_safeButton = m_inventoryWindow:getChildById('safeButton')
	
	m_lockBoxEnabled = toboolean(g_settings.get('inventoryLock', true))
	m_lockBox:setOn(not m_lockBoxEnabled)
	lock(m_lockBox)
	
	g_keyboard.bindKeyDown('Ctrl+I', toggle)
  
	connect(LocalPlayer, {
		onInventoryChange = onInventoryChange,
		onSetModeChange = onSetModeChange,
		onFreeCapacityChange = onFreeCapacityChange
	})
  
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onChaseModeChange = onChaseModeChange
	})

	if g_game.isOnline() then
		onGameStart()
	end
end

function toggle()
	if m_inventoryButton:isOn() then
		m_inventoryWindow:close()
		m_inventoryButton:setOn(false)
	else
		m_inventoryWindow:open()
		m_inventoryButton:setOn(true)
	end
end

function onUnload()
	disconnect(LocalPlayer, {
		onInventoryChange = onInventoryChange,
		onSetModeChange = onSetModeChange,
		onFreeCapacityChange = onFreeCapacityChange
	})

	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onChaseModeChange = onChaseModeChange
	})

	g_keyboard.unbindKeyDown('Ctrl+I')
	m_inventoryWindow:destroy()
	m_inventoryWindow = nil
	
	m_capacityButton = nil
	m_chaseButton = nil
	m_stayButton = nil
	m_outfitButton = nil
	m_guildButton = nil
	m_skillsButton = nil
	m_safeButton = nil
	m_lockBox = nil

	m_inventoryPanel = nil
	onGameEnd()
	
	g_settings.set('inventoryLock', m_lockBoxEnabled)
end

function onGameStart()
	m_inventoryButton = modules.client_topmenu.addTopButton('InventoryButton', 'inventoryButton', tr('Inventory') .. ' (Ctrl+I)', toggle)
	m_inventoryButton:setOn(m_inventoryWindow:isVisible())
	
	local player = g_game.getLocalPlayer()
	for i = InventorySlotFirst, InventorySlotLast do
		if g_game.isOnline() then
			onInventoryChange(player, i, player:getInventoryItem(i))
		else
			onInventoryChange(player, i, nil)
		end
	end
	
	if g_game.isOnline() then
		onFreeCapacityChange(player, player:getFreeCapacity())
	end
	
	onSetChaseMode(DontChase)
	onSetSafeFight(modules.client_options.getOption('secureMode'))
end

function onGameEnd()
	if m_inventoryButton then
		m_inventoryButton:destroy()
		m_inventoryButton = nil
	end
end

function onFreeCapacityChange(player, freeCapacity, oldFreeCapacity, totalCapacity)
	m_capacityButton:setText(tr('Capacity') .. ': ' .. freeCapacity / 1000)
end
  
function onInventoryChange(player, slot, item, oldItem, z, favorite)
	if slot >= InventorySlotScroll and slot <= InventorySlotBellows then
		modules.game_rewards.m_CraftingFunctions.onInventoryChange(player, slot, item, oldItem, z, favorite)
		return true
	elseif slot >= InventoryRuneConvert1 and slot <= InventoryRuneResult then
		modules.game_runes.m_RunesFunctions.onInventoryChange(player, slot, item, oldItem, z, favorite)
		return true
	end
	
	if slot >= InventorySlotLast then 
		return false
	end
	
	local widget = nil
	if slot >= InventoryRune1 and slot <= InventoryRune10 then
		local window = modules.game_character.m_RunesFunctions.getRunesWindow()
		if window then
			widget = window:getChildById('slot' .. slot)
		end
	else
		widget = m_inventoryPanel:getChildById('slot' .. slot)
	end
	
	if not widget then
		return false
	end
	
	local label = widget:getChildById('label')
	local star = widget:getChildById('star')
	if item then
		-- if item:getId() == 16814 then
			-- item:setShader(g_shaders.getShader('Test'), 0, 0)
		-- end
		
		if slot < InventoryRune1 then
			widget:setStyle('InventoryItem')
		end
		
		widget:setItem(item)
		
		local color = item:getLabelColor()
		if item:isLabelable() and color ~= 0 then
			label:show()
			label:setImageColor(getOufitColor(77 + color))
			label:setText(item:getLabelSymbol())
		else
			label:hide()
		end
		
		updateItemQuality(widget, item:getClassId())
		star:setVisible(favorite)
	else
		if modules.game_lookitemmove and widget:getItem() then
			modules.game_lookitemmove.m_LookItemMoveFunction.destroyWindowByItem(widget:getItem():getId())
		end
		
		if slot < InventoryRune1 then
			widget:setStyle(InventorySlotStyles[slot])
		end
		
		widget:setItem(nil)
		
		if slot >= InventoryRune1 and slot <= InventoryRune10 then
			widget:setImageClip('32 1753 34 35')
		end
		
		label:hide()
		widget:getChildById('corner'):hide()
		star:hide()
	end
end

local function updateFavoriteStar(slot, item, value)
	if not item then
		return false
	end
	
	local itemWidget = nil
	if slot >= InventoryRune1 and slot <= InventoryRune10 then
		local window = modules.game_character.m_RunesFunctions.getRunesWindow()
		if window then
			itemWidget = window:getChildById('slot' .. slot)
		end
	else
		itemWidget = m_inventoryPanel:getChildById('slot' .. slot)
	end
	
	if not itemWidget then
		return false
	end
	
	local star = itemWidget:getChildById('star')
	if value then
		star:setVisible(item:isInFavorites())
	else
		star:hide()
	end
end

local function updateCorner(slot, item, value)
	if not item then
		return false
	end
	
	local itemWidget = nil
	if slot >= InventoryRune1 and slot <= InventoryRune10 then
		local window = modules.game_character.m_RunesFunctions.getRunesWindow()
		if window then
			itemWidget = window:getChildById('slot' .. slot)
		end
	else
		itemWidget = m_inventoryPanel:getChildById('slot' .. slot)
	end
	
	if not itemWidget then
		return false
	end
	
	local corner = itemWidget:getChildById('corner')
	if value then
		local classId = item:getClassId()
		if classId ~= ITEMCLASS_IGNORE then
			corner:setImageClip(modules.game_containers.getImageClipByClassId(classId))
			corner:show()
		else
			corner:hide()
		end
	else
		corner:hide()
	end
end

local function updateFrame(slot, item, value)
	if not item then
		return false
	end
	
	local itemWidget = nil
	if slot >= InventoryRune1 and slot <= InventoryRune10 then
		local window = modules.game_character.m_RunesFunctions.getRunesWindow()
		if window then
			itemWidget = window:getChildById('slot' .. slot)
		end
	else
		itemWidget = m_inventoryPanel:getChildById('slot' .. slot)
	end
	
	if not itemWidget then
		return false
	end
	
	itemWidget:setImageClip(modules.game_containers.getFrameClipByClassId(value and item:getClassId() or ITEMCLASS_IGNORE))
end

function updateFavoriteStars(value)
	if not g_game.isOnline() then
		return false
	end

	local player = g_game.getLocalPlayer()
	for i = InventorySlotFirst, InventorySlotLast do
		updateFavoriteStar(i, player:getInventoryItem(i), value)
	end
end

function updateCorners(value)
	if not g_game.isOnline() then
		return false
	end

	local player = g_game.getLocalPlayer()
	for i = InventorySlotFirst, InventorySlotLast do
		updateCorner(i, player:getInventoryItem(i), value)
	end
end

function updateFrames(value)
	if not g_game.isOnline() then
		return false
	end

	local player = g_game.getLocalPlayer()
	for i = InventorySlotFirst, InventorySlotLast do
		updateFrame(i, player:getInventoryItem(i), value)
	end
end

function blinkButton(var)
	executeBlink(var, true)
end

function stopBlink(var)
	executeBlink(var, false)
end

function executeBlink(var, visible)
	local id = ''
	local fromParent = nil
	if var == BLINK_QUEST_ITEMS then
		if modules.game_containers.isOpenQuestContainer() then
			return true
		end
		
		id = 'slot10'
		fromParent = m_inventoryPanel
	end
	
	local widget = fromParent:getChildById(id)
	if widget then
		if visible then
			widget:getChildById('animatedSquare'):show()
		else
			widget:getChildById('animatedSquare'):hide()
		end
	end
end

function onSetSafeFight(enabled)
	g_game.setSafeFight(enabled)
	modules.client_options.setOption('secureMode', enabled)
	m_safeButton:setOn(not enabled)
end

function onSetChaseMode(id)
	if id == DontChase then
		m_chaseButton:setOn(false)
		m_stayButton:setOn(true)
	else
		m_chaseButton:setOn(true)
		m_stayButton:setOn(false)
	end
	
	g_game.setChaseMode(id)
end

function onChaseModeChange(chaseMode)
	m_chaseButton:setOn(chaseMode == 1)
	m_stayButton:setOn(chaseMode == 0)
end

function onGuildPanel()
	if m_guildButton:isOn() then
		modules.game_guild.destroy()
	else
		modules.game_guild.open()
	end
end

function onOutfitMode()
	if m_outfitButton:isOn() then
		modules.game_outfit.destroy()
		m_outfitButton:setOn(false)
	else
		g_game.requestOutfit()
		m_outfitButton:setOn(true)
	end
end

function setGuild(state)
	m_guildButton:setOn(state)
end

function setOutfit()
	m_outfitButton:setOn(false)
end

function setSkills(on)
	if on then
		setChecked(false)
	end

	m_skillsButton:setOn(on)
end

function setChecked(checked)
	-- m_skillsButton:setChecked(checked)
end

function blinkSkills()
	-- if modules.game_skills.isOpenSkills() then
		-- return true
	-- end
	
	-- m_skillsButton:setChecked(true)
end

function lock(self)
	m_lockBoxEnabled = not self:isOn()
	self:setOn(m_lockBoxEnabled)
	
	if not m_lockBoxEnabled then
		m_inventoryWindow:setDraggable(false)
		m_inventoryWindow:getChildById('background'):hide()
	else
		m_inventoryWindow:setDraggable(true)
		m_inventoryWindow:getChildById('background'):show()
	end
end
