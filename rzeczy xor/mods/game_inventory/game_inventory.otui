InventoryItem < ContainerItem
  size: 34 34
  image-source: /ui/gui_atlas
  image-clip: 8 445 34 34

HeadSlot < InventoryItem
  id: slot1
  image-clip: 48 286 34 34
  &position: {x=65535, y=1, z=0}

NeckSlot < InventoryItem
  id: slot2
  image-clip: 8 286 34 34
  &position: {x=65535, y=2, z=0}

BackSlot < InventoryItem
  id: slot3
  image-clip: 88 286 34 34
  &position: {x=65535, y=3, z=0}
  
BodySlot < InventoryItem
  id: slot4
  image-clip: 48 326 34 34
  &position: {x=65535, y=4, z=0}

RightSlot < InventoryItem
  id: slot5
  image-clip: 88 326 34 34
  &position: {x=65535, y=5, z=0}

LeftSlot < InventoryItem
  id: slot6
  image-clip: 8 326 34 34
  &position: {x=65535, y=6, z=0}

LegSlot < InventoryItem
  id: slot7
  image-clip: 48 366 34 34
  &position: {x=65535, y=7, z=0}

FeetSlot < InventoryItem
  id: slot8
  image-clip: 48 405 34 34
  &position: {x=65535, y=8, z=0}

FingerSlot < InventoryItem
  id: slot9
  image-clip: 88 366 34 34
  &position: {x=65535, y=9, z=0}

AmmoSlot < InventoryItem
  id: slot10
  image-clip: 8 405 34 34
  &position: {x=65535, y=10, z=0}
  
  AnimatedSquare

LightSlot < InventoryItem
  id: slot11
  image-clip: 67 450 34 34
  &position: {x=65535, y=11, z=0}

SecondSlot < InventoryItem
  id: slot12
  image-clip: 88 366 34 34
  &position: {x=65535, y=12, z=0}

SettingsButton < UIWidget
  image-source: /ui/gui_atlas
  size: 22 22
  
UIMiniWindow
  id: inventoryWindow
  size: 170 210
  focusable: false
  &save: true
  
  UIWidget
    id: background
    anchors.fill: parent
    image-source: /ui/gui_atlas
    image-clip: 301 310 148 210
    image-border: 12
    phantom: true

  LeafWindowContents
    margin-top: 0

    BackSlot
      anchors.top: next.top
      anchors.left: next.right
      margin-left: 6

    HeadSlot
      anchors.top: parent.top
      anchors.horizontalCenter: parent.horizontalCenter
      margin-top: 38

    NeckSlot
      anchors.top: prev.top
      anchors.right: prev.left
      margin-right: 6

    LeftSlot
      anchors.top: prev.bottom
      anchors.horizontalCenter: prev.horizontalCenter
      margin-top: 3

    BodySlot
      anchors.top: prev.top
      anchors.left: prev.right
      margin-left: 6

    RightSlot
      anchors.top: prev.top
      anchors.left: prev.right
      margin-left: 6

    SecondSlot
      anchors.top: prev.bottom
      anchors.horizontalCenter: prev.horizontalCenter
      margin-top: 3

    LegSlot
      anchors.top: prev.top
      anchors.right: prev.left
      margin-right: 6

    FingerSlot
      anchors.top: prev.top
      anchors.right: prev.left
      margin-right: 6

    AmmoSlot
      anchors.top: prev.bottom
      anchors.horizontalCenter: prev.horizontalCenter
      margin-top: 3

    FeetSlot
      anchors.top: prev.top
      anchors.left: prev.right
      margin-left: 6

    LightSlot
      anchors.top: prev.top
      anchors.left: prev.right
      margin-left: 6
  
  Label
    id: capacity
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    margin: 10
    color: #FFFFFF
    
  SettingsButton
    id: stay
    anchors.right: next.left
    anchors.verticalcenter: next.verticalcenter
    margin-right: 2
    !tooltip: tr('Do not chase opponent')
    @onClick: modules.game_inventory.onSetChaseMode(0)
    image-clip: 448 6 22 22
    
    $hover:
      image-clip: 448 34 22 22

    $pressed:
      image-clip: 448 62 22 22
    
    $on:
      image-clip: 448 90 22 22
    
  SettingsButton
    id: chase
    anchors.right: next.left
    anchors.verticalcenter: next.verticalcenter
    margin-right: 2
    !tooltip: tr('Chase opponent')
    @onClick: modules.game_inventory.onSetChaseMode(1)
    image-clip: 423 6 22 22
    
    $hover:
      image-clip: 423 34 22 22

    $pressed:
      image-clip: 423 62 22 22
    
    $on:
      image-clip: 423 90 22 22
    
  UIWidget
    id: guild
    image-source: /ui/gui_atlas
    size: 28 28
    margin-top: 8
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    !tooltip: tr('Open guild panel')
    @onClick: modules.game_inventory.onGuildPanel()
    image-clip: 392 3 28 28
    
    $hover:
      image-clip: 392 31 28 28

    $pressed:
      image-clip: 392 59 28 28
    
    $on:
      image-clip: 392 87 28 28
  
  SettingsButton
    id: safeButton
    anchors.left: prev.right
    anchors.verticalcenter: prev.verticalcenter
    margin-left: 2
    !tooltip: tr('Secure mode')
    @onClick: modules.game_inventory.onSetSafeFight(self:isOn())
    image-clip: 367 6 22 22
    
    $hover:
      image-clip: 367 34 22 22

    $pressed:
      image-clip: 367 62 22 22
    
    $on:
      image-clip: 367 90 22 22
    
  SettingsButton
    id: outfit
    anchors.left: prev.right
    anchors.verticalcenter: prev.verticalcenter
    margin-left: 2
    !tooltip: tr('Change outfit')
    @onClick: modules.game_inventory.onOutfitMode()
    image-clip: 342 6 22 22
    
    $hover:
      image-clip: 342 34 22 22

    $pressed:
      image-clip: 342 62 22 22
    
    $on:
      image-clip: 342 90 22 22
    
  AtlasWidget
    image-clip: 218 55 19 37
    anchors.top: parent.top
    anchors.left: parent.left
    margin-top: -4
    margin-left: -5
    size: 19 37
    phantom: true
    
  AtlasWidget
    image-clip: 251 22 77 68
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    margin-bottom: -13
    margin-right: -9
    size: 77 68
    phantom: true
  
  LockBox
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    @onClick: modules.game_inventory.lock(self)