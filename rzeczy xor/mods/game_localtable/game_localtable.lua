m_LocalTableFunctions = {}
m_LocalTableList = {}

m_LocalTableFunctions.destroy = function()
	if m_LocalTableList.window then
		m_LocalTableList.list:destroyChildren()
		
		m_LocalTableList.window:destroy()
		m_LocalTableList = {}
	end
end

m_LocalTableFunctions.select = function(self)
	if m_LocalTableList.current then
		if m_LocalTableList.current == self then
			return true
		end
		
		m_LocalTableList.current:getChildById('mask'):hide()
	end
	
	m_LocalTableList.current = self
	self:getChildById('mask'):show()
	m_LocalTableList.description:setText(self.description)
	m_LocalTableList.accept:show()
end

m_LocalTableFunctions.accept = function()
	if not m_LocalTableList.current then
		m_LocalTableList.accept:hide()
		return true
	end
	
	g_game.sendChooseQuest(m_LocalTableList.current.id, m_LocalTableList.current.value)
	m_LocalTableList.accept:hide()
	m_LocalTableList.current:destroy()
	m_LocalTableList.current = nil
	m_LocalTableList.description:clearText()
end

function onLoad()
	connect(g_game, {
		onGameEnd = onGameEnd,
		onAddQuestLocalTable = onAddQuestLocalTable
	})
end

function onUnload()
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onAddQuestLocalTable = onAddQuestLocalTable
	})
	
	onGameEnd()
end

function onCreaturePositionChange(creature, newPos, oldPos)
	if creature:isLocalPlayer() then
		m_LocalTableFunctions.destroy()
	end
end

function onGameEnd()
	m_LocalTableFunctions.destroy()
end

function onAddQuestLocalTable(id, name, description, value)
	if id == 0 then
		m_LocalTableFunctions.destroy()
		return true
	end
	
	if not m_LocalTableList.window then
		m_LocalTableList.window = g_ui.displayUI('game_localtable')
		m_LocalTableList.list = m_LocalTableList.window:getChildById('list')
		m_LocalTableList.description = m_LocalTableList.window:getChildById('description')
		m_LocalTableList.accept = m_LocalTableList.window:getChildById('accept')
	end
	
	if id > 0 then
		local widget = g_ui.createWidget('LocalTableLabel', m_LocalTableList.list)
		widget:setText(name)
		widget.name = name
		widget.description = description
		widget.id = id
		widget.value = value
	end
end
