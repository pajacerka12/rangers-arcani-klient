m_TaskFunctions = {}
m_TaskList = {}

m_TaskFunctions.GameServerTaskList = 61

function parseTaskList(protocol, msg)
	local list = {}
	local size = msg:getU8()
	if size > 0 then
		for i = 1, size do
			local id = msg:getU8()
			local delay = msg:getU32()
			
			table.insert(list, {id, delay})
		end
	end
	
	m_TaskFunctions.execute(list, size)
end

function onGameEnd()
	m_TaskFunctions.destroy()
	m_TaskFunctions.protocol = nil
end

function onGameStart()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_TaskFunctions.language = LANGUAGE_ENGLISH
	else
		m_TaskFunctions.language = LANGUAGE_POLISH
	end
	
	m_TaskFunctions.protocol = g_game.getProtocolGame()
end

function onLoad()
	g_ui.importStyle('task_widget')
	ProtocolGame.registerOpcode(m_TaskFunctions.GameServerTaskList, parseTaskList)
	
	if g_game.isOnline() then
		onGameStart()
	end
	
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
end

function onUnload()
	m_TaskFunctions.destroy()
	ProtocolGame.unregisterOpcode(m_TaskFunctions.GameServerTaskList, parseTaskList)
	
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
end

m_TaskFunctions.destroy = function()
	if m_TaskList.window then
		m_TaskList.taskList:destroyChildren()
		m_TaskList.monsterList:destroyChildren()
		
		m_TaskList.window:destroy()
		m_TaskList = {}
	end
end

m_TaskFunctions.selectMonster = function(self)
	modules.game_encyclopedia.m_BestiaryFunctions.selectMonsterByName(self.name, self.classId)
end

m_TaskFunctions.select = function(self)
	if m_TaskList.currentSelected then
		m_TaskList.currentSelected:getChildById('mask'):hide()
	end
	
	m_TaskList.currentSelected = self
	self:getChildById('mask'):show()
	
	m_TaskList.experience:setText(self.experience)
	m_TaskList.bossName:setText(self.bossName)
	m_TaskList.taskGroup:setText(self.name)
	
	m_TaskList.monsterList:destroyChildren()
	for i = 1, #self.creatureList do
		local name = self.creatureList[i]
		local mType = modules.game_lookat.m_LookAtFunctions.getMonsterByName(name)
		if mType then
			local widget = g_ui.createWidget('CreatureType', m_TaskList.monsterList)
			widget:getChildById('creature'):setOutfit(mType.look)
			widget:getChildById('name'):setText(mType.description[m_TaskFunctions.language])
			widget.name = mType.name
			widget.classId = mType.classId
			widget.onMouseRelease = m_TaskFunctions.selectMonster
		end
	end
	
	m_TaskList.items:destroyChildren()
	m_TaskList.items:setWidth((#self.itemList / 3) * 64)
	for i = 1, #self.itemList / 3 do
		local it = modules.game_lookat.m_LookAtFunctions.getItemDescription(self.itemList[i * 3 - 2])
		if it then
			local widget = g_ui.createWidget('TaskItem', m_TaskList.items)
			
			local tmpWidget = widget:getChildById('item')
			tmpWidget.classId = self.itemList[i * 3]
			tmpWidget:setItemId(it.clientId)
			tmpWidget:setItemCount(self.itemList[i * 3 - 1])
			widget:getChildById('corner'):setImageClip(modules.game_containers.getImageClipByClassId(tmpWidget.classId))
		end
	end
end

m_TaskFunctions.getTaskByName = function(name)
	if name == 'Proclay' then
		return PROCLAY_TASK_LIST
	elseif name == 'Dantven' then
		return DANTVEN_TASK_LIST
	elseif name == 'Inquisition' then
		return {[1] = {class = {'INKWIZYCJA', 'INQUISITION'}, count = 666}}
	elseif name == 'Shaburak' then
		return {[1] = {class = {'SHABURAK', 'SHABURAK'}, count = 400}}
	elseif name == 'Askarak' then
		return {[1] = {class = {'ASKARAK', 'ASKARAK'}, count = 400}}
	elseif name == 'Cemetery' then
		return {[1] = {class = {'GRUNFELD - NIEUMARLI', 'GRUNFELD - UNDEADS'}, count = 200}}
	elseif name == 'Goblin' then
		return {[1] = {class = {'GOBLINY', 'GOBLINS'}, count = 500}}
	elseif name == 'Larva' then
		return {[1] = {class = {'LARWY', 'LARVAS'}, count = 50}}
	elseif name == 'Drazen' then
		return {[1] = {class = {'DRAZEN - ORKI', 'DRAZEN - ORCS'}, count = 700},
			[2] = {class = {'DRAZEN - TEMLARIUSZE', 'DRAZEN - TEMPLARS'}, count = 500}}
	elseif name == 'Savage' then
		return {[1] = {class = {'BARTOLOMEUS - DZIKUSY', 'BARTOLOMEUS - SAVAGES'}, count = 500},
			[2] = {class = {'BARTOLOMEUS - DZIKUSY', 'BARTOLOMEUS - SAVAGES'}, count = 750},
			[3] = {class = {'BARTOLOMEUS - DZIKUSY', 'BARTOLOMEUS - SAVAGES'}, count = 1000},
			[4] = {class = {'BARTOLOMEUS - DZIKUSY', 'BARTOLOMEUS - SAVAGES'}, count = 1500},
			[4] = {class = {'BARTOLOMEUS - DZIKUSY', 'BARTOLOMEUS - SAVAGES'}, count = 2000}}
	end
	
	return {}
end

m_TaskFunctions.execute = function(list, size)
	if size == 0 then
		return m_TaskFunctions.destroy()
	end
	
	if not m_TaskList.window then
		m_TaskList.window = g_ui.displayUI('game_task')
		m_TaskList.taskList = m_TaskList.window:getChildById('taskList')
		m_TaskList.monsterList = m_TaskList.window:getChildById('monsterList')
		m_TaskList.taskGroup = m_TaskList.window:getChildById('taskGroup')
		m_TaskList.itemList = m_TaskList.window:getChildById('itemList')
		m_TaskList.items = m_TaskList.itemList:getChildById('items')
		m_TaskList.taskPanel = m_TaskList.window:getChildById('taskPanel')
		m_TaskList.experience = m_TaskList.taskPanel:getChildById('experience')
		m_TaskList.bossName = m_TaskList.taskPanel:getChildById('bossName')
		
	else
		m_TaskList.currentSelected = nil
		m_TaskList.taskList:destroyChildren()
		m_TaskList.monsterList:destroyChildren()
	end
	
	local begin = true
	local taskList = size == #PROCLAY_TASK_LIST and PROCLAY_TASK_LIST or DANTVEN_TASK_LIST
	for i = 1, #list do
		local id, delay = unpack(list[i])
		
		local v = taskList[id]
		if v then
			local mType = modules.game_lookat.m_LookAtFunctions.getMonsterByName(v.boss)
			if not mType then
				mType = modules.game_lookat.m_LookAtFunctions.getMonsterByName(v.list[1])
			end
			
			if mType then
				local widget = g_ui.createWidget('CreatureClass', m_TaskList.taskList)
				local name = v.class[m_TaskFunctions.language]
				widget:getChildById('creature'):setOutfit(mType.look)
				widget:getChildById('name'):setText(name)
				widget:getChildById('description'):setText(tr('Required kills') .. ': ' .. v.count)
				
				widget.name = name
				widget.taskId = i
				widget.bossName = v.boss
				widget.itemList = v.rewards
				widget.experience = v.experience
				widget.creatureList = v.list
				widget.onMouseRelease = m_TaskFunctions.select
				if delay == 0 and begin then
					m_TaskFunctions.select(widget)
					begin = false
				elseif delay > 0 then
					widget:setEnabled(false)
					
					local tmpWidget = widget:getChildById('block')
					tmpWidget:show()
					
					local temp = os.date("*t", os.time() + delay)
					tmpWidget:setText(temp.hour .. ':' .. temp.min .. '\n' .. temp.day .. '/' .. temp.month .. '/' .. temp.year)
				end
			end
		end
	end
end

m_TaskFunctions.cancel = function()
	local msg = OutputMessage.create()
	msg:addU8(79)
	msg:addU16(0)
	g_game.getProtocolGame():send(msg)
	
	m_TaskFunctions.destroy()
end

m_TaskFunctions.accept = function()
	if not m_TaskList.currentSelected then
		return true
	end
	
	local msg = OutputMessage.create()
	msg:addU8(79)
	msg:addU16(m_TaskList.currentSelected.taskId)
	g_game.getProtocolGame():send(msg)
	
	m_TaskFunctions.destroy()
end