ITEM_BRONZE_CRATE = 13184
ITEM_SILVER_CRATE = 13185
PROCLAY_TASK_LIST = {
			-- actionID = 9273
			
			-- *
			[1] = { class = {'BANDYCI', 'BANDITS'}, list = {'outcast', 'outcast bleed', 'outlaw', 'outlaw bleed', 'poacher', 'slinger', 'slinger bleed', 'valkyrie', 'villain', 'villain bleed', 'amazon', 'assasin', 'hunter'},
					experience = 16000, count = 650, boss = 'Nemrod', difficulty = '*',
					rewards = {13754, 4, ITEMCLASS_PERFECT, 13754, 1, ITEMCLASS_LEGENDARY, ITEM_BRONZE_CRATE, 1, ITEMCLASS_NONE}, 
					fromPos = {x = 1545, y = 988, z = 7}, toPos = {x = 1560, y = 986, z = 8}, centerPos = {x = 1563, y = 986, z = 8},
					sizeX = 5, sizeY = 4 },
			
			[2] = { class = {'ELFY', 'ELVES'}, list = {'elf', 'elf arcanist', 'elf scout'},
					experience = 10000, count = 500, boss = 'Wrath of Nature', difficulty = '*',
					rewards = {2143, 4, ITEMCLASS_PERFECT, 2143, 1, ITEMCLASS_LEGENDARY, 5809, 2, ITEMCLASS_PERFECT, ITEM_BRONZE_CRATE, 1, ITEMCLASS_NONE}, 
					fromPos = {x = 971, y = 1135, z = 6}, toPos = {x = 965, y = 1128, z = 7}, centerPos = {x = 963, y = 1131, z = 7},
					sizeX = 8, sizeY = 7 },
			
			[3] = { class = {'TROLLE', 'TROLLS'}, list = {'swamp troll', 'troll bleed', 'troll champion bleed', 'troll champion fire', 'troll champion ice', 'troll champion leaf', 'troll champion shadow', 'troll fire', 'troll ice', 'troll leaf', 'troll shadow'},
					experience = 7000, count = 500, boss = 'Ellort', difficulty = '*',
					rewards = {13754, 2, ITEMCLASS_PERFECT, 13706, 2, ITEMCLASS_PERFECT, 5925, 2, ITEMCLASS_PERFECT, 14175, 2, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1411, y = 992, z = 6}, toPos = {x = 1387, y = 1016, z = 8}, centerPos = {x = 1390, y = 1023, z = 8},
					sizeX = 10, sizeY = 8 },
			
			[4] = { class = {'GOBLINY', 'GOBLINS'}, list = {'goblin assassin', 'goblin scavenger', 'goblin'},
					experience = 1500, count = 500, boss = 'Demonplague Consumer', difficulty = '*',
					rewards = {5925, 2, ITEMCLASS_PERFECT, 13706, 2, ITEMCLASS_PERFECT, 13155, 2, ITEMCLASS_PERFECT, ITEM_BRONZE_CRATE, 1, ITEMCLASS_NONE},
					fromPos = {x = 1313, y = 981, z = 9}, toPos = {x = 1351, y = 965, z = 8}, centerPos = {x = 1358, y = 963, z = 8},
					sizeX = 8, sizeY = 6 },
			
			[5] = { class = {'SZCZURAKI', 'CORYMS'}, list = {'corym charlatan', 'corym skirmisher', 'corym vanguard'},
					experience = 9000, count = 950, boss = 'Voltar The Thief', difficulty = '*',
					rewards = {13707, 1, ITEMCLASS_LEGENDARY, 13754, 4, ITEMCLASS_PERFECT, 5880, 1, ITEMCLASS_LEGENDARY, 2157, 2, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1409, y = 882, z = 8}, toPos = {x = 1415, y = 888, z = 8}, centerPos = {x = 1420, y = 886, z = 8},
					sizeX = 6, sizeY = 5 },
			
			-- **
			[6] = { class = {'MAGOWIE', 'MAGES'}, list = {'dark magician', 'dark apprentice', 'witch', 'stalker', 'monk'},
					experience = 40000, count = 350, boss = 'Forbidden Hunger Mage', difficulty = '**',
					rewards = {5809, 4, ITEMCLASS_PERFECT, 5809, 1, ITEMCLASS_LEGENDARY, 5892, 1, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1636, y = 825, z = 5}, toPos = {x = 1654, y = 807, z = 6}, centerPos = {x = 1654, y = 811, z = 6},
					sizeX = 9, sizeY = 5 },
			
			[7] = { class = {'ORKOWIE', 'ORCS'}, list = {'orc berserker', 'orc leader', 'orc rider', 'orc spearman', 'orc shaman', 'orc warlord', 'orc warrior', 'orc'},
					experience = 8000, count = 700, boss = 'Nobbu', difficulty = '**',
					rewards = {13754, 4, ITEMCLASS_PERFECT, 13754, 1, ITEMCLASS_LEGENDARY, 13706, 4, ITEMCLASS_PERFECT, 13706, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1175, y = 806, z = 6}, toPos = {x = 1185, y = 807, z = 8}, centerPos = {x = 1185, y = 813, z = 8},
					sizeX = 5, sizeY = 8 },
			
			[8] = { class = {'MINOTAURY', 'MINOTAURS'}, list = {'minotaur archer', 'minotaur guard', 'minotaur mage', 'minotaur'},
					experience = 4000, count = 600, boss = 'Wounded Moothan', difficulty = '**',
					rewards = {13155, 1, ITEMCLASS_LEGENDARY, 13706, 4, ITEMCLASS_PERFECT, ITEM_BRONZE_CRATE, 1, ITEMCLASS_NONE}, 
					fromPos = {x = 1506, y = 953, z = 10}, toPos = {x = 1514, y = 955, z = 10}, centerPos = {x = 1521, y = 956, z = 10},
					sizeX = 7, sizeY = 6 },
			
			[9] = { class = {'KRASNOLUDY', 'DWARVES'}, list = {'dwarf geomancer', 'dwarf guard', 'dwarf soldier', 'dwarf'},
					experience = 8000, count = 450, boss = 'Durin', difficulty = '**',
					rewards = {5880, 4, ITEMCLASS_PERFECT, 5880, 1, ITEMCLASS_LEGENDARY, 11693, 2, ITEMCLASS_PERFECT},
					fromPos = {x = 1564, y = 873, z = 8}, toPos = {x = 1562, y = 856, z = 8}, centerPos = {x = 1562, y = 861, z = 8},
					sizeX = 7, sizeY = 6 },
			
			[10] = { class = {'ELEMENTALI�CI', 'ELEMENTALISTS'}, list = {'teracian'},
					experience = 40000, count = 1250, boss = 'Illusionary Hypnotizer-lurker', difficulty = '**',
					rewards = {5809, 5, ITEMCLASS_PERFECT, 5809, 2, ITEMCLASS_LEGENDARY, 17941, 3, ITEMCLASS_PERFECT, 17941, 1, ITEMCLASS_LEGENDARY},
					fromPos = {x = 1146, y = 1074, z = 7}, toPos = {x = 1248, y = 1144, z = 9}, centerPos = {x = 1247, y = 1150, z = 9},
					sizeX = 14, sizeY = 14 },
					
			-- ***
			[11] = { class = {'KR�LEWSKIE KRASNOLUDY', 'ROYAL DWARVES'},  list = {'lost husher', 'lost thrower', 'lost basher'},
					experience = 9000, count = 1500, boss = 'Throer', difficulty = '***',
					rewards = {9971, 4, ITEMCLASS_PERFECT, 9971, 1, ITEMCLASS_LEGENDARY, 11693, 8, ITEMCLASS_PERFECT, 11693, 2, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1094, y = 902, z = 13}, toPos = {x = 1097, y = 899, z = 13}, centerPos = {x = 1106, y = 899, z = 13},
					sizeX = 10, sizeY = 6 },
			
			[12] = { class = {'DZIKUSY', 'SAVAGES'}, list = {'savage female', 'savage hunter male', 'savage mage female', 'savage male'},
					experience = 20000, count = 800, boss = 'Hand Faerie', difficulty = '***',
					rewards = {2157, 4, ITEMCLASS_PERFECT, 2157, 1, ITEMCLASS_LEGENDARY, 5878, 2, ITEMCLASS_PERFECT, 5887, 2, ITEMCLASS_PERFECT},
					fromPos = {x = 848, y = 1055, z = 7}, toPos = {x = 856, y = 1062, z = 7}, centerPos = {x = 859, y = 1066, z = 7},
					sizeX = 8, sizeY = 7 },
			
			[13] = { class = {'PIRACI', 'PIRATES'}, list = {'pirate buccaneer', 'pirate corsair', 'pirate cutthroat', 'pirate marauder'},
					experience = 45000, count = 1000, boss = 'Captain Hook', difficulty = '***',
					rewards = {11694, 4, ITEMCLASS_PERFECT, 11694, 1, ITEMCLASS_LEGENDARY, 2157, 4, ITEMCLASS_PERFECT, 2143, 2, ITEMCLASS_LEGENDARY},
					fromPos = {x = 1036, y = 916, z = 6}, toPos = {x = 1030, y = 898, z = 7}, centerPos = {x = 1031, y = 901, z = 7},
					sizeX = 5, sizeY = 6 },
			
			[14] = { class = {'KR�LEWSKIE MINOTAURY', 'ROYAL MINOTAURS'}, list = {'royal minotaur hunter', 'royal minotaur mage', 'royal minotaur warrior', 'royal minotaur'},
					experience = 12000, count = 900, boss = 'Mar\'Tal The Leader', difficulty = '***',
					rewards = {5878, 4, ITEMCLASS_PERFECT, 5878, 1, ITEMCLASS_LEGENDARY, 11693, 2, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1559, y = 886, z = 13}, toPos = {x = 1565, y = 899, z = 13}, centerPos = {x = 1567, y = 902, z = 13},
					sizeX = 6, sizeY = 5 },
			
			[15] = { class = {'NEKROMANCI', 'NECROMANCERS'}, list = {'necromancer', 'fallen island warlock', 'trustee of darkness'},
					experience = 19000, count = 800, boss = 'Ner\'zhul', difficulty = '***',
					rewards = {17941, 2, ITEMCLASS_LEGENDARY, 11694, 2, ITEMCLASS_LEGENDARY, 2143, 5, ITEMCLASS_LEGENDARY, 2144, 5, ITEMCLASS_LEGENDARY},
					fromPos = {x = 1450, y = 727, z = 5}, toPos = {x = 1463, y = 765, z = 8}, centerPos = {x = 1463, y = 768, z = 8},
					sizeX = 6, sizeY = 5 },
			
			-- ****
			[16] = { class = {'CZARNOKSIʯNICY', 'WARLOCKS'}, list = {'warlock', 'infernalist'},
					experience = 40000, count = 1000, boss = 'Infernal Oozer-orb', difficulty = '****',
					rewards = {14504, 2, ITEMCLASS_PERFECT, 11694, 1, ITEMCLASS_LEGENDARY, 9971, 1, ITEMCLASS_LEGENDARY, 2144, 4, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1071, y = 681, z = 10}, toPos = {x = 1131, y = 713, z = 12}, centerPos = {x = 1135, y = 713, z = 12},
					sizeX = 6, sizeY = 4 },
			
			[17] = { class = {'TEMPLARIUSZE', 'TEMPLARS'}, list = {'knight templar', 'commodore of the kingdom'},
					experience = 15000, count = 1300, boss = 'Goldmoon', difficulty = '****',
					rewards = {13163, 4, ITEMCLASS_PERFECT, 13163, 1, ITEMCLASS_LEGENDARY, 13164, 2, ITEMCLASS_PERFECT, 13165, 2, ITEMCLASS_PERFECT},
					fromPos = {x = 1627, y = 694, z = 7}, toPos = {x = 1692, y = 769, z = 7}, centerPos = {x = 1691, y = 777, z = 7},
					sizeX = 16, sizeY = 13 },
			
			-- *****
			[18] = { class = {'WI�KSZE SMOKI', 'GREATER DRAGONS'}, list = {'dark dragon', 'ghastly dragon', 'sapphire dragon', 'young sapphire dragon', 'undead dragon', 'dark draptor'},
					experience = 40000, count = 1500, boss = 'Old Dragon Mother', difficulty = '*****',
					rewards = {14493, 4, ITEMCLASS_PERFECT, 14493, 1, ITEMCLASS_LEGENDARY, 17940, 4, ITEMCLASS_PERFECT, 17940, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1377, y = 729, z = 9}, toPos = {x = 1372, y = 725, z = 9}, centerPos = {x = 1372, y = 722, z = 9},
					sizeX = 7, sizeY = 5 },
			}
			
DANTVEN_TASK_LIST = {
			-- actionID = 9274
			-- *
			[1] = { class = {'ZWIERZ�TA', 'ANIMALS'}, list = {'bat', 'bear', 'starving bear', 'black sheep', 'sheep', 'blue butterfly', 'pink butterfly', 'red butterfly', 'yellow butterfly', 'boar', 'starving boar', 'cave rat', 'rat', 'centipede', 'chicken', 'azure frog', 'coral frog', 'crimson frog', 'green frog', 'orchid frog', 'crab', 'crocodile', 'deer', 'giant crab', 'hyaena', 'kongra', 'merlkin', 'sibang', 'lion', 'panda', 'parrot', 'penguin', 'pig', 'polar bear', 'rabbit', 'silver rabbit', 'stampor', 'tortoise', 'tiger', 'wolf', 'starving wolf', 'toad'},
					experience = 3000, count = 500, boss = 'Animalus', difficulty = '*',
					rewards = {13706, 4, ITEMCLASS_PERFECT, 13706, 1, ITEMCLASS_LEGENDARY, 13155, 4, ITEMCLASS_PERFECT, ITEM_BRONZE_CRATE, 1, ITEMCLASS_NONE}, 
					fromPos = {x = 1613, y = 787, z = 7}, toPos = {x = 1619, y = 774, z = 7}, centerPos = {x = 1620, y = 779, z = 7},
					sizeX = 8, sizeY = 6 },
			
			[2] = { class = {'POCZWARY', 'ROTWORMS'}, list = {'dirtbite', 'rotworm', 'rotworm queen', 'carrion worm'},
					experience = 5000, count = 400, boss = 'Old Queen', difficulty = '*',
					rewards = {14175, 4, ITEMCLASS_PERFECT, 14175, 1, ITEMCLASS_LEGENDARY, 13706, 4, ITEMCLASS_PERFECT, ITEM_BRONZE_CRATE, 1, ITEMCLASS_NONE}, 
					fromPos = {x = 1543, y = 918, z = 7}, toPos = {x = 1527, y = 853, z = 6}, centerPos = {x = 1535, y = 853, z = 6},
					sizeX = 10, sizeY = 8 },
			
			[3] = { class = {'WʯE', 'SNAKES'}, list = {'snake', 'cobra'},
					experience = 4000, count = 100, boss = 'Snaker', difficulty = '*',
					rewards = {2143, 2, ITEMCLASS_PERFECT, 2144, 2, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1456, y = 938, z = 7}, toPos = {x = 1441, y = 897, z = 9}, centerPos = {x = 1455, y = 898, z = 9},
					sizeX = 15, sizeY = 12 },
			
			[4] = { class = {'INSEKTY', 'INSECTS'}, list = {'wasp', 'spider', 'poison spider', 'scorpion', 'larva', 'bug', 'sandcrawler', 'aggressive insect', 'dragonfly'},
					experience = 1500, count = 200, boss = 'Waspero', difficulty = '*',
					rewards = {13707, 4, ITEMCLASS_PERFECT, 13707, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1414, y = 970, z = 5}, toPos = {x = 1450, y = 962, z = 8}, centerPos = {x = 1456, y = 962, z = 8},
					sizeX = 8, sizeY = 9 },
			
			-- **
			[5] = { class = {'D�INY', 'DJINNS'}, list = {'blue djinn', 'marid', 'red djinn', 'efreet', 'green djinn', 'afrytt'},
					experience = 2500, count = 600, boss = 'All a Din', difficulty = '**',
					rewards = {5809, 4, ITEMCLASS_PERFECT, 5809, 1, ITEMCLASS_LEGENDARY, 2143, 4, ITEMCLASS_PERFECT, 2143, 1, ITEMCLASS_LEGENDARY} },
			
			[6] = { class = {'W�ADCY KO�CI', 'BONELORDS'}, list = {'bonelord', 'young bonelord', 'elder bonelord', 'arch bonelord'},
					experience = 4500, count = 800, boss = 'Puppet Master', difficulty = '**',
					rewards = {5809, 4, ITEMCLASS_PERFECT, 2143, 4, ITEMCLASS_PERFECT, 5809, 1, ITEMCLASS_LEGENDARY, 2157, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1073, y = 1052, z = 7}, toPos = {x = 1048, y = 983, z = 8}, centerPos = {x = 1053, y = 984, z = 8},
					sizeX = 11, sizeY = 10 },
			
			[7] = { class = {'GIGANCI', 'GIANTS'}, list = {'cyclops', 'cyclops drone', 'cyclops smith', 'cyclops elite', 'frost giant', 'frost giantess', 'stone golem'},
					experience = 6000, count = 900, boss = 'Cyclopus', difficulty = '**',
					rewards = {5892, 1, ITEMCLASS_PERFECT, 5887, 1, ITEMCLASS_PERFECT, 13707, 2, ITEMCLASS_LEGENDARY, 5880, 2, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1406, y = 1069, z = 9}, toPos = {x = 1410, y = 1056, z = 9}, centerPos = {x = 1415, y = 1059, z = 9},
					sizeX = 7, sizeY = 5 },
			
			[8] = { class = {'SZLAMY', 'SLIMES'}, list = {'slime', 'tar blob', 'mercury blob', 'acid blob', 'aqua blob'},
					experience = 4000, count = 300, boss = 'Shlamer', difficulty = '**',
					rewards = {5809, 2, ITEMCLASS_PERFECT, 13707, 2, ITEMCLASS_PERFECT} },
			
			[9] = { class = {'STAWONOGI', 'ARTHROPODS'}, list = {'tarantula', 'swarmer', 'scarab', 'lancer beetle'},
					experience = 6000, count = 500, boss = 'Tarantulos', difficulty = '**',
					rewards = {13707, 4, ITEMCLASS_PERFECT, 13707, 1, ITEMCLASS_LEGENDARY, 13706, 4, ITEMCLASS_PERFECT, 13706, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1469, y = 837, z = 9}, toPos = {x = 1457, y = 835, z = 9}, centerPos = {x = 1453, y = 836, z = 9},
					sizeX = 5, sizeY = 4 },
			
			-- ***
			[10] = { class = {'PAJ�CZAKI', 'ARACHNIDS'}, list = {'sandstone scorpion', 'giant spider', 'dustrunner', 'brimstone bug', 'ancient scarab', 'wailing widow', 'crystal spider'},
					experience = 13000, count = 1000, boss = 'Spiderus', difficulty = '***',
					rewards = {13706, 12, ITEMCLASS_PERFECT, 13706, 3, ITEMCLASS_LEGENDARY, 5878, 2, ITEMCLASS_PERFECT, 11694, 2, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1067, y = 1055, z = 7}, toPos = {x = 1486, y = 845, z = 9}, centerPos = {x = 1491, y = 844, z = 9},
					sizeX = 11, sizeY = 10 },
			
			[11] = { class = {'NIEUMARLI', 'UNDEADS'}, list = {'skeletor', 'undead cavebear', 'banshee', 'bonebeast', 'corpse eater', 'crypt shambler', 'demon skeleton', 'demon skeleton warrior', 'skeleton', 'skeleton warrior', 'frightening slime', 'ghost', 'ghoul', 'lich', 'mummy', 'nightstalker', 'wisp', 'zombie'},
					experience = 17000, count = 1200, boss = 'Legion', difficulty = '***',
					rewards = {5925, 12, ITEMCLASS_PERFECT, 5925, 3, ITEMCLASS_LEGENDARY, 5809, 2, ITEMCLASS_LEGENDARY, 11693, 2, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1429, y = 799, z = 5}, toPos = {x = 1464, y = 781, z = 8}, centerPos = {x = 1475, y = 783, z = 8},
					sizeX = 16, sizeY = 10 },
			
			[12] = { class = {'WAMPIRY', 'VAMPIRES'}, list = {'vampire', 'vampire bride', 'vampire count', 'vampire viscount', 'vampire lord'},
					experience = 12000, count = 1000, boss = 'Alucard', difficulty = '***',
					rewards = {11694, 4, ITEMCLASS_PERFECT, 11694, 1, ITEMCLASS_LEGENDARY, 5892, 4, ITEMCLASS_PERFECT, 5892, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1500, y = 699, z = 7}, toPos = {x = 1448, y = 730, z = 8}, centerPos = {x = 1453, y = 730, z = 8},
					sizeX = 11, sizeY = 11 },
			
			[13] = { class = {'JASZCZURY', 'LIZARDS'}, list = {'lizard chosen', 'lizard dragon priest', 'lizard high guard', 'lizard legionnaire', 'lizard sentinel', 'lizard snakecharmer', 'lizard templar', 'lizard zaogun', 'primeval'},
					experience = 15500, count = 1200, boss = 'The Chosen One', difficulty = '***',
					rewards = {5878, 4, ITEMCLASS_PERFECT, 5878, 1, ITEMCLASS_LEGENDARY, 5887, 4, ITEMCLASS_PERFECT, 5887, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1374, y = 851, z = 11}, toPos = {x = 1340, y = 850, z = 11}, centerPos = {x = 1337, y = 860, z = 11},
					sizeX = 14, sizeY = 13 },
			
			[14] = { class = {'SMOKI', 'DRAGONS'}, list = {'dragon hatchling', 'dragon lord hatchling', 'red dragon', 'dragon of storm', 'green dragon', 'frost dragon hatchling', 'frost dragon', 'swamply dragon', 'young swamply dragon', 'wyvern'},
					experience = 40000, count = 3000, boss = 'Old Dragon Mother', difficulty = '***',
					rewards = {17941, 6, ITEMCLASS_PERFECT, 17941, 2, ITEMCLASS_LEGENDARY, 9971, 6, ITEMCLASS_PERFECT, 9971, 2, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1415, y = 709, z = 7}, toPos = {x = 1372, y = 725, z = 9}, centerPos = {x = 1372, y = 722, z = 9},
					sizeX = 7, sizeY = 5 },
			
			[15] = { class = {'ZMUTOWANE', 'MUTATEDS'}, list = {'infected prisoner', 'mutated tiger'},
					experience = 3500, count = 500, boss = 'X\'men\'us', difficulty = '***',
					rewards = {13706, 4, ITEMCLASS_PERFECT, 13706, 1, ITEMCLASS_LEGENDARY, 13155, 2, ITEMCLASS_LEGENDARY, 13155, 6, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1376, y = 850, z = 9}, toPos = {x = 1390, y = 845, z = 9}, centerPos = {x = 1389, y = 850, z = 9},
					sizeX = 6, sizeY = 6 },
			
			[16] = { class = {'ZIEMNE �YWIO�AKI', 'GEO ELEMENTALS'}, list = {'bog raider', 'carniphila', 'defiler', 'earth elemental', 'plague bearer', 'muddy earth elemental', 'massive earth elemental', 'jagged earth elemental', 'earth destroyer', 'ent', 'forest spirit', 'gargoyle', 'golem', 'nepethes', 'treebeard', 'young nepethes', 'leaf golem'},
					experience = 5000, count = 700, boss = 'Giant Nepethes', difficulty = '***',
					rewards = {17941, 2, ITEMCLASS_PERFECT, 2157, 1, ITEMCLASS_LEGENDARY, 5887, 2, ITEMCLASS_PERFECT, 11693, 2, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1321, y = 1055, z = 12}, toPos = {x = 1339, y = 1072, z = 12}, centerPos = {x = 1350, y = 1074, z = 12},
					sizeX = 13, sizeY = 12 },
			
			[17] = { class = {'OGNITE �YWIO�AKI', 'PYRO ELEMENTALS'}, list = {'fire elemental', 'blazing fire elemental', 'flicker of death', 'massive fire elemental', 'blistering fire elemental', 'spark of the phoenix', 'hellfire fighter'},
					experience = 7700, count = 750, boss = 'Hellfire Warrior', difficulty = '***',
					rewards = {9971, 4, ITEMCLASS_PERFECT, 9971, 1, ITEMCLASS_LEGENDARY, 5809, 12, ITEMCLASS_PERFECT, 2157, 12, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1372, y = 696, z = 10}, toPos = {x = 1397, y = 686, z = 10}, centerPos = {x = 1396, y = 694, z = 10},
					sizeX = 11, sizeY = 11 },
			
			[18] = { class = {'ELEKTRO �YWIO�AKI', 'ELE ELEMENTALS'}, list = {'energy elemental', 'charged energy elemental', 'energy suppressor', 'massive energy elemental', 'overcharged energy elemental', 'electric glimmer'},
					experience = 5500, count = 700, boss = 'Electro', difficulty = '***',
					rewards = {2143, 8, ITEMCLASS_PERFECT, 2143, 2, ITEMCLASS_LEGENDARY, 9971, 1, ITEMCLASS_PERFECT}, 
					fromPos = {x = 1244, y = 749, z = 9}, toPos = {x = 1246, y = 740, z = 10}, centerPos = {x = 1246, y = 748, z = 10},
					sizeX = 11, sizeY = 10 },
			
			-- ****
			[19] = { class = {'POMIOTY PIEKIE�', 'HELLSPAWNS'}, list = {'hellspawn', 'nightmare', 'nightmare scion', 'diabolic imp', 'black devil', 'spectre'},
					experience = 40000, count = 2000, boss = 'Harbinger', difficulty = '****',
					rewards = {17941, 4, ITEMCLASS_PERFECT, 17941, 1, ITEMCLASS_LEGENDARY, 14504, 1, ITEMCLASS_LEGENDARY, ITEM_SILVER_CRATE, 2, ITEMCLASS_NONE},
					fromPos = {x = 1203, y = 1045, z = 14}, toPos = {x = 1215, y = 1048, z = 14}, centerPos = {x = 1228, y = 1045, z = 14},
					sizeX = 14, sizeY = 14 },
			[20] = { class = {'PODWODNE', 'UNDERWATERS'}, list = {'massive water elemental', 'water elemental', 'roaring water elemental', 'slick water elemental', 'deepling guard', 'deepling spellsinger', 'deepling warrior'},
					experience = 15000, count = 700, boss = 'Aqualius', difficulty = '****',
					rewards = {14493, 2, ITEMCLASS_PERFECT, 14504, 2, ITEMCLASS_PERFECT, 17941, 1, ITEMCLASS_LEGENDARY, 9971, 1, ITEMCLASS_LEGENDARY},
					fromPos = {x = 1221, y = 862, z = 10}, toPos = {x = 1222, y = 848, z = 10}, centerPos = {x = 1226, y = 841, z = 10},
					sizeX = 11, sizeY = 9 },
			
			[21] = { class = {'UPADLI', 'FALLENS'}, list = {'blightwalker', 'betrayed wraith', 'grim reaper', 'lost soul', 'souleater'},
					experience = 27000, count = 1800, boss = 'Last Breath', difficulty = '****',
					rewards = {17940, 3, ITEMCLASS_PERFECT, 17940, 1, ITEMCLASS_LEGENDARY, 11694, 2, ITEMCLASS_LEGENDARY, ITEM_SILVER_CRATE, 2, ITEMCLASS_NONE}, 
					fromPos = {x = 1557, y = 843, z = 14}, toPos = {x = 1580, y = 945, z = 11}, centerPos = {x = 1578, y = 934, z = 11},
					sizeX = 15, sizeY = 17 },
			
			[22] = { class = {'WʯOWE STWORY', 'SERPENTS'}, list = {'hydra', 'medusa', 'sandstone cobra', 'serpent spawn'},
					experience = 9500, count = 1200, boss = 'Steno', difficulty = '****',
					rewards = {13164, 2, ITEMCLASS_PERFECT, 17940, 2, ITEMCLASS_PERFECT, 5887, 1, ITEMCLASS_LEGENDARY, 11694, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1331, y = 1069, z = 13}, toPos = {x = 1367, y = 1079, z = 13}, centerPos = {x = 1368, y = 1088, z = 13},
					sizeX = 10, sizeY = 10 },
			
			[23] = { class = {'TYTANI', 'TITANS'}, list = {'behemoth', 'cliff strider', 'ogre'},
					experience = 16000, count = 900, boss = 'Emerald Behemoth', difficulty = '****',
					rewards = {13163, 2, ITEMCLASS_PERFECT, 14493, 2, ITEMCLASS_PERFECT, 5887, 1, ITEMCLASS_LEGENDARY, 9971, 1, ITEMCLASS_LEGENDARY}, 
					fromPos = {x = 1621, y = 788, z = 9}, toPos = {x = 1610, y = 788, z = 9}, centerPos = {x = 1605, y = 784, z = 9},
					sizeX = 7, sizeY = 6 },
			
			-- *****
			[24] = { class = {'SMOKONY', 'DRAKENS'}, list = {'draken spellweaver', 'draken warmaster', 'draken elite', 'draken abomination'},
					experience = 18000, count = 3000, boss = 'Kiri Norol', difficulty = '*****',
					rewards = {14493, 2, ITEMCLASS_LEGENDARY, 14504, 2, ITEMCLASS_LEGENDARY, 14494, 1, ITEMCLASS_DAMAGED, 18046, 1, ITEMCLASS_DAMAGED}, 
					fromPos = {x = 1543, y = 605, z = 12}, toPos = {x = 1562, y = 627, z = 12}, centerPos = {x = 1567, y = 627, z = 12},
					sizeX = 6, sizeY = 4 },
			
			[25] = { class = {'DEMONY', 'DEMONS'}, list = {'plaguesmith', 'hand of cursed fate', 'fury', 'dark torturer', 'destroyer', 'yielothax', 'grimeleech', 'grimspirit'},
					experience = 40000, count = 2000, boss = 'Lucifer', difficulty = '****',
					rewards = {13165, 1, ITEMCLASS_LEGENDARY, 13164, 1, ITEMCLASS_LEGENDARY, 13163, 1, ITEMCLASS_LEGENDARY, 13166, 1, ITEMCLASS_DAMAGED} },
			
			[26] = { class = {'WI�KSZE DEMONY', 'GREATER DEMONS'}, list = {'chimera', 'demon', 'juggernaut', 'hellhound', 'arch devil', 'abomination creature'},
					experience = 40000, count = 2500, boss = 'Azazel', difficulty = '*****',
					rewards = {13165, 2, ITEMCLASS_LEGENDARY, 13164, 2, ITEMCLASS_LEGENDARY, 13163, 2, ITEMCLASS_LEGENDARY, 18046, 1, ITEMCLASS_NORMAL}, 
					fromPos = {x = 1874, y = 1362, z = 13}, toPos = {x = 1868, y = 1367, z = 13}, centerPos = {x = 1863, y = 1363, z = 13},
					sizeX = 8, sizeY = 12 },
			}