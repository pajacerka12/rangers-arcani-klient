m_HouseFunctions = {}
m_HouseList = {}

m_HouseFunctions.GUEST_LIST = 0
m_HouseFunctions.SUBOWNER_LIST = 1

m_HouseFunctions.GameServerUpdateBalance = 57
m_HouseFunctions.GameServerUpdateHouseList = 95

m_HouseFunctions.TOWN_CIRITH = 1
m_HouseFunctions.TOWN_MARAROKO = 10

m_HouseFunctions.townId = m_HouseFunctions.TOWN_CIRITH

m_HouseFunctions.range = {
	[m_HouseFunctions.TOWN_CIRITH] = {1, 2},
	[m_HouseFunctions.TOWN_MARAROKO] = {3, 5},
}

m_HouseFunctions.getSubownerList = function(listId)
	local text = ''
	for _, pid in pairs(m_HouseList.subOwnerList:getChildren()) do
		if listId == m_HouseFunctions.GUEST_LIST then
			if not pid:getChildById('icon'):isOn() then
				text = text .. pid:getText() .. '\n'
			end
		elseif listId == m_HouseFunctions.SUBOWNER_LIST then
			if pid:getChildById('icon'):isOn() then
				text = text .. pid:getText() .. '\n'
			end
		end
	end
	
	if listId == m_HouseFunctions.GUEST_LIST then
		m_HouseList.current.var[3] = text
	elseif listId == m_HouseFunctions.SUBOWNER_LIST then
		m_HouseList.current.var[4] = text
	end
	
	return text
end

m_HouseFunctions.updateWidget = function(parent)
	for id = 1, parent:getChildCount() do
		local pid = parent:getChildByIndex(id)
		pid.marginTop = pid:getMarginTop()
		pid.marginLeft = pid:getMarginLeft()
		pid.marginRight = pid:getMarginRight()
		pid.marginBottom = pid:getMarginBottom()
		
		pid.width = pid:getWidth()
		pid.height = pid:getHeight()
		
		for _, cid in pairs(pid:getChildren()) do
			cid.width = cid:getWidth()
			cid.height = cid:getHeight()
		end
	end
end

m_HouseFunctions.open = function()
	destroy()
	
	m_HouseList.window = g_ui.displayUI('game_house')
	m_HouseList.description = m_HouseList.window:getChildById('description')
	m_HouseList.name = m_HouseList.window:getChildById('name')
	m_HouseList.buy = m_HouseList.window:getChildById('buy')
	m_HouseList.manage = m_HouseList.window:getChildById('manage')
	m_HouseList.balance = m_HouseList.window:getChildById('balance')
	m_HouseList.managePanel = m_HouseList.window:getChildById('managePanel')
	m_HouseList.current = {}
	m_HouseList.subOwnerList = m_HouseList.managePanel:getChildById('subOwnerList')
	m_HouseList.newGuest = m_HouseList.managePanel:getChildById('newGuest')
	
	m_HouseList.maps = {
		m_HouseList.window:getChildById('cirith_1'),
		m_HouseList.window:getChildById('cirith_2'),
		m_HouseList.window:getChildById('mararoko_1'),
		m_HouseList.window:getChildById('mararoko_2'),
		m_HouseList.window:getChildById('mararoko_3'),
	}
	
	for _, v in pairs(m_HouseList.maps) do
		m_HouseFunctions.updateWidget(v)
		v.onMouseWheel = onMouseWheel
		
		if m_HouseFunctions.move then
			v:setZoom(m_HouseFunctions.zoom)
			v:moves(m_HouseFunctions.move)
		else
			v:zoomIn()
		end
	end
end

m_HouseFunctions.parseBalance = function(protocol, msg)
	local balance = msg:getU64()
	if m_HouseList.balance then
		m_HouseList.balance:setText('Balance: ' .. g_game.getMilharNumber(balance))
	end
	
	if modules.game_rewards then
		modules.game_rewards.m_CraftingFunctions.updateBalance(balance)
	end
	
	if modules.game_dungeons then
		modules.game_dungeons.m_DungeonFunctions.updateBalance(balance)
	end
	
	if modules.game_runes then
		modules.game_runes.m_RunesFunctions.updateBalance(balance)
	end
	
	if modules.game_character then
		modules.game_character.m_CharmsFunctions.updateBalance(balance)
	end
	
	if modules.game_market then
		modules.game_market.m_MarketFunctions.updateBalance(balance)
	end
end

m_HouseFunctions.parseHouseList = function(protocol, msg)
	m_HouseFunctions.townId = msg:getU8()
	
	local size = msg:getU16()
	if size == 0 then
		destroy()
		return true
	end
	
	m_HouseFunctions.open()
	m_HouseList.list = {}
	for i = 1, size do
		local name = msg:getString()
		local owner = msg:getString()
		local guestsList = msg:getString()
		local subOwnerList = msg:getString()
		local paid = msg:getString()
		local id = msg:getU32()
		local price = msg:getU32()
		local rent = msg:getU32()
		local size = msg:getU32()
		
		m_HouseList.list[name] = {id, owner, guestsList, subOwnerList, paid, price, rent, size}
		
		local v = m_HouseFunctions.range[m_HouseFunctions.townId]
		m_HouseList.maps[v[1]]:show()
		if owner ~= '' then
			for i = v[1], v[2] do
				local widget = m_HouseList.maps[i]:getChildById(name)
				if widget then
					widget:setOn(true)
					onHoverChange(widget, false)
				end
			end
		end
	end
end

m_HouseFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_HouseFunctions.GameServerUpdateHouseList, m_HouseFunctions.parseHouseList)
	ProtocolGame.registerOpcode(m_HouseFunctions.GameServerUpdateBalance, m_HouseFunctions.parseBalance)
end

m_HouseFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_HouseFunctions.GameServerUpdateHouseList, m_HouseFunctions.parseHouseList)
	ProtocolGame.unregisterOpcode(m_HouseFunctions.GameServerUpdateBalance, m_HouseFunctions.parseBalance)
end

function onLoad()
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	if g_game.isOnline() then
		m_HouseFunctions.registerProtocol()
	end
end

function onUnload()
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd
	})
	
	destroy()
	m_HouseFunctions.unregisterProtocol()
end

function onGameStart()
	m_HouseFunctions.registerProtocol()
end

function onGameEnd()
	destroy()
	m_HouseFunctions.unregisterProtocol()
end

function destroy()
	if m_HouseList.window then
		m_HouseFunctions.move = m_HouseList.maps[1]:getImageOffset()
		m_HouseFunctions.zoom = m_HouseList.maps[1].zoom
		
		m_HouseList.subOwnerList:destroyChildren()
		m_HouseList.window:destroy()
	end
	
	if m_HouseList.warning then
		m_HouseList.warning:destroy()
	end
	
	m_HouseList = {}
end

function select(self)
	local name = self:getId()
	if name == m_HouseList.current.id then
		return true
	end
	
	local list = {self}
	local v = m_HouseFunctions.range[m_HouseFunctions.townId]
	for i = v[1], v[2] do
		if m_HouseList.current.id then
			local tmpWidget = m_HouseList.maps[i]:getChildById(m_HouseList.current.id)
			if tmpWidget then
				tmpWidget:setChecked(false)
				onHoverChange(tmpWidget, false)
			end
		end
		
		local widget = m_HouseList.maps[i]:getChildById(name)
		if widget then
			table.insert(list, widget)
		end
	end
	
	local color
	if self:isOn() then
		if self:isChecked() then
			color = '#FF0D0088' -- red
		else
			color = '#FF8C0088' -- orange
		end
	elseif self:isChecked() then
		color = '#0073FF88' -- blue
	else
		color = '#FF00AA88' -- pink
	end
	
	for i = 1, #list do
		list[i]:setChecked(true)
		onHoverChange(list[i], false)
	end
	
	local var = m_HouseList.list[name]
	local id, owner, guestsList, subOwnerList, paid, price, rent, size = unpack(var)
	local description = tr('Owner') .. ': ' .. (owner == '' and tr('None') or owner)
	description = description .. '\n' .. tr('Size') .. ': ' .. size .. ' sqm'
	description = description .. '\n' .. tr('Monthly rent') .. ': ' .. rent .. ' ' .. tr('copper coins')
	description = description .. '\n' .. tr('Price') .. ': ' .. price .. ' ' .. tr('copper coins')
	description = description .. '\n' .. tr('Paid until') .. ': ' .. (owner == '' and 'n/a' or paid)
	
	m_HouseList.current = {var = var, id = name}
	m_HouseList.managePanel:hide()
	m_HouseList.name:setText(name)
	m_HouseList.manage:setOn(false)
	m_HouseList.description:setText(description)
	m_HouseList.buy:setEnabled(owner == '')
	m_HouseList.manage:setEnabled(owner == g_game.getLocalPlayer():getName())
end

function sendMessage(text)
	if not m_HouseList.warning then
		m_HouseList.warning = g_ui.displayUI('popup_message')
	end
	
	m_HouseList.window:setEnabled(false)
	m_HouseList.warning:getChildById('description'):setText(text)
end

function subOwner(self)
	self:setOn(not self:isOn())
end

function accept()
	if m_HouseList.warning then
		m_HouseList.warning:destroy()
		m_HouseList.warning = nil
	end
	
	m_HouseList.window:setEnabled(true)
	m_HouseList.window:focus()
end

function manage(self, mousePos, button)
	if self:isOn() then
		m_HouseList.managePanel:hide()
	else
		m_HouseList.managePanel:show()
		m_HouseList.subOwnerList:destroyChildren()
	
		local text = m_HouseList.current.var[4]
		if text and text ~= '' then
			text = text:explode('\n')
			for i = 1, #text do
				if text[i] ~= '' then
					local widget = g_ui.createWidget('SubOwnerMember', m_HouseList.subOwnerList)
					widget:setParent(m_HouseList.subOwnerList)
					widget:setText(text[i])
					widget:getChildById('icon'):setOn(true)
				end
			end
		end
	
		local text = m_HouseList.current.var[3]
		if text and text ~= '' then
			text = text:explode('\n')
			for i = 1, #text do
				if text[i] ~= '' then
					local widget = g_ui.createWidget('SubOwnerMember', m_HouseList.subOwnerList)
					widget:setParent(m_HouseList.subOwnerList)
					widget:setText(text[i])
				end
			end
		end
	end
	
	self:setOn(not self:isOn())
end

function remove(self)
	self:destroy()
end

function add()
	local name = m_HouseList.newGuest:getText()
	if name == '' then
		return true
	end
	
	m_HouseList.newGuest:clearText()
	for _, pid in pairs(m_HouseList.subOwnerList:getChildren()) do
		if pid:getText() == name then
			return true
		end
	end
	
	local widget = g_ui.createWidget('SubOwnerMember', m_HouseList.subOwnerList)
	widget:setParent(m_HouseList.subOwnerList)
	widget:setText(name)
end

function buy(self, mousePos, button)
	local msg = OutputMessage.create()
	msg:addU8(71)
	msg:addU8(0)
	msg:addU32(m_HouseList.current.var[1])
	g_game.getProtocolGame():send(msg)
end

function sell(self, mousePos, button)
	local msg = OutputMessage.create()
	msg:addU8(71)
	msg:addU8(1)
	msg:addU32(m_HouseList.current.var[1])
	g_game.getProtocolGame():send(msg)
end

function update()
	local msg = OutputMessage.create()
	msg:addU8(71)
	msg:addU8(2)
	msg:addU32(m_HouseList.current.var[1])
	msg:addString(m_HouseFunctions.getSubownerList(m_HouseFunctions.GUEST_LIST))
	msg:addString(m_HouseFunctions.getSubownerList(m_HouseFunctions.SUBOWNER_LIST))
	g_game.getProtocolGame():send(msg)
end

function zoomOut()
	for _, v in pairs(m_HouseList.maps) do
		v:zoomOut()
	end
end

function zoomIn()
	for _, v in pairs(m_HouseList.maps) do
		v:zoomIn()
	end
end

function floorUp()
	if m_HouseList.maps[2]:isVisible() or m_HouseList.maps[5]:isVisible() then
		return true
	end
	
	local fromId, toId
	if m_HouseList.maps[1]:isVisible() then
		fromId = 1
		toId = 2
	elseif m_HouseList.maps[3]:isVisible() then
		fromId = 3
		toId = 4
	elseif m_HouseList.maps[4]:isVisible() then
		fromId = 4
		toId = 5
	end
	
	if fromId and toId then
		m_HouseList.maps[fromId]:hide()
		m_HouseList.maps[toId]:show()
		
		m_HouseList.maps[toId]:setZoom(m_HouseList.maps[fromId].zoom)
		m_HouseList.maps[toId]:moves(m_HouseList.maps[fromId]:getImageOffset())
	end
end

function floorDown()
	if m_HouseList.maps[1]:isVisible() or m_HouseList.maps[3]:isVisible() then
		return true
	end
	
	local fromId, toId
	if m_HouseList.maps[2]:isVisible() then
		fromId = 2
		toId = 1
	elseif m_HouseList.maps[5]:isVisible() then
		fromId = 5
		toId = 4
	elseif m_HouseList.maps[4]:isVisible() then
		fromId = 4
		toId = 3
	end
	
	if fromId and toId then
		m_HouseList.maps[fromId]:hide()
		m_HouseList.maps[toId]:show()
		
		m_HouseList.maps[toId]:setZoom(m_HouseList.maps[fromId].zoom)
		m_HouseList.maps[toId]:moves(m_HouseList.maps[fromId]:getImageOffset())
	end
end

function onMouseWheel(self, mousePos, direction)
	local keyboardModifiers = g_keyboard.getModifiers()
	if direction == MouseWheelUp and keyboardModifiers == KeyboardNoModifier then
		zoomIn()
	elseif direction == MouseWheelDown and keyboardModifiers == KeyboardNoModifier then
		zoomOut()
	elseif direction == MouseWheelDown and keyboardModifiers == KeyboardCtrlModifier then
		floorUp(1)
	elseif direction == MouseWheelUp and keyboardModifiers == KeyboardCtrlModifier then
		floorDown(1)
	end
end

function onMouseRelease(self, mousePos, button)
	select(self)
end

function onMousePress(self, mousePos, button)
	local color
	if self:isOn() then
		if self:isChecked() then
			color = '#9A080088'
		else
			color = '#9A540088'
		end
	elseif self:isChecked() then
		color = '#00459A88'
	else
		color = '#9A006688'
	end
	
	for _, pid in pairs(self:getChildren()) do
		pid:setBackgroundColor(color)
	end
	
	self:setImageColor(color)
end

function onHoverChange(self, hovered)
	local color
	if hovered then
		if self:isOn() then
			if self:isChecked() then
				color = '#FF6E6788'
			else
				color = '#FFBA6788'
			end
		elseif self:isChecked() then
			color = '#80B9FF88'
		else
			color = '#FF67CC88'
		end
	else
		if self:isOn() then
			if self:isChecked() then
				color = '#FF0D0088'
			else
				color = '#FF8C0088'
			end
		elseif self:isChecked() then
			color = '#0073FF88'
		else
			color = '#FF00AA88'
		end
	end
	
	for _, pid in pairs(self:getChildren()) do
		pid:setBackgroundColor(color)
	end
	
	self:setImageColor(color)
end
