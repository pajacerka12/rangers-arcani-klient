NPCItemListWidget < UIWidget
  anchors.right: parent.right
  size: 32 13
  margin: 4
  phantom: true

NPCClassList < UIWidget
  margin: 4
  size: 100 80
  background-color: alpha
  @onClick: modules.game_encyclopedia.m_ItemlistFunctions.selectNpc(self)
  
  $hover:
    background-color: #313131

  $pressed:
    background-color: #0A0A0A

  UICreature
    id: creature
    anchors.centerIn: parent
    image-source: /images/leaf/blank
    fixed-creature: 1.0
    size: 64 64
    phantom: true
    background-color: #FFFFFF00
    margin-bottom: 12
  
  NPCItemListWidget
    anchors.bottom: parent.bottom
    image-source: /images/leaf/shop/sell
    
  Label
    id: sell
    anchors.left: parent.left
    anchors.right: prev.left
    anchors.top: prev.top
    anchors.bottom: prev.bottom
    font: perfect-sans-11px
    text-align: right
    margin-right: 4
  
  NPCItemListWidget
    anchors.top: parent.top
    image-source: /images/leaf/shop/buy
    
  Label
    id: buy
    anchors.left: parent.left
    anchors.right: prev.left
    anchors.top: prev.top
    anchors.bottom: prev.bottom
    font: perfect-sans-11px
    text-align: right
    margin-right: 4

CreatureClassList < UIWidget
  margin: 4
  size: 80 80
  text-align: bottom
  text-offset: 20 0
  background-color: alpha

  $hover:
    background-color: #313131

  $pressed:
    background-color: #0A0A0A

  UICreature
    id: creature
    anchors.centerIn: parent
    image-source: /images/leaf/blank
    fixed-creature: 1.0
    size: 64 64
    phantom: true
    background-color: #FFFFFF00

ItemCategory < UIWidget
  id: itemCategory
  font: MyriadPro-Bold-13px
  text-offset: 40 2
  text-align: left
  text-wrap: true
  height: 38
  
  $!on:
    background-color: alpha

  $hover !on:
    background-color: #313131

  $pressed !on:
    background-color: #0A0A0A

  $on:
    background-color: #004180

  $hover on:
    background-color: #1E90FF

  $pressed on:
    background-color: #000D1B
  
  UIItem
    id: item
    size: 34 34
    image-source: /ui/blank
    anchors.left: parent.left
    anchors.verticalcenter: parent.verticalcenter
    virtual: true
    enabled: false
    margin-left: 2
      
  UIWidget
    id: mask
    anchors.fill: prev
    icon: /ui/blank
    visible: false
    phantom: false

SortItemListButton < AtlasButton
  size: 67 40
  anchors.top: prev.top
  anchors.left: prev.right
  margin-left: 1
  font: perfect-sans-18px-bold
  text-align: center

ItemListWindow < UIWindow
  anchors.fill: parent
  draggable: false
  phantom: true
  
  ScrollablePanel
    id: monsterListPanel
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    horizontal-scrollbar: monsterListScroll
    height: 100
    image-source: /ui/gui_atlas
    image-clip: 365 119 153 176
    image-border: 8
    margin: 5
    margin-right: 226
    layout:
      type: horizontalBox
  
  HorizontalScrollBar
    id: monsterListScroll
    anchors.bottom: prev.top
    anchors.left: prev.left
    anchors.right: prev.right
    step: 88
    pixels-scroll: true
    visible: true
  
  ScrollablePanel
    id: itemListPanel
    vertical-scrollbar: itemListScroll
    anchors.bottom: monsterListPanel.top
    anchors.top: attributeBox.bottom
    anchors.left: parent.left
    margin-top: 8
    margin-left: 5
    margin-bottom: 18
    padding: 5
    width: 255
    image-source: /ui/gui_atlas
    image-clip: 365 119 153 176
    image-border: 8
    layout:
      type: verticalBox
  
  VerticalScrollBar
    id: itemListScroll
    anchors.top: prev.top
    anchors.bottom: prev.bottom
    anchors.left: prev.right
    step: 76
    pixels-scroll: true
  
  ScrollablePanel
    id: itemCategoryPanel
    anchors.top: parent.top
    anchors.right: parent.right
    anchors.bottom: itemListPanel.bottom
    vertical-scrollbar: itemCategoryScroll
    margin-top: 5
    margin-right: 5
    padding: 5
    width: 255
    image-source: /ui/gui_atlas
    image-clip: 365 119 153 176
    image-border: 8
    layout:
      type: verticalBox
  
  VerticalScrollBar
    id: itemCategoryScroll
    anchors.top: prev.top
    anchors.bottom: prev.bottom
    anchors.right: prev.left
    step: 76
    pixels-scroll: true

  TextEdit
    id: nameTextEdit
    anchors.top: parent.top
    anchors.left: parent.left
    margin-top: 4
    margin-left: 6
    width: 268
    auto-focus: first

  ComboBox
    id: sortBox
    anchors.top: prev.bottom
    anchors.right: prev.right
    anchors.left: prev.left
    margin-top: 4
    height: 22
    menu-scroll: true
    menu-height: 125
    menu-scroll-step: 25

  ComboBox
    id: qualityBox
    anchors.top: prev.bottom
    anchors.right: prev.right
    anchors.left: prev.left
    margin-top: 4
    height: 22
    menu-scroll: true
    menu-height: 125
    menu-scroll-step: 25

  ComboBox
    id: attributeBox
    anchors.top: prev.bottom
    anchors.right: prev.right
    anchors.left: prev.left
    margin-top: 4
    height: 22
    menu-scroll: true
    menu-height: 125
    menu-scroll-step: 25
  
  ScrollablePanel
    id: npcListPanel
    anchors.bottom: monsterListPanel.bottom
    anchors.top: monsterListPanel.top
    anchors.left: monsterListPanel.right
    anchors.right: parent.right
    horizontal-scrollbar: npcListScroll
    image-source: /ui/gui_atlas
    image-clip: 365 119 153 176
    image-border: 8
    margin-left: 6
    margin-right: 5
    layout:
      type: horizontalBox
  
  HorizontalScrollBar
    id: npcListScroll
    anchors.bottom: prev.top
    anchors.left: prev.left
    anchors.right: prev.right
    step: 108
    pixels-scroll: true
    visible: true
  
  ScrollablePanel
    id: itemPanel
    vertical-scrollbar: itemPanelScroll
    anchors.top: itemCategoryPanel.top
    anchors.bottom: itemListPanel.bottom
    anchors.horizontalcenter: parent.horizontalcenter
    width: 340
    phantom: true
    layout:
      type: verticalBox
  
  VerticalScrollBar
    id: itemPanelScroll
    anchors.top: prev.top
    anchors.bottom: prev.bottom
    anchors.left: prev.right
    step: 20
    pixels-scroll: true