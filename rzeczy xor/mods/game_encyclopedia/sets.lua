sets = {
	{ name = {"Pirata", "Pirate"}, level = 1,
		list = {14462, 6095, 5918, 5462, 5926},
		clientIdList = {13540, 6095, 5918, 5461, 5926},
		attributes = {
						{ itemAmount = 3, requiredList = {5926}, luck = 500 },
						
						{ itemAmount = 2, requiredList = {5462}, speed = 20 },
						
						{ itemAmount = 3, requiredList = {14462}, increment = {type = "physical", value = 8} },
						
						{ itemAmount = 5, requiredList = {14462}, skill = {type = "dexterity", value = 25} }
						}
	},
	{ name = {"Sk�rzany", "Leather"}, level = 1,
		list = {15772, 15773, 15774, 15775, 15778, 15776, 2455},
		clientIdList = {14850, 14851, 14852, 14853, 14856, 14854, 3349},
		attributes = {
						{ itemAmount = 2, stat = {type = "health", value = 15} },
						
						{ itemAmount = 3, stat = {type = "health", value = 15} },
						{ itemAmount = 3, skill = {type = "dexterity", value = 2} },
						{ itemAmount = 3, skill = {type = "vigor", value = 2} },
						
						{ itemAmount = 4, skill = {type = "dexterity", value = 1} },
						{ itemAmount = 4, skill = {type = "vigor", value = 2} },
						{ itemAmount = 4, monsterdefense = {type = "human", value = 10} },
						
						{ itemAmount = 5, stat = {type = "health", value = 20} },
						{ itemAmount = 5, skill = {type = "strength", value = 5} },
						{ itemAmount = 5, monsterdefense = {type = "human", value = 5} },
						}
	},
	{ name = {"Lamparci", "Lampart"}, level = 1,
		list = {12961, 12960, 12959, 12958, 12963, 12962},
		clientIdList = {12039, 12038, 12037, 12036, 12041, 12040},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 2} },
						{ itemAmount = 2, skill = {type = "dexterity", value = 1} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 3} },
						{ itemAmount = 3, skill = {type = "dexterity", value = 1} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 2} },
						{ itemAmount = 4, doublehit = 20 },
						{ itemAmount = 4, monsterdefense = {type = "animal", value = 10} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 3} },
						{ itemAmount = 5, skill = {type = "dexterity", value = 2} },
						{ itemAmount = 5, skill = {type = "condition", value = 2} },
						{ itemAmount = 5, doublehit = 20 },
						}
	},
	{ name = {"Mosi�ny", "Brass"}, level = 1,
		list = {15750, 15751, 15752, 15753, 15754},
		clientIdList = {14828, 14829, 14830, 14831, 14832},
		attributes = {
						{ itemAmount = 2, skill = {type = "condition", value = 3} },
						
						{ itemAmount = 3, skill = {type = "condition", value = 2} },
						{ itemAmount = 3, skill = {type = "strength", value = 2} },
						
						{ itemAmount = 4, skill = {type = "condition", value = 2} },
						{ itemAmount = 4, skill = {type = "strength", value = 3} },
						{ itemAmount = 4, absorb = {type = "physical", value = 5} },
						}
	},
	{ name = {"Miedziany", "Copper"}, level = 1,
		list = {15785, 15786, 15787, 15788, 15789, 15790, 15784},
		clientIdList = {14863, 14864, 14865, 14866, 14867, 14868, 14862},
		attributes = {
						{ itemAmount = 2, skill = {type = "condition", value = 2} },
						{ itemAmount = 2, skill = {type = "strength", value = 2} },
						
						{ itemAmount = 3, skill = {type = "condition", value = 1} },
						{ itemAmount = 3, skill = {type = "strength", value = 1} },
						{ itemAmount = 3, skill = {type = "vigor", value = 2} },
						{ itemAmount = 3, stat = {type = "health", value = 40} },
						
						{ itemAmount = 4, skill = {type = "condition", value = 2} },
						{ itemAmount = 4, skill = {type = "strength", value = 2} },
						{ itemAmount = 4, skill = {type = "vigor", value = 1} },
						{ itemAmount = 4, stat = {type = "health", value = 20} },
						{ itemAmount = 4, increment = {type = "physical", value = 3} },
						{ itemAmount = 4, regeneration = {type = "health", value = 5} },
						
						{ itemAmount = 5, skill = {type = "condition", value = 2} },
						{ itemAmount = 5, skill = {type = "vigor", value = 2} },
						{ itemAmount = 5, stat = {type = "health", value = 20} },
						{ itemAmount = 5, increment = {type = "physical", value = 2} },
						}
	},
	{ name = {"z Br�zu", "Bronze"}, level = 1,
		list = {14354, 14353, 14355, 14356, 14358, 14357},
		clientIdList = {13432, 13431, 13433, 13434, 13436, 13435},
		attributes = {
						{ itemAmount = 2, skill = {type = "condition", value = 3} },
						{ itemAmount = 2, skill = {type = "strength", value = 2} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 1} },
						{ itemAmount = 3, absorb = {type = "fire", value = 10} },
						
						{ itemAmount = 4, skill = {type = "condition", value = 2} },
						{ itemAmount = 4, skill = {type = "strength", value = 4} },
						{ itemAmount = 4, criticalhitchance = 3 },
						{ itemAmount = 4, regeneration = {type = "health", value = 10} },
						
						{ itemAmount = 5, skill = {type = "condition", value = 3} },
						{ itemAmount = 5, absorb = {type = "fire", value = 2} },
						{ itemAmount = 5, criticalhitchance = 2 },
						{ itemAmount = 5, regeneration = {type = "health", value = 2} },
						}
	},
	{ name = {"z Wzmocnionej sk�ry", "Enhanced leather"}, level = 1,
		list = {16305, 16306, 16307, 16308},
		clientIdList = {15383, 15384, 15385, 15386},
		attributes = {
						{ itemAmount = 2, skill = {type = "dexterity", value = 5} },
						{ itemAmount = 2, skill = {type = "wisdom", value = 2} },
						
						{ itemAmount = 3, skill = {type = "dexterity", value = 2} },
						{ itemAmount = 3, skill = {type = "wisdom", value = 1} },
						{ itemAmount = 3, regeneration = {type = "mana", value = 2} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 2} },
						{ itemAmount = 4, skill = {type = "condition", value = 2} },
						{ itemAmount = 4, skill = {type = "strength", value = 2} },
						{ itemAmount = 4, regeneration = {type = "mana", value = 2} },
						}
	},
	
	
	{ name = {"Legionisty", "Legionnaire"}, level = 1,
		list = {14479, 14170},
		clientIdList = {13557, 13248},
		attributes = {
						{ itemAmount = 2, chanceblockcritical = 50 },
						}
	},
	{ name = {"�elazny", "Iron"}, level = 1,
		list = {14349, 14348, 14350, 14351, 14352, 14339},
		clientIdList = {13427, 13426, 13428, 13429, 13430, 13417},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 5} },
						{ itemAmount = 2, skill = {type = "dexterity", value = 2} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 3} },
						{ itemAmount = 3, skill = {type = "dexterity", value = 1} },
						{ itemAmount = 3, absorb = {type = "physical", value = 3} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 2} },
						{ itemAmount = 4, skill = {type = "condition", value = 3} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 10} },
						{ itemAmount = 5, skill = {type = "condition", value = 2} },
						{ itemAmount = 5, absorb = {type = "physical", value = 2} },
						{ itemAmount = 5, criticalhitchance = 3 },
						}
	},
	{ name = {"Wiradona", "Wiradon's"}, level = 1,
		list = {13906, 13907, 13908, 13909, 13910, 13911},
		clientIdList = {12984, 12985, 12986, 12987, 12988, 12989},
		attributes = {
						{ itemAmount = 2, skill = {type = "wisdom", value = 8} },
						
						{ itemAmount = 3, skill = {type = "wisdom", value = 2} },
						{ itemAmount = 3, increment = {type = "energy", value = 5} },
						{ itemAmount = 3, regeneration = {type = "mana", value = 2} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 5} },
						{ itemAmount = 4, increment = {type = "energy", value = 2} },
						{ itemAmount = 4, regeneration = {type = "mana", value = 2} },
						
						{ itemAmount = 5, skill = {type = "wisdom", value = 7} },
						{ itemAmount = 5, increment = {type = "energy", value = 3} },
						{ itemAmount = 5, regeneration = {type = "mana", value = 3} },
						}
	},
	{ name = {"Berynitowy", "Berynit"}, level = 1,
		list = {14478, 14171, 2535, 15721, 15719},
		clientIdList = {13556, 13249, 3435, 14799, 14797},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 8} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 7} },
						{ itemAmount = 3, skill = {type = "condition", value = 10} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 9} },
						{ itemAmount = 4, skill = {type = "condition", value = 2} },
						}
	},
	{ name = {"Stalowy", "Steel"}, level = 1,
		list = {14363, 14362, 14327, 14326, 14324, 14325},
		clientIdList = {13441, 13440, 13405, 13404, 13402, 13403},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 10} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 15} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 5} },
						{ itemAmount = 4, skill = {type = "condition", value = 10} },
						{ itemAmount = 4, monsterdefense = {type = "undead", value = 15} },
						
						{ itemAmount = 5, skill = {type = "condition", value = 10} },
						{ itemAmount = 5, absorb = {type = "physical", value = 1} },
						{ itemAmount = 5, regeneration = {type = "health", value = 10} },
						}
		},
	{ name = {"W�owy", "Snake"}, level = 1,
		list = {15755, 15756, 15757, 15758, 13940, 15760, 15761},
		clientIdList = {14833, 14834, 14835, 14836, 13018, 14838, 14839},
		attributes = {
						{ itemAmount = 2, skill = {type = "wisdom", value = 5} },
						{ itemAmount = 2, skill = {type = "dexterity", value = 2} },
						{ itemAmount = 2, skill = {type = "condition", value = 2} },
						
						{ itemAmount = 3, skill = {type = "wisdom", value = 2} },
						{ itemAmount = 3, skill = {type = "dexterity", value = 1} },
						{ itemAmount = 3, increment = {type = "earth", value = 5} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 8} },
						{ itemAmount = 4, skill = {type = "dexterity", value = 2} },
						{ itemAmount = 4, skill = {type = "condition", value = 1} },
						{ itemAmount = 4, increment = {type = "earth", value = 5} },
						{ itemAmount = 4, regeneration = {type = "mana", value = 2} },
						
						{ itemAmount = 5, skill = {type = "wisdom", value = 5} },
						{ itemAmount = 5, skill = {type = "dexterity", value = 2} },
						{ itemAmount = 5, skill = {type = "vigor", value = 2} },
						{ itemAmount = 4, increment = {type = "earth", value = 5} },
						{ itemAmount = 4, regeneration = {type = "mana", value = 1} },
						},
		},
	{name = {"Z kr�lewskiej stali", "Royal steel"}, level = 1,
		list = {14322, 14323, 14321, 14320, 14319, 14318, 13941},
		clientIdList = {13400, 13401, 13399, 13398, 13397, 13396, 13019},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 12} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 13} },
						{ itemAmount = 3, skill = {type = "condition", value = 5} },
						{ itemAmount = 3, skill = {type = "vigor", value = 5} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 5} },
						{ itemAmount = 4, absorb = {type = "physical", value = 5} },
						{ itemAmount = 4, monsterdefense = {type = "human", value = 15} },
						{ itemAmount = 4, monsterdefense = {type = "dragon", value = 15} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 10} },
						{ itemAmount = 5, skill = {type = "condition", value = 10} },
						}
		},
	{name = {"Bagienny", "Swamplair"}, level = 1,
		list = {16168, 16167, 16166, 16165, 16169, 14453},
		clientIdList = {15246, 15245, 15244, 15243, 15247, 13531},	
		attributes = {
						{ itemAmount = 2, skill = {type = "wisdom", value = 8} },
						{ itemAmount = 2, skill = {type = "dexterity", value = 6} },
						
						{ itemAmount = 3, skill = {type = "wisdom", value = 7} },
						{ itemAmount = 3, skill = {type = "dexterity", value = 4} },
						{ itemAmount = 3, increment = {type = "earth", value = 8} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 10} },
						{ itemAmount = 4, skill = {type = "dexterity", value = 5} },
						{ itemAmount = 4, skill = {type = "vigor", value = 5} },
						{ itemAmount = 4, increment = {type = "earth", value = 2} },
						
						{ itemAmount = 5, skill = {type = "wisdom", value = 10} },
						{ itemAmount = 5, skill = {type = "dexterity", value = 5} },
						{ itemAmount = 5, skill = {type = "vigor", value = 3} },
						{ itemAmount = 5, increment = {type = "earth", value = 2} },
						}
		},
	{name = {"Z czarnej stali", "Black steel"}, level = 1,
		list = {16158, 16157, 16156, 16155, 16164, 16162, 16159, 16161, 16160},
		clientIdList = {15236, 15235, 15234, 15233, 15242, 15240, 15237, 15239, 15238},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 14} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 16} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 10} },
						{ itemAmount = 4, skill = {type = "condition", value = 10} },
						{ itemAmount = 4, absorb = {type = "physical", value = 5} },
						{ itemAmount = 4, monsterdefense = {type = "lizard", value = 15} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 5} },
						{ itemAmount = 5, skill = {type = "condition", value = 5} },
						{ itemAmount = 5, increment = {type = "physical", value = 5} },
						}
		},
	{ name = {"Spopielacza", "Ashbringer"}, level = 1,
		list = {13945, 13947, 13946, 13944, 16486, 16478, 2191, 16906},
		clientIdList = {13023, 13025, 13024, 13022, 15564, 15556, 3075, 15984},
		attributes = {
						{ itemAmount = 2, skill = {type = "wisdom", value = 15} },
						
						{ itemAmount = 3, skill = {type = "wisdom", value = 15} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 10} },
						{ itemAmount = 4, criticalhitchance = 3 },
						
						{ itemAmount = 5, skill = {type = "wisdom", value = 10} },
						{ itemAmount = 5, criticalhitchance = 2 },
						}
		},
	{ name = {"�witu", "The Dawn"}, level = 1,
		list = {16290, 16291, 16292, 16293, 16294, 18279},
		clientIdList = {15368, 15369, 15370, 15371, 15372, 17357},
		attributes = {
						{ itemAmount = 2, skill = {type = "wisdom", value = 10} },
						
						{ itemAmount = 3, skill = {type = "wisdom", value = 10} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 5} },
						{ itemAmount = 4, monsterdefense = {type = "undead", value = 5} },	
						
						{ itemAmount = 5, skill = {type = "wisdom", value = 13} },
						{ itemAmount = 5, monsterdefense = {type = "undead", value = 5} },	
						}
		},
	
	
	{ name = {"Mro�ny", "Frosty"}, level = 1,
		list = {16192, 16191, 16190, 16189, 16193, 13894, 13889, 16194, 13950, 13937},
		clientIdList = {15270, 15269, 15268, 15267, 15271, 12972, 12967, 15272, 13028, 13015},
		attributes = {
						{ itemAmount = 2, skill = {type = "condition", value = 20} },
						{ itemAmount = 2, absorb = {type = "ice", value = 5} },
						
						{ itemAmount = 3, skill = {type = "condition", value = 10} },
						{ itemAmount = 3, skill = {type = "strength", value = 10} },
						{ itemAmount = 3, absorb = {type = "ice", value = 5} },
						{ itemAmount = 3, stat = {type = "health", value = 100} },
						
						{ itemAmount = 4, skill = {type = "condition", value = 10} },
						{ itemAmount = 4, skill = {type = "strength", value = 5} },
						{ itemAmount = 4, absorb = {type = "ice", value = 2} },
						{ itemAmount = 4, stat = {type = "health", value = 20} },
						{ itemAmount = 4, monsterdefense = {type = "underwater", value = 10} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 5} },
						{ itemAmount = 5, absorb = {type = "ice", value = 3} },
						{ itemAmount = 5, stat = {type = "health", value = 30} },
						{ itemAmount = 5, monsterdefense = {type = "underwater", value = 5} },
						}
		},
	{ name = {"Ognisty", "Fiery"}, level = 1,
		list = {16203, 16202, 16201, 16200, 16204, 2392, 13897, 13951, 13939, 13893},
		clientIdList = {15281, 15280, 15279, 15278, 15282, 3280, 12975, 13029, 13017, 12971},
		attributes = {
						{ itemAmount = 2, skill = {type = "condition", value = 20} },
						{ itemAmount = 2, absorb = {type = "fire", value = 5} },
						
						{ itemAmount = 3, skill = {type = "condition", value = 10} },
						{ itemAmount = 3, skill = {type = "strength", value = 10} },
						{ itemAmount = 3, absorb = {type = "fire", value = 5} },
						{ itemAmount = 3, stat = {type = "health", value = 100} },
						
						{ itemAmount = 4, skill = {type = "condition", value = 10} },
						{ itemAmount = 4, skill = {type = "strength", value = 5} },
						{ itemAmount = 4, absorb = {type = "fire", value = 2} },
						{ itemAmount = 4, stat = {type = "health", value = 20} },
						{ itemAmount = 4, monsterdefense = {type = "pyro-elemental", value = 10} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 5} },
						{ itemAmount = 5, absorb = {type = "fire", value = 3} },
						{ itemAmount = 5, stat = {type = "health", value = 30} },
						{ itemAmount = 5, monsterdefense = {type = "pyro-elemental", value = 5} },
						}
		},
	{ name = {"Upad�ego", "Fallen"}, level = 1,
		list = {16147, 16146, 16145, 16144, 16148},
		clientIdList = {15225, 15224, 15223, 15222, 15226},
		attributes = {
						{ itemAmount = 2, skill = {type = "wisdom", value = 20} },
						{ itemAmount = 2, increment = {type = "death", value = 4} },
						
						{ itemAmount = 3, skill = {type = "wisdom", value = 15} },
						{ itemAmount = 3, increment = {type = "death", value = 2} },
						
						{ itemAmount = 4, skill = {type = "wisdom", value = 7} },
						{ itemAmount = 4, increment = {type = "death", value = 2} },
						
						{ itemAmount = 5, skill = {type = "wisdom", value = 8} },
						{ itemAmount = 5, increment = {type = "death", value = 4} },
						}
		},
	{ name = {"Nawiedzony", "Haunted"}, level = 1,
		list = {2537, 2531, 2532, 2538, 13797, 13895},
		clientIdList = {3437, 3431, 3432, 3438, 12875, 12973},
		attributes = {
						{ itemAmount = 2, skill = {type = "condition", value = 22} },
						{ itemAmount = 2, absorb = {type = "death", value = 4} },
						
						{ itemAmount = 3, skill = {type = "condition", value = 8} },
						{ itemAmount = 3, absorb = {type = "death", value = 1} },
						
						{ itemAmount = 4, skill = {type = "condition", value = 10} },
						{ itemAmount = 4, skill = {type = "strength", value = 20} },
						{ itemAmount = 4, absorb = {type = "death", value = 2} },
						}
		},
	{ name = {"Strzelca", "Marksman"}, level = 1,
		list = {12956, 12955, 12954, 12953, 12957},
		clientIdList = {12034, 12033, 12032, 12031, 12035},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 20} },
						{ itemAmount = 2, criticalhitchance = 1 },
						
						{ itemAmount = 3, skill = {type = "strength", value = 10} },
						{ itemAmount = 3, criticalhitchance = 1 },
						
						{ itemAmount = 4, skill = {type = "strength", value = 10} },
						{ itemAmount = 4, criticalhitmultiplier = 50 },
						
						{ itemAmount = 5, skill = {type = "strength", value = 15} },
						{ itemAmount = 5, criticalhitmultiplier = 50 },
						}
		},
	{ name = {"Smoczy", "Dragon"}, level = 1,
		list = {15743, 15744, 15745, 15746, 15749, 15747, 15748},
		clientIdList = {14821, 14822, 14823, 14824, 14827, 14825, 14826},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 20} },
						{ itemAmount = 2, absorb = {type = "physical", value = 2} },
						{ itemAmount = 2, absorb = {type = "fire", value = 5} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 5} },
						{ itemAmount = 3, skill = {type = "condition", value = 10} },
						{ itemAmount = 3, absorb = {type = "physical", value = 2} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 15} },
						{ itemAmount = 4, skill = {type = "condition", value = 10} },
						{ itemAmount = 4, absorb = {type = "physical", value = 2} },
						{ itemAmount = 4, absorb = {type = "fire", value = 3} },
						{ itemAmount = 4, criticalhitchance = 3 },
						}
		},
	{ name = {"Z�oty", "Golden"}, level = 1,
		list = {16139, 16138, 16137, 16136, 16142, 16140, 16141, 16143, 13949},
		clientIdList = {15217, 15216, 15215, 15214, 15220, 15218, 15219, 15221, 13027},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 20} },
						{ itemAmount = 2, skill = {type = "condition", value = 10} },
						{ itemAmount = 2, absorb = {type = "holy", value = 3} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 10} },
						{ itemAmount = 3, skill = {type = "condition", value = 10} },
						{ itemAmount = 3, increment = {type = "holy", value = 5} },
						{ itemAmount = 3, monsterdefense = {type = "undead", value = 10} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 10} },
						{ itemAmount = 4, skill = {type = "condition", value = 2} },
						{ itemAmount = 4, absorb = {type = "holy", value = 2} },
						{ itemAmount = 4, increment = {type = "holy", value = 3} },
						{ itemAmount = 4, monsterdefense = {type = "undead", value = 5} },
						{ itemAmount = 4, stat = {type = "health", value = 150} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 10} },
						{ itemAmount = 5, skill = {type = "condition", value = 3} },
						{ itemAmount = 4, increment = {type = "holy", value = 2} },
						}
		},
	{ name = {"Meteorytowy", "Meteorite"}, level = 1,
		list = {16208, 16207, 16206, 16205, 16211, 16209, 16210, 13938},
		clientIdList = {15286, 15285, 15284, 15283, 15289, 15287, 15288, 13016},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 12} },
						{ itemAmount = 2, skill = {type = "wisdom", value = 12} },
						{ itemAmount = 2, skill = {type = "condition", value = 10} },
						{ itemAmount = 2, bonusdamage = {type = "energy", value = 2} },
						{ itemAmount = 2, absorb = {type = "death", value = 3} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 5} },
						{ itemAmount = 3, skill = {type = "wisdom", value = 5} },
						{ itemAmount = 3, skill = {type = "condition", value = 5} },
						{ itemAmount = 3, bonusdamage = {type = "energy", value = 4} },
						{ itemAmount = 3, absorb = {type = "death", value = 2} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 3} },
						{ itemAmount = 4, skill = {type = "wisdom", value = 3} },
						{ itemAmount = 4, skill = {type = "condition", value = 5} },
						{ itemAmount = 4, bonusdamage = {type = "energy", value = 2} },
						{ itemAmount = 4, absorb = {type = "death", value = 1} },
						
						{ itemAmount = 5, skill = {type = "strength", value = 2} },
						{ itemAmount = 5, skill = {type = "wisdom", value = 2} },
						{ itemAmount = 5, skill = {type = "condition", value = 10} },
						{ itemAmount = 5, skill = {type = "dexterity", value = 10} },
						{ itemAmount = 5, bonusdamage = {type = "energy", value = 3} },
						{ itemAmount = 5, absorb = {type = "death", value = 1} },
						}
		},
	{ name = {"Rubinowy", "Ruby"}, level = 1,
		list = {16173, 16172, 16171, 16170, 16176, 16174, 16175},
		clientIdList = {15251, 15250, 15249, 15248, 15254, 15252, 15253},
		attributes = {
						{ itemAmount = 2, skill = {type = "strength", value = 30} },
						{ itemAmount = 2, skill = {type = "condition", value = 10} },
						{ itemAmount = 2, increment = {type = "energy", value = 5} },
						{ itemAmount = 2, absorb = {type = "energy", value = 5} },
						
						{ itemAmount = 3, skill = {type = "strength", value = 12} },
						{ itemAmount = 3, increment = {type = "energy", value = 4} },
						{ itemAmount = 3, absorb = {type = "energy", value = 5} },
						
						{ itemAmount = 4, skill = {type = "strength", value = 13} },
						{ itemAmount = 4, skill = {type = "condition", value = 15} },
						{ itemAmount = 4, increment = {type = "energy", value = 6} },
						{ itemAmount = 4, absorb = {type = "energy", value = 2} },
						}
		},
	{ name = {"Bog�w", "Gods"}, level = 1,
		list = {13791, 13789, 13788, 13786, 13785, 13784},
		clientIdList = {12869, 12867, 12866, 12864, 12863, 12862},
		attributes = {
						{ itemAmount = 2, requiredList = {13784}, skill = {type = "wisdom", value = -500} },
						{ itemAmount = 2, requiredList = {13791}, skill = {type = "strength", value = -500} },
						{ itemAmount = 2, requiredList = {13789}, skill = {type = "strength", value = -500} },
						{ itemAmount = 2, requiredList = {13788}, skill = {type = "strength", value = -500} },
						{ itemAmount = 2, requiredList = {13786}, skill = {type = "strength", value = -500} },
						{ itemAmount = 2, requiredList = {13785}, skill = {type = "wisdom", value = -500} },
						{ itemAmount = 2, requiredList = {13791, 13789, 13788, 13786, 13785, 13784}, skill = {type = "condition", value = -500} },
						{ itemAmount = 2, requiredList = {13791, 13789, 13788, 13786, 13785, 13784}, skill = {type = "vigor", value = -500} },
						}
		},
}