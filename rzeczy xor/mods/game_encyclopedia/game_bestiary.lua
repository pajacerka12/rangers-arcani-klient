m_BestiaryFunctions.SORTBY_NAME = 1
m_BestiaryFunctions.SORTBY_HEALTH = 2
m_BestiaryFunctions.SORTBY_EXPERIENCE = 3
m_BestiaryFunctions.SORTBY_KILL_COUNT = 4

m_BestiaryFunctions.GameServerUpdateLootList = 75
m_BestiaryFunctions.lootListId = 1
m_BestiaryFunctions.lootListAmount = 0

m_BestiaryFunctions.SORTBY_HIGH = 1
m_BestiaryFunctions.SORTBY_LOW = 2

m_BestiaryFunctions.language = LANGUAGE_ENGLISH

m_BestiaryFunctions.autoLootList = {}
m_BestiaryFunctions.bestiaryCache = {}
m_BestiaryFunctions.lootList = {}
m_BestiaryFunctions.trackList = {}

m_BestiaryFunctions.tooltipList = {
	[1] = 'Available for everybody',
	[2] = 'Available with at least 75 completed bestiary',
	[3] = 'Available for players with level 100 and higher',
	[4] = 'Available from premium shop',
	[5] = 'Available for finish all Bartolomeus tasks',
	[6] = 'Unavailable',
	[7] = 'Unavailable',
	[8] = 'Unavailable',
	[9] = 'Unavailable',
	[10] = 'Unavailable'
}

m_BestiaryFunctions.sortList = {
	{tr('Ascending by name'), m_BestiaryFunctions.SORTBY_NAME, m_BestiaryFunctions.SORTBY_HIGH},
	{tr('Descending by name'), m_BestiaryFunctions.SORTBY_NAME, m_BestiaryFunctions.SORTBY_LOW},
	{tr('Ascending by health'), m_BestiaryFunctions.SORTBY_HEALTH, m_BestiaryFunctions.SORTBY_HIGH},
	{tr('Descending by health'), m_BestiaryFunctions.SORTBY_HEALTH, m_BestiaryFunctions.SORTBY_LOW},
	{tr('Ascending by experience'), m_BestiaryFunctions.SORTBY_EXPERIENCE, m_BestiaryFunctions.SORTBY_HIGH},
	{tr('Descending by experience'), m_BestiaryFunctions.SORTBY_EXPERIENCE, m_BestiaryFunctions.SORTBY_LOW},
	{tr('Ascending by kill count'), m_BestiaryFunctions.SORTBY_KILL_COUNT, m_BestiaryFunctions.SORTBY_HIGH},
	{tr('Descending by kill count'), m_BestiaryFunctions.SORTBY_KILL_COUNT, m_BestiaryFunctions.SORTBY_LOW}
}

m_BestiaryFunctions.elementsList = {
	'physical',
	'bleed',
	'holy',
	'death',
	'energy',
	'earth',
	'fire',
	'ice'
}

m_BestiaryFunctions.config = {
	{id = MONSTER_ANIMAL, outfit = {type = 16}},
	{id = MONSTER_APE, outfit = {type = 116}},
	{id = MONSTER_ARACHNID, outfit = {type = 38}},
	{id = MONSTER_BONELORD, outfit = {type = 17}},
	{id = MONSTER_BOSS, outfit = {type = 362}},
	{id = MONSTER_CRYO_ELEMENTAL, outfit = {type = 11}},
	{id = MONSTER_DEMON, outfit = {type = 35}},
	{id = MONSTER_DJINN, outfit = {type = 658, addons = 3}},
	{id = MONSTER_DRAGON, outfit = {type = 34}},
	{id = MONSTER_DRAKEN, outfit = {type = 373}},
	{id = MONSTER_DWARF, outfit = {type = 695}},
	{id = MONSTER_ELECTRO_ELEMENTAL, outfit = {type = 290}},
	{id = MONSTER_ELF, outfit = {type = 479}},
	{id = MONSTER_GEO_ELEMENTAL, outfit = {type = 285}},
	{id = MONSTER_GIANT, outfit = {type = 67}},
	{id = MONSTER_GOBLIN, outfit = {type = 61}},
	{id = MONSTER_HUMAN, outfit = {type = 403}},
	{id = MONSTER_KYNOHEAD, outfit = {type = 662}},
	{id = MONSTER_LIZARD, outfit = {type = 338}},
	{id = MONSTER_MINOTAUR, outfit = {type = 463}},
	{id = MONSTER_MUTATED, outfit = {type = 323}},
	{id = MONSTER_WIZARD, outfit = {type = 492, addons = 2}},
	{id = MONSTER_ORC, outfit = {type = 5}},
	{id = MONSTER_PYRO_ELEMENTAL, outfit = {type = 242}},
	{id = MONSTER_SERPENT, outfit = {type = 220}},
	{id = MONSTER_TROLL, outfit = {type = 187}},
	{id = MONSTER_UNDEAD, outfit = {type = 414}},
	{id = MONSTER_UNDERWATER, outfit = {type = 525}},
	{id = MONSTER_VAMPIRE, outfit = {type = 513}},
	{id = MONSTER_YUAN_TI, outfit = {type = 370}},
	{id = MONSTER_PIRATE, outfit = {type = 96}},
	{id = MONSTER_PERVERTED, outfit = {type = 442}},
	{id = MONSTER_RATATOSK, outfit = {type = 260}},
}

function onBestiaryLine(list)
	m_BestiaryFunctions.onBestiaryLine(list)
end

m_BestiaryFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_BestiaryFunctions.language = LANGUAGE_ENGLISH
	else
		m_BestiaryFunctions.language = LANGUAGE_POLISH
	end
end

m_BestiaryFunctions.sendItemToList = function(id, add)
	local lootId = m_BestiaryFunctions.isOnList(id)
	if not add and not lootId then
		return true
	elseif add and lootId then
		return true
	end
	
	local msg = OutputMessage.create()
	msg:addU8(69)
	msg:addU8(lootId or m_BestiaryFunctions.lootListId)
	msg:addU16(id)
	msg:addU8(add and 1 or 0)
	g_game.getProtocolGame():send(msg)
end

m_BestiaryFunctions.updateLootList = function()
	for _, v in pairs(m_BestiaryList.bestiaryLootListWindow:getChildren()) do
		v:setItemId(nil)
	end
	
	local list = m_BestiaryFunctions.autoLootList[m_BestiaryFunctions.lootListId]
	if list then
		for i = 1, math.min(10, #list) do
			m_BestiaryList.bestiaryLootListWindow:getChildById(i):setItemId(list[i])
		end
	end
end

m_BestiaryFunctions.isOnList = function(id)
	for i = 1, 10 do
		local list = m_BestiaryFunctions.autoLootList[i]
		if list and isInArray(list, id) then
			return i
		end
	end
	
	return false
end

m_BestiaryFunctions.parseLootList = function(protocol, msg)
	m_BestiaryFunctions.lootListAmount = msg:getU8()
	m_BestiaryFunctions.lootListId = math.max(1, msg:getU8())
	local size = msg:getU8()
	for i = 1, size do
		local id = msg:getU8()
		m_BestiaryFunctions.autoLootList[id] = {}
		
		local tmpSize = msg:getU8()
		for i = 1, tmpSize do
			local itemId = msg:getU16()
			table.insert(m_BestiaryFunctions.autoLootList[id], itemId)
		end
	end
	
	if m_BestiaryList.window then
		m_BestiaryFunctions.updateLootList()
	end
end

m_BestiaryFunctions.registerProtocol = function()
	ProtocolGame.registerOpcode(m_BestiaryFunctions.GameServerUpdateLootList, m_BestiaryFunctions.parseLootList)
end

m_BestiaryFunctions.unregisterProtocol = function()
	ProtocolGame.unregisterOpcode(m_BestiaryFunctions.GameServerUpdateLootList, m_BestiaryFunctions.parseLootList)
end

m_BestiaryFunctions.onLoad = function()
	m_BestiaryFunctions.loadLanguage()
	connect(g_game, {
		onBestiaryLine = onBestiaryLine
	})
	
	if g_game.isOnline() then
		m_BestiaryFunctions.registerProtocol()
	end
end

m_BestiaryFunctions.onUnload = function()
	m_BestiaryFunctions.loaded = false
	disconnect(g_game, {
		onBestiaryLine = onBestiaryLine
	})
	
	m_BestiaryFunctions.unregisterProtocol()
end

m_BestiaryFunctions.onGameStart = function()
	m_BestiaryFunctions.loaded = false
	m_BestiaryFunctions.bestiaryCache = {}
	
	m_BestiaryFunctions.lootListId = 1
	m_BestiaryFunctions.lootListAmount = 0
	
	m_BestiaryFunctions.sendItemToList(0, true)
	m_BestiaryFunctions.loadLanguage()
	
	m_BestiaryFunctions.name = g_game.getLocalPlayer():getName()
	m_BestiaryFunctions.trackList = g_settings.getNode('TrackList' .. m_BestiaryFunctions.name) or {}
	m_BestiaryFunctions.registerProtocol()
end

m_BestiaryFunctions.onGameEnd = function()
	if not m_BestiaryFunctions.name then
		m_BestiaryFunctions.name = g_game.getLocalPlayer():getName()
	end
	
	local list = {}
	for k, v in pairs(m_BestiaryFunctions.trackList) do
		if v then
			list[k] = v
		end
	end
	
	g_settings.setNode('TrackList' .. m_BestiaryFunctions.name, list)
	m_BestiaryFunctions.unregisterProtocol()
end

m_BestiaryFunctions.destroy = function()
	m_BestiaryFunctions.destroyPopup()
	
	if m_BestiaryList.window then
		m_BestiaryList.bestiaryMonsterList:destroyChildren()
		m_BestiaryList.bestiaryMonsterInfo:destroyChildren()
		m_BestiaryList.bestiaryLootInfoList:destroyChildren()
		m_BestiaryList.bestiaryLootListWindow:destroyChildren()
		m_BestiaryList.bestiaryLootListTemplate:destroyChildren()
		
		m_BestiaryList.window:destroy()
		m_BestiaryList = {}
		
		modules.client_options.setOption('visibleUnknownMonsters', m_BestiaryFunctions.visibleUnknownMonsters, true)
		modules.client_options.setOption('visibleCompletedTasks', m_BestiaryFunctions.visibleCompletedTasks, true)
	end
end

m_BestiaryFunctions.open = function(parent)
	if m_BestiaryList.window then
		m_BestiaryFunctions.destroy()
	else
		m_BestiaryList.window = g_ui.createWidget('BestiaryWindow', parent)
		m_BestiaryList.bestiaryClassList = m_BestiaryList.window:getChildById('classList')
		m_BestiaryList.bestiaryMonsterList = m_BestiaryList.window:getChildById('monsterList')
		m_BestiaryList.bestiaryMonsterInfo = m_BestiaryList.window:getChildById('monsterInfo')
		m_BestiaryList.bestiaryLootInfoList = m_BestiaryList.window:getChildById('lootInfoList')
		m_BestiaryList.bestiaryLootListWindow = m_BestiaryList.window:getChildById('lootListWindow')
		m_BestiaryList.bestiaryLootListTemplate = m_BestiaryList.window:getChildById('lootListTemplate')
		m_BestiaryList.bestiarySearchMonsterName = m_BestiaryList.window:getChildById('searchMonsterName')
		m_BestiaryList.sortBox = m_BestiaryList.window:getChildById('sortBox')
		m_BestiaryList.trackKills = m_BestiaryList.window:getChildById('trackKills')
		
		m_BestiaryList.currentSorting = m_BestiaryFunctions.SORTBY_NAME
		m_BestiaryList.sortby = m_BestiaryFunctions.SORTBY_HIGH
		
		m_BestiaryFunctions.execute()
	end
end

m_BestiaryFunctions.selectTemplate = function(self)
	if self:getChildById('mask'):isVisible() then
		return true
	end
	
	if m_BestiaryFunctions.lootListId == tonumber(self:getId()) then
		return true
	end
	
	m_BestiaryList.bestiaryLootListTemplate:getChildById(m_BestiaryFunctions.lootListId):getChildById('blink'):hide()
	m_BestiaryFunctions.lootListId = tonumber(self:getId())
	m_BestiaryList.bestiaryLootListTemplate:getChildById(m_BestiaryFunctions.lootListId):getChildById('blink'):show()
	
	local msg = OutputMessage.create()
	msg:addU8(69)
	msg:addU8(255)
	msg:addU8(m_BestiaryFunctions.lootListId)
	g_game.getProtocolGame():send(msg)
	
	m_BestiaryFunctions.updateLootList()
end

m_BestiaryFunctions.getVariable = function(id)
	local var = m_BestiaryFunctions.lootListAmount
	if var < 2 ^ id then
		return true
	end
	
	for i = 10, id, -1 do
		if var >= 2 ^ i then
			if i == id then
				return false
			end
			
			var = var - (2 ^ i)
		end
	end

	return true
end

m_BestiaryFunctions.execute = function()
	m_BestiaryFunctions.autoLootList = {}
	
	for i = 1, #m_BestiaryFunctions.sortList do
		m_BestiaryList.sortBox:addOption(m_BestiaryFunctions.sortList[i][1])
	end
	
	m_BestiaryList.sortBox:setCurrentOption(m_BestiaryFunctions.sortList[1])
	connect(m_BestiaryList.sortBox, { onOptionChange = onBestiarySortOptionChange })
	m_BestiaryList.sortId = m_BestiaryFunctions.sortList[1][1]
	
	m_BestiaryList.bestiarySearchMonsterName:focus()
	m_BestiaryList.window:getChildById('checkButton'):setChecked(m_BestiaryFunctions.visibleUnknownMonsters)
	m_BestiaryList.window:getChildById('displayCompletedTasks'):setChecked(m_BestiaryFunctions.visibleCompletedTasks)
	m_BestiaryList.bestiarySearchMonsterName.onTextChange = function(self, text, oldText)
		local list = m_BestiaryList.bestiaryMonsterList:getChildren()
		if not text:find(oldText) then
			for _, v in pairs(list) do
				if (v.kills == 0 and not m_BestiaryFunctions.visibleUnknownMonsters) or (v.completed and not m_BestiaryFunctions.visibleCompletedTasks) then
					--
				else
					local name = v:getChildById('name')
					if name:getText():lower():find(text:lower()) then
						v:show()
					end
				end
			end
		end
		
		for _, v in pairs(list) do
			local name = v:getChildById('name'):getText()
			local tmpList = {
				['�'] = '�',
				['�'] = '�',
				['�'] = '�',
				['�'] = '�'
			}
			
			for k, v in pairs(tmpList) do
				name = name:gsub(k, v)
			end
			
			if not name:lower():find(text:lower()) then
				v:hide()
			end
		end
	end
	
	local premium = g_game.getLocalPlayer():isPremium()
	for i = 1, 10 do
		local widget = g_ui.createWidget('LootItemTemplate', m_BestiaryList.bestiaryLootListTemplate)
		widget:setText(i)
		widget:setId(i)
		if m_BestiaryFunctions.getVariable(i - 1) then
			widget:getChildById('mask'):show()
			widget:setTooltip(m_BestiaryFunctions.tooltipList[i])
		end
		
		if i == m_BestiaryFunctions.lootListId then
			widget:getChildById('blink'):show()
		end
		
		local widget = g_ui.createWidget('LootItemList', m_BestiaryList.bestiaryLootListWindow)
		if i > 5 and not premium then
			widget:getChildById('mask'):show()
			widget:setOn(true)
		end
		
		widget:setId(i)
		widget.onMouseRelease = function(self, mousePosition, mouseButton)
									if m_BestiaryList.CancelNextRelease then
										m_BestiaryList.CancelNextRelease = false
										return false
									end
									
									if (g_mouse.isPressed(MouseLeftButton) and mouseButton == MouseRightButton) or 
										(g_mouse.isPressed(MouseRightButton) and mouseButton == MouseLeftButton) then
										m_BestiaryList.CancelNextRelease = true
										
										local classId = getClassNameById(self.classId or self:getClassType())
										modules.game_lookat.m_LookAtFunctions.inspectItem(self:getItem(), classId, false, self:isVirtual() or self:getItemCount() == 0)
										return true
									elseif mouseButton == MouseRightButton then
										local item = widget:getItem()
										if not item then
											return true
										end
										
										local itemId = widget:getItem():getId()
										if m_BestiaryFunctions.isOnList(itemId) then
											local menu = g_ui.createWidget('PopupMenu')
											menu:setGameMenu(true)
											menu:addOption(tr('Remove from loot list'), function() return m_BestiaryFunctions.sendItemToList(itemId, false) end)
											menu:display(mousePosition)
											return true
										end
									end
									
									return false
								end
	end
	
	m_BestiaryFunctions.sendItemToList(0, true)
	m_BestiaryFunctions.load()
end

m_BestiaryFunctions.load = function()
	g_game.requestBestiaryLine(m_BestiaryFunctions.loaded)
	m_BestiaryFunctions.loaded = true
end

function onBestiarySortOptionChange(comboBox, text, data)
	if m_BestiaryList.sortId == text then
		return false
	end
	
	m_BestiaryList.sortId = text
	
	for i = 1, #m_BestiaryFunctions.sortList do
		if text == m_BestiaryFunctions.sortList[i][1] then
			m_BestiaryList.currentSorting = m_BestiaryFunctions.sortList[i][2]
			m_BestiaryList.sortby = m_BestiaryFunctions.sortList[i][3]
			break
		end
	end
	
	m_BestiaryFunctions.addMonsters(m_BestiaryFunctions.classId)
	return true
end

m_BestiaryFunctions.setVisibleCompletedTasks = function(checked)
	m_BestiaryFunctions.visibleCompletedTasks = checked
	m_BestiaryFunctions.updateVisibleMonsters()
end

m_BestiaryFunctions.setVisibleUnknownMonsters = function(checked)
	m_BestiaryFunctions.visibleUnknownMonsters = checked
	m_BestiaryFunctions.updateVisibleMonsters()
end

m_BestiaryFunctions.setTrack = function(checked, name)
	name = name or m_BestiaryFunctions.openByItemlist[1]
	if checked then
		if not isInArray(m_BestiaryFunctions.trackList, name) then
			m_BestiaryFunctions.trackList[name] = true
		end
	else
		if m_BestiaryFunctions.trackList[name] then
			m_BestiaryFunctions.trackList[name] = false
		end
	end
	
	modules.game_tracker.m_TrackerFunctions.updateByBestiary(name, checked)
end

m_BestiaryFunctions.getTrackList = function()
	return m_BestiaryFunctions.trackList
end

m_BestiaryFunctions.isInTrackList = function(name)
	return m_BestiaryFunctions.trackList[name]
end

m_BestiaryFunctions.updateVisibleMonsters = function()
	if not m_BestiaryList.bestiaryMonsterList then
		return true
	end
	
	for _, pid in pairs(m_BestiaryList.bestiaryMonsterList:getChildren()) do
		if (pid.kills == 0 and not m_BestiaryFunctions.visibleUnknownMonsters) or (pid.completed and not m_BestiaryFunctions.visibleCompletedTasks) then
			pid:hide()
		else
			pid:show()
		end
	end
end

m_BestiaryFunctions.sortByClassName = function(k, v)
	return k[1] < v[1]
end

m_BestiaryFunctions.onOpenBestiary = function(list)
	if not m_BestiaryList.window then
		return true
	end
	
	local i = 1
	local sent = false
	local tmpList = {}
	for k = 1, #m_BestiaryFunctions.config do
		table.insert(tmpList, m_BestiaryFunctions.getNameByClass(m_BestiaryFunctions.config[k]))
	end
	
	table.sort(tmpList, m_BestiaryFunctions.sortByClassName)
	for k = 1, #tmpList do
		local widget = g_ui.createWidget('BestiaryCreatureClass', m_BestiaryList.bestiaryClassList)
		local className = tmpList[k][1]
		local id = tmpList[k][3]
		
		widget.class = id
		widget:getChildById('creature'):setOutfit(tmpList[k][2])
		widget:getChildById('name'):setText(className)
		widget:setParent(m_BestiaryList.bestiaryClassList)
		widget.onClick = function()
			m_BestiaryFunctions.openByItemlist = {'', id}
			m_BestiaryFunctions.openByItemlist[3] = m_BestiaryFunctions.select(m_BestiaryList.bestiaryClassList:getChildren(), id) * 65
			m_BestiaryFunctions.addMonsters(id)
			m_BestiaryList.bestiarySearchMonsterName:clearText()
		end
		
		if not sent then
			if m_BestiaryFunctions.openByItemlist then
				if id == m_BestiaryFunctions.openByItemlist[2] then
					m_BestiaryFunctions.select(m_BestiaryList.bestiaryClassList:getChildren(), id)
					m_BestiaryFunctions.addMonsters(id)
					if not m_BestiaryFunctions.openByItemlist[3] then
						m_BestiaryFunctions.openByItemlist[3] = i * 65
					end
					
					sent = true
				end
			elseif i == 1 then
				m_BestiaryFunctions.select(m_BestiaryList.bestiaryClassList:getChildren(), id)
				m_BestiaryFunctions.addMonsters(id)
			end
		end
		
		i = i + 1
	end
end

m_BestiaryFunctions.onBestiaryLine = function(list)
	for i = 1, #list do
		local name, kills = unpack(list[i])
		m_BestiaryFunctions.bestiaryCache[name] = (m_BestiaryFunctions.bestiaryCache[name] or 0) + kills
	end
	
	m_BestiaryFunctions.onOpenBestiary()
end

m_BestiaryFunctions.getMonsterKilled = function(name)
	return g_game.getLocalPlayer():getGroupId() > 1 and 10000 or (m_BestiaryFunctions.bestiaryCache[name] or 0)
end

m_BestiaryFunctions.sortByChance = function(k, v)
	return k.chance < v.chance
end

m_BestiaryFunctions.addMonsters = function(classId)
	m_BestiaryFunctions.classId = classId
	m_BestiaryList.bestiaryMonsterList:destroyChildren()
	
	local monsterId = 0
	local tmpList = modules.game_lookat.m_LookAtFunctions.getMonstersList(classId)
	if not tmpList then
		return true
	end
	
	local known, unknown = {}, {}
	for _, pid in pairs(tmpList) do
		if pid.kills == 0 then
			table.insert(unknown, pid)
		else
			table.insert(known, pid)
		end
	end
	
	if #known > 1 then
		if m_BestiaryList.currentSorting == m_BestiaryFunctions.SORTBY_HEALTH then
			table.sort(known, m_BestiaryFunctions.sortByHealth)
		elseif m_BestiaryList.currentSorting == m_BestiaryFunctions.SORTBY_NAME then
			table.sort(known, m_BestiaryFunctions.sortByName)
		elseif m_BestiaryList.currentSorting == m_BestiaryFunctions.SORTBY_EXPERIENCE then
			table.sort(known, m_BestiaryFunctions.sortByExperience)
		elseif m_BestiaryList.currentSorting == m_BestiaryFunctions.SORTBY_KILL_COUNT then
			table.sort(known, m_BestiaryFunctions.sortByKillCount)
		end
	end
	
	for i = 1, #unknown do
		table.insert(known, unknown[i])
	end
	
	for i = 1, #known do
		local v = known[i]
		local widget = g_ui.createWidget('BestiaryCreatureClass', m_BestiaryList.bestiaryMonsterList)
		
		local tmpCreature = Creature.create()
		tmpCreature:setOutfit(v.look)
		widget:getChildById('creature'):setCreature(tmpCreature)
		
		local kills = g_game.getLocalPlayer():getGroupId() > 1 and v.killAmount[3] or (m_BestiaryFunctions.bestiaryCache[v.name] or 0)
		local label = widget:getChildById('name')
		if kills == 0 then
			label:setText('?')
			widget:setTooltip('?\nKills: ' .. kills)
			tmpCreature:setOutfitColor('#000000')
			
			if not m_BestiaryFunctions.visibleUnknownMonsters then
				widget:hide()
			end
		else
			local tmpName = v.description[m_BestiaryFunctions.language]
			label:setText(tmpName)
			widget:setTooltip(tmpName .. '\nKills: ' .. kills)
		end
		
		if kills >= v.killAmount[3] then
			tmpCreature:setShader(g_shaders.getShader('Amplitude'), 0, 0)
			widget.completed = true
			
			if not m_BestiaryFunctions.visibleCompletedTasks then
				widget:hide()
			end
		end
		
		widget.kills = kills
		widget.class = i
		widget.details = v
		
		widget.onClick = function()
			m_BestiaryFunctions.setMonsterInfo(v, kills)
			m_BestiaryFunctions.setLootList(v, kills)
			m_BestiaryFunctions.select(m_BestiaryList.bestiaryMonsterList:getChildren(), i)
		end
		
		if m_BestiaryFunctions.openByItemlist and m_BestiaryFunctions.openByItemlist[1] ~= '' then
			if v.name:lower() == m_BestiaryFunctions.openByItemlist[1]:lower() then
				monsterId = i
				
				m_BestiaryList.bestiaryMonsterList:increment()
				if m_BestiaryFunctions.openByItemlist[3] and m_BestiaryFunctions.openByItemlist[3] > 325 then
					m_BestiaryList.bestiaryClassList:increment(m_BestiaryFunctions.openByItemlist[3])
				end
			end
		elseif monsterId == 0 then
			if (kills ~= 0 or m_BestiaryFunctions.visibleUnknownMonsters) and (not widget.completed or m_BestiaryFunctions.visibleCompletedTasks) then
				monsterId = i
				m_BestiaryList.bestiaryMonsterList:increment()
			end
		end
	end
	
	local v = known[math.max(1, monsterId)]
	local kills = g_game.getLocalPlayer():getGroupId() > 1 and v.killAmount[3] or (m_BestiaryFunctions.bestiaryCache[v.name] or 0)
	if kills > 0 or m_BestiaryFunctions.visibleUnknownMonsters then
		m_BestiaryFunctions.setMonsterInfo(v, kills)
		m_BestiaryFunctions.setLootList(v, kills)
		m_BestiaryFunctions.select(m_BestiaryList.bestiaryMonsterList:getChildren(), monsterId)
	else
		m_BestiaryList.bestiaryLootInfoList:destroyChildren()
		
		local progressBar = m_BestiaryList.bestiaryMonsterInfo:getChildById('progressBar')
		for i = 1, 3 do
			local tmpWidget = progressBar:getChildById(tostring(i))
			tmpWidget:setPercent(0)
			if i == 2 then
				tmpWidget:clearText()
			end
		end
		
		for i = 1, 3 do
			local widget = m_BestiaryList.bestiaryMonsterInfo:getChildById('Stat_' .. i)
			widget:getChildById('label'):setText('?')
			widget:setTooltip('')
		end
	end
end

m_BestiaryFunctions.select = function(var, i, checkId)
	local found = 0
	for k, v in pairs(var) do
		local blink = v:getChildById('blink')
		if blink then
			if v.class == i then
				if checkId then
					return k
				end
			
				blink:show()
				v:focus()
				found = k
			else
				if not checkId then
					blink:hide()
				end
			end
		end
	end
	
	return found
end

m_BestiaryFunctions.setLootList = function(v, kills)
	m_BestiaryList.bestiaryLootInfoList:destroyChildren()
	
	local list = m_BestiaryFunctions.lootList[v.name]
	if not list then
		local tmpList = modules.game_lookat.m_LookAtFunctions.getMonstersList(v.classId)
		if not tmpList then
			return true
		end
		
		for i = 1, #tmpList do
			local var = tmpList[i]
			if v.name:lower() == var.name:lower() then
				list = var.loot
				if list then
					table.sort(list, m_BestiaryFunctions.sortByChance)
					m_BestiaryFunctions.lootList[v.name] = list
				end
				
				break
			end
		end
	end
	
	if not list then
		return true
	end
	
	for i = #list, 1, -1 do
		local var = list[i]
		if type(var.id) == 'table' then
			for j = 1, #var.id do
				m_BestiaryFunctions.addItem(var.id[j], var.chance, kills, v.killAmount)
			end
		else
			m_BestiaryFunctions.addItem(var.id, var.chance, kills, v.killAmount)
		end
	end
end

m_BestiaryFunctions.setMonsterInfo = function(v, kills)
	if m_BestiaryFunctions.openByItemlist then
		m_BestiaryFunctions.openByItemlist[1] = v.name
	else
		m_BestiaryFunctions.openByItemlist = {v.name, i}
	end
	
	m_BestiaryList.trackKills:setChecked(m_BestiaryFunctions.trackList[v.name])
	
	local name = v.description[m_BestiaryFunctions.language]
	
	local progressBar = m_BestiaryList.bestiaryMonsterInfo:getChildById('progressBar')
	local progress = {}
	for i = 1, 3 do
		progress[i] = {}
		progress[i].widget = progressBar:getChildById(tostring(i))
		progress[i].percent = 0
	end
	
	local text = kills .. ' / '
	local stage = 0
	
	if kills < v.killAmount[1] then
		text = text .. v.killAmount[1]
		progress[1].percent = kills / v.killAmount[3]
		stage = 1
	elseif kills < v.killAmount[2] then
		text = text .. v.killAmount[2]
		progress[1].percent = 100
		progress[2].percent = (kills - v.killAmount[1]) / (v.killAmount[2] - v.killAmount[1]) * 100
		stage = 2
	else
		text = text .. v.killAmount[3]
		progress[1].percent = 100
		progress[2].percent = 100
		progress[3].percent = (kills - v.killAmount[2]) / (v.killAmount[3] - v.killAmount[2]) * 100
		stage = 3
	end
	
	for i = 1, 3 do
		progress[i].widget:setPercent(progress[i].percent)
	end
	
	progress[2].widget:setText(text)
	
	local stats = {
		name,
		v.healthMax,
		v.experience,
		v.charmPoints,
		v.boss or false
	}
	
	for i = 1, #stats do
		local widget = m_BestiaryList.bestiaryMonsterInfo:getChildById('Stat_' .. i)
		local label = widget:getChildById('label')
		if stats[i] then
			widget:show()
		else
			widget:hide()
		end
		
		if stage < 2 then
			label:setText('?')
			widget:setTooltip('You need one more defeated monsters of this type to\nunlock another portion of information on them.')
		else
			if stats[i] then
				if i == 5 then
					widget:setText(tr('Boss instead monster: ') .. stats[i])
					widget:setOn(true)
					widget.onMouseRelease = function(self, mousePosition, mouseButton)
						m_BestiaryFunctions.selectMonsterByName(stats[i], MONSTER_BOSS)
					end
				else
					label:setText(stats[i])
					widget:setTooltip('')
				end
			end
		end
	end
	
	if stage < 3 then
		for i = 1, #m_BestiaryFunctions.elementsList do
			local widget = m_BestiaryList.bestiaryMonsterInfo:getChildById('elemental_' .. m_BestiaryFunctions.elementsList[i])
			widget:setText('?%')
			widget:setTooltip('You need kill more monsters of this type to\nunlock another portion of information on them.')
		end
	else
		for i = 1, #m_BestiaryFunctions.elementsList do
			local widget = m_BestiaryList.bestiaryMonsterInfo:getChildById('elemental_' .. m_BestiaryFunctions.elementsList[i])
			widget:setText('0%')
			widget:setTooltip('')
		end
		
		if v.elements then
			for i = 1, #v.elements do
				local widget = m_BestiaryList.bestiaryMonsterInfo:getChildById('elemental_' .. v.elements[i].type)
				if widget then
					widget:setText(v.elements[i].value .. '%')
				end
			end
		end
	end
end

m_BestiaryFunctions.addItem = function(itemId, chance, kills, amount)
	local it = modules.game_lookat.m_LookAtFunctions.getItemDescription(itemId)
	if not it then
		-- print(itemId)
		return true
	end
	
	local widget = g_ui.createWidget('LootItem', m_BestiaryList.bestiaryLootInfoList)
	local id = widget:getChildById('item')
	id:setItemId(it.clientId)
	
	if kills < amount[1] then
		widget:setText('?%')
		widget:getChildById('mask'):show()
		id:setOn(true)
	else
		if kills < amount[2] then
			widget:setText('?%')
		else
			widget:setText(chance / 1000 .. '%')
		end
		
		id.onMouseRelease = function(self, mousePosition, mouseButton)
			if mouseButton == MouseLeftButton then
				local var = g_game.getItemType(it)
				if var ~= 0 then
					modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(it.clientId, var)
				end
			elseif mouseButton == MouseRightButton then
				local menu = g_ui.createWidget('PopupMenu')
				menu:setGameMenu(true)
				
				local classId = getClassNameById(self.classId or self:getClassType())
				menu:addOption(tr('Look'), function() return modules.game_lookat.m_LookAtFunctions.inspectItem(self:getItem(), classId, false, self:isVirtual() or self:getItemCount() == 0) end)
				
				if m_BestiaryFunctions.isOnList(it.clientId) then
					menu:addOption(tr('Remove from loot list'), function() return m_BestiaryFunctions.sendItemToList(it.clientId, false) end)
				else
					menu:addOption(tr('Add to loot list'), function() return m_BestiaryFunctions.sendItemToList(it.clientId, true) end)
				end
				
				menu:display(mousePosition)
			end
		end
	end
end

m_BestiaryFunctions.selectMonsterByName = function(name, class)
	m_BestiaryFunctions.openByItemlist = {name, class}
	if not m_BestiaryList.window then
		modules.game_encyclopedia.m_EncyclopediaFunctions.selectById(ID_BESTIARY)
	else
		m_BestiaryFunctions.openByItemlist[3] = m_BestiaryFunctions.select(m_BestiaryList.bestiaryClassList:getChildren(), class) * 65
		m_BestiaryFunctions.addMonsters(class)
		m_BestiaryList.bestiarySearchMonsterName:clearText()
	end
end

m_BestiaryFunctions.getCombatByClass = function(class)
	local name = ''
	if class == COMBAT_PHYSICALDAMAGE then
		name = 'Physical'
	elseif class == COMBAT_DROWNDAMAGE then
		name = 'Drown'
	elseif class == COMBAT_BLEEDDAMAGE then
		name = 'Bleed'
	elseif class == COMBAT_HOLYDAMAGE then
		name = 'Holy'
	elseif class == COMBAT_DEATHDAMAGE then
		name = 'Death'
	elseif class == COMBAT_TOXICDAMAGE then
		name = 'Toxic'
	elseif class == COMBAT_ENERGYDAMAGE then
		name = 'Energy'
	elseif class == COMBAT_EARTHDAMAGE then
		name = 'Earth'
	elseif class == COMBAT_FIREDAMAGE then
		name = 'Fire'
	elseif class == COMBAT_ICEDAMAGE then
		name = 'Ice'
	end
	
	return tr(name)
end

m_BestiaryFunctions.getNameByClass = function(var)
	local class = var.id
	local name = ''
	if class == MONSTER_UNDERWATER then
		name = 'Underwaters'
	elseif class == MONSTER_ANIMAL then
		name = 'Animals'
	elseif class == MONSTER_UNDEAD then
		name = 'Undeads'
	elseif class == MONSTER_HUMAN then
		name = 'Humans'
	elseif class == MONSTER_SERPENT then
		name = 'Serpents'
	elseif class == MONSTER_DRAKEN then
		name = 'Drakens'
	elseif class == MONSTER_LIZARD then
		name = 'Lizards'
	elseif class == MONSTER_DRAGON then
		name = 'Dragons'
	elseif class == MONSTER_BOSS then
		name = 'Bosses'
	elseif class == MONSTER_MUTATED then
		name = 'Mutated'
	elseif class == MONSTER_APE then
		name = 'Apes'
	elseif class == MONSTER_DJINN then
		name = 'Djinns'
	elseif class == MONSTER_BONELORD then
		name = 'Bonelords'
	elseif class == MONSTER_MINOTAUR then
		name = 'Minotaurs'
	elseif class == MONSTER_TROLL then
		name = 'Trolls'
	elseif class == MONSTER_ORC then
		name = 'Orcs'
	elseif class == MONSTER_GOBLIN then
		name = 'Goblins'
	elseif class == MONSTER_GIANT then
		name = 'Giants'
	elseif class == MONSTER_ELF then
		name = 'Elves'
	elseif class == MONSTER_DWARF then
		name = 'Dwarves'
	elseif class == MONSTER_GEO_ELEMENTAL then
		name = 'Geo-Elementals'
	elseif class == MONSTER_PYRO_ELEMENTAL then
		name = 'Pyro-Elementals'
	elseif class == MONSTER_CRYO_ELEMENTAL then
		name = 'Cryo-Elementals'
	elseif class == MONSTER_ELECTRO_ELEMENTAL then
		name = 'Electro-Elementals'
	elseif class == MONSTER_DEMON then
		name = 'Demons'
	elseif class == MONSTER_ARACHNID then
		name = 'Arachnids'
	elseif class == MONSTER_YUAN_TI then
		name = 'Yuan-ti'
	elseif class == MONSTER_VAMPIRE then
		name = 'Vampires'
	elseif class == MONSTER_WIZARD then
		name = 'Wizards'
	elseif class == MONSTER_KYNOHEAD then
		name = 'Kynoheads'
	elseif class == MONSTER_PIRATE then
		name = 'Pirates'
	elseif class == MONSTER_PERVERTED then
		name = 'Perverted'
	elseif class == MONSTER_RATATOSK then
		name = 'Ratatosk'
	end
	
	return {tr(name), var.outfit, class}
end

m_BestiaryFunctions.sortByHealth = function(k, v)
	if m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_HIGH then
		return k.healthMax < v.healthMax
	elseif m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_LOW then
		return k.healthMax > v.healthMax
	end
end

m_BestiaryFunctions.sortByName = function(k, v)
	if m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_HIGH then
		return k.description[m_BestiaryFunctions.language] < v.description[m_BestiaryFunctions.language]
	elseif m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_LOW then
		return k.description[m_BestiaryFunctions.language] > v.description[m_BestiaryFunctions.language]
	end
end

m_BestiaryFunctions.sortByExperience = function(k, v)
	if m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_HIGH then
		return k.experience < v.experience
	elseif m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_LOW then
		return k.experience > v.experience
	end
end

m_BestiaryFunctions.sortByKillCount = function(k, v)
	local from, to = (m_BestiaryFunctions.bestiaryCache[k.name] or 0), (m_BestiaryFunctions.bestiaryCache[v.name] or 0)
	if m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_HIGH then
		return from < to
	elseif m_BestiaryList.sortby == m_BestiaryFunctions.SORTBY_LOW then
		return from > to
	end
end

m_BestiaryFunctions.destroyPopup = function()
	if m_BestiaryList.popup then
		m_BestiaryList.popup:destroy()
		m_BestiaryList.popup = nil
	end
end

m_BestiaryFunctions.createPopup = function(name)
	m_BestiaryFunctions.destroyPopup()
	
	m_BestiaryList.popup = g_ui.displayUI(name)
	m_BestiaryList.popup:focus()
	m_BestiaryList.popup:raise()
end

m_BestiaryFunctions.sendMessage = function(text)
	m_BestiaryFunctions.createPopup('popup_window')
	m_BestiaryList.popup:getChildById('description'):setText(text)
end