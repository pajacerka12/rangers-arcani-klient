m_SetsFunctions.setId = 1
m_SetsFunctions.weaponSlot = 5
m_SetsFunctions.shieldSlot = 6
m_SetsFunctions.percentId = {ITEM_ABSORB, ITEM_INCREMENT, ITEM_CRITICAL, ITEM_CRITICAL_HIT_MULTIPLIER, ITEM_BLOCK_CRITICAL, ITEM_DOUBLE_HIT, ITEM_LUCK, ITEM_BONUS_DAMAGE}

m_SetsFunctions.SlotHead = 1
m_SetsFunctions.SlotBody = 2
m_SetsFunctions.SlotLegs = 3
m_SetsFunctions.SlotFeet = 4
m_SetsFunctions.SlotLeft = 5
m_SetsFunctions.SlotRight = 6
m_SetsFunctions.SlotNeck = 7
m_SetsFunctions.SlotBack = 8
m_SetsFunctions.SlotRing = 9
m_SetsFunctions.SlotSRing = 10

m_SetsFunctions.SlotsStyles = {
  ['slot1'] = "HeadSetsSlot",
  ['slot2'] = "BodySetsSlot",
  ['slot3'] = "LegsSetsSlot",
  ['slot4'] = "FeetSetsSlot",
  ['slot5'] = "LeftSetsSlot",
  ['slot6'] = "RightSetsSlot",
  ['slot7'] = "NeckSetsSlot",
  ['slot8'] = "BackSetsSlot",
  ['slot9'] = "FingerSetsSlot"
}

m_SetsFunctions.onLoad = function()
	
end

m_SetsFunctions.onUnload = function()
	
end

m_SetsFunctions.onGameEnd = function()
	
end

m_SetsFunctions.destroy = function()
	if m_SetsList.window then
		m_SetsList.window:destroy()
		m_SetsList.window = nil
		
		m_SetsList = {}
	end
end

m_SetsFunctions.open = function(parent)
	if m_SetsList.window then
		m_SetsFunctions.destroy()
	else
		m_SetsList.window = g_ui.createWidget('SetsWindow', parent)
		m_SetsList.expandButton = m_SetsList.window:getChildById('expandButton')
		m_SetsList.expandItems = m_SetsList.window:getChildById('expandItems')
		m_SetsList.setsList = m_SetsList.window:getChildById('setsList')
		m_SetsList.description = m_SetsList.window:getChildById('description'):getChildById('list')
		
		m_SetsList.slots = {}
		for i = 1, 9 do
			m_SetsList.slots[i] = m_SetsList.window:getChildById('slot' .. i)
		end
		
		m_SetsFunctions.execute()
		m_SetsFunctions.updateList()
		m_SetsFunctions.updateSelect()
	end
end

m_SetsFunctions.getSlotIdByName = function(name)
	if name == 'necklace' then
		return m_SetsFunctions.SlotNeck
	elseif name == 'head' then
		return m_SetsFunctions.SlotHead
	elseif name == 'body' then
		return m_SetsFunctions.SlotBody
	elseif name == 'legs' then
		return m_SetsFunctions.SlotLegs
	elseif name == 'feet' then
		return m_SetsFunctions.SlotFeet
	elseif name == 'shield' then
		return m_SetsFunctions.SlotRight
	elseif name == 'onehand' or name == 'wand' or name == 'twohand' then
		return m_SetsFunctions.SlotLeft
	elseif name == 'backpack' then
		return m_SetsFunctions.SlotBack
	elseif name == 'ring' then
		return m_SetsFunctions.SlotRing
	end
end

m_SetsFunctions.select = function(self)
	if not self:getItem() then
		return true
	end

	local widget = self:getChildById('animatedSquare')
	if widget:isVisible() then
		widget:hide()
	else
		widget:show()
	end
	
	m_SetsFunctions.updateDescription()
end

m_SetsFunctions.selectById = function(self)
	m_SetsFunctions.setId = self.id
	m_SetsFunctions.selectPage(0, true)
end

m_SetsFunctions.updateList = function()
	for i = 1, #sets do
		local widget = g_ui.createWidget('SetsListItem', m_SetsList.setsList)
		widget:setItemId(sets[i].clientIdList[2])
		widget:setId(i)
		widget.id = i
	end
end

m_SetsFunctions.updateSelect = function()
	if m_SetsList.lastChecked then
		m_SetsList.lastChecked:getChildById('animatedSquare'):hide()
	end
	
	m_SetsList.lastChecked = m_SetsList.setsList:getChildById(m_SetsFunctions.setId)
	m_SetsList.lastChecked:getChildById('animatedSquare'):show()
end

m_SetsFunctions.selectPage = function(value, ignore)
	for i = 1, 9 do
		local widget = m_SetsList.slots[i]
		widget:setItemId(0)
		widget:setStyle(m_SetsFunctions.SlotsStyles[widget:getId()])
		widget:getChildById('animatedSquare'):hide()
		widget.itemId = 0
		widget.clientId = 0
		widget.slotType = false
	end
	
	m_SetsFunctions.execute()
	if m_SetsList.expandButton:isOn() then
		m_SetsFunctions.expand(m_SetsList.expandButton)
	end
	
	m_SetsFunctions.updateSelect()
end

m_SetsFunctions.expand = function(self)
	local var = self:isOn()
	
	m_SetsList.expandItems:destroyChildren()
	for i = 1, #self.items do
		if not m_SetsList.currentWeapon or m_SetsList.currentWeapon ~= self.items[i][1] then
			local widget = g_ui.createWidget('SetsExpandItem', m_SetsList.expandItems)
			widget:setItemId(self.items[i][1])
			widget.itemId = self.items[i][1]
			widget.serverId = self.items[i][2]
			widget.slotType = self.items[i][3]
		end
	end
	
	if var then
		m_SetsList.expandItems:hide()
	else
		m_SetsList.expandItems:setHeight((#self.items - 1) * 48)
		m_SetsList.expandItems:show()
	end
	
	self:setOn(not var)
end

m_SetsFunctions.updateShield = function(slotType)
	local widget = m_SetsList.slots[m_SetsFunctions.shieldSlot]
	if slotType == 'two-handed' then
		widget:getChildById('animatedSquare'):hide()
		widget:setStyle(m_SetsFunctions.SlotsStyles[widget:getId()])
		widget:setItemId(0)
	elseif widget.clientId ~= 0 then
		-- widget:getChildById('animatedSquare'):show()
		widget:setStyle('SetsPartItem')
		widget:setItemId(widget.clientId)
	end
end

m_SetsFunctions.choose = function(self)
	m_SetsFunctions.updateShield(self.slotType)
	
	local widget = m_SetsList.slots[m_SetsFunctions.weaponSlot]
	widget:setItemId(self.itemId)
	widget.itemId = self.serverId
	
	m_SetsList.currentWeapon = self.itemId
	m_SetsFunctions.expand(m_SetsList.expandButton)
	
	m_SetsFunctions.updateDescription()
end

m_SetsFunctions.execute = function()
	local list = sets[m_SetsFunctions.setId]
	if not list then
		m_SetsFunctions.setId = 1
		list = sets[m_SetsFunctions.setId]
	end
	
	m_SetsList.expandButton.items = {}
	for i = 1, #list.clientIdList do
		local it = modules.game_lookat.m_LookAtFunctions.getItemDescription(list.list[i])
		if it then
			local slotType = it.attributes.slottype
			if slotType == 'hand' or slotType == 'two-handed' then
				slotType = it.attributes.weapontype
			end
			
			local slot = m_SetsFunctions.getSlotIdByName(slotType)
			if slot == m_SetsFunctions.SlotLeft then
				table.insert(m_SetsList.expandButton.items, {list.clientIdList[i], list.list[i], it.attributes.slottype})
				m_SetsList.currentWeapon = list.clientIdList[i]
			end
			
			local widget = m_SetsList.slots[slot]
			widget:setItemId(list.clientIdList[i])
			widget:getItem():setCategoryId(g_game.getItemType(it))
			widget:setStyle('SetsPartItem')
			widget:getChildById('animatedSquare'):show()
			widget.itemId = list.list[i]
			widget.clientId = list.clientIdList[i]
			widget.slotType = it.attributes.slottype
		end
	end
	
	m_SetsFunctions.updateShield(m_SetsList.slots[m_SetsFunctions.weaponSlot].slotType)
	
	if #m_SetsList.expandButton.items > 1 then
		m_SetsList.expandButton:show()
	else
		m_SetsList.expandButton:hide()
	end
	
	m_SetsFunctions.updateDescription(list)
end

m_SetsFunctions.updateDescription = function(list)
	if not list then
		list = sets[m_SetsFunctions.setId]
		if not list then
			return true
		end
	end
	
	m_SetsList.description:destroyChildren()
	
	local attributes = {}
	for i = 1, #list.attributes do
		local v = list.attributes[i]
		
		local amount = 0
		local found = 0
		for i = 1, 9 do
			local item = m_SetsList.slots[i]
			if item:getChildById('animatedSquare'):isVisible() then
				amount = amount + 1
				
				if v.requiredList and isInArray(v.requiredList, item.itemId) then
					found = found + 1
				end
			end
		end
		
		if found >= (v.requiredList and #v.requiredList or 0) and amount >= v.itemAmount then
			local id = 0
			local value = 0
			local type = 0
			if v.skill then
				id = ITEM_SKILL
				value = v.skill.value
				type = modules.game_lookat.m_LookAtFunctions.getSkillId(v.skill.type) + 1
			elseif v.absorb then
				id = ITEM_ABSORB
				value = v.absorb.value
				type = modules.game_lookat.m_LookAtFunctions.getCombatId(v.absorb.type)
			elseif v.bonusdamage then
				id = ITEM_BONUS_DAMAGE
				value = v.bonusdamage.value
				type = modules.game_lookat.m_LookAtFunctions.getCombatId(v.bonusdamage.type)
			elseif v.increment then
				id = ITEM_INCREMENT
				value = v.increment.value
				type = modules.game_lookat.m_LookAtFunctions.getCombatId(v.increment.type)
			elseif v.monsterdefense then
				id = ITEM_MONSTER_DEFENSE
				value = v.monsterdefense.value
				type = modules.game_lookat.m_LookAtFunctions.getMonsterClassId(v.monsterdefense.type)
			elseif v.criticalhitchance then
				id = ITEM_CRITICAL
				value = v.criticalhitchance
			elseif v.criticalhitmultiplier then
				id = ITEM_CRITICAL_HIT_MULTIPLIER
				value = v.criticalhitmultiplier
			elseif v.chanceblockcritical then
				id = ITEM_BLOCK_CRITICAL
				value = v.chanceblockcritical
			elseif v.doublehit then
				id = ITEM_DOUBLE_HIT
				value = v.doublehit / 10
			elseif v.speed then
				id = ITEM_SPEED
				value = v.speed
			elseif v.attackspeed then
				id = ITEM_ATTACK_SPEED
				value = v.attackspeed * 10
			elseif v.luck then
				id = ITEM_LUCK
				value = v.luck / 100
			elseif v.regeneration then
				if v.regeneration.type == 'health' then
					id = ITEM_REGEN_HEALTH
				else
					id = ITEM_REGEN_MANA
				end
				
				value = v.regeneration.value
				type = modules.game_lookat.m_LookAtFunctions.getStatId(v.regeneration.type)
			elseif v.stat then
				if v.stat.type == 'health' then
					id = ITEM_MAXHEALTH
				else
					id = ITEM_MAXMANA
				end
				
				value = v.stat.value
				type = modules.game_lookat.m_LookAtFunctions.getStatId(v.stat.type)
			end
			
			local begin = true
			for i = 1, #attributes do
				if attributes[i].id == id and attributes[i].type == type then
					attributes[i].value = attributes[i].value + value
					begin = false
					break
				end
			end
			
			if begin then
				local i = #attributes + 1
				attributes[i] = {}
				attributes[i].id = id
				attributes[i].value = value
				attributes[i].type = type
			end
		end
	end
	
	for i = 1, #attributes do
		local v = attributes[i]
		local var = modules.game_lookat.m_LookAtFunctions.getAttributeDetails(v.id)
		local widget = g_ui.createWidget('SetsAttributeLabel', m_SetsList.description)
		
		local icon = widget:getChildById('icon')
		if type(var.icon) == 'table' then
			modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, var.icon[v.type])
		else
			modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, var.icon)
		end
		
		local label = widget:getChildById('label')
		local value = (v.value > 0 and '+' or '') .. v.value
		if isInArray(m_SetsFunctions.percentId, v.id) then
			value = value .. '%'
		end
		
		if type(var.description) == 'table' then
			if var.firstDescription then
				label:setText(tr(var.firstDescription, value) .. var.description[v.type])
			else
				label:setText(var.description[v.type] .. ' ' .. value)
			end
		else
			if var.opposite then
				label:setText(value .. ' ' .. var.description)
			else
				label:setText(var.description .. ' ' .. value)
			end
		end
	end
end
