m_NpcGlosaryFunctions.npcGlosaryList = {
	{ name = 'Redever', position = {x = 1281, y = 824, z = 3} },
	{ name = 'Redrich', position = {x = 1305, y = 824, z = 3} },
	{ name = 'Kethor', position = {x = 1281, y = 843, z = 3} },
	{ name = 'Isumson', position = {x = 1304, y = 842, z = 3} },
	{ name = 'Fand', position = {x = 1148, y = 903, z = 3} },
	{ name = 'Daggerspell', position = {x = 1542, y = 766, z = 4} },
	{ name = 'Hyllus', position = {x = 1288, y = 829, z = 4} },
	{ name = 'Theudofrid', position = {x = 1292, y = 828, z = 4} },
	{ name = 'Sir Wessax', position = {x = 1293, y = 828, z = 4} },
	{ name = 'Francesca', position = {x = 1294, y = 828, z = 4} },
	{ name = 'Arnald', position = {x = 1291, y = 829, z = 4} },
	{ name = 'Erramun', position = {x = 1295, y = 829, z = 4} },
	{ name = 'Ignatius', position = {x = 1293, y = 837, z = 4} },
	{ name = 'Hermana', position = {x = 1287, y = 836, z = 4} },
	{ name = 'Laelius', position = {x = 1302, y = 838, z = 4} },
	{ name = 'Volunteer Rhgart', position = {x = 1196, y = 913, z = 4} },
	{ name = 'Naffain', position = {x = 1274, y = 1001, z = 4} },
	{ name = 'Sara', position = {x = 1296, y = 567, z = 5} },
	{ name = 'Alp', position = {x = 1301, y = 568, z = 5} },
	{ name = 'Petteri', position = {x = 1306, y = 568, z = 5} },
	{ name = 'Visa', position = {x = 1312, y = 570, z = 5} },
	{ name = 'Tenho', position = {x = 1294, y = 577, z = 5} },
	{ name = 'Juva', position = {x = 1302, y = 580, z = 5} },
	{ name = 'Turo', position = {x = 1303, y = 580, z = 5} },
	{ name = 'Utu', position = {x = 1291, y = 831, z = 5} },
	{ name = 'Zephyros', position = {x = 1287, y = 833, z = 5} },
	{ name = 'Salomon', position = {x = 1298, y = 835, z = 5} },
	{ name = 'Marian', position = {x = 1286, y = 840, z = 5} },
	{ name = 'Motte', position = {x = 1288, y = 840, z = 5} },
	{ name = 'Prizio', position = {x = 1326, y = 843, z = 5} },
	{ name = 'Florian', position = {x = 1377, y = 842, z = 5} },
	{ name = 'Usebio', position = {x = 1401, y = 861, z = 5} },
	{ name = 'Premysl', position = {x = 1401, y = 880, z = 5} },
	{ name = 'Vectur', position = {x = 1309, y = 885, z = 5} },
	{ name = 'Indow', position = {x = 1309, y = 903, z = 5} },
	{ name = 'Vojtech', position = {x = 1403, y = 899, z = 5} },
	{ name = 'Ehenu', position = {x = 1314, y = 900, z = 5} },
	{ name = 'Shemen', position = {x = 1314, y = 901, z = 5} },
	{ name = 'Ibrahim', position = {x = 1315, y = 907, z = 5} },
	{ name = 'Stanislav', position = {x = 1401, y = 918, z = 5} },
	{ name = 'Larenton', position = {x = 1295, y = 918, z = 5} },
	{ name = 'Hyrum', position = {x = 1314, y = 940, z = 5} },
	{ name = 'Jebel', position = {x = 1325, y = 940, z = 5} },
	{ name = 'Forenthen', position = {x = 1207, y = 1128, z = 5} },
	{ name = 'Graden', position = {x = 1209, y = 1134, z = 5} },
	{ name = 'Markens', position = {x = 1321, y = 550, z = 6} },
	{ name = 'Jankus', position = {x = 1319, y = 552, z = 6} },
	{ name = 'Osmo', position = {x = 1323, y = 553, z = 6} },
	{ name = 'Roberth', position = {x = 1323, y = 554, z = 6} },
	{ name = 'Yarthan', position = {x = 1321, y = 557, z = 6} },
	{ name = 'Tomi', position = {x = 1313, y = 560, z = 6} },
	{ name = 'Rauha', position = {x = 1319, y = 562, z = 6} },
	{ name = 'Arja', position = {x = 1316, y = 564, z = 6} },
	{ name = 'Beliba', position = {x = 1296, y = 704, z = 6} },
	{ name = 'Thorge', position = {x = 1561, y = 713, z = 6} },
	{ name = 'Gricio', position = {x = 1562, y = 721, z = 6} },
	{ name = 'Proclay', position = {x = 1506, y = 767, z = 6} },
	{ name = 'Dantven', position = {x = 1527, y = 777, z = 6} },
	{ name = 'Owen', position = {x = 1462, y = 825, z = 6} },
	{ name = 'Silvius', position = {x = 1293, y = 825, z = 6} },
	{ name = 'Sabina', position = {x = 1298, y = 830, z = 6} },
	{ name = 'Lillian', position = {x = 1298, y = 833, z = 6} },
	{ name = 'Chares', position = {x = 1299, y = 837, z = 6} },
	{ name = 'Cyprianus', position = {x = 1303, y = 838, z = 6} },
	{ name = 'Iolanthe', position = {x = 1281, y = 841, z = 6} },
	{ name = 'Nicodemus', position = {x = 1282, y = 842, z = 6} },
	{ name = 'Gersenda', position = {x = 1280, y = 843, z = 6} },
	{ name = 'Liakopulos', position = {x = 1392, y = 852, z = 6} },
	{ name = 'Risinger', position = {x = 1380, y = 856, z = 6} },
	{ name = 'Bartolomeus', position = {x = 1270, y = 866, z = 6} },
	{ name = 'Rhoben', position = {x = 1275, y = 867, z = 6} },
	{ name = 'Mia', position = {x = 1425, y = 875, z = 6} },
	{ name = 'Wade', position = {x = 1415, y = 877, z = 6} },
	{ name = 'Adarth', position = {x = 1413, y = 879, z = 6} },
	{ name = 'Lovi', position = {x = 1330, y = 878, z = 6} },
	{ name = 'Devo', position = {x = 1335, y = 881, z = 6} },
	{ name = 'Bauzis', position = {x = 1314, y = 887, z = 6} },
	{ name = 'Lara', position = {x = 1330, y = 888, z = 6} },
	{ name = 'Veer', position = {x = 1365, y = 887, z = 6} },
	{ name = 'Klein', position = {x = 1328, y = 899, z = 6} },
	{ name = 'Khoz', position = {x = 1304, y = 902, z = 6} },
	{ name = 'Faramond', position = {x = 1360, y = 912, z = 6} },
	{ name = 'Chantalle', position = {x = 1326, y = 915, z = 6} },
	{ name = 'Semmir', position = {x = 1390, y = 930, z = 6} },
	{ name = 'Fedro', position = {x = 1390, y = 938, z = 6} },
	{ name = 'Zefiro', position = {x = 1390, y = 939, z = 6} },
	{ name = 'Valeriana', position = {x = 1348, y = 940, z = 6} },
	{ name = 'Julian', position = {x = 1369, y = 945, z = 6} },
	{ name = 'Elenor', position = {x = 1346, y = 958, z = 6} },
	{ name = 'Valens', position = {x = 1382, y = 958, z = 6} },
	{ name = 'Galnar', position = {x = 1342, y = 1043, z = 6} },
	{ name = 'Damord', position = {x = 1341, y = 1053, z = 6} },
	{ name = 'Petr', position = {x = 1408, y = 1120, z = 6} },
	{ name = 'Vestonator', position = {x = 1367, y = 1137, z = 6} },
	{ name = 'Jellena', position = {x = 1352, y = 1147, z = 6} },
	{ name = 'Henry', position = {x = 1294, y = 1277, z = 6} },
	{ name = 'Mensen', position = {x = 1192, y = 1291, z = 6} },
	{ name = 'Unvensen', position = {x = 1190, y = 1293, z = 6} },
	{ name = 'Tarnfaeln', position = {x = 1198, y = 1292, z = 6} },
	{ name = 'Hondvor', position = {x = 1365, y = 1378, z = 6} },
	{ name = 'Bryer', position = {x = 1379, y = 1386, z = 6} },
	{ name = 'Ocid', position = {x = 1368, y = 1401, z = 6} },
	{ name = 'Juha', position = {x = 1305, y = 545, z = 7} },
	{ name = 'Polat', position = {x = 1314, y = 546, z = 7} },
	{ name = 'Gerden', position = {x = 1306, y = 549, z = 7} },
	{ name = 'Assith', position = {x = 1319, y = 549, z = 7} },
	{ name = 'Anu', position = {x = 1304, y = 552, z = 7} },
	{ name = 'Harkan', position = {x = 1313, y = 551, z = 7} },
	{ name = 'Avalax', position = {x = 1313, y = 552, z = 7} },
	{ name = 'Unto', position = {x = 1298, y = 553, z = 7} },
	{ name = 'Sonja', position = {x = 1309, y = 552, z = 7} },
	{ name = 'Henri', position = {x = 1294, y = 557, z = 7} },
	{ name = 'Pia', position = {x = 1303, y = 557, z = 7} },
	{ name = 'Vihtori', position = {x = 1304, y = 558, z = 7} },
	{ name = 'Jarkko', position = {x = 1299, y = 562, z = 7} },
	{ name = 'Vernon', position = {x = 1381, y = 602, z = 7} },
	{ name = 'Rubyscire', position = {x = 1383, y = 605, z = 7} },
	{ name = 'Hyphving', position = {x = 1389, y = 607, z = 7} },
	{ name = 'Petr Orilion', position = {x = 1562, y = 701, z = 7} },
	{ name = 'Arton', position = {x = 1563, y = 701, z = 7} },
	{ name = 'Assith', position = {x = 1300, y = 707, z = 7} },
	{ name = 'Hoysus', position = {x = 1289, y = 713, z = 7} },
	{ name = 'Cuthbert', position = {x = 1560, y = 712, z = 7} },
	{ name = 'Rumir', position = {x = 1301, y = 714, z = 7} },
	{ name = 'Mara', position = {x = 1544, y = 714, z = 7} },
	{ name = 'Tina', position = {x = 1554, y = 719, z = 7} },
	{ name = 'Arius', position = {x = 1557, y = 721, z = 7} },
	{ name = 'Eordrin', position = {x = 1547, y = 721, z = 7} },
	{ name = 'Lava', position = {x = 1564, y = 721, z = 7} },
	{ name = 'Aererd', position = {x = 1564, y = 722, z = 7} },
	{ name = 'Axerk', position = {x = 1564, y = 726, z = 7} },
	{ name = 'Concio', position = {x = 1555, y = 731, z = 7} },
	{ name = 'Baya', position = {x = 1234, y = 734, z = 7} },
	{ name = 'Ilbak', position = {x = 1232, y = 735, z = 7} },
	{ name = 'Othman', position = {x = 1232, y = 738, z = 7} },
	{ name = 'Kahina', position = {x = 1239, y = 738, z = 7} },
	{ name = 'Svela', position = {x = 1588, y = 750, z = 7} },
	{ name = 'Dusek', position = {x = 1594, y = 758, z = 7} },
	{ name = 'Garnart', position = {x = 1603, y = 762, z = 7} },
	{ name = 'Dena', position = {x = 1599, y = 766, z = 7} },
	{ name = 'Maxigashi', position = {x = 1522, y = 768, z = 7} },
	{ name = 'Wrad', position = {x = 1588, y = 769, z = 7} },
	{ name = 'Jana', position = {x = 1584, y = 770, z = 7} },
	{ name = 'Bertiers', position = {x = 1583, y = 771, z = 7} },
	{ name = 'Cermes', position = {x = 1130, y = 773, z = 7} },
	{ name = 'Zelbe', position = {x = 1142, y = 773, z = 7} },
	{ name = 'Den', position = {x = 1512, y = 771, z = 7} },
	{ name = 'Drunkan', position = {x = 1133, y = 774, z = 7} },
	{ name = 'Difmos', position = {x = 1135, y = 774, z = 7} },
	{ name = 'Ganir', position = {x = 1146, y = 773, z = 7} },
	{ name = 'Okenos', position = {x = 1306, y = 773, z = 7} },
	{ name = 'Drahn', position = {x = 1141, y = 776, z = 7} },
	{ name = 'Maekus', position = {x = 1148, y = 779, z = 7} },
	{ name = 'Francois', position = {x = 1314, y = 779, z = 7} },
	{ name = 'Ubus', position = {x = 1141, y = 779, z = 7} },
	{ name = 'Juan', position = {x = 1299, y = 779, z = 7} },
	{ name = 'Kyjun', position = {x = 1131, y = 785, z = 7} },
	{ name = 'Sebris', position = {x = 1151, y = 783, z = 7} },
	{ name = 'Ariane', position = {x = 1308, y = 785, z = 7} },
	{ name = 'Dethron', position = {x = 1328, y = 785, z = 7} },
	{ name = 'Irtyx', position = {x = 1143, y = 784, z = 7} },
	{ name = 'Ganir', position = {x = 1145, y = 786, z = 7} },
	{ name = 'Ethar', position = {x = 1151, y = 787, z = 7} },
	{ name = 'Ziggy', position = {x = 1309, y = 793, z = 7} },
	{ name = 'Livian', position = {x = 1335, y = 795, z = 7} },
	{ name = 'Flaen', position = {x = 1319, y = 795, z = 7} },
	{ name = 'Dijkstra', position = {x = 1331, y = 797, z = 7} },
	{ name = 'Harnald', position = {x = 1315, y = 801, z = 7} },
	{ name = 'Uray', position = {x = 1458, y = 816, z = 7} },
	{ name = 'Vedget', position = {x = 1374, y = 820, z = 7} },
	{ name = 'Grunfeld', position = {x = 1378, y = 819, z = 7} },
	{ name = 'Drazen', position = {x = 1464, y = 823, z = 7} },
	{ name = 'Dominic', position = {x = 1462, y = 827, z = 7} },
	{ name = 'Okha', position = {x = 1464, y = 827, z = 7} },
	{ name = 'Brigida', position = {x = 1300, y = 829, z = 7} },
	{ name = 'Verena', position = {x = 1300, y = 831, z = 7} },
	{ name = 'Alammred', position = {x = 1224, y = 831, z = 7} },
	{ name = 'Aesop', position = {x = 1298, y = 836, z = 7} },
	{ name = 'Kastinan', position = {x = 1490, y = 836, z = 7} },
	{ name = 'Leontinus', position = {x = 1359, y = 846, z = 7} },
	{ name = 'Sajer', position = {x = 1301, y = 849, z = 7} },
	{ name = 'Emil', position = {x = 1289, y = 847, z = 7} },
	{ name = 'Rahel', position = {x = 1289, y = 850, z = 7} },
	{ name = 'Ductor', position = {x = 1380, y = 850, z = 7} },
	{ name = 'Roland', position = {x = 1346, y = 850, z = 7} },
	{ name = 'Lodmon', position = {x = 1125, y = 852, z = 7} },
	{ name = 'Gaiane', position = {x = 1281, y = 850, z = 7} },
	{ name = 'Tobias', position = {x = 1393, y = 851, z = 7} },
	{ name = 'Sabbatius', position = {x = 1357, y = 855, z = 7} },
	{ name = 'Miros', position = {x = 1478, y = 857, z = 7} },
	{ name = 'Yaze', position = {x = 1341, y = 857, z = 7} },
	{ name = 'Stailim', position = {x = 1324, y = 859, z = 7} },
	{ name = 'Luka', position = {x = 1285, y = 862, z = 7} },
	{ name = 'Tran', position = {x = 1324, y = 866, z = 7} },
	{ name = 'Saral', position = {x = 1293, y = 867, z = 7} },
	{ name = 'Malia', position = {x = 1349, y = 866, z = 7} },
	{ name = 'Tarnaelt', position = {x = 1268, y = 868, z = 7} },
	{ name = 'Abithar', position = {x = 1272, y = 869, z = 7} },
	{ name = 'Virtelion', position = {x = 1388, y = 871, z = 7} },
	{ name = 'Kael', position = {x = 1461, y = 873, z = 7} },
	{ name = 'Martyrius', position = {x = 1295, y = 872, z = 7} },
	{ name = 'Zdenka', position = {x = 1295, y = 873, z = 7} },
	{ name = 'Kabathar', position = {x = 1283, y = 873, z = 7} },
	{ name = 'Quilse', position = {x = 1276, y = 877, z = 7} },
	{ name = 'Maron', position = {x = 1391, y = 876, z = 7} },
	{ name = 'Chalcil', position = {x = 1358, y = 877, z = 7} },
	{ name = 'Garandriel', position = {x = 1287, y = 881, z = 7} },
	{ name = 'Hosseyni', position = {x = 1360, y = 879, z = 7} },
	{ name = 'Brahim', position = {x = 1358, y = 880, z = 7} },
	{ name = 'Harry', position = {x = 1364, y = 882, z = 7} },
	{ name = 'Ahag', position = {x = 1358, y = 883, z = 7} },
	{ name = 'Pauline', position = {x = 1416, y = 882, z = 7} },
	{ name = 'Gerwazy', position = {x = 1365, y = 885, z = 7} },
	{ name = 'Clerebold', position = {x = 1331, y = 888, z = 7} },
	{ name = 'Hayn', position = {x = 1401, y = 887, z = 7} },
	{ name = 'Adonia', position = {x = 1367, y = 891, z = 7} },
	{ name = 'Tanni', position = {x = 1368, y = 891, z = 7} },
	{ name = 'Sedhor', position = {x = 1292, y = 892, z = 7} },
	{ name = 'Azmelqart', position = {x = 1408, y = 895, z = 7} },
	{ name = 'Anne', position = {x = 1477, y = 894, z = 7} },
	{ name = 'Kristo', position = {x = 1477, y = 895, z = 7} },
	{ name = 'Thelber', position = {x = 1325, y = 895, z = 7} },
	{ name = 'Ulo', position = {x = 1452, y = 898, z = 7} },
	{ name = 'Raimunda', position = {x = 1392, y = 900, z = 7} },
	{ name = 'Tiglight', position = {x = 1302, y = 900, z = 7} },
	{ name = 'Klement', position = {x = 1342, y = 900, z = 7} },
	{ name = 'Pavish', position = {x = 1419, y = 900, z = 7} },
	{ name = 'Probo', position = {x = 1437, y = 905, z = 7} },
	{ name = 'Terpandro', position = {x = 1446, y = 907, z = 7} },
	{ name = 'Andre', position = {x = 1482, y = 907, z = 7} },
	{ name = 'Ember', position = {x = 1421, y = 907, z = 7} },
	{ name = 'Toby', position = {x = 1424, y = 909, z = 7} },
	{ name = 'Bernard', position = {x = 1415, y = 911, z = 7} },
	{ name = 'Adriana', position = {x = 1303, y = 910, z = 7} },
	{ name = 'Orlando', position = {x = 1311, y = 912, z = 7} },
	{ name = 'Rauno', position = {x = 1407, y = 910, z = 7} },
	{ name = 'Timmu', position = {x = 1407, y = 914, z = 7} },
	{ name = 'Haszkel', position = {x = 1343, y = 914, z = 7} },
	{ name = 'Reina', position = {x = 1359, y = 912, z = 7} },
	{ name = 'Saturas', position = {x = 1325, y = 913, z = 7} },
	{ name = 'Hieronim', position = {x = 1354, y = 916, z = 7} },
	{ name = 'Liliet', position = {x = 1414, y = 916, z = 7} },
	{ name = 'Philon', position = {x = 1409, y = 918, z = 7} },
	{ name = 'Sollya', position = {x = 1371, y = 921, z = 7} },
	{ name = 'Herbet', position = {x = 1392, y = 924, z = 7} },
	{ name = 'Kerena', position = {x = 1305, y = 924, z = 7} },
	{ name = 'Izavel', position = {x = 1391, y = 927, z = 7} },
	{ name = 'Hawkyr', position = {x = 1312, y = 926, z = 7} },
	{ name = 'Golfas', position = {x = 1378, y = 930, z = 7} },
	{ name = 'Lavue', position = {x = 1317, y = 936, z = 7} },
	{ name = 'Yakhia', position = {x = 1371, y = 938, z = 7} },
	{ name = 'Calixtus', position = {x = 1365, y = 939, z = 7} },
	{ name = 'Danel', position = {x = 1387, y = 938, z = 7} },
	{ name = 'Ragins', position = {x = 1340, y = 946, z = 7} },
	{ name = 'Jenny', position = {x = 1341, y = 949, z = 7} },
	{ name = 'Edward', position = {x = 1336, y = 949, z = 7} },
	{ name = 'Galileo', position = {x = 1378, y = 949, z = 7} },
	{ name = 'Lorkan Hak', position = {x = 1212, y = 953, z = 7} },
	{ name = 'Renna', position = {x = 1347, y = 956, z = 7} },
	{ name = 'Anja', position = {x = 1372, y = 958, z = 7} },
	{ name = 'Jaremar', position = {x = 1335, y = 972, z = 7} },
	{ name = 'Edvard', position = {x = 1316, y = 978, z = 7} },
	{ name = 'Bawlor', position = {x = 1331, y = 981, z = 7} },
	{ name = 'Genovefa', position = {x = 1339, y = 980, z = 7} },
	{ name = 'Faliana', position = {x = 1326, y = 983, z = 7} },
	{ name = 'Godhon', position = {x = 1335, y = 983, z = 7} },
	{ name = 'Onald', position = {x = 1313, y = 987, z = 7} },
	{ name = 'Harold', position = {x = 1301, y = 997, z = 7} },
	{ name = 'Warten', position = {x = 1269, y = 1000, z = 7} },
	{ name = 'Evraal', position = {x = 1173, y = 1022, z = 7} },
	{ name = 'Syslaven', position = {x = 1399, y = 1053, z = 7} },
	{ name = 'Rafael', position = {x = 1327, y = 1059, z = 7} },
	{ name = 'Thaomir', position = {x = 1387, y = 1061, z = 7} },
	{ name = 'Madabolt', position = {x = 1414, y = 1062, z = 7} },
	{ name = 'Vonten', position = {x = 1402, y = 1065, z = 7} },
	{ name = 'Borek', position = {x = 1404, y = 1099, z = 7} },
	{ name = 'Viktor', position = {x = 1397, y = 1102, z = 7} },
	{ name = 'Guerran', position = {x = 1395, y = 1104, z = 7} },
	{ name = 'Cold Hearted', position = {x = 1404, y = 1103, z = 7} },
	{ name = 'Fisher', position = {x = 1081, y = 1107, z = 7} },
	{ name = 'Fisher', position = {x = 1207, y = 1117, z = 7} },
	{ name = 'Nubolir', position = {x = 1410, y = 1109, z = 7} },
	{ name = 'Makaio', position = {x = 1391, y = 1109, z = 7} },
	{ name = 'Sharp Blade', position = {x = 1397, y = 1113, z = 7} },
	{ name = 'Red Baron', position = {x = 1397, y = 1121, z = 7} },
	{ name = 'Karlon', position = {x = 1201, y = 1132, z = 7} },
	{ name = 'Ravender', position = {x = 1366, y = 1135, z = 7} },
	{ name = 'Belona', position = {x = 1358, y = 1136, z = 7} },
	{ name = 'Metrenis', position = {x = 1208, y = 1138, z = 7} },
	{ name = 'Havoy', position = {x = 1379, y = 1137, z = 7} },
	{ name = 'Nynthus', position = {x = 1386, y = 1141, z = 7} },
	{ name = 'Levhan', position = {x = 1377, y = 1148, z = 7} },
	{ name = 'Qadir', position = {x = 1237, y = 1196, z = 7} },
	{ name = 'Salim', position = {x = 1315, y = 1225, z = 7} },
	{ name = 'Cerber', position = {x = 1310, y = 1217, z = 7} },
	{ name = 'Ari', position = {x = 1321, y = 1220, z = 7} },
	{ name = 'Ralker', position = {x = 1389, y = 1232, z = 7} },
	{ name = 'Ralker back', position = {x = 1402, y = 1221, z = 7} },
	{ name = 'Bianka', position = {x = 1225, y = 1224, z = 7} },
	{ name = 'Gretos', position = {x = 1316, y = 1241, z = 7} },
	{ name = 'Challin', position = {x = 1332, y = 1250, z = 7} },
	{ name = 'Hanward', position = {x = 1318, y = 1258, z = 7} },
	{ name = 'Tom', position = {x = 1341, y = 1272, z = 7} },
	{ name = 'Soya', position = {x = 1294, y = 1273, z = 7} },
	{ name = 'Coloven', position = {x = 1391, y = 1277, z = 7} },
	{ name = 'Redgard', position = {x = 1302, y = 1281, z = 7} },
	{ name = 'Karol', position = {x = 1313, y = 1283, z = 7} },
	{ name = 'Sofia', position = {x = 1285, y = 1285, z = 7} },
	{ name = 'Zeghyrdan', position = {x = 1197, y = 1288, z = 7} },
	{ name = 'Phassya', position = {x = 1202, y = 1288, z = 7} },
	{ name = 'Taja', position = {x = 1400, y = 1286, z = 7} },
	{ name = 'Auren', position = {x = 1192, y = 1291, z = 7} },
	{ name = 'Jorten', position = {x = 1192, y = 1293, z = 7} },
	{ name = 'Yabu', position = {x = 1382, y = 1293, z = 7} },
	{ name = 'Wakron', position = {x = 1206, y = 1293, z = 7} },
	{ name = 'Laxmon', position = {x = 1208, y = 1293, z = 7} },
	{ name = 'Apollo', position = {x = 1285, y = 1294, z = 7} },
	{ name = 'Mentril', position = {x = 1254, y = 1296, z = 7} },
	{ name = 'Manten', position = {x = 1187, y = 1296, z = 7} },
	{ name = 'Dihsar', position = {x = 1207, y = 1297, z = 7} },
	{ name = 'Ragnar', position = {x = 1206, y = 1298, z = 7} },
	{ name = 'Karmin', position = {x = 1249, y = 1297, z = 7} },
	{ name = 'Guerin', position = {x = 1248, y = 1299, z = 7} },
	{ name = 'Beol', position = {x = 1277, y = 1304, z = 7} },
	{ name = 'Hardouin', position = {x = 1251, y = 1309, z = 7} },
	{ name = 'Sewer', position = {x = 1309, y = 1308, z = 7} },
	{ name = 'Tral', position = {x = 1291, y = 1311, z = 7} },
	{ name = 'Asgard', position = {x = 1397, y = 1312, z = 7} },
	{ name = 'Selius', position = {x = 1278, y = 1316, z = 7} },
	{ name = 'Rupert', position = {x = 1234, y = 1317, z = 7} },
	{ name = 'Traug', position = {x = 1311, y = 1322, z = 7} },
	{ name = 'Amber', position = {x = 1423, y = 1321, z = 7} },
	{ name = 'Synthia', position = {x = 1288, y = 1324, z = 7} },
	{ name = 'Murhus', position = {x = 1411, y = 1325, z = 7} },
	{ name = 'Hajduk', position = {x = 1305, y = 1331, z = 7} },
	{ name = 'Alastor', position = {x = 1342, y = 1331, z = 7} },
	{ name = 'Hyacynth', position = {x = 1411, y = 1333, z = 7} },
	{ name = 'Filia', position = {x = 1295, y = 1339, z = 7} },
	{ name = 'Kate', position = {x = 1336, y = 1340, z = 7} },
	{ name = 'Aron', position = {x = 1380, y = 1339, z = 7} },
	{ name = 'Albert', position = {x = 1359, y = 1339, z = 7} },
	{ name = 'Slyntem', position = {x = 1318, y = 1350, z = 7} },
	{ name = 'Napster', position = {x = 1355, y = 1354, z = 7} },
	{ name = 'Dervil', position = {x = 1389, y = 1360, z = 7} },
	{ name = 'Niko', position = {x = 1384, y = 1361, z = 7} },
	{ name = 'Miriam', position = {x = 1245, y = 1362, z = 7} },
	{ name = 'Nynthus', position = {x = 1602, y = 1362, z = 7} },
	{ name = 'Gandalf', position = {x = 1250, y = 1365, z = 7} },
	{ name = 'Maria', position = {x = 1354, y = 1371, z = 7} },
	{ name = 'Timothy', position = {x = 1380, y = 1370, z = 7} },
	{ name = 'Livia', position = {x = 1398, y = 1370, z = 7} },
	{ name = 'Jeb', position = {x = 1402, y = 1372, z = 7} },
	{ name = 'Rick', position = {x = 1404, y = 1372, z = 7} },
	{ name = 'Sanyo', position = {x = 1245, y = 1372, z = 7} },
	{ name = 'Athushi', position = {x = 1242, y = 1375, z = 7} },
	{ name = 'Derva', position = {x = 1338, y = 1375, z = 7} },
	{ name = 'Sulur', position = {x = 1394, y = 1379, z = 7} },
	{ name = 'Tonil', position = {x = 1408, y = 1378, z = 7} },
	{ name = 'Ageit', position = {x = 1408, y = 1382, z = 7} },
	{ name = 'Sam', position = {x = 1400, y = 1384, z = 7} },
	{ name = 'Uncross de Harr', position = {x = 1388, y = 1392, z = 7} },
	{ name = 'Marta', position = {x = 1400, y = 1393, z = 7} },
	{ name = 'Filip', position = {x = 1369, y = 1392, z = 7} },
	{ name = 'Shaira', position = {x = 1355, y = 851, z = 8} },
	{ name = 'Arvis', position = {x = 1348, y = 853, z = 8} },
	{ name = 'Hasdrubal', position = {x = 1335, y = 885, z = 8} },
	{ name = 'Sygina', position = {x = 1301, y = 892, z = 8} },
	{ name = 'Vill', position = {x = 1391, y = 1384, z = 8} },
	{ name = 'Margatnep', position = {x = 1342, y = 878, z = 9} },
	{ name = 'Keneis', position = {x = 1331, y = 880, z = 9} },
	{ name = 'Blake', position = {x = 1331, y = 882, z = 9} },
	{ name = 'Hera', position = {x = 1481, y = 894, z = 9} },
	{ name = 'Konan', position = {x = 1508, y = 916, z = 9} },
	{ name = 'Puchamed', position = {x = 1305, y = 762, z = 10} },
	{ name = 'Uras', position = {x = 1508, y = 916, z = 10} },
	{ name = 'Keltus', position = {x = 1343, y = 946, z = 10} },
	{ name = 'Mefisto', position = {x = 1497, y = 870, z = 11} },
	{ name = 'Aldape', position = {x = 1517, y = 893, z = 11} },
	{ name = 'Sedron', position = {x = 1496, y = 899, z = 11} },
	{ name = 'Lawrence', position = {x = 1396, y = 1384, z = 7} },
	{ name = 'Sato Okada', position = {x = 1517, y = 769, z = 7} },
	{ name = 'Sato Okada', position = {x = 1292, y = 1288, z = 7} },
	{ name = 'Kurokawa', position = {x = 1517, y = 769, z = 7} },
	{ name = 'Kurokawa', position = {x = 1294, y = 1287, z = 7} },
	{ name = 'Cyrian', position = {x = 1211, y = 1127, z = 7} },
	{ name = 'Santa', position = {x = 1257, y = 792, z = 2} },
	{ name = 'Santa\'s Helper', position = {x = 1293, y = 1287, z = 7} },
	
	{ name = 'Hikma', position = {x = 1776, y = 1106, z = 7} },
	{ name = 'Nawfal', position = {x = 1787, y = 1102, z = 7} },
	{ name = 'Furqaan', position = {x = 1787, y = 1090, z = 7} },
	{ name = 'Zarqa', position = {x = 1762, y = 1086, z = 7} },
	{ name = 'Khaira', position = {x = 1765, y = 1086, z = 6} },
	{ name = 'Aadila', position = {x = 1776, y = 1085, z = 6} },
	{ name = 'Rabi', position = {x = 1775, y = 1083, z = 5} },
	{ name = 'Laiqa', position = {x = 1775, y = 1084, z = 4} },
	{ name = 'Muzna', position = {x = 1783, y = 1080, z = 7} },
	{ name = 'Mahyar', position = {x = 1729, y = 1104, z = 7} },
	{ name = 'Kobad', position = {x = 1775, y = 1116, z = 7} },
	{ name = 'Azartash', position = {x = 1721, y = 1080, z = 7} },
	{ name = 'Cumhur', position = {x = 1727, y = 1080, z = 7} },
	{ name = 'Aisha', position = {x = 1792, y = 1100, z = 5} },
	{ name = 'Jumail', position = {x = 1799, y = 1083, z = 7} },
	{ name = 'Saaliha', position = {x = 1761, y = 1065, z = 7} },
	{ name = 'Uqbah', position = {x = 1788, y = 1091, z = 6} },
	{ name = 'Zubair', position = {x = 1724, y = 1110, z = 7} },
	{ name = 'Shaahir', position = {x = 1720, y = 1086, z = 7} },
	{ name = 'Asad', position = {x = 1757, y = 1070, z = 7} },
	{ name = 'Silva', position = {x = 1517, y = 769, z = 7} },
	{ name = 'Silva', position = {x = 1294, y = 1289, z = 7} },
}

m_NpcGlosaryFunctions.sort = function(k, v)
	return k.name < v.name
end

table.sort(m_NpcGlosaryFunctions.npcGlosaryList, m_NpcGlosaryFunctions.sort)

m_NpcGlosaryFunctions.centerPosition = {x = 1309, y = 893}
m_NpcGlosaryFunctions.categoryList = {tr('Merchants'), tr('Travelers'), tr('Others')}

m_NpcGlosaryFunctions.onLoad = function()
	if g_game.isOnline() then
		m_NpcGlosaryFunctions.onGameStart()
	end
end

m_NpcGlosaryFunctions.onUnload = function()
	
end

m_NpcGlosaryFunctions.onGameStart = function()
	if g_settings.get('locale', 'en') == 'en' then
		m_NpcGlosaryFunctions.language = LANGUAGE_ENGLISH
	else
		m_NpcGlosaryFunctions.language = LANGUAGE_POLISH
	end
end

m_NpcGlosaryFunctions.onGameEnd = function()
	
end

m_NpcGlosaryFunctions.destroy = function()
	if m_NpcGlosaryList.window then
		m_NpcGlosaryList.window:destroy()
		m_NpcGlosaryList.window = nil
		
		m_NpcGlosaryList = {}
	end
end

m_NpcGlosaryFunctions.addLabel = function(parent, name, id)
	local widget = g_ui.createWidget(id, parent)
	widget:setText(name)
	return widget
end

m_NpcGlosaryFunctions.expand = function(self)
	local parent = self:getParent()
	if parent:isOn() then
		parent:setHeight(50)
	else
		parent:setHeight((parent.widgets * 52) + 50)
	end
	
	self:getChildById('mask'):setVisible(not parent:isOn())
	parent:setOn(not parent:isOn())
end

m_NpcGlosaryFunctions.selectNpc = function(name)
	m_NpcGlosaryFunctions.openByNpc = name
	
	if not m_NpcGlosaryList.window then
		modules.game_encyclopedia.m_EncyclopediaFunctions.selectById(ID_NPCGLOSARY)
	else
		for _, v in pairs(m_NpcGlosaryList.list:getChildren()) do
			if v == name then
				m_NpcGlosaryFunctions.select(v)
				break
			end
		end
	end
end

m_NpcGlosaryFunctions.selectItemId = function(self)
	if self.toPosition then
		m_NpcGlosaryList.destinations:moves({x = -(self.toPosition.x * 1) + 130, y = -(self.toPosition.y * 1) + 128})
	else
		local var = g_game.getItemType(self.item)
		if var ~= 0 then
			modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(self.item.clientId, var)
		end
	end
end

m_NpcGlosaryFunctions.select = function(self)
	if m_NpcGlosaryList.current then
		m_NpcGlosaryList.current:getChildById('mask'):hide()
	end

	m_NpcGlosaryList.current = self
	self:getChildById('mask'):show()
	
	m_NpcGlosaryFunctions.openByNpc = self.v.name
	m_NpcGlosaryList.name:setText(self.v.name)
	m_NpcGlosaryList.creature:setOutfit(self.v.look)
	
	local description = ''
	if self.position.z == 7 then
		description = tr('Ground Floor')
	elseif self.position.z < 7 then
		description = tr('Above Ground Floor')
	else
		description = tr('Underground Floor')
	end
	
	m_NpcGlosaryList.position:setText(description)
	
	m_NpcGlosaryList.buyList:destroyChildren()
	m_NpcGlosaryList.sellList:destroyChildren()
	
	m_NpcGlosaryList.minimap:moves({x = -(self.position.x * 1) + 130, y = -(self.position.y * 1) + 110})
	
	if self.tradeList then
		m_NpcGlosaryList.destinations:hide()
		m_NpcGlosaryList.buyList:show()
		m_NpcGlosaryList.buyLabel:show()
		m_NpcGlosaryList.buyListScroll:show()
		m_NpcGlosaryList.sellList:show()
		m_NpcGlosaryList.sellLabel:show()
		m_NpcGlosaryList.sellListScroll:show()
		m_NpcGlosaryList.buyLabel:setOn(false)
		
		for i = 1, #self.tradeList do
			local item = modules.game_lookat.m_LookAtFunctions.getItemDescription(self.tradeList[i].id)
			if item then
				if self.tradeList[i].buy ~= 0 then
					local widget = g_ui.createWidget('NpcGlosaryItem', m_NpcGlosaryList.buyList)
					widget:getChildById('item'):setItemId(item.clientId)
					widget:getChildById('price'):setText(self.tradeList[i].buy)
					widget:setText(item.name[m_NpcGlosaryFunctions.language])
					widget.item = item
				end
				
				if self.tradeList[i].sell ~= 0 then
					local widget = g_ui.createWidget('NpcGlosaryItem', m_NpcGlosaryList.sellList)
					widget:getChildById('item'):setItemId(item.clientId)
					widget:getChildById('price'):setText(self.tradeList[i].sell)
					widget:setText(item.name[m_NpcGlosaryFunctions.language])
					widget.item = item
				end
			else
				print(self.tradeList[i].id)
			end
		end
	elseif self.travelList then
		m_NpcGlosaryList.destinations:show()
		m_NpcGlosaryList.buyList:show()
		m_NpcGlosaryList.buyLabel:show()
		m_NpcGlosaryList.buyListScroll:show()
		m_NpcGlosaryList.sellList:hide()
		m_NpcGlosaryList.sellLabel:hide()
		m_NpcGlosaryList.sellListScroll:hide()
		
		m_NpcGlosaryList.buyLabel:setOn(true)
		
		for i = 1, #self.travelList do
			local widget = g_ui.createWidget('NpcGlosaryItem', m_NpcGlosaryList.buyList)
			widget:getChildById('item'):setItemId(17418)
			widget:getChildById('price'):setText(self.travelList[i].price)
			widget.toPosition = self.travelList[i].position
			widget:setText(self.travelList[i].name)
			
			if i == 1 then
				m_NpcGlosaryFunctions.selectItemId(widget)
			end
		end
	else
		m_NpcGlosaryList.destinations:hide()
		m_NpcGlosaryList.buyList:hide()
		m_NpcGlosaryList.buyLabel:hide()
		m_NpcGlosaryList.buyListScroll:hide()
		m_NpcGlosaryList.sellList:hide()
		m_NpcGlosaryList.sellLabel:hide()
		m_NpcGlosaryList.sellListScroll:hide()
	end
end

m_NpcGlosaryFunctions.disable = function(parent)
	parent.onMouseWheel = nil
	parent.onDragMove = nil
	parent:setZoom(1)
end

m_NpcGlosaryFunctions.open = function(parent)
	if m_NpcGlosaryList.window then
		m_NpcGlosaryFunctions.destroy()
	else
		m_NpcGlosaryList.window = g_ui.createWidget('NpcGlosaryWindow', parent)
		m_NpcGlosaryList.list = m_NpcGlosaryList.window:getChildById('list')
		m_NpcGlosaryList.searchNPCName = m_NpcGlosaryList.window:getChildById('searchNPCName')
		m_NpcGlosaryList.description = m_NpcGlosaryList.window:getChildById('description')
		m_NpcGlosaryList.name = m_NpcGlosaryList.description:getChildById('name')
		m_NpcGlosaryList.creature = m_NpcGlosaryList.description:getChildById('creature')
		m_NpcGlosaryList.position = m_NpcGlosaryList.description:getChildById('position')
		m_NpcGlosaryList.buyList = m_NpcGlosaryList.description:getChildById('buyList')
		m_NpcGlosaryList.buyLabel = m_NpcGlosaryList.description:getChildById('buyLabel')
		m_NpcGlosaryList.buyListScroll = m_NpcGlosaryList.description:getChildById('buyListScroll')
		m_NpcGlosaryList.sellList = m_NpcGlosaryList.description:getChildById('sellList')
		m_NpcGlosaryList.sellLabel = m_NpcGlosaryList.description:getChildById('sellLabel')
		m_NpcGlosaryList.sellListScroll = m_NpcGlosaryList.description:getChildById('sellListScroll')
		m_NpcGlosaryList.minimap = m_NpcGlosaryList.description:getChildById('minimap')
		m_NpcGlosaryList.destinations = m_NpcGlosaryList.description:getChildById('destinations')
		m_NpcGlosaryFunctions.disable(m_NpcGlosaryList.destinations)
		m_NpcGlosaryFunctions.disable(m_NpcGlosaryList.minimap)
		
		m_NpcGlosaryList.searchNPCName.onTextChange = function(self, text, oldText)
			local list = {}
			for _, v in pairs(m_NpcGlosaryList.list:getChildren()) do
				for _, pid in pairs(v:getChildren()) do
					if not pid.ignore then
						table.insert(list, pid)
					end
				end
			end
			
			if not text:find(oldText) then
				for _, v in pairs(list) do
					if not v.ignore and not v:isVisible() and v:getText():lower():find(text:lower()) then
						local parent = v:getParent()
						parent.widgets = parent.widgets + 1
						v:show()
					end
				end
			end
			
			for _, v in pairs(list) do
				if not v.ignore and v:isVisible() and not v:getText():lower():find(text:lower()) then
					v:hide()
					
					local parent = v:getParent()
					parent.widgets = parent.widgets - 1
				end
			end
			
			for i = 1, #m_NpcGlosaryFunctions.categoryList do
				local widget = m_NpcGlosaryList.list:getChildById(m_NpcGlosaryFunctions.categoryList[i])
				if widget and widget:isOn() then
					widget:setHeight((widget.widgets * 52) + 50)
				end
			end
		end
		
		local list = {}
		for i = 1, #m_NpcGlosaryFunctions.categoryList do
			local parent = g_ui.createWidget('NpcGlosaryContainer', m_NpcGlosaryList.list)
			parent:setId(m_NpcGlosaryFunctions.categoryList[i])
			
			local widget = m_NpcGlosaryFunctions.addLabel(parent, m_NpcGlosaryFunctions.categoryList[i], 'NpcGlosaryCategoryWidget')
			widget.ignore = true
			widget.onMouseRelease = m_NpcGlosaryFunctions.expand
			table.insert(list, {parent, widget, 1})
		end
		
		local value = {1, 1}
		local selected = false
		for i = 1, #m_NpcGlosaryFunctions.npcGlosaryList do
			local v = modules.game_lookat.m_LookAtFunctions.getNPCByName(m_NpcGlosaryFunctions.npcGlosaryList[i].name)
			if v then
				local trade = modules.game_lookat.m_LookAtFunctions.getNPCTradeListByName(m_NpcGlosaryFunctions.npcGlosaryList[i].name)
				local travel = modules.game_lookat.m_LookAtFunctions.getNPCTravelListByName(m_NpcGlosaryFunctions.npcGlosaryList[i].name)
				local parentId = trade and 1 or v.travel and 2 or 3
				
				local tmpWidget = m_NpcGlosaryFunctions.addLabel(list[parentId][1], m_NpcGlosaryFunctions.npcGlosaryList[i].name, 'NpcGlosaryNpcWidget')
				tmpWidget:getChildById('creature'):setOutfit(v.look)
				tmpWidget.v = v
				tmpWidget.name = m_NpcGlosaryFunctions.npcGlosaryList[i].name
				tmpWidget.position = m_NpcGlosaryFunctions.npcGlosaryList[i].position
				tmpWidget.tradeList = trade
				tmpWidget.travelList = travel
				list[parentId][1].widgets = (list[parentId][1].widgets or 0) + 1
				
				if m_NpcGlosaryFunctions.openByNpc then
					if m_NpcGlosaryFunctions.openByNpc == m_NpcGlosaryFunctions.npcGlosaryList[i].name then
						m_NpcGlosaryFunctions.select(tmpWidget)
						value = {list[parentId][3], parentId}
					end
				elseif not selected and parentId == 1 then
					m_NpcGlosaryFunctions.select(tmpWidget)
					selected = true
				end
				
				list[parentId][3] = list[parentId][3] + 1
			end
		end
		
		m_NpcGlosaryFunctions.expand(list[value[2]][2])
		
		if value[1] > 9 - value[2] then
			scheduleEvent(function()
				m_NpcGlosaryList.list:increment((value[1] * 52) + (value[2] * 50))
			end, 50)
		end
	end
end
