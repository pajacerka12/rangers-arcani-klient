m_CalendarEventsFunctions.monthNames = {
	[1] = {name = {'Stycze�', 'January'}, days = 31},
	[2] = {name = {'Luty', 'February'}, days = 28},
	[3] = {name = {'Marzec', 'March'}, days = 31},
	[4] = {name = {'Kwiecie�', 'April'}, days = 30},
	[5] = {name = {'Maj', 'May'}, days = 31},
	[6] = {name = {'Czerwiec', 'June'}, days = 30},
	[7] = {name = {'Lipiec', 'July'}, days = 31},
	[8] = {name = {'Sierpie�', 'August'}, days = 31},
	[9] = {name = {'Wrzesie�', 'September'}, days = 30},
	[10] = {name = {'Pa�dziernik', 'October'}, days = 31},
	[11] = {name = {'Listopad', 'November'}, days = 30},
	[12] = {name = {'Grudzie�', 'December'}, days = 31}
}

m_CalendarEventsFunctions.dayNames = {
	[1] = {'Poniedzia�ek', 'Monday'},
	[2] = {'Wtorek', 'Tuesday'},
	[3] = {'�roda', 'Wednesday'},
	[4] = {'Czwartek', 'Thursday'},
	[5] = {'Pi�tek', 'Friday'},
	[6] = {'Sobota', 'Saturday'},
	[7] = {'Niedziela', 'Sunday'},
}

m_CalendarEventsFunctions.EVENT_BOSS = 1
m_CalendarEventsFunctions.EVENT_SKULL = 2
m_CalendarEventsFunctions.EVENT_DARKNESS = 3
m_CalendarEventsFunctions.EVENT_RAPID = 4

m_CalendarEventsFunctions.eventName = {
	[m_CalendarEventsFunctions.EVENT_BOSS] = 'Boss Token Event',
	[m_CalendarEventsFunctions.EVENT_SKULL] = 'Skull Monsters Token Event',
	[m_CalendarEventsFunctions.EVENT_DARKNESS] = 'Darkness Event',
	[m_CalendarEventsFunctions.EVENT_RAPID] = 'Rapid Respawn',
}

m_CalendarEventsFunctions.events = {
	day = 4, month = 7, year = 2020, id = m_CalendarEventsFunctions.EVENT_SKULL,
}

m_CalendarEventsFunctions.outfitId = {
	[m_CalendarEventsFunctions.EVENT_BOSS] = {685, 686},
	[m_CalendarEventsFunctions.EVENT_SKULL] = {679, 680},
	[m_CalendarEventsFunctions.EVENT_DARKNESS] = {694}
}

m_CalendarEventsFunctions.language = LANGUAGE_ENGLISH

m_CalendarEventsFunctions.onLoad = function()
	if g_game.isOnline() then
		m_CalendarEventsFunctions.onGameStart()
	end
	
	m_CalendarEventsFunctions.year = tonumber(os.date('%Y'))
	m_CalendarEventsFunctions.month = tonumber(os.date('%m'))
	m_CalendarEventsFunctions.day = tonumber(os.date('%d'))
	
	if m_CalendarEventsFunctions.year % 4 == 0 then
		m_CalendarEventsFunctions.monthNames[2].days = 29
	end
	
	m_CalendarEventsFunctions.date = {
		m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month == 1 and 12 or m_CalendarEventsFunctions.month - 1],
		m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month],
		m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month == 12 and 1 or m_CalendarEventsFunctions.month + 1]
	}
	
	local fromDate = os.time{year = m_CalendarEventsFunctions.events.year, month = m_CalendarEventsFunctions.events.month, day = m_CalendarEventsFunctions.events.day}
	local id = {0, 0}
	for i = 1, 7 do
		local toDate = os.time{year = m_CalendarEventsFunctions.year, month = m_CalendarEventsFunctions.month, day = i}
		local dateId = tonumber(os.date('%w', toDate))
		if dateId == 6 then
			local distance = math.floor((toDate - fromDate) / 86400) / 7
			id = {i, (distance % 4) + 1}
		end
	end
	
	if id[1] ~= 0 then
		m_CalendarEventsFunctions.eventList = {}
		
		local previousMonth = m_CalendarEventsFunctions.month == 1 and 12 or m_CalendarEventsFunctions.month - 1
		local tmpId = id[2] == m_CalendarEventsFunctions.EVENT_BOSS and m_CalendarEventsFunctions.EVENT_RAPID or id[2] - 1
		if 7 - id[1] < 0 then
			table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = 1, id = tmpId })
		else
			table.insert(m_CalendarEventsFunctions.eventList, { month = previousMonth, day = m_CalendarEventsFunctions.monthNames[previousMonth].days - (7 - id[1]), id = tmpId })
		end
		
		table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = id[1], id = id[2] })
		
		local tmpId = id[2] == m_CalendarEventsFunctions.EVENT_RAPID and m_CalendarEventsFunctions.EVENT_BOSS or id[2] + 1
		table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = id[1] + 7, id = tmpId })
		
		local tmpId = tmpId == m_CalendarEventsFunctions.EVENT_RAPID and m_CalendarEventsFunctions.EVENT_BOSS or tmpId + 1
		table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = id[1] + 14, id = tmpId })
		
		local tmpId = tmpId == m_CalendarEventsFunctions.EVENT_RAPID and m_CalendarEventsFunctions.EVENT_BOSS or tmpId + 1
		table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = id[1] + 21, id = tmpId })
		
		local tmpId = tmpId == m_CalendarEventsFunctions.EVENT_RAPID and m_CalendarEventsFunctions.EVENT_BOSS or tmpId + 1
		if id[1] + 28 > m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month].days then
			local nextMonth = m_CalendarEventsFunctions.month == 12 and 1 or m_CalendarEventsFunctions.month + 1
			table.insert(m_CalendarEventsFunctions.eventList, { month = nextMonth, day = id[1] + 28 - m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month].days, id = tmpId })
		else
			table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = id[1] + 28, id = tmpId })
		end
		
		local tmpId = tmpId == m_CalendarEventsFunctions.EVENT_RAPID and m_CalendarEventsFunctions.EVENT_BOSS or tmpId + 1
		if id[1] + 35 > m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month].days then
			local nextMonth = m_CalendarEventsFunctions.month == 12 and 1 or m_CalendarEventsFunctions.month + 1
			table.insert(m_CalendarEventsFunctions.eventList, { month = nextMonth, day = id[1] + 35 - m_CalendarEventsFunctions.monthNames[m_CalendarEventsFunctions.month].days, id = tmpId })
		else
			table.insert(m_CalendarEventsFunctions.eventList, { month = m_CalendarEventsFunctions.month, day = id[1] + 35, id = tmpId })
		end
	end
end

m_CalendarEventsFunctions.onUnload = function()
	
end

m_CalendarEventsFunctions.onGameStart = function()
	m_BestiaryFunctions.loadLanguage()
end

m_CalendarEventsFunctions.onGameEnd = function()
	
end

m_CalendarEventsFunctions.destroy = function()
	if m_CalendarEventsList.window then
		m_CalendarEventsFunctions.close()
		
		m_CalendarEventsList.window:destroy()
		m_CalendarEventsList.window = nil
		
		m_CalendarEventsList = {}
	end
end

m_CalendarEventsFunctions.loadLanguage = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_CalendarEventsFunctions.language = LANGUAGE_ENGLISH
	else
		m_CalendarEventsFunctions.language = LANGUAGE_POLISH
	end
end

m_CalendarEventsFunctions.open = function(parent)
	if m_CalendarEventsList.window then
		m_CalendarEventsFunctions.destroy()
	else
		m_CalendarEventsList.window = g_ui.createWidget('CalendarEventsWindow', parent)
		
		m_CalendarEventsList.name = m_CalendarEventsList.window:getChildById('name')
		m_CalendarEventsList.panel = m_CalendarEventsList.window:getChildById('panel')
		
		m_CalendarEventsList.name:setText(m_CalendarEventsFunctions.date[2].name[m_CalendarEventsFunctions.language])
		
		local var = os.time{year = m_CalendarEventsFunctions.year, month = m_CalendarEventsFunctions.month, day = 1}
		local dateId = tonumber(os.date('%w', var))
		if dateId == 0 then
			dateId = 7
		end
		
		for i = 1, 42 do
			local widget = g_ui.createWidget('CalendarEventsPanel', m_CalendarEventsList.panel)
			local currentMonth = m_CalendarEventsFunctions.month
			local value = i - dateId + 1
			if i < dateId then
				currentMonth = m_CalendarEventsFunctions.month == 1 and 12 or (m_CalendarEventsFunctions.month - 1)
				value = m_CalendarEventsFunctions.date[1].days - dateId + i + 1
				widget:setOn(true)
				widget:setText(value)
			elseif value > m_CalendarEventsFunctions.date[2].days then
				currentMonth = m_CalendarEventsFunctions.month == 12 and 1 or (m_CalendarEventsFunctions.month + 1)
				value = i - m_CalendarEventsFunctions.date[2].days - dateId + 1
				widget:setOn(true)
				widget:setText(value)
			else
				widget:setText(value)
			end
			
			local dayId = ((i - 1) % 7) + 1
			widget:getChildById('name'):setText(m_CalendarEventsFunctions.dayNames[dayId][m_CalendarEventsFunctions.language])
			
			if dayId == 6 or dayId == 7 then
				for i = 1, #m_CalendarEventsFunctions.eventList do
					if dayId ~= 7 or m_CalendarEventsFunctions.eventList[i].id ~= m_CalendarEventsFunctions.EVENT_RAPID then
						if value == m_CalendarEventsFunctions.eventList[i].day or value == m_CalendarEventsFunctions.eventList[i].day + 1 and currentMonth == m_CalendarEventsFunctions.eventList[i].month then
							widget.id = m_CalendarEventsFunctions.eventList[i].id
							widget:getChildById('mark'):setOn(true)
							break
						end
					end
				end
			end
		end
	end
end

m_CalendarEventsFunctions.close = function()
	if m_CalendarEventsList.eventWindow then
		m_CalendarEventsList.eventWindow:destroy()
		m_CalendarEventsList.eventWindow = nil
	end
end

m_CalendarEventsFunctions.click = function(self)
	if not self:getChildById('mark'):isOn() then
		return true
	end
	
	if m_CalendarEventsList.eventWindow then
		m_CalendarEventsList.eventWindow:destroy()
	end
	
	m_CalendarEventsList.eventWindow = g_ui.displayUI('event_window_' .. self.id)
	
	if m_CalendarEventsFunctions.outfitId[self.id] then
		for i = 1, #m_CalendarEventsFunctions.outfitId[self.id] do
			m_CalendarEventsList.eventWindow:getChildById('outfit' .. i):setOutfit({type = m_CalendarEventsFunctions.outfitId[self.id][i], head = 76, body = 17, legs = 67, feet = 28})
		end
	end
end

m_CalendarEventsFunctions.setAddon = function(self, id, outfit, addon)
	local widget = m_CalendarEventsList.eventWindow:getChildById('outfit' .. id)
	if self:isChecked() then
		if addon == 1 then
			widget.addons = widget.addons == 2 and 3 or 1
		elseif addon == 2 then
			widget.addons = widget.addons == 1 and 3 or 2
		end
	else
		if addon == 1 then
			widget.addons = widget.addons == 3 and 2 or 0
		elseif addon == 2 then
			widget.addons = widget.addons == 3 and 1 or 0
		end
	end
	
	m_CalendarEventsList.eventWindow:getChildById('outfit' .. id):setOutfit({type = outfit, head = 76, body = 17, legs = 67, feet = 28, addons = widget.addons})
end