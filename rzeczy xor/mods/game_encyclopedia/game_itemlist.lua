m_ItemlistFunctions.SORTBY_NAME = 1
m_ItemlistFunctions.SORTBY_CAP = 2
m_ItemlistFunctions.SORTBY_STAT = 3

m_ItemlistFunctions.SORTBY_HIGH = 1
m_ItemlistFunctions.SORTBY_LOW = 2

m_ItemlistFunctions.language = LANGUAGE_ENGLISH

m_ItemlistFunctions.disablesQuality = {ITEMTYPE_AMMUNITION, ITEMTYPE_POTION, ITEMTYPE_FOOD, ITEMTYPE_VALUABLE, ITEMTYPE_CREATURE_PRODUCT, ITEMTYPE_DOLL, ITEMTYPE_TROPHY, ITEMTYPE_TOOL, ITEMTYPE_CONTAINERS, ITEMTYPE_PREMIUM}
m_ItemlistFunctions.disablesUnique = ITEMTYPE_CRAFTING_MATERIAL
m_ItemlistFunctions.itemTypeList = {
	{ITEMTYPE_ONEHANDED_MELEE, 13403},
	{ITEMTYPE_TWOHANDED_MELEE, 13430},
	{ITEMTYPE_BOW, 15509},
	{ITEMTYPE_CROSSBOW, 12803},
	{ITEMTYPE_SPEAR, 13027},
	{ITEMTYPE_AMMUNITION, 3447},
	{ITEMTYPE_QUIVER, 18339},
	{ITEMTYPE_WAND, 16782},
	{ITEMTYPE_HELMET, 13432},
	{ITEMTYPE_ARMOR, 13431},
	{ITEMTYPE_LEGS, 13433},
	{ITEMTYPE_FEET, 13434},
	{ITEMTYPE_SHIELD, 13435},
	{ITEMTYPE_NECKLACE, 14832},
	{ITEMTYPE_RING, 14820},
	{ITEMTYPE_BACKPACK, 3253},
	{ITEMTYPE_RUNE, 3178},
	{ITEMTYPE_POTION, 15893},
	{ITEMTYPE_FOOD, 3582},
	{ITEMTYPE_VALUABLE, 12220},
	{ITEMTYPE_CREATURE_PRODUCT, 5882},
	{ITEMTYPE_CRAFTING_MATERIAL, 12241},
	{ITEMTYPE_DOLL, 6568},
	{ITEMTYPE_TROPHY, 7393},
	{ITEMTYPE_TOOL, 5710},
	{ITEMTYPE_CONTAINERS, 2873},
}

m_ItemlistFunctions.sortList = {
	{tr('Ascending by name'), m_ItemlistFunctions.SORTBY_NAME, m_ItemlistFunctions.SORTBY_HIGH},
	{tr('Descending by name'), m_ItemlistFunctions.SORTBY_NAME, m_ItemlistFunctions.SORTBY_LOW},
	{tr('Ascending by capacity'), m_ItemlistFunctions.SORTBY_CAP, m_ItemlistFunctions.SORTBY_HIGH},
	{tr('Descending by capacity'), m_ItemlistFunctions.SORTBY_CAP, m_ItemlistFunctions.SORTBY_LOW},
	{tr('Ascending by strength'), m_ItemlistFunctions.SORTBY_STAT, m_ItemlistFunctions.SORTBY_HIGH},
	{tr('Descending by strength'), m_ItemlistFunctions.SORTBY_STAT, m_ItemlistFunctions.SORTBY_LOW}
}

m_ItemlistFunctions.expandQualityList = {tr('Unique'), tr('Legendary'), tr('Perfect'), tr('Normal'), tr('Damaged'), tr('Desolate')}
m_ItemlistFunctions.qualityList = {tr('Legendary'), tr('Perfect'), tr('Normal'), tr('Damaged'), tr('Desolate')}
m_ItemlistFunctions.attributeList = {
	{ITEM_ALL, tr('All')},
	{ITEM_DISTANCE_DAMAGE, tr('Additional damage')},
	{ITEM_ARMOR, tr('Armor')},
	{ITEM_HIT_CHANCE, tr('Hit chance')},
	{ITEM_COUNT_STOLEN_LIFE, tr('Stealing life')},
	{ITEM_MAXHEALTH, tr('Maximum health')},
	{ITEM_REGEN_HEALTH, tr('Health regeneration per turn')},
	{ITEM_COUNT_STOLEN_MANA, tr('Stealing mana')},
	{ITEM_MAXMANA, tr('Maximum mana')},
	{ITEM_REGEN_MANA, tr('Mana regeneration per turn')},
	{ITEM_CRITICAL, tr('Critical hit')},
	{ITEM_CRITICAL_HIT_MULTIPLIER, tr('Critical hit multiplier')},
	{ITEM_BLOCK_CRITICAL, tr('Block critical hit')},
	{ITEM_REDUCE_CRITICAL_HIT, tr('Reduce damage of critical hit')},
	{ITEM_MAGIC_ATTACK, tr('Magic increase')},
	{ITEM_FAMILIAR, tr('Familiar damage')},
	{ITEM_SPEED, tr('Speed')},
	{ITEM_DOUBLE_HIT, tr('Double hit')},
	{ITEM_FULL_IGNORE_ARMOR, tr('Chance to penetrate resistances')},
	{ITEM_LUCK, tr('Luck')},
	{ITEM_DEATH_HIT, tr('Death hit')},
	{ITEM_SUPPRESS, tr('Suppress')},
	{ITEM_ABSORB, tr('Absorb damage')},
	{ITEM_INCREMENT, tr('Increment damage')},
	{ITEM_BONUS_DAMAGE, tr('Additional damage')},
	{ITEM_REFLECT_CHANCE, tr('Reflect damage')},
	{ITEM_CONDITION, tr('Applies condition')},
	{ITEM_MONSTER_DAMAGE, tr('Increment damage vs monsters')},
	{ITEM_MONSTER_DEFENSE, tr('Absorb damage vs monsters')},
	{ITEM_BREAK_CHANCE, tr('Break chance')},
	{ITEM_SPELL_CHANCE, tr('Cast spell chance')},
	{ITEM_LOOTLIST, tr('Extra item drop rate')},
	{ITEM_INVISIBILITY, tr('Invisibility')},
	{ITEM_MANASHIELD, tr('Mana shield')},
	{ITEM_CRAFTABLE, tr('Craftable')},
	{ITEM_DURATION, tr('Has duration')}
}

m_ItemlistFunctions.reverseTranslate = {
	[tr('Unique')] = 'unique',
	[tr('Legendary')] = 'legendary',
	[tr('Perfect')] = 'perfect',
	[tr('Normal')] = 'normal',
	[tr('Damaged')] = 'damaged',
	[tr('Desolate')] = 'desolate'
}

function onOptionChange(comboBox, text, data)
	if m_ItemlistList.quality == text:lower() then
		return false
	end
	
	m_ItemlistList.quality = text:lower()
	m_ItemlistList.pureQuality = m_ItemlistFunctions.reverseTranslate[text] or m_ItemlistList.quality
	m_ItemlistFunctions.updateItemList()
	if not m_ItemlistList.currentSelectItem then
		return false
	end
	
	modules.game_lookat.m_LookAtFunctions.inspectItem(m_ItemlistList.currentSelectItem:getChildById('item'):getItem(), m_ItemlistList.pureQuality, true)
	return true
end

m_ItemlistFunctions.updateItemList = function()
	for _, v in pairs(m_ItemlistList.itemListPanel:getChildren()) do
		if m_ItemlistList.attributeId == ITEM_ALL then
			v:show()
		elseif not modules.game_lookat.m_LookAtFunctions.hasAttribute(v.itemId, m_ItemlistList.attributeId, m_ItemlistList.pureQuality == 'legendary') then
			v:hide()
		else
			v:show()
		end
	end
end

m_ItemlistFunctions.onLoad = function()
	
end

m_ItemlistFunctions.onUnload = function()
	
end

m_ItemlistFunctions.destroyCurrentItem = function()
	if m_ItemlistList.currentItem then
		modules.game_lookat.m_LookAtFunctions.destroyWindow(m_ItemlistList.currentItem.id)
		m_ItemlistList.currentItem = nil
	end
end

m_ItemlistFunctions.destroy = function()
	m_ItemlistFunctions.destroyCurrentItem()
	
	if m_ItemlistList.window then
		m_ItemlistList.itemCategoryPanel:destroyChildren()
		
		m_ItemlistList.window:destroy()
		m_ItemlistList.window = nil
		
		m_ItemlistList = {}
	end
end

m_ItemlistFunctions.open = function(parent)
	if m_ItemlistList.window then
		m_ItemlistFunctions.destroy()
	else
		m_ItemlistList.window = g_ui.createWidget('ItemListWindow', parent)
		m_ItemlistList.itemListPanel = m_ItemlistList.window:getChildById('itemListPanel')
		m_ItemlistList.itemCategoryPanel = m_ItemlistList.window:getChildById('itemCategoryPanel')
		m_ItemlistList.monsterListPanel = m_ItemlistList.window:getChildById('monsterListPanel')
		m_ItemlistList.npcListPanel = m_ItemlistList.window:getChildById('npcListPanel')
		m_ItemlistList.nameTextEdit = m_ItemlistList.window:getChildById('nameTextEdit')
		m_ItemlistList.sortBox = m_ItemlistList.window:getChildById('sortBox')
		m_ItemlistList.qualityBox = m_ItemlistList.window:getChildById('qualityBox')
		m_ItemlistList.attributeBox = m_ItemlistList.window:getChildById('attributeBox')
		m_ItemlistList.itemPanel = m_ItemlistList.window:getChildById('itemPanel')
		
		m_ItemlistList.currentSorting = m_BestiaryFunctions.SORTBY_NAME
		m_ItemlistList.sortby = m_BestiaryFunctions.SORTBY_HIGH
		m_ItemlistList.currentCategory = ITEMTYPE_ONEHANDED_MELEE
		
		m_ItemlistFunctions.execute()
	end
end

m_ItemlistFunctions.sortByCategoryName = function(k, v)
	return k[2] < v[2]
end

m_ItemlistFunctions.execute = function()
	for i = 1, #m_ItemlistFunctions.sortList do
		m_ItemlistList.sortBox:addOption(m_ItemlistFunctions.sortList[i][1])
	end
	
	m_ItemlistList.sortBox:setCurrentOption(m_ItemlistFunctions.sortList[1])
	connect(m_ItemlistList.sortBox, { onOptionChange = onItemListSortOptionChange })
	m_ItemlistList.sortId = m_ItemlistFunctions.sortList[1][1]
	
	for _, v in pairs(m_ItemlistFunctions.expandQualityList) do
		m_ItemlistList.qualityBox:addOption(v)
	end
	
	m_ItemlistList.qualityBox:setCurrentOption(m_ItemlistFunctions.expandQualityList[4])
	connect(m_ItemlistList.qualityBox, { onOptionChange = onOptionChange })
	m_ItemlistList.quality = 'normal'
	
	table.sort(m_ItemlistFunctions.attributeList, m_ItemlistFunctions.sortByCategoryName)
	for i = 1, #m_ItemlistFunctions.attributeList do
		m_ItemlistList.attributeBox:addOption(m_ItemlistFunctions.attributeList[i][2])
	end
	
	m_ItemlistList.attributeBox:setCurrentOption(m_ItemlistFunctions.attributeList[1])
	connect(m_ItemlistList.attributeBox, { onOptionChange = onAttributeOptionChange })
	m_ItemlistList.attributeId = ITEM_ALL
	
	m_ItemlistFunctions.updateCategories()
	m_ItemlistList.nameTextEdit:focus()
	m_ItemlistList.nameTextEdit.onTextChange = function(self, text, oldText)
		local list = m_ItemlistList.itemListPanel:getChildren()
		if not text:find(oldText) then
			for _, v in pairs(list) do
				if v:getText():lower():find(text:lower()) and (m_ItemlistList.attributeId == ITEM_ALL or modules.game_lookat.m_LookAtFunctions.hasAttribute(v.itemId, m_ItemlistList.attributeId, m_ItemlistList.pureQuality == 'legendary')) then
					v:show()
				end
			end
		end
		
		for _, v in pairs(list) do
			if not v:getText():lower():find(text:lower()) then
				v:hide()
			end
		end
	end
	
	m_ItemlistFunctions.onItemList()
	modules.game_encyclopedia.m_BestiaryFunctions.load()
end

function onAttributeOptionChange(comboBox, text, data)
	for i = 1, #m_ItemlistFunctions.attributeList do
		if m_ItemlistFunctions.attributeList[i][2] == text then
			m_ItemlistList.attributeId = m_ItemlistFunctions.attributeList[i][1]
			break
		end
	end
	
	m_ItemlistFunctions.updateItemList()
end

function onItemListSortOptionChange(comboBox, text, data)
	if m_ItemlistList.sortId == text then
		return false
	end
	
	m_ItemlistList.sortId = text
	
	for i = 1, #m_ItemlistFunctions.sortList do
		if text == m_ItemlistFunctions.sortList[i][1] then
			m_ItemlistList.currentSorting = m_ItemlistFunctions.sortList[i][2]
			m_ItemlistList.sortby = m_ItemlistFunctions.sortList[i][3]
			break
		end
	end
	
	m_ItemlistFunctions.openCategory(m_ItemlistList.currentCategory, true)
	return true
end

m_ItemlistFunctions.updateCategories = function()
	for i = 1, #m_ItemlistFunctions.itemTypeList do
		local widget = g_ui.createWidget('ItemCategory', m_ItemlistList.itemCategoryPanel)
		
		widget:setParent(m_ItemlistList.itemCategoryPanel)
		widget:setText(tr(ITEM_CATEGORY[m_ItemlistFunctions.itemTypeList[i][1]]))
		widget:setId(m_ItemlistFunctions.itemTypeList[i][1])
		widget:getChildById('item'):setItemId(m_ItemlistFunctions.itemTypeList[i][2])
		widget.onMouseRelease = m_ItemlistFunctions.setCategory
	end
end

m_ItemlistFunctions.onGameEnd = function()
	
end

m_ItemlistFunctions.onGameStart = function()
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		m_ItemlistFunctions.language = LANGUAGE_ENGLISH
	else
		m_ItemlistFunctions.language = LANGUAGE_POLISH
	end
end

m_ItemlistFunctions.onItemList = function(list)
	if m_ItemlistList.window then
		if m_ItemlistFunctions.openByItem then
			m_ItemlistList.itemCategoryPanel:getChildById(m_ItemlistFunctions.openByItem[2]):setOn(true)
			m_ItemlistFunctions.openCategory(m_ItemlistFunctions.openByItem[2])
		else
			m_ItemlistList.itemCategoryPanel:getChildById(ITEMTYPE_ONEHANDED_MELEE):setOn(true)
			m_ItemlistFunctions.openCategory(ITEMTYPE_ONEHANDED_MELEE)
		end
	end
end

m_ItemlistFunctions.sortByCap = function(k, v)
	if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
		return k.attributes.weight < v.attributes.weight
	elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
		return k.attributes.weight > v.attributes.weight
	end
end

m_ItemlistFunctions.sortByName = function(k, v)
	if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
		return k.name[m_ItemlistFunctions.language] < v.name[m_ItemlistFunctions.language]
	elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
		return k.name[m_ItemlistFunctions.language] > v.name[m_ItemlistFunctions.language]
	end
end

m_ItemlistFunctions.sortByStat = function(k, v)
	if k.itemType == ITEMTYPE_SPEAR or k.itemType == ITEMTYPE_AMMUNITION or k.itemType == ITEMTYPE_ONEHANDED_MELEE or k.itemType == ITEMTYPE_TWOHANDED_MELEE or k.itemType == ITEMTYPE_WAND then
		local tmpK = k.attributes.mindamage + k.attributes.maxdamage
		local tmpV = v.attributes.mindamage + v.attributes.maxdamage
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return tmpK < tmpV
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return tmpK > tmpV
		end
	elseif k.itemType == ITEMTYPE_CROSSBOW or k.itemType == ITEMTYPE_BOW then
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return k.attributes.range + (k.attributes.maxdamage or 0) < v.attributes.range + (v.attributes.maxdamage or 0)
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return k.attributes.range + (k.attributes.maxdamage or 0) > v.attributes.range + (v.attributes.maxdamage or 0)
		end
	elseif k.itemType == ITEMTYPE_SHIELD then
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return (k.attributes.defense or 0) < (v.attributes.defense or 0)
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return (k.attributes.defense or 0) > (v.attributes.defense or 0)
		end
	elseif k.itemType == ITEMTYPE_HELMET or k.itemType == ITEMTYPE_ARMOR or k.itemType == ITEMTYPE_LEGS or k.itemType == ITEMTYPE_FEET then
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return (k.attributes.armor or 0) < (v.attributes.armor or 0)
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return (k.attributes.armor or 0) > (v.attributes.armor or 0)
		end
	elseif k.itemType == ITEMTYPE_FOOD then
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return k.attributes.food < v.attributes.food
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return k.attributes.food > v.attributes.food
		end
	elseif k.itemType == ITEMTYPE_POTION then
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return k.attributes.potion[1].value < v.attributes.potion[1].value
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return k.attributes.potion[1].value > v.attributes.potion[1].value
		end
	elseif k.itemType == ITEMTYPE_BACKPACK or k.itemType == ITEMTYPE_QUIVER then
		if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
			return k.attributes.containersize < v.attributes.containersize
		elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
			return k.attributes.containersize > v.attributes.containersize
		end
	end
	
	if m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_HIGH then
		return k.id < v.id
	elseif m_ItemlistList.sortby == m_ItemlistFunctions.SORTBY_LOW then
		return k.id > v.id
	end
end

m_ItemlistFunctions.openCategory = function(category, ignore)
	m_ItemlistFunctions.destroyCurrentItem()
	
	m_ItemlistList.monsterListPanel:destroyChildren()
	m_ItemlistList.npcListPanel:destroyChildren()
	m_ItemlistList.itemListPanel:destroyChildren()
	
	m_ItemlistList.currentCategory = category
	m_ItemlistList.currentSelectItem = nil
	
	local list = modules.game_lookat.m_LookAtFunctions.getItemList()[category]
	if not list then
		return true
	end
	
	if m_ItemlistList.currentSorting == m_ItemlistFunctions.SORTBY_CAP then
		table.sort(list, m_ItemlistFunctions.sortByCap)
	elseif m_ItemlistList.currentSorting == m_ItemlistFunctions.SORTBY_NAME then
		table.sort(list, m_ItemlistFunctions.sortByName)
	elseif m_ItemlistList.currentSorting == m_ItemlistFunctions.SORTBY_STAT then
		table.sort(list, m_ItemlistFunctions.sortByStat)
	end
	
	if not m_ItemlistFunctions.categoryList then
		m_ItemlistFunctions.categoryList = {ITEMTYPE_ONEHANDED_MELEE, ITEMTYPE_TWOHANDED_MELEE, ITEMTYPE_BOW, ITEMTYPE_CROSSBOW, ITEMTYPE_SPEAR, ITEMTYPE_QUIVER, ITEMTYPE_AMMUNITION, ITEMTYPE_WAND, ITEMTYPE_HELMET}
	end
	
	if not ignore and m_ItemlistFunctions.openByItem and not isInArray(m_ItemlistFunctions.categoryList, category) then
		for i = 1, #m_ItemlistFunctions.itemTypeList do
			if m_ItemlistFunctions.itemTypeList[i][1] == category then
				scheduleEvent(function()
					m_ItemlistList.itemCategoryPanel:increment(i * 38)
				end, 50)
				
				break
			end
		end
	end
	
	for i = 1, #list do
		local it = list[i]
		local widget = g_ui.createWidget('ItemCategory', m_ItemlistList.itemListPanel)
		widget:setText(it.name[m_ItemlistFunctions.language])
		widget.onMouseRelease = m_ItemlistFunctions.selectItem
		widget.itemId = it.clientId
		widget.category = category
		
		if m_ItemlistList.attributeId ~= ITEM_ALL and not modules.game_lookat.m_LookAtFunctions.hasAttribute(it.clientId, m_ItemlistList.attributeId, m_ItemlistList.pureQuality == 'legendary') then
			widget:hide()
		end
		
		local item = widget:getChildById('item')
		item:setItemId(it.clientId)
		item:getItem():setCategoryId(it.itemType)
		
		if m_ItemlistFunctions.openByItem and m_ItemlistFunctions.openByItem[1] == it.clientId then
			m_ItemlistFunctions.selectItem(widget, nil, MouseLeftButton)
			
			if i > 7 then
				scheduleEvent(function()
					m_ItemlistList.itemListPanel:increment(i * 38)
				end, 50)
			end
		end
	end
end

m_ItemlistFunctions.isInspect = function()
	if not m_ItemlistList.window then
		return false
	end
	
	return true
end

m_ItemlistFunctions.selectNpc = function(self)
	modules.game_encyclopedia.m_NpcGlosaryFunctions.selectNpc(self.name)
end

m_ItemlistFunctions.selectItem = function(self, mousePosition, mouseButton)
	if mouseButton == MouseLeftButton then
		if m_ItemlistList.currentSelectItem == self then
			return false
		end
		
		modules.game_lookat.m_LookAtFunctions.inspectItem(self:getChildById('item'):getItem(), m_ItemlistList.pureQuality, true)
	elseif mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		menu:addOption(tr('Look'), function() return modules.game_lookat.m_LookAtFunctions.inspectItem(self:getChildById('item'):getItem(), m_ItemlistList.pureQuality, true) end)
		menu:addOption(tr('Create chat widget'), function() g_game.createLookWindow(self:getChildById('item'):getItem()) end)
		menu:display(mousePosition)
	else
		return false
	end
	
	if m_ItemlistList.currentSelectItem then
		m_ItemlistList.currentSelectItem:setOn(false)
	end
	
	m_ItemlistList.currentSelectItem = self
	self:setOn(true)
	
	local list = modules.game_lookat.m_LookAtFunctions.getItemList()[self.category]
	if not list then
		return true
	end
	
	m_ItemlistList.monsterListPanel:destroyChildren()
	m_ItemlistList.npcListPanel:destroyChildren()
	for i = 1, #list do
		if list[i].clientId == self.itemId then
			for j = 1, #list[i].npcList do
				local v = list[i].npcList[j]
				local widget = g_ui.createWidget('NPCClassList', m_ItemlistList.npcListPanel)
				widget:setTooltip(v.name)
				widget:getChildById('sell'):setText(v.sell)
				widget:getChildById('buy'):setText(v.buy)
				widget.name = v.name
				
				local creature = Creature.create()
				creature:setOutfit(v.look)
				widget:getChildById('creature'):setCreature(creature)
			end
			
			for j = 1, #list[i].list do
				local v = list[i].list[j]
				if v.classId then
					local killed = modules.game_encyclopedia.m_BestiaryFunctions.getMonsterKilled(v.name)
					if killed >= v.killAmount[1] then
						local chance = '?'
						if killed >= v.killAmount[2] then
							for k = 1, #v.loot do
								if list[i].id == v.loot[k].id then
									chance = v.loot[k].chance / 1000
									break
								end
							end
						end
						
						local widget = g_ui.createWidget('CreatureClassList', m_ItemlistList.monsterListPanel)
						widget:setTooltip(v.description[m_ItemlistFunctions.language])
						widget:setText(chance .. '%')
						
						local creature = Creature.create()
						creature:setOutfit(v.look)
						widget:getChildById('creature'):setCreature(creature)
						
						widget.onMouseRelease = function() m_ItemlistFunctions.selectCreature(v.name, v.classId) end
					end
				end
			end
			
			break
		end
	end
	
	return true
end

m_ItemlistFunctions.clearChecks = function(var)
	for _, pid in pairs(var) do
		pid:setOn(false)
	end
end

m_ItemlistFunctions.addItemDescription = function(widget)
	m_ItemlistFunctions.destroyCurrentItem()
	
	m_ItemlistList.currentItem = widget
	widget:setPhantom(true)
	widget:setParent(m_ItemlistList.itemPanel)
	
	if m_ItemlistList.currentSelectItem then
		m_ItemlistFunctions.clearChecks(m_ItemlistList.itemListPanel:getChildren())
		m_ItemlistList.currentSelectItem:setOn(true)
		
		m_ItemlistFunctions.openByItem = {m_ItemlistList.currentSelectItem.itemId, m_ItemlistList.currentSelectItem.category}
	end
	
	m_ItemlistList.nameTextEdit:focus()
end

m_ItemlistFunctions.updateHeight = function(widget)
	if m_ItemlistList.currentItem == widget then
		widget:setHeight(math.min(widget:getHeight(), m_ItemlistList.window:getHeight() - 10))
	end
end

m_ItemlistFunctions.setCategory = function(self, mousePosition, mouseButton)
	if not m_ItemlistList.window then
		return false
	end
	
	m_ItemlistList.nameTextEdit:focus()
	
	m_ItemlistFunctions.clearChecks(m_ItemlistList.itemCategoryPanel:getChildren())
	self:setOn(true)
	
	local id = tonumber(self:getId())
	m_ItemlistFunctions.openCategory(id, true)
	
	if id == m_ItemlistFunctions.disablesUnique and m_ItemlistList.lastId ~= m_ItemlistFunctions.disablesUnique then
		m_ItemlistList.qualityBox:clearOptions()
		
		for _, v in pairs(m_ItemlistFunctions.qualityList) do
			m_ItemlistList.qualityBox:addOption(v)
		end
		
		m_ItemlistList.qualityBox:setCurrentOption(m_ItemlistFunctions.qualityList[3])
	elseif m_ItemlistList.lastId == m_ItemlistFunctions.disablesUnique then
		m_ItemlistList.qualityBox:clearOptions()
		
		for _, v in pairs(m_ItemlistFunctions.expandQualityList) do
			m_ItemlistList.qualityBox:addOption(v)
		end
		
		m_ItemlistList.qualityBox:setCurrentOption(m_ItemlistFunctions.expandQualityList[4])
	end
	
	if isInArray(m_ItemlistFunctions.disablesQuality, id) then
		m_ItemlistList.qualityBox:setEnabled(false)
		m_ItemlistList.qualityBox:setCurrentOption(m_ItemlistFunctions.qualityList[3])
	else
		m_ItemlistList.qualityBox:setEnabled(true)
	end
	
	m_ItemlistList.lastId = id
end

m_ItemlistFunctions.selectCreature = function(name, class)
	modules.game_encyclopedia.m_BestiaryFunctions.selectMonsterByName(name, class)
end

m_ItemlistFunctions.openItemListByBestiary = function(itemId, class)
	m_ItemlistFunctions.openByItem = {itemId, class}
	if class == 0 then
		return true
	end
	
	if not m_ItemlistList.window then
		modules.game_encyclopedia.m_EncyclopediaFunctions.selectById(ID_ITEMLIST)
	else
		m_ItemlistFunctions.clearChecks(m_ItemlistList.itemCategoryPanel:getChildren())
		m_ItemlistList.itemCategoryPanel:getChildById(m_ItemlistFunctions.openByItem[2]):setOn(true)
		
		m_ItemlistFunctions.openCategory(m_ItemlistFunctions.openByItem[2])
	end
end