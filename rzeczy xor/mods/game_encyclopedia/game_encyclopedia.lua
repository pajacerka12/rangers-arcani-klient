m_EncyclopediaFunctions = {}

m_BestiaryList = {}
m_BestiaryFunctions = {}

m_ItemlistList = {}
m_ItemlistFunctions = {}

m_SetsList = {}
m_SetsFunctions = {}

m_NpcGlosaryList = {}
m_NpcGlosaryFunctions = {}

m_CalendarEventsList = {}
m_CalendarEventsFunctions = {}

ID_BESTIARY = 1
ID_ITEMLIST = 2
ID_SETS = 3
ID_NPCGLOSARY = 4
ID_CALENDAREVENTS = 5

m_EncyclopediaFunctions.config = {
	[ID_BESTIARY] = {'2383 342 40 40', tr('BESTIARY')},
	[ID_ITEMLIST] = {'2374 389 40 40', tr('ITEM LIST')},
	[ID_SETS] = {'2420 389 40 40', tr('SETS')},
	[ID_NPCGLOSARY] = {'2281 388 40 40', tr('NPC LIST')},
	[ID_CALENDAREVENTS] = {'2427 342 40 40', tr('EVENTS')}
}

m_EncyclopediaFunctions.destroySelect = function()
	if m_EncyclopediaFunctions.selectId == ID_BESTIARY then
		m_BestiaryFunctions.destroy()
	elseif m_EncyclopediaFunctions.selectId == ID_ITEMLIST then
		m_ItemlistFunctions.destroy()
	elseif m_EncyclopediaFunctions.selectId == ID_SETS then
		m_SetsFunctions.destroy()
	elseif m_EncyclopediaFunctions.selectId == ID_NPCGLOSARY then
		m_NpcGlosaryFunctions.destroy()
	elseif m_EncyclopediaFunctions.selectId == ID_CALENDAREVENTS then
		m_CalendarEventsFunctions.destroy()
	end
end

m_EncyclopediaFunctions.updateIcon = function()
	local v = m_EncyclopediaFunctions.config[m_EncyclopediaFunctions.selectId]
	m_EncyclopediaFunctions.icon:setImageClip(v[1])
	m_EncyclopediaFunctions.title:setText(v[2]:gsub('\n', ' '))
end

m_EncyclopediaFunctions.select = function(self, id, ignore)
	id = tonumber(id)
	if not ignore and m_EncyclopediaFunctions.selectId == id then
		return true
	end

	m_EncyclopediaFunctions.destroySelect()
	m_EncyclopediaFunctions.selectId = id
	if id == ID_BESTIARY then
		m_BestiaryFunctions.open(m_EncyclopediaFunctions.description)
	elseif id == ID_ITEMLIST then
		m_ItemlistFunctions.open(m_EncyclopediaFunctions.description)
	elseif id == ID_SETS then
		m_SetsFunctions.open(m_EncyclopediaFunctions.description)
	elseif id == ID_NPCGLOSARY then
		m_NpcGlosaryFunctions.open(m_EncyclopediaFunctions.description)
	elseif id == ID_CALENDAREVENTS then
		m_CalendarEventsFunctions.open(m_EncyclopediaFunctions.description)
	end
	
	if m_EncyclopediaFunctions.current then
		m_EncyclopediaFunctions.current:setOn(false)
	end
	
	m_EncyclopediaFunctions.updateIcon()
	m_EncyclopediaFunctions.current = self
	self:setOn(true)
end

m_EncyclopediaFunctions.instantSelect = function(id)
	if id == ID_BESTIARY then
		m_EncyclopediaFunctions.select(m_EncyclopediaFunctions.window:getChildById('1'), id, true)
	elseif id == ID_ITEMLIST then
		m_EncyclopediaFunctions.select(m_EncyclopediaFunctions.window:getChildById('2'), id, true)
	elseif id == ID_SETS then
		m_EncyclopediaFunctions.select(m_EncyclopediaFunctions.window:getChildById('3'), id, true)
	elseif id == ID_NPCGLOSARY then
		m_EncyclopediaFunctions.select(m_EncyclopediaFunctions.window:getChildById('4'), id, true)
	elseif id == ID_CALENDAREVENTS then
		m_EncyclopediaFunctions.select(m_EncyclopediaFunctions.window:getChildById('5'), id, true)
	end
end

m_EncyclopediaFunctions.selectById = function(id)
	if not m_EncyclopediaFunctions.window then
		m_EncyclopediaFunctions.create(true)
	end
	
	m_EncyclopediaFunctions.instantSelect(id)
end

m_EncyclopediaFunctions.create = function(ignore)
	if m_EncyclopediaFunctions.window then
		m_EncyclopediaFunctions.destroy()
	else
		modules.game_character.m_CharacterFunctions.destroy()
		modules.game_rewards.m_RewardsFunctions.destroy()
		modules.game_questlog.m_QuestLogFunctions.destroy()
		modules.game_store.m_StoreFunctions.onCloseStore()
		modules.game_market.m_MarketFunctions.destroy()
		
		m_EncyclopediaFunctions.window = g_ui.displayUI('game_encyclopedia')
		m_EncyclopediaFunctions.description = m_EncyclopediaFunctions.window:getChildById('descriptionWindow')
		m_EncyclopediaFunctions.icon = m_EncyclopediaFunctions.window:getChildById('icon')
		m_EncyclopediaFunctions.title = m_EncyclopediaFunctions.window:getChildById('title')
		
		if not ignore then
			if m_EncyclopediaFunctions.selectId then
				m_EncyclopediaFunctions.instantSelect(m_EncyclopediaFunctions.selectId)
			else
				m_EncyclopediaFunctions.select(m_EncyclopediaFunctions.window:getChildById('2'), ID_ITEMLIST)
			end
		end
	
		modules.game_bottompanel.m_MainFunctions.open(WINDOW_ENCYCLOPEDIA)
	end
end

m_EncyclopediaFunctions.destroy = function()
	if m_EncyclopediaFunctions.window then
		m_EncyclopediaFunctions.destroySelect()
		
		m_EncyclopediaFunctions.window:destroy()
		m_EncyclopediaFunctions.window = nil
		
		m_EncyclopediaFunctions.description = nil
		m_EncyclopediaFunctions.icon = nil
		m_EncyclopediaFunctions.title = nil
		m_EncyclopediaFunctions.current = nil
		
		modules.game_bottompanel.m_MainFunctions.close(WINDOW_ENCYCLOPEDIA)
	end
end

function onGameEnd()
	m_EncyclopediaFunctions.destroy()
	
	m_BestiaryFunctions.onGameEnd()
	m_ItemlistFunctions.onGameEnd()
	m_SetsFunctions.onGameEnd()
	m_NpcGlosaryFunctions.onGameEnd()
	m_CalendarEventsFunctions.onGameEnd()
end

function onGameStart()
	m_BestiaryFunctions.onGameStart()
	m_ItemlistFunctions.onGameStart()
	m_NpcGlosaryFunctions.onGameStart()
	m_CalendarEventsFunctions.onGameStart()
end

function onLoad()
	g_ui.importStyle('game_bestiary')
	g_ui.importStyle('game_itemlist')
	g_ui.importStyle('game_sets')
	g_ui.importStyle('game_npcglosary')
	g_ui.importStyle('game_calendarevents')
	
	m_BestiaryFunctions.onLoad()
	m_ItemlistFunctions.onLoad()
	m_SetsFunctions.onLoad()
	m_NpcGlosaryFunctions.onLoad()
	m_CalendarEventsFunctions.onLoad()
	
	connect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
end

function onUnload()
	m_EncyclopediaFunctions.destroy()
	
	m_BestiaryFunctions.onUnload()
	m_ItemlistFunctions.onUnload()
	m_SetsFunctions.onUnload()
	m_NpcGlosaryFunctions.onUnload()
	m_CalendarEventsFunctions.onUnload()
	
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
end