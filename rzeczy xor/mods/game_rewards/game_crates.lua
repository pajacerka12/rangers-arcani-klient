m_CratesFunctions.CRATE_NONE = -1
m_CratesFunctions.CRATE_SMALL = 0
m_CratesFunctions.CRATE_MEDIUM = 1
m_CratesFunctions.CRATE_BIG = 2
m_CratesFunctions.CRATE_HUGE = 3
m_CratesFunctions.CRATE_BACKPACK = 4
m_CratesFunctions.CRATE_RUNE = 5
m_CratesFunctions.CRATE_LAST = m_CratesFunctions.CRATE_RUNE

m_CratesFunctions.speed = 2
m_CratesFunctions.rewardWindowSize = 40
m_CratesFunctions.squareAmount = 8

m_CratesFunctions.imageClip = {
	[m_CratesFunctions.CRATE_SMALL] = {
		clip = '192 0 64 64',
		name = tr('BRONZE CHEST'),
		description = tr('You can obtain this chest from monsters with 0.1%% chance and from selected tasks at Head Hunters Camp.')
	},
	[m_CratesFunctions.CRATE_MEDIUM] = {
		clip = '128 0 64 64', 
		name = tr('SILVER CHEST'), 
		description = tr('You can obtain this chest from boses with 5%% chance and from selected tasks at Head Hunters Camp.')
	},
	[m_CratesFunctions.CRATE_BIG] = {
		clip = '64 0 64 64', 
		name = tr('GOLDEN CHEST'), 
		description = tr('This chest is not obtainable at the moment.')
	},
	[m_CratesFunctions.CRATE_HUGE] = {
		clip = '0 0 64 64', 
		name = tr('CRYSTAL CHEST'), 
		description = tr('This chest is not obtainable at the moment.')
	},
	[m_CratesFunctions.CRATE_BACKPACK] = {
		clip = '256 0 64 64', 
		name = tr('SCALE CHEST'),
		description = tr('You can obtain this chest from monsters with random chance and from premium shop.')
	},
	[m_CratesFunctions.CRATE_RUNE] = {
		clip = '320 0 64 64', 
		name = tr('STONE CHEST'), 
		description = tr('You can obtain this chest from monsters with random chance, bosses with higher chance and from quests as blank rune converted into a chest after use.')
	},
}

m_CratesFunctions.cornerClip = {
	['desolate'] = ITEMCLASS_DESOLATE,
	['damaged'] = ITEMCLASS_DAMAGED,
	['normal'] = ITEMCLASS_NORMAL,
	['perfect'] = ITEMCLASS_PERFECT,
	['legendary'] = ITEMCLASS_LEGENDARY,
	['unique'] = ITEMCLASS_UNIQUE,
	['shop'] = ITEMCLASS_SHOP,
	['quest'] = ITEMCLASS_QUEST
}

m_CratesFunctions.m_CrateAmount = {}
for i = m_CratesFunctions.CRATE_SMALL, m_CratesFunctions.CRATE_LAST do
	m_CratesFunctions.m_CrateAmount[i] = 0
end

function onSendCrateList(list)
	for k, v in pairs(list) do
		m_CratesFunctions.m_CrateAmount[k] = v
		
		if m_CratesList.window then
			local widget = m_CratesList.panel:getChildById(k)
			if widget then
				widget:getChildById('amount'):setText(v)
				
				if m_CratesList.currentSelect == widget then
					m_CratesList.randomize:setEnabled(v > 0)
				end
			end
		end
	end
end

function onSendCrateItemList(list)
	m_CratesList.distance = (#list * m_CratesFunctions.rewardWindowSize) + (-m_CratesFunctions.rewardWindowSize + #list - m_CratesFunctions.squareAmount + 5)
	
	for i = 1, m_CratesFunctions.squareAmount do
		m_CratesList.items[i]:setMarginLeft(0)
	end
	
	for i = 1, m_CratesFunctions.squareAmount do
		local widget = m_CratesList.items[i]
		local item = widget:getChildById('item')
		if not list[i][4] or list[i][4] == 'none' then
			list[i][4] = 'normal'
		end
		
		m_CratesFunctions.updateItem(widget, list[i][4], list[i][1])
		
		if i <= 3 then
			m_CratesFunctions.updateItem(m_CratesList.item, list[#list - 3 + i][4], list[#list - 3 + i][1])
		else
			m_CratesFunctions.updateItem(m_CratesList.item, list[i - 3][4], list[i - 3][1])
		end
	end
	
	m_CratesFunctions.update(list, 5, 5)
end

m_CratesFunctions.onLoad = function()
	connect(g_game, {
		onSendCrateList = onSendCrateList,
		onSendCrateItemList = onSendCrateItemList
	})
end

m_CratesFunctions.onUnload = function()
	disconnect(g_game, {
		onSendCrateList = onSendCrateList,
		onSendCrateItemList = onSendCrateItemList
	})
end

m_CratesFunctions.onGameStart = function()
	
end

m_CratesFunctions.destroy = function()
	if m_CratesList.window then
		if m_CratesList.distance > 0 then
			g_game.sendRewardToPlayer()
		end
		
		m_CratesList.panel:destroyChildren()
		
		m_CratesList.window:destroy()
		m_CratesList.window = nil
		
		m_CratesList = {}
	end
end

m_CratesFunctions.open = function(parent)
	if m_CratesList.window then
		m_CratesFunctions.destroy()
	else
		m_CratesList.window = g_ui.createWidget('CratesWindow', parent)
		m_CratesList.panel = m_CratesList.window:getChildById('panel')
		m_CratesList.selectedCrate = m_CratesList.window:getChildById('selectedCrate')
		m_CratesList.crateName = m_CratesList.window:getChildById('crateName')
		m_CratesList.crateDescription = m_CratesList.window:getChildById('crateDescription')
		m_CratesList.randomize = m_CratesList.window:getChildById('randomize')
		m_CratesList.randomizePanel = m_CratesList.window:getChildById('randomizePanel')
		m_CratesList.itemList = m_CratesList.randomizePanel:getChildById('itemList')
		m_CratesList.item = m_CratesList.randomizePanel:getChildById('item')
		
		m_CratesList.items = {}
		for i = 1, m_CratesFunctions.squareAmount do
			m_CratesList.items[i] = m_CratesList.itemList:getChildById(i)
		end
		
		m_CratesList.distance = 0
		
		m_CratesFunctions.execute()
	end
end

m_CratesFunctions.setClassType = function(widget, class)
	widget:setBackgroundColor(ITEM_NAME_COLOR[class] .. '44')
	
	local corner = widget:getChildById('corner')
	corner:setImageClip(getImageClipByClassId(m_CratesFunctions.cornerClip[class]))
	corner:show()
end

m_CratesFunctions.randomize = function()
	if not m_CratesList.currentSelect then
		return true
	end
	
	g_game.sendRewardToPlayer()
	g_game.sendCrateOpening(tonumber(m_CratesList.currentSelect:getId()))
end

m_CratesFunctions.select = function(self)
	if m_CratesList.currentSelect then
		m_CratesList.currentSelect:getChildById('blink'):hide()
	end
		
	local id = tonumber(self:getId())
	self:getChildById('blink'):show()
	
	m_CratesList.crateName:setText(m_CratesFunctions.imageClip[id].name)
	m_CratesList.crateDescription:setText(m_CratesFunctions.imageClip[id].description)
	m_CratesList.selectedCrate:setImageClip(m_CratesFunctions.imageClip[id].clip)
	m_CratesList.selectedCrate:show()
	
	m_CratesList.currentSelect = self
	m_CratesList.randomize:setEnabled(m_CratesFunctions.m_CrateAmount[id] > 0)
end

m_CratesFunctions.execute = function()
	for i = m_CratesFunctions.CRATE_SMALL, m_CratesFunctions.CRATE_LAST do
		local widget = g_ui.createWidget('CratePlatform', m_CratesList.panel)
		widget:getChildById('crate'):setImageClip(m_CratesFunctions.imageClip[i].clip)
		widget:setId(i)
		widget.onMouseRelease = m_CratesFunctions.select
		
		if m_CratesFunctions.m_CrateAmount[i] then
			widget:getChildById('amount'):setText(m_CratesFunctions.m_CrateAmount[i])
		end
	end
end

m_CratesFunctions.updateItem = function(widget, classId, itemId)
	if not classId or classId == 'none' then
		classId = 'normal'
	end
	
	local item = widget:getChildById('item')
	item:setItem(itemId)
	item.classId = getClassIdByName(classId)
	m_CratesFunctions.setClassType(widget, classId)
end

m_CratesFunctions.update = function(list, value, realValue, moves)
	if not m_CratesList.distance or m_CratesList.distance <= 0 then
		m_CratesList.distance = 0
		g_game.sendRewardToPlayer()
		
		if m_CratesList.window then
			m_CratesFunctions.updateItem(m_CratesList.item, list[#list][4], list[#list][1])
		end
		
		return true
	end
	
	local x, widget = 0, nil
	for i = 1, m_CratesFunctions.squareAmount do
		local window = m_CratesList.items[i]
		if x == 0 or x > window:getX() then
			x = window:getX()
			widget = window
		end
	end
	
	for i = 1, moves or 1 do
		m_CratesFunctions.move(widget, -math.min(m_CratesList.distance, m_CratesFunctions.speed))
	end
	
	widget = m_CratesFunctions.checkSquares()
	if widget then
		value = value + 1
		realValue = realValue + 1
		
		if value > #list then
			value = 1
			realValue = #list
		end
		
		if not list[value][4] or list[value][4] == 'none' then
			list[value][4] = 'normal'
		end
		
		m_CratesFunctions.updateItem(widget, list[value][4], list[value][1])
		
		if value <= 3 then
			m_CratesFunctions.updateItem(m_CratesList.item, list[#list - 3 + value][4], list[#list - 3 + value][1])
		else
			m_CratesFunctions.updateItem(m_CratesList.item, list[value - 3][4], list[value - 3][1])
		end
	end
	
	local delay = 17
	if realValue >= #list then
		delay = 3
	elseif realValue >= #list - 2 then
		delay = 4
	elseif realValue >= #list - 4 then
		delay = 5
	elseif realValue >= #list - 6 then
		delay = 6
	elseif realValue >= #list - 8 then
		delay = 7
	elseif realValue >= #list - 10 then
		delay = 8
	elseif realValue >= #list - 12 then
		delay = 9
	elseif realValue >= #list - 14 then
		delay = 10
	elseif realValue >= #list - 16 then
		delay = 11
	elseif realValue >= #list - 18 then
		delay = 12
	elseif realValue >= #list - 20 then
		delay = 13
	elseif realValue >= #list - 22 then
		delay = 14
	elseif realValue >= #list - 24 then
		delay = 15
	elseif realValue >= #list - 26 then
		delay = 16
	end
	
	delay = delay - 1

	removeEvent(m_CratesList.event)
	m_CratesList.event = scheduleEvent(function() m_CratesFunctions.update(list, value, realValue, delay) end, 50)
end

m_CratesFunctions.move = function(widget, x)
	if x ~= 0 then
		widget:setMarginLeft(widget:getMarginLeft() + x)
	end
	
	m_CratesList.distance = m_CratesList.distance + x
end

m_CratesFunctions.checkSquares = function()
	for i = 1, m_CratesFunctions.squareAmount do
		local window = m_CratesList.items[i]
		if window:getX() + m_CratesFunctions.rewardWindowSize + 1 < m_CratesList.itemList:getX() then
			local id = i + 1
			if i == m_CratesFunctions.squareAmount then
				id = 1
			end
				
			local widget = m_CratesList.itemList:getChildById(id)
			widget:addAnchor(AnchorLeft, 'parent', AnchorLeft)
			widget:setMarginLeft((window:getX() + m_CratesFunctions.rewardWindowSize + 1 - m_CratesList.itemList:getX()))
			
			if i == 1 then
				id = m_CratesFunctions.squareAmount
			else
				id = i - 1
			end
				
			window:addAnchor(AnchorLeft, id, AnchorRight)
			window:setMarginLeft(0)
			return window
		end
	end
	
	return false
end