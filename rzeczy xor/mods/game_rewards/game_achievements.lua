m_AchievementsFunctions.ACHIEVEMENT_STATUS_UNCOMPLETE 	= 0
m_AchievementsFunctions.ACHIEVEMENT_STATUS_OBTAIN 		= 1
m_AchievementsFunctions.ACHIEVEMENT_STATUS_COMPLETE	 	= 2

m_AchievementsFunctions.text = 'FOR NEXT REWARD YOU NEED %d STARS'

function onAchievementsList(requiredStars, rewards, list)
	m_AchievementsFunctions.onAchievementsList(requiredStars, rewards, list)
end

function onAmountAchievement(id, amount, level, status, requirements)
	m_AchievementsFunctions.onAmountAchievement(id, amount, level, status, requirements)
end

function onUpdateAchievement(id, status)
	m_AchievementsFunctions.onUpdateAchievement(id, status)
end

m_AchievementsFunctions.onLoad = function()
	connect(g_game, {
		onAchievementsList = onAchievementsList,
		onAmountAchievement = onAmountAchievement,
		onUpdateAchievement = onUpdateAchievement
	})
end

m_AchievementsFunctions.onUnload = function()
	disconnect(g_game, {
		onAchievementsList = onAchievementsList,
		onAmountAchievement = onAmountAchievement,
		onUpdateAchievement = onUpdateAchievement
	})
end

m_AchievementsFunctions.onGameStart = function()
	
end

m_AchievementsFunctions.destroy = function()
	if m_AchievementsList.window then
		m_AchievementsFunctions.executeWindow(true)
		m_AchievementsList.list:destroyChildren()
		
		m_AchievementsList.window:destroy()
		
		m_AchievementsList = {}
	end
end

m_AchievementsFunctions.open = function(parent)
	if m_AchievementsList.window then
		m_AchievementsFunctions.destroy()
	else
		m_AchievementsList.window = g_ui.createWidget('AchievementsWindow', parent)
		m_AchievementsList.requiredStars = m_AchievementsList.window:getChildById('requiredStars')
		m_AchievementsList.achievements = m_AchievementsList.window:getChildById('achievements')
		m_AchievementsList.rewardList = m_AchievementsList.window:getChildById('rewardList')
		
		
		m_AchievementsList.list = m_AchievementsList.window:getChildById('list')
		m_AchievementsList.achievements = m_AchievementsList.window:getChildById('achievements')
		m_AchievementsList.currentId = {}
		
		m_AchievementsFunctions.execute()
	end
end

m_AchievementsFunctions.execute = function()
	if g_game.isOnline() then
		m_AchievementsFunctions.executeWindow(false)
	end
end

m_AchievementsFunctions.executeWindow = function(closed)
	if g_game.isOnline() then
		local msg = OutputMessage.create()
		msg:addU8(81)
		msg:addU8(closed and 1 or 0)
		g_game.getProtocolGame():send(msg)
	end
end

m_AchievementsFunctions.claimReward = function(id)
	if id == 1 and not m_AchievementsList.achievements:isOn() then
		return false
	end
	
	if g_game.isOnline() then
		local msg = OutputMessage.create()
		msg:addU8(85)
		msg:addU16(id)
		g_game.getProtocolGame():send(msg)
	end
end

m_AchievementsFunctions.add = function(achievement)
	local id, amount, requirements, level, status, name, description, items = unpack(achievement)
	if name == '' then
		return 0
	end
	
	if #m_AchievementsList.currentId > 0 and not isInArray(m_AchievementsList.currentId, id) then
		return level
	end
	
	local widget = m_AchievementsList.list:getChildById(id)
	if widget then
		m_AchievementsFunctions.onAmountAchievement(id, amount, level, status, requirements)
		return level
	end
	
	local widget = g_ui.createWidget('AchievementsRegistration', m_AchievementsList.list)
	widget:setId(id)
	widget:setParent(m_AchievementsList.list)
	widget:getChildById('name'):setText(name)
	widget:getChildById('description'):setText(description)
	widget:getChildById('amount'):setText(amount .. ' / ' .. requirements)
	
	widget.level = level
	
	local list = widget:getChildById('level')
	for i = 1, 5 do
		local uid = g_ui.createWidget('AchievementsStar', list)
		uid:setParent(list)
		uid:setId(i)
		if level == 5 and status == m_AchievementsFunctions.ACHIEVEMENT_STATUS_COMPLETE then
			uid:setChecked(true)
		elseif i <= level then
			uid:setOn(true)
		end
	end
	
	local list = widget:getChildById('items')
	for i = 1, #items do
		local widget = g_ui.createWidget('AchievementsItem', list)
		widget:getChildById('item'):setItem(items[i])
	end
	
	list:setOn(#items > 1)
	list:setWidth((#items * 48) + 6)
	m_AchievementsFunctions.updateButton(widget, id, status)
	return level
end

m_AchievementsFunctions.look = function(self)
	local menu = g_ui.createWidget('PopupMenu')
	menu:setGameMenu(true)
	
	local classId = getClassNameById(self.classId or self:getClassType())
	menu:addOption(tr('Look'), function() return modules.game_lookat.m_LookAtFunctions.inspectItem(self:getItem(), classId, false, self:isVirtual() or self:getItemCount() == 0) end)
	menu:display(mousePosition)
end

m_AchievementsFunctions.onAmountAchievement = function(id, amount, level, status, requirements)
	if not m_AchievementsList.window then
		return false
	end
	
	local widget = m_AchievementsList.list:getChildById(id)
	if not widget then
		table.insert(m_AchievementsList.currentId, id)
		m_AchievementsFunctions.executeWindow(false)
		return false
	end
	
	for i = 1, level do
		widget:getChildById('level'):getChildById(i):setOn(true)
	end
	
	m_AchievementsFunctions.update(widget, id, status)
	widget:getChildById('amount'):setText(amount .. ' / ' .. requirements)
	widget.level = level
	
	local amount = 0
	for _, pid in pairs(m_AchievementsList.list:getChildren()) do
		amount = amount + pid.level
	end
	
	m_AchievementsList.achievements:setText(amount)
end

m_AchievementsFunctions.updateButton = function(widget, id, status)
	local button = widget:getChildById('button')
	if status == m_AchievementsFunctions.ACHIEVEMENT_STATUS_OBTAIN then
		button:setOn(true)
		button:setChecked(false)
		button.onMouseRelease = function(widget, mousePosition, mouseButton)
			if mouseButton == MouseLeftButton then
				m_AchievementsFunctions.claimReward(id)
			end
		end
	elseif status == m_AchievementsFunctions.ACHIEVEMENT_STATUS_COMPLETE then
		button:setOn(false)
		button:setChecked(true)
		button.onMouseRelease = nil
	end
end

m_AchievementsFunctions.update = function(widget, id, status)
	m_AchievementsFunctions.updateButton(widget, id, status)
	
	if status == m_AchievementsFunctions.ACHIEVEMENT_STATUS_COMPLETE then
		local list = widget:getChildById('level')
		for _, pid in pairs(list:getChildren()) do
			pid:setOn(false)
			pid:setChecked(true)
		end
	end
end

m_AchievementsFunctions.onUpdateAchievement = function(id, status)
	if not m_AchievementsList.window then
		return false
	end
	
	local widget = m_AchievementsList.list:getChildById(id)
	if not widget then
		return false
	end
	
	m_AchievementsFunctions.update(widget, id, status)
end

m_AchievementsFunctions.onAchievementsList = function(requiredStars, rewards, list)
	if not m_AchievementsList.window then
		return false
	end
	
	local amount = 0
	for i = 1, #list do
		amount = amount + m_AchievementsFunctions.add(list[i])
	end
	
	m_AchievementsList.rewardList:destroyChildren()
	for i = 1, #rewards do
		local widget = g_ui.createWidget('Item', m_AchievementsList.rewardList)
		widget:setItem(rewards[i])
	end
	
	m_AchievementsList.currentId = {}
	m_AchievementsList.achievements:setText(amount)
	m_AchievementsList.requiredStars:setText(tr(m_AchievementsFunctions.text, requiredStars))
	m_AchievementsList.achievements:setOn(amount >= requiredStars)
	
	m_AchievementsList.stars = requiredStars
end