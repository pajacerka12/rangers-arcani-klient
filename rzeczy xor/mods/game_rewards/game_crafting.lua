m_CraftingFunctions.GameServerSendCraft = 87
m_CraftingFunctions.GameServerUpdateCraft = 68

m_CraftingFunctions.color = {
	'#846F60',
	'#B00101'
}

m_CraftingFunctions.itemTypeList = {ITEMTYPE_CREATURE_PRODUCT, ITEMTYPE_ONEHANDED_MELEE, ITEMTYPE_TWOHANDED_MELEE, ITEMTYPE_BOW, ITEMTYPE_CROSSBOW, ITEMTYPE_SPEAR, ITEMTYPE_QUIVER, ITEMTYPE_WAND, ITEMTYPE_HELMET, ITEMTYPE_ARMOR, ITEMTYPE_LEGS, ITEMTYPE_FEET, ITEMTYPE_SHIELD, ITEMTYPE_NECKLACE, ITEMTYPE_RING, ITEMTYPE_ALL}

m_CraftingFunctions.tr = {}

m_CraftingFunctions.text = {
	tr('Success!'),
	tr('Failure!'),
	tr('Duration'),
	tr('Chance'),
	tr(' chance for recipe scroll')
}

m_CraftingFunctions.additivesValue = {
	[12133] = 1.2,
	[12134] = 1.5,
	[12135] = 2.0,
	
	[12139] = 0.85,
	[12140] = 0.70,
	[12141] = 0.50,
	
	[12136] = 1.10,
	[12137] = 1.20,
	[12138] = 1.30,
	
	[12148] = 0.70,
	[12149] = 0.50,
	[12150] = 0.10
}

m_CraftingFunctions.additives = {
	[InventorySlotClover] = 12134,
	[InventorySlotHammer] = 12149,
	[InventorySlotAnvil] = 12137,
	[InventorySlotBellows] = 12140
}

m_CraftingFunctions.quality = {
	[1] = 'Desolate',
	[2] = 'Damaged',
	[3] = 'Normal',
	[4] = 'Perfect',
	[5] = 'Legendary'
}

m_CraftingFunctions.selectWindow = function(change, parent)
	if m_CraftingList.event then
		m_CraftingList.event:cancel()
		m_CraftingList.event = nil
	end
	
	if m_CraftingList.window then
		m_CraftingList.window:destroy()
		m_CraftingList = {}
	end
	
	m_CraftingFunctions.open(change, parent)
end

m_CraftingFunctions.isCraftingItem = function(id)
	for _, v in pairs(m_CraftingFunctions.list) do
		if v.id == id then
			return v.itemList
		end
	end
	
	return false
end

m_CraftingFunctions.getCraftingList = function(id)
	local list = {}
	for _, v in pairs(m_CraftingFunctions.list) do
		for i = 1, #v.itemList do
			if v.itemList[i].id == id then
				table.insert(list, {id = v.id, amount = 1})
				break
			end
		end
	end
	
	return list
end

m_CraftingFunctions.smeltItem = function(item)
	local position = item:getPosition()
	local id = item:getId()
	local stackpos = item:getStackPos()
	
	local msg = OutputMessage.create()
	msg:addU8(m_CraftingFunctions.GameServerSendCraft)
	msg:addU8(12)
	m_CraftingFunctions.protocol:addPosition(msg, position)
	msg:addU16(id)
	msg:addU8(stackpos)
	m_CraftingFunctions.protocol:send(msg)
end

m_CraftingFunctions.updateQuality = function()
	local multiplier = m_CraftingList.additivesMultipliers[m_CraftingList.currentId][InventorySlotAnvil]
	if not multiplier then
		return true
	end
	
	local amount = m_CraftingList.qualityPanel:getChildCount()
	if amount < 2 then
		return true
	end
	
	local total = 0
	for i = 1, amount do
		local widget = m_CraftingList.qualityPanel:getChildByIndex(i)
		local current = math.min(math.max(90, widget.quality), widget.quality * multiplier)
		
		total = total + current
		widget.amount = current
		widget:setText(m_CraftingFunctions.quality[widget.type] .. ': ' .. current .. '%')
	end
	
	if total ~= 100 then
		local diffrent = total - 100
		local j = 0
		while(diffrent ~= 0 and j < 10) do
		local k = amount - 1
		for i = amount, 2, -1 do
			local widget = m_CraftingList.qualityPanel:getChildByIndex(i)
			local current = math.min(diffrent, math.ceil(widget.amount * (k / (amount + 2))))
			
			diffrent = diffrent - current
			widget.amount = widget.amount - current
			widget:setText(m_CraftingFunctions.quality[widget.type] .. ': ' .. widget.amount .. '%')
			k = k - 1
		end
		
		j = j + 1
		end
	end
end

m_CraftingFunctions.updateAdditives = function(slot, item)
	if item then
		m_CraftingList.additivesMultipliers[m_CraftingList.currentId][slot] = m_CraftingFunctions.additivesValue[item:getId()]
	else
		m_CraftingList.additivesMultipliers[m_CraftingList.currentId][slot] = 1
	end
	
	if not m_CraftingList.currentItem then
		return true
	end
	
	if slot == InventorySlotClover then
		m_CraftingFunctions.updateChance(m_CraftingList.currentItem.data.level)
	elseif slot == InventorySlotHammer then
		local player = g_game:getLocalPlayer()
		local diffrent = math.max(0, m_CraftingList.currentItem.data.level - player:getLevel())
		m_CraftingFunctions.updateTime((m_CraftingList.currentItem.data.duration * 60) + (diffrent * diffrent * 60))
	elseif slot == InventorySlotAnvil then
		m_CraftingFunctions.updateQuality()
	elseif slot == InventorySlotBellows then
		for i = 1, #m_CraftingList.currentItem.data.itemList do
			local widget = m_CraftingList.itemList:getChildById(i)
			if widget then
				m_CraftingFunctions.updateLabel(widget, i)
			end
		end
		
		m_CraftingFunctions.updateQuality()
	end
end

m_CraftingFunctions.updateChance = function(level)
	if not m_CraftingList.chancePanel then
		return true
	end
	
	if level == 0 then
		m_CraftingList.chancePanel.chance = 100
	else
		local playerLevel = g_game:getLocalPlayer():getLevel()
		m_CraftingList.chancePanel.chance = math.floor(math.min(100, math.min(55, math.max(1, 55 - (level - playerLevel))) * (m_CraftingList.additivesMultipliers[m_CraftingList.currentId][InventorySlotClover] or 1)))
	end
	
	m_CraftingList.chancePanel:setText(m_CraftingFunctions.text[4] .. ': ' .. m_CraftingList.chancePanel.chance .. '%')
end

m_CraftingFunctions.updateTime = function(seconds, smelting)
	if not m_CraftingList.timePanel then
		return true
	end
	
	local hours = 0
	local minutes = 0
	if seconds then
		if smelting then
			seconds = seconds * (m_CraftingList.additivesMultipliers[m_CraftingList.currentId][InventorySlotSmeltingHammer] or 1)
		else
			seconds = seconds * (m_CraftingList.additivesMultipliers[m_CraftingList.currentId][InventorySlotHammer] or 1)
		end
	else
		seconds = m_CraftingList.timePanel.time
	end
	
	while (seconds >= 60) do
		minutes = minutes + 1
		seconds = seconds - 60
					
		if minutes >= 60 then
			minutes = 0
			hours = hours + 1
		end
	end
	
	m_CraftingList.timePanel:setText(m_CraftingFunctions.text[3] .. ': ' .. hours .. ' : ' .. (minutes < 10 and '0' or '') .. minutes .. ' : ' .. (seconds < 10 and '0' or '') .. seconds)
end

m_CraftingFunctions.createLabel = function(widget, iconId, description, widgetType)
	local uid = g_ui.createWidget(widgetType or 'AttributeLabel', widget)
	local icon = uid:getChildById('icon')
	local label = uid:getChildById('label')
	
	modules.game_lookat.m_LookAtFunctions.setIconImageType(icon, iconId)
	label:setText(description)
	uid:setParent(widget)
	uid:setHeight(math.max(20, label:getHeight()))
	return uid
end

m_CraftingFunctions.hover = function(self, hovered)
	if not hovered then
		if m_CraftingList.hover then
			m_CraftingList.hover:destroy()
			m_CraftingList.hover = nil
		end
		
		return true
	end
	
	local rootWidget = modules.game_interface.getRootPanel()
	m_CraftingList.hover = g_ui.displayUI('item_info')
	
	local it = modules.game_lookat.m_LookAtFunctions.getItemByClientId(tonumber(self.itemId))
	local list = {
		[ITEM_CRAFTING] = true,
		[ITEM_CLASS_ITEM] = {value = self.type}
	}
	
	m_CraftingList.hover = modules.game_lookat.m_LookAtFunctions.execute(false, m_CraftingList.hover, it, list, 1)
	
	local pos = self:getPosition()
	pos.x = pos.x + self:getWidth() - 12
	
	m_CraftingList.hover:setPosition(pos)
end

m_CraftingFunctions.updateLabel = function(label, id)
	if label then
		local amount = 0
		for i = 1, 5 do
			local slotId = 15 + i + (id * 13)
			local pid = m_CraftingList.itemList:getChildById(slotId)
			if pid then
				local slot = pid:getChildById('slot')
				if slot.amount then
					local item = slot:getItem()
					if item then
						amount = amount + item:getCount()
					end
				end
			end
		end
		
		local count = math.ceil(label.amount * (m_CraftingList.additivesMultipliers[m_CraftingList.currentId][InventorySlotBellows] or 1.0))
		label:setText(amount .. '/' .. count)
		if not m_CraftingList.label then
			m_CraftingList.label = {}
		end
		
		m_CraftingList.label[id] = {amount, count}
		
		local craft = true
		local total = 0
		for k, v in pairs(m_CraftingList.label) do
			if not v or v[1] < v[2] then
				craft = false
			end
			
			if v then
				total = total + math.min(v[1], v[2])
			end
		end
		
		local crafting = m_CraftingFunctions.data[m_CraftingList.currentId]
		local timeEnd = 0
		if crafting then
			crafting = m_CraftingFunctions.data[m_CraftingList.currentId].crafting
			timeEnd = m_CraftingFunctions.data[m_CraftingList.currentId].timeEnd
		end
		
		m_CraftingList.qualityPanel:destroyChildren()
		if total > 0 then
			local quality = {}
			local pid = m_CraftingList.creatureProducts[m_CraftingList.currentId]
			for i = 5, 1, -1 do
				if pid and pid[i] then
					local amount = 0
					for _, item in pairs(pid[i]) do
						amount = amount + item:getCount()
					end
					
					if amount > 0 then
						quality[i] = math.round(amount / total * 100)
					end
					
					if quality[i] and quality[i] > 0 then
						local widget = g_ui.createWidget('QualityCrafting', m_CraftingList.qualityPanel)
						widget:setParent(m_CraftingList.qualityPanel)
						widget.type = i
						widget.itemId = m_CraftingList.currentItem:getId()
						widget.quality = quality[i]
						widget.onHoverChange = m_CraftingFunctions.hover
						
						local class = m_CraftingFunctions.quality[i]
						widget:setColor(ITEM_NAME_COLOR[class:lower()])
						widget:setText(class .. ': ' .. quality[i] .. '%')
					end
				end
			end
		end
		
		if crafting then
			m_CraftingList.smith:setEnabled(false)
			m_CraftingList.cancel:setEnabled(false)
		else
			m_CraftingList.smith:setEnabled(craft)
		end
	end
end

m_CraftingFunctions.selectItem = function(self, ignoreCrafting)
	if not self or (not ignoreCrafting and m_CraftingList.blocking) then
		return true
	end
	
	if m_CraftingList.currentItem then
		m_CraftingList.currentItem:getChildById('mask'):hide()
	end
	
	local crafting = m_CraftingFunctions.data[m_CraftingList.currentId] and m_CraftingFunctions.data[m_CraftingList.currentId].crafting
	local pid = m_CraftingList.craftingAdditives[m_CraftingList.currentId]
	for i = InventorySlotClover, InventorySlotBellows do
		local widget = m_CraftingList.description:getChildById(i)
		local slot = widget:getChildById('slot')
		
		slot:setDraggable(not crafting)
		slot:setVirtual(crafting)
		slot.position = {x = 65534, y = i, z = m_CraftingList.currentId}
		
		if pid and pid[i] then
			pid[i]:setPosition(slot.position)
			slot:setItem(pid[i])
		else
			slot:setItem(nil)
		end
		
		if crafting and pid then
			widget:getChildById('visibleMask'):show()
			widget:getChildById('visibleMask'):setPhantom(false)
		else
			widget:getChildById('visibleMask'):hide()
		end
		
		m_CraftingFunctions.updateAdditives(i, pid and pid[i])
	end
	
	m_CraftingList.label = {}
	m_CraftingList.currentItem = self
	m_CraftingList.currentItem:getChildById('mask'):show()
	m_CraftingList.itemList:destroyChildren()
	
	local player = g_game.getLocalPlayer()
	local diffrent = math.max(0, self.data.level - player:getLevel())
	m_CraftingFunctions.updateTime((self.data.duration * 60) + (diffrent * diffrent * 60))
	
	m_CraftingFunctions.updateChance(self.data.level)
	local crafting = m_CraftingFunctions.data[m_CraftingList.currentId]
	if crafting then
		crafting = m_CraftingFunctions.data[m_CraftingList.currentId].crafting
	end
	
	if crafting then
		for i = InventorySlotClover, InventorySlotBellows do
			local widget = m_CraftingList.description:getChildById(i)
			widget:getChildById('slot'):setDraggable(false)
			widget:getChildById('slot'):setVirtual(true)
			widget:getChildById('visibleMask'):show()
			widget:getChildById('visibleMask'):setPhantom(false)
		end
	end
	
	local list = m_CraftingList.creatureProducts[m_CraftingList.currentId]
	for i = 1, 6 do
		local cid = nil
		if list then
			cid = list[i]
		end
		
		for j = 1, #self.data.itemList do
			local v = self.data.itemList[j]
			if i < 6 then
				local slotId = 15 + i + (j * 13)
				local widget = g_ui.createWidget('ItemCrafting', m_CraftingList.itemList)
				widget:setParent(m_CraftingList.itemList)
				widget:setId(slotId)
				
				local slot = widget:getChildById('slot')
				slot:setItemId(v.id)
				slot.itemId = v.id
				slot.classId = i
				slot.ignoreSmelt = true
				
				local visibleMask = widget:getChildById('visibleMask')
				local corner = slot:getChildById('corner')
				corner:setImageClip(modules.game_containers.getImageClipByClassId(i))
				corner:show()
				
				slot.position = {x = 65534, y = slotId, z = m_CraftingList.currentId}
				local found = false
				if cid then
					for _, pid in pairs(cid) do
						if pid:getId() == v.id then
							pid:setPosition(slot.position)
							slot:setItemId(nil)
							slot:setItem(pid)
							found = true
							
							if crafting and visibleMask then
								slot:setDraggable(false)
								slot:setVirtual(true)
								visibleMask:show()
								visibleMask:setPhantom(false)
							end
							
							break
						end
					end
				end
				
				slot.amount = found
				slot:setOn(not found)
			else
				local widget = g_ui.createWidget('LabelCrafting', m_CraftingList.itemList)
				widget:setParent(m_CraftingList.itemList)
				widget:setId(j)
				widget.amount = v.amount
				
				m_CraftingFunctions.updateLabel(widget, j)
			end
		end
	end
	
	m_CraftingFunctions.updateQuality()
	m_CraftingFunctions.updateCost(crafting)
	-- m_CraftingList.rent:setEnabled(not self:getChildById('item'):isOn())
	m_CraftingList.itemList:setWidth((#self.data.itemList * 38) + ((#self.data.itemList - 1) * 6) + 24)
end

m_CraftingFunctions.rent = function()
	if not m_CraftingList.currentItem then
		return true
	end
	
	local msg = OutputMessage.create()
	msg:addU8(m_CraftingFunctions.GameServerSendCraft)
	msg:addU8(0)
	msg:addU16(m_CraftingList.currentItem.id)
	msg:addU8(m_CraftingList.currentId)
	m_CraftingFunctions.protocol:send(msg)
end

m_CraftingFunctions.cancel = function()
	m_CraftingFunctions.send(1)
	m_CraftingList.smith:setEnabled(false)
	
	local list = m_CraftingList.creatureProducts[m_CraftingList.currentId]
	if not list then
		return true
	end
	
	for k, v in pairs(list) do
		for i = 1, #v do
			local slot = v[i]:getPosition().y
			local widget = m_CraftingList.itemList:getChildById(slot)
			
			local product = widget:getChildById('slot')
			product:setItem(nil)
			product:setItemId(product.itemId)
			
			product.amount = item and true or false
			product:setOn(true)
			
			local id = math.floor((slot - 15) / 13)
			m_CraftingFunctions.updateLabel(m_CraftingList.itemList:getChildById(id), id)
		end
	end
	
	m_CraftingList.creatureProducts[m_CraftingList.currentId] = {}
end

m_CraftingFunctions.smith = function()
	m_CraftingFunctions.send(4)
	m_CraftingFunctions.disabled(false)
end

m_CraftingFunctions.smelt = function()
	m_CraftingFunctions.send(8)
	m_CraftingFunctions.disabled(true)
end

m_CraftingFunctions.collect = function()
	m_CraftingFunctions.send(5)
end

m_CraftingFunctions.collectSmelt = function()
	m_CraftingFunctions.send(10)
end

m_CraftingFunctions.send = function(id)
	local msg = OutputMessage.create()
	msg:addU8(m_CraftingFunctions.GameServerSendCraft)
	msg:addU8(id)
	msg:addU8(m_CraftingList.currentId)
	m_CraftingFunctions.protocol:send(msg)
end

m_CraftingFunctions.disabled = function(smelting)
	if smelting then
		m_CraftingList.smelt:setEnabled(false)
		m_CraftingList.collect:setEnabled(false)
	else
		m_CraftingList.cancel:setEnabled(false)
		m_CraftingList.rent:setEnabled(false)
		m_CraftingList.smith:setEnabled(false)
		m_CraftingList.collect:hide()
		m_CraftingList.cost:hide()
		
		m_CraftingList.resultPanel:clearText()
	end
end

m_CraftingFunctions.updateSmeltTime = function()
	local data = m_CraftingList.smeltingData[m_CraftingList.currentId]
	local item = data and data.item
	if not item then
		m_CraftingList.timePanel:clearText()
		return true
	end
	
	local player = g_game.getLocalPlayer()
	local data = m_CraftingFunctions.getItemDescription(item:getId())
	local diffrent = math.max(0, data.level - player:getLevel())
	m_CraftingFunctions.updateTime((data.duration * 2) + (diffrent * diffrent * 2), true)
end

m_CraftingFunctions.updateSmeltItem = function(item)
	local corner = m_CraftingList.item:getChildById('corner')
	m_CraftingList.itemList:destroyChildren()
	if item then
		local classId = item:getClassId()
		local data = m_CraftingList.smeltingData[m_CraftingList.currentId]
		if not data.smelting then
			m_CraftingList.chance:show()
			
			local chance = 0
			if classId == ITEMCLASS_DESOLATE then
				chance = 1
			elseif classId == ITEMCLASS_DAMAGED then
				chance = 5
			elseif classId == ITEMCLASS_NORMAL then
				chance = 10
			elseif classId == ITEMCLASS_PERFECT then
				chance = 25
			elseif classId == ITEMCLASS_LEGENDARY then
				chance = 100
			elseif classId == ITEMCLASS_UNIQUE then
				chance = 0
			end
			
			m_CraftingList.chance:setText(chance .. '% ' .. m_CraftingFunctions.text[5])
		else
			m_CraftingList.chance:hide()
		end
		
		item:setPosition({x = 65533, y = InventorySlotSmelting, z = m_CraftingList.currentId})
		m_CraftingList.item:setItem(item)
		corner:setImageClip(modules.game_containers.getImageClipByClassId(classId))
		corner:show()
		
		local player = g_game.getLocalPlayer()
		local var = m_CraftingFunctions.getItemDescription(item:getId())
		local diffrent = math.max(0, var.level - player:getLevel())
		m_CraftingFunctions.updateTime((var.duration * 2) + (diffrent * diffrent * 2), true)
		
		m_CraftingList.item:setDraggable(not data.smelting)
		m_CraftingList.item:setVirtual(data.smelting)
		
		for i = 1, 5 do
			local cid = nil
			if data.items then
				cid = data.items[i]
			end
			
			for j = 1, #var.itemList do
				local v = var.itemList[j]
				if i < 6 then
					local slotId = 15 + i + (j * 13)
					local widget = g_ui.createWidget('ItemCrafting', m_CraftingList.itemList)
					widget:setParent(m_CraftingList.itemList)
					widget:setId(slotId)
					
					local slot = widget:getChildById('slot')
					slot:setItemId(v.id)
					slot.itemId = v.id
					slot.classId = i
					
					local visibleMask = widget:getChildById('visibleMask')
					local corner = slot:getChildById('corner')
					corner:setImageClip(modules.game_containers.getImageClipByClassId(i))
					corner:show()
					
					slot:setDraggable(false)
					slot:setVirtual(true)
					slot.position = {x = 65533, y = slotId, z = m_CraftingList.currentId}
					local found = false
					if cid then
						for _, pid in pairs(cid) do
							if pid:getId() == v.id then
								pid:setPosition(slot.position)
								slot:setItemId(nil)
								slot:setItem(pid)
								found = true
								
								if crafting and visibleMask then
									slot:setDraggable(false)
									slot:setVirtual(true)
									visibleMask:show()
									visibleMask:setPhantom(false)
								end
								
								break
							end
						end
					end
					
					slot.amount = found
					slot:setOn(not found)
				end
			end
		end
		
		m_CraftingList.itemList:setWidth((#var.itemList * 38) + ((#var.itemList - 1) * 6) + 24)
	else
		m_CraftingList.chance:hide()
		m_CraftingList.item:setItem(nil)
		
		m_CraftingList.item:setDraggable(true)
		m_CraftingList.item:setVirtual(false)
		
		corner:hide()
		m_CraftingList.timePanel:clearText()
	end
end

m_CraftingFunctions.updateItem = function()
	local data = m_CraftingList.smeltingData[m_CraftingList.currentId]
	if not data then
		return false
	end
	
	local enabled = not m_CraftingFunctions.automaticallySendToMagazine or m_CraftingList.timePanel.time ~= 0
	m_CraftingFunctions.updateSmeltItem(enabled and m_CraftingList.smeltingData[m_CraftingList.currentId].item)
	
	local slot = m_CraftingList.hammer:getChildById('slot')
	slot.position = {x = 65533, y = InventorySlotSmeltingHammer, z = m_CraftingList.currentId}
	if enabled and data.additives then
		data.additives:setPosition({x = 65533, y = InventorySlotSmeltingHammer, z = m_CraftingList.currentId})
		slot:setItem(data.additives)
	else
		slot:setItem(nil)
	end
	
	m_CraftingList.scroll.position = {x = 65533, y = InventorySlotSmeltingHammer, z = m_CraftingList.currentId}
	if enabled and data.scroll then
		data.scroll:setPosition({x = 65533, y = InventorySlotScroll, z = m_CraftingList.currentId})
		m_CraftingList.scroll:show()
		m_CraftingList.scroll:setItem(data.scroll)
	else
		m_CraftingList.scroll:hide()
		m_CraftingList.scroll:setItem(nil)
	end
	
	m_CraftingFunctions.updateSmeltTime()
	
	slot:setDraggable(not data.smelting)
	slot:setVirtual(data.smelting)
	if data.smelting then
		m_CraftingList.hammer:getChildById('visibleMask'):show()
		m_CraftingList.hammer:getChildById('visibleMask'):setPhantom(false)
	else
		m_CraftingList.hammer:getChildById('visibleMask'):hide()
	end
end

m_CraftingFunctions.select = function(self, ignore)
	if m_CraftingList.current then
		if m_CraftingList.current == self then
			return true
		end
	
		m_CraftingList.current:setOn(false)
	end
	
	if m_CraftingList.event then
		m_CraftingList.event:cancel()
		m_CraftingList.event = nil
	end
	
	m_CraftingList.item:setItem(nil)
	m_CraftingList.item:getChildById('corner'):hide()
	m_CraftingList.item:setDraggable(true)
	m_CraftingList.item:setVirtual(false)
	
	m_CraftingList.current = self
	m_CraftingList.currentId = tonumber(m_CraftingList.current:getId())
	self:setOn(true)
	
	if self:isChecked() then
		m_CraftingList.item.position = {x = 65533, y = InventorySlotSmelting, z = m_CraftingList.currentId}
		m_CraftingFunctions.disabled(true)
		m_CraftingFunctions.updateItem()
		m_CraftingFunctions.send(7)
	else
		m_CraftingList.resultPanel:clearText()
	
		m_CraftingList.blocking = false
		m_CraftingList.label = {}
		m_CraftingFunctions.disabled(false)
		m_CraftingList.qualityPanel:destroyChildren()
		m_CraftingList.item.position = {x = 65534, y = InventorySlotReward, z = m_CraftingList.currentId}
	
		for _, pid in pairs(m_CraftingList.itemList:getChildren()) do
			local slot = pid:getChildById('slot')
			if slot then
				slot:setItem(nil)
				slot:setDraggable(false)
				slot:setVirtual(true)
				pid:getChildById('visibleMask'):hide()
				slot.position.z = m_CraftingList.currentId
			else
				pid:setText('0/' .. pid.amount)
			end
		end
		
		m_CraftingFunctions.selectItem(m_CraftingList.list:getChildByIndex(1))
		if not ignore then
			m_CraftingFunctions.send(2)
		end
	end
end

m_CraftingFunctions.runEvent = function()
	m_CraftingFunctions.updateTime()
	
	if m_CraftingList.timePanel.time <= 0 then
		if not m_CraftingList.smelting then
			-- m_CraftingFunctions.send(6)
		end
		
		local id = m_CraftingFunctions.data[m_CraftingList.currentId]
		if m_CraftingList.event and (m_CraftingList.smelting or (id and id.class)) then
			m_CraftingList.event:cancel()
			m_CraftingList.event = nil
		end
		
		if m_CraftingFunctions.automaticallySendToMagazine then
			m_CraftingFunctions.sendAutomatically(m_CraftingFunctions.automaticallySendToMagazine)
		elseif m_CraftingList.smelting then
			m_CraftingList.collect:setEnabled(true)
		end
		
		return true
	end
	
	m_CraftingList.timePanel.time = m_CraftingList.timePanel.time - 1
	if not m_CraftingList.event then
		m_CraftingList.event = cycleEvent(m_CraftingFunctions.runEvent, 1000)
	end
end

m_CraftingFunctions.getItemDescription = function(id)
	for _, v in pairs(m_CraftingFunctions.list) do
		if v.id == id then
			return v
		end
	end
end

m_CraftingFunctions.selectCraftItem = function(itemId)
	for k, v in pairs(m_CraftingFunctions.itemList) do
		for i = 1, #v do
			if v[i][1] == itemId then
				m_CraftingList.comboBox:setCurrentOption(tr(ITEM_CATEGORY[k]), false, true)
				return true
			end
		end
	end
end

m_CraftingFunctions.updateBalance = function(balance)
	m_CraftingFunctions.balance = g_game.getMilharNumber(balance)
	if m_CraftingList.balance then
		m_CraftingList.balance:setText(m_CraftingFunctions.balance)
	end
end

m_CraftingFunctions.updateCost = function(crafting)
	if crafting then
		m_CraftingList.cost:hide()
	else
		m_CraftingList.cost:show()
		m_CraftingList.cost:setText(g_game.getMilharNumber(m_CraftingList.currentItem.data.price))
	end
end

function parseUpdateCraft(protocol, msg)
	local id = msg:getU8()
	if id == 0 then
		if not m_CraftingList.window then
			modules.game_rewards.m_RewardsFunctions.create(true)
			modules.game_rewards.m_RewardsFunctions.instantSelect(3)
		end
		
		local size = msg:getU16()
		for i = 1, size do
			local id = msg:getU16()
			local name = msg:getString()
			for i = 1, #m_CraftingFunctions.list do
				local var = m_CraftingFunctions.list[i]
				if id == var.id then
					local type = var.type
					if not m_CraftingFunctions.itemList[type] then
						m_CraftingFunctions.itemList[type] = {}
					end
					
					local found = false
					for _, v in pairs(m_CraftingFunctions.itemList[type]) do
						if v[1] == id then
							found = true
							break
						end
					end
					
					if not found then
						table.insert(m_CraftingFunctions.itemList[type], {id, name, var})
					end
					
					break
				end
			end
		end
		
		if not m_CraftingList.smelting then
			m_CraftingList.comboBox:setCurrentOption(tr(ITEM_CATEGORY[ITEMTYPE_ALL]))
		end
	elseif id == 1 then
		local slot = msg:getU8()
		local data = {}
		data.itemId = msg:getU16()
		data.crafting = false
		data.timeEnd = 0
		
		local currentWindow = not m_CraftingList.smelting and slot == m_CraftingList.currentId
		if data.itemId ~= 0 then
			data.crafting = msg:getU8() == 1
			if data.crafting then
				data.timeEnd = msg:getU32()
				
				if currentWindow then
					m_CraftingList.timePanel.time = data.timeEnd
					m_CraftingFunctions.runEvent()
				end
				
				if data.timeEnd == 0 then
					data.success = msg:getU8() == 1
					if data.success then
						data.item = m_CraftingFunctions.protocol:getItem(msg, 0, true)
						data.item:setPosition({x = 65534, y = 28, z = m_CraftingList.currentId})
						if currentWindow then
							m_CraftingList.resultPanel:setText(m_CraftingFunctions.text[1])
							m_CraftingList.resultPanel:setColor(m_CraftingFunctions.color[1])
							m_CraftingList.item:setItem(data.item)
							local corner = m_CraftingList.item:getChildById('corner')
							corner:setImageClip(modules.game_containers.getImageClipByClassId(data.item:getClassId()))
							corner:show()
						end
					elseif currentWindow then
						m_CraftingList.collect:show()
						m_CraftingList.resultPanel:setText(m_CraftingFunctions.text[2])
						m_CraftingList.resultPanel:setColor(m_CraftingFunctions.color[2])
					end
				end
			end
		end
		
		if not m_CraftingList.creatureProducts then
			m_CraftingList.creatureProducts = {}
		end
		
		m_CraftingList.creatureProducts[slot] = {}
		for j = 1, 5 do
			local itemSize = msg:getU16()
			for k = 1, itemSize do
				local item = m_CraftingFunctions.protocol:getItem(msg, 0, true)
				
				local class = item:getClassId()
				if not m_CraftingList.creatureProducts[slot][class] then
					m_CraftingList.creatureProducts[slot][class] = {}
				end
				
				table.insert(m_CraftingList.creatureProducts[slot][class], item)
			end
		end
		
		for i = InventorySlotClover, InventorySlotBellows do
			if msg:getU8() == 1 then
				local item = m_CraftingFunctions.protocol:getItem(msg, 0, false)
				if not m_CraftingList.craftingAdditives[slot] then
					m_CraftingList.craftingAdditives[slot] = {}
				end
				
				m_CraftingList.craftingAdditives[slot][i] = item
			end
		end
		
		m_CraftingFunctions.data[slot] = data
		
		if currentWindow then
			m_CraftingList.blocking = false
			m_CraftingList.cancel:setEnabled(data.itemId > 1)
			m_CraftingList.rent:setEnabled(data.itemId == 0)
			
			if data.itemId ~= 0 then
				m_CraftingList.cost:hide()
				m_CraftingList.requiredId = data.itemId
				m_CraftingFunctions.selectCraftItem(data.itemId)
				m_CraftingList.requiredId = nil
				m_CraftingList.blocking = true
			else
				-- m_CraftingFunctions.updateCost()
			end
		end
	elseif id == 2 then
		local slot = msg:getU8()
		local timeEnd = msg:getU32()
		
		m_CraftingFunctions.data[slot].crafting = true
		m_CraftingFunctions.data[slot].timeEnd = timeEnd
		if not m_CraftingList.smelting and slot == m_CraftingList.currentId then
			m_CraftingList.timePanel.time = timeEnd
			m_CraftingFunctions.runEvent()
			
			for _, pid in pairs(m_CraftingList.itemList:getChildren()) do
				local slot = pid:getChildById('slot')
				if slot and slot:getItem() then
					local widget = pid:getChildById('visibleMask')
					if widget then
						slot:setDraggable(false)
						slot:setVirtual(true)
						widget:show()
					end
				end
			end
			
			for i = InventorySlotClover, InventorySlotBellows do
				local widget = m_CraftingList.description:getChildById(i)
				widget:getChildById('slot'):setDraggable(false)
				widget:getChildById('slot'):setVirtual(true)
				widget:getChildById('visibleMask'):show()
				widget:getChildById('visibleMask'):setPhantom(false)
			end
		end
	elseif id == 3 then
		local slot = msg:getU8()
		if not m_CraftingList.smelting and slot == m_CraftingList.currentId then
			for _, pid in pairs(m_CraftingList.itemList:getChildren()) do
				local widget = pid:getChildById('slot')
				if widget then
					widget:setItem(nil)
					widget:setDraggable(true)
					widget:setVirtual(false)
					pid:getChildById('visibleMask'):hide()
				else
					pid:setText('0/' .. pid.amount)
				end
			end
			
			for i = InventorySlotClover, InventorySlotBellows do
				local widget = m_CraftingList.description:getChildById(i)
				widget:getChildById('slot'):setDraggable(true)
				widget:getChildById('slot'):setVirtual(false)
				widget:getChildById('visibleMask'):hide()
			end
			
			m_CraftingList.qualityPanel:destroyChildren()
			
			m_CraftingList.item:setItemId(nil)
			m_CraftingList.item:getChildById('corner'):hide()
			
			m_CraftingList.blocking = false
			m_CraftingFunctions.disabled(false)
			m_CraftingList.rent:setEnabled(true)
			-- m_CraftingFunctions.updateCost()
		end
		
		m_CraftingList.creatureProducts[slot] = {}
		m_CraftingFunctions.data[slot] = {}
	elseif id == 4 then
		local slot = msg:getU8()
		
		m_CraftingFunctions.data[slot].timeEnd = 0
		local success = msg:getU8() == 1
		if success then
			m_CraftingList.resultPanel:setText(m_CraftingFunctions.text[1])
			m_CraftingList.resultPanel:setColor(m_CraftingFunctions.color[1])
			m_CraftingFunctions.data[slot].item = m_CraftingFunctions.protocol:getItem(msg, 0, true)
			
			m_CraftingFunctions.data[slot].item:setPosition({x = 65534, y = 28, z = m_CraftingList.currentId})
			
			if not m_CraftingList.smelting and slot == m_CraftingList.currentId then
				m_CraftingList.item:setItem(m_CraftingFunctions.data[slot].item)
				
				local corner = m_CraftingList.item:getChildById('corner')
				corner:setImageClip(modules.game_containers.getImageClipByClassId(m_CraftingFunctions.data[slot].item:getClassId()))
				corner:show()
			end
		elseif not m_CraftingList.smelting and slot == m_CraftingList.currentId then
			m_CraftingList.collect:show()
			m_CraftingList.resultPanel:setText(m_CraftingFunctions.text[2])
			m_CraftingList.resultPanel:setColor(m_CraftingFunctions.color[2])
		end
	elseif id == 5 then
		local slot = msg:getU8()
		local data = {}
		if msg:getU8() == 1 then
			data.item = m_CraftingFunctions.protocol:getItem(msg, 0, true)
			data.smelting = msg:getU8() == 1
			if data.smelting then
				if msg:getU8() == 1 then
					data.scroll = m_CraftingFunctions.protocol:getItem(msg, 0, true)
				end
				
				data.timeEnd = msg:getU32()
				data.items = {}
				
				for j = 1, 5 do
					local itemSize = msg:getU16()
					for k = 1, itemSize do
						local item = m_CraftingFunctions.protocol:getItem(msg, 0, true)
						
						local class = item:getClassId()
						if not data.items[class] then
							data.items[class] = {}
						end
						
						table.insert(data.items[class], item)
					end
				end
			end
		end
		
		if msg:getU8() == 1 then
			data.additives = m_CraftingFunctions.protocol:getItem(msg, 0, false)
			m_CraftingList.additivesMultipliers[slot][InventorySlotSmeltingHammer] = m_CraftingFunctions.additivesValue[data.additives:getId()]
		end
		
		if not m_CraftingList.smeltingData then
			m_CraftingList.smeltingData =  {}
		end
		
		m_CraftingList.smeltingData[slot] = data
		if m_CraftingList.smelting and slot == m_CraftingList.currentId then
			if data.smelting then
				m_CraftingList.smelt:setEnabled(false)
				m_CraftingList.collect:setEnabled(data.timeEnd == 0)
				
				m_CraftingList.timePanel.time = data.timeEnd
				m_CraftingFunctions.runEvent()
			else
				m_CraftingList.smelt:setEnabled(data.item)
			end
		
			m_CraftingFunctions.updateItem()
		end
	elseif id == 6 then
		local slot = msg:getU8()
		if m_CraftingList.smelting and slot == m_CraftingList.currentId then
			m_CraftingList.smelt:setEnabled(false)
			m_CraftingList.collect:setEnabled(true)
			m_CraftingList.timePanel:setText(m_CraftingFunctions.text[3] .. ' 0 : 00 : 00')
		end
	elseif id == 7 then
		local slot = msg:getU8()
		if m_CraftingList.smelting and slot == m_CraftingList.currentId then
			m_CraftingList.smelt:setEnabled(false)
			m_CraftingList.collect:setEnabled(false)
			m_CraftingList.itemList:destroyChildren()
			m_CraftingList.timePanel:clearText()
			m_CraftingList.scroll:setItem(nil)
			m_CraftingList.scroll:hide()
			m_CraftingList.item:setItem(nil)
			m_CraftingList.item:setDraggable(true)
			m_CraftingList.item:setVirtual(false)
			m_CraftingList.item.position = {x = 65533, y = InventorySlotSmelting, z = slot}
			
			m_CraftingList.smeltingData[slot] = {}
			
			local widget = m_CraftingList.description:getChildById(InventorySlotSmeltingHammer)
			local slot = widget:getChildById('slot')
			slot:setDraggable(true)
			slot:setVirtual(false)
			slot:setItem(nil)
			widget:getChildById('visibleMask'):hide()
		end
	elseif id == 8 then
		
	end
end

m_CraftingFunctions.sort = function(text, list)
	local list = list or m_CraftingList.list:getChildren()
	local text = text or m_CraftingList.searchItemName:getText()
	for _, v in pairs(list) do
		local name = v:getChildById('name')
		if not name:getText():lower():find(text:lower()) then
			v:hide()
		end
	end
end

m_CraftingFunctions.open = function(change, parent)
	m_CraftingList.currentId = 0
	
	if change then
		m_CraftingList.window = g_ui.createWidget('SmeltPanel', parent)
		m_CraftingList.description = m_CraftingList.window:getChildById('description')
		m_CraftingList.itemList = m_CraftingList.description:getChildById('list')
		m_CraftingList.item = m_CraftingList.description:getChildById('item')
		m_CraftingList.smelt = m_CraftingList.description:getChildById('smelt')
		m_CraftingList.collect = m_CraftingList.description:getChildById('collect')
		m_CraftingList.timePanel = m_CraftingList.description:getChildById('time')
		m_CraftingList.chance = m_CraftingList.description:getChildById('chance')
		m_CraftingList.scroll = m_CraftingList.description:getChildById(InventorySlotScroll)
		m_CraftingList.hammer = m_CraftingList.description:getChildById(InventorySlotSmeltingHammer)
		m_CraftingList.smelting = true
		m_CraftingList.item.ignoreSmelt = true
		m_CraftingList.item.position = {x = 65533, y = InventorySlotSmelting, z = m_CraftingList.currentId}
		m_CraftingList.smeltingData = {}
		m_CraftingList.hammer:getChildById('item'):setItemId(m_CraftingFunctions.additives[InventorySlotHammer])
		m_CraftingList.description:getChildById('sendAutomatically'):setChecked(m_CraftingFunctions.automaticallySendToMagazine)
	else
		m_CraftingList.window = g_ui.createWidget('CraftPanel', parent)
		m_CraftingList.list = m_CraftingList.window:getChildById('list')
		m_CraftingList.description = m_CraftingList.window:getChildById('description')
		m_CraftingList.itemList = m_CraftingList.description:getChildById('list')
		m_CraftingList.rent = m_CraftingList.description:getChildById('rent')
		m_CraftingList.cancel = m_CraftingList.description:getChildById('cancel')
		m_CraftingList.smith = m_CraftingList.description:getChildById('smith')
		m_CraftingList.collect = m_CraftingList.description:getChildById('collect')
		m_CraftingList.cost = m_CraftingList.description:getChildById('cost')
		m_CraftingList.balance = m_CraftingList.description:getChildById('balance')
		m_CraftingList.qualityPanel = m_CraftingList.description:getChildById('quality')
		m_CraftingList.timePanel = m_CraftingList.description:getChildById('time')
		m_CraftingList.chancePanel = m_CraftingList.description:getChildById('chance')
		m_CraftingList.resultPanel = m_CraftingList.description:getChildById('result')
		m_CraftingList.item = m_CraftingList.description:getChildById('item')
		m_CraftingList.item.ignoreSmelt = true
		m_CraftingList.itemClasses = {}
		m_CraftingList.creatureProducts = {}
		m_CraftingList.craftingAdditives = {}
		m_CraftingList.smelting = false
		
		if m_CraftingFunctions.balance then
			m_CraftingList.balance:setText(m_CraftingFunctions.balance)
		end
		
		m_CraftingList.comboBox = m_CraftingList.window:getChildById('comboBox')
		for i = 1, #m_CraftingFunctions.itemTypeList do
			local name = tr(ITEM_CATEGORY[m_CraftingFunctions.itemTypeList[i]])
			m_CraftingFunctions.tr[name] = m_CraftingFunctions.itemTypeList[i]
			m_CraftingList.comboBox:addOption(name)
		end
		connect(m_CraftingList.comboBox, { onOptionChange = onOptionChange })
		
		m_CraftingList.searchItemName = m_CraftingList.window:getChildById('searchItemName')
		m_CraftingList.searchItemName.onTextChange = function(self, text, oldText)
			local list = m_CraftingList.list:getChildren()
			if not text:find(oldText) then
				for _, v in pairs(list) do
					local name = v:getChildById('name')
					if name:getText():lower():find(text:lower()) then
						v:show()
					end
				end
			end
			
			m_CraftingFunctions.sort(text, list)
		end
		
		for i = InventorySlotClover, InventorySlotBellows do
			local widget = m_CraftingList.description:getChildById(i)
			if widget then
				widget:getChildById('item'):setItemId(m_CraftingFunctions.additives[i])
			end
		end
		
		local msg = OutputMessage.create()
		msg:addU8(m_CraftingFunctions.GameServerSendCraft)
		msg:addU8(3)
		m_CraftingFunctions.protocol:send(msg)
	end
	
	m_CraftingList.buttons = {}
	m_CraftingList.additivesMultipliers = {}
	for i = 0, 2 do
		m_CraftingList.additivesMultipliers[i] = {}
		m_CraftingList.buttons[i] = m_CraftingList.window:getChildById(i)
		
		if i == 2 then
			local premium = g_game.getLocalPlayer():isPremium()
			if premium then
				m_CraftingList.window:getChildById('effectPanelMask'):hide()
			else
				m_CraftingList.window:getChildById('effectPanelMask'):show()
			end
		end
	end
	
	m_CraftingFunctions.select(m_CraftingList.buttons[0], true)
end

m_CraftingFunctions.onLoad = function()
	m_CraftingFunctions.itemList = {}
	m_CraftingFunctions.slots = {}
	m_CraftingFunctions.data = {}
	ProtocolGame.registerOpcode(m_CraftingFunctions.GameServerUpdateCraft, parseUpdateCraft)
	if g_game.isOnline() then
		m_CraftingFunctions.onGameStart()
		
		local player = g_game.getLocalPlayer()
		for i = InventorySlotClover, InventorySlotBellows do
			if g_game.isOnline() then
				m_CraftingFunctions.onInventoryChange(player, i, player:getInventoryItem(i))
			else
				m_CraftingFunctions.onInventoryChange(player, i, nil)
			end
		end
		
		if g_game.isOnline() then
			m_CraftingFunctions.onInventoryChange(player, InventorySlotSmelting, player:getInventoryItem(InventorySlotSmelting))
			m_CraftingFunctions.onInventoryChange(player, InventorySlotReward, player:getInventoryItem(InventorySlotReward))
			m_CraftingFunctions.onInventoryChange(player, InventorySlotScroll, player:getInventoryItem(InventorySlotScroll))
		else
			m_CraftingFunctions.onInventoryChange(player, InventorySlotSmelting, nil)
			m_CraftingFunctions.onInventoryChange(player, InventorySlotReward, nil)
			m_CraftingFunctions.onInventoryChange(player, InventorySlotScroll, nil)
		end
	end
end

m_CraftingFunctions.onUnload = function()
	ProtocolGame.unregisterOpcode(m_CraftingFunctions.GameServerUpdateCraft, parseUpdateCraft)
	m_CraftingFunctions.onGameEnd()
	
	modules.client_options.setOption('automaticallySendToMagazine', m_CraftingFunctions.automaticallySendToMagazine, true)
end

m_CraftingFunctions.onGameStart = function()
	m_CraftingFunctions.protocol = g_game.getProtocolGame()
	m_CraftingFunctions.sendAutomatically(m_CraftingFunctions.automaticallySendToMagazine)
end

m_CraftingFunctions.onGameEnd = function()
	m_CraftingFunctions.itemList = {}
	
	m_CraftingFunctions.protocol = nil
	m_CraftingFunctions.destroy()
end

m_CraftingFunctions.onInventoryChange = function(player, slot, item, oldItem, z, favorite)
	if not m_CraftingList.window then
		return true
	end
	
	if not m_CraftingList.smeltingData then
		m_CraftingList.smeltingData = {}
	end
	
	if not m_CraftingList.smeltingData[z] then
		m_CraftingList.smeltingData[z] = {}
	end
	
	z = z or 0
	if slot == InventorySlotScroll then
		m_CraftingList.smeltingData[z].scroll = item
		if item and m_CraftingList.smelting and z == m_CraftingList.currentId and m_CraftingList.scroll then
			item:setPosition({x = 65533, y = InventorySlotScroll, z = z})
			m_CraftingList.scroll:setItem(item)
		end
	elseif slot == InventorySlotSmeltingHammer then
		m_CraftingList.smeltingData[z].additives = item
		if item then
			m_CraftingList.additivesMultipliers[z][slot] = m_CraftingFunctions.additivesValue[item:getId()]
		else
			m_CraftingList.additivesMultipliers[z][slot] = 1
		end
		
		if m_CraftingList.smelting and z == m_CraftingList.currentId then
			local widget = m_CraftingList.description:getChildById(slot)
			if not widget then
				return true
			end
			
			local widget = widget:getChildById('slot')
			if item then
				item:setPosition(widget.position)
				widget:setItem(item)
			else
				widget:setItem(nil)
			end
			
			m_CraftingFunctions.updateSmeltTime()
		end
		
		return true
	elseif slot >= InventorySlotSmelting and slot <= InventorySlotReward then
		if m_CraftingList.smelting and z == m_CraftingList.currentId then
			if ((slot == InventorySlotReward and not m_CraftingList.smelting) or m_CraftingList.smelting) then
				local corner = m_CraftingList.item:getChildById('corner')
				if item then
					m_CraftingList.item:setItem(item)
					
					corner:setImageClip(modules.game_containers.getImageClipByClassId(item:getClassId()))
					corner:show()
				else
					m_CraftingList.item:setItem(nil)
					corner:hide()
				end
				
				if slot == InventorySlotSmelting then
					m_CraftingList.smelt:setEnabled(item)
				end
			end
			
			if slot == InventorySlotSmelting then
				m_CraftingList.smeltingData[z].item = item
				m_CraftingFunctions.updateSmeltItem(item)
			end
		end
		
		return true
	elseif slot >= InventorySlotClover and slot <= InventorySlotBellows then
		if not m_CraftingList.craftingAdditives[z] then
			m_CraftingList.craftingAdditives[z] = {}
		end
		
		m_CraftingList.craftingAdditives[z][slot] = item
		
		if not m_CraftingList.smelting and z == m_CraftingList.currentId then
			local widget = m_CraftingList.description:getChildById(slot)
			if not widget then
				return true
			end
			
			local widget = widget:getChildById('slot')
			if item then
				item:setPosition(widget.position)
				widget:setItem(item)
			else
				widget:setItem(nil)
			end
			
			m_CraftingFunctions.updateAdditives(slot, item)
		end
		
		return true
	end
	
	if not m_CraftingList.itemList or m_CraftingList.smelting then
		return true
	end
	
	local var = m_CraftingFunctions.data[m_CraftingList.currentId]
	if var then
		if var.itemId == 0 or var.crafting then
			return true
		end
	end
	
	local widget = m_CraftingList.itemList:getChildById(slot)
	if not widget then
		return true
	end
	
	local class = (slot - 15) % 13
	local list = m_CraftingList.creatureProducts[m_CraftingList.currentId][class]
	if not list then
		list = {}
	end
	
	if not item and oldItem then
		for i = #list, 1, -1 do
			if oldItem:getId() == list[i]:getId() then
				table.remove(list, i)
				break
			end
		end
	elseif item then
		local found = false
		for i = #list, 1, -1 do
			if item:getId() == list[i]:getId() then
				list[i] = item
				found = true
				break
			end
		end
		
		if not found then
			table.insert(list, item)
		end
	end
	
	-- if z == m_CraftingList.currentId then
		local product = widget:getChildById('slot')
		if item then
			item:setPosition(product.position)
			product:setItem(item)
		else
			product:setItem(nil)
			product:setItemId(product.itemId)
		end
		
		product.amount = item and true or false
		product:setOn(not item)
		
		local id = math.floor((slot - 15) / 13)
		local label = m_CraftingList.itemList:getChildById(id)
		m_CraftingList.creatureProducts[m_CraftingList.currentId][class] = list
		m_CraftingFunctions.updateLabel(label, id)
		m_CraftingFunctions.updateQuality()
	-- end
end

m_CraftingFunctions.destroy = function()
	if m_CraftingList.window then
		m_CraftingList.window:destroy()
	end
	
	if m_CraftingList.hover then
		m_CraftingList.hover:destroy()
	end
	
	if m_CraftingList.event then
		m_CraftingList.event:cancel()
	end
	
	m_CraftingList = {}
end

m_CraftingFunctions.sendAutomatically = function(checked)
	m_CraftingFunctions.automaticallySendToMagazine = checked
	
	if m_CraftingFunctions.protocol and g_game.isOnline() then
		local msg = OutputMessage.create()
		msg:addU8(m_CraftingFunctions.GameServerSendCraft)
		msg:addU8(13)
		msg:addU8(checked and 1 or 0)
		m_CraftingFunctions.protocol:send(msg)
	end
end

m_CraftingFunctions.onMouseRelease = function(self, mousePosition, mouseButton)
	if mouseButton == MouseRightButton then
		local menu = g_ui.createWidget('PopupMenu')
		menu:setGameMenu(true)
		menu:addOption(tr('Goto item list'), function() modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(self:getItem():getId(), self:getItem():getCategoryId()) end)
		menu:display(menuPosition)
	else
		modules.game_encyclopedia.m_ItemlistFunctions.openItemListByBestiary(self:getItem():getId(), self:getItem():getCategoryId())
	end
end

m_CraftingFunctions.addItem = function(v, i)
	local language = g_settings.get('locale', 'en')
	if language == 'en' then
		language = LANGUAGE_ENGLISH
	else
		language = LANGUAGE_POLISH
	end
	
	local widget = g_ui.createWidget('ItemPanelCrafting', m_CraftingList.list)
	widget:setParent(m_CraftingList.list)
	widget:setId(v[1])
	
	local it = modules.game_lookat.m_LookAtFunctions.getItemByClientId(v[1])
	local tmpWidget = widget:getChildById('item')
	tmpWidget:setItemId(v[1])
	tmpWidget:setOn(i == -1)
	tmpWidget:getItem():setCategoryId(it.itemType)
	tmpWidget.onMouseRelease = m_CraftingFunctions.onMouseRelease
	
	local tmpWidget = widget:getChildById('name')
	tmpWidget:setText(it.name[language])
	tmpWidget:setOn(i == -1)
	widget.data = v[3]
	widget.id = v[1]
	
	if m_CraftingList.requiredId then
		if v[1] == m_CraftingList.requiredId then
			m_CraftingFunctions.selectItem(widget)
		end
	elseif i == 1 then
		m_CraftingFunctions.selectItem(widget)
	end
end

function onOptionChange(comboBox, text, data)
	if not m_CraftingList.requiredId and (text == m_CraftingList.currentOption or m_CraftingList.blocking) then
		m_CraftingList.comboBox:setCurrentOption(m_CraftingList.currentOption, true)
		return false
	end
	
	m_CraftingList.currentItem = nil
	m_CraftingList.currentOption = text
	m_CraftingList.list:destroyChildren()
	
	local id = m_CraftingFunctions.tr[text]
	if id == ITEMTYPE_ALL then
		for k, v in pairs(m_CraftingFunctions.itemList) do
			for i = 1, #v do
				m_CraftingFunctions.addItem(v[i], i)
			end
		end
		
		for i = 1, #m_CraftingFunctions.list do
			local v = m_CraftingFunctions.list[i]
			local var = m_CraftingFunctions.itemList[v.type]
			
			local add = true
			if var and #var > 0 then
				for j = 1, #var do
					if var[j][1] == v.id then
						add = false
						break
					end
				end
			end
			
			if add then
				m_CraftingFunctions.addItem({v.id, v.name, v}, -1)
			end
		end
	else
		local var = m_CraftingFunctions.itemList[id]
		if var and #var > 0 then
			for i = 1, #var do
				m_CraftingFunctions.addItem(var[i], i)
			end
		end
		
		for i = 1, #m_CraftingFunctions.list do
			local v = m_CraftingFunctions.list[i]
			if v.type == id then
				local add = true
				if var and #var > 0 then
					for j = 1, #var do
						if var[j][1] == v.id then
							add = false
							break
						end
					end
				end
			
				if add then
					m_CraftingFunctions.addItem({v.id, v.name, v}, -1)
				end
			end
		end
	end
	
	m_CraftingFunctions.sort()
end
