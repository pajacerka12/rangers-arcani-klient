m_CraftingFunctions.list = {
			-- Steel
			{id = 5892, name = 'Steel', level = 0, duration = 1, type = ITEMTYPE_CREATURE_PRODUCT, price = 45,
				itemList = {{id = 5880, amount = 5}, -- Iron ore
							{id = 13253, amount = 10}, -- Coal
							}
				},
			-- Royal steel
			{id = 5887, name = 'Royal steel', level = 0, duration = 2, type = ITEMTYPE_CREATURE_PRODUCT, price = 145,
				itemList = {{id = 5880, amount = 10}, -- Iron ore
							{id = 13253, amount = 10}, -- Coal
							{id = 3026, amount = 3}, -- White pearl
							}
				},
			-- Gold ignot
			{id = 9058, name = 'Gold ignot', level = 0, duration = 5, type = ITEMTYPE_CREATURE_PRODUCT, price = 270,
				itemList = {{id = 3040, amount = 5}, -- Gold nugget
							{id = 13253, amount = 10}, -- Coal
							}
				},
			-- Hardener leather
			{id = 5878, name = 'Hardener leather', level = 0, duration = 2, type = ITEMTYPE_CREATURE_PRODUCT, price = 23,
				itemList = {{id = 12233, amount = 10}, -- Untreated leather
							{id = 12832, amount = 2}, -- Rope belt
							{id = 12784, amount = 10}, -- Beastly claw
							}
				},
			-- Radiating powder
			{id = 17019, name = 'Radiating powder', level = 0, duration = 3, type = ITEMTYPE_CREATURE_PRODUCT, price = 47,
				itemList = {{id = 5925, amount = 10}, -- Hardned bone
							{id = 12784, amount = 10}, -- Beastly claw
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Fine fabric
			{id = 10772, name = 'Fine fabric', level = 0, duration = 2, type = ITEMTYPE_CREATURE_PRODUCT, price = 45,
				itemList = {{id = 12233, amount = 12}, -- Untreated leather
							{id = 12832, amount = 5}, -- Rope belt
							{id = 3026, amount = 1}, -- White pearl
							}
				},
			-- Pure mithril
			{id = 17018, name = 'Pure mithril', level = 0, duration = 8, type = ITEMTYPE_CREATURE_PRODUCT, price = 615,
				itemList = {{id = 10771, amount = 5}, -- Mithril ore
							{id = 13253, amount = 20}, -- Coal
							{id = 5809, amount = 3}, -- Magical element
							}
				},
			-- Magical thread
			{id = 13582, name = 'Magical thread', level = 0, duration = 6, type = ITEMTYPE_CREATURE_PRODUCT, price = 423,
				itemList = {{id = 10772, amount = 5}, -- Fine fabric
							{id = 3026, amount = 4}, -- White pearl
							{id = 5809, amount = 4}, -- Magical element
							}
				},
			-- Forbund
			{id = 13571, name = 'Forbund', level = 0, duration = 12, type = ITEMTYPE_CREATURE_PRODUCT, price = 385,
				itemList = {{id = 12785, amount = 30}, -- Cuprite
							{id = 5892, amount = 3}, -- Steel
							{id = 10771, amount = 1}, -- Mithril ore
							}
				},
			-- Steel edge
			{id = 12241, name = 'Steel edge', level = 0, duration = 10, type = ITEMTYPE_CREATURE_PRODUCT, price = 825,
				itemList = {{id = 5892, amount = 7}, -- Steel
							{id = 17019, amount = 10}, -- Radiating powder
							{id = 13253, amount = 20}, -- Coal
							}
				},
			-- Royal steel edge
			{id = 12242, name = 'Royal steel edge', level = 0, duration = 14, type = ITEMTYPE_CREATURE_PRODUCT, price = 1666,
				itemList = {{id = 5887, amount = 7}, -- Royal steel
							{id = 17019, amount = 13}, -- Radiating powder
							{id = 13253, amount = 20}, -- Coal
							}
				},
			-- Golden edge
			{id = 12243, name = 'Golden edge', level = 0, duration = 20, type = ITEMTYPE_CREATURE_PRODUCT, price = 2541,
				itemList = {{id = 9058, amount = 7}, -- Gold ignot
							{id = 17019, amount = 13}, -- Radiating powder
							{id = 13253, amount = 20}, -- Coal
							}
				},
			-- Mithril edge
			{id = 12244, name = 'Mithril edge', level = 0, duration = 46, type = ITEMTYPE_CREATURE_PRODUCT, price = 5925,
				itemList = {{id = 17018, amount = 5}, -- Pure mithril
							{id = 17019, amount = 50}, -- Radiating powder
							{id = 3026, amount = 20}, -- White pearl
							}
				},
			-- Mithril shard
			{id = 17124, name = 'Mithril shard', level = 0, duration = 50, type = ITEMTYPE_CREATURE_PRODUCT, price = 4465,
				itemList = {{id = 17018, amount = 5}, -- Pure mithril
							{id = 10772, amount = 20}, -- Fine fabric
							{id = 3026, amount = 20}, -- White pearl
							}
				},
			-- Dark mithril
			{id = 13572, name = 'Dark mithril', level = 0, duration = 52, type = ITEMTYPE_CREATURE_PRODUCT, price = 14575,
				itemList = {{id = 17018, amount = 5}, -- Pure mithril
							{id = 5887, amount = 20}, -- Royal steel
							{id = 13571, amount = 20}, -- Forbund
							}
				},
			
			-- Bandana
			{id = 13540, name = 'Bandana', level = 10, duration = 15, type = ITEMTYPE_HELMET, price = 20,
				itemList = {{id = 12785, amount = 3}, -- Cuprite
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Hood
			{id = 13414, name = 'Hood', level = 1, duration = 5, type = ITEMTYPE_HELMET, price = 1,
				itemList = {{id = 12233, amount = 1}, -- Untreated leather
							}
				},
			-- Leather helmet
			{id = 14850, name = 'Leather helmet', level = 4, duration = 10, type = ITEMTYPE_HELMET, price = 6,
				itemList = {{id = 12784, amount = 3}, -- Beastly claw
							{id = 12233, amount = 3}, -- Untreated leather
							}
				},
			-- Chain helmet
			{id = 13565, name = 'Chain helmet', level = 8, duration = 15, type = ITEMTYPE_HELMET, price = 12,
				itemList = {{id = 5880, amount = 1}, -- Iron ore
							{id = 5925, amount = 3}, -- Hardned bone
							{id = 12233, amount = 3}, -- Untreated leather
							}
				},
			-- Leaf helmet
			{id = 12927, name = 'Leaf helmet', level = 67, duration = 500, type = ITEMTYPE_HELMET, price = 488,
				itemList = {{id = 17019, amount = 4}, -- Radiating powder
							{id = 5809, amount = 4}, -- Magical element
							{id = 10771, amount = 2}, -- Mithril ore
							}
				},
			-- Glacier hood
			{id = 13564, name = 'Glacier hood', level = 80, duration = 646, type = ITEMTYPE_HELMET, price = 725,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							}
				},
			-- Void hood
			{id = 12077, name = 'Void hood', level = 116, duration = 720, type = ITEMTYPE_HELMET, price = 6408,
				itemList = {{id = 5809, amount = 5}, -- Magical element
							{id = 12233, amount = 6}, -- Untreated leather
							{id = 5878, amount = 4}, -- Hardened leather
							{id = 13571, amount = 4}, -- Forbund
							{id = 17124, amount = 1}, -- Mithril shard
							}
				},
			-- Lampart helmet
			{id = 12039, name = 'Lampart helmet', level = 10, duration = 20, type = ITEMTYPE_HELMET, price = 9,
				itemList = {{id = 5925, amount = 4}, -- Hardned bone
							{id = 12784, amount = 3}, -- Beastly claw
							{id = 12233, amount = 1}, -- Untreated leather
							}
				},
			-- Brass helmet
			{id = 14828, name = 'Brass helmet', level = 15, duration = 25, type = ITEMTYPE_HELMET, price = 20,
				itemList = {{id = 12832, amount = 3}, -- Rope belt
							{id = 12785, amount = 1}, -- Cuprite
							{id = 5880, amount = 2}, -- Iron ore
							}
				},
			-- Enhanced leather hood
			{id = 15383, name = 'Enhanced leather hood', level = 30, duration = 70, type = ITEMTYPE_HELMET, price = 45,
				itemList = {{id = 3026, amount = 1}, -- White pearl
							{id = 12785, amount = 3}, -- Cuprite
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Snake helmet
			{id = 14833, name = 'Snake helmet', level = 40, duration = 165, type = ITEMTYPE_HELMET, price = 149,
				itemList = {{id = 5878, amount = 2}, -- Hardener leather
							{id = 5892, amount = 1}, -- Steel
							{id = 3040, amount = 1}, -- Gold nugget
							{id = 13253, amount = 4}, -- Coal
							}
				},
			-- Copper helmet
			{id = 14863, name = 'Copper helmet', level = 18, duration = 30, type = ITEMTYPE_HELMET, price = 23,
				itemList = {{id = 12785, amount = 2}, -- Cuprite
							{id = 5880, amount = 2}, -- Iron ore
							{id = 12233, amount = 3}, -- Untreated leather
							}
				},
			-- Bronze helmet
			{id = 13432, name = 'Bronze helmet', level = 25, duration = 120, type = ITEMTYPE_HELMET, price = 48,
				itemList = {{id = 5878, amount = 1}, -- Hardener leather
							{id = 13253, amount = 5}, -- Coal
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Witch hat
			{id = 9653, name = 'Witch hat', level = 28, duration = 144, type = ITEMTYPE_HELMET, price = 70,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Wiradon's hat
			{id = 12984, name = 'Wiradon\'s hat', level = 30, duration = 180, type = ITEMTYPE_HELMET, price = 186,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							}
				},
			--- Soldier helmet
			{id = 13539, name = 'Soldier helmet', level = 30, duration = 30, type = ITEMTYPE_HELMET, price = 22,
				itemList = {{id = 13253, amount = 4}, -- Coal
							{id = 5880, amount = 2}, -- Iron ore
							{id = 12233, amount = 4}, -- Untreated leather
							}
				},
			-- Guard's Helmet
			{id = 13562, name = 'Guard\'s Helmet', level = 32, duration = 40, type = ITEMTYPE_HELMET, price = 39,
				itemList = {{id = 5878, amount = 1}, -- Hardener leather
							{id = 12832, amount = 4}, -- Rope belt
							{id = 5880, amount = 2}, -- Iron ore
							}
				},
			-- Cobalt helmet
			{id = 13510, name = 'Cobalt helmet', level = 70, duration = 500, type = ITEMTYPE_HELMET, price = 756,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 5892, amount = 3}, -- Steel
							{id = 5880, amount = 6}, -- Iron ore
							}
				},
			-- Royal steel helmet
			{id = 13400, name = 'Royal steel helmet', level = 60, duration = 486, type = ITEMTYPE_HELMET, price = 583,
				itemList = {{id = 10772, amount = 3}, -- Fine fabric
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 3}, -- Steel
							{id = 13253, amount = 12}, -- Coal
							}
				},
			-- Swamplair helmet
			{id = 15246, name = 'Swamplair helmet', level = 90, duration = 760, type = ITEMTYPE_HELMET, price = 724,
				itemList = {{id = 9058, amount = 1}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 3026, amount = 3}, -- White pearl
							{id = 5809, amount = 3}, -- Magical element
							{id = 5925, amount = 11}, -- Hardned bone
							}
				},
			-- Haunted helmet
			{id = 3437, name = 'Haunted helmet', level = 75, duration = 600, type = ITEMTYPE_HELMET, price = 1006,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 5892, amount = 2}, -- Steel
							{id = 3027, amount = 4}, -- Black pearl
							}
				},
			-- Iron helmet
			{id = 13427, name = 'Iron helmet', level = 40, duration = 380, type = ITEMTYPE_HELMET, price = 186,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 13253, amount = 5}, -- Coal
							{id = 5880, amount = 4}, -- Iron ore
							{id = 12784, amount = 11}, -- Beastly claw
							}
				},
			-- Ancient bronze helmet
			{id = 12030, name = 'Ancient bronze helmet', level = 50, duration = 444, type = ITEMTYPE_HELMET, price = 61,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 13253, amount = 3}, -- Coal
							{id = 12785, amount = 2}, -- Cuprite
							}
				},
			-- Berynit helmet
			{id = 13556, name = 'Berynit helmet', level = 32, duration = 140, type = ITEMTYPE_HELMET, price = 100,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 5809, amount = 1}, -- Magical element
							{id = 12785, amount = 6}, -- Cuprite
							}
				},
			-- Strange helmet
			{id = 13557, name = 'Strange helmet', level = 38, duration = 200, type = ITEMTYPE_HELMET, price = 675,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 5878, amount = 4}, -- Hardener leather
							{id = 5887, amount = 1}, -- Royal steel
							{id = 13253, amount = 4}, -- Coal
							}
				},
			-- Steel helmet
			{id = 13441, name = 'Steel helmet', level = 40, duration = 240, type = ITEMTYPE_HELMET, price = 209,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 5892, amount = 2}, -- Steel
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Visor
			{id = 13566, name = 'Visor', level = 54, duration = 375, type = ITEMTYPE_HELMET, price = 269,
				itemList = {{id = 5892, amount = 3}, -- Steel
							{id = 3040, amount = 2}, -- Gold nugget
							{id = 5880, amount = 4}, -- Iron ore
							{id = 12832, amount = 9}, -- Rope belt
							}
				},
			-- Ancient helmet
			{id = 13560, name = 'Ancient helmet', level = 42, duration = 240, type = ITEMTYPE_HELMET, price = 704,
				itemList = {{id = 10772, amount = 3}, -- Fine fabric
							{id = 9058, amount = 1}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5925, amount = 8}, -- Hardned bone
							}
				},
			-- Titan helmet
			{id = 12872, name = 'Titan helmet', level = 120, duration = 1000, type = ITEMTYPE_HELMET, price = 2098,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5878, amount = 10}, -- Hardener leather
							}
				},
			-- Dragon rider helmet
			{id = 12087, name = 'Dragon rider helmet', level = 140, duration = 1280, type = ITEMTYPE_HELMET, price = 20586,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 17124, amount = 1}, -- Mithril shard
							{id = 10771, amount = 15}, -- Mithril ore
							{id = 5878, amount = 2}, -- Hardener leather
							}
				},
			-- Legionnaire helmet
			{id = 3539, name = 'Legionnaire helmet', level = 110, duration = 1111, type = ITEMTYPE_HELMET, price = 2314,
				itemList = {{id = 17124, amount = 1}, -- Mithril shard
							{id = 13571, amount = 4}, -- Forbund
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 5892, amount = 3}, -- Steel
							{id = 5887, amount = 2}, -- Royal steel
							{id = 12785, amount = 7}, -- Cuprite
							}
				},
			-- Black steel helmet
			{id = 15236, name = 'Black steel helmet', level = 100, duration = 788, type = ITEMTYPE_HELMET, price = 1208,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 5892, amount = 2}, -- Steel
							{id = 5880, amount = 11}, -- Iron ore
							}
				},
			-- Hellfire helmet
			{id = 15281, name = 'Hellfire helmet', level = 105, duration = 812, type = ITEMTYPE_HELMET, price = 2227,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 13582, amount = 1}, -- Magical thread
							{id = 5887, amount = 3}, -- Royal steel
							{id = 5878, amount = 9}, -- Hardener leather
							{id = 3027, amount = 11}, -- Black pearl
							}
				},
			-- Meteorite helmet
			{id = 15286, name = 'Meteorite helmet', level = 130, duration = 1010, type = ITEMTYPE_HELMET, price = 38610,
				itemList = {{id = 3026, amount = 11}, -- White pearl
							{id = 5809, amount = 4}, -- Magical element
							{id = 9058, amount = 4}, -- Gold ingot
							{id = 13582, amount = 5}, -- Magical thread
							{id = 13571, amount = 3}, -- Forbund
							{id = 17124, amount = 1}, -- Mithril shard
							{id = 13572, amount = 2}, -- Dark mithril
							}
				},
			-- Hood of the dawn
			{id = 15368, name = 'Hood of the dawn', level = 65, duration = 600, type = ITEMTYPE_HELMET, price = 1125,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 5809, amount = 8}, -- Magical element
							{id = 3026, amount = 8}, -- White pearl
							}
				},
			-- Ashbringer hood
			{id = 13023, name = 'Ashbringer hood', level = 80, duration = 720, type = ITEMTYPE_HELMET, price = 1125,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 5809, amount = 8}, -- Magical element
							{id = 3027, amount = 8}, -- Black pearl
							}
				},
			
			-- Light armor
			{id = 13078, name = 'Light armor', level = 10, duration = 40, type = ITEMTYPE_ARMOR, price = 30,
				itemList = {{id = 12832, amount = 4}, -- Rope belt
							{id = 5880, amount = 4}, -- Iron ore
							{id = 12784, amount = 4}, -- Beastly claw
							}
				},
			-- Leather armor
			{id = 14851, name = 'Leather armor', level = 6, duration = 10, type = ITEMTYPE_ARMOR, price = 15,
				itemList = {{id = 12785, amount = 2}, -- Cuprite
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Red robe
			{id = 3566, name = 'Red robe', level = 72, duration = 600, type = ITEMTYPE_ARMOR, price = 2100,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Pirate shirt
			{id = 6095, name = 'Pirate shirt', level = 30, duration = 108, type = ITEMTYPE_ARMOR, price = 182,
				itemList = {{id = 5878, amount = 4}, -- Hardener leather
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Lampart armor
			{id = 12038, name = 'Lampart armor', level = 12, duration = 20, type = ITEMTYPE_ARMOR, price = 30,
				itemList = {{id = 12832, amount = 3}, -- Rope belt
							{id = 12785, amount = 2}, -- Cuprite
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- The armor of hardened leather
			{id = 3537, name = 'The armor of hardened leather', level = 15, duration = 40, type = ITEMTYPE_ARMOR, price = 85,
				itemList = {{id = 12785, amount = 2}, -- Cuprite
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 12832, amount = 4}, -- Rope belt
							}
				},
			-- Brass armor
			{id = 14829, name = 'Brass armor', level = 17, duration = 25, type = ITEMTYPE_ARMOR, price = 86,
				itemList = {{id = 3026, amount = 1}, -- White pearl
							{id = 5878, amount = 2}, -- Hardener leather
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Copper armor
			{id = 14864, name = 'Copper armor', level = 20, duration = 32, type = ITEMTYPE_ARMOR, price = 75,
				itemList = {{id = 12785, amount = 2}, -- Cuprite
							{id = 5880, amount = 4}, -- Iron ore
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Bronze armor
			{id = 13431, name = 'Bronze armor', level = 27, duration = 120, type = ITEMTYPE_ARMOR, price = 75,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 5880, amount = 5}, -- Iron ore
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Enhanced leather armor
			{id = 15384, name = 'Enhanced leather armor', level = 34, duration = 165, type = ITEMTYPE_ARMOR, price = 58,
				itemList = {{id = 3040, amount = 1}, -- Gold nugget
							{id = 5925, amount = 4}, -- Hardned bone
							{id = 12784, amount = 3}, -- Beastly claw
							}
				},
			-- Wiradon's robe
			{id = 12985, name = 'Wiradon\'s robe', level = 32, duration = 180, type = ITEMTYPE_ARMOR, price = 277,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 4}, -- Radiating powder
							}
				},
			-- Cloak of fear
			{id = 13084, name = 'Cloak of fear', level = 33, duration = 240, type = ITEMTYPE_ARMOR, price = 1045,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Armour legionnaires
			{id = 13248, name = 'Armour legionnaires', level = 34, duration = 220, type = ITEMTYPE_ARMOR, price = 120,
				itemList = {{id = 5892, amount = 2}, -- Steel
							{id = 12785, amount = 3}, -- Cuprite
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Cirith guardians armor
			{id = 13062, name = 'Cirith guardians armor', level = 34, duration = 320, type = ITEMTYPE_ARMOR, price = 325,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Void robe
			{id = 12078, name = 'Void robe', level = 130, duration = 690, type = ITEMTYPE_ARMOR, price = 21139,
				itemList = {{id = 12832, amount = 4}, -- Rope belt
							{id = 5878, amount = 6}, -- Hardened leather
							{id = 5887, amount = 5}, -- Royal steel
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 13572, amount = 1}, -- Dark mithril
							{id = 17124, amount = 1}, -- Mithril shard
							}
				},
			-- Blue robe
			{id = 3567, name = 'Blue robe', level = 35, duration = 260, type = ITEMTYPE_ARMOR, price = 452,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 5809, amount = 4}, -- Magical element
							{id = 3027, amount = 3}, -- Black pearl
							}
				},
			-- Red robe
			{id = 3557, name = 'Red robe', level = 38, duration = 298, type = ITEMTYPE_ARMOR, price = 589,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 17019, amount = 1}, -- Radiating powder
							{id = 5878, amount = 4}, -- Hardener leather
							}
				},
			-- Iron plate
			{id = 13426, name = 'Iron plate', level = 35, duration = 200, type = ITEMTYPE_ARMOR, price = 292,
				itemList = {{id = 10772, amount = 4}, -- Fine fabric
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Breastplate
			{id = 3552, name = 'Breastplate', level = 33, duration = 120, type = ITEMTYPE_ARMOR, price = 145,
				itemList = {{id = 10771, amount = 1}, -- Mithril ore
							{id = 12785, amount = 5}, -- Cuprite
							{id = 5880, amount = 4}, -- Iron ore
							}
				},
			-- Robe of the Underworld
			{id = 13067, name = 'Robe of the Underworld', level = 50, duration = 498, type = ITEMTYPE_ARMOR, price = 6003,
				itemList = {{id = 17124, amount = 1}, -- Mithril shard
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17019, amount = 4}, -- Radiating powder
							}
				},
			-- Dark armor
			{id = 13079, name = 'Dark armor', level = 42, duration = 366, type = ITEMTYPE_ARMOR, price = 139,
				itemList = {{id = 5878, amount = 3}, -- Hardener leather
							{id = 5892, amount = 1}, -- Steel
							{id = 5880, amount = 5}, -- Iron ore
							}
				},
			-- Steel armor
			{id = 13440, name = 'Steel armor', level = 40, duration = 300, type = ITEMTYPE_ARMOR, price = 139,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 12785, amount = 5}, -- Cuprite
							{id = 5880, amount = 5}, -- Iron ore
							}
				},
			-- Ashbringer robe
			{id = 13025, name = 'Ashbringer robe', level = 120, duration = 1110, type = ITEMTYPE_ARMOR, price = 2443,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 3027, amount = 10}, -- Black pearl
							{id = 5878, amount = 6}, -- Hardener leather
							}
				},
			-- Robe of the dawn
			{id = 15369, name = 'Robe of the dawn', level = 116, duration = 1090, type = ITEMTYPE_ARMOR, price = 1648,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 5809, amount = 5}, -- Magical element
							{id = 3026, amount = 10}, -- White pearl
							{id = 5878, amount = 6}, -- Hardener leather
							}
				},
			-- Plate of underwater
			{id = 13066, name = 'Plate of underwater', level = 36, duration = 300, type = ITEMTYPE_ARMOR, price = 875,
				itemList = {{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Forest whisper cape
			{id = 15275, name = 'Forest whisper cape', level = 40, duration = 400, type = ITEMTYPE_ARMOR, price = 2195,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Sparkling cape
			{id = 13087, name = 'Sparkling cape', level = 48, duration = 470, type = ITEMTYPE_ARMOR, price = 2395,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Green robe
			{id = 3556, name = 'Green robe', level = 42, duration = 400, type = ITEMTYPE_ARMOR, price = 633,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 2}, -- Radiating powder
							}
				},
			-- Armor of loyalists
			{id = 13063, name = 'Armor of loyalists', level = 48, duration = 500, type = ITEMTYPE_ARMOR, price = 241,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 5892, amount = 2}, -- Steel
							{id = 12832, amount = 4}, -- Rope belt
							}
				},
			-- Knight armor
			{id = 13088, name = 'Knight armor', level = 54, duration = 600, type = ITEMTYPE_ARMOR, price = 236,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 5878, amount = 2}, -- Hardener leather
							{id = 5887, amount = 1}, -- Royal steel
							}
				},
			-- Emerald armor
			{id = 13068, name = 'Emerald armor', level = 56, duration = 624, type = ITEMTYPE_ARMOR, price = 505,
				itemList = {{id = 9058, amount = 1}, -- Gold ignot
							{id = 5887, amount = 1}, -- Royal steel
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Armor of leaf clan
			{id = 13251, name = 'Armor of leaf clan', level = 59, duration = 687, type = ITEMTYPE_ARMOR, price = 1305,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Amber armor
			{id = 13064, name = 'Amber armor', level = 60, duration = 700, type = ITEMTYPE_ARMOR, price = 1810,
				itemList = {{id = 9058, amount = 1}, -- Gold ignot
							{id = 5887, amount = 4}, -- Royal steel
							{id = 10771, amount = 5}, -- Mithril ore
							{id = 5878, amount = 20}, -- Hardener leather
							}
				},
			-- Snake armor
			{id = 14834, name = 'Snake armor', level = 56, duration = 750, type = ITEMTYPE_ARMOR, price = 375,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 5887, amount = 1}, -- Royal steel
							}
				},
			-- Lavos armor
			{id = 13073, name = 'Lavos armor', level = 80, duration = 1440, type = ITEMTYPE_ARMOR, price = 1177,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 4}, -- Radiating powder
							}
				},
			-- Berynit armor
			{id = 13249, name = 'Berynit armor', level = 38, duration = 400, type = ITEMTYPE_ARMOR, price = 382,
				itemList = {{id = 5878, amount = 4}, -- Hardener leather
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Royal steel armor
			{id = 13401, name = 'Royal steel armor', level = 55, duration = 800, type = ITEMTYPE_ARMOR, price = 567,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 5878, amount = 4}, -- Hardener leather
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Black steel armor
			{id = 15235, name = 'Black steel armor', level = 64, duration = 900, type = ITEMTYPE_ARMOR, price = 944,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 3}, -- Steel
							}
				},
			-- Swamplair armor
			{id = 15245, name = 'Swamplair armor', level = 70, duration = 700, type = ITEMTYPE_ARMOR, price = 1130,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							}
				},
			-- Mythril plate armor
			{id = 13069, name = 'Mythril plate armor', level = 120, duration = 1440, type = ITEMTYPE_ARMOR, price = 7950,
				itemList = {{id = 17018, amount = 10}, -- Pure mithril
							{id = 13582, amount = 4}, -- Magical thread
							}
				},
			-- Chest guard
			{id = 13250, name = 'Chest guard', level = 88, duration = 1000, type = ITEMTYPE_ARMOR, price = 3150,
				itemList = {{id = 5892, amount = 6}, -- Steel
							{id = 13572, amount = 1}, -- Dark mithril
							{id = 13571, amount = 2}, -- Forbund
							{id = 12784, amount = 10}, -- Beastly claw
							{id = 5887, amount = 5}, -- Royal steel
							}
				},
			-- Golden armor
			{id = 15216, name = 'Golden armor', level = 105, duration = 1440, type = ITEMTYPE_ARMOR, price = 2660,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 9058, amount = 4}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Leaf armor
			{id = 12926, name = 'Leaf armor', level = 80, duration = 660, type = ITEMTYPE_ARMOR, price = 1810,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 5}, -- Gold ignot
							{id = 5892, amount = 5}, -- Steel
							}
				},
			-- Dragon plate
			{id = 14822, name = 'Dragon plate', level = 110, duration = 1200, type = ITEMTYPE_ARMOR, price = 1910,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Magma armor
			{id = 13072, name = 'Magma armor', level = 112, duration = 1500, type = ITEMTYPE_ARMOR, price = 3290,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Warrior's armor
			{id = 13075, name = 'Warrior\'s armor', level = 118, duration = 1310, type = ITEMTYPE_ARMOR, price = 2698,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 4}, -- Fine fabric
							}
				},
			-- Frosty armor
			{id = 15269, name = 'Frosty armor', level = 120, duration = 1408, type = ITEMTYPE_ARMOR, price = 3383,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 17019, amount = 4}, -- Radiating powder
							}
				},
			-- Bronze plate
			{id = 13070, name = 'Bronze plate', level = 130, duration = 1620, type = ITEMTYPE_ARMOR, price = 6000,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 4}, -- Pure mithril
							{id = 9058, amount = 5}, -- Gold ignot
							}
				},
			-- Meteorite plate armor
			{id = 15285, name = 'Meteorite plate armor', level = 124, duration = 1555, type = ITEMTYPE_ARMOR, price = 41110,
				itemList = {{id = 9058, amount = 5}, -- Gold ingot
							{id = 17018, amount = 5}, -- Pure mithril
							{id = 13582, amount = 3}, -- Magical thread
							{id = 13571, amount = 4}, -- Forbund
							{id = 17124, amount = 1}, -- Mithril shard
							{id = 13572, amount = 2}, -- Dark mithril
							}
				},
			-- Hellfire armor
			{id = 15280, name = 'Hellfire armor', level = 126, duration = 1600, type = ITEMTYPE_ARMOR, price = 5815,
				itemList = {{id = 13571, amount = 4}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 9058, amount = 5}, -- Gold ignot
							}
				},
			-- Magic plate armor
			{id = 3366, name = 'Magic plate armor', level = 130, duration = 1666, type = ITEMTYPE_ARMOR, price = 23440,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 17124, amount = 1}, -- Mithril shard
							{id = 13571, amount = 5}, -- Forbund
							{id = 13582, amount = 5}, -- Magical thread
							}
				},
			
			-- Leather legs
			{id = 14852, name = 'Leather legs', level = 5, duration = 50, type = ITEMTYPE_LEGS, price = 6,
				itemList = {{id = 12233, amount = 3}, -- Untreated leather
							{id = 12784, amount = 3}, -- Beastly claw
							}
				},
			-- Lampart legs
			{id = 12037, name = 'Lampart legs', level = 11, duration = 110, type = ITEMTYPE_LEGS, price = 14,
				itemList = {{id = 12233, amount = 5}, -- Untreated leather
							{id = 5880, amount = 1}, -- Iron ore
							{id = 5925, amount = 3}, -- Hardned bone
							}
				},
			-- Brass legs
			{id = 14830, name = 'Brass legs', level = 16, duration = 160, type = ITEMTYPE_LEGS, price = 30,
				itemList = {{id = 12785, amount = 3}, -- Cuprite
							{id = 5880, amount = 2}, -- Iron ore
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Copper legs
			{id = 14865, name = 'Copper legs', level = 19, duration = 190, type = ITEMTYPE_LEGS, price = 25,
				itemList = {{id = 12785, amount = 4}, -- Cuprite
							{id = 12784, amount = 5}, -- Beastly claw
							}
				},
			-- Bronze legs
			{id = 13433, name = 'Bronze legs', level = 26, duration = 260, type = ITEMTYPE_LEGS, price = 115,
				itemList = {{id = 12785, amount = 12}, -- Cuprite
							{id = 5892, amount = 1}, -- Steel
							{id = 12233, amount = 10}, -- Untreated leather
							}
				},
			-- Wiradon's legs
			{id = 12986, name = 'Wiradon\'s legs', level = 31, duration = 310, type = ITEMTYPE_LEGS, price = 278,
				itemList = {{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 2}, -- Radiating powder
							{id = 5809, amount = 2}, -- Magical element
							}
				},
			-- Boar loincloth
			{id = 12992, name = 'Boar loincloth', level = 22, duration = 220, type = ITEMTYPE_LEGS, price = 74,
				itemList = {{id = 5878, amount = 3}, -- Hardener leather
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Kilt of the fallen
			{id = 15223, name = 'Kilt of the fallen', level = 50, duration = 500, type = ITEMTYPE_LEGS, price = 1313,
				itemList = {{id = 10772, amount = 4}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 13582, amount = 2}, -- Magical thread
							}
				},
			-- Leaf legs
			{id = 12925, name = 'Leaf legs', level = 72, duration = 720, type = ITEMTYPE_LEGS, price = 2620,
				itemList = {{id = 9058, amount = 3}, -- Gold ignot
							{id = 5892, amount = 2}, -- Steel
							{id = 13571, amount = 4}, -- Forbund
							}
				},
			-- Enhanced leather legs
			{id = 15385, name = 'Enhanced leather legs', level = 30, duration = 300, type = ITEMTYPE_LEGS, price = 150,
				itemList = {{id = 3027, amount = 1}, -- Black pearl
							{id = 12785, amount = 5}, -- Cuprite
							{id = 3040, amount = 2}, -- Gold nugget
							}
				},
			-- Snake legs
			{id = 14835, name = 'Snake legs', level = 48, duration = 480, type = ITEMTYPE_LEGS, price = 1414,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Forest whisper legs
			{id = 15274, name = 'Forest whisper legs', level = 90, duration = 900, type = ITEMTYPE_LEGS, price = 3293,
				itemList = {{id = 13571, amount = 4}, -- Forbund
							{id = 13582, amount = 3}, -- Magical thread
							{id = 10772, amount = 5}, -- Fine fabric
							}
				},
			-- Iron legs
			{id = 13428, name = 'Iron legs', level = 36, duration = 360, type = ITEMTYPE_LEGS, price = 517,
				itemList = {{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 3}, -- Steel
							{id = 5878, amount = 4}, -- Hardener leather
							}
				},
			-- Swamplair legs
			{id = 15244, name = 'Swamplair legs', level = 64, duration = 640, type = ITEMTYPE_LEGS, price = 2188,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							}
				},
			-- Steel legs
			{id = 13405, name = 'Steel legs', level = 40, duration = 400, type = ITEMTYPE_LEGS, price = 1345,
				itemList = {{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 4}, -- Royal steel
							{id = 5892, amount = 5}, -- Steel
							}
				},
			-- Knight legs
			{id = 3540, name = 'Knight legs', level = 60, duration = 600, type = ITEMTYPE_LEGS, price = 2088,
				itemList = {{id = 17018, amount = 3}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 12785, amount = 4}, -- Cuprite
							}
				},
			-- Royal steel legs
			{id = 13399, name = 'Royal steel legs', level = 72, duration = 720, type = ITEMTYPE_LEGS, price = 2523,
				itemList = {{id = 13571, amount = 4}, -- Forbund
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 17019, amount = 4}, -- Radiating powder
							}
				},
			-- Black steel legs
			{id = 15234, name = 'Black steel legs', level = 90, duration = 900, type = ITEMTYPE_LEGS, price = 5180,
				itemList = {{id = 13582, amount = 4}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5887, amount = 5}, -- Royal steel
							}
				},
			-- Hellfire legs
			{id = 15279, name = 'Hellfire legs', level = 114, duration = 1140, type = ITEMTYPE_LEGS, price = 10932,
				itemList = {{id = 17124, amount = 2}, -- Mithril shard
							{id = 13571, amount = 4}, -- Forbund
							{id = 17019, amount = 6}, -- Radiating powder
							}
				},
			-- Golden legs
			{id = 15215, name = 'Golden legs', level = 110, duration = 1100, type = ITEMTYPE_LEGS, price = 5585,
				itemList = {{id = 17124, amount = 1}, -- Mithril shard
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 17019, amount = 6}, -- Radiating powder
							}
				},
			-- Void legs
			{id = 12079, name = 'Void legs', level = 128, duration = 1200, type = ITEMTYPE_LEGS, price = 6489,
				itemList = {{id = 12832, amount = 5}, -- Rope belt
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 3026, amount = 12}, -- White pearl
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17124, amount = 1}, -- Mithril shard
							}
				},
			-- Ashbringer legs
			{id = 13024, name = 'Ashbringer legs', level = 110, duration = 1000, type = ITEMTYPE_LEGS, price = 1604,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 2}, -- Fine fabric
							}
				},
			-- Legs of the dawn
			{id = 15370, name = 'Legs of the dawn', level = 110, duration = 1000, type = ITEMTYPE_LEGS, price = 1179,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5809, amount = 4}, -- Magical element
							{id = 10772, amount = 2}, -- Fine fabric
							}
				},
			-- Frosty legs
			{id = 15268, name = 'Frosty legs', level = 124, duration = 1240, type = ITEMTYPE_LEGS, price = 9195,
				itemList = {{id = 17124, amount = 1}, -- Mithril shard
							{id = 13571, amount = 5}, -- Forbund
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 2}, -- Pure mithril
							}
				},
			-- Meteorite legs
			{id = 15284, name = 'Meteorite legs', level = 130, duration = 1300, type = ITEMTYPE_LEGS, price = 53725,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 17124, amount = 5}, -- Mithril shard
							{id = 13582, amount = 5}, -- Magical thread
							}
				},
			
			-- Leather boots
			{id = 14853, name = 'Leather boots', level = 3, duration = 10, type = ITEMTYPE_FEET, price = 6,
				itemList = {{id = 12233, amount = 3}, -- Untreated leather
							{id = 12784, amount = 3}, -- Beastly claw
							}
				},
			-- Lampart boots
			{id = 12036, name = 'Lampart boots', level = 9, duration = 20, type = ITEMTYPE_FEET, price = 14,
				itemList = {{id = 12233, amount = 5}, -- Untreated leather
							{id = 5880, amount = 1}, -- Iron ore
							{id = 5925, amount = 3}, -- Hardned bone
							}
				},
			-- Brass boots
			{id = 14831, name = 'Brass boots', level = 14, duration = 25, type = ITEMTYPE_FEET, price = 30,
				itemList = {{id = 12785, amount = 3}, -- Cuprite
							{id = 5880, amount = 2}, -- Iron ore
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Copper boots
			{id = 14866, name = 'Copper boots', level = 17, duration = 30, type = ITEMTYPE_FEET, price = 25,
				itemList = {{id = 12785, amount = 4}, -- Cuprite
							{id = 12784, amount = 5}, -- Beastly claw
							}
				},
			-- Wiradon's boots
			{id = 12987, name = 'Wiradon\'s boots', level = 29, duration = 180, type = ITEMTYPE_FEET, price = 278,
				itemList = {{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 2}, -- Radiating powder
							{id = 5809, amount = 2}, -- Magical element
							}
				},
			-- Boots of haste
			{id = 3079, name = 'Boots of haste', level = 60, duration = 600, type = ITEMTYPE_FEET, price = 3508,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Sandals
			{id = 3551, name = 'Sandals', level = 6, duration = 36, type = ITEMTYPE_FEET, price = 28,
				itemList = {{id = 5878, amount = 1}, -- Hardener leather
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Void boots
			{id = 12080, name = 'Void boots', level = 100, duration = 550, type = ITEMTYPE_LEGS, price = 15984,
				itemList = {{id = 3027, amount = 7}, -- Black pearl
							{id = 3026, amount = 8}, -- White pearl
							{id = 13582, amount = 2}, -- Magical thread
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 13572, amount = 1}, -- Dark mithril
							}
				},
			-- Light boots
			{id = 13081, name = 'Light boots', level = 22, duration = 40, type = ITEMTYPE_FEET, price = 127,
				itemList = {{id = 5880, amount = 3}, -- Iron ore
							{id = 5878, amount = 1}, -- Hardener leather
							{id = 10772, amount = 2}, -- Fine fabric
							}
				},
			-- Snake boots
			{id = 14836, name = 'Snake boots', level = 38, duration = 120, type = ITEMTYPE_FEET, price = 343,
				itemList = {{id = 17019, amount = 2}, -- Radiating powder
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Enhanced leather boots
			{id = 15386, name = 'Enhanced leather boots', level = 30, duration = 98, type = ITEMTYPE_FEET, price = 121,
				itemList = {{id = 17019, amount = 1}, -- Radiating powder
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 12784, amount = 5}, -- Beastly claw
							}
				},
			-- Boots of the fallen
			{id = 15222, name = 'Boots of the fallen', level = 70, duration = 590, type = ITEMTYPE_FEET, price = 2862,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 17019, amount = 6}, -- Radiating powder
							}
				},
			-- Iron boots
			{id = 13429, name = 'Iron boots', level = 39, duration = 300, type = ITEMTYPE_FEET, price = 670,
				itemList = {{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 4}, -- Steel
							{id = 10771, amount = 2}, -- Mithril ore
							}
				},
			-- Pirate boots
			{id = 5461, name = 'Pirate boots', level = 44, duration = 416, type = ITEMTYPE_FEET, price = 1091,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 5878, amount = 2}, -- Hardener leather
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Steel boots
			{id = 13404, name = 'Steel boots', level = 54, duration = 598, type = ITEMTYPE_FEET, price = 1915,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 5887, amount = 4}, -- Royal steel
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Boots of the dawn
			{id = 15371, name = 'Boots of the dawn', level = 110, duration = 780, type = ITEMTYPE_FEET, price = 1227,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 5809, amount = 2}, -- Magical element
							{id = 10772, amount = 2}, -- Fine fabric
							}
				},
			-- Black steel boots
			{id = 15233, name = 'Black steel boots', level = 72, duration = 796, type = ITEMTYPE_FEET, price = 3025,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 1}, -- Gold ignot
							}
				},
			-- Dragon boots
			{id = 14824, name = 'Dragon boots', level = 110, duration = 900, type = ITEMTYPE_FEET, price = 4368,
				itemList = {{id = 13571, amount = 5}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Boots of marksman
			{id = 12031, name = 'Boots of marksman', level = 76, duration = 788, type = ITEMTYPE_FEET, price = 2325,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Swamplair boots
			{id = 15243, name = 'Swamplair boots', level = 70, duration = 666, type = ITEMTYPE_FEET, price = 2931,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 17019, amount = 4}, -- Radiating powder
							}
				},
			-- Meteorite boots
			{id = 15283, name = 'Meteorite boots', level = 98, duration = 1000, type = ITEMTYPE_FEET, price = 4454,
				itemList = {{id = 13572, amount = 1}, -- Dark Mithril
				            {id = 17124, amount = 1}, -- Mithril shard
							{id = 13582, amount = 4}, -- Magical thread
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 3}, -- Fine fabric
							}
				},
			-- Frosty boots
			{id = 15267, name = 'Frosty boots', level = 93, duration = 998, type = ITEMTYPE_FEET, price = 4105,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 5}, -- Gold ignot
							}
				},
			-- Bronze boots
			{id = 13434, name = 'Bronze boots', level = 50, duration = 590, type = ITEMTYPE_FEET, price = 1051,
				itemList = {{id = 10772, amount = 8}, -- Fine fabric
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 5887, amount = 4}, -- Royal steel
							}
				},
			-- Royal steel boots
			{id = 13398, name = 'Royal steel boots', level = 68, duration = 724, type = ITEMTYPE_FEET, price = 2743,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							}
				},
			-- Ruby boots
			{id = 15248, name = 'Ruby boots', level = 120, duration = 1000, type = ITEMTYPE_FEET, price = 10515,
				itemList = {{id = 17124, amount = 2}, -- Mithril shard
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Hellfire boots
			{id = 15278, name = 'Hellfire boots', level = 118, duration = 1050, type = ITEMTYPE_FEET, price = 10443,
				itemList = {{id = 17124, amount = 2}, -- Mithril shard
							{id = 13571, amount = 3}, -- Forbund
							{id = 10772, amount = 5}, -- Fine fabric
							}
				},
			
			-- Wooden shield
			{id = 14856, name = 'Wooden shield', level = 5, duration = 10, type = ITEMTYPE_SHIELD, price = 6,
				itemList = {{id = 12233, amount = 3}, -- Untreated leather
							{id = 12784, amount = 3}, -- Beastly claw
							}
				},
			-- Lampart shield
			{id = 12041, name = 'Lampart shield', level = 11, duration = 20, type = ITEMTYPE_SHIELD, price = 18,
				itemList = {{id = 12233, amount = 2}, -- Untreated leather
							{id = 5880, amount = 2}, -- Iron ore
							{id = 5925, amount = 5}, -- Hardned bone
							}
				},
			-- Orc shield
			{id = 13415, name = 'Orc shield', level = 18, duration = 26, type = ITEMTYPE_SHIELD, price = 18,
				itemList = {{id = 5880, amount = 2}, -- Iron ore
							{id = 12784, amount = 4}, -- Beastly claw
							{id = 12233, amount = 4}, -- Untreated leather
							}
				},
			-- Copper shield
			{id = 14862, name = 'Copper shield', level = 19, duration = 30, type = ITEMTYPE_SHIELD, price = 28,
				itemList = {{id = 12785, amount = 5}, -- Cuprite
							{id = 12784, amount = 3}, -- Beastly claw
							}
				},
			-- Bronze shield
			{id = 13435, name = 'Bronze shield', level = 27, duration = 120, type = ITEMTYPE_SHIELD, price = 115,
				itemList = {{id = 12785, amount = 12}, -- Cuprite
							{id = 5892, amount = 1}, -- Steel
							{id = 12233, amount = 10}, -- Untreated leather
							}
				},
			-- Iron shield
			{id = 13417, name = 'Iron shield', level = 30, duration = 132, type = ITEMTYPE_SHIELD, price = 95,
				itemList = {{id = 5880, amount = 5}, -- Iron ore
							{id = 5892, amount = 1}, -- Steel
							{id = 3026, amount = 1}, -- White pearl
							}
				},
			-- Enhanced oak shield
			{id = 13395, name = 'Enhanced oak shield', level = 34, duration = 136, type = ITEMTYPE_SHIELD, price = 74,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 12785, amount = 5}, -- Cuprite
							{id = 5925, amount = 3}, -- Hardned bone
							}
				},
			-- Dark shield
			{id = 3421, name = 'Dark shield', level = 38, duration = 144, type = ITEMTYPE_SHIELD, price = 165,
				itemList = {{id = 5892, amount = 2}, -- Steel
							{id = 3027, amount = 1}, -- Black pearl
							{id = 3040, amount = 1}, -- Gold nugget
							}
				},
			-- Steel shield
			{id = 13402, name = 'Steel shield', level = 45, duration = 180, type = ITEMTYPE_SHIELD, price = 255,
				itemList = {{id = 5880, amount = 4}, -- Iron ore
							{id = 5892, amount = 2}, -- Steel
							{id = 5887, amount = 1}, -- Royal steel
							}
				},
			-- Phoenix shield
			{id = 3439, name = 'Phoenix shield', level = 70, duration = 222, type = ITEMTYPE_SHIELD, price = 2220,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Griffin shield
			{id = 3435, name = 'Griffin shield', level = 52, duration = 220, type = ITEMTYPE_SHIELD, price = 1080,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 5887, amount = 1}, -- Royal steel
							{id = 5809, amount = 3}, -- Magical element
							}
				},
			-- Tortoise shield
			{id = 6131, name = 'Tortoise shield', level = 44, duration = 206, type = ITEMTYPE_SHIELD, price = 320,
				itemList = {{id = 5887, amount = 2}, -- Royal steel
							{id = 12785, amount = 3}, -- Cuprite
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Guardian shield
			{id = 13085, name = 'Guardian shield', level = 39, duration = 200, type = ITEMTYPE_SHIELD, price = 241,
				itemList = {{id = 5892, amount = 3}, -- Steel
							{id = 13253, amount = 3}, -- Coal
							{id = 5809, amount = 4}, -- Magical element
							}
				},
			-- Spike shield
			{id = 13521, name = 'Spike shield', level = 50, duration = 250, type = ITEMTYPE_SHIELD, price = 360,
				itemList = {{id = 3040, amount = 2}, -- Gold nugget
							{id = 5892, amount = 3}, -- Steel
							{id = 5809, amount = 5}, -- Magical element
							}
				},
			-- Royal steel shield
			{id = 13397, name = 'Royal steel shield', level = 58, duration = 288, type = ITEMTYPE_SHIELD, price = 595,
				itemList = {{id = 5887, amount = 3}, -- Royal steel
							{id = 5892, amount = 3}, -- Steel
							{id = 5880, amount = 5}, -- Iron ore
							}
				},
			-- Holy shield
			{id = 13094, name = 'Holy shield', level = 55, duration = 290, type = ITEMTYPE_SHIELD, price = 435,
				itemList = {{id = 9058, amount = 1}, -- Gold ignot
							{id = 5892, amount = 2}, -- Steel
							{id = 3026, amount = 3}, -- White pearl
							}
				},
			-- Treebeard shield
			{id = 12082, name = 'Treebeard shield', level = 70, duration = 366, type = ITEMTYPE_SHIELD, price = 475,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 5809, amount = 3}, -- Magical element
							{id = 12785, amount = 10}, -- Cuprite
							}
				},
			-- Lightning shield
			{id = 3442, name = 'Lightning shield', level = 68, duration = 360, type = ITEMTYPE_SHIELD, price = 1140,
				itemList = {{id = 17018, amount = 1}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Haunted shield
			{id = 12875, name = 'Haunted shield', level = 75, duration = 400, type = ITEMTYPE_SHIELD, price = 1860,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 5887, amount = 3}, -- Royal steel
							{id = 5892, amount = 3}, -- Steel
							}
				},
			-- Silver shield
			{id = 12874, name = 'Silver shield', level = 88, duration = 490, type = ITEMTYPE_SHIELD, price = 2755,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 13582, amount = 3}, -- Magical thread
							{id = 5878, amount = 5}, -- Hardener leather
							}
				},
			-- Black steel shield
			{id = 15242, name = 'Black steel shield', level = 92, duration = 555, type = ITEMTYPE_SHIELD, price = 3023,
				itemList = {{id = 13571, amount = 4}, -- Forbund
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 9058, amount = 4}, -- Gold ignot
							}
				},
			-- Medusa shield
			{id = 3436, name = 'Medusa shield', level = 90, duration = 572, type = ITEMTYPE_SHIELD, price = 3308,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Sleep shield
			{id = 3434, name = 'Sleep shield', level = 96, duration = 600, type = ITEMTYPE_SHIELD, price = 4665,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Shield of invaders
			{id = 13006, name = 'Shield of invaders', level = 100, duration = 610, type = ITEMTYPE_SHIELD, price = 4760,
				itemList = {{id = 13571, amount = 5}, -- Forbund
							{id = 13582, amount = 4}, -- Magical thread
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Hellfire shield
			{id = 15282, name = 'Hellfire shield', level = 111, duration = 640, type = ITEMTYPE_SHIELD, price = 7165,
				itemList = {{id = 17124, amount = 1}, -- Mithril shard
							{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Meteorite shield
			{id = 15289, name = 'Meteorite shield', level = 116, duration = 632, type = ITEMTYPE_SHIELD, price = 6860,
				itemList = {{id = 17124, amount = 1}, -- Mithril shard
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Freeze shield
			{id = 15271, name = 'Freeze shield', level = 130, duration = 700, type = ITEMTYPE_SHIELD, price = 16275,
				itemList = {{id = 17124, amount = 3}, -- Mithril shard
							{id = 13571, amount = 4}, -- Forbund
							{id = 5887, amount = 8}, -- Royal steel
							}
				},
			-- Drakonid's shield
			{id = 3422, name = 'Drakonid\'s shield', level = 140, duration = 1000, type = ITEMTYPE_SHIELD, price = 4150,
				itemList = {{id = 17124, amount = 2}, -- Mithril shard
							{id = 13571, amount = 4}, -- Forbund
							{id = 5887, amount = 3}, -- Royal steel
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 5880, amount = 10}, -- Iron ore
							{id = 12785, amount = 10}, -- Cuprite
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Demonic shield
			{id = 16299, name = 'Demonic shield', level = 150, duration = 1920, type = ITEMTYPE_SHIELD, price = 32725,
				itemList = {{id = 17124, amount = 2}, -- Mithril shard
							{id = 13572, amount = 1}, -- Dark mithril
							{id = 12243, amount = 3}, -- Golden edge
							{id = 5887, amount = 5}, -- Royal steel
							{id = 5809, amount = 5}, -- Magical element
							}
				},
			
			-- Wiradon's spellbook
			{id = 12989, name = 'Wiradon\'s spellbook', level = 31, duration = 180, type = ITEMTYPE_SHIELD, price = 984,
				itemList = {{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Spellbook of enlightenment
			{id = 8072, name = 'Spellbook of enlightenment', level = 42, duration = 240, type = ITEMTYPE_SHIELD, price = 1515,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5809, amount = 3}, -- Magical element
							}
				},
			-- Spellbook of apprentice
			{id = 8073, name = 'Spellbook of apprentice', level = 46, duration = 255, type = ITEMTYPE_SHIELD, price = 1545,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 5892, amount = 2}, -- Steel
							{id = 3026, amount = 5}, -- White pearl
							}
				},
			-- Spellbook of mind control
			{id = 8074, name = 'Spellbook of mind control', level = 60, duration = 410, type = ITEMTYPE_SHIELD, price = 2247,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 17019, amount = 6}, -- Radiating powder
							}
				},
			-- Spellbook of lost souls
			{id = 8075, name = 'Spellbook of lost souls', level = 78, duration = 600, type = ITEMTYPE_SHIELD, price = 3060,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Spellscroll of prophecies
			{id = 8076, name = 'Spellscroll of prophecies', level = 120, duration = 1200, type = ITEMTYPE_SHIELD, price = 17955,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 10772, amount = 10}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 13582, amount = 6}, -- Magical thread
							}
				},
			-- Soulcatcher spellbook
			{id = 10816, name = 'Soulcatcher spellbook', level = 132, duration = 1160, type = ITEMTYPE_SHIELD, price = 15361,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 3026, amount = 15}, -- White pearl
							}
				},
			
			-- Skuller
			{id = 14857, name = 'Skuller', level = 2, duration = 10, type = ITEMTYPE_ONEHANDED_MELEE, price = 6,
				itemList = {{id = 5925, amount = 5}, -- Hardned bone
							}
				},
			-- Breaker
			{id = 14861, name = 'Breaker', level = 6, duration = 15, type = ITEMTYPE_ONEHANDED_MELEE, price = 15,
				itemList = {{id = 5880, amount = 2}, -- Iron ore
							{id = 5925, amount = 4}, -- Hardned bone
							}
				},
			-- Copper axe
			{id = 14868, name = 'Copper axe', level = 18, duration = 15, type = ITEMTYPE_ONEHANDED_MELEE, price = 18,
				itemList = {{id = 12832, amount = 3}, -- Rope belt
							{id = 12785, amount = 2}, -- Cuprite
							{id = 12784, amount = 3}, -- Beastly claw
							}
				},
			-- Scimitar
			{id = 14800, name = 'Scimitar', level = 24, duration = 30, type = ITEMTYPE_ONEHANDED_MELEE, price = 50,
				itemList = {{id = 12785, amount = 5}, -- Cuprite
							{id = 5880, amount = 5}, -- Iron ore
							}
				},
			-- Bronze axe
			{id = 13436, name = 'Bronze axe', level = 28, duration = 60, type = ITEMTYPE_ONEHANDED_MELEE, price = 51,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 13253, amount = 3}, -- Coal
							}
				},
			-- Chopper
			{id = 14796, name = 'Chopper', level = 31, duration = 80, type = ITEMTYPE_ONEHANDED_MELEE, price = 50,
				itemList = {{id = 3026, amount = 1}, -- White pearl
							{id = 12785, amount = 4}, -- Cuprite
							{id = 12784, amount = 5}, -- Beastly claw
							}
				},
			-- Goblin's chopper
			{id = 3274, name = 'Goblin\'s chopper', level = 35, duration = 120, type = ITEMTYPE_ONEHANDED_MELEE, price = 26,
				itemList = {{id = 13253, amount = 5}, -- Coal
							{id = 5925, amount = 5}, -- Hardned bone
							{id = 12784, amount = 5}, -- Beastly claw
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Glorious axe
			{id = 14795, name = 'Glorious axe', level = 43, duration = 136, type = ITEMTYPE_ONEHANDED_MELEE, price = 182,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 17019, amount = 1}, -- Radiating powder
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Battle hammer
			{id = 14797, name = 'Battle hammer', level = 50, duration = 190, type = ITEMTYPE_ONEHANDED_MELEE, price = 215,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 5892, amount = 1}, -- Steel
							{id = 5880, amount = 5}, -- Iron ore
							}
				},
			-- Impaler
			{id = 7435, name = 'Impaler', level = 40, duration = 120, type = ITEMTYPE_ONEHANDED_MELEE, price = 120,
				itemList = {{id = 12832, amount = 3}, -- Rope belt
							{id = 10771, amount = 1}, -- Mithril ore
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Lightning axe
			{id = 12966, name = 'Lightning axe', level = 37, duration = 140, type = ITEMTYPE_ONEHANDED_MELEE, price = 206,
				itemList = {{id = 5878, amount = 2}, -- Hardener leather
							{id = 5892, amount = 3}, -- Steel
							{id = 3027, amount = 1}, -- Black pearl
							}
				},
			-- Cranial basher
			{id = 14793, name = 'Cranial basher', level = 36, duration = 76, type = ITEMTYPE_ONEHANDED_MELEE, price = 71,
				itemList = {{id = 3040, amount = 1}, -- Gold nugget
							{id = 5880, amount = 3}, -- Iron ore
							{id = 5925, amount = 5}, -- Hardned bone
							}
				},
			-- Crimson sword
			{id = 13090, name = 'Crimson sword', level = 44, duration = 240, type = ITEMTYPE_ONEHANDED_MELEE, price = 120,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 5809, amount = 2}, -- Magical element
							{id = 12785, amount = 5}, -- Cuprite
							}
				},
			-- Steel blade
			{id = 13403, name = 'Steel blade', level = 47, duration = 240, type = ITEMTYPE_ONEHANDED_MELEE, price = 251,
				itemList = {{id = 17019, amount = 1}, -- Radiating powder
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 5892, amount = 3}, -- Steel
							}
				},
			-- Frosty rapier
			{id = 12972, name = 'Frosty rapier', level = 50, duration = 300, type = ITEMTYPE_ONEHANDED_MELEE, price = 429,
				itemList = {{id = 17019, amount = 2}, -- Radiating powder
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Trident
			{id = 13320, name = 'Trident', level = 80, duration = 600, type = ITEMTYPE_ONEHANDED_MELEE, price = 854,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Fire sword
			{id = 3280, name = 'Fire sword', level = 54, duration = 420, type = ITEMTYPE_ONEHANDED_MELEE, price = 589,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 17019, amount = 2}, -- Radiating powder
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Crusher
			{id = 13057, name = 'Crusher', level = 67, duration = 600, type = ITEMTYPE_ONEHANDED_MELEE, price = 910,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 5887, amount = 3}, -- Royal steel
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Fire axe
			{id = 12666, name = 'Fire axe', level = 55, duration = 520, type = ITEMTYPE_ONEHANDED_MELEE, price = 1370,
				itemList = {{id = 12785, amount = 50}, -- Cuprite
							{id = 5887, amount = 4}, -- Royal steel
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Black steel blade
			{id = 15239, name = 'Black steel blade', level = 75, duration = 600, type = ITEMTYPE_ONEHANDED_MELEE, price = 2785,
				itemList = {{id = 12241, amount = 2}, -- Steel edge
							{id = 13571, amount = 1}, -- Forbund
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Axe of black steel
			{id = 15237, name = 'Axe of black steel', level = 90, duration = 600, type = ITEMTYPE_ONEHANDED_MELEE, price = 1425,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 10772, amount = 1}, -- Fine fabric
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Dragon axe
			{id = 14825, name = 'Dragon axe', level = 104, duration = 660, type = ITEMTYPE_ONEHANDED_MELEE, price = 2835,
				itemList = {{id = 12241, amount = 1}, -- Steel edge
							{id = 13582, amount = 1}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Bloody crusher
			{id = 13056, name = 'Bloody crusher', level = 100, duration = 800, type = ITEMTYPE_ONEHANDED_MELEE, price = 3275,
				itemList = {{id = 12242, amount = 1}, -- Royal steel edge
							{id = 12241, amount = 1}, -- Steel edge
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Spiky squelcher
			{id = 15551, name = 'Spiky squelcher', level = 110, duration = 999, type = ITEMTYPE_ONEHANDED_MELEE, price = 1395,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 3}, -- Steel
							}
				},
			-- Frosty axe
			{id = 12967, name = 'Frosty axe', level = 80, duration = 680, type = ITEMTYPE_ONEHANDED_MELEE, price = 3290,
				itemList = {{id = 5809, amount = 12}, -- Magical element
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 10771, amount = 5}, -- Mithril ore
							}
				},
			-- Skeleton striking face
			{id = 14806, name = 'Skeleton striking face', level = 135, duration = 1500, type = ITEMTYPE_ONEHANDED_MELEE, price = 7315,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 12244, amount = 1}, -- Mithril edge
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5887, amount = 4}, -- Royal steel
							}
				},
			-- Fire hammer
			{id = 12975, name = 'Fire hammer', level = 135, duration = 1900, type = ITEMTYPE_ONEHANDED_MELEE, price = 23650,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12244, amount = 1}, -- Mithril edge
							{id = 12241, amount = 2}, -- Steel edge
							{id = 17018, amount = 4}, -- Pure mithril
							}
				},
			-- Golden axe
			{id = 15218, name = 'Golden axe', level = 88, duration = 720, type = ITEMTYPE_ONEHANDED_MELEE, price = 3365,
				itemList = {{id = 12243, amount = 4}, -- Golden edge
							{id = 5880, amount = 8}, -- Iron ore
							{id = 12832, amount = 10}, -- Rope belt
							{id = 3040, amount = 5}, -- Gold nugget
							}
				},
			-- Spiky mace
			{id = 16945, name = 'Spiky mace', level = 30, duration = 80, type = ITEMTYPE_ONEHANDED_MELEE, price = 85,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 3027, amount = 1}, -- Black pearl
							{id = 12785, amount = 3}, -- Cuprite
							}
				},
			
			-- Sword
			{id = 14871, name = 'Sword', level = 16, duration = 10, type = ITEMTYPE_TWOHANDED_MELEE, price = 15,
				itemList = {{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Copper sword
			{id = 14867, name = 'Copper sword', level = 20, duration = 20, type = ITEMTYPE_TWOHANDED_MELEE, price = 21,
				itemList = {{id = 12785, amount = 3}, -- Cuprite
							{id = 12784, amount = 1}, -- Beastly claw
							{id = 12832, amount = 3}, -- Rope belt
							}
				},
			-- Star-spoke
			{id = 14870, name = 'Star-spoke', level = 23, duration = 30, type = ITEMTYPE_TWOHANDED_MELEE, price = 57,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 5925, amount = 10}, -- Hardned bone
							}
				},
			-- Heavy axe
			{id = 13412, name = 'Heavy axe', level = 29, duration = 40, type = ITEMTYPE_TWOHANDED_MELEE, price = 48,
				itemList = {{id = 12785, amount = 5}, -- Cuprite
							{id = 5880, amount = 3}, -- Iron ore
							{id = 12832, amount = 5}, -- Rope belt
							}
				},
			-- Runed sword
			{id = 13411, name = 'Runed sword', level = 30, duration = 40, type = ITEMTYPE_TWOHANDED_MELEE, price = 98,
				itemList = {{id = 5892, amount = 2}, -- Steel
							{id = 12832, amount = 5}, -- Rope belt
							}
				},
			-- Melted axe
			{id = 16948, name = 'Melted axe', level = 32, duration = 60, type = ITEMTYPE_TWOHANDED_MELEE, price = 75,
				itemList = {{id = 12785, amount = 5}, -- Cuprite
							{id = 5880, amount = 5}, -- Iron ore
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Mantreads
			{id = 16944, name = 'Mantreads', level = 27, duration = 60, type = ITEMTYPE_TWOHANDED_MELEE, price = 213,
				itemList = {{id = 5892, amount = 4}, -- Steel
							{id = 12832, amount = 5}, -- Rope belt
							{id = 3026, amount = 1}, -- White pearl
							}
				},
			-- Longsword
			{id = 14791, name = 'Longsword', level = 30, duration = 64, type = ITEMTYPE_TWOHANDED_MELEE, price = 120,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 12785, amount = 10}, -- Cuprite
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Broadsword
			{id = 14798, name = 'Broadsword', level = 33, duration = 90, type = ITEMTYPE_TWOHANDED_MELEE, price = 163,
				itemList = {{id = 5892, amount = 3}, -- Steel
							{id = 12785, amount = 5}, -- Cuprite
							{id = 12233, amount = 3}, -- Untreated leather
							}
				},
			-- Barbarian axe
			{id = 14794, name = 'Barbarian axe', level = 37, duration = 99, type = ITEMTYPE_TWOHANDED_MELEE, price = 253,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 3040, amount = 2}, -- Gold nugget
							{id = 12832, amount = 5}, -- Rope belt
							}
				},
			-- Iron blade
			{id = 13430, name = 'Iron blade', level = 40, duration = 110, type = ITEMTYPE_TWOHANDED_MELEE, price = 281,
				itemList = {{id = 3027, amount = 2}, -- Black pearl
							{id = 10771, amount = 2}, -- Mithril ore
							{id = 5880, amount = 5}, -- Iron ore
							{id = 5925, amount = 5}, -- Hardned bone
							}
				},
			-- Ragged blade
			{id = 14799, name = 'Ragged blade', level = 38, duration = 108, type = ITEMTYPE_TWOHANDED_MELEE, price = 210,
				itemList = {{id = 5809, amount = 2}, -- Magical element
							{id = 3040, amount = 3}, -- Gold nugget
							{id = 12784, amount = 5}, -- Beastly claw
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Two handed sword
			{id = 14801, name = 'Two handed sword', level = 46, duration = 128, type = ITEMTYPE_TWOHANDED_MELEE, price = 455,
				itemList = {{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 2}, -- Steel
							{id = 3027, amount = 3}, -- Black pearl
							}
				},
			-- Double axe
			{id = 14807, name = 'Double axe', level = 50, duration = 140, type = ITEMTYPE_TWOHANDED_MELEE, price = 507,
				itemList = {{id = 5878, amount = 4}, -- Hardener leather
							{id = 9058, amount = 1}, -- Gold ignot
							{id = 5892, amount = 3}, -- Steel
							{id = 13253, amount = 5}, -- Coal
							}
				},
			-- Giant sword
			{id = 12974, name = 'Giant sword', level = 54, duration = 152, type = ITEMTYPE_TWOHANDED_MELEE, price = 680,
				itemList = {{id = 9058, amount = 1}, -- Gold ignot
							{id = 5892, amount = 2}, -- Steel
							{id = 10771, amount = 3}, -- Mithril ore
							{id = 5880, amount = 4}, -- Iron ore
							}
				},
			-- War axe
			{id = 14808, name = 'War axe', level = 58, duration = 160, type = ITEMTYPE_TWOHANDED_MELEE, price = 790,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 5878, amount = 4}, -- Hardener leather
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Minotaur hammer
			{id = 14805, name = 'Minotaur hammer', level = 60, duration = 200, type = ITEMTYPE_TWOHANDED_MELEE, price = 2550,
				itemList = {{id = 12241, amount = 2}, -- Steel edge
							{id = 5887, amount = 4}, -- Royal steel
							{id = 3040, amount = 1}, -- Gold nugget
							}
				},
			-- Shaburak club
			{id = 3336, name = 'Shaburak club', level = 64, duration = 240, type = ITEMTYPE_TWOHANDED_MELEE, price = 2614,
				itemList = {{id = 12242, amount = 1}, -- Royal steel edge
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 3026, amount = 2}, -- White pearl
							}
				},
			-- Haunted blade
			{id = 12964, name = 'Haunted blade', level = 54, duration = 255, type = ITEMTYPE_TWOHANDED_MELEE, price = 2233,
				itemList = {{id = 12241, amount = 1}, -- Steel edge
							{id = 13571, amount = 2}, -- Forbund
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Devastator
			{id = 14811, name = 'Devastator', level = 62, duration = 280, type = ITEMTYPE_TWOHANDED_MELEE, price = 2500,
				itemList = {{id = 17018, amount = 3}, -- Pure mithril
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Knight axe
			{id = 14810, name = 'Knight axe', level = 60, duration = 300, type = ITEMTYPE_TWOHANDED_MELEE, price = 2313,
				itemList = {{id = 13582, amount = 4}, -- Magical thread
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Twiceslicer
			{id = 10810, name = 'Twiceslicer', level = 62, duration = 318, type = ITEMTYPE_TWOHANDED_MELEE, price = 2258,
				itemList = {{id = 17018, amount = 3}, -- Pure mithril
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 5892, amount = 5}, -- Steel
							}
				},
			-- Deepling axe
			{id = 3315, name = 'Deepling axe', level = 74, duration = 380, type = ITEMTYPE_TWOHANDED_MELEE, price = 3193,
				itemList = {{id = 12243, amount = 1}, -- Golden edge
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 5892, amount = 5}, -- Steel
							}
				},
			-- Hammer of black steel
			{id = 15240, name = 'Hammer of black steel', level = 70, duration = 400, type = ITEMTYPE_TWOHANDED_MELEE, price = 2413,
				itemList = {{id = 12242, amount = 1}, -- Royal steel edge
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 7}, -- Radiating powder
							{id = 5878, amount = 5}, -- Hardener leather
							}
				},
			-- Vile axe
			{id = 14812, name = 'Vile axe', level = 90, duration = 560, type = ITEMTYPE_TWOHANDED_MELEE, price = 7680,
				itemList = {{id = 12243, amount = 2}, -- Golden edge
							{id = 12241, amount = 1}, -- Steel edge
							{id = 5887, amount = 6}, -- Royal steel
							{id = 5892, amount = 6}, -- Steel
							}
				},
			-- Daramanian axe
			{id = 3328, name = 'Daramanian axe', level = 82, duration = 544, type = ITEMTYPE_TWOHANDED_MELEE, price = 4507,
				itemList = {{id = 12242, amount = 2}, -- Royal steel edge
							{id = 13571, amount = 1}, -- Forbund
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 17019, amount = 2}, -- Radiating powder
							}
				},
			-- Drakinata
			{id = 10388, name = 'Drakinata', level = 81, duration = 560, type = ITEMTYPE_TWOHANDED_MELEE, price = 4296,
				itemList = {{id = 12242, amount = 1}, -- Royal steel edge
							{id = 12241, amount = 1}, -- Steel edge
							{id = 17019, amount = 8}, -- Radiating powder
							{id = 9058, amount = 4}, -- Gold ignot
							}
				},
			-- War hammer
			{id = 3279, name = 'War hammer', level = 89, duration = 588, type = ITEMTYPE_TWOHANDED_MELEE, price = 2532,
				itemList = {{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 7}, -- Fine fabric
							{id = 5892, amount = 4}, -- Steel
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Berserker
			{id = 14802, name = 'Berserker', level = 92, duration = 600, type = ITEMTYPE_TWOHANDED_MELEE, price = 3665,
				itemList = {{id = 12241, amount = 3}, -- Steel edge
							{id = 9058, amount = 1}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 5}, -- Steel
							}
				},
			-- Bastard Sword
			{id = 12977, name = 'Bastard Sword', level = 110, duration = 900, type = ITEMTYPE_TWOHANDED_MELEE, price = 16049,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 13571, amount = 3}, -- Forbund
							{id = 5878, amount = 8}, -- Hardener leather
							}
				},
			-- Bloody axe
			{id = 13031, name = 'Bloody axe', level = 98, duration = 850, type = ITEMTYPE_TWOHANDED_MELEE, price = 12067,
				itemList = {{id = 12244, amount = 1}, -- Mithril edge
							{id = 12242, amount = 3}, -- Royal steel edge
							{id = 10772, amount = 6}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Meteorite hammer
			{id = 15288, name = 'Meteorite hammer', level = 130, duration = 1000, type = ITEMTYPE_TWOHANDED_MELEE, price = 35155,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 12244, amount = 2}, -- Mithril edge
							{id = 12243, amount = 3}, -- Golden edge
							{id = 5892, amount = 8}, -- Steel
							}
				},
			-- Axe of hellstorm
			{id = 12971, name = 'Axe of hellstorm', level = 132, duration = 1200, type = ITEMTYPE_TWOHANDED_MELEE, price = 26082,
				itemList = {{id = 12244, amount = 3}, -- Mithril edge
							{id = 12243, amount = 2}, -- Golden edge
							{id = 17018, amount = 4}, -- Pure mithril
							{id = 10772, amount = 6}, -- Fine fabric
							}
				},
			
			-- Blade
			{id = 3267, name = 'Blade', level = 8, duration = 5, type = ITEMTYPE_ONEHANDED_MELEE, price = 5,
				itemList = {{id = 5880, amount = 1}, -- Iron ore
							}
				},
			-- Killer's blade
			{id = 14854, name = 'Killer\'s blade', level = 20, duration = 10, type = ITEMTYPE_ONEHANDED_MELEE, price = 8,
				itemList = {{id = 5880, amount = 1}, -- Iron ore
							{id = 5925, amount = 1}, -- Hardned bone
							{id = 12784, amount = 1}, -- Beastly claw
							}
				},
			-- Copper blade
			{id = 16734, name = 'Copper blade', level = 26, duration = 20, type = ITEMTYPE_ONEHANDED_MELEE, price = 16,
				itemList = {{id = 12785, amount = 2}, -- Cuprite
							{id = 5925, amount = 5}, -- Hardned bone
							}
				},
			-- Rat tusk
			{id = 16302, name = 'Rat tusk', level = 30, duration = 120, type = ITEMTYPE_ONEHANDED_MELEE, price = 187,
				itemList = {{id = 12784, amount = 50}, -- Beastly claw
							{id = 17019, amount = 1}, -- Radiating powder
							{id = 5892, amount = 2}, -- Steel
							}
				},
			
			
			-- Bow
			{id = 3350, name = 'Bow', level = 5, duration = 10, type = ITEMTYPE_BOW, price = 9,
				itemList = {{id = 12832, amount = 5}, -- Rope belt
							{id = 5925, amount = 1}, -- Hardned bone
							}
				},
			-- Long bow
			{id = 14816, name = 'Long bow', level = 15, duration = 30, type = ITEMTYPE_BOW, price = 26,
				itemList = {{id = 12785, amount = 4}, -- Cuprite
							{id = 12784, amount = 6}, -- Beastly claw
							}
				},
			-- Short bow
			{id = 13093, name = 'Short bow', level = 19, duration = 45, type = ITEMTYPE_BOW, price = 66,
				itemList = {{id = 5878, amount = 2}, -- Hardener leather
							{id = 5880, amount = 4}, -- Iron ore
							}
				},
			-- Massive wooden bow
			{id = 10486, name = 'Massive wooden bow', level = 26, duration = 52, type = ITEMTYPE_BOW, price = 154,
				itemList = {{id = 10772, amount = 3}, -- Fine fabric
							{id = 12785, amount = 4}, -- Cuprite
							}
				},
			-- Massive ornamented bow
			{id = 14687, name = 'Massive ornamented bow', level = 30, duration = 76, type = ITEMTYPE_BOW, price = 177,
				itemList = {{id = 5878, amount = 4}, -- Hardener leather
							{id = 3027, amount = 3}, -- Black pearl
							{id = 5880, amount = 2}, -- Iron ore
							}
				},
			-- Snake bow
			{id = 14838, name = 'Snake bow', level = 52, duration = 260, type = ITEMTYPE_BOW, price = 1480,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Bone bow
			{id = 14815, name = 'Bone bow', level = 44, duration = 225, type = ITEMTYPE_BOW, price = 1218,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Skeletor bow
			{id = 14818, name = 'Skeletor bow', level = 80, duration = 460, type = ITEMTYPE_BOW, price = 2605,
				itemList = {{id = 12241, amount = 2}, -- Steel edge
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 1}, -- Royal steel
							}
				},
			-- Skullcrusher bow
			{id = 14685, name = 'Skullcrusher bow', level = 52, duration = 388, type = ITEMTYPE_BOW, price = 1805,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 13571, amount = 1}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							}
				},
			-- Frosty bow
			{id = 12969, name = 'Frosty bow', level = 40, duration = 365, type = ITEMTYPE_BOW, price = 994,
				itemList = {{id = 17019, amount = 2}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Composite bow
			{id = 12970, name = 'Composite bow', level = 70, duration = 620, type = ITEMTYPE_BOW, price = 3640,
				itemList = {{id = 12243, amount = 1}, -- Golden edge
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Swamplair bow
			{id = 15247, name = 'Swamplair bow', level = 96, duration = 800, type = ITEMTYPE_BOW, price = 5665,
				itemList = {{id = 12242, amount = 2}, -- Royal steel edge
							{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 1}, -- Pure mithril
							}
				},
			-- Reflective bow
			{id = 14819, name = 'Reflective bow', level = 92, duration = 798, type = ITEMTYPE_BOW, price = 5613,
				itemList = {{id = 12241, amount = 3}, -- Steel edge
							{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Necrotic bow
			{id = 14817, name = 'Necrotic bow', level = 62, duration = 555, type = ITEMTYPE_BOW, price = 3085,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Black steel bow
			{id = 15238, name = 'Black steel bow', level = 134, duration = 1200, type = ITEMTYPE_BOW, price = 44475,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12244, amount = 1}, -- Mithril edge
							{id = 12242, amount = 5}, -- Royal steel edge
							}
				},
			-- Silkweaver bow
			{id = 8029, name = 'Silkweaver bow', level = 108, duration = 1000, type = ITEMTYPE_BOW, price = 9015,
				itemList = {{id = 12244, amount = 1}, -- Mithril edge
							{id = 13571, amount = 3}, -- Forbund
							{id = 13582, amount = 4}, -- Magical thread
							}
				},
			-- Black forest bow
			{id = 15509, name = 'Black forest bow', level = 124, duration = 1160, type = ITEMTYPE_BOW, price = 32207,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12243, amount = 1}, -- Golden edge
							{id = 10772, amount = 6}, -- Fine fabric
							}
				},
			-- Fiery bow
			{id = 10888, name = 'Fiery bow', level = 120, duration = 1108, type = ITEMTYPE_BOW, price = 30332,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 6}, -- Radiating powder
							}
				},
			-- Hellfire bow
			{id = 10757, name = 'Hellfire bow', level = 58, duration = 500, type = ITEMTYPE_BOW, price = 5960,
				itemList = {{id = 13571, amount = 5}, -- Forbund
							{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 4}, -- Pure mithril
							}
				},
			-- Elvish bow
			{id = 7438, name = 'Elvish bow', level = 39, duration = 322, type = ITEMTYPE_BOW, price = 1480,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			
			-- Crossbow
			{id = 3349, name = 'Crossbow', level = 6, duration = 10, type = ITEMTYPE_CROSSBOW, price = 19,
				itemList = {{id = 12832, amount = 5}, -- Rope belt
							{id = 5880, amount = 2}, -- Iron ore
							{id = 5925, amount = 1}, -- Hardned bone
							}
				},
			-- Heavy crossbow
			{id = 10488, name = 'Heavy crossbow', level = 30, duration = 120, type = ITEMTYPE_CROSSBOW, price = 240,
				itemList = {{id = 12832, amount = 20}, -- Rope belt
							{id = 5892, amount = 3}, -- Steel
							{id = 12785, amount = 5}, -- Cuprite
							{id = 3026, amount = 2}, -- White pearl
							}
				},
			-- Modified crossbow
			{id = 8021, name = 'Modified crossbow', level = 34, duration = 150, type = ITEMTYPE_CROSSBOW, price = 184,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 5892, amount = 1}, -- Steel
							{id = 3027, amount = 2}, -- Black pearl
							}
				},
			-- Repeating crossbow
			{id = 13020, name = 'Repeating crossbow', level = 40, duration = 220, type = ITEMTYPE_CROSSBOW, price = 201,
				itemList = {{id = 5887, amount = 1}, -- Royal steel
							{id = 5878, amount = 2}, -- Hardener leather
							{id = 5925, amount = 8}, -- Hardned bone
							}
				},
			-- Snake crossbow
			{id = 13018, name = 'Snake crossbow', level = 50, duration = 265, type = ITEMTYPE_CROSSBOW, price = 366,
				itemList = {{id = 5892, amount = 3}, -- Steel
							{id = 5809, amount = 5}, -- Magical element
							{id = 3040, amount = 2}, -- Gold nugget
							{id = 13253, amount = 3}, -- Coal
							}
				},
			-- Royal steel crossbow
			{id = 13019, name = 'Royal steel crossbow', level = 56, duration = 280, type = ITEMTYPE_CROSSBOW, price = 493,
				itemList = {{id = 9058, amount = 1}, -- Gold ignot
							{id = 12832, amount = 5}, -- Rope belt
							{id = 10771, amount = 2}, -- Mithril ore
							{id = 12785, amount = 3}, -- Cuprite
							}
				},
			-- Icey crossbow
			{id = 13015, name = 'Icey crossbow', level = 60, duration = 330, type = ITEMTYPE_CROSSBOW, price = 1274,
				itemList = {{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Hellfire crossbow
			{id = 13017, name = 'Hellfire crossbow', level = 64, duration = 420, type = ITEMTYPE_CROSSBOW, price = 1769,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 17019, amount = 7}, -- Radiating powder
							}
				},
			-- Thunderstorm crossbow
			{id = 13016, name = 'Thunderstorm crossbow', level = 69, duration = 500, type = ITEMTYPE_CROSSBOW, price = 2550,
				itemList = {{id = 12241, amount = 2}, -- Steel edge
							{id = 13571, amount = 1}, -- Forbund
							{id = 10771, amount = 2}, -- Mithril ore
							}
				},
			-- Light crossbow
			{id = 13002, name = 'Light crossbow', level = 72, duration = 590, type = ITEMTYPE_CROSSBOW, price = 4243,
				itemList = {{id = 12243, amount = 1}, -- Golden edge
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							}
				},
			-- Arbalest
			{id = 5803, name = 'Arbalest', level = 75, duration = 600, type = ITEMTYPE_CROSSBOW, price = 4938,
				itemList = {{id = 12242, amount = 2}, -- Royal steel edge
							{id = 13582, amount = 2}, -- Magical thread
							{id = 9058, amount = 1}, -- Gold ignot
							{id = 12832, amount = 5}, -- Rope belt
							}
				},
			-- Deepling crossbow
			{id = 14826, name = 'Deepling crossbow', level = 84, duration = 626, type = ITEMTYPE_CROSSBOW, price = 5510,
				itemList = {{id = 12242, amount = 2}, -- Royal steel edge
							{id = 12241, amount = 1}, -- Steel edge
							{id = 13571, amount = 1}, -- Forbund
							{id = 5892, amount = 8}, -- Steel
							}
				},
			-- Incendiary heavy crossbow
			{id = 8022, name = 'Incendiary heavy crossbow', level = 120, duration = 900, type = ITEMTYPE_CROSSBOW, price = 102750,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12244, amount = 1}, -- Mythril blade
							{id = 3026, amount = 10}, -- White pearl
							{id = 3027, amount = 10}, -- Black pearl
							{id = 12832, amount = 8}, -- Rope belt
							{id = 10771, amount = 5}, -- Mithril ore
							{id = 12785, amount = 14}, -- Cuprite
							}
				},
			-- Ballista
			{id = 12803, name = 'Ballista', level = 115, duration = 720, type = ITEMTYPE_CROSSBOW, price = 102750,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 12244, amount = 2}, -- Mythril blade
							{id = 17018, amount = 4}, -- Pure mithril
							{id = 13582, amount = 4}, -- Magical thread
							{id = 5809, amount = 10}, -- Magical element
							}
				},
			-- Siege crossbow
			{id = 8025, name = 'Siege crossbow', level = 130, duration = 800, type = ITEMTYPE_CROSSBOW, price = 103125,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12244, amount = 1}, -- Mythril blade
							{id = 5809, amount = 10}, -- Magical element
							{id = 10771, amount = 5}, -- Mithril ore
							{id = 13571, amount = 4}, -- Forbund
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5892, amount = 9}, -- Steel
							}
				},
			-- Marksman crossbow
			{id = 12035, name = 'Marksman crossbow', level = 130, duration = 990, type = ITEMTYPE_CROSSBOW, price = 103125,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12244, amount = 2}, -- Mythril blade
							{id = 10771, amount = 8}, -- Mithril ore
							{id = 13571, amount = 4}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 5887, amount = 6}, -- Royal steel
							}
				},
			
			-- Lampart spear
			{id = 12040, name = 'Lampart spear', level = 8, duration = 5, type = ITEMTYPE_SPEAR, price = 6,
				itemList = {{id = 12233, amount = 1}, -- Untreated leather
							{id = 5925, amount = 2}, -- Hardned bone
							{id = 12784, amount = 2}, -- Beastly claw
							}
				},
			-- Steel spear
			{id = 14860, name = 'Steel spear', level = 22, duration = 120, type = ITEMTYPE_SPEAR, price = 70,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Hunting spear
			{id = 13026, name = 'Hunting spear', level = 28, duration = 132, type = ITEMTYPE_SPEAR, price = 115,
				itemList = {{id = 5892, amount = 2}, -- Steel
							{id = 12785, amount = 3}, -- Cuprite
							{id = 5880, amount = 2}, -- Iron ore
							}
				},
			-- Crystal spear
			{id = 13030, name = 'Crystal spear', level = 48, duration = 240, type = ITEMTYPE_SPEAR, price = 260,
				itemList = {{id = 10771, amount = 2}, -- Mithril ore
							{id = 3040, amount = 1}, -- Gold nugget
							{id = 12784, amount = 5}, -- Beastly claw
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- Sanctified spear
			{id = 16806, name = 'Sanctified spear', level = 56, duration = 300, type = ITEMTYPE_SPEAR, price = 850,
				itemList = {{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5809, amount = 3}, -- Magical element
							}
				},
			-- Cursed spear
			{id = 12670, name = 'Cursed spear', level = 60, duration = 310, type = ITEMTYPE_SPEAR, price = 993,
				itemList = {{id = 5878, amount = 6}, -- Hardener leather
							{id = 5887, amount = 4}, -- Royal steel
							{id = 3027, amount = 3}, -- Black pearl
							{id = 10771, amount = 2}, -- Mithril ore
							}
				},
			-- Hellfire spear
			{id = 13029, name = 'Hellfire spear', level = 77, duration = 360, type = ITEMTYPE_SPEAR, price = 1363,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 5892, amount = 3}, -- Steel
							{id = 3026, amount = 6}, -- White pearl
							}
				},
			-- Frosty spear
			{id = 13028, name = 'Frosty spear', level = 86, duration = 400, type = ITEMTYPE_SPEAR, price = 2970,
				itemList = {{id = 12241, amount = 1}, -- Steel edge
							{id = 13571, amount = 3}, -- Forbund
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Golden spear
			{id = 13027, name = 'Golden spear', level = 110, duration = 560, type = ITEMTYPE_SPEAR, price = 16139,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 13582, amount = 3}, -- Magical thread
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 5809, amount = 5}, -- Magical element
							}
				},
			-- Ruby spear
			{id = 15252, name = 'Ruby spear', level = 132, duration = 900, type = ITEMTYPE_SPEAR, price = 37070,
				itemList = {{id = 13572, amount = 2}, -- Dark mithril
							{id = 12244, amount = 1}, -- Mythril blade
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 3026, amount = 6}, -- White pearl
							}
				},
			
			-- Red dragon wand
			{id = 8093, name = 'Red dragon wand', level = 100, duration = 718, type = ITEMTYPE_WAND, price = 2194,
				itemList = {{id = 12242, amount = 1}, -- Royal steel edge
							{id = 12785, amount = 4}, -- Cuprite
							{id = 17019, amount = 2}, -- Radiating powder
							{id = 3026, amount = 4}, -- White pearl
							{id = 10771, amount = 1}, -- Mithril ore
							}
				},
			-- Wand of draconia
			{id = 13527, name = 'Wand of draconia', level = 25, duration = 60, type = ITEMTYPE_WAND, price = 125,
				itemList = {{id = 3026, amount = 2}, -- White pearl
							{id = 3027, amount = 2}, -- Black pearl
							{id = 5880, amount = 5}, -- Iron ore
							}
				},
			-- Wiradon's staff
			{id = 12988, name = 'Wiradon\'s staff', level = 30, duration = 78, type = ITEMTYPE_WAND, price = 243,
				itemList = {{id = 17019, amount = 4}, -- Radiating powder
							{id = 5892, amount = 1}, -- Steel
							{id = 13253, amount = 5}, -- Coal
							}
				},
			-- Necrotic rod
			{id = 13526, name = 'Necrotic rod', level = 28, duration = 64, type = ITEMTYPE_WAND, price = 155,
				itemList = {{id = 5809, amount = 2}, -- Magical element
							{id = 3040, amount = 2}, -- Gold nugget
							{id = 5925, amount = 4}, -- Hardned bone
							}
				},
			-- Holy staff of the day
			{id = 15372, name = 'Holy staff of the day', level = 110, duration = 1500, type = ITEMTYPE_WAND, price = 16285,
				itemList = {{id = 13572, amount = 1}, -- Dark mithril
							{id = 13582, amount = 2}, -- Magical thread
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Ethereal staff
			{id = 17151, name = 'Ethereal staff', level = 46, duration = 290, type = ITEMTYPE_WAND, price = 1087,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 5878, amount = 2}, -- Hardener leather
							}
				},
			-- Burning stick
			{id = 17150, name = 'Burning stick', level = 48, duration = 360, type = ITEMTYPE_WAND, price = 1498,
				itemList = {{id = 17018, amount = 2}, -- Pure mithril
							{id = 10772, amount = 5}, -- Fine fabric
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Magic rod
			{id = 13537, name = 'Magic rod', level = 32, duration = 300, type = ITEMTYPE_WAND, price = 120,
				itemList = {{id = 5809, amount = 2}, -- Magical element
							{id = 12785, amount = 4}, -- Cuprite
							{id = 3040, amount = 1}, -- Gold nugget
							}
				},
			-- Ice cross
			{id = 13529, name = 'Ice cross', level = 40, duration = 310, type = ITEMTYPE_WAND, price = 150,
				itemList = {{id = 3026, amount = 3}, -- White pearl
							{id = 3027, amount = 2}, -- Black pearl
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Thunder rod
			{id = 3074, name = 'Thunder rod', level = 44, duration = 325, type = ITEMTYPE_WAND, price = 175,
				itemList = {{id = 17019, amount = 2}, -- Radiating powder
							{id = 3027, amount = 1}, -- Black pearl
							{id = 12832, amount = 4}, -- Rope belt
							{id = 3040, amount = 1}, -- Gold nugget
							}
				},
			-- Rider of light
			{id = 12076, name = 'Rider of light', level = 50, duration = 400, type = ITEMTYPE_WAND, price = 235,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 5878, amount = 2}, -- Hardener leather
							{id = 10771, amount = 1}, -- Mithril ore
							}
				},
			-- Swamplair rod
			{id = 13531, name = 'Swamplair rod', level = 58, duration = 444, type = ITEMTYPE_WAND, price = 770,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Wand of inferno
			{id = 3071, name = 'Wand of inferno', level = 55, duration = 426, type = ITEMTYPE_WAND, price = 376,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 5892, amount = 2}, -- Steel
							{id = 3027, amount = 4}, -- Black pearl
							}
				},
			-- Staff of wind
			{id = 13530, name = 'Staff of wind', level = 48, duration = 400, type = ITEMTYPE_WAND, price = 135,
				itemList = {{id = 5809, amount = 5}, -- Magical element
							{id = 12784, amount = 5}, -- Beastly claw
							{id = 12233, amount = 5}, -- Untreated leather
							}
				},
			-- The frosty midnight fire
			{id = 16782, name = 'The frosty midnight fire', level = 70, duration = 690, type = ITEMTYPE_WAND, price = 809,
				itemList = {{id = 17018, amount = 1}, -- Pure mithril
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 3026, amount = 5}, -- White pearl
							}
				},
			-- The magic edge of life
			{id = 14849, name = 'The magic edge of life', level = 90, duration = 900, type = ITEMTYPE_WAND, price = 1437,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 10772, amount = 6}, -- Fine fabric
							{id = 9058, amount = 1}, -- Gold ignot
							}
				},
			-- Sapphire rod
			{id = 13524, name = 'Sapphire rod', level = 98, duration = 996, type = ITEMTYPE_WAND, price = 2005,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- The creeper of darkness
			{id = 13533, name = 'The creeper of darkness', level = 72, duration = 800, type = ITEMTYPE_WAND, price = 1105,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- A foul rod
			{id = 12663, name = 'A foul rod', level = 76, duration = 720, type = ITEMTYPE_WAND, price = 1383,
				itemList = {{id = 17018, amount = 1}, -- Pure mithril
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 5887, amount = 4}, -- Royal steel
							}
				},
			-- Burning stick
			{id = 17150, name = 'Burning stick', level = 40, duration = 480, type = ITEMTYPE_WAND, price = 450,
				itemList = {{id = 3027, amount = 4}, -- Black pearl
							{id = 5809, amount = 2}, -- Magical element
							{id = 10771, amount = 3}, -- Mithril ore
							}
				},
			-- Spruce staff of destruction
			{id = 13535, name = 'Spruce staff of destruction', level = 90, duration = 900, type = ITEMTYPE_WAND, price = 2530,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- A crystal rod of frost
			{id = 15988, name = 'A crystal rod of frost', level = 72, duration = 720, type = ITEMTYPE_WAND, price = 1390,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 1}, -- Gold ignot
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Electric rod
			{id = 17149, name = 'Electric rod', level = 81, duration = 864, type = ITEMTYPE_WAND, price = 1680,
				itemList = {{id = 17018, amount = 2}, -- Pure mithril
							{id = 10771, amount = 3}, -- Mithril ore
							{id = 3040, amount = 3}, -- Gold nugget
							}
				},
			-- A dirty eye of despair
			{id = 15226, name = 'A dirty eye of despair', level = 80, duration = 855, type = ITEMTYPE_WAND, price = 1643,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 5878, amount = 5}, -- Hardener leather
							}
				},
			-- Oak staff of the earth
			{id = 13534, name = 'Oak staff of the earth', level = 91, duration = 966, type = ITEMTYPE_WAND, price = 1920,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Sickle of the wind
			{id = 15987, name = 'Sickle of the wind', level = 100, duration = 1000, type = ITEMTYPE_WAND, price = 2490,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 5878, amount = 5}, -- Hardener leather
							}
				},
			-- Death enchanted in the staff
			{id = 16239, name = 'Death enchanted in the staff', level = 102, duration = 1180, type = ITEMTYPE_WAND, price = 2670,
				itemList = {{id = 12241, amount = 1}, -- Steel edge
							{id = 13571, amount = 2}, -- Forbund
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Sphere frost
			{id = 10699, name = 'Sphere frost', level = 85, duration = 912, type = ITEMTYPE_WAND, price = 1800,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 9058, amount = 1}, -- Gold ignot
							{id = 5892, amount = 4}, -- Steel
							}
				},
			-- Staff of powerful energy
			{id = 13528, name = 'Staff of powerful energy', level = 108, duration = 1200, type = ITEMTYPE_WAND, price = 2755,
				itemList = {{id = 13571, amount = 3}, -- Forbund
							{id = 17018, amount = 2}, -- Pure mithril
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- A staff of eternal fire
			{id = 13525, name = 'A staff of eternal fire', level = 74, duration = 790, type = ITEMTYPE_WAND, price = 1289,
				itemList = {{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 9058, amount = 2}, -- Gold ignot
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Storm staff
			{id = 8092, name = 'Storm staff', level = 93, duration = 1000, type = ITEMTYPE_WAND, price = 1749,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 5887, amount = 1}, -- Royal steel
							}
				},
			
			-- Energy ring
			{id = 3051, name = 'Energy ring', level = 25, duration = 40, type = ITEMTYPE_RING, price = 120,
				itemList = {{id = 5892, amount = 1}, -- Steel
							{id = 5809, amount = 2}, -- Magical element
							{id = 12785, amount = 5}, -- Cuprite
							}
				},
			-- Might ring
			{id = 3048, name = 'Might ring', level = 45, duration = 120, type = ITEMTYPE_RING, price = 289,
				itemList = {{id = 17019, amount = 2}, -- Radiating powder
							{id = 5887, amount = 1}, -- Royal steel
							{id = 3026, amount = 2}, -- White pearl
							}
				},
			-- Sapphire ring
			{id = 14820, name = 'Sapphire ring', level = 50, duration = 300, type = ITEMTYPE_RING, price = 885,
				itemList = {{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 1}, -- Royal steel
							{id = 3026, amount = 5}, -- White pearl
							{id = 3027, amount = 3}, -- Black pearl
							}
				},
			-- Ruby ring
			{id = 349, name = 'Ruby ring', level = 50, duration = 300, type = ITEMTYPE_RING, price = 885,
				itemList = {{id = 9058, amount = 2}, -- Gold ignot
							{id = 5887, amount = 1}, -- Royal steel
							{id = 3026, amount = 5}, -- White pearl
							{id = 3027, amount = 3}, -- Black pearl
							}
				},
			-- Ring with diamond
			{id = 3006, name = 'Ring with diamond', level = 90, duration = 550, type = ITEMTYPE_RING, price = 1649,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 3}, -- Fine fabric
							}
				},
			-- Emerald bangle
			{id = 3010, name = 'Emerald bangle', level = 48, duration = 300, type = ITEMTYPE_RING, price = 530,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 5809, amount = 2}, -- Magical element
							{id = 3040, amount = 1}, -- Gold nugget
							}
				},
			-- Shiny ring
			{id = 13520, name = 'Shiny ring', level = 68, duration = 416, type = ITEMTYPE_RING, price = 2120,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 13582, amount = 1}, -- Magical thread
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Brass ring
			{id = 3004, name = 'Brass ring', level = 40, duration = 200, type = ITEMTYPE_RING, price = 1053,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 17019, amount = 4}, -- Radiating powder
							{id = 5887, amount = 3}, -- Royal steel
							}
				},
			-- Time ring
			{id = 3053, name = 'Time ring', level = 25, duration = 90, type = ITEMTYPE_RING, price = 165,
				itemList = {{id = 3026, amount = 2}, -- White pearl
							{id = 5880, amount = 3}, -- Iron ore
							{id = 3027, amount = 4}, -- Black pearl
							}
				},
			-- Dwarven ring
			{id = 3097, name = 'Dwarven ring', level = 32, duration = 100, type = ITEMTYPE_RING, price = 130,
				itemList = {{id = 5892, amount = 2}, -- Steel
							{id = 3027, amount = 1}, -- Black pearl
							{id = 5880, amount = 3}, -- Iron ore
							}
				},
			-- Life ring
			{id = 3052, name = 'Life ring', level = 20, duration = 40, type = ITEMTYPE_RING, price = 150,
				itemList = {{id = 3026, amount = 2}, -- White pearl
							{id = 3027, amount = 2}, -- Black pearl
							{id = 5809, amount = 2}, -- Magical element
							}
				},
			-- Ring of endurance
			{id = 3098, name = 'Ring of endurance', level = 42, duration = 190, type = ITEMTYPE_RING, price = 621,
				itemList = {{id = 13571, amount = 1}, -- Forbund
							{id = 17019, amount = 3}, -- Radiating powder
							{id = 3027, amount = 2}, -- Black pearl
							}
				},
			-- Stealth ring
			{id = 3049, name = 'Stealth ring', level = 70, duration = 290, type = ITEMTYPE_RING, price = 898,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 4}, -- Fine fabric
							{id = 9058, amount = 1}, -- Gold ignot
							}
				},
			
			-- Star amulet
			{id = 3014, name = 'Star amulet', level = 45, duration = 80, type = ITEMTYPE_NECKLACE, price = 245,
				itemList = {{id = 3040, amount = 1}, -- Gold nugget
							{id = 5887, amount = 1}, -- Royal steel
							{id = 3026, amount = 2}, -- White pearl
							}
				},
			-- Protection amulet
			{id = 3084, name = 'Protection amulet', level = 26, duration = 60, type = ITEMTYPE_NECKLACE, price = 160,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 5809, amount = 4}, -- Magical element
							{id = 17019, amount = 1}, -- Radiating powder
							{id = 3026, amount = 4}, -- White pearl
							}
				},
			-- Toxic amulet
			{id = 814, name = 'Toxic amulet', level = 24, duration = 60, type = ITEMTYPE_NECKLACE, price = 100,
				itemList = {{id = 3026, amount = 1}, -- White pearl
							{id = 3027, amount = 1}, -- Black pearl
							{id = 5809, amount = 2}, -- Magical element
							}
				},
			-- Wailing widow's necklace
			{id = 10412, name = 'Wailing widow\'s necklace', level = 34, duration = 72, type = ITEMTYPE_NECKLACE, price = 197,
				itemList = {{id = 3040, amount = 2}, -- Gold nugget
							{id = 3027, amount = 2}, -- Black pearl
							{id = 17019, amount = 1}, -- Radiating powder
							}
				},
			-- Beetle necklace
			{id = 10457, name = 'Beetle necklace', level = 38, duration = 80, type = ITEMTYPE_NECKLACE, price = 158,
				itemList = {{id = 10772, amount = 2}, -- Fine fabric
							{id = 5878, amount = 3}, -- Hardener leather
							}
				},
			-- Platinum amulet
			{id = 3055, name = 'Platinum amulet', level = 40, duration = 98, type = ITEMTYPE_NECKLACE, price = 180,
				itemList = {{id = 3026, amount = 1}, -- White pearl
							{id = 5880, amount = 2}, -- Iron ore
							{id = 5887, amount = 1}, -- Royal steel
							}
				},
			-- Scarf
			{id = 3572, name = 'Scarf', level = 5, duration = 16, type = ITEMTYPE_NECKLACE, price = 31,
				itemList = {{id = 12832, amount = 4}, -- Rope belt
							{id = 12785, amount = 5}, -- Cuprite
							}
				},
			-- Alpha shanir
			{id = 13421, name = 'Alpha shanir', level = 30, duration = 60, type = ITEMTYPE_NECKLACE, price = 100,
				itemList = {{id = 5809, amount = 2}, -- Magical element
							{id = 3027, amount = 1}, -- Black pearl
							{id = 3026, amount = 1}, -- White pearl
							}
				},
			-- Wolf amulet
			{id = 12994, name = 'Wolf amulet', level = 40, duration = 98, type = ITEMTYPE_NECKLACE, price = 118,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 5878, amount = 3}, -- Hardener leather
							{id = 12832, amount = 3}, -- Rope belt
							}
				},
			-- Wolf tooth chain
			{id = 3012, name = 'Wolf tooth chain', level = 19, duration = 30, type = ITEMTYPE_NECKLACE, price = 23,
				itemList = {{id = 13253, amount = 4}, -- Coal
							{id = 5880, amount = 2}, -- Iron ore
							{id = 12784, amount = 5}, -- Beastly claw
							}
				},
			-- Luck amulet
			{id = 10476, name = 'Luck amulet', level = 110, duration = 900, type = ITEMTYPE_NECKLACE, price = 3735,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 3}, -- Pure mithril
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Beta shanir
			{id = 816, name = 'Beta shanir', level = 70, duration = 410, type = ITEMTYPE_NECKLACE, price = 1180,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Ruby amulet
			{id = 3016, name = 'Ruby amulet', level = 46, duration = 300, type = ITEMTYPE_NECKLACE, price = 1135,
				itemList = {{id = 13582, amount = 2}, -- Magical thread
							{id = 17019, amount = 5}, -- Radiating powder
							}
				},
			-- Silver necklace
			{id = 3015, name = 'Silver necklace', level = 34, duration = 225, type = ITEMTYPE_NECKLACE, price = 632,
				itemList = {{id = 9058, amount = 2}, -- Gold ignot
							{id = 5878, amount = 4}, -- Hardener leather
							}
				},
			-- Sapphire necklace
			{id = 3021, name = 'Sapphire necklace', level = 31, duration = 212, type = ITEMTYPE_NECKLACE, price = 584,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 2}, -- Fine fabric
							{id = 5892, amount = 1}, -- Steel
							}
				},
			-- Shockwave amulet
			{id = 9304, name = 'Shockwave amulet', level = 50, duration = 333, type = ITEMTYPE_NECKLACE, price = 725,
				itemList = {{id = 13582, amount = 1}, -- Magical thread
							{id = 10772, amount = 3}, -- Fine fabric
							{id = 17019, amount = 3}, -- Radiating powder
							}
				},
			-- Leviathan's amulet
			{id = 9303, name = 'Leviathan\'s amulet', level = 50, duration = 333, type = ITEMTYPE_NECKLACE, price = 2143,
				itemList = {{id = 13582, amount = 3}, -- Magical thread
							{id = 17018, amount = 1}, -- Pure mithril
							{id = 10772, amount = 4}, -- Fine fabric
							}
				},
			-- Hellfire lavos amulet
			{id = 9301, name = 'Hellfire lavos amulet', level = 50, duration = 333, type = ITEMTYPE_NECKLACE, price = 1042,
				itemList = {{id = 13571, amount = 2}, -- Forbund
							{id = 17019, amount = 1}, -- Radiating powder
							{id = 5892, amount = 3}, -- Steel
							}
				},
			-- Sacred tree amulet
			{id = 9302, name = 'Sacred tree amulet', level = 50, duration = 333, type = ITEMTYPE_NECKLACE, price = 904,
				itemList = {{id = 17019, amount = 2}, -- Radiating powder
							{id = 9058, amount = 3}, -- Gold ignot
							}
				},
			-- Dragon necklace
			{id = 3085, name = 'Dragon necklace', level = 20, duration = 100, type = ITEMTYPE_NECKLACE, price = 74,
				itemList = {{id = 5809, amount = 2}, -- Magical element
							{id = 12785, amount = 4}, -- Cuprite
							{id = 12784, amount = 4}, -- Beastly claw
							}
				},
			-- Magma amulet
			{id = 13059, name = 'Magma amulet', level = 35, duration = 260, type = ITEMTYPE_NECKLACE, price = 568,
				itemList = {{id = 17019, amount = 4}, -- Radiating powder
							{id = 5887, amount = 2}, -- Royal steel
							{id = 5892, amount = 2}, -- Steel
							}
				},
			-- Golden amulet
			{id = 3013, name = 'Golden amulet', level = 60, duration = 300, type = ITEMTYPE_NECKLACE, price = 789,
				itemList = {{id = 10772, amount = 3}, -- Fine fabric
							{id = 5878, amount = 5}, -- Hardener leather
							{id = 9058, amount = 2}, -- Gold ignot
							}
				},
			-- Terra amulet
			{id = 13058, name = 'Terra amulet', level = 35, duration = 260, type = ITEMTYPE_NECKLACE, price = 175,
				itemList = {{id = 3026, amount = 3}, -- White pearl
							{id = 3027, amount = 3}, -- Black pearl
							{id = 5809, amount = 1}, -- Magical element
							}
				},
			-- Lightning amulet
			{id = 13061, name = 'Lightning amulet', level = 35, duration = 260, type = ITEMTYPE_NECKLACE, price = 683,
				itemList = {{id = 10772, amount = 4}, -- Fine fabric
							{id = 17019, amount = 5}, -- Radiating powder
							{id = 9058, amount = 1}, -- Gold ignot
							}
				},
			-- Garlic necklace
			{id = 3083, name = 'Garlic necklace', level = 10, duration = 30, type = ITEMTYPE_NECKLACE, price = 100,
				itemList = {{id = 5892, amount = 2}, -- Steel
							{id = 13253, amount = 5}, -- Coal
							}
				},
			-- Tooth necklace
			{id = 14858, name = 'Tooth necklace', level = 6, duration = 20, type = ITEMTYPE_NECKLACE, price = 31,
				itemList = {{id = 5925, amount = 2}, -- Hardened bone
							{id = 12784, amount = 3}, -- Beastly claw
							{id = 12785, amount = 5}, -- Cuprite
							}
				},
			-- Silver amulet
			{id = 3054, name = 'Silver amulet', level = 8, duration = 24, type = ITEMTYPE_NECKLACE, price = 150,
				itemList = {{id = 3026, amount = 2}, -- White pearl
							{id = 10771, amount = 1}, -- Mithril ore
							}
				},
			-- Crystal necklace
			{id = 3008, name = 'Crystal necklace', level = 30, duration = 120, type = ITEMTYPE_NECKLACE, price = 111,
				itemList = {{id = 10772, amount = 1}, -- Fine fabric
							{id = 5892, amount = 1}, -- Steel
							{id = 5880, amount = 3}, -- Iron ore
							{id = 5925, amount = 5}, -- Hardned bone
							}
				},
			-- Brass necklace
			{id = 14832, name = 'Brass necklace', level = 16, duration = 40, type = ITEMTYPE_NECKLACE, price = 97,
				itemList = {{id = 5878, amount = 3}, -- Hardener leather
							{id = 12832, amount = 5}, -- Rope belt
							{id = 5880, amount = 4}, -- Iron ore
							}
				},
			-- Stone skin amulet
			{id = 3081, name = 'Stone skin amulet', level = 80, duration = 400, type = ITEMTYPE_NECKLACE, price = 1146,
				itemList = {{id = 5878, amount = 2}, -- Hardener leather
							{id = 9058, amount = 3}, -- Gold ignot
							{id = 5887, amount = 2}, -- Royal steel
							}
				},
			-- Amber eye
			{id = 14869, name = 'Amber eye', level = 28, duration = 200, type = ITEMTYPE_NECKLACE, price = 217,
				itemList = {{id = 17019, amount = 1}, -- Radiating powder
							{id = 5887, amount = 1}, -- Royal steel
							{id = 12785, amount = 5}, -- Cuprite
							}
				},
		}