m_RewardsFunctions = {}

m_CraftingFunctions = {}
m_CraftingList = {}

m_CratesFunctions = {}
m_CratesList = {}

m_AchievementsFunctions = {}
m_AchievementsList = {}

m_MagazineFunctions = {}
m_MagazineList = {}

ID_CRATES = 1
ID_ACHIEVEMENTS = 2
ID_CRAFTING = 3
ID_SMELTING = 4
ID_MAGAZINE = 5

m_RewardsFunctions.config = {
	[ID_CRATES] = {'2605 342 40 40', tr('CRATES')},
	[ID_ACHIEVEMENTS] = {'2427 342 40 40', tr('ACHIEVEMENTS')},
	[ID_CRAFTING] = {'2471 342 40 40', tr('CRAFTING')},
	[ID_SMELTING] = {'2516 342 40 40', tr('SMELTING')},
	[ID_MAGAZINE] = {'2648 388 40 40', tr('MAGAZINE')}
}

m_RewardsFunctions.destroySelect = function()
	if m_RewardsFunctions.selectId == ID_CRATES then
		m_CratesFunctions.destroy()
	elseif m_RewardsFunctions.selectId == ID_ACHIEVEMENTS then
		m_AchievementsFunctions.destroy()
	elseif m_RewardsFunctions.selectId == ID_CRAFTING then
		m_CraftingFunctions.destroy()
	elseif m_RewardsFunctions.selectId == ID_SMELTING then
		m_CraftingFunctions.destroy()
	elseif m_RewardsFunctions.selectId == ID_MAGAZINE then
		m_MagazineFunctions.destroy()
	end
end

m_RewardsFunctions.updateIcon = function()
	local v = m_RewardsFunctions.config[m_RewardsFunctions.selectId]
	m_RewardsFunctions.icon:setImageClip(v[1])
	m_RewardsFunctions.title:setText(v[2])
end

m_RewardsFunctions.select = function(self, id, ignore)
	id = tonumber(id)
	if not ignore and m_RewardsFunctions.selectId == id then
		return true
	end
	
	m_RewardsFunctions.destroySelect()
	m_RewardsFunctions.selectId = id
	if id == ID_CRATES then
		m_CratesFunctions.open(m_RewardsFunctions.description)
	elseif id == ID_ACHIEVEMENTS then
		m_AchievementsFunctions.open(m_RewardsFunctions.description)
	elseif id == ID_CRAFTING then
		m_CraftingFunctions.selectWindow(false, m_RewardsFunctions.description)
	elseif id == ID_SMELTING then
		m_CraftingFunctions.selectWindow(true, m_RewardsFunctions.description)
	elseif id == ID_MAGAZINE then
		m_MagazineFunctions.open(m_RewardsFunctions.description)
	end
	
	if m_RewardsFunctions.current then
		m_RewardsFunctions.current:setOn(false)
	end
	
	m_RewardsFunctions.updateIcon()
	m_RewardsFunctions.current = self
	self:setOn(true)
end

m_RewardsFunctions.instantSelect = function(id)
	if id == ID_CRATES then
		m_RewardsFunctions.select(m_RewardsFunctions.window:getChildById('1'), ID_CRATES, true)
	elseif id == ID_ACHIEVEMENTS then
		m_RewardsFunctions.select(m_RewardsFunctions.window:getChildById('2'), ID_ACHIEVEMENTS, true)
	elseif id == ID_CRAFTING then
		m_RewardsFunctions.select(m_RewardsFunctions.window:getChildById('3'), ID_CRAFTING, true)
	elseif id == ID_SMELTING then
		m_RewardsFunctions.select(m_RewardsFunctions.window:getChildById('4'), ID_SMELTING, true)
	elseif id == ID_MAGAZINE then
		m_RewardsFunctions.select(m_RewardsFunctions.window:getChildById('5'), ID_MAGAZINE, true)
	end
end

m_RewardsFunctions.selectById = function(id)
	if not m_RewardsFunctions.window then
		m_RewardsFunctions.create(true)
	end
	
	m_RewardsFunctions.instantSelect(id)
end

m_RewardsFunctions.create = function(ignore)
	if m_RewardsFunctions.window then
		m_RewardsFunctions.destroy()
	else
		modules.game_character.m_CharacterFunctions.destroy()
		modules.game_encyclopedia.m_EncyclopediaFunctions.destroy()
		modules.game_questlog.m_QuestLogFunctions.destroy()
		modules.game_store.m_StoreFunctions.onCloseStore()
		modules.game_market.m_MarketFunctions.destroy()
		
		m_RewardsFunctions.window = g_ui.displayUI('game_rewards')
		m_RewardsFunctions.description = m_RewardsFunctions.window:getChildById('descriptionWindow')
		m_RewardsFunctions.icon = m_RewardsFunctions.window:getChildById('icon')
		m_RewardsFunctions.title = m_RewardsFunctions.window:getChildById('title')
		
		if not ignore then
			if m_RewardsFunctions.selectId then
				m_RewardsFunctions.instantSelect(m_RewardsFunctions.selectId)
			else
				m_RewardsFunctions.select(m_RewardsFunctions.window:getChildById('3'), ID_CRAFTING)
			end
		end
	
		modules.game_bottompanel.m_MainFunctions.open(WINDOW_CRATES)
	end
end

m_RewardsFunctions.destroy = function()
	if m_RewardsFunctions.window then
		m_RewardsFunctions.destroySelect()
		
		m_RewardsFunctions.window:destroy()
		m_RewardsFunctions.window = nil
		
		m_RewardsFunctions.description = nil
		m_RewardsFunctions.icon = nil
		m_RewardsFunctions.title = nil
		m_RewardsFunctions.current = nil
		
		modules.game_bottompanel.m_MainFunctions.close(WINDOW_CRATES)
	end
end

function onGameEnd()
	m_CraftingFunctions.onGameEnd()
	m_MagazineFunctions.onGameEnd()
	m_RewardsFunctions.destroy()
end

function onGameStart()
	m_CraftingFunctions.onGameStart()
	m_CratesFunctions.onGameStart()
	m_AchievementsFunctions.onGameStart()
	m_MagazineFunctions.onGameStart()
end

function onLoad()
	g_ui.importStyle('game_crates')
	g_ui.importStyle('game_achievements')
	g_ui.importStyle('game_achievements')
	g_ui.importStyle('game_crafting')
	g_ui.importStyle('game_magazine')
	g_ui.importStyle('craft_window')
	g_ui.importStyle('smelt_window')
	
	m_CratesFunctions.onLoad()
	m_CraftingFunctions.onLoad()
	m_AchievementsFunctions.onLoad()
	m_MagazineFunctions.onLoad()
	
	connect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
end

function onUnload()
	m_RewardsFunctions.destroy()
	
	m_CratesFunctions.onUnload()
	m_CraftingFunctions.onUnload()
	m_AchievementsFunctions.onUnload()
	m_MagazineFunctions.onUnload()
	
	disconnect(g_game, {
		onGameEnd = onGameEnd,
		onGameStart = onGameStart
	})
end