m_MagazineFunctions.categories = {
								{name = tr('All available items'), id = 16019, resize = true, class = ITEMCLASS_NONE},
								{name = tr('Legendary items'), id = 12238, class = ITEMCLASS_LEGENDARY},
								{name = tr('Perfect items'), id = 12237, class = ITEMCLASS_PERFECT},
								{name = tr('Normal items'), id = 12236, class = ITEMCLASS_NORMAL},
								{name = tr('Damaged items'), id = 12235, class = ITEMCLASS_DAMAGED},
								{name = tr('Desolate items'), id = 12234, class = ITEMCLASS_DESOLATE},
								}

m_MagazineFunctions.GameServerSendMagazine = 118
m_MagazineFunctions.GameServerUpdateMagazine = 149
m_MagazineFunctions.items = {}

m_MagazineFunctions.onLoad = function()
	ProtocolGame.registerOpcode(m_MagazineFunctions.GameServerUpdateMagazine, parseUpdateMagazine)
	if g_game.isOnline() then
		m_MagazineFunctions.onGameStart()
	end
end

m_MagazineFunctions.onUnload = function()
	ProtocolGame.unregisterOpcode(m_MagazineFunctions.GameServerUpdateMagazine, parseUpdateMagazine)
end

m_MagazineFunctions.onGameStart = function()
	m_MagazineFunctions.items = {}
	m_MagazineFunctions.protocol = g_game.getProtocolGame()
	m_MagazineFunctions.update = false
end

m_MagazineFunctions.onGameEnd = function()
	m_MagazineFunctions.items = {}
	m_MagazineFunctions.protocol = nil
	m_MagazineFunctions.update = false
end

m_MagazineFunctions.destroy = function()
	if m_MagazineList.window then
		m_MagazineList.categories:destroyChildren()
		m_MagazineList.list:destroyChildren()
		
		m_MagazineList.window:destroy()
		m_MagazineList = {}
	end
end

m_MagazineFunctions.select = function(self)
	if m_MagazineList.current then
		m_MagazineList.current:getChildById('mask'):hide()
	end
	
	m_MagazineFunctions.refresh()
	m_MagazineList.current = self
	self:getChildById('mask'):show()
	
	m_MagazineFunctions.addItems(self.class)
	
	if m_MagazineList.searchItem:getText() ~= '' then
		m_MagazineFunctions.onTextChange(m_MagazineList.searchItem)
	end
end

m_MagazineFunctions.manage = function(id)
	local msg = OutputMessage.create()
	msg:addU8(m_MagazineFunctions.GameServerSendMagazine)
	msg:addU8(id)
	m_MagazineFunctions.protocol:send(msg)
end

m_MagazineFunctions.open = function(parent)
	if m_MagazineList.window then
		m_MagazineFunctions.destroy()
	else
		m_MagazineList.window = g_ui.createWidget('MagazineWindow', parent)
		m_MagazineList.categories = m_MagazineList.window:getChildById('categories')
		m_MagazineList.list = m_MagazineList.window:getChildById('list')
		m_MagazineList.quantityScroll = m_MagazineList.window:getChildById('quantityScroll')
		m_MagazineList.quantityConvertScroll = m_MagazineList.window:getChildById('quantityConvertScroll')
		m_MagazineList.label = m_MagazineList.window:getChildById('label')
		m_MagazineList.convert = m_MagazineList.window:getChildById('convert')
		m_MagazineList.collect = m_MagazineList.window:getChildById('collect')
		m_MagazineList.searchItem = m_MagazineList.window:getChildById('searchItem')
		m_MagazineList.slot = m_MagazineList.window:getChildById(InventorySlotMagazine)
		
		for i = 1, #m_MagazineFunctions.categories do
			local v = m_MagazineFunctions.categories[i]
			local widget = g_ui.createWidget('MagazineCategoryWidget', m_MagazineList.categories)
			widget:setParent(m_MagazineList.categories)
			widget:setText(v.name)
			widget:setId('button' .. i)
			widget.class = v.class
			
			local item = widget:getChildById('item')
			item:setItemId(v.id)
			item:setOn(v.resize)
		end
		
		m_MagazineList.slot:getChildById('item'):setItemId(10882)
		-- if not m_MagazineFunctions.update then
			m_MagazineFunctions.items = {}
			m_MagazineFunctions.update = false
			m_MagazineFunctions.manage(0)
		-- else
			-- m_MagazineFunctions.addItems(ITEMCLASS_NONE)
			-- m_MagazineFunctions.select(m_MagazineList.categories:getChildById('button1'))
		-- end
	end
end

m_MagazineFunctions.refresh = function()
	m_MagazineList.quantityConvertScroll:setMaximum(0)
	m_MagazineList.quantityConvertScroll:setValue(0)
	m_MagazineList.quantityConvertScroll:setEnabled(false)
	m_MagazineList.convert:setEnabled(false)
	
	m_MagazineList.quantityScroll:setMaximum(0)
	m_MagazineList.quantityScroll:setValue(0)
	m_MagazineList.quantityScroll:setEnabled(false)
	
	m_MagazineList.currentItem = nil
end

m_MagazineFunctions.updateConvert = function(amount)
	local convertAmount = 0
	local classId = m_MagazineList.currentItem:getChildById('item').classId
	if classId == ITEMCLASS_DESOLATE then
		convertAmount = 10
	elseif classId == ITEMCLASS_DAMAGED then
		convertAmount = 25
	elseif classId == ITEMCLASS_NORMAL then
		convertAmount = 50
	elseif classId == ITEMCLASS_PERFECT then
		convertAmount = 200
	end
	
	if convertAmount ~= 0 then
		local value = math.floor(amount / convertAmount)
		m_MagazineList.quantityConvertScroll:setMaximum(value)
		m_MagazineList.quantityConvertScroll:setValue(math.min(1, value))
		m_MagazineList.quantityConvertScroll:setEnabled(value > 0)
		m_MagazineList.convert:setEnabled(value > 0)
	else
		m_MagazineList.quantityConvertScroll:setMaximum(0)
		m_MagazineList.quantityConvertScroll:setValue(0)
		m_MagazineList.quantityConvertScroll:setEnabled(false)
		m_MagazineList.convert:setEnabled(false)
	end
end

m_MagazineFunctions.use = function(self)
	if m_MagazineList.currentItem then
		m_MagazineList.currentItem:getChildById('mask'):hide()
		
		if m_MagazineList.currentItem == self:getParent() then
			m_MagazineFunctions.refresh()
			return true
		end
	end
	
	m_MagazineList.currentItem = self:getParent()
	m_MagazineList.currentItem:getChildById('mask'):show()
	
	local amount = self.amount or 0
	m_MagazineFunctions.updateConvert(amount)
	
	m_MagazineList.quantityScroll:setMaximum(amount)
	m_MagazineList.quantityScroll:setValue(amount)
	m_MagazineList.quantityScroll:setEnabled(true)
	
	if amount > 0 then
		m_MagazineList.collect:setEnabled(true)
	else
		m_MagazineList.collect:setEnabled(false)
	end
end

m_MagazineFunctions.collect = function()
	local amount = m_MagazineList.quantityScroll:getValue()
	if amount == 0 then
		return true
	end
	
	local item = m_MagazineList.currentItem:getChildById('item')
	
	local msg = OutputMessage.create()
	msg:addU8(m_MagazineFunctions.GameServerSendMagazine)
	msg:addU8(1)
	msg:addU8(item.classId)
	msg:addU16(item.itemId)
	msg:addU32(amount)
	m_MagazineFunctions.protocol:send(msg)
end

m_MagazineFunctions.convert = function()
	local amount = m_MagazineList.quantityConvertScroll:getValue()
	if amount == 0 then
		return true
	end
	
	local item = m_MagazineList.currentItem:getChildById('item')
	
	local msg = OutputMessage.create()
	msg:addU8(m_MagazineFunctions.GameServerSendMagazine)
	msg:addU8(3)
	msg:addU8(item.classId)
	msg:addU16(item.itemId)
	msg:addU32(amount)
	m_MagazineFunctions.protocol:send(msg)
end

m_MagazineFunctions.onTextChange = function(self)
	local text = self:getText():lower()
	if self:getText() ~= '' then
		self:getChildById('mask'):hide()
	else
		self:getChildById('mask'):show()
	end
	
	for _, pid in pairs(m_MagazineList.list:getChildren()) do
		if pid.name:find(text) then
			pid:show()
		else
			pid:hide()
		end
	end
end

m_MagazineFunctions.moveItem = function(item, id, update)
	local position = item:getPosition()
	local itemId = item:getId()
	local stackpos = item:getStackPos()
	
	local msg = OutputMessage.create()
	msg:addU8(m_MagazineFunctions.GameServerSendMagazine)
	msg:addU8(id)
	m_MagazineFunctions.protocol:addPosition(msg, position)
	msg:addU16(itemId)
	msg:addU8(stackpos)
	
	if id == 4 then
		item:addToFavorites(update)
		
		local container = item:getParentContainer()
		local widget = nil
		if container then
			widget = container.itemsPanel:getChildById('item' .. stackpos)
		else
			widget = modules.game_inventory.m_inventoryPanel:getChildById('slot' .. position.y)
		end
		
		widget:getChildById('star'):setVisible(update)
		msg:addU8(update and 1 or 0)
	end
	
	m_MagazineFunctions.protocol:send(msg)
end

m_MagazineFunctions.addItem = function(id, class, amount, name)
	if not amount then
		return true
	end

	local widget = g_ui.createWidget('MagazineItem', m_MagazineList.list)
	widget:setParent(m_MagazineList.list)
	widget:setId(id .. '_' .. class)
	widget.name = name
	
	local item = widget:getChildById('item')
	item:setItemId(id)
	item.classId = class
	item.amount = amount
	item.itemId = id
	widget:setText(amount)
	
	local corner = item:getChildById('corner')
	corner:setImageClip(modules.game_containers.getImageClipByClassId(class))
	corner:show()
end

m_MagazineFunctions.addItems = function(id)
	m_MagazineList.list:destroyChildren()
	
	for k, v in pairs(m_MagazineFunctions.items) do
		if id == ITEMCLASS_NONE then
			for j = ITEMCLASS_DESOLATE, ITEMCLASS_LEGENDARY do
				m_MagazineFunctions.addItem(k, j, v[j], v.name)
			end
		elseif v[id] then
			m_MagazineFunctions.addItem(k, id, v[id], v.name)
		end
	end
end

m_MagazineFunctions.updateItems = function(list)
	local removeIds = {}
	for _, pid in pairs(m_MagazineList.list:getChildren()) do
		if pid:isVisible() then
			local item = pid:getChildById('item')
			local v = list[item.itemId]
			if v then
				local amount = v[item.classId]
				if amount then
					pid:setText(amount)
					item.amount = amount
					
					table.insert(removeIds, item.itemId)
				end
			end
		end
	end
	
	if #removeIds > 0 then
		for _, v in pairs(removeIds) do
			list[v] = nil
		end
	end
	
	local text = m_MagazineList.searchItem:getText():lower()
	for k, v in pairs(list) do
		if text == '' or v.name:find(text) then
			for i = ITEMCLASS_DESOLATE, ITEMCLASS_LEGENDARY do
				m_MagazineFunctions.addItem(k, i, v[i], v.name)
			end
		end
	end
end

m_MagazineFunctions.removeItem = function(clientId, class, amount)
	local widget = m_MagazineList.list:getChildById(clientId .. '_' .. class)
	amount = amount or 0
	if amount == 0 then
		if widget == m_MagazineList.currentItem then
			m_MagazineList.currentItem = nil
			m_MagazineFunctions.refresh()
		end
		
		widget:destroy()
		m_MagazineFunctions.items[clientId][class] = nil
		return true
	end
	
	m_MagazineFunctions.items[clientId][class] = amount
	widget:setText(amount)
	
	local item = widget:getChildById('item')
	item.amount = amount
	m_MagazineList.quantityScroll:setMaximum(amount)
	m_MagazineList.collect:setEnabled(amount > 0)
	
	m_MagazineFunctions.updateConvert(amount)
	local var = m_MagazineFunctions.items[clientId]
	for i = ITEMCLASS_DESOLATE, ITEMCLASS_LEGENDARY do
		if var[i] and var[i] > 0 then
			return true
		end
	end
	
	m_MagazineFunctions.items[clientId] = nil
end

function parseUpdateMagazine(protocol, msg)
	local list = {}
	local id = msg:getU8()
	if id == 0 then
		local size = msg:getU16()
		local update = m_MagazineFunctions.update
		for i = 1, size do
			local clientId = msg:getU16()
			local name = msg:getString():lower()
			if not m_MagazineFunctions.items[clientId] then
				m_MagazineFunctions.items[clientId] = {name = name}
			end
			
			list[clientId] = {name = name}
			
			local itemSize = msg:getU16()
			for j = 1, itemSize do
				local class = msg:getU8()
				local amount = msg:getU32()
				
				local count = m_MagazineFunctions.items[clientId][class] or 0
				m_MagazineFunctions.items[clientId][class] = count + amount
				list[clientId][class] = count + amount
				m_MagazineFunctions.update = true
			end
		end
		
		if m_MagazineList.window then
			if update then
				m_MagazineFunctions.updateItems(list)
			else
				m_MagazineFunctions.addItems(ITEMCLASS_NONE)
				m_MagazineFunctions.select(m_MagazineList.categories:getChildById('button1'))
			end
		end
	elseif id == 1 then
		local clientId = msg:getU16()
		local class = msg:getU8()
		local amount = msg:getU32()
		m_MagazineFunctions.removeItem(clientId, class, amount)
	end
end
