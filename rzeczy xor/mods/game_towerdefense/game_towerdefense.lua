local towerList = {}
local lastTileList = {}
local upgradeCache = {}
local updateTowerPosition = {}

local towerdefenseWindow = nil
local towersList = nil
local coinsWidget = nil
local healthWidget = nil
local selectedType = nil
local hoverWindow = nil
local towerMoveShadow = nil
local lastTile = nil
local lastTarget = nil
local coinEvent = nil

local TOWER_DEFENSE_COINS_CODE = 1
local TOWER_DEFENSE_HEALTH_CODE = 2
local TOWER_DEFENSE_UPDATE_TOWER_X = 3
local TOWER_DEFENSE_UPDATE_TOWER_Y = 4
local TOWER_DEFENSE_UPDATE_TOWER_Z = 5
local TOWER_DEFENSE_NOT_ENOUGH_COINS = 6

local TOWER_DEFENSE_TILE_ID = {7348, 423, 937, 6967, 416, 10145}

local config = {
				[1] = {
						{0, 1, 1, 1, 0},
						{1, 1, 1, 1, 1},
						{1, 1, 3, 1, 1},
						{1, 1, 1, 1, 1},
						{0, 1, 1, 1, 0}
						},
				[2] = {
						{1, 1, 1},
						{1, 3, 1},
						{1, 1, 1}
						},
				[3] = {
						{0, 0, 1, 0, 0},
						{0, 1, 1, 1, 0},
						{1, 1, 3, 1, 1},
						{0, 1, 1, 1, 0},
						{0, 0, 1, 0, 0}
						},
				[4] = {
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0},
						{1, 1, 1, 3, 1, 1, 1},
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0}
						},
				[5] = {
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 1, 1, 1, 0, 0},
						{1, 1, 1, 3, 1, 1, 1},
						{0, 0, 1, 1, 1, 0, 0},
						{0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0}
						},
				[6] = {
						{0, 0, 1, 1, 1, 0, 0},
						{0, 1, 1, 1, 1, 1, 0},
						{1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 3, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1},
						{0, 1, 1, 1, 1, 1, 0},
						{0, 0, 1, 1, 1, 0, 0}
						}
				}

local function deleteShadow()
	if towerMoveShadow then
		towerMoveShadow:destroy()
		towerMoveShadow = nil
	end
end

local function unselectTiles()
	if lastTile then
		lastTile:unselect()
	end

	for _, pid in pairs(lastTileList) do
		pid:unselect()
	end
	
	lastTileList = {}
end

local function destroyHoverWindow()
	if hoverWindow then
		hoverWindow:destroy()
		hoverWindow = nil
	end
	
	unselectTiles()
end

function updateTower()
	local msg = OutputMessage.create()
	msg:addU8(254)
	msg:addU8(OPCODE_UPDATE_TOWER)
	
	pos = hoverWindow.tile:getPosition()
	msg:addU16(hoverWindow.id)
	msg:addU16(pos.x)
	msg:addU16(pos.y)
	msg:addU16(pos.z)
	
	g_game.getProtocolGame():send(msg)
end

function removeTower()
	local msg = OutputMessage.create()
	msg:addU8(254)
	msg:addU8(OPCODE_REMOVE_TOWER)
	
	pos = hoverWindow.tile:getPosition()
	msg:addU16(hoverWindow.id)
	msg:addU16(pos.x)
	msg:addU16(pos.y)
	msg:addU16(pos.z)
	
	g_game.getProtocolGame():send(msg)
	
	destroyHoverWindow()
end

local function updateSelection(tile, id, canBuild)
	lastTile = tile
		
	local x, y, fromPos = 0, 0, tile:getPosition()
	local cache = config[id]
	for py = 1, #cache do
		for px = 1, #cache[py] do
			if cache[py][px] == 3 then
				x = px - 1
				y = py - 1
				break
			end
		end
	end
	
	if canBuild then
		tile:selectColor('#00FF00')
	else
		tile:selectColor('#FF0000')
	end
	
	for py = 1, #cache do
		for px = 1, #cache[py] do
			local var = cache[py][px]
			if var == 1 then
				local tilePos = {x = fromPos.x + (px - x - 1), y = fromPos.y + (py - y - 1), z = fromPos.z}
				local tile = g_map.getTile(tilePos)
				if tile then
					if canBuild then
						tile:selectColor('#70DC70')
					else
						tile:selectColor('#CD5C5C')
					end
					
					tile:select()
					table.insert(lastTileList, tile)
				end
			end
		end
	end
end

local function createHoverWindow(target)
	if hoverWindow then
		return true
	end
	
	local id = tonumber(target:getName())
	if not id then
		return false
	end
	
	hoverWindow = g_ui.createWidget('HoverTowerDefenseWindow', modules.game_interface.getRootPanel())
	
	local pos = g_window.getMousePosition()
	local tile = modules.game_interface.getMapPanel():getTile(pos)
	local level = target.level or 1
	
	if hoverPosition then
		pos = hoverPosition
	else
		pos.x = pos.x - hoverWindow:getWidth() + (hoverWindow:getWidth() / 2)
		pos.y = pos.y - hoverWindow:getHeight() + 8
	end
	
	hoverWindow.id = id
	hoverWindow.tile = tile
	hoverWindow:setPosition(pos)
	hoverWindow:getChildById('item'):setItemId(upgradeCache[id][math.min(level, 4)][2])
	
	local upgradePrice = hoverWindow:getChildById('upgradePrice')
	local sellPrice = hoverWindow:getChildById('sellPrice')
	if level > 4 then
		upgradePrice:setOn(true)
		hoverWindow:getChildById('upgrade'):setOn(true)
		hoverWindow:getChildById('remove'):setOn(true)
	else
		upgradePrice:setText(upgradeCache[id][level][1] .. '$')
	end
	
	sellPrice:setText(math.floor(towerList[id].price / 2) .. '$')
end

local function updatePosition()
	if not towerdefenseWindow then
		return false
	end

	local pos = g_window.getMousePosition()
	local tile = modules.game_interface.getMapPanel():getTile(pos)
	if not tile then
		return false
	end
	
	if towerMoveShadow then
		pos.x = pos.x - 48
		pos.y = pos.y - 48
		
		towerMoveShadow:setPosition(pos)
		
		if lastTile == tile then
			return true
		end
		
		unselectTiles()
		
		local ground = tile:getGround()
		local canBuild = tile:isWalkable() and not tile:hasCreature() and not isInArray(TOWER_DEFENSE_TILE_ID, ground:getId())
		if canBuild then
			tile:selectColor('#00FF00')
		else
			tile:selectColor('#FF0000')
		end
		
		tile:select()
		updateSelection(tile, towerMoveShadow.id, canBuild)
	else
		local target = tile:getTopCreature()
		if lastTarget and lastTarget ~= target then
			lastTarget:setOutfitColor('#FFFFFFFF', 0)
			lastTarget = nil
			
			destroyHoverWindow()
		end
		
		if target then
			local id = tonumber(target:getName())
			if not id then
				return false
			end
			
			lastTarget = target
			target:setOutfitColor('#337993', 0)
			
			updateSelection(tile, id, true)
		end
	end
end

local function checkTower(pos)
	if not lastTarget then
		return false
	end

	local tile = modules.game_interface.getMapPanel():getTile(pos)
	if not tile then
		return false
	end
	
	local target = tile:getTopCreature()
	if target == lastTarget then
		createHoverWindow(target)
	end
end

local function select(var, i, selected)
	deleteShadow()
	unselectTiles()
	
	if i == -1 then
		selectedType = nil
	end
	
	for k = 1, #var do
		local blink = var[k]:getChildById('blink')
		if blink then
			if k == i and (not selected or selected ~= i) then
				blink:show()
				towerMoveShadow = g_ui.createWidget('TowerMoveShadow', modules.game_interface.getRootPanel())
				towerMoveShadow:setItemId(var[k].id)
				towerMoveShadow.id = i
				towerMoveShadow.onMouseRelease = function(self, mousePosition, mouseButton)
													if mousePosition == MouseRightButton then
														select(towerList, -1, selectedType)
													end
												end
				
				updatePosition()
			else
				blink:hide()
			end
		end
	end
	
	modules.game_interface.getRootPanel().onMouseMove = function(widget, mousePos, mouseMoved)
																updatePosition()
															end
end

function putTower(pos, mouseButton)
	if not towerdefenseWindow then
		return false
	end
	
	if not selectedType then
		if mouseButton == MouseRightButton then
			checkTower(pos)
		end
		
		return true
	end
	
	if mouseButton == MouseRightButton then
		select(towerList, -1, selectedType)
		return true
	end
	
	local tile = modules.game_interface.getMapPanel():getTile(pos)
	if not tile or not tile:isWalkable() or tile:hasCreature() then
		return true
	end
	
	local msg = OutputMessage.create()
	msg:addU8(254)
	msg:addU8(OPCODE_PUT_TOWER)
	
	pos = tile:getPosition()
	msg:addU16(selectedType)
	msg:addU16(pos.x)
	msg:addU16(pos.y)
	msg:addU16(pos.z)
	
	g_game.getProtocolGame():send(msg)
	return true
end

local function destroyWidgets(var)
	if #var == 0 then
		return true
	end

	for _, widget in pairs(var) do
		widget:destroy()
	end
end

function destroyTowerDefenseWindow()
	destroyHoverWindow()
	
	deleteShadow()
	unselectTiles()
	
	if coinEvent then
		coinEvent:cancel()
		coinEvent = nil
	end
	
	if towerdefenseWindow then
		destroyWidgets(towerList)
		towerList = {}
		
		upgradeCache = {}
		updateTowerPosition = {}
		
		towersList = nil
		coinsWidget = nil
		healthWidget = nil
		lastTile = nil
		hoverPosition = nil
		
		if lastTarget then
			lastTarget:setOutfitColor('#FFFFFFFF', 0)
			lastTarget = nil
		end
		
		towerdefenseWindow:destroy()
		towerdefenseWindow = nil
	end
end

function onLoad()
	g_ui.importStyle('game_towerdefense')
	
	connect(g_game, {
		onGameEnd = destroyTowerDefenseWindow,
		onTaskWindow = onTaskWindow,
		onOpcode = onOpcode
	})
end

function onUnload()
	destroyTowerDefenseWindow()
	
	disconnect(g_game, {
		onGameEnd = destroyTowerDefenseWindow,
		onTaskWindow = onTaskWindow,
		onOpcode = onOpcode
	})
end

function onOpcode(code, var)
	if code == TOWER_DEFENSE_COINS_CODE then
		coinsWidget:setText(var)
	elseif code == TOWER_DEFENSE_HEALTH_CODE then
		healthWidget:setText(var)
	elseif code >= TOWER_DEFENSE_UPDATE_TOWER_X and code <= TOWER_DEFENSE_UPDATE_TOWER_Z then
		if code == TOWER_DEFENSE_UPDATE_TOWER_X then
			updateTowerPosition.x = var
		elseif code == TOWER_DEFENSE_UPDATE_TOWER_Y then
			updateTowerPosition.y = var
		elseif code == TOWER_DEFENSE_UPDATE_TOWER_Z then
			updateTowerPosition.z = var
		end
		
		if updateTowerPosition.x and updateTowerPosition.y and updateTowerPosition.z then
			local tile = g_map.getTile(updateTowerPosition)
			if tile then
				local target = tile:getTopCreature()
				if target then
					target.level = (target.level or 1) + 1
					
					if hoverWindow then
						hoverPosition = hoverWindow:getPosition()
					end
					
					destroyHoverWindow()
					createHoverWindow(target)
					updateTowerPosition = {}
					hoverPosition = nil
				end
			end
		end
	elseif code == TOWER_DEFENSE_NOT_ENOUGH_COINS then
		if coinEvent then
			coinEvent:cancel()
			coinEvent = nil
		end
		
		coinsWidget:setColor('#FF0000')
		coinEvent = scheduleEvent(function()
											if coinsWidget then
												coinsWidget:setColor('#9E996E')
											end
											
											coinEvent = nil
										end, 1000)
	end
end

function onTaskWindow(id, list)
	if id ~= 1901 or #list == 0 then
		return destroyTowerDefenseWindow()
	end
	
	if not towerdefenseWindow then
		towerdefenseWindow = g_ui.createWidget('TowerDefenseWindow', modules.game_interface.getRootPanel())
		coinsWidget = towerdefenseWindow:getChildById('coins')
		healthWidget = towerdefenseWindow:getChildById('health')
		towersList = towerdefenseWindow:getChildById('towerList')
	end
	
	destroyWidgets(towerList)
	towerList = {}
	local coins, health = 0, 0
	for i = 1, #list do
		local j, name, id, price, cash, life, _, upgrades = unpack(list[i])
		coins = cash
		health = life
		
		local widget = g_ui.createWidget('TowerWidget', towersList)
		widget:getChildById('item'):setItemId(id)
		widget:getChildById('name'):setText(name .. '\n' .. price .. '$')
		widget:setParent(towersList)
		
		widget.level = 1
		widget.id = id
		widget.price = price
		widget.onClick = function() 
									select(towerList, i, selectedType)
									if selectedType == i then
										selectedType = nil
									else
										selectedType = i
									end
								end
		
		table.insert(towerList, widget)
		table.insert(upgradeCache, upgrades)
	end
	
	coinsWidget:setText(coins)
	healthWidget:setText(health)
end

