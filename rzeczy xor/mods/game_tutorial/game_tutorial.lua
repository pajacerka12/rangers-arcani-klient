m_TutorialFunctions = {}
m_TutorialList = {}

m_TutorialFunctions.tutorialList = {}
m_TutorialFunctions.button = {}
m_TutorialFunctions.name = ''
m_TutorialFunctions.runeId = 3147

m_TutorialFunctions.list = {
	[0] = { image = 'images/melee_attack', description = tr('Strength\n\nEffective strength (do not confuse with basic strength) is used to calculate melee and ranged damage.\n\nEach point of effective strength increases damage by 0.45%% for one-handed melee weapons and 0.5%% for two-handed melee weapons.\n\n100 effective strength increases damage by 45%% for one-handed melee weapons and 50%% for two-handed melee weapons, so an one-handed melee weapon with 50-100 damage will hit 73-145 [50-100 * 1.45] and a two-handed melee weapon 75-150 [50-100 * 1.5].') },
	[1] = { image = 'images/distance_attack', description = tr('Strength\n\nEffective strength (do not confuse with basic strength) is used to calculate melee and ranged damage. Each point of effective strength increases damage by 0.4%% for throwing weapons (spears, knives, shurikens, stones) and 0.5%% for shooting weapon(bows and crossbows).\n\n100 effective strength will increase throwing damage by 40%% and shooting damage by 50%%, so a throwing weapon with 50-100 damage will hit 70-140 [50-100*1.4] and a shooting weapon with same stats will hit 75-150 [50-100*1.5].') },
	[3] = { image = 'images/magic_attack', description = tr('Wisdom\n\nEffective wisdom (do not confuse with basic wisdom) is used to calculate magic damage. Each point of effective wisdom increases damage by 0.35%%, counting from the basic value given in the spell description.\n\nI 100 effective wisdom increases damage by 35%%, so a spell with 100 damage will hit 135 [100 * 1.35].') },
}

function isIdenticalPositions(firstPos, secondPos, rangeX, rangeY)
	if rangeX or rangeY then
		rangeX = rangeX or rangeY
		rangeY = rangeY or rangeX
	
		return firstPos.x >= secondPos.x - rangeX and firstPos.x <= secondPos.x + rangeX and 
			firstPos.y >= secondPos.y - rangeY and firstPos.y <= secondPos.y + rangeY and firstPos.z == secondPos.z
	end
	
	return firstPos.x == secondPos.x and firstPos.y == secondPos.y and firstPos.z == secondPos.z
end

function onLoad()
	m_TutorialFunctions.tutorialList = g_settings.getNode('tutorialList', m_TutorialFunctions.tutorialList)
	if not m_TutorialFunctions.tutorialList or m_TutorialFunctions.tutorialList == '' then
		m_TutorialFunctions.tutorialList = {}
	end
	
	if g_game.isOnline() then
		onGameStart()
	end
	
	connect(LocalPlayer, {
		onPositionChange = onPositionChange
	})
	
	connect(Creature, {
		onAppear = onCreatureAppear
	})
	
	connect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onLoot = onLoot,
	})
end

function onUnload()
	disconnect(LocalPlayer, {
		onPositionChange = onPositionChange
	})
	
	disconnect(Creature, {
		onAppear = onCreatureAppear
	})
	
	disconnect(g_game, {
		onGameStart = onGameStart,
		onGameEnd = onGameEnd,
		onLoot = onLoot,
	})
	
	g_settings.setNode('tutorialList', m_TutorialFunctions.tutorialList)
	m_TutorialFunctions.destroyButton()
	m_TutorialFunctions.destroy()
end

function onGameStart()
	m_TutorialFunctions.button = {}
	m_TutorialFunctions.name = g_game.getLocalPlayer():getName()
	if not m_TutorialFunctions.tutorialList[m_TutorialFunctions.name] then
		m_TutorialFunctions.tutorialList[m_TutorialFunctions.name] = {}
	end
end

function onGameEnd()
	m_TutorialFunctions.destroyButton()
	m_TutorialFunctions.destroy()
end

local config = {
	[1] = {list = {{x = 1381, y = 1394, z = 7}, {x = 1381, y = 1395, z = 7}}, id = 4},
	[2] = {list = {{x = 1400, y = 1385, z = 8}, {x = 1400, y = 1386, z = 8}, {x = 1379, y = 1367, z = 7}, {x = 1380, y = 1367, z = 7}, {x = 1381, y = 1367, z = 7}, {x = 1382, y = 1367, z = 7}, {x = 1383, y = 1367, z = 7}, {x = 1384, y = 1367, z = 7}}, id = 5},
	[3] = {list = {{x = 1311, y = 986, z = 7}, {x = 1311, y = 987, z = 7}, {x = 1311, y = 988, z = 7}, {x = 1311, y = 989, z = 7}}, id = 8},
	[4] = {verticalList = {{x = 1597, y = 697, z = 7}, {x = 1597, y = 786, z = 7}}, id = 9},
	[5] = {list = {{x = 1191, y = 1182, z = 6}, {x = 1191, y = 1183, z = 6}}, id = 10},
}

function onPositionChange(localPlayer, newPos, oldPos)
	-- Skill window
	m_TutorialFunctions.check(newPos, 'SkillWindowTutorial', 1)
	
	-- Spell window
	m_TutorialFunctions.check(newPos, 'SpellWindowTutorial', 2)
	
	-- New Citizen of Cirith
	m_TutorialFunctions.check(newPos, 'CitizenOfCirithTutorial', 3)
	
	-- Dangerous places
	m_TutorialFunctions.check(newPos, 'TemplarCamp', 4)
	m_TutorialFunctions.check(newPos, 'BanditCamp', 5)
end

function onCreatureAppear(creature)
	-- Meet creature skull
	if creature:getSkull() > 0 then
		local statusId = m_TutorialFunctions.tutorialList[m_TutorialFunctions.name]['CreatureSkullTutorial']
		if not statusId then
			m_TutorialFunctions.select(6)
			m_TutorialFunctions.tutorialList[m_TutorialFunctions.name]['CreatureSkullTutorial'] = true
		end
	end
end

function onLoot(list)
	-- First rune found
	local statusId = m_TutorialFunctions.tutorialList[m_TutorialFunctions.name]['RuneItemTutorial']
	if not statusId then
		for i = 1, #list do
			local id, count, name = unpack(list[i])
			if id == m_TutorialFunctions.runeId then
				m_TutorialFunctions.select(7)
				m_TutorialFunctions.tutorialList[m_TutorialFunctions.name]['RuneItemTutorial'] = true
				break
			end
		end
	end
end

m_TutorialFunctions.updatePosition = function(enabled)
	m_TutorialFunctions.enabled = enabled
	
	local value = enabled and 19 or 72
	for i = 1, #m_TutorialFunctions.button do
		m_TutorialFunctions.button[i]:setMarginTop(value)
	end
end

m_TutorialFunctions.check = function(newPos, tutorialId, id)
	local v = m_TutorialFunctions.tutorialList[m_TutorialFunctions.name]
	if not v then
		return true
	end
	
	local statusId = v[tutorialId]
	if not statusId then
		local var = config[id]
		if var.list then
			for i = 1, #var.list do
				if isIdenticalPositions(newPos, var.list[i]) then
					m_TutorialFunctions.select(var.id)
					m_TutorialFunctions.tutorialList[m_TutorialFunctions.name][tutorialId] = true
					break
				end
			end
		elseif var.verticalList then
			if newPos.x == var.verticalList[1].x and newPos.y >= var.verticalList[1].y and newPos.y <= var.verticalList[2].y and newPos.z == var.verticalList[1].z then
				m_TutorialFunctions.select(var.id)
				m_TutorialFunctions.tutorialList[m_TutorialFunctions.name][tutorialId] = true
			end
		end
	end
end

m_TutorialFunctions.destroyButton = function(self)
	if not self then
		for i = 1, #m_TutorialFunctions.button do
			m_TutorialFunctions.button[i]:destroy()
		end
		
		m_TutorialFunctions.button = {}
	else
		for i = 1, #m_TutorialFunctions.button do
			if m_TutorialFunctions.button[i] == self then
				table.remove(m_TutorialFunctions.button, i)
				break
			end
		end
		
		self:destroy()
	end
end

m_TutorialFunctions.destroy = function()
	if m_TutorialList.skillIconInfo then
		m_TutorialList.skillIconInfo:destroy()
		m_TutorialList.skillIconInfo = nil
	end
	
	if m_TutorialList.window then
		m_TutorialList.window:destroy()
		m_TutorialList = {}
	end
end

m_TutorialFunctions.open = function(id)
	m_TutorialFunctions.destroy()
	
	if not m_TutorialList.window then
		m_TutorialList.window = g_ui.displayUI(id or 'basic_information')
		m_TutorialList.image = m_TutorialList.window:getChildById('image')
		m_TutorialList.description = m_TutorialList.window:getChildById('description')
		
		if id then
			m_TutorialList.language = g_settings.get('locale', 'en')
			if m_TutorialList.language == 'en' then
				m_TutorialList.language = LANGUAGE_ENGLISH
			else
				m_TutorialList.language = LANGUAGE_POLISH
			end
			
			m_TutorialList.leftAddMouseButton = m_TutorialList.window:getChildById('leftAddMouseButton')
		end
	end
	
	m_TutorialList.window:focus()
end

m_TutorialFunctions.goto = function(id)
	m_TutorialFunctions.destroy()
	if id == 1 then
		modules.game_character.m_CharacterFunctions.create(false, 1)
	elseif id == 2 then
		modules.game_character.m_CharacterFunctions.create(false, 2)
	elseif id == 3 then
		modules.game_rewards.m_RewardsFunctions.create(false, 1)
	elseif id == 4 then
		modules.game_character.m_CharacterFunctions.create(false, 4)
	end
end

m_TutorialFunctions.select = function(id)
	local id = tonumber(id) or 0
	if id < 4 then
		local var = m_TutorialFunctions.list[id]
		if not var then
			return true
		end
		
		m_TutorialFunctions.open()
		m_TutorialList.image:setImageSource(var.image)
		m_TutorialList.description:setText(var.description)
		m_TutorialList.window:setHeight(260 + m_TutorialList.description:getHeight())
	elseif id == 4 then
		m_TutorialFunctions.open('skill_tree')
		m_TutorialList.leftAddMouseButton:setWidth(m_TutorialList.language == LANGUAGE_POLISH and 450 or 300)
		m_TutorialList.description.onText = function(self, coords, text, iconCoords)
			local i, prev = 1
			for _, v in pairs(iconCoords) do
				local id, icon = unpack(v)
				if icon.x ~= 0 and icon.y ~= 0 then
					local pos = self:getPosition()
					local offset = {x = icon.x - pos.x, y = icon.y - pos.y}
					if id == 4 then
						offset = {x = prev.x - pos.x + 44, y = prev.y - pos.y}
					elseif id == 3 then
						prev = icon
					end
					
					local name = ''
					local skillIcon = g_ui.createWidget('SkillTreeTutorialWidget', m_TutorialList.description)
					skillIcon:setId(id)
					if id == 0 or id == 3 then
						skillIcon.onHoverChange = function(self, hovered)
							if hovered then
								local pos = self:getPosition()
								pos.x = pos.x + self:getWidth() - 2
								pos.y = pos.y + self:getHeight() - 2
								
								m_TutorialList.skillIconInfo = g_ui.displayUI('icon_panel')
								m_TutorialList.skillIconInfo:setPosition(pos)
							elseif m_TutorialList.skillIconInfo then
								m_TutorialList.skillIconInfo:destroy()
								m_TutorialList.skillIconInfo = nil
							end
						end
					else
						skillIcon.onHoverChange = nil
					end
					
					if id == 0 then
						id = 19
						name = 'Strength'
					elseif id == 1 then
						id = 51
						offset.x = offset.x - 3
						name = 'Dexterity'
					elseif id == 2 then
						id = 27
						offset.x = offset.x - 8
						name = 'Condition'
					elseif id == 3 then
						id = 17
						offset.x = offset.x - 18
						name = 'Wisdom'
					elseif id == 4 then
						id = 49
						name = 'Vigor'
					end
					
					i = i + 1
					modules.game_lookat.m_LookAtFunctions.setIconImageType(skillIcon, id)
					skillIcon:setMarginTop(offset.y)
					skillIcon:setMarginLeft(offset.x)
					skillIcon:setTooltip(name)
				end
			end
		end
	elseif id == 5 then
		local vocationId = g_game.getLocalPlayer():getVocation()
		local name = {}
		if vocationId <= 3 then
			name = {tr('Mage'), tr('Wizard'), tr('Druid'), tr('Mage')}
			tmpId = 1
		elseif vocationId <= 6 then
			name = {tr('Hunter'), tr('Grenadier'), tr('Tracker'), tr('Hunter')}
			tmpId = 2
		else
			name = {tr('Mercenary'), tr('Barbarian'), tr('Paladin'), tr('Mercenary')}
			tmpId = 3
		end
		
		m_TutorialFunctions.open('spell_tree')
		m_TutorialList.image:setImageSource('images/spell_tree_' .. tmpId)
		m_TutorialList.leftAddMouseButton:setWidth(m_TutorialList.language == LANGUAGE_POLISH and 375 or 300)
		m_TutorialList.description:setText(tr('Spells tree is divided into 3 main parts: Utilities, %s, %s and %s.\n\nSome of the Utilities spells are already given to you. %s tree got 2 branches, each branch ends with promotion spell which is available only on Mainland.\n\nDuring your play you will be able to test spells from both branches, but after leaving the Chaeck only one will be available for you.', unpack(name)))
	elseif id == 6 or id == 7 then
		local widget = g_ui.displayUI('game_tutorial')
		widget:setId(id)
		
		if m_TutorialFunctions.enabled then
			widget:setMarginTop(19)
		end
		
		table.insert(m_TutorialFunctions.button, widget)
	elseif id == 8 then
		m_TutorialFunctions.open('citizen_of_cirith')
	elseif id == 9 or id == 10 then
		m_TutorialFunctions.open('dangerous_window')
		m_TutorialList.image:setImageSource('images/' .. (id == 9 and 'templar_camp' or 'bandit_camp'))
	end
end

m_TutorialFunctions.openByHint = function(self, id)
	m_TutorialFunctions.destroyButton(self)
	
	if id == 6 then
		m_TutorialFunctions.open('creature_skull')
	elseif id == 7 then
		m_TutorialFunctions.open('rune_item')
		m_TutorialList.leftAddMouseButton:setWidth(m_TutorialList.language == LANGUAGE_POLISH and 380 or 300)
	end
end