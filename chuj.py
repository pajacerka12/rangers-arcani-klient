import sys
import os
import shutil


elements = []
#recursively finds elements that are files, returns array with full path to files
def findAllElements(folderPath):
    with os.scandir(folderPath) as elementsInDir:
        for elementInDir in elementsInDir:
            if elementInDir.is_file():
                elements.append(elementInDir.path)
            if elementInDir.is_dir():
                findAllElements(elementInDir.path)
    return elements

def xorAllFiles(src, dst):
    elements = findAllElements(".\\" + src)
    for element in elements:
        # Read two files as byte arrays
        file1_b = bytearray(open(element, 'rb').read())
        keyFile_b = bytearray(open("key", 'rb').read())

        length = len(file1_b)

        k = 0
        key = keyFile_b.copy()

        keyFile_b = bytearray()

        for i in range(0, length):
            if(k >= len(key)):
                k = 0
            keyFile_b.append(key[k])
            k += 1

        open("keygened", 'wb').write(keyFile_b)
        # Set the length to be the smaller one
        size = len(file1_b)
        xord_byte_array = bytearray(size)

        # XOR file using keyfile
        for i in range(size):
            xord_byte_array[i] = file1_b[i] ^ keyFile_b[i]

        # Write the XORd bytes to the output file
        elementNewPath = str(element).replace("\\" + src +"\\", "\\" + dst + "\\", 1)
        os.makedirs(os.path.dirname(elementNewPath), exist_ok=True)
        open(elementNewPath, 'wb').write(xord_byte_array)
        print(element)
    print(len(elements))

srcFolder = "rzeczy"
dstFolder = "rzeczy xor"
dstZipFolder = "Ranger's Arcani Online"

xorAllFiles(srcFolder, dstFolder)

dirsInDstFolder = [f for f in os.listdir(".\\" + dstFolder) if os.path.isdir(os.path.join(".\\" + dstFolder, f))]

for dirName in dirsInDstFolder:
    zipName = shutil.make_archive(".\\" + dstZipFolder + "\\" + dirName, 'zip', ".\\" + dstFolder + "\\" + dirName)
    pre, ext = os.path.splitext(zipName)
    newZipName = pre + ".dll"
    os.remove(newZipName)
    os.rename(zipName, newZipName)
    
