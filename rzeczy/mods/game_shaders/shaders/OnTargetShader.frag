uniform float outLineSize  = 1.0 / 2048;

uniform sampler2D u_Tex0;
varying vec2 v_TexCoord;
uniform float u_Time;

void main()
{
    vec4 col = texture2D(u_Tex0, v_TexCoord);
    if(col.a == 0.0 && col.w == 0)
	{
		if (texture2D(u_Tex0, v_TexCoord + vec2(0.0,          outLineSize)).a  != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(0.0,         -outLineSize)).a  != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(outLineSize,  0.0)).a          != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(-outLineSize, 0.0)).a          != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(-outLineSize, outLineSize)).a  != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(-outLineSize, -outLineSize)).a != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(outLineSize,  outLineSize)).a  != 0.0 ||
            texture2D(u_Tex0, v_TexCoord + vec2(outLineSize,  -outLineSize)).a != 0.0) 
            col = vec4(255, col.g, col.b, (cos(u_Time * 9.57) + 1.0)/2.0 * 0.2 + 0.8);
	}
	
	gl_FragColor = col;
}