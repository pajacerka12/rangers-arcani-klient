#ifdef GL_ES
precision mediump float;
#endif

uniform float u_Time;
uniform sampler2D u_Tex0;
varying vec2 v_TexCoord;

void main( void ) {  
    vec4 col = texture2D(u_Tex0, v_TexCoord);
	if (col.w > 0) {
		vec2 uPos = v_TexCoord.xy - 0.4;
		
		float multiplier = 0.005;
		const float step = 5.0;
		const float loop = 100.0;
		const float timeScale = 1.5;
		
		vec4 redGodColor = vec4(0.0);
		for(float i = 1.0; i < loop; i++) {		
			float t = u_Time * timeScale - step * i;
			vec2 point = vec2(0.55 * sin(t), 0.5 * sin(t * .02));
			point += vec2(0.55 * cos(t * 0.1), 0.5 * sin(t * 1.3));
			point /= 2. * sin(i);
			float componentColor = multiplier / ((uPos.x - point.x) * (uPos.x - point.x) + (uPos.y - point.y) * (uPos.y - point.y)) / i;
			redGodColor += vec4(componentColor / 1.0, componentColor / 4.0, componentColor, 1.0);
		}
		
		vec4 color = vec4(0.0);
		color += pow(redGodColor, vec4(0.5, 2.1, 1.1, 1.0));
		
		gl_FragColor = col + color;
	}
	else
		gl_FragColor = col;
}